/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.business.IManageCertificateBusiness;
import eu.europa.ec.bdmsl.business.IManageParticipantIdentifierBusiness;
import eu.europa.ec.bdmsl.business.IManageServiceMetadataBusiness;
import eu.europa.ec.bdmsl.common.bo.ParticipantBO;
import eu.europa.ec.bdmsl.common.bo.ParticipantListBO;
import eu.europa.ec.bdmsl.common.bo.ServiceMetadataPublisherBO;

import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import eu.europa.ec.bdmsl.common.enums.DNSSubSomainRecordTypeEnum;
import eu.europa.ec.bdmsl.common.exception.BadRequestException;
import eu.europa.ec.bdmsl.common.exception.SMPOutOfParticipantQuotaException;
import eu.europa.ec.bdmsl.common.exception.SubDomainOutOfParticipantQuotaException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.test.DnsMessageSenderServiceMock;
import eu.europa.ec.bdmsl.security.ICertificateAuthentication;
import eu.europa.ec.bdmsl.security.UnsecureAuthentication;
import eu.europa.ec.bdmsl.service.dns.IDnsMessageSenderService;
import org.busdox.servicemetadata.locator._1.ParticipantIdentifierPageType;
import org.busdox.transport.identifiers._1.ParticipantIdentifierType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.bean.override.mockito.MockitoSpyBean;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.xbill.DNS.CNAMERecord;
import org.xbill.DNS.DClass;
import org.xbill.DNS.Name;
import org.xbill.DNS.Record;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Flavio SANTOS
 * @since 28/02/2017
 */
class ManageParticipantIdentifierServiceImplTest extends AbstractJUnit5Test {

    @Autowired
    private IDnsMessageSenderService dnsMessageSenderService;

    @Autowired
    private ICertificateAuthentication customAuthentication;

    @Autowired
    private IManageParticipantIdentifierService manageParticipantIdentifierService;

    @Autowired
    @MockitoSpyBean
    private IManageParticipantIdentifierBusiness iManageParticipantIdentifierBusiness;

    @Autowired
    @MockitoSpyBean
    private IManageServiceMetadataBusiness iManageServiceMetadataBusiness;

    @Autowired
    @MockitoSpyBean
    private IManageServiceMetadataService iManageServiceMetadataService;

    @Autowired
    @MockitoSpyBean
    private IManageCertificateBusiness iManageCertificateBusiness;

    @Autowired
    @MockitoSpyBean
    private ConversionService conversionService;

    @BeforeEach
    void before() throws Exception {
        UnsecureAuthentication authentication = new UnsecureAuthentication();
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        loggingService = Mockito.spy(loggingService);
        ReflectionTestUtils.setField(manageParticipantIdentifierService, "loggingService", loggingService);
        ReflectionTestUtils.setField(iManageParticipantIdentifierBusiness, "loggingService", loggingService);
        ReflectionTestUtils.setField(iManageParticipantIdentifierBusiness, "dnsClientService", dnsClientService);
    }

    @AfterEach
    public void after() {
        Mockito.clearInvocations(loggingService);
    }

    private void checkParticipant(String smpId, String... ids) throws TechnicalException {
        for (String id : ids) {
            ParticipantBO partBO = new ParticipantBO();
            partBO.setSmpId(smpId);
            partBO.setScheme("iso6523-actorid-upis");
            partBO.setParticipantId(id);
            ParticipantBO found = manageParticipantIdentifierService.getManageParticipantIdentifierBusiness().findParticipant(partBO);
            assertNull(found);
        }
    }

    private ParticipantListBO createParticipant(String smpId, String... ids) {
        ParticipantIdentifierPageType participantIdentifierPageType = new ParticipantIdentifierPageType();
        participantIdentifierPageType.setServiceMetadataPublisherID(smpId);
        for (String id : ids) {
            ParticipantIdentifierType participantType = new ParticipantIdentifierType();
            participantType.setScheme("iso6523-actorid-upis");
            participantType.setValue(id);
            participantIdentifierPageType.getParticipantIdentifiers().add(participantType);
        }
        return conversionService.convert(participantIdentifierPageType, ParticipantListBO.class);
    }

    @Test
    void testIsParticipantSMPExceededDomainQuota() throws Exception {
        String smpId = "foundUnsecure";
        String domainId = "acc.edelivery.tech.ec.europa.eu";
        ServiceMetadataPublisherBO smp = iManageServiceMetadataService.read(smpId);

        long currNumber = iManageParticipantIdentifierBusiness.getParticipantCountForSMPDomain(smp);
        subdomainDAO.updateSubDomainValues(domainId,
                null, null, null, null, null, BigInteger.valueOf(currNumber),
                null);


        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId("9957:" + UUID.randomUUID());
        participantBO.setSmpId(smpId);
        participantBO.setScheme("iso6523-actorid-upis");

        SubDomainOutOfParticipantQuotaException exception = assertThrows(SubDomainOutOfParticipantQuotaException.class,
                () -> manageParticipantIdentifierService.create(participantBO)
        );

        assertNotNull(exception);
        assertEquals("[ERR-120] SubDomain: [" + domainId + "] exceeded max. number [" + (currNumber)
                + "] of allowed participants for the subdomain. Expected new count [" + (currNumber + 1) + "]!", exception.getMessage());
    }

    @Test
    void testIsParticipantSMPExceededQuota() throws Exception {
        String smpId = "foundUnsecure";
        String domainId = "acc.edelivery.tech.ec.europa.eu";
        ServiceMetadataPublisherBO smp = iManageServiceMetadataService.read(smpId);

        long currNumber = iManageParticipantIdentifierBusiness.getParticipantCountForSMP(smp);
        subdomainDAO.updateSubDomainValues(domainId,
                null, null, null, null, null, null,
                BigInteger.valueOf(currNumber));


        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId("9957:" + UUID.randomUUID());
        participantBO.setSmpId(smpId);
        participantBO.setScheme("iso6523-actorid-upis");

        SMPOutOfParticipantQuotaException exception = assertThrows(SMPOutOfParticipantQuotaException.class,
                () -> manageParticipantIdentifierService.create(participantBO)
        );

        assertNotNull(exception);
        assertEquals("[ERR-121] SMP: [" + smpId + "] exceeded max. number [" + (currNumber)
                + "] of allowed participants for the SMP. Expected new count [" + (currNumber + 1) + "]!", exception.getMessage());
    }

    @Test
    void testIsParticipantAlreadyCreatedInDNSMustNotThrowException() throws Exception {
        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId("9957:" + UUID.randomUUID());
        participantBO.setSmpId("foundUnsecure");
        participantBO.setScheme("iso6523-actorid-upis");
        List<Record> records = new ArrayList<Record>() {
            {
                add(new CNAMERecord(new Name("B-40ffaa7d142dbc3f620da712bf95da37.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu."), DClass.IN, 60, new Name("smp.test.com.eu" + ".")));
            }
        };
        Mockito.doReturn(records).when(dnsClientService).lookup("B-40ffaa7d142dbc3f620da712bf95da37.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu", 5);

        manageParticipantIdentifierService.create(participantBO);

        ParticipantBO found = manageParticipantIdentifierService.getManageParticipantIdentifierBusiness().findParticipant(participantBO);
        assertNotNull(found);
        assertEquals(participantBO.getParticipantId(), found.getParticipantId());
    }

    @Test
    void testIsParticipantAlreadyCreatedInDNS_NaptrOnlyDomain() throws Exception {
        // GIVEN
        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId("9957:995799579957");
        participantBO.setSmpId("smpId");
        participantBO.setScheme("iso6523-actorid-upis");

        SubdomainBO subdomainBO = new SubdomainBO();
        subdomainBO.setDnsRecordTypes(DNSSubSomainRecordTypeEnum.NAPTR.getCode());
        subdomainBO.setSubdomainName("acc.edelivery.tech.ec.europa.eu");
        subdomainBO.setDnsZone("acc.edelivery.tech.ec.europa.eu");

        ServiceMetadataPublisherBO smpBO = new ServiceMetadataPublisherBO();
        smpBO.setSubdomain(subdomainBO);
        smpBO.setSmpId("smpId");
        smpBO.setCertificateId("certificateId");

        givenParticipant(participantBO, smpBO, subdomainBO, Optional.empty());
        Mockito.doNothing().when(loggingService).info(Mockito.anyString());
        Mockito.doReturn(true).when(dnsClientService).isParticipantAlreadyCreated(participantBO, DNSSubSomainRecordTypeEnum.NAPTR);

        // WHEN
        manageParticipantIdentifierService.create(participantBO);

        // THEN
        Mockito.verify(loggingService).warn("The participant identifier [9957:995799579957] with scheme: [iso6523-actorid-upis] already exist in DNS: [acc.edelivery.tech.ec.europa.eu]");
    }

    @Test
    void testIsParticipantAlreadyCreatedInDNS() throws Exception {
        // GIVEN
        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId("9957:995799579957");
        participantBO.setSmpId("smpId");
        participantBO.setScheme("iso6523-actorid-upis");

        SubdomainBO subdomainBO = new SubdomainBO();
        subdomainBO.setDnsRecordTypes(DNSSubSomainRecordTypeEnum.NAPTR.getCode());
        subdomainBO.setSubdomainName("acc.edelivery.tech.ec.europa.eu");
        subdomainBO.setDnsZone("acc.edelivery.tech.ec.europa.eu");

        ServiceMetadataPublisherBO smpBO = new ServiceMetadataPublisherBO();
        smpBO.setSubdomain(subdomainBO);
        smpBO.setSmpId("smpId");
        smpBO.setCertificateId("certificateId");

        givenParticipant(participantBO, smpBO, subdomainBO, Optional.empty());
        Mockito.doNothing().when(loggingService).info(Mockito.anyString());
        Mockito.doReturn(true).when(dnsClientService).isParticipantAlreadyCreated(participantBO, DNSSubSomainRecordTypeEnum.NAPTR);

        // WHEN
        manageParticipantIdentifierService.create(participantBO);

        // THEN
        Mockito.verify(loggingService).warn("The participant identifier [9957:995799579957] with scheme: [iso6523-actorid-upis] already exist in DNS: [acc.edelivery.tech.ec.europa.eu]");
    }

    @Test
    void testIsParticipantAlreadyCreatedInDatabase() throws Exception {
        // GIVEN
        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId("9957:995799579957");
        participantBO.setSmpId("smpId");
        participantBO.setScheme("iso6523-actorid-upis");

        SubdomainBO subdomainBO = new SubdomainBO();
        subdomainBO.setSubdomainName("acc.edelivery.tech.ec.europa.eu");
        subdomainBO.setDnsZone("acc.edelivery.tech.ec.europa.eu");
        subdomainBO.setDnsRecordTypes(DNSSubSomainRecordTypeEnum.NAPTR.getCode());

        ServiceMetadataPublisherBO smpBO = new ServiceMetadataPublisherBO();
        smpBO.setSubdomain(subdomainBO);
        smpBO.setSmpId("smpId");
        smpBO.setCertificateId("certificateId");

        givenParticipant(participantBO, smpBO, subdomainBO, Optional.of(participantBO));

        // WHEN
        BadRequestException result = assertThrows(BadRequestException.class, () -> manageParticipantIdentifierService.create(participantBO));

        // THEN
        assertEquals("[ERR-106] The participant value: 9957:995799579957' with scheme: 'iso6523-actorid-upis' already exist on subdomain: 'acc.edelivery.tech.ec.europa.eu'", result.getMessage());
    }

    private void givenParticipant(ParticipantBO participantBO, ServiceMetadataPublisherBO smpBO, SubdomainBO subdomainBO, Optional<ParticipantBO> persistedParticipantBO) throws Exception {
        ReflectionTestUtils.setField(manageParticipantIdentifierService, "manageServiceMetadataBusiness", iManageServiceMetadataBusiness);
        ReflectionTestUtils.setField(manageParticipantIdentifierService, "manageCertificateBusiness", iManageCertificateBusiness);
        ReflectionTestUtils.setField(manageParticipantIdentifierService, "manageParticipantIdentifierBusiness", iManageParticipantIdentifierBusiness);

        Mockito.doReturn(smpBO).when(iManageServiceMetadataBusiness).verifySMPExist(participantBO.getSmpId());
        Mockito.doReturn(Optional.empty()).when(iManageParticipantIdentifierBusiness).getParticipantForDomain(participantBO, subdomainBO);
        Mockito.doReturn(true).when(iManageCertificateBusiness).equalsCertificate(Mockito.anyString(), Mockito.anyString());
        Mockito.doNothing().when(dnsClientService).createDNSRecordsForParticipant(participantBO, smpBO);
        Mockito.doNothing().when(iManageParticipantIdentifierBusiness).createParticipant(participantBO);
        Mockito.doReturn(persistedParticipantBO).when(iManageParticipantIdentifierBusiness).getParticipantForDifferentSMPOnDomain(participantBO, smpBO);
    }

    @Test
    void testCreateListParticipantAfterCheckingDnsException() throws Exception {
        // if exists in dns it must not throw exception

        final String smpId = "foundUnsecure";
        // First we ensure the participants don't exist
        final String partId1 = "0088:1505831429576";
        final String partId2 = "0002:1505831369852";
        final String partId3 = "0007:1234567891011";

        checkParticipant(smpId, partId1, partId2, partId3);
        ParticipantListBO participantListBO = createParticipant(smpId, partId1, partId2, partId3);
        List<Record> records = new ArrayList<Record>() {
            {
                add(new CNAMERecord(new Name("B-8073ec5e8880d0738a14770347e9b5c4.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu."), DClass.IN, 60, new Name("smp.test.com.eu" + ".")));
            }
        };
        Mockito.doReturn(records).when(dnsClientService).lookup("B-8073ec5e8880d0738a14770347e9b5c4.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu", 5);

        manageParticipantIdentifierService.createList(participantListBO);

        Mockito.verify(dnsClientService, Mockito.atLeastOnce()).lookup("B-8073ec5e8880d0738a14770347e9b5c4.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu", 5);
    }

    @Test
    void testCreateListParticipantFailSMPExceededQuota() throws Exception {
        // if exists in dns it must not throw exception

        final String smpId = "foundUnsecure";
        String domainId = "acc.edelivery.tech.ec.europa.eu";
        // First we ensure the participants don't exist
        final String partId1 = "0088:" + UUID.randomUUID();
        final String partId2 = "0002:" + UUID.randomUUID();
        final String partId3 = "0007:" + UUID.randomUUID();

        ServiceMetadataPublisherBO smp = iManageServiceMetadataService.read(smpId);
        long currNumber = iManageParticipantIdentifierBusiness.getParticipantCountForSMP(smp);
        subdomainDAO.updateSubDomainValues(domainId,
                null, null, null, null, null, null,
                BigInteger.valueOf(currNumber + 2));


        checkParticipant(smpId, partId1, partId2, partId3);
        ParticipantListBO participantListBO = createParticipant(smpId, partId1, partId2, partId3);

        SMPOutOfParticipantQuotaException exception = assertThrows(SMPOutOfParticipantQuotaException.class,
                () -> manageParticipantIdentifierService.createList(participantListBO)
        );

        assertNotNull(exception);
        assertEquals("[ERR-121] SMP: [" + smpId + "] exceeded max. number [" + (currNumber + 2)
                + "] of allowed participants for the SMP. Expected new count [" + (currNumber + participantListBO.getParticipantBOList().size()) + "]!", exception.getMessage());
    }

    @Test
    void testCreateListParticipantFailDomainExceededQuota() throws Exception {
        // if exists in dns it must not throw exception

        final String smpId = "foundUnsecure";
        String domainId = "acc.edelivery.tech.ec.europa.eu";
        // First we ensure the participants don't exist
        final String partId1 = "0088:" + UUID.randomUUID();
        final String partId2 = "0002:" + UUID.randomUUID();
        final String partId3 = "0007:" + UUID.randomUUID();

        ServiceMetadataPublisherBO smp = iManageServiceMetadataService.read(smpId);
        long currNumber = iManageParticipantIdentifierBusiness.getParticipantCountForSMPDomain(smp);
        subdomainDAO.updateSubDomainValues(domainId,
                null, null, null, null, null,
                BigInteger.valueOf(currNumber + 2), null);


        checkParticipant(smpId, partId1, partId2, partId3);
        ParticipantListBO participantListBO = createParticipant(smpId, partId1, partId2, partId3);

        SubDomainOutOfParticipantQuotaException exception = assertThrows(SubDomainOutOfParticipantQuotaException.class,
                () -> manageParticipantIdentifierService.createList(participantListBO)
        );

        assertNotNull(exception);
        assertEquals("[ERR-120] SubDomain: [" + domainId + "] exceeded max. number [" + (currNumber + 2)
                + "] of allowed participants for the subdomain. Expected new count [" + (currNumber + participantListBO.getParticipantBOList().size()) + "]!", exception.getMessage());
    }

    @Test
    void testCreateListParticipantAfterCheckingDnsOk() throws Exception {
        final String smpId = "foundUnsecure";
        // First we ensure the participants don't exist
        final String partId1 = "0088:" + System.currentTimeMillis();

        checkParticipant(smpId, partId1);
        ParticipantListBO participantListBO = createParticipant(smpId, partId1);

        manageParticipantIdentifierService.create(participantListBO.getParticipantBOList().get(0));

        ParticipantBO found = manageParticipantIdentifierService.getManageParticipantIdentifierBusiness().findParticipant(participantListBO.getParticipantBOList().get(0));
        assertNotNull(found);
    }

    @Test
    void testCreateListParticipantsAfterCheckingDnsOk() throws Exception {
        final String smpId = "foundUnsecure";
        // First we ensure the participants don't exist
        final String partId1 = "0088:" + System.currentTimeMillis();
        final String partId2 = "0002:" + System.currentTimeMillis();
        final String partId3 = "0007:" + System.currentTimeMillis();

        checkParticipant(smpId, partId1, partId2, partId3);
        ParticipantListBO participantListBO = createParticipant(smpId, partId1, partId2, partId3);

        manageParticipantIdentifierService.createList(participantListBO);

        List<ParticipantBO> list = manageParticipantIdentifierService.getManageParticipantIdentifierBusiness().findParticipants(smpId, participantListBO.getParticipantBOList());
        assertEquals(3, list.size());
    }

    @Test
    void testCreateParticipantAfterCheckDnsOk() throws Exception {
        ParticipantBO partBO = new ParticipantBO();
        partBO.setSmpId("foundUnsecure");
        partBO.setScheme("iso6523-actorid-upis");
        partBO.setParticipantId("0009:123456789Base32");

        manageParticipantIdentifierService.create(partBO);

        ParticipantBO found = manageParticipantIdentifierService.getManageParticipantIdentifierBusiness().findParticipant(partBO);
        assertNotNull(found);
    }

    @Test
    void testCreateListParticipantsWithAllRecords() throws Exception {
        //GIVE - WHEN
        String messages = createListParticipantsForCheckingDNSRecords("22591");
        //THEN
        assertThat(messages, containsString("B-b47596d8f2feee286c486152994284ec.iso6523-actorid-upis.22591.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tCNAME\tSMPTESTFOREDELIVERY22591.publisher.22591.acc.edelivery.tech.ec.europa.eu."));
        assertThat(messages, containsString("B-68c85b378d7d6f2268596b37599746ef.iso6523-actorid-upis.22591.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tCNAME\tSMPTESTFOREDELIVERY22591.publisher.22591.acc.edelivery.tech.ec.europa.eu."));
        assertThat(messages, containsString("I3PSVHPIAXWDJDCYDNMWVUQQF5XV4Z3UXG5SDGB2ZSRPGL3KRA2A.iso6523-actorid-upis.22591.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tNAPTR\t100 10 \"U\" \"Meta:SMP\" \"!.*!http://logicalAddress!\" ."));
        assertThat(messages, containsString("TV6S7TSE7722NCL6YHHTKQ2DYS7QTYNFO7BKFLFVDH5KTDPEMFFQ.iso6523-actorid-upis.22591.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tNAPTR\t100 10 \"U\" \"Meta:SMP\" \"!.*!http://logicalAddress!\" ."));
    }

    @Test
    void testCreateParticipantWithAllRecords() throws Exception {
        //GIVE - WHEN
        String messages = createParticipantForCheckingDNSRecords("22591", "0009:EDELIVERY-5");

        //THEN
        assertThat(messages, containsString("B-9550dc017e1d20f8b6ac8abce70f8595.iso6523-actorid-upis.22591.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tCNAME\tSMPTESTFOREDELIVERY22591.publisher.22591.acc.edelivery.tech.ec.europa.eu."));
        assertThat(messages, containsString("AY75LSVG45FBWD7M6EF4M6BB7CHR54DCKXXMS5YYVMZNMYUGQG2A.iso6523-actorid-upis.22591.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tNAPTR\t100 10 \"U\" \"Meta:SMP\" \"!.*!http://logicalAddress!\" ."));
    }

    @Test
    void testCreateListParticipantsWithCnameOnly() throws Exception {
        //GIVE - WHEN
        String messages = createListParticipantsForCheckingDNSRecords("22592");

        //THEN
        assertFalse(messages.contains("NAPTR"));
        assertThat(messages, containsString("B-60416de3081ca3c65f3fb3bfe0cac9c9.iso6523-actorid-upis.22592.acc.edelivery.tech.ec.europa.eu.\t0\tANY\tANY\n"));
        assertThat(messages, containsString("B-60416de3081ca3c65f3fb3bfe0cac9c9.iso6523-actorid-upis.22592.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tCNAME\tSMPTESTFOREDELIVERY22592.publisher.22592.acc.edelivery.tech.ec.europa.eu."));
        assertThat(messages, containsString("B-dcd6e067687109e87c3b0ba8c5b1b23c.iso6523-actorid-upis.22592.acc.edelivery.tech.ec.europa.eu.\t0\tANY\tANY\n"));
        assertThat(messages, containsString("B-dcd6e067687109e87c3b0ba8c5b1b23c.iso6523-actorid-upis.22592.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tCNAME\tSMPTESTFOREDELIVERY22592.publisher.22592.acc.edelivery.tech.ec.europa.eu."));
    }

    @Test
    void testCreateParticipantWithCnameOnly() throws Exception {
        //GIVE - WHEN
        String messages = createParticipantForCheckingDNSRecords("22592", "0009:EDELIVERY-4");

        //THEN
        assertFalse(messages.contains("NAPTR"));
        assertThat(messages, containsString("B-dd0edf287e001cabfb5d9702e9d4e8ff.iso6523-actorid-upis.22592.acc.edelivery.tech.ec.europa.eu.\t0\tANY\tANY\n"));
        assertThat(messages, containsString("B-dd0edf287e001cabfb5d9702e9d4e8ff.iso6523-actorid-upis.22592.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tCNAME\tSMPTESTFOREDELIVERY22592.publisher.22592.acc.edelivery.tech.ec.europa.eu."));
    }

    @Test
    void testCreateListParticipantsWithNaptrOnly() throws Exception {
        //GIVE - WHEN
        String messages = createListParticipantsForCheckingDNSRecords("22593");

        //THEN
        assertFalse(messages.contains("CNAME"));
        assertThat(messages, containsString("5H2SKG545Q7ODSE7RF6DAMGWOALS6LOC5VLUNLEZOLBCXHN4PIFQ.iso6523-actorid-upis.22593.acc.edelivery.tech.ec.europa.eu.\t0\tANY\tANY"));
        assertThat(messages, containsString("5H2SKG545Q7ODSE7RF6DAMGWOALS6LOC5VLUNLEZOLBCXHN4PIFQ.iso6523-actorid-upis.22593.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tNAPTR\t100 10 \"U\" \"Meta:SMP\" \"!.*!http://logicalAddress!\" ."));
        assertThat(messages, containsString("JGCNLI7MC5U7RMAYG6DQR2MBCVRYRZ5WOMHXGFLOKHKCNCH4PCLA.iso6523-actorid-upis.22593.acc.edelivery.tech.ec.europa.eu.\t0\tANY\tANY"));
        assertThat(messages, containsString("JGCNLI7MC5U7RMAYG6DQR2MBCVRYRZ5WOMHXGFLOKHKCNCH4PCLA.iso6523-actorid-upis.22593.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tNAPTR\t100 10 \"U\" \"Meta:SMP\" \"!.*!http://logicalAddress!\" ."));
    }

    @Test
    void testCreateParticipantWithNaptrOnly() throws Exception {
        //GIVE - WHEN
        String messages = createParticipantForCheckingDNSRecords("22593", "0009:EDELIVERY-6");

        //THEN
        assertFalse(messages.contains("CNAME"));
        assertThat(messages, containsString("27PEXT7AKSRULLV55VRGWUI2SHSGEDLPEI5IVJ56JPIHSVLZWMIA.iso6523-actorid-upis.22593.acc.edelivery.tech.ec.europa.eu.\t0\tANY\tANY"));
        assertThat(messages, containsString("27PEXT7AKSRULLV55VRGWUI2SHSGEDLPEI5IVJ56JPIHSVLZWMIA.iso6523-actorid-upis.22593.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tNAPTR\t100 10 \"U\" \"Meta:SMP\" \"!.*!http://logicalAddress!\" ."));
    }

    private String createListParticipantsForCheckingDNSRecords(String serialNumber) throws Exception {
        //GIVEN
        DnsMessageSenderServiceMock dnsMessageSenderService = (DnsMessageSenderServiceMock) this.dnsMessageSenderService;
        customAuthentication.blueCoatAuthentication(serialNumber, "CN=SMP_TEST_FOR_EDELIVERY_" + serialNumber + ",O=DG-DIGIT,C=BE", "CN=SMP_TEST_FOR_EDELIVERY_" + serialNumber + ",O=DG-DIGIT,C=BE", true);

        // First we ensure the participants don't exist
        final String partId1 = "0009:EDELIVERY-1" + serialNumber;
        final String partId2 = "0009:EDELIVERY-2" + serialNumber;
        final String smpId = "SMPTESTFOREDELIVERY" + serialNumber;
        checkParticipant(smpId, partId1, partId2);
        ParticipantListBO participantListBO = createParticipant(smpId, partId1, partId2);

        //WHEN
        manageParticipantIdentifierService.createList(participantListBO);

        //THEN
        List<ParticipantBO> list = manageParticipantIdentifierService.getManageParticipantIdentifierBusiness().findParticipants(smpId, participantListBO.getParticipantBOList());
        assertEquals(list.size(), 2);

        return dnsMessageSenderService.getMessages();
    }

    private String createParticipantForCheckingDNSRecords(String serialNumber, String participantId) throws Exception {
        //GIVEN
        DnsMessageSenderServiceMock dnsMessageSenderService = (DnsMessageSenderServiceMock) this.dnsMessageSenderService;
        customAuthentication.blueCoatAuthentication(serialNumber, "CN=SMP_TEST_FOR_EDELIVERY_" + serialNumber + ",O=DG-DIGIT,C=BE", "CN=SMP_TEST_FOR_EDELIVERY_" + serialNumber + ",O=DG-DIGIT,C=BE", true);

        // First we ensure the participants don't exist
        final String smpId = "SMPTESTFOREDELIVERY" + serialNumber;
        checkParticipant(smpId, participantId);
        ParticipantListBO participantListBO = createParticipant(smpId, participantId);

        //WHEN
        manageParticipantIdentifierService.create(participantListBO.getParticipantBOList().get(0));

        //THEN
        List<ParticipantBO> list = manageParticipantIdentifierService.getManageParticipantIdentifierBusiness().findParticipants(smpId, participantListBO.getParticipantBOList());
        assertEquals(list.size(), 1);
        return dnsMessageSenderService.getMessages();
    }
}

