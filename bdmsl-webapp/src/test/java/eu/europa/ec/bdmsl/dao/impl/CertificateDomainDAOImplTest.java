/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.impl;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.bo.CertificateDomainBO;
import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import eu.europa.ec.bdmsl.common.exception.CertificateNotFoundException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.dao.ICertificateDomainDAO;
import eu.europa.ec.bdmsl.dao.ISubdomainDAO;
import eu.europa.ec.bdmsl.security.CertificateDetails;
import eu.europa.ec.bdmsl.util.CertificateUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Flavio SANTOS.
 */
class CertificateDomainDAOImplTest extends AbstractJUnit5Test {

    @Autowired
    private ICertificateDomainDAO certificateDomainDAO;

    @Autowired
    private ISubdomainDAO subdomainDAO;

    @Test
    @Transactional
    void testCreateCertificateDomain() throws TechnicalException {
        CertificateDomainBO certificateDomainBO = new CertificateDomainBO();

        certificateDomainBO.setRootCA(true);
        certificateDomainBO.setCertificate("CN=certificate,OU=DEVELOPMENT,O=DIGIT,C=BE");
        certificateDomainBO.setCrl("http://localhost/test");
        certificateDomainBO.setValidFrom(Calendar.getInstance());
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, 2);
        certificateDomainBO.setValidFrom(c);
        // there must be at least on domain in test database
        List<SubdomainBO> lst = subdomainDAO.findAll();
        assertFalse(lst.isEmpty());
        certificateDomainBO.setSubdomain(lst.get(1));

        certificateDomainDAO.createCertificateDomain(certificateDomainBO);

        CertificateDomainBO persisted = certificateDomainDAO.findDomain(certificateDomainBO.getCertificate(), certificateDomainBO.isRootCA());

        assertNotNull(persisted.getId());
        assertEquals(certificateDomainBO.getCertificate(), persisted.getCertificate());
        assertEquals(certificateDomainBO.getCrl(), persisted.getCrl());
        assertEquals(certificateDomainBO.getValidFrom(), persisted.getValidFrom());
        assertEquals(certificateDomainBO.getValidTo(), persisted.getValidTo());
    }

    @Test
    @Transactional
    void testUpdateDomainCrl() throws TechnicalException {
        String newCertificateId = "CN=DEVELOPER,O=DIGIT,C=BE:123456789";

        CertificateDomainBO certificateDomainBO = new CertificateDomainBO();
        certificateDomainBO.setRootCA(true);
        certificateDomainBO.setCrl("http://localhost/crl");
        certificateDomainBO.setCertificate(newCertificateId);
        // there must be at least on domain in test database
        List<SubdomainBO> lst = subdomainDAO.findAll();
        assertFalse(lst.isEmpty());
        certificateDomainBO.setSubdomain(lst.get(1));
        certificateDomainDAO.createCertificateDomain(certificateDomainBO);

        CertificateDomainBO res = certificateDomainDAO.findCertificateDomainByCertificate(newCertificateId);
        assertEquals("http://localhost/crl", res.getCrl());

        certificateDomainDAO.updateCertificateDomain(res, null, "http://test.ec.europa.eu/crl", null);

        CertificateDomainBO res2 = certificateDomainDAO.findCertificateDomainByCertificate(newCertificateId);
        assertEquals("http://test.ec.europa.eu/crl", res2.getCrl());
        assertEquals(lst.get(1).getSubdomainName(), res2.getSubdomain().getSubdomainName());

    }

    @Test
    @Transactional
    void testUpdateDomainCrlEmpty() throws TechnicalException {
        String newCertificateId = "CN=DEVELOPER,O=DIGIT,C=BE:123456789";

        CertificateDomainBO certificateDomainBO = new CertificateDomainBO();
        certificateDomainBO.setRootCA(true);
        certificateDomainBO.setCrl("http://localhost/crl");
        certificateDomainBO.setCertificate(newCertificateId);
        // there must be at least on domain in test database
        List<SubdomainBO> lst = subdomainDAO.findAll();
        assertFalse(lst.isEmpty());
        certificateDomainBO.setSubdomain(lst.get(1));
        certificateDomainDAO.createCertificateDomain(certificateDomainBO);

        CertificateDomainBO res = certificateDomainDAO.findCertificateDomainByCertificate(newCertificateId);
        assertEquals("http://localhost/crl", res.getCrl());

        certificateDomainDAO.updateCertificateDomain(res, null, "", Boolean.FALSE);

        CertificateDomainBO res2 = certificateDomainDAO.findCertificateDomainByCertificate(newCertificateId);
        assertNull(res2.getCrl());
        assertEquals(lst.get(1).getSubdomainName(), res2.getSubdomain().getSubdomainName());
        assertFalse(res2.isAdmin());
    }

    @Test
    @Transactional
    void testUpdateDomainSubdomain() throws TechnicalException {
        String newCertificateId = "CN=DEVELOPER,O=DIGIT,C=BE:123456789";

        CertificateDomainBO certificateDomainBO = new CertificateDomainBO();
        certificateDomainBO.setRootCA(true);
        certificateDomainBO.setCrl("http://localhost/crl");
        certificateDomainBO.setCertificate(newCertificateId);
        // there must be at least on domain in test database
        List<SubdomainBO> lst = subdomainDAO.findAll();
        assertFalse(lst.isEmpty());
        certificateDomainBO.setSubdomain(lst.get(1));
        certificateDomainDAO.createCertificateDomain(certificateDomainBO);

        CertificateDomainBO res = certificateDomainDAO.findCertificateDomainByCertificate(newCertificateId);
        assertEquals(lst.get(1).getSubdomainName(), res.getSubdomain().getSubdomainName());

        certificateDomainDAO.updateCertificateDomain(res, lst.get(0), null, Boolean.TRUE);

        CertificateDomainBO res2 = certificateDomainDAO.findCertificateDomainByCertificate(newCertificateId);
        assertEquals(res.getCrl(), res2.getCrl());
        assertEquals(lst.get(0).getSubdomainName(), res2.getSubdomain().getSubdomainName());
        assertTrue(res2.isAdmin());

    }

    @Test
    void testFindCertificateDomainByCertificate() throws TechnicalException {
        String certificateId = CertificateUtils.removeSerialFromSubject("CN=SMP_TEST_CHANGE_CERTIFICATE,O=DG-DIGIT,C=BE:123456789959854848");
        assertEquals("CN=SMP_TEST_CHANGE_CERTIFICATE,O=DG-DIGIT,C=BE", certificateDomainDAO.findCertificateDomainByCertificate(certificateId).getCertificate());
    }

    @Test
    void testFindCertificateDomainByCertificateNull() throws TechnicalException {
        String certificateId = CertificateUtils.removeSerialFromSubject("CN=SMP_TEST_CHANGE_CERTIFICATE_1,O=DG-DIGIT,C=BE:123456789959854848");
        assertNull(certificateDomainDAO.findCertificateDomainByCertificate(certificateId));
    }

    @Test
    void testFindDomainForRootCA() throws TechnicalException {
        positiveCasesForFindDomain("CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA,OU=FOR TEST PURPOSES ONLY,O=NATIONAL IT AND TELECOM AGENCY,C=DK", "CN=EHEALTH_SMP_SAMPLE_1,O=DG-DIGIT,C=BE", "CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA,OU=FOR TEST PURPOSES ONLY,O=NATIONAL IT AND TELECOM AGENCY,C=DK");
    }

    @Test
    void testFindDomainForNonRootCA() throws TechnicalException {
        positiveCasesForFindDomain("CN=EHEALTH SERVICE METADATA PUBLISHER TEST CA,OU=FOR TEST PURPOSES ONLY,O=NATIONAL IT AND TELECOM AGENCY,C=DK", "CN=EHEALTH_SMP_77777777,O=DG-DIGIT,C=BE", "CN=EHEALTH_SMP_77777777,O=DG-DIGIT,C=BE");
    }

    @Test
    void testFindDomainForNotFound() throws TechnicalException {
        assertThrows(CertificateNotFoundException.class,
                () ->
                        negativeCasesForFindDomain("CN=EHEALTH SERVICE METADATA PUBLISHER TEST CA,OU=FOR TEST PURPOSES ONLY,O=NATIONAL IT AND TELECOM AGENCY,C=DK", "CN=EHEALTH_SMP_SAMPLE_1,O=DG-DIGIT,C=UK")
        );
    }

    @Test
    void testFindDomainForWrongFormatFound() throws TechnicalException {
        assertThrows(CertificateNotFoundException.class,
                () ->
                        negativeCasesForFindDomain("CN=EHEALTH SERVICE METADATA PUBLISHER TEST CA", "CN=EHEALTH_SMP_SAMPLE_1"));

    }


    @Test
    @Transactional
    void listDomainCertificatesCaseInsensitive() throws TechnicalException {
        String newCertificateId = "CN=DEVELOPER,O=DIGIT,C=BE:123456789";

        CertificateDomainBO certificateDomainBO = new CertificateDomainBO();
        certificateDomainBO.setRootCA(true);
        certificateDomainBO.setCrl("http://localhost/crl");
        certificateDomainBO.setCertificate(newCertificateId);
        // there must be at least on domain in test database
        List<SubdomainBO> lst = subdomainDAO.findAll();
        assertFalse(lst.isEmpty());

        certificateDomainBO.setSubdomain(lst.get(1));
        certificateDomainDAO.createCertificateDomain(certificateDomainBO);

        List<CertificateDomainBO> res = certificateDomainDAO.listCertificateDomains(newCertificateId.toLowerCase(), null);
        assertEquals(1, res.size());


    }

    @Test
    @Transactional
    void listDomainCertificatesPartialName() throws TechnicalException {
        String newCertificateId = "CN=DEVELOPER,O=DIGIT,C=BE:123456789FirstTest";

        CertificateDomainBO certificateDomainBO = new CertificateDomainBO();
        certificateDomainBO.setRootCA(true);
        certificateDomainBO.setCrl("http://localhost/crl");
        certificateDomainBO.setCertificate(newCertificateId);
        // there must be at least on domain in test database
        List<SubdomainBO> lst = subdomainDAO.findAll();
        assertFalse(lst.isEmpty());

        certificateDomainBO.setSubdomain(lst.get(1));
        certificateDomainDAO.createCertificateDomain(certificateDomainBO);

        List<CertificateDomainBO> res = certificateDomainDAO.listCertificateDomains("123456789FIRSTTEST", null);
        assertEquals(1, res.size());
    }

    @Test
    @Transactional
    void listDomainCertificatesPartialNameByDomain() throws TechnicalException {
        String newCertificateId = "CN=DEVELOPER,O=DIGIT,C=BE:123456789NewTest";

        CertificateDomainBO certificateDomainBO = new CertificateDomainBO();
        certificateDomainBO.setRootCA(true);
        certificateDomainBO.setCrl("http://localhost/crl");
        certificateDomainBO.setCertificate(newCertificateId);
        // there must be at least on domain in test database
        List<SubdomainBO> lst = subdomainDAO.findAll();
        assertFalse(lst.isEmpty());

        certificateDomainBO.setSubdomain(lst.get(1));
        certificateDomainDAO.createCertificateDomain(certificateDomainBO);

        List<CertificateDomainBO> res = certificateDomainDAO.listCertificateDomains("123456789NewTest", lst.get(1));
        assertFalse(res.isEmpty());
    }


    private void negativeCasesForFindDomain(String issuer, String subject) throws TechnicalException {
        CertificateDetails certificateDetails = new CertificateDetails();
        certificateDetails.setRootCertificateDN(issuer);
        certificateDetails.setSubject(subject);

        CertificateDomainBO certificateDomainBO = certificateDomainDAO.findDomain(certificateDetails);
    }

    private void positiveCasesForFindDomain(String issuer, String subject, String expected) throws TechnicalException {
        CertificateDetails certificateDetails = new CertificateDetails();
        certificateDetails.setRootCertificateDN(issuer);
        certificateDetails.setSubject(subject);

        CertificateDomainBO certificateDomainBO = certificateDomainDAO.findDomain(certificateDetails);
        assertNotNull(certificateDomainBO);
        assertEquals(expected, certificateDomainBO.getCertificate());
    }
}
