/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2017 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.util;
/**
 * In this class we define constants used in the test.
 * <p/>
 *
 * @author Thomas Dussart
 * @since 13/05/2024
 */

public class Constant {
    public final static String NEW_CERT_PUBLIC_KEY = "-----BEGIN CERTIFICATE-----\n" +
            "MIICqjCCAhOgAwIBAgIBATANBgkqhkiG9w0BAQUFADB4MQswCQYDVQQGEwJCRTEL\n" +
            "MAkGA1UECAwCQkUxETAPBgNVBAcMCEJydXNzZWxzMQ4wDAYDVQQKDAVESUdJVDEL\n" +
            "MAkGA1UECwwCQjQxDzANBgNVBAMMBnJvb3RDTjEbMBkGCSqGSIb3DQEJARYMcm9v\n" +
            "dEB0ZXN0LmJlMB4XDTE1MDMxNzE2MjEyOFoXDTI1MDMxNDE2MjEyOFowgYAxCzAJ\n" +
            "BgNVBAYTAkJFMQswCQYDVQQIDAJCRTERMA8GA1UEBwwIQnJ1c3NlbHMxDjAMBgNV\n" +
            "BAoMBURJR0lUMQswCQYDVQQLDAJCNDETMBEGA1UEAwwKcmVjZWl2ZXJDTjEfMB0G\n" +
            "CSqGSIb3DQEJARYQcmVjZWl2ZXJAdGVzdC5iZTCBnzANBgkqhkiG9w0BAQEFAAOB\n" +
            "jQAwgYkCgYEA97XDD51lJIdVde8GZNpFkrBBODpBWptieZvLTplnEFYK2q9vXVat\n" +
            "Q9sBfgV+06lbtuv4iwtHiYM1TlkEHDjJ+PseIrLT5UmV64kIZMVSfq9pDuhN5Ifr\n" +
            "Mj9ueY80bQLpNGqvypXuP8acjuiBhUrlG+pkHiy/L6nCbEWR3DB0TtUCAwEAAaM7\n" +
            "MDkwHwYDVR0jBBgwFoAUcJVKHhadZHwwYEZ52v5skf3q1zgwCQYDVR0TBAIwADAL\n" +
            "BgNVHQ8EBAMCBPAwDQYJKoZIhvcNAQEFBQADgYEAVaJIsBpOXvOsl7edJx47f6Pg\n" +
            "4g+y3eeWFPmmWH0+v+yybks8nvWlnWHxO2YWwFXYl+kgZNSKu6uKbv4cX98shByl\n" +
            "D+VA7xhh+JBVCWA3QN4xK8OqJif4QQjaJxMsYIe0ROfv3AnLEkAuJkH8dmrQZy72\n" +
            "E91Q9nytFTwPo1XnkR4=\n" +
            "-----END CERTIFICATE-----\n";
}
