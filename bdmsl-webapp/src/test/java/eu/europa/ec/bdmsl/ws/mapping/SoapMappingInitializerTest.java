/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.ws.mapping;

import ec.services.wsdl.bdmsl.admin.data._1.ChangeCertificate;
import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.bo.ChangeCertificateBO;
import eu.europa.ec.bdmsl.common.bo.ServiceMetadataPublisherBO;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.security.UnsecureAuthentication;
import org.busdox.servicemetadata.locator._1.PublisherEndpointType;
import org.busdox.servicemetadata.locator._1.ServiceMetadataPublisherServiceType;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.security.core.context.SecurityContextHolder;

import java.security.Security;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Adrien FERIAL
 * @since 16/06/2015
 */
class SoapMappingInitializerTest extends AbstractJUnit5Test {

    private String newCertPublicKey = "-----BEGIN CERTIFICATE-----\n" +
            "MIICpTCCAg6gAwIBAgIBATANBgkqhkiG9w0BAQUFADB4MQswCQYDVQQGEwJCRTEL\n" +
            "MAkGA1UECAwCQkUxETAPBgNVBAcMCEJydXNzZWxzMQ4wDAYDVQQKDAVESUdJVDEL\n" +
            "MAkGA1UECwwCQjQxDzANBgNVBAMMBnJvb3RDTjEbMBkGCSqGSIb3DQEJARYMcm9v\n" +
            "dEB0ZXN0LmJlMB4XDTE1MDMxNzE2MTkwN1oXDTI1MDMxNDE2MTkwN1owfDELMAkG\n" +
            "A1UEBhMCQkUxCzAJBgNVBAgMAkJFMREwDwYDVQQHDAhCcnVzc2VsczEOMAwGA1UE\n" +
            "CgwFRElHSVQxCzAJBgNVBAsMAkI0MREwDwYDVQQDDAhzZW5kZXJDTjEdMBsGCSqG\n" +
            "SIb3DQEJARYOc2VuZGVyQHRlc3QuYmUwgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJ\n" +
            "AoGBANxLUPjIn7R0CsHf86kIwNzCu+6AdmWM8fBLUHL+VXT6ayr1kwgGbFMb/vUU\n" +
            "X6a46jRCiZBM+9IK1Hpjg9QX/QIQiWtvD+yDr6jUxahZ/w13kqFG/K81IVu9DwLB\n" +
            "oiNwDvQ6l6UbvMvV+1nWy3gjRcKlFs/C+E2uybgJxSM/sMkbAgMBAAGjOzA5MB8G\n" +
            "A1UdIwQYMBaAFHCVSh4WnWR8MGBGedr+bJH96tc4MAkGA1UdEwQCMAAwCwYDVR0P\n" +
            "BAQDAgTwMA0GCSqGSIb3DQEBBQUAA4GBAK6idNRxyeBmqPoSKxq7Ck3ej6R2QPyW\n" +
            "bwZ+6/S7iCRt8PfgOu++Yu5YEjlUX1hlkbQKF/JuKTLqxNnKIE6Ef65+JP2ZaI9O\n" +
            "2wdzpRclAhAd00XbNKpyipr4jMdWmu2U8vyBBwn/utG1ZrLhAUiqnPvmaQrResiG\n" +
            "HM2xzCmVwtse\n" +
            "-----END CERTIFICATE-----\n";

    @Autowired
    private ConversionService conversionService;

    @BeforeAll
    static void beforeClass() throws TechnicalException {
        UnsecureAuthentication authentication = new UnsecureAuthentication();
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    @Test
    void testSmpBoToSmpWs() {
        ServiceMetadataPublisherServiceType input = new ServiceMetadataPublisherServiceType();
        PublisherEndpointType publisherEndpointType = new PublisherEndpointType();
        publisherEndpointType.setLogicalAddress("logicalAddress");
        publisherEndpointType.setPhysicalAddress("physicalAddress");
        input.setPublisherEndpoint(publisherEndpointType);
        input.setServiceMetadataPublisherID("123");

        ServiceMetadataPublisherBO expected = new ServiceMetadataPublisherBO();
        expected.setSmpId("123");
        expected.setLogicalAddress("logicalAddress");
        expected.setPhysicalAddress("physicalAddress");
        expected.setCertificateId("unsecure-http-client");

        ServiceMetadataPublisherBO resultSmpBo = conversionService.convert(input, ServiceMetadataPublisherBO.class);

        assertEquals(expected, resultSmpBo);
    }

    @Test
    void testChangeCertificateWStoChangeCertificateBO() {


        ChangeCertificate cct = new ChangeCertificate();
        cct.setServiceMetadataPublisherID("SMPID");
        cct.setNewCertificatePublicKey(newCertPublicKey.getBytes());

        ChangeCertificateBO ccBO = new ChangeCertificateBO();
        ccBO.setServiceMetadataPublisherID("SMPID");
        ccBO.setPublicKey(newCertPublicKey.getBytes());

        ChangeCertificateBO mappedBO = conversionService.convert(cct, ChangeCertificateBO.class);

        assertEquals(ccBO, mappedBO);
    }
}
