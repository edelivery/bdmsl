/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.security;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Flavio SANTOS
 * @since 24/05/2017
 */
class UnsecureAuthenticationTest extends AbstractJUnit5Test {

    @Test
    void testUnsecureAuthentication() throws Exception {
        UnsecureAuthentication authentication = new UnsecureAuthentication();
        assertEquals("unsecure-http-client", authentication.getName());
        assertEquals("PreAuthenticatedAnonymousPrincipal", authentication.getPrincipal().toString());
        assertEquals("CN=unsecure_subject,O=delete_in_production,C=only_for_testing", ((CertificateDetails) authentication.getDetails()).getSubject());
        assertEquals("CN=unsecure_root,O=delete_in_production,C=only_for_testing", ((CertificateDetails) authentication.getDetails()).getIssuer());
        assertEquals("CN=unsecure_root,O=delete_in_production,C=only_for_testing", ((CertificateDetails) authentication.getDetails()).getRootCertificateDN());
        assertEquals("unsecure-http-client", ((CertificateDetails) authentication.getDetails()).getCertificateId());
        assertEquals("unsecure-serial", ((CertificateDetails) authentication.getDetails()).getSerial());
    }

    @Test
    void testUnsecureAuthenticationAuthorities() throws Exception {
        UnsecureAuthentication authentication = new UnsecureAuthentication();
        assertEquals(3, authentication.getAuthorities().size());

        assertEquals("ROLE_SMP", ((List<SimpleGrantedAuthority>) authentication.getAuthorities()).get(0).toString());
        assertEquals("ROLE_MONITOR", ((List<SimpleGrantedAuthority>) authentication.getAuthorities()).get(1).toString());
        assertEquals("ROLE_ADMIN", ((List<SimpleGrantedAuthority>) authentication.getAuthorities()).get(2).toString());
    }

}
