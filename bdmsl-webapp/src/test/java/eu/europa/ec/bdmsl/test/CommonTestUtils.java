/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.test;

import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import eu.europa.ec.bdmsl.common.util.Constant;
import eu.europa.ec.bdmsl.security.CertificateDetails;
import eu.europa.ec.bdmsl.util.HttpConnectionMock;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.*;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.springframework.aop.framework.Advised;
import org.springframework.aop.support.AopUtils;
import org.springframework.security.crypto.bcrypt.BCrypt;

import java.io.FileInputStream;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Flavio SANTOS
 * @since 29/11/2016
 */
public class CommonTestUtils {

    public static final String ADMIN_PLAINTEXT_PASSWORD = "thisisasha256password";
    public static final String ADMIN_HASHED_PASSWORD = BCrypt.hashpw(ADMIN_PLAINTEXT_PASSWORD, BCrypt.gensalt());
    public static final String TEST_SERIAL_NUMBER = "2016112131415";

    public static Date convertStringToDate(String dateStr) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        return formatter.parse(dateStr);
    }


    public static X509Certificate createCertificate(String issuer, String subject) throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, -1);
        Date notBefore = calendar.getTime();
        calendar.add(Calendar.YEAR, 2);
        Date notAfter = calendar.getTime();
        return createCertificate(TEST_SERIAL_NUMBER, issuer, subject, notBefore, notAfter, false);
    }

    public static X509Certificate createCertificate(String issuer, String subject, boolean hasCrl) throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, -1);
        Date notBefore = calendar.getTime();
        calendar.add(Calendar.YEAR, 2);
        Date notAfter = calendar.getTime();
        return createCertificate(TEST_SERIAL_NUMBER, issuer, subject, notBefore, notAfter, hasCrl);
    }

    public static X509Certificate createCertificate(String serialNumber, String issuer, String subject, boolean hasCrl) throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, -1);
        Date notBefore = calendar.getTime();
        calendar.add(Calendar.YEAR, 2);
        Date notAfter = calendar.getTime();
        return createCertificate(serialNumber, issuer, subject, notBefore, notAfter, hasCrl);
    }

    public static X509Certificate createCertificate(String issuer, String subject, Date startDate, Date expiryDate, boolean hasCrl) throws Exception {
        return createCertificate(TEST_SERIAL_NUMBER, issuer, subject, startDate, expiryDate, hasCrl);
    }

    public static X509Certificate createCertificate(String serialNumber, String issuer, String subject, Date startDate, Date expiryDate) throws Exception {
        return createCertificate(serialNumber, issuer, subject, startDate, expiryDate, true);
    }

    public static X509Certificate createCertificate(String serialNumber, String issuer, String subject, Date startDate, Date expiryDate, boolean hasCRL) throws Exception {

        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(1024);
        KeyPair key = keyGen.generateKeyPair();
        X509v3CertificateBuilder certBuilder = new X509v3CertificateBuilder(new X500Name(issuer), new BigInteger(serialNumber, 16), startDate, expiryDate, new X500Name(subject), SubjectPublicKeyInfo.getInstance(key.getPublic().getEncoded()));
        if (hasCRL) {
            //CRL Distribution Points
            DistributionPointName distPointOne = new DistributionPointName(new GeneralNames(
                    new GeneralName(GeneralName.uniformResourceIdentifier, Thread.currentThread().getContextClassLoader().getResource("test.crl").toString())));

            DistributionPoint[] distPoints = new DistributionPoint[]{new DistributionPoint(distPointOne, null, null)};
            certBuilder.addExtension(Extension.cRLDistributionPoints, false, new CRLDistPoint(distPoints));
        }
        ContentSigner sigGen = new JcaContentSignerBuilder("SHA1WithRSAEncryption").setProvider("BC").build(key.getPrivate());
        return new JcaX509CertificateConverter().setProvider("BC").getCertificate(certBuilder.build(sigGen));
    }

    public static X509Certificate createCertificate(String serialNumber, String issuer, String subject, Date startDate, Date expiryDate, List<String> distributionList) throws Exception {

        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(1024);
        KeyPair key = keyGen.generateKeyPair();
        X509v3CertificateBuilder certBuilder = new X509v3CertificateBuilder(new X500Name(issuer), new BigInteger(serialNumber, 16), startDate, expiryDate, new X500Name(subject), SubjectPublicKeyInfo.getInstance(key.getPublic().getEncoded()));
        if (!distributionList.isEmpty()) {

            List<DistributionPoint> distributionPoints = distributionList.stream().map(url -> {
                DistributionPointName distPointOne = new DistributionPointName(new GeneralNames(
                        new GeneralName(GeneralName.uniformResourceIdentifier, url)));

                return new DistributionPoint(distPointOne, null, null);
            }).collect(Collectors.toList());

            certBuilder.addExtension(Extension.cRLDistributionPoints, false, new CRLDistPoint(distributionPoints.toArray(new DistributionPoint[]{})));
        }

        ContentSigner sigGen = new JcaContentSignerBuilder("SHA1WithRSAEncryption").setProvider("BC").build(key.getPrivate());
        return new JcaX509CertificateConverter().setProvider("BC").getCertificate(certBuilder.build(sigGen));
    }

    public static X509Certificate[] createCertificateChain(String[] subjects, Date startDate, Date expiryDate) throws Exception {
        KeyStore keyStore = KeyStore.getInstance("JKS");
        keyStore.load(new FileInputStream(Thread.currentThread().getContextClassLoader().getResource("keystore.jks").getFile()), "test".toCharArray());

        String issuer = null;
        PrivateKey issuerKey = null;
        long iSerial = 10000;
        X509Certificate[] certs = new X509Certificate[subjects.length];

        int index = subjects.length;
        for (String sbj : subjects) {
            KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
            keyGen.initialize(1024);
            KeyPair key = keyGen.generateKeyPair();

            X509v3CertificateBuilder certBuilder = new X509v3CertificateBuilder(new X500Name(issuer == null ? sbj : issuer),
                    BigInteger.valueOf(iSerial++), startDate, expiryDate, new X500Name(sbj),
                    SubjectPublicKeyInfo.getInstance(key.getPublic().getEncoded()));

            ContentSigner sigGen = new JcaContentSignerBuilder("SHA1WithRSAEncryption")
                    .setProvider("BC").build(issuerKey == null ? key.getPrivate() : issuerKey);

            certs[--index] = new JcaX509CertificateConverter().setProvider("BC").getCertificate(certBuilder.build(sigGen));
            issuer = sbj;
            issuerKey = key.getPrivate();

        }
        return certs;
    }

    public static CertificateDetails createCertificateDetails(String serialNumber, String issuer, String subject) {
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.YEAR, -5);
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.YEAR, 20);
        return createCertificateDetails(serialNumber, issuer, subject, getPastYearDate(1).getTime(), getFutureYearDate(1).getTime());
    }

    public static CertificateDetails createCertificateDetails(String serialNumber, String issuer, String subject, Date startDate, Date expiryDate) {
        CertificateDetails certificateDetails = new CertificateDetails();
        certificateDetails.setSerial(serialNumber);
        certificateDetails.setIssuer(issuer);
        certificateDetails.setRootCertificateDN(issuer);
        certificateDetails.setSubject(subject);
        certificateDetails.setValidFrom(DateUtils.toCalendar(startDate));
        certificateDetails.setValidTo(DateUtils.toCalendar(expiryDate));
        certificateDetails.setCertificateId(subject + (StringUtils.isEmpty(serialNumber) ? "" :
                ":" + StringUtils.leftPad(serialNumber, 32, "0")));
        return certificateDetails;
    }

    public static String createNewPKIHeaderCertificateForPeppolForBlueCoat(String subject) throws Exception {
        String issuer = "CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA - G2, OU=FOR TEST ONLY, O=OpenPEPPOL AISBL, C=BE";
        return createHeaderCertificateForBlueCoat(subject, issuer, false);
    }

    public static String createHeaderCertificateForBlueCoat() throws Exception {
        return createHeaderCertificateForBlueCoat(null, false);
    }

    public static String createHeaderCertificateForBlueCoat(String subject, boolean isPolicyParamIncluded) throws Exception {
        String issuer = "CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA,  C=DK, O=NATIONAL IT AND TELECOM AGENCY, OU=FOR TEST PURPOSES ONLY";
        return createHeaderCertificateForBlueCoat(subject, issuer, isPolicyParamIncluded);
    }

    public static String createHeaderCertificateForBlueCoat(String serial, String issuer, String subject) {
        return createHeaderCertificateForBlueCoat(serial, issuer, subject, getPastYearDate(1).getTime(), getFutureYearDate(5).getTime());
    }

    public static String createHeaderCertificateForBlueCoat(String tSubject, String issuer, boolean isPolicyParamIncluded) {
        synchronized (CommonTestUtils.class) {
            String serial = "123ABCD";
            // different order for the issuer certificate with extra spaces

            String subject = "O=DG-DIGIT,C=BE,CN=SMP_123456789";
            if (!StringUtils.isEmpty(tSubject)) {
                subject = tSubject;
            }

            Calendar validFrom = getPastYearDate(1);
            Calendar validTo = getFutureYearDate(5);

            if (!isPolicyParamIncluded) {
                return createHeaderCertificateForBlueCoat(serial, issuer, subject, validFrom.getTime(), validTo.getTime());
            }
            return createHeaderCertificateForBlueCoatWithPolicyOids(serial, issuer, subject, validFrom.getTime(), validTo.getTime(), "&policy_oids=5.0.6.0.4.1.0000.66.99");
        }
    }

    public static String createHeaderCertificateForBlueCoat(String serialNumber, String issuer, String subject, Date startDate, Date expiryDate) {
        DateFormat df = new SimpleDateFormat("MMM d hh:mm:ss yyyy zzz", Constant.LOCALE);
        return "serial=" + serialNumber + "&subject=" + subject + "&validFrom=" + df.format(startDate) + "&validTo=" + df.format(expiryDate) + "&issuer=" + issuer;
    }

    public static String createHeaderCertificateForBlueCoatWithPolicyOids(String serialNumber, String issuer, String subject, Date startDate, Date expiryDate, String policy_oids) {
        return createHeaderCertificateForBlueCoat(serialNumber, issuer, subject, startDate, expiryDate) + policy_oids;
    }

    public static Calendar getFutureYearDate(int years) {
        Calendar date = Calendar.getInstance();
        date.set(date.get(Calendar.YEAR) + years, 1, 1);
        return date;
    }

    public static Calendar getPastYearDate(int years) {
        Calendar date = Calendar.getInstance();
        date.set(date.get(Calendar.YEAR) - years, 1, 1);
        return date;
    }

    /**
     * Convert  Spring Proxy Interface into Implementation
     *
     * @param proxy is the autowired interface
     * @return is the expected implementation
     */
    public static <T> T getTargetObject(Object proxy) throws Exception {
        if (AopUtils.isJdkDynamicProxy(proxy)) {
            return (T) getTargetObject(((Advised) proxy).getTargetSource().getTarget());
        }
        return (T) proxy;
    }

    /**
     * Helper method for creating SubdomainBO
     *
     * @param domainName
     * @param dnsZone
     * @param regExp
     * @param urlScheme
     * @return
     */
    public static SubdomainBO createSubDomainBO(String domainName, String dnsZone, String regExp, String recordType, String urlScheme) {
        return createSubDomainBO(domainName, dnsZone, regExp, recordType, urlScheme, null);
    }

    /**
     * Helper method for creating SubdomainBO
     *
     * @param domainName
     * @param dnsZone
     * @param regExp
     * @param urlScheme
     * @param certRegExp
     * @return
     */
    public static SubdomainBO createSubDomainBO(String domainName, String dnsZone, String regExp, String recordType, String urlScheme, String certRegExp) {
        SubdomainBO smpBO = new SubdomainBO();
        smpBO.setSubdomainName(domainName);
        smpBO.setDnsRecordTypes(recordType);
        smpBO.setDnsZone(dnsZone);
        smpBO.setParticipantIdRegexp(regExp);
        smpBO.setSmpUrlSchemas(urlScheme);
        smpBO.setSmpCertSubjectRegex(certRegExp);
        smpBO.setMaxParticipantCountForDomain(BigInteger.valueOf(999999));
        smpBO.setMaxParticipantCountForSMP(BigInteger.valueOf(100000));
        return smpBO;
    }

    public static X509Certificate loadCertificate(String filename) throws CertificateException {
        CertificateFactory fact = CertificateFactory.getInstance("X.509");

        X509Certificate cer = (X509Certificate)
                fact.generateCertificate(HttpConnectionMock.class.getResourceAsStream("/certificates/" + filename));
        return cer;
    }
}
