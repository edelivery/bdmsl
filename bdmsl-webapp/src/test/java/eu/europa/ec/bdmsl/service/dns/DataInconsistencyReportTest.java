/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.dns;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.reports.DataInconsistencyReport;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.net.UnknownHostException;
import java.util.List;

import static java.net.InetAddress.getLocalHost;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Flavio SANTOS
 */
class DataInconsistencyReportTest extends AbstractJUnit5Test {

    @Autowired
    private DataInconsistencyReport report;

    private final String emailContent_OK = "## DATA INCONSISTENCY TOOL ## \n" +
            "\n" +
            "\n" +
            "# Server Info # \n" +
            "\n" +
            "Server Local Host: " + getLocalHost().getHostName() + "\n" +
            "\n" +
            "# Overview # \n" +
            "\n" +
            "There is 1 SMP(s) in the DNS\n" +
            "There is 1 SMP(s) in the Database\n" +
            "There are 0 Inactive SMP(s) in the Database\n" +
            "There are 10 Participants(s) in the Database\n" +
            "There are 0 Disabled (CNAME) Participant(s) in the Database\n" +
            "There are 0 Disabled (NAPTR) Participant(s) in the Database\n" +
            "There are 10 CNAME for Participants(s) in the DNS\n" +
            "There are 10 NAPTR for Participants(s) in the DNS\n" +
            "\n" +
            "There are no inconsistencies between the database and the DNS!";

    private final String emailContentSmpMissingInDb = "## DATA INCONSISTENCY TOOL ## \n" +
            "\n" +
            "\n" +
            "# Server Info # \n" +
            "\n" +
            "Server Local Host: " + getLocalHost().getHostName() + "\n" +
            "\n" +
            "# Overview # \n" +
            "\n" +
            "There are 0 SMP(s) in the DNS\n" +
            "There are 0 SMP(s) in the Database\n" +
            "There are 0 Inactive SMP(s) in the Database\n" +
            "There are 0 Participants(s) in the Database\n" +
            "There are 0 Disabled (CNAME) Participant(s) in the Database\n" +
            "There are 0 Disabled (NAPTR) Participant(s) in the Database\n" +
            "There are 0 CNAME for Participants(s) in the DNS\n" +
            "There are 0 NAPTR for Participants(s) in the DNS\n" +
            "There is [ 1 ] difference between the database and the DNS. Please take appropriate actions to solve the problem.\n" +
            "\n" +
            "\n" +
            "# Detail #\n" +
            "\n" +
            "The SMP with hash SMP_01.publisher.acc.edelivery.tech.ec.europa.eu is in the DNS with target target.localhost  but it is not in the Database or is Inactive.";

    private final String emailContentSmpMissingInDNS = "## DATA INCONSISTENCY TOOL ## \n" +
            "\n" +
            "\n" +
            "# Server Info # \n" +
            "\n" +
            "Server Local Host: " + getLocalHost().getHostName() + "\n" +
            "\n" +
            "# Overview # \n" +
            "\n" +
            "There are 0 SMP(s) in the DNS\n" +
            "There are 0 SMP(s) in the Database\n" +
            "There are 0 Inactive SMP(s) in the Database\n" +
            "There are 0 Participants(s) in the Database\n" +
            "There are 0 Disabled (CNAME) Participant(s) in the Database\n" +
            "There are 0 Disabled (NAPTR) Participant(s) in the Database\n" +
            "There are 0 CNAME for Participants(s) in the DNS\n" +
            "There are 0 NAPTR for Participants(s) in the DNS\n" +
            "There is [ 1 ] difference between the database and the DNS. Please take appropriate actions to solve the problem.\n" +
            "\n" +
            "\n" +
            "# Detail #\n" +
            "\n" +
            "The SMP with ID null hash SMP_01.publisher.acc.edelivery.tech.ec.europa.eu is with target target.localhost in the Database but it is not in the DNS.";

    private final String emailContentPartcCNameMissingInDB = "## DATA INCONSISTENCY TOOL ## \n" +
            "\n" +
            "\n" +
            "# Server Info # \n" +
            "\n" +
            "Server Local Host: " + getLocalHost().getHostName() + "\n" +
            "\n" +
            "# Overview # \n" +
            "\n" +
            "There are 0 SMP(s) in the DNS\n" +
            "There are 0 SMP(s) in the Database\n" +
            "There are 0 Inactive SMP(s) in the Database\n" +
            "There are 0 Participants(s) in the Database\n" +
            "There are 0 Disabled (CNAME) Participant(s) in the Database\n" +
            "There are 0 Disabled (NAPTR) Participant(s) in the Database\n" +
            "There are 0 CNAME for Participants(s) in the DNS\n" +
            "There are 0 NAPTR for Participants(s) in the DNS\n" +
            "There is [ 1 ] difference between the database and the DNS. Please take appropriate actions to solve the problem.\n" +
            "\n" +
            "\n" +
            "# Detail #\n" +
            "\n" +
            "The PARTICIPANT with hash b-1234563a5dd2d6671ea8c5bb61987654.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu and target SMP_01.publisher.acc.edelivery.tech.ec.europa.eu is in the DNS but it is not in the Database or is Inactive.";

    private final String emailContentPartcCNameMissingInDNS = "## DATA INCONSISTENCY TOOL ## \n" +
            "\n" +
            "\n" +
            "# Server Info # \n" +
            "\n" +
            "Server Local Host: " + getLocalHost().getHostName() + "\n" +
            "\n" +
            "# Overview # \n" +
            "\n" +
            "There are 0 SMP(s) in the DNS\n" +
            "There are 0 SMP(s) in the Database\n" +
            "There are 0 Inactive SMP(s) in the Database\n" +
            "There are 0 Participants(s) in the Database\n" +
            "There are 0 Disabled (CNAME) Participant(s) in the Database\n" +
            "There are 0 Disabled (NAPTR) Participant(s) in the Database\n" +
            "There are 0 CNAME for Participants(s) in the DNS\n" +
            "There are 0 NAPTR for Participants(s) in the DNS\n" +
            "There is [ 1 ] difference between the database and the DNS. Please take appropriate actions to solve the problem.\n" +
            "\n" +
            "\n" +
            "# Detail #\n" +
            "\n" +
            "The PARTICIPANT with ID test-scheme:id1212 hash b-1234563a5dd2d6671ea8c5bb61987654.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu and target SMP_01.publisher.acc.edelivery.tech.ec.europa.eu is in the Database but it is not in the DNS.";


    private final String emailContentPartcNaptrMissingInDB = "## DATA INCONSISTENCY TOOL ## \n" +
            "\n" +
            "\n" +
            "# Server Info # \n" +
            "\n" +
            "Server Local Host: " + getLocalHost().getHostName() + "\n" +
            "\n" +
            "# Overview # \n" +
            "\n" +
            "There are 0 SMP(s) in the DNS\n" +
            "There are 0 SMP(s) in the Database\n" +
            "There are 0 Inactive SMP(s) in the Database\n" +
            "There are 0 Participants(s) in the Database\n" +
            "There are 0 Disabled (CNAME) Participant(s) in the Database\n" +
            "There are 0 Disabled (NAPTR) Participant(s) in the Database\n" +
            "There are 0 CNAME for Participants(s) in the DNS\n" +
            "There are 0 NAPTR for Participants(s) in the DNS\n" +
            "There is [ 1 ] difference between the database and the DNS. Please take appropriate actions to solve the problem.\n" +
            "\n" +
            "\n" +
            "# Detail #\n" +
            "\n" +
            "The PARTICIPANT with hash WT2L5VIQLNYE5NGDSHAXBM42LRCP6SOKKZDXYWQILEH7E3H7UB3A.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu and target SMP_01.publisher.acc.edelivery.tech.ec.europa.eu is in the DNS but it is not in the Database or is Inactive.";

    private final String emailContentPartcNaptrMissingInDNS = "## DATA INCONSISTENCY TOOL ## \n" +
            "\n" +
            "\n" +
            "# Server Info # \n" +
            "\n" +
            "Server Local Host: " + getLocalHost().getHostName() + "\n" +
            "\n" +
            "# Overview # \n" +
            "\n" +
            "There are 0 SMP(s) in the DNS\n" +
            "There are 0 SMP(s) in the Database\n" +
            "There are 0 Inactive SMP(s) in the Database\n" +
            "There are 0 Participants(s) in the Database\n" +
            "There are 0 Disabled (CNAME) Participant(s) in the Database\n" +
            "There are 0 Disabled (NAPTR) Participant(s) in the Database\n" +
            "There are 0 CNAME for Participants(s) in the DNS\n" +
            "There are 0 NAPTR for Participants(s) in the DNS\n" +
            "There is [ 1 ] difference between the database and the DNS. Please take appropriate actions to solve the problem.\n" +
            "\n" +
            "\n" +
            "# Detail #\n" +
            "\n" +
            "The PARTICIPANT with ID test-scheme:id1212 hash WT2L5VIQLNYE5NGDSHAXBM42LRCP6SOKKZDXYWQILEH7E3H7UB3A.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu and target SMP_01.publisher.acc.edelivery.tech.ec.europa.eu is in the Database but it is not in the DNS.";


    private final String emailContentMisc = "## DATA INCONSISTENCY TOOL ## \n" +
            "\n" +
            "\n" +
            "# Server Info # \n" +
            "\n" +
            "Server Local Host: " + getLocalHost().getHostName() + "\n" +
            "\n" +
            "# Overview # \n" +
            "\n" +
            "There are 0 SMP(s) in the DNS\n" +
            "There are 0 SMP(s) in the Database\n" +
            "There are 0 Inactive SMP(s) in the Database\n" +
            "There are 0 Participants(s) in the Database\n" +
            "There are 0 Disabled (CNAME) Participant(s) in the Database\n" +
            "There are 0 Disabled (NAPTR) Participant(s) in the Database\n" +
            "There are 0 CNAME for Participants(s) in the DNS\n" +
            "There are 0 NAPTR for Participants(s) in the DNS\n" +
            "There are [ 6 ] differences between the database and the DNS. Please take appropriate actions to solve the problem.\n" +
            "\n" +
            "\n" +
            "# Detail #\n" +
            "\n" +
            "The SMP with ID null hash SMP_01.publisher.acc.edelivery.tech.ec.europa.eu is with target target.localhost in the Database but it is not in the DNS.\n" +
            "The SMP with hash SMP_01.publisher.acc.edelivery.tech.ec.europa.eu is in the DNS with target target-new.localhost  but it is not in the Database or is Inactive.\n" +
            "\n" +
            "The PARTICIPANT with hash b-1234563a5dd2d6671ea8c5bb61987654.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu and target SMP_01.publisher.acc.edelivery.tech.ec.europa.eu is in the DNS but it is not in the Database or is Inactive.\n" +
            "The PARTICIPANT with ID test-scheme:id1212 hash b-1234563a5dd2d6671ea8c5bb61987654.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu and target SMP_01.publisher.acc.edelivery.tech.ec.europa.eu is in the Database but it is not in the DNS.\n" +
            "The PARTICIPANT with ID test-scheme:id1212 hash WT2L5VIQLNYE5NGDSHAXBM42LRCP6SOKKZDXYWQILEH7E3H7UB3A.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu and target SMP_01.publisher.acc.edelivery.tech.ec.europa.eu is in the Database but it is not in the DNS.\n" +
            "The PARTICIPANT with hash WT2L5VIQLNYE5NGDSHAXBM42LRCP6SOKKZDXYWQILEH7E3H7UB3A.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu and target SMP_01.publisher.acc.edelivery.tech.ec.europa.eu is in the DNS but it is not in the Database or is Inactive.\n";


    DataInconsistencyReportTest() throws UnknownHostException {
    }


    @Test
    void testSubjectVerb() {
        DataInconsistencyReport report = new DataInconsistencyReport();

        assertEquals("is", report.getSubjectVerb(1, 1));
        assertEquals("are", report.getSubjectVerb(1, 2));
        assertEquals("", report.getSubjectVerb(2, 1));
        assertEquals("s", report.getSubjectVerb(2, 2));
    }

    @Test
    void testMessageForOverview() {
        assertEquals("There are [ 2 ] differences between the database and the DNS. Please take appropriate actions to solve the problem.", report.messageForOverview(2, null, null));
        assertEquals("There are 2 SMP(s) in the DNS", report.messageForOverview(2, "SMP", "DNS"));
    }

    @Test
    void testCreateReportOk() throws Exception {
        DataInconsistencyAnalyzer dataInconsistencyAnalyzer = new DataInconsistencyAnalyzer();
        dataInconsistencyAnalyzer.setDbTotalOfSMPs(1);
        dataInconsistencyAnalyzer.setDnsTotalOfSMPs(1);

        dataInconsistencyAnalyzer.setDnsTotalOfParticipantsCNAME(10);
        dataInconsistencyAnalyzer.setDnsTotalOfParticipantsNAPTR(10);
        dataInconsistencyAnalyzer.setDbTotalOfParticipantsCNAME(10);
        dataInconsistencyAnalyzer.setDbTotalOfParticipantsNAPTR(10);
        dataInconsistencyAnalyzer.setDbTotalOfParticipants(10);
        report.createReport(dataInconsistencyAnalyzer);

        assertEquals(removeDatetimeFromReport(emailContent_OK), removeDatetimeFromReport(report.getDetailedReport()));
    }

    @Test
    void testCreateReportMissingSMPInDB() throws Exception {
        DataInconsistencyAnalyzer dataInconsistencyAnalyzer = new DataInconsistencyAnalyzer();

        List<DataInconsistencyAnalyzer.DataInconsistencyEntry> dataInconsistencyEntries
                = (List<DataInconsistencyAnalyzer.DataInconsistencyEntry>) ReflectionTestUtils.getField(dataInconsistencyAnalyzer, "dataInconsistencyEntries");

        DataInconsistencyAnalyzer.DataInconsistencyEntry entry = new DataInconsistencyAnalyzer.DataInconsistencyEntry();
        entry.setDomainName("SMP_01.publisher.acc.edelivery.tech.ec.europa.eu");
        entry.setDataType(DataInconsistencyAnalyzer.DataInconsistencyEntry.DataType.SMP);
        entry.setMissingSourceType(DataInconsistencyAnalyzer.DataInconsistencyEntry.MissingSourceType.DATABASE);
        entry.setMissingType(DataInconsistencyAnalyzer.DataInconsistencyEntry.IssueType.CNAME);
        entry.setTarget("target.localhost");
        dataInconsistencyEntries.add(entry);
        report.createReport(dataInconsistencyAnalyzer);

        assertEquals(removeDatetimeFromReport(emailContentSmpMissingInDb), removeDatetimeFromReport(report.getDetailedReport()));
    }

    @Test
    void testCreateReportMissingSMPInDNS() throws Exception {
        DataInconsistencyAnalyzer dataInconsistencyAnalyzer = new DataInconsistencyAnalyzer();

        List<DataInconsistencyAnalyzer.DataInconsistencyEntry> dataInconsistencyEntries
                = (List<DataInconsistencyAnalyzer.DataInconsistencyEntry>) ReflectionTestUtils.getField(dataInconsistencyAnalyzer, "dataInconsistencyEntries");

        DataInconsistencyAnalyzer.DataInconsistencyEntry entry = new DataInconsistencyAnalyzer.DataInconsistencyEntry();
        entry.setDomainName("SMP_01.publisher.acc.edelivery.tech.ec.europa.eu");
        entry.setDataType(DataInconsistencyAnalyzer.DataInconsistencyEntry.DataType.SMP);
        entry.setMissingSourceType(DataInconsistencyAnalyzer.DataInconsistencyEntry.MissingSourceType.DNS);
        entry.setMissingType(DataInconsistencyAnalyzer.DataInconsistencyEntry.IssueType.CNAME);
        entry.setTarget("target.localhost");
        dataInconsistencyEntries.add(entry);
        report.createReport(dataInconsistencyAnalyzer);

        assertEquals(removeDatetimeFromReport(emailContentSmpMissingInDNS), removeDatetimeFromReport(report.getDetailedReport()));
    }


    @Test
    void testCreateReportMissingParticipantCNAMEInDB() throws Exception {
        DataInconsistencyAnalyzer dataInconsistencyAnalyzer = new DataInconsistencyAnalyzer();

        List<DataInconsistencyAnalyzer.DataInconsistencyEntry> dataInconsistencyEntries
                = (List<DataInconsistencyAnalyzer.DataInconsistencyEntry>) ReflectionTestUtils.getField(dataInconsistencyAnalyzer, "dataInconsistencyEntries");

        DataInconsistencyAnalyzer.DataInconsistencyEntry entry = new DataInconsistencyAnalyzer.DataInconsistencyEntry();
        entry.setDomainName("b-1234563a5dd2d6671ea8c5bb61987654.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu");
        entry.setDataType(DataInconsistencyAnalyzer.DataInconsistencyEntry.DataType.PARTICIPANT);
        entry.setMissingSourceType(DataInconsistencyAnalyzer.DataInconsistencyEntry.MissingSourceType.DATABASE);
        entry.setMissingType(DataInconsistencyAnalyzer.DataInconsistencyEntry.IssueType.CNAME);
        entry.setTarget("SMP_01.publisher.acc.edelivery.tech.ec.europa.eu");
        dataInconsistencyEntries.add(entry);
        report.createReport(dataInconsistencyAnalyzer);

        assertEquals(removeDatetimeFromReport(emailContentPartcCNameMissingInDB), removeDatetimeFromReport(report.getDetailedReport()));
    }

    @Test
    void testCreateReportMissingParticipantCNAMEInDNS() throws Exception {
        DataInconsistencyAnalyzer dataInconsistencyAnalyzer = new DataInconsistencyAnalyzer();

        List<DataInconsistencyAnalyzer.DataInconsistencyEntry> dataInconsistencyEntries
                = (List<DataInconsistencyAnalyzer.DataInconsistencyEntry>) ReflectionTestUtils.getField(dataInconsistencyAnalyzer, "dataInconsistencyEntries");

        DataInconsistencyAnalyzer.DataInconsistencyEntry entry = new DataInconsistencyAnalyzer.DataInconsistencyEntry();
        entry.setDomainName("b-1234563a5dd2d6671ea8c5bb61987654.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu");
        entry.setDataType(DataInconsistencyAnalyzer.DataInconsistencyEntry.DataType.PARTICIPANT);
        entry.setId("test-scheme:id1212");
        entry.setMissingSourceType(DataInconsistencyAnalyzer.DataInconsistencyEntry.MissingSourceType.DNS);
        entry.setMissingType(DataInconsistencyAnalyzer.DataInconsistencyEntry.IssueType.CNAME);
        entry.setTarget("SMP_01.publisher.acc.edelivery.tech.ec.europa.eu");
        dataInconsistencyEntries.add(entry);
        report.createReport(dataInconsistencyAnalyzer);

        assertEquals(removeDatetimeFromReport(emailContentPartcCNameMissingInDNS), removeDatetimeFromReport(report.getDetailedReport()));
    }


    @Test
    void testCreateReportMissingParticipantNaptrInDB() throws Exception {
        DataInconsistencyAnalyzer dataInconsistencyAnalyzer = new DataInconsistencyAnalyzer();

        List<DataInconsistencyAnalyzer.DataInconsistencyEntry> dataInconsistencyEntries
                = (List<DataInconsistencyAnalyzer.DataInconsistencyEntry>) ReflectionTestUtils.getField(dataInconsistencyAnalyzer, "dataInconsistencyEntries");

        DataInconsistencyAnalyzer.DataInconsistencyEntry entry = new DataInconsistencyAnalyzer.DataInconsistencyEntry();
        entry.setDomainName("WT2L5VIQLNYE5NGDSHAXBM42LRCP6SOKKZDXYWQILEH7E3H7UB3A.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu");
        entry.setDataType(DataInconsistencyAnalyzer.DataInconsistencyEntry.DataType.PARTICIPANT);
        entry.setMissingSourceType(DataInconsistencyAnalyzer.DataInconsistencyEntry.MissingSourceType.DATABASE);
        entry.setMissingType(DataInconsistencyAnalyzer.DataInconsistencyEntry.IssueType.NAPTR);
        entry.setTarget("SMP_01.publisher.acc.edelivery.tech.ec.europa.eu");
        dataInconsistencyEntries.add(entry);
        report.createReport(dataInconsistencyAnalyzer);

        assertEquals(removeDatetimeFromReport(emailContentPartcNaptrMissingInDB), removeDatetimeFromReport(report.getDetailedReport()));
    }

    @Test
    void testCreateReportMissingParticipantNaptrInDNS() throws Exception {
        DataInconsistencyAnalyzer dataInconsistencyAnalyzer = new DataInconsistencyAnalyzer();

        List<DataInconsistencyAnalyzer.DataInconsistencyEntry> dataInconsistencyEntries
                = (List<DataInconsistencyAnalyzer.DataInconsistencyEntry>) ReflectionTestUtils.getField(dataInconsistencyAnalyzer, "dataInconsistencyEntries");

        DataInconsistencyAnalyzer.DataInconsistencyEntry entry = new DataInconsistencyAnalyzer.DataInconsistencyEntry();
        entry.setDomainName("WT2L5VIQLNYE5NGDSHAXBM42LRCP6SOKKZDXYWQILEH7E3H7UB3A.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu");
        entry.setDataType(DataInconsistencyAnalyzer.DataInconsistencyEntry.DataType.PARTICIPANT);
        entry.setId("test-scheme:id1212");
        entry.setMissingSourceType(DataInconsistencyAnalyzer.DataInconsistencyEntry.MissingSourceType.DNS);
        entry.setMissingType(DataInconsistencyAnalyzer.DataInconsistencyEntry.IssueType.NAPTR);
        entry.setTarget("SMP_01.publisher.acc.edelivery.tech.ec.europa.eu");
        dataInconsistencyEntries.add(entry);
        report.createReport(dataInconsistencyAnalyzer);

        assertEquals(removeDatetimeFromReport(emailContentPartcNaptrMissingInDNS), removeDatetimeFromReport(report.getDetailedReport()));
    }


    @Test
    void testCreateReportWithInconsitencies() throws Exception {
        DataInconsistencyAnalyzer dataInconsistencyAnalyzer = new DataInconsistencyAnalyzer();

        List<DataInconsistencyAnalyzer.DataInconsistencyEntry> dataInconsistencyEntries
                = (List<DataInconsistencyAnalyzer.DataInconsistencyEntry>) ReflectionTestUtils.getField(dataInconsistencyAnalyzer, "dataInconsistencyEntries");

        DataInconsistencyAnalyzer.DataInconsistencyEntry entry = new DataInconsistencyAnalyzer.DataInconsistencyEntry();
        entry.setDomainName("WT2L5VIQLNYE5NGDSHAXBM42LRCP6SOKKZDXYWQILEH7E3H7UB3A.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu");
        entry.setDataType(DataInconsistencyAnalyzer.DataInconsistencyEntry.DataType.PARTICIPANT);
        entry.setId("test-scheme:id1212");
        entry.setMissingSourceType(DataInconsistencyAnalyzer.DataInconsistencyEntry.MissingSourceType.DNS);
        entry.setMissingType(DataInconsistencyAnalyzer.DataInconsistencyEntry.IssueType.NAPTR);
        entry.setTarget("SMP_01.publisher.acc.edelivery.tech.ec.europa.eu");
        dataInconsistencyEntries.add(entry);

        entry = new DataInconsistencyAnalyzer.DataInconsistencyEntry();
        entry.setDomainName("WT2L5VIQLNYE5NGDSHAXBM42LRCP6SOKKZDXYWQILEH7E3H7UB3A.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu");
        entry.setDataType(DataInconsistencyAnalyzer.DataInconsistencyEntry.DataType.PARTICIPANT);
        entry.setMissingSourceType(DataInconsistencyAnalyzer.DataInconsistencyEntry.MissingSourceType.DATABASE);
        entry.setMissingType(DataInconsistencyAnalyzer.DataInconsistencyEntry.IssueType.NAPTR);
        entry.setTarget("SMP_01.publisher.acc.edelivery.tech.ec.europa.eu");
        dataInconsistencyEntries.add(entry);

        entry = new DataInconsistencyAnalyzer.DataInconsistencyEntry();
        entry.setDomainName("b-1234563a5dd2d6671ea8c5bb61987654.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu");
        entry.setDataType(DataInconsistencyAnalyzer.DataInconsistencyEntry.DataType.PARTICIPANT);
        entry.setMissingSourceType(DataInconsistencyAnalyzer.DataInconsistencyEntry.MissingSourceType.DATABASE);
        entry.setMissingType(DataInconsistencyAnalyzer.DataInconsistencyEntry.IssueType.CNAME);
        entry.setTarget("SMP_01.publisher.acc.edelivery.tech.ec.europa.eu");
        dataInconsistencyEntries.add(entry);

        entry = new DataInconsistencyAnalyzer.DataInconsistencyEntry();
        entry.setDomainName("b-1234563a5dd2d6671ea8c5bb61987654.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu");
        entry.setId("test-scheme:id1212");
        entry.setDataType(DataInconsistencyAnalyzer.DataInconsistencyEntry.DataType.PARTICIPANT);
        entry.setMissingSourceType(DataInconsistencyAnalyzer.DataInconsistencyEntry.MissingSourceType.DNS);
        entry.setMissingType(DataInconsistencyAnalyzer.DataInconsistencyEntry.IssueType.CNAME);
        entry.setTarget("SMP_01.publisher.acc.edelivery.tech.ec.europa.eu");
        dataInconsistencyEntries.add(entry);

        entry = new DataInconsistencyAnalyzer.DataInconsistencyEntry();
        entry.setDomainName("SMP_01.publisher.acc.edelivery.tech.ec.europa.eu");
        entry.setDataType(DataInconsistencyAnalyzer.DataInconsistencyEntry.DataType.SMP);
        entry.setMissingSourceType(DataInconsistencyAnalyzer.DataInconsistencyEntry.MissingSourceType.DNS);
        entry.setMissingType(DataInconsistencyAnalyzer.DataInconsistencyEntry.IssueType.CNAME);
        entry.setTarget("target.localhost");
        dataInconsistencyEntries.add(entry);

        entry = new DataInconsistencyAnalyzer.DataInconsistencyEntry();
        entry.setDomainName("SMP_01.publisher.acc.edelivery.tech.ec.europa.eu");
        entry.setDataType(DataInconsistencyAnalyzer.DataInconsistencyEntry.DataType.SMP);
        entry.setMissingSourceType(DataInconsistencyAnalyzer.DataInconsistencyEntry.MissingSourceType.DATABASE);
        entry.setMissingType(DataInconsistencyAnalyzer.DataInconsistencyEntry.IssueType.CNAME);
        entry.setTarget("target-new.localhost");
        dataInconsistencyEntries.add(entry);

        report.createReport(dataInconsistencyAnalyzer);

        assertEquals(removeDatetimeFromReport(emailContentMisc), removeDatetimeFromReport(report.getDetailedReport()));
    }

    private String removeDatetimeFromReport(String report) throws IOException {
        BufferedReader bufReader = new BufferedReader(new StringReader(report));
        String line = null;
        StringBuilder stringBuilder = new StringBuilder();

        while ((line = bufReader.readLine()) != null) {
            if (!line.startsWith("Report created")) {
                stringBuilder.append(line + "\n");
            }
        }

        return stringBuilder.toString();
    }
}
