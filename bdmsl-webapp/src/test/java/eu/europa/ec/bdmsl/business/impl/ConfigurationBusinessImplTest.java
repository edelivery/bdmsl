/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.business.impl;

import eu.europa.ec.bdmsl.common.bo.ConfigurationBO;
import eu.europa.ec.bdmsl.common.enums.SMLPropertyEnum;
import eu.europa.ec.bdmsl.common.exception.BadRequestException;
import eu.europa.ec.bdmsl.common.exception.KeyException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.dao.IConfigurationDAO;
import eu.europa.ec.bdmsl.service.validation.PropertyTypeSMLPropertyValidator;
import eu.europa.ec.edelivery.security.cert.impl.CertificateRVStrategyEnum;
import jakarta.persistence.NoResultException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mockito;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;

/**
 * @author Flavio SANTOS
 * @since 16/05/2017
 */
class ConfigurationBusinessImplTest {

    protected Path resourceDirectory = Paths.get("src", "test", "resources");
    IConfigurationDAO configurationDAO = Mockito.mock(IConfigurationDAO.class);
    PropertyTypeSMLPropertyValidator validator = Mockito.spy(new PropertyTypeSMLPropertyValidator());
    ConfigurationBusinessImpl testInstance = Mockito.spy(new ConfigurationBusinessImpl(configurationDAO, Arrays.asList(validator)));


    @Test
    void testFindConfigurationProperty() throws TechnicalException {
        // given
        String property = "configurationDir";
        String value = "target/";
        doReturn(createConfigurationBO(property, value)).when(configurationDAO).findConfigurationProperty(property);
        // when
        ConfigurationBO configurationBO = testInstance.findConfigurationProperty(property);
        //then
        assertNotNull(configurationBO);
        assertEquals(property, configurationBO.getProperty());
        assertEquals(value, configurationBO.getValue());
    }

    @Test
    void testFindConfigurationPropertyNotOk() throws TechnicalException {
        // given
        String property = "dummyProperty";
        NoResultException exception = new NoResultException("Property does not exist!");
        doThrow(exception).when(configurationDAO).findConfigurationProperty(property);
        // when
        NoResultException resultException = assertThrows(NoResultException.class, () -> testInstance.findConfigurationProperty(property));
        // then
        assertNotNull(resultException);
        assertEquals("Property does not exist!", exception.getMessage());
    }

    @Test
    void testSetPropertyToDatabase() throws TechnicalException {
        // given
        SMLPropertyEnum property = SMLPropertyEnum.CONFIGURATION_DIR;
        String value = "src/test/resources/";
        String description = "property description";
        ConfigurationBO configurationBO = createConfigurationBO(property.getProperty(), value);
        doReturn(configurationBO).when(configurationDAO).setPropertyToDatabase(property, value, description);
        // when
        ConfigurationBO result = testInstance.setPropertyToDatabase(property, value, description);
        //then
        assertNotNull(result);
        assertEquals(property.getProperty(), configurationBO.getProperty());
        assertEquals(value, configurationBO.getValue());
    }

    @ParameterizedTest
    @CsvSource({
            "KEYSTORE_PASSWORD",
            "TRUSTSTORE_PASSWORD",
            "MAIL_SERVER_PASSWORD",
            "HTTP_PROXY_PASSWORD",
            "DNS_TSIG_KEY_VALUE",
    })
    void testSetPropertyToDatabasePassword(SMLPropertyEnum property) throws TechnicalException {
        // given
        String value = "ThisIsMyPassword";
        String encValue = UUID.randomUUID().toString();
        String description = "property description";
        ConfigurationBO configurationBO = createConfigurationBO(property.getProperty(), value);
        doReturn(configurationBO).when(configurationDAO).setPropertyToDatabase(property, encValue, description);
        doReturn(encValue).when(testInstance).encrypt(any());

        // when
        ConfigurationBO result = testInstance.setPropertyToDatabase(property, value, description);
        //then
        assertNotNull(result);
        Mockito.validateMockitoUsage();
        assertEquals(property.getProperty(), configurationBO.getProperty());
        assertEquals("*******", configurationBO.getValue());
    }

    @Test
    void testSetPropertyToDatabaseTSIGValue() throws TechnicalException {
        // given
        SMLPropertyEnum property = SMLPropertyEnum.KEYSTORE_PASSWORD;
        String value = "ThisIsMyPassword";
        String encValue = UUID.randomUUID().toString();
        String description = "property description";
        ConfigurationBO configurationBO = createConfigurationBO(property.getProperty(), value);
        doReturn(configurationBO).when(configurationDAO).setPropertyToDatabase(property, encValue, description);
        doReturn(encValue).when(testInstance).encryptString(value);

        // when
        ConfigurationBO result = testInstance.setPropertyToDatabase(property, value, description);
        //then
        assertNotNull(result);
        Mockito.validateMockitoUsage();
        assertEquals(property.getProperty(), configurationBO.getProperty());
        assertEquals("*******", configurationBO.getValue());
    }

    @Test
    void testSetPropertyToDatabaseInvalidValue() throws TechnicalException {
        // given
        SMLPropertyEnum property = SMLPropertyEnum.AUTH_BLUE_COAT_ENABLED;
        String value = "notvalidboolean";
        String description = "property description";
        ConfigurationBO configurationBO = createConfigurationBO(property.getProperty(), value);
        doReturn(configurationBO).when(configurationDAO).setPropertyToDatabase(property, value, description);
        // when
        BadRequestException requestException = assertThrows(BadRequestException.class, () -> testInstance.setPropertyToDatabase(property, value, description));
        //then
        assertNotNull(requestException);
        assertEquals("[ERR-106] The property [authentication.bluecoat.enabled] of type [BOOLEAN] has an invalid value [notvalidboolean]! Error: Invalid boolean value: [notvalidboolean]. Error: Only {true, false} are allowed!", requestException.getMessage());
    }

    @Test
    void tesEncryptString() throws TechnicalException {
        // given
        String value = "test value to encrypt and decrypt";
        String encryptedValue = "ZOIm+LNGdPEPFEMDu1D9vFKjZr8IRJEyafw+E32k6npxS44cSJ78rZ6R2GH0nmta";

        doReturn("encryptedPasswordKey.key").when(testInstance).getEncryptionFilename();
        doReturn(resourceDirectory.resolve("encryptedPasswordKey.key").toFile()).when(testInstance).getConfigurationFile(anyString());

        // when
        String result = testInstance.encryptString(value);
        //then
        assertNotNull(result);
        assertEquals(encryptedValue, result);

    }

    @Test
    void tesEncryptStringFailKeyNotExits() {
        // given
        String value = "test value to encrypt and decrypt";

        doReturn("encryptedPasswordKey.key").when(testInstance).getEncryptionFilename();
        doReturn(null).when(testInstance).getConfigurationFile(anyString());

        // when
        KeyException requestException = assertThrows(KeyException.class, () -> testInstance.encryptString(value));

        //then
        assertNotNull(requestException);
        assertEquals("[ERR-105] Bad configuration. Encryption key does not exist!", requestException.getMessage());
    }

    @Test
    void tesDecryptString() throws TechnicalException {
        // given
        String decryptedvalue = "test value to encrypt and decrypt";
        String encryptedValue = "ZOIm+LNGdPEPFEMDu1D9vFKjZr8IRJEyafw+E32k6npxS44cSJ78rZ6R2GH0nmta";
        doReturn("encryptedPasswordKey.key").when(testInstance).getEncryptionFilename();
        doReturn(resourceDirectory.resolve("encryptedPasswordKey.key").toFile()).when(testInstance).getConfigurationFile(anyString());

        // when
        String result = testInstance.decryptString(encryptedValue);
        //then
        assertNotNull(result);
        assertEquals(decryptedvalue, result);
    }

    @Test
    void tesDecryptStringFailKeyNotExits() {
        // given
        String encryptedValue = "ZOIm+LNGdPEPFEMDu1D9vFKjZr8IRJEyafw+E32k6npxS44cSJ78rZ6R2GH0nmta";
        doReturn("encryptedPasswordKey.key").when(testInstance).getEncryptionFilename();
        doReturn(null).when(testInstance).getConfigurationFile(anyString());

        // when
        KeyException requestException = assertThrows(KeyException.class, () -> testInstance.decryptString(encryptedValue));

        //then
        assertNotNull(requestException);
        assertEquals("[ERR-105] Bad configuration. Encryption key does not exist!", requestException.getMessage());
    }


    @Test
    void getMonitorToken() {
        // given
        String value = setMockRandomStringValueForProperty(SMLPropertyEnum.MONITOR_TOKEN);
        // when
        String result = testInstance.getMonitorToken();
        //then
        assertEquals(value, result);
    }


    @Test
    void isClientCertEnabled() {
        // given
        String value = setMockStringValueForProperty(SMLPropertyEnum.AUTH_BLUE_COAT_ENABLED, "true");
        // when
        Boolean result = testInstance.isClientCertEnabled();
        //then
        assertEquals(Boolean.parseBoolean(value), result);
    }

    @Test
    void isDeployedOnCluster() {
        // given
        String value = setMockStringValueForProperty(SMLPropertyEnum.BDMSL_CLUSTER_ENABLED, "false");
        // when
        Boolean result = testInstance.isDeployedOnCluster();
        //then
        assertEquals(Boolean.parseBoolean(value), result);
    }

    @Test
    void getSMPCertRegularExpression() {
        // given
        String value = setMockRandomStringValueForProperty(SMLPropertyEnum.AUTH_SMP_CERT_REGEXP);
        // when
        String result = testInstance.getSMPCertRegularExpression();
        //then
        assertEquals(value, result);
    }

    @Test
    void getCertificateChangeCron() {
        // given
        String value = setMockRandomStringValueForProperty(SMLPropertyEnum.CERT_CHANGE_CRON);
        // when
        String result = testInstance.getCertificateChangeCron();
        //then
        assertEquals(value, result);
    }

    @Test
    void getConfigurationFolder() {
        // given
        String value = setMockRandomStringValueForProperty(SMLPropertyEnum.CONFIGURATION_DIR);
        // when
        String result = testInstance.getConfigurationFolder();
        //then
        assertEquals(value, result);
    }

    @Test
    void getInconsistencyReportCron() {
        // given
        String value = setMockRandomStringValueForProperty(SMLPropertyEnum.DIA_CRON);
        // when
        String result = testInstance.getInconsistencyReportCron();
        //then
        assertEquals(value, result);
    }

    @Test
    void getInconsistencyReportMailTo() {
        // given
        String value = setMockRandomStringValueForProperty(SMLPropertyEnum.DIA_MAIL_TO);
        // when
        String result = testInstance.getInconsistencyReportMailTo();
        //then
        assertEquals(value, result);
    }

    @Test
    void getInconsistencyReportMailFrom() {
        // given
        String value = setMockRandomStringValueForProperty(SMLPropertyEnum.DIA_MAIL_FROM);
        // when
        String result = testInstance.getInconsistencyReportMailFrom();
        //then
        assertEquals(value, result);
    }

    @Test
    void getInconsistencyReportGenerateByInstance() {
        // given
        String value = setMockRandomStringValueForProperty(SMLPropertyEnum.DIA_GENERATE_SERVER);
        // when
        String result = testInstance.getInconsistencyReportGenerateByInstance();
        //then
        assertEquals(value, result);
    }

    @Test
    void getMailSMPTHost() {
        // given
        String value = setMockRandomStringValueForProperty(SMLPropertyEnum.MAIL_SERVER_HOST);
        // when
        String result = testInstance.getMailSMPTHost();
        //then
        assertEquals(value, result);
    }

    @Test
    void getMailSMPTPort() {
        // given
        int value = 1234;
        setMockStringValueForProperty(SMLPropertyEnum.MAIL_SERVER_PORT, String.valueOf(value));
        // when
        int result = testInstance.getMailSMPTPort();
        //then
        assertEquals(value, result);
    }

    @Test
    void isDNSSig0Enabled() {
        // given
        String value = setMockStringValueForProperty(SMLPropertyEnum.DNS_SIG0_ENABLED, "true");
        // when
        Boolean result = testInstance.isDNSSig0Enabled();
        //then
        assertEquals(Boolean.parseBoolean(value), result);
    }

    @Test
    void isLegacyDomainAuthorizationEnabled() {
        // given
        String value = setMockStringValueForProperty(SMLPropertyEnum.AUTH_LEGACY_ENABLED, "true");
        // when
        Boolean result = testInstance.isLegacyDomainAuthorizationEnabled();
        //then
        assertEquals(Boolean.parseBoolean(value), result);
    }

    @Test
    void isCertRevocationValidationGraceful() {
        // given
        String value = setMockStringValueForProperty(SMLPropertyEnum.CERT_VERIFICATION_GRACEFUL, "true");
        // when
        Boolean result = testInstance.isCertRevocationValidationGraceful();
        //then
        assertEquals(Boolean.parseBoolean(value), result);
    }

    @Test
    void testGetCertRevocationValidationAllowedUrlProtocols() {
        // given
        String value = setMockStringValueForProperty(SMLPropertyEnum.CRL_ALLOWED_PROTOCOLS, "http,https");
        // when
        List<String> result = testInstance.getCertRevocationValidationAllowedUrlProtocols();
        //then
        assertArrayEquals(Arrays.stream(value.split(",")).toArray(), result.toArray());
    }

    @ParameterizedTest
    @CsvSource({
            ", OCSP_CRL",
            "'', OCSP_CRL",
            "badCode, OCSP_CRL",
            "OCSP_CRL, OCSP_CRL",
            "CRL_OCSP, CRL_OCSP",
            "OCSP_ONLY, OCSP_ONLY",
            "CRL_ONLY, CRL_ONLY",
            "' CRL_ONLY ', CRL_ONLY",
            "crl_only, CRL_ONLY",

    })
    void getCertRevocationValidationStrategy(String value, CertificateRVStrategyEnum expected) {
        // given
        setMockStringValueForProperty(SMLPropertyEnum.CERT_VERIFICATION_STRATEGY, value);
        // when
        CertificateRVStrategyEnum result = testInstance.getCertRevocationValidationStrategy();
        //then
        assertEquals(expected, result);
    }

    @Test
    void getDNSSig0KeyFilename() {
        // given
        String value = setMockRandomStringValueForProperty(SMLPropertyEnum.DNS_SIG0_KEY_FILENAME);
        // when
        String result = testInstance.getDNSSig0KeyFilename();
        //then
        assertEquals(value, result);
    }

    @Test
    void getDNSTCPTimeout() {
        // given
        int value = 180;
        setMockStringValueForProperty(SMLPropertyEnum.DNS_TCP_TIMEOUT, String.valueOf(value));
        // when
        int result = testInstance.getDNSTCPTimeout();
        //then
        assertEquals(value, result);
    }

    @Test
    void getDNSSig0KeyName() {
        // given
        String value = setMockRandomStringValueForProperty(SMLPropertyEnum.DNS_SIG0_PKEY_NAME);
        // when
        String result = testInstance.getDNSSig0KeyName();
        //then
        assertEquals(value, result);
    }

    @Test
    void isDNSEnabled() {
        // given
        String value = setMockStringValueForProperty(SMLPropertyEnum.DNS_ENABLED, "true");
        // when
        Boolean result = testInstance.isDNSEnabled();
        //then
        assertEquals(Boolean.parseBoolean(value), result);
    }

    @Test
    void isShowDNSEntriesEnabled() {
        // given
        String value = setMockStringValueForProperty(SMLPropertyEnum.DNS_SHOW_ENTRIES, "true");
        // when
        Boolean result = testInstance.isShowDNSEntriesEnabled();
        //then
        assertEquals(Boolean.parseBoolean(value), result);
    }

    @Test
    void getDNSServer() {
        // given
        String value = setMockRandomStringValueForProperty(SMLPropertyEnum.DNS_SERVER);
        // when
        String result = testInstance.getDNSServer();
        //then
        assertEquals(value, result);
    }

    @Test
    void getDNSPublisherPrefix() {
        // given
        String value = setMockRandomStringValueForProperty(SMLPropertyEnum.DNS_PUBLISHER_PREFIX);
        // when
        String result = testInstance.getDNSPublisherPrefix();
        //then
        assertEquals(value, result);
    }

    @Test
    void getEncryptionFilename() {
        // given
        String value = setMockRandomStringValueForProperty(SMLPropertyEnum.ENCRYPTION_FILENAME);
        // when
        String result = testInstance.getEncryptionFilename();
        //then
        assertEquals(value, result);
    }

    @Test
    void isProxyEnabled() {
        // given
        String value = setMockStringValueForProperty(SMLPropertyEnum.PROXY_ENABLED, "true");
        // when
        Boolean result = testInstance.isProxyEnabled();
        //then
        assertEquals(Boolean.parseBoolean(value), result);
    }

    @Test
    void getHttpProxyHost() {
        // given
        String value = setMockRandomStringValueForProperty(SMLPropertyEnum.HTTP_PROXY_HOST);
        // when
        String result = testInstance.getHttpProxyHost();
        //then
        assertEquals(value, result);
    }

    @Test
    void getHttpNoProxyHosts() {
        // given
        String value = setMockRandomStringValueForProperty(SMLPropertyEnum.HTTP_NO_PROXY_HOSTS);
        // when
        String result = testInstance.getHttpNoProxyHosts();
        //then
        assertEquals(value, result);
    }

    @Test
    void getHttpProxyPort() {
        // given
        int value = 1234;
        setMockStringValueForProperty(SMLPropertyEnum.HTTP_PROXY_PORT, String.valueOf(value));
        // when
        int result = testInstance.getHttpProxyPort();
        //then
        assertEquals(value, result);
    }

    @Test
    void getHttpProxyUsername() {
        // given
        String value = setMockRandomStringValueForProperty(SMLPropertyEnum.HTTP_PROXY_USER);
        // when
        String result = testInstance.getHttpProxyUsername();
        //then
        assertEquals(value, result);
    }

    @Test
    void getHttpProxyPasswordNull() {
        // given
        String value = "";
        setMockStringValueForProperty(SMLPropertyEnum.HTTP_PROXY_PASSWORD, value);
        // when
        String result = testInstance.getHttpProxyPassword();
        //then
        assertNull(result);
    }

    @Test
    void getHttpProxyPassword() {
        // given
        String value = "test value to encrypt and decrypt";
        String encryptedValue = "ZOIm+LNGdPEPFEMDu1D9vFKjZr8IRJEyafw+E32k6npxS44cSJ78rZ6R2GH0nmta";
        setMockStringValueForProperty(SMLPropertyEnum.HTTP_PROXY_PASSWORD, encryptedValue);

        doReturn("encryptedPasswordKey.key").when(testInstance).getEncryptionFilename();
        doReturn(resourceDirectory.resolve("encryptedPasswordKey.key").toFile()).when(testInstance).getConfigurationFile(anyString());

        // when
        String result = testInstance.getHttpProxyPassword();
        //then
        assertNotNull(result);
        assertEquals(value, result);

    }

    @Test
    void isSignResponseEnabled() {
        // given
        String value = setMockStringValueForProperty(SMLPropertyEnum.SIGN_RESPONSE, "true");
        // when
        Boolean result = testInstance.isSignResponseEnabled();
        //then
        assertEquals(Boolean.parseBoolean(value), result);
    }

    @Test
    void getSignAlias() {
        // given
        String value = setMockRandomStringValueForProperty(SMLPropertyEnum.SIGNATURE_ALIAS);
        // when
        String result = testInstance.getSignAlias();
        //then
        assertEquals(value, result);
    }

    @Test
    void getKeystoreFilename() {
        // given
        String value = setMockRandomStringValueForProperty(SMLPropertyEnum.KEYSTORE_FILENAME);
        // when
        String result = testInstance.getKeystoreFilename();
        //then
        assertEquals(value, result);
    }


    @Test
    void getKeystorePassword() {
        // given
        String value = setMockRandomStringValueForProperty(SMLPropertyEnum.KEYSTORE_PASSWORD);
        // when
        String result = testInstance.getKeystorePassword();
        //then
        assertEquals(value, result);
    }

    @Test
    void getTruststoreFilename() {
        // given
        String value = setMockRandomStringValueForProperty(SMLPropertyEnum.TRUSTSTORE_FILENAME);
        // when
        String result = testInstance.getTruststoreFilename();
        //then
        assertEquals(value, result);
    }

    @Test
    void getTruststorePassword() {
        // given
        String value = setMockRandomStringValueForProperty(SMLPropertyEnum.TRUSTSTORE_PASSWORD);
        // when
        String result = testInstance.getTruststorePassword();
        //then
        assertEquals(value, result);
    }

    @Test
    void getListPageSize() {
        // given
        int value = 55548;
        setMockStringValueForProperty(SMLPropertyEnum.PAGE_SIZE, value + "");
        // when
        int result = testInstance.getListPageSize();
        //then
        assertEquals(value, result);
    }

    @Test
    void getSMPUpdateMaxParticipantCount() {
        // given
        int value = 55548;
        setMockStringValueForProperty(SMLPropertyEnum.SMP_CHANGE_MAX_PARTC_SIZE, String.valueOf(value));
        // when
        int result = testInstance.getSMPUpdateMaxParticipantCount();
        //then
        assertEquals(value, result);
    }


    @Test
    void isUnsecureLoginEnabled() {
        // given
        String value = setMockStringValueForProperty(SMLPropertyEnum.AUTH_UNSEC_LOGIN, "true");
        // when
        Boolean result = testInstance.isUnsecureLoginEnabled();
        //then
        assertEquals(Boolean.parseBoolean(value), result);
    }


    String setMockStringValueForProperty(SMLPropertyEnum property, String value) {
        // given
        doReturn(value).when(configurationDAO).getProperty(property);
        return value;
    }

    String setMockRandomStringValueForProperty(SMLPropertyEnum property) {
        return setMockStringValueForProperty(property, UUID.randomUUID().toString());
    }

    private ConfigurationBO createConfigurationBO(String property, String value) {
        ConfigurationBO configurationBO = new ConfigurationBO();
        configurationBO.setValue(value);
        configurationBO.setProperty(property);
        return configurationBO;
    }
}
