/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2017 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.impl;

import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;
import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.business.IDataQualityInconsistencyAnalyzerBusiness;
import eu.europa.ec.bdmsl.common.exception.GenericTechnicalException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.common.logging.ILoggingService;
import eu.europa.ec.bdmsl.common.reports.DataInconsistencyReport;
import eu.europa.ec.bdmsl.service.IMailSenderService;
import org.apache.commons.lang3.StringUtils;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.test.util.ReflectionTestUtils;

import java.net.InetAddress;
import java.net.ServerSocket;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.spy;
/**
 * @author Flavio SANTOS
 */
class DataQualityInconsistencyAnalyzerServiceImplTest extends AbstractJUnit5Test {


    private DataQualityInconsistencyAnalyzerServiceImpl testInstance = new DataQualityInconsistencyAnalyzerServiceImpl();

    private IDataQualityInconsistencyAnalyzerBusiness dataQualityInconsistencyAnalyzerBusiness;

    ILoggingService loggingService = Mockito.mock(ILoggingService.class);

    @Autowired
    private JavaMailSenderImpl mailSender;

    @Autowired
    private IMailSenderService mailSenderService;

    private GreenMail mockSmtp;

    @BeforeEach
    void setup() throws Exception {
        //Dummy SMTP Server
        ServerSocket serverSocket = new ServerSocket(0);
        int availablePort = serverSocket.getLocalPort();
        serverSocket.close();
        ServerSetup SMTP = new ServerSetup(availablePort, null, ServerSetup.PROTOCOL_SMTP);
        mockSmtp = new GreenMail(SMTP);
        mockSmtp.start();
        mailSender.setPort(availablePort);
        mailSender.setHost("localhost");

        mailSenderService = spy(mailSenderService);

        dataQualityInconsistencyAnalyzerBusiness = Mockito.mock(IDataQualityInconsistencyAnalyzerBusiness.class);


        ReflectionTestUtils.setField(testInstance, "mailSenderService", mailSenderService);
        ReflectionTestUtils.setField(testInstance, "configurationBusiness", configurationBusiness);
        ReflectionTestUtils.setField(testInstance, "dataQualityCheckerBusiness", dataQualityInconsistencyAnalyzerBusiness);
        ReflectionTestUtils.setField(testInstance, "loggingService", loggingService);


        Mockito.doReturn(null).when(configurationBusiness).getInconsistencyReportGenerateByInstance();

        testInstance = spy(testInstance);
    }

    @Test
    void testCheckDataInconsistenciesDnsOff() throws Exception {
        // given
        Mockito.doReturn(false).when(configurationBusiness).isDNSEnabled();

        // when
        testInstance.checkDataInconsistencies();
        //then
        Mockito.verify(testInstance, Mockito.never()).getServerAddress();
        Mockito.verify(testInstance, Mockito.never()).generateDataInconsistencyReport(anyString());
    }

    @Test
    void generateDataInconsistencyReportDnsOn() throws Exception {
        // given
        String mail = "mail@test.eu";
        // generation is executed because this is the  right server instance
        // given
        ArgumentCaptor<String> parameter1 = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> parameter2 = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> parameter3 = ArgumentCaptor.forClass(String.class);
        Mockito.doReturn(true).when(configurationBusiness).isDNSEnabled();
        Mockito.doNothing().when(testInstance).generateDataInconsistencyReport(anyString(), anyString(), anyString());
        // when
        testInstance.generateDataInconsistencyReport(mail);
        //then
        Mockito.verify(testInstance, Mockito.times(1)).generateDataInconsistencyReport(parameter1.capture(), parameter2.capture(), parameter3.capture());
        assertEquals(getLocalhost(), parameter1.getValue());
        assertEquals(configurationBusiness.getInconsistencyReportMailFrom(), parameter2.getValue());
        assertEquals(mail, parameter3.getValue());
    }

    @Test
    void generateDataInconsistencyReport() throws Exception {
        // given
        String toMail = "mail@test.eu";
        String fromMail = "from.mail@test.eu";
        String serverName = "serverName";
        DataInconsistencyReport report = new DataInconsistencyReport();
        report.addText("ERRPR", "Test text!");
        ArgumentCaptor<String> parameter1 = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> parameter2 = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> parameter3 = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> parameter4 = ArgumentCaptor.forClass(String.class);
        Mockito.doReturn(report).when(dataQualityInconsistencyAnalyzerBusiness).checkDataInconsistencies();
        Mockito.doNothing().when(mailSenderService).sendMessage(anyString(), anyString(), anyString(), anyString());

        // when
        testInstance.generateDataInconsistencyReport(serverName, fromMail, toMail);
        //then
        Mockito.verify(dataQualityInconsistencyAnalyzerBusiness, Mockito.times(1)).checkDataInconsistencies();
        Mockito.verify(mailSenderService, Mockito.times(1)).sendMessage(parameter1.capture(), parameter2.capture(),
                parameter3.capture(), parameter4.capture());

        assertEquals("SML Data Inconsistency Report for " + serverName, parameter1.getValue());
        assertEquals("Test text!", parameter2.getValue());
        assertEquals(fromMail, parameter3.getValue());
        assertEquals(toMail, parameter4.getValue());
    }

    @Test
    void generateDataInconsistencyReportFail1() throws Exception {
        // given
        String toMail = "mail@test.eu";
        String fromMail = "from.mail@test.eu";
        String serverName = "serverName";
        DataInconsistencyReport report = new DataInconsistencyReport();
        report.addText("ERRPR", "Test text!");
        TechnicalException te = new GenericTechnicalException("Server error");

        Mockito.doThrow(te).when(dataQualityInconsistencyAnalyzerBusiness).checkDataInconsistencies();
        Mockito.doNothing().when(mailSenderService).sendMessage(anyString(), anyString(), anyString(), anyString());

        ArgumentCaptor<String> parameter1 = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<TechnicalException> parameter2 = ArgumentCaptor.forClass(TechnicalException.class);

        // when
        testInstance.generateDataInconsistencyReport(serverName, fromMail, toMail);
        //then
        Mockito.verify(dataQualityInconsistencyAnalyzerBusiness, Mockito.times(1)).checkDataInconsistencies();
        Mockito.verify(mailSenderService, Mockito.never()).sendMessage(anyString(), anyString(), anyString(), anyString());
        Mockito.verify(loggingService, Mockito.times(1)).error(parameter1.capture(), parameter2.capture());

        assertEquals("Error occurred when generating DataInconsistencyReport", parameter1.getValue());
        assertEquals(te, parameter2.getValue());

    }

    @Test
    void generateDataInconsistencyReportFail2() throws Exception {
        // given
        String toMail = "mail@test.eu";
        String fromMail = "from.mail@test.eu";
        String serverName = "serverName";
        DataInconsistencyReport report = new DataInconsistencyReport();
        report.addText("ERRPR", "Test text!");
        TechnicalException te = new GenericTechnicalException("Mail error");

        Mockito.doReturn(report).when(dataQualityInconsistencyAnalyzerBusiness).checkDataInconsistencies();
        Mockito.doThrow(te).when(mailSenderService).sendMessage(anyString(), anyString(), anyString(), anyString());

        ArgumentCaptor<String> parameter1 = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<TechnicalException> parameter2 = ArgumentCaptor.forClass(TechnicalException.class);

        // when
        testInstance.generateDataInconsistencyReport(serverName, fromMail, toMail);
        //then
        Mockito.verify(dataQualityInconsistencyAnalyzerBusiness, Mockito.times(1)).checkDataInconsistencies();
        Mockito.verify(mailSenderService, Mockito.times(1)).sendMessage(anyString(), anyString(), anyString(), anyString());
        Mockito.verify(loggingService, Mockito.times(1)).error(parameter1.capture(), parameter2.capture());

        assertEquals("[ERR-105] Mail error", parameter1.getValue());
        assertEquals(te, parameter2.getValue());

    }

    @Test
    void testGenerateDataInconsistencyReportDnsOff() throws Exception {
        String mail = "mail@test.eu";
        Mockito.doReturn(false).when(configurationBusiness).isDNSEnabled();
        // when
        testInstance.generateDataInconsistencyReport(mail);
        //then
        Mockito.verify(testInstance, Mockito.never()).generateDataInconsistencyReport(anyString(), anyString(), anyString());
    }

    @Test
    void testCheckDataInconsistenciesDnsOnNotCall() throws Exception {
        // generation is not executed because this is not right server instance
        // given
        Mockito.doReturn(true).when(configurationBusiness).isDNSEnabled();
        Mockito.doReturn("NotThisServer").when(configurationBusiness).getInconsistencyReportGenerateByInstance();
        // when
        testInstance.checkDataInconsistencies();
        //then
        Mockito.verify(testInstance, Mockito.atMostOnce()).getServerAddress();
        Mockito.verify(testInstance, Mockito.never()).generateDataInconsistencyReport(anyString(), anyString(), anyString());
    }

    @Test
    void testGetServerAddress() throws Exception {
        //given
        String serverAddress = getLocalhost();
        // when then
        assertEquals(serverAddress, testInstance.getServerAddress());
    }

    @Test
    void testCheckDataInconsistenciesDnsOnCall() throws Exception {
        // generation is executed because this is the  right server instance
        // given
        String serverAddress = getLocalhost();
        //when-then
        validateCheckDataInconsistenciesWithServer(serverAddress);
    }

    @Test
    void testCheckDataInconsistenciesDnsOnCallWithEmpty() throws Exception {
        // generation is executed because this is the  right server instance
        // given
        String serverAddress = "";
        //when-then
        validateCheckDataInconsistenciesWithServer(serverAddress);
    }

    @Test
    void testCheckDataInconsistenciesDnsOnCallWithNull() throws Exception {
        // generation is executed because this is the  right server instance
        // given
        String serverAddress = null;
        //when-then
        validateCheckDataInconsistenciesWithServer(serverAddress);
    }

    void validateCheckDataInconsistenciesWithServer(String serverAddress) throws Exception {
        // generation is executed because this is the  right server instance
        // given
        ArgumentCaptor<String> parameter1 = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> parameter2 = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> parameter3 = ArgumentCaptor.forClass(String.class);

        Mockito.doReturn(true).when(configurationBusiness).isDNSEnabled();
        Mockito.doReturn(serverAddress).when(configurationBusiness).getInconsistencyReportGenerateByInstance();
        Mockito.doNothing().when(testInstance).generateDataInconsistencyReport(anyString(), anyString(), anyString());
        // when
        testInstance.checkDataInconsistencies();
        //then
        Mockito.verify(testInstance, Mockito.times(1)).getServerAddress();
        Mockito.verify(testInstance, Mockito.times(1)).generateDataInconsistencyReport(parameter1.capture(), parameter2.capture(), parameter3.capture());
        assertEquals(getLocalhost(), parameter1.getValue());
        assertEquals(configurationBusiness.getInconsistencyReportMailFrom(), parameter2.getValue());
        assertEquals(configurationBusiness.getInconsistencyReportMailTo(), parameter3.getValue());

    }

    @AfterEach
    void cleanup() throws Exception {
        mockSmtp.stop();
    }

    String getLocalhost() {
        String serverAddress;
        try {
            serverAddress = InetAddress.getLocalHost().getHostName();
        } catch (Exception e) {
            loggingService.error(e.getMessage(), e);
            serverAddress = StringUtils.EMPTY;
        }
        return serverAddress;
    }
}
