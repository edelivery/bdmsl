/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.impl;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.bo.CertificateBO;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.dao.ICertificateDAO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Flavio SANTOS
 */
class CertificateDAOImplTest extends AbstractJUnit5Test {

    @Autowired
    private ICertificateDAO certificateDAO;

    @Test
    void testFindCertificateByCertificateId() throws TechnicalException {
        //GIVEN - WHEN
        String certificateId = "CN=SMP_TEST_CHANGE_CERTIFICATE,O=DG-DIGIT,C=BE:0000000000000000000000000123abcd";
        CertificateBO certificateBO = certificateDAO.findCertificateByCertificateId(certificateId);

        //THEN
        assertNotNull(certificateBO);
        assertEquals(certificateId, certificateBO.getCertificateId());
    }

    @Test
    @Transactional
    void testCreateCertificate() throws TechnicalException {
        //GIVEN
        String certificateId = "CN=test:newCertificateAdded";
        CertificateBO certificateBO = new CertificateBO();
        certificateBO.setCertificateId(certificateId);
        certificateBO.setValidFrom(Calendar.getInstance());
        certificateBO.setValidTo(Calendar.getInstance());
        certificateBO.setMigrationDate(Calendar.getInstance());

        //WHEN
        Long createdId = certificateDAO.createCertificate(certificateBO);

        //THEN
        assertNotNull(createdId);
        CertificateBO result = certificateDAO.findCertificateByCertificateId(certificateId);
        assertNotNull(result);
        assertEquals("CN=test:newCertificateAdded", result.getCertificateId());
    }

    @Test
    @Transactional
    void testUpdateCertificate() throws TechnicalException {
        //GIVEN
        CertificateBO certificateBO = certificateDAO.findCertificateByCertificateId("CN=test:Update");
        Calendar currentMigrationDate = certificateBO.getMigrationDate();
        Calendar newMigrationDate = Calendar.getInstance();
        newMigrationDate.set(1984, 9, 3);
        certificateBO.setMigrationDate(newMigrationDate);

        //WHEN
        certificateDAO.updateCertificate(certificateBO);

        //THEN
        certificateBO = certificateDAO.findCertificateByCertificateId("CN=test:Update");
        assertNotEquals(currentMigrationDate, certificateBO.getMigrationDate());
        assertNull(currentMigrationDate);
    }

    @Test
    @Transactional
    void testUpdateNonPKICertificate() throws TechnicalException {
        //GIVEN
        CertificateBO certificateBO = certificateDAO.findCertificateByCertificateId("CN=EHEALTH_SMP_1000000032,O=DG-DIGIT,C=BE:6caa15ffed56ad049dd2b9bc835d2e46");
        assertNotNull(certificateBO);
        Calendar currentMigrationDate = certificateBO.getMigrationDate();
        Calendar newMigrationDate = Calendar.getInstance();
        newMigrationDate.set(1984, 9, 3);
        certificateBO.setMigrationDate(newMigrationDate);

        //WHEN
        certificateDAO.updateCertificate(certificateBO);

        //THEN
        certificateBO = certificateDAO.findCertificateByCertificateId("CN=EHEALTH_SMP_1000000032,O=DG-DIGIT,C=BE:6caa15ffed56ad049dd2b9bc835d2e46");
        assertNotEquals(currentMigrationDate, certificateBO.getMigrationDate());
        assertNull(currentMigrationDate);
    }

    @Test
    @Transactional
    void testDelete() throws TechnicalException {
        //GIVEN
        String certificateId = "CN=test:Delete";
        CertificateBO certificateBO = certificateDAO.findCertificateByCertificateId(certificateId);

        //WHEN
        certificateDAO.delete(certificateBO);

        //THEN
        certificateBO = certificateDAO.findCertificateByCertificateId(certificateId);
        assertNull(certificateBO);
    }

    @Test
    void findCertificateById() throws TechnicalException {
        //GIVEN - WHEN
        CertificateBO certificateBO = certificateDAO.findCertificateById(new Long("9"));

        //THEN
        assertNotNull(certificateBO);
        assertEquals("CN=test:Update", certificateBO.getCertificateId());
    }
}

