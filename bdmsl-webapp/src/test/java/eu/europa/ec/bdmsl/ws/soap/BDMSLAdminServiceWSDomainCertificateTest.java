/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.ws.soap;

import ec.services.wsdl.bdmsl.admin.data._1.*;
import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.bo.CertificateDomainBO;
import eu.europa.ec.bdmsl.common.enums.SMLRoleEnum;
import eu.europa.ec.bdmsl.common.exception.Messages;
import eu.europa.ec.bdmsl.dao.impl.CertificateDomainDAOImpl;
import eu.europa.ec.bdmsl.test.CommonTestUtils;
import eu.europa.ec.bdmsl.security.CertificateAuthentication;
import eu.europa.ec.edelivery.security.PreAuthenticatedCertificatePrincipal;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.security.Security;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.CoreMatchers.*;

class BDMSLAdminServiceWSDomainCertificateTest extends AbstractJUnit5Test {

    static {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    @Autowired
    private IBDMSLAdminServiceWS bdmslAdminServiceWS;

    @Autowired
    private CertificateDomainDAOImpl certificateDomainDAO;

    private PreAuthenticatedCertificatePrincipal principal;

    private CertificateDomainBO grantedSubDomainAnchor;

    @BeforeEach
    void before() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        grantedSubDomainAnchor = certificateDomainDAO.findDomain("CN=DIGIT_SMP_TECH_TEAM_3,O=DIGIT,OU=FOR TEST ONLY,C=BE", false);

        String certHeaderValue = "serial=48:b6:81:ee:8e:0d:cc:08&subject=EMAILADDRESS=receiver@test.be,C=BE, O=DIGIT, OU=FOR TEST ONLY,CN=DIGIT_SMP_TECH_TEAM_3&validfrom=Feb  1 14:20:18 2017 GMT&validto=Jul  9 23:59:00 2019 GMT&issuer=C=BE, O=DIGIT, OU=FOR TEST ONLY, CN=DIGIT_SMP_TECH_TEAM_2";
        principal = blueCoatAuthenticationFilter.buildPrincipalFromHeader(certHeaderValue);
        Authentication authentication = new CertificateAuthentication(grantedSubDomainAnchor, principal, Collections.singletonList(SMLRoleEnum.ROLE_ADMIN.getAuthority()));
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Test
    void testAddSubDomainCertificateNullRequest() {
        BadRequestFault badRequestFault = assertThrows(BadRequestFault.class, () -> bdmslAdminServiceWS.addSubDomainCertificate(null));
        assertThat(badRequestFault.getFaultInfo().getFaultMessage(), containsString("[ERR-106] The input values must not be null!"));
    }

    @Test
    void testUpdateSubDomainCertificateNullRequest() {
        BadRequestFault badRequestFault = assertThrows(BadRequestFault.class, () -> bdmslAdminServiceWS.updateSubDomainCertificate(null));
        assertThat(badRequestFault.getFaultInfo().getFaultMessage(), containsString("[ERR-106] The input values must not be null!"));
    }

    @Test
    void testListSubDomainCertificateNullRequest() {
        BadRequestFault badRequestFault = assertThrows(BadRequestFault.class, () -> bdmslAdminServiceWS.listSubDomainCertificate(null));
        assertThat(badRequestFault.getFaultInfo().getFaultMessage(), containsString("[ERR-106] The input values must not be null!"));
    }

    @Test
    void testAddSubDomainCertificatePositiveCase() throws Exception {
        String serial = "0123456789101112";
        String issuer = "CN=SMP_issuerCN,O=DIGIT,C=BE";
        String subject = "CN=SMP_receiverCN,O=DIGIT,C=BE";

        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        AddSubDomainCertificateRequest request = createAddDomainCertificate(createSubDomain.getSubDomainName(), subject, issuer, serial);

        DomainCertificateType domainCertificate = bdmslAdminServiceWS.addSubDomainCertificate(request);

        assertNotNull(domainCertificate);
        assertEquals(request.isIsAdminCertificate(), domainCertificate.isIsAdminCertificate());
        assertEquals(request.isIsRootCertificate(), domainCertificate.isIsRootCertificate());
        assertNull(domainCertificate.getCRLDistributionPointsUrl());
        assertEquals(subject, domainCertificate.getCertificateDomainId());
    }

    @Test
    void testAddSubDomainCertificatePositiveCaseWithColonInCN() throws Exception {
        String serial = "0123456789101113";
        String issuer = "CN=SMP_issuerCN,O=DIGIT,C=BE";
        String subject = "CN=GPR:SMP_receiverCN,O=DIGIT,C=BE";

        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        AddSubDomainCertificateRequest request = createAddDomainCertificate(createSubDomain.getSubDomainName(), subject, issuer, serial);

        DomainCertificateType domainCertificate = bdmslAdminServiceWS.addSubDomainCertificate(request);

        assertNotNull(domainCertificate);
        assertEquals(request.isIsAdminCertificate(), domainCertificate.isIsAdminCertificate());
        assertEquals(request.isIsRootCertificate(), domainCertificate.isIsRootCertificate());
        assertNull(domainCertificate.getCRLDistributionPointsUrl());
        assertEquals(subject, domainCertificate.getCertificateDomainId());
    }

    @Test
    void testAddSubDomainCertificatePositiveCaseHttpCrl() throws Exception {
        String serial = "0123456789101112";
        String issuer = "CN=SMP_issuerCN,O=DIGIT,C=BE";
        String subject = "CN=SMP_receiverCN,O=DIGIT,C=BE";
        String crl = "http://localhost/crl";

        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        AddSubDomainCertificateRequest request = createAddDomainCertificate(createSubDomain.getSubDomainName(),
                subject, issuer, serial, Collections.singletonList(crl));

        DomainCertificateType domainCertificate = bdmslAdminServiceWS.addSubDomainCertificate(request);

        assertNotNull(domainCertificate);
        assertEquals(request.isIsAdminCertificate(), domainCertificate.isIsAdminCertificate());
        assertEquals(request.isIsRootCertificate(), domainCertificate.isIsRootCertificate());
        assertEquals(crl, domainCertificate.getCRLDistributionPointsUrl());
        assertEquals(subject, domainCertificate.getCertificateDomainId());
    }

    @Test
    void testAddSubDomainCertificatePositiveCaseHttpsCrl() throws Exception {
        String serial = "0123456789101112";
        String issuer = "CN=SMP_issuerCN,O=DIGIT,C=BE";
        String subject = "CN=SMP_receiverCN,O=DIGIT,C=BE";
        String crl = "https://localhost/crl-https";

        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        AddSubDomainCertificateRequest request = createAddDomainCertificate(createSubDomain.getSubDomainName(),
                subject, issuer, serial, Collections.singletonList(crl));

        DomainCertificateType domainCertificate = bdmslAdminServiceWS.addSubDomainCertificate(request);

        assertNotNull(domainCertificate);
        assertEquals(request.isIsAdminCertificate(), domainCertificate.isIsAdminCertificate());
        assertEquals(request.isIsRootCertificate(), domainCertificate.isIsRootCertificate());
        assertEquals(crl, domainCertificate.getCRLDistributionPointsUrl());
        assertEquals(subject, domainCertificate.getCertificateDomainId());
    }

    @Test
    void testAddSubDomainCertificatePositiveCaseIgnoreLdapCrl() throws Exception {
        String serial = "0123456789101112";
        String issuer = "CN=SMP_issuerCN,O=DIGIT,C=BE";
        String subject = "CN=SMP_receiverCN,O=DIGIT,C=BE";
        String crl = "ldap://localhost/crl-ldap";

        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        AddSubDomainCertificateRequest request = createAddDomainCertificate(createSubDomain.getSubDomainName(),
                subject, issuer, serial, Collections.singletonList(crl));

        DomainCertificateType domainCertificate = bdmslAdminServiceWS.addSubDomainCertificate(request);

        assertNotNull(domainCertificate);
        assertEquals(request.isIsAdminCertificate(), domainCertificate.isIsAdminCertificate());
        assertEquals(request.isIsRootCertificate(), domainCertificate.isIsRootCertificate());
        assertNull(domainCertificate.getCRLDistributionPointsUrl());
        assertEquals(subject, domainCertificate.getCertificateDomainId());
    }

    @Test
    void testAddSubDomainCertificatePositiveCaseIgnoreAllChoseHttps() throws Exception {
        String serial = "0123456789101112";
        String issuer = "CN=SMP_issuerCN,O=DIGIT,C=BE";
        String subject = "CN=SMP_receiverCN,O=DIGIT,C=BE";
        String crlHttps = "https://localhost/crl-https";
        List<String> crls = Arrays.asList("ldap://localhost/crl-ldap", crlHttps, "http://localhost/crl");

        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        AddSubDomainCertificateRequest request = createAddDomainCertificate(createSubDomain.getSubDomainName(),
                subject, issuer, serial, crls);

        DomainCertificateType domainCertificate = bdmslAdminServiceWS.addSubDomainCertificate(request);

        assertNotNull(domainCertificate);
        assertEquals(request.isIsAdminCertificate(), domainCertificate.isIsAdminCertificate());
        assertEquals(request.isIsRootCertificate(), domainCertificate.isIsRootCertificate());
        assertEquals(crlHttps, domainCertificate.getCRLDistributionPointsUrl());
        assertEquals(subject, domainCertificate.getCertificateDomainId());
    }

    @Test
    void testAddSubDomainCertificateEmptyDomainName() throws Exception {

        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        AddSubDomainCertificateRequest request = createAddDomainCertificate(createSubDomain.getSubDomainName());
        request.setSubDomainName("");
        assertThrows(BadRequestFault.class, () -> bdmslAdminServiceWS.addSubDomainCertificate(request));
    }

    @Test
    void testAddSubDomainCertificateEmptyPublicKey() throws Exception {


        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        AddSubDomainCertificateRequest request = createAddDomainCertificate(createSubDomain.getSubDomainName());
        request.setCertificatePublicKey(null);
        BadRequestFault badRequestFault = assertThrows(BadRequestFault.class, () -> bdmslAdminServiceWS.addSubDomainCertificate(request));
        assertThat(badRequestFault.getFaultInfo().getFaultMessage(), containsString(Messages.BAD_REQUEST_PARAMETER_EMPTY_PUBLIC_KEY));
    }

    @Test
    void testAddSubDomainCertificateInvalidCombinationRootAndAdmin() throws Exception {


        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        AddSubDomainCertificateRequest request = createAddDomainCertificate(createSubDomain.getSubDomainName());
        request.setIsRootCertificate(true);
        request.setIsAdminCertificate(true);
        BadRequestFault badRequestFault = assertThrows(BadRequestFault.class, () -> bdmslAdminServiceWS.addSubDomainCertificate(request));
        assertThat(badRequestFault.getFaultInfo().getFaultMessage(), containsString(Messages.BAD_REQUEST_PARAMETER_INCONSISTEN_DOMAIN_CERT));
    }

    @Test
    void testUpdateSubDomainCertificatePositiveCaseCLR() throws Exception {
        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);
        AddSubDomainCertificateRequest request = createAddDomainCertificate(createSubDomain.getSubDomainName());
        DomainCertificateType domainCertificate = bdmslAdminServiceWS.addSubDomainCertificate(request);
        assertNotNull(domainCertificate);
        assertNull(domainCertificate.getCRLDistributionPointsUrl());
        String crlHttps = "https://localhost/clr";

        UpdateSubDomainCertificateRequest updateRequest = new UpdateSubDomainCertificateRequest();
        updateRequest.setCertificateDomainId(domainCertificate.getCertificateDomainId());
        updateRequest.setCrlDistributionPoint(crlHttps);
        updateRequest.setIsAdminCertificate(!request.isIsAdminCertificate());

        DomainCertificateType updatedDomainCertificate = bdmslAdminServiceWS.updateSubDomainCertificate(updateRequest);

        assertEquals(!request.isIsAdminCertificate(), updatedDomainCertificate.isIsAdminCertificate());
        assertEquals(request.isIsRootCertificate(), updatedDomainCertificate.isIsRootCertificate());
        assertEquals(crlHttps, updatedDomainCertificate.getCRLDistributionPointsUrl());
    }

    @Test
    void testUpdateSubDomainCertificatePositiveCaseSubDomain() throws Exception {
        SubDomainType createSubDomain1 = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        SubDomainType createSubDomain2 = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        createSubDomain2.setSubDomainName("new-domain." + createSubDomain2.getDNSZone());
        bdmslAdminServiceWS.createSubDomain(createSubDomain1);
        bdmslAdminServiceWS.createSubDomain(createSubDomain2);


        AddSubDomainCertificateRequest request = createAddDomainCertificate(createSubDomain1.getSubDomainName());
        DomainCertificateType domainCertificate = bdmslAdminServiceWS.addSubDomainCertificate(request);
        assertNotNull(domainCertificate);
        assertNull(domainCertificate.getCRLDistributionPointsUrl());
        String crlHttps = "https://localhost/clr";

        UpdateSubDomainCertificateRequest updateRequest = new UpdateSubDomainCertificateRequest();
        updateRequest.setCertificateDomainId(domainCertificate.getCertificateDomainId());
        updateRequest.setSubDomainName(createSubDomain2.getSubDomainName());

        DomainCertificateType updatedDomainCertificate = bdmslAdminServiceWS.updateSubDomainCertificate(updateRequest);

        assertEquals(createSubDomain2.getSubDomainName(), updatedDomainCertificate.getSubDomainName());
    }

    @Test
    void testUpdateSubDomainCertificateEmptyRequest() {
        BadRequestFault badRequestFault = assertThrows(BadRequestFault.class, () -> bdmslAdminServiceWS.updateSubDomainCertificate(new UpdateSubDomainCertificateRequest()));
        assertThat(badRequestFault.getFaultInfo().getFaultMessage(), containsString(Messages.BAD_REQUEST_PARAMETER_EMPTY_CERTIFICATE));
    }

    @Test
    void testUpdateSubDomainCertificateSetToNullCrl() throws Exception {
        String serial = "0123456789101112";
        String issuer = "CN=SMP_issuerCN,O=DIGIT,C=BE";
        String subject = "CN=SMP_receiverCN,O=DIGIT,C=BE";
        String crlHttps = "https://localhost/crl-https";
        List<String> crls = Arrays.asList("ldap://localhost/crl-ldap", crlHttps, "http://localhost/crl");

        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        AddSubDomainCertificateRequest request = createAddDomainCertificate(createSubDomain.getSubDomainName(),
                subject, issuer, serial, crls);
        DomainCertificateType domainCertificate = bdmslAdminServiceWS.addSubDomainCertificate(request);
        assertNotNull(domainCertificate.getCRLDistributionPointsUrl());


        UpdateSubDomainCertificateRequest updateRequest = new UpdateSubDomainCertificateRequest();
        updateRequest.setCertificateDomainId(domainCertificate.getCertificateDomainId());
        updateRequest.setCrlDistributionPoint("");

        DomainCertificateType updatedDomainCertificate = bdmslAdminServiceWS.updateSubDomainCertificate(updateRequest);

        assertNull(updatedDomainCertificate.getCRLDistributionPointsUrl());

    }

    @Test
    void testUpdateSubDomainCertificateInvalidRootAndAdmin() throws Exception {
        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);
        AddSubDomainCertificateRequest request = createAddDomainCertificate(createSubDomain.getSubDomainName());
        request.setIsRootCertificate(true);
        request.setIsAdminCertificate(false);
        DomainCertificateType domainCertificate = bdmslAdminServiceWS.addSubDomainCertificate(request);

        UpdateSubDomainCertificateRequest updateRequest = new UpdateSubDomainCertificateRequest();

        updateRequest.setCertificateDomainId(domainCertificate.getCertificateDomainId());
        updateRequest.setIsAdminCertificate(true);

        BadRequestFault badRequestFault = assertThrows(BadRequestFault.class, () -> bdmslAdminServiceWS.updateSubDomainCertificate(updateRequest));
        assertThat(badRequestFault.getFaultInfo().getFaultMessage(), containsString(Messages.BAD_REQUEST_PARAMETER_INCONSISTEN_DOMAIN_CERT));
    }

    @Test
    void testUpdateSubDomainCertificateEmptyCertificate() throws Exception {
        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);
        AddSubDomainCertificateRequest request = createAddDomainCertificate(createSubDomain.getSubDomainName());
        bdmslAdminServiceWS.addSubDomainCertificate(request);

        UpdateSubDomainCertificateRequest updateRequest = new UpdateSubDomainCertificateRequest();

        updateRequest.setIsAdminCertificate(true);

        BadRequestFault badRequestFault = assertThrows(BadRequestFault.class, () -> bdmslAdminServiceWS.updateSubDomainCertificate(updateRequest));
        MatcherAssert.assertThat(badRequestFault.getMessage(), CoreMatchers.containsStringIgnoringCase(Messages.BAD_REQUEST_PARAMETER_EMPTY_CERTIFICATE));
    }


    @Test
    void testListSubDomainCertificatePositiveCase() throws Exception {

        ListSubDomainCertificateRequest requestType = new ListSubDomainCertificateRequest();

        ListSubDomainCertificateResponse response = bdmslAdminServiceWS.listSubDomainCertificate(requestType);

        assertNotNull(response);
        assertFalse(response.getDomainCertificateTypes().isEmpty());
    }


    @Test
    void testListSubDomainCertificatePositiveCaseSubDomain() throws Exception {
        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);
        AddSubDomainCertificateRequest subdomainCertificateRequest = createAddDomainCertificate(createSubDomain.getSubDomainName());
        DomainCertificateType domainCertificate = bdmslAdminServiceWS.addSubDomainCertificate(subdomainCertificateRequest);


        ListSubDomainCertificateRequest requestType = new ListSubDomainCertificateRequest();
        requestType.setSubDomainName(createSubDomain.getSubDomainName());
        ListSubDomainCertificateResponse response = bdmslAdminServiceWS.listSubDomainCertificate(requestType);

        assertNotNull(response);
        assertEquals(1, response.getDomainCertificateTypes().size());
        assertEquals(domainCertificate.getCertificateDomainId(), response.getDomainCertificateTypes().get(0).getCertificateDomainId());
        assertEquals(domainCertificate.getCRLDistributionPointsUrl(), response.getDomainCertificateTypes().get(0).getCRLDistributionPointsUrl());
        assertEquals(domainCertificate.isIsAdminCertificate(), response.getDomainCertificateTypes().get(0).isIsAdminCertificate());
        assertEquals(domainCertificate.isIsAdminCertificate(), response.getDomainCertificateTypes().get(0).isIsRootCertificate());
        assertEquals(domainCertificate.getSubDomainName(), response.getDomainCertificateTypes().get(0).getSubDomainName());
    }

    @Test
    void testListSubDomainCertificatePositiveCaseNoRecords() throws Exception {
        ListSubDomainCertificateRequest requestType = new ListSubDomainCertificateRequest();
        requestType.setCertificateDomainId("NOCertificateWithThisId-12558555788515");
        ListSubDomainCertificateResponse response = bdmslAdminServiceWS.listSubDomainCertificate(requestType);

        assertNotNull(response);
        assertTrue(response.getDomainCertificateTypes().isEmpty());
    }

    @Test
    void testListSubDomainCertificateInvalidSubDomain() {
        ListSubDomainCertificateRequest requestType = new ListSubDomainCertificateRequest();
        requestType.setSubDomainName("NOSubDomainWithThisName-12558555788515");

        assertThrows(NotFoundFault.class, () -> bdmslAdminServiceWS.listSubDomainCertificate(requestType),"[ERR-118] SubDomain does not exist: " + requestType.getSubDomainName().toLowerCase());
    }

    @Test
    void testListSubDomainCertificatePositiveCaseCertificateId() throws Exception {
        String serial = "0123456789101112";
        String issuer = "CN=SMP_issuerCN,O=DIGIT,C=BE";
        String subject1 = "CN=SMP_receiverCN FIRST,O=DIGIT,C=BE";
        String subject2 = "CN=SMP_receiverCN SECOND,O=DIGIT,C=BE";

        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);
        AddSubDomainCertificateRequest subdomainCertificateRequest1 = createAddDomainCertificate(createSubDomain.getSubDomainName(), subject1, issuer, serial);
        AddSubDomainCertificateRequest subdomainCertificateRequest2 = createAddDomainCertificate(createSubDomain.getSubDomainName(), subject2, issuer, serial);
        DomainCertificateType domainCertificate1 = bdmslAdminServiceWS.addSubDomainCertificate(subdomainCertificateRequest1);
        DomainCertificateType domainCertificate2 = bdmslAdminServiceWS.addSubDomainCertificate(subdomainCertificateRequest2);

        ListSubDomainCertificateRequest requestType = new ListSubDomainCertificateRequest();
        requestType.setCertificateDomainId("SMP_receiverCN");
        ListSubDomainCertificateResponse response = bdmslAdminServiceWS.listSubDomainCertificate(requestType);
        // test both
        assertNotNull(response);
        assertEquals(2, response.getDomainCertificateTypes().size());
        // test first
        requestType.setCertificateDomainId("SMP_receiverCN FIRST");
        response = bdmslAdminServiceWS.listSubDomainCertificate(requestType);
        assertEquals(1, response.getDomainCertificateTypes().size());

        assertEquals(domainCertificate1.getCertificateDomainId(), response.getDomainCertificateTypes().get(0).getCertificateDomainId());
        assertEquals(domainCertificate1.getCRLDistributionPointsUrl(), response.getDomainCertificateTypes().get(0).getCRLDistributionPointsUrl());
        assertEquals(domainCertificate1.isIsAdminCertificate(), response.getDomainCertificateTypes().get(0).isIsAdminCertificate());
        assertEquals(domainCertificate1.isIsAdminCertificate(), response.getDomainCertificateTypes().get(0).isIsRootCertificate());
        assertEquals(domainCertificate1.getSubDomainName(), response.getDomainCertificateTypes().get(0).getSubDomainName());

        // test first
        requestType.setCertificateDomainId("SECOND");
        response = bdmslAdminServiceWS.listSubDomainCertificate(requestType);
        assertEquals(1, response.getDomainCertificateTypes().size());

        assertEquals(domainCertificate2.getCertificateDomainId(), response.getDomainCertificateTypes().get(0).getCertificateDomainId());
        assertEquals(domainCertificate2.getCRLDistributionPointsUrl(), response.getDomainCertificateTypes().get(0).getCRLDistributionPointsUrl());
        assertEquals(domainCertificate2.isIsAdminCertificate(), response.getDomainCertificateTypes().get(0).isIsAdminCertificate());
        assertEquals(domainCertificate2.isIsAdminCertificate(), response.getDomainCertificateTypes().get(0).isIsRootCertificate());
        assertEquals(domainCertificate2.getSubDomainName(), response.getDomainCertificateTypes().get(0).getSubDomainName());
    }

    @Test
    void testDeleteSubDomainCertificateNullRequest() {
        BadRequestFault badRequestFault = assertThrows(BadRequestFault.class, () -> bdmslAdminServiceWS.deleteSubDomainCertificate(null));
        assertEquals("[ERR-106] " + Messages.BAD_REQUEST_NULL, badRequestFault.getFaultInfo().getFaultMessage());
    }

    @Test
    void testDeleteSubDomainCertificateEmptyRequest() {
        BadRequestFault badRequestFault = assertThrows(BadRequestFault.class, () -> bdmslAdminServiceWS.deleteSubDomainCertificate(new DeleteSubDomainCertificateRequest()));
        assertEquals("[ERR-106] " + Messages.BAD_REQUEST_PARAMETER_EMPTY_CERTIFICATE, badRequestFault.getFaultInfo().getFaultMessage());
    }

    @Test
    void testDeleteSubDomainCertificateInvalidSubdomain() throws Exception {
        SubDomainType subdomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(subdomain);
        AddSubDomainCertificateRequest addRequest = createAddDomainCertificate(subdomain.getSubDomainName());
        DomainCertificateType addResult = bdmslAdminServiceWS.addSubDomainCertificate(addRequest);

        DeleteSubDomainCertificateRequest deleteRequest = new DeleteSubDomainCertificateRequest();
        deleteRequest.setCertificateDomainId(addResult.getCertificateDomainId());
        deleteRequest.setSubDomainName("INVALID." + subdomain.getSubDomainName());

        NotFoundFault notFoundFault = assertThrows(NotFoundFault.class, () -> bdmslAdminServiceWS.deleteSubDomainCertificate(deleteRequest));
        assertThat( deleteRequest.getSubDomainName().toLowerCase(), notFoundFault.getFaultInfo().getFaultMessage(), containsString("[ERR-118] SubDomain does not exist: "));
    }

    @Test
    void testDeleteSubDomainCertificateMissingCertificate() throws Exception {
        SubDomainType subdomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(subdomain);
        AddSubDomainCertificateRequest addRequest = createAddDomainCertificate(subdomain.getSubDomainName());
        DomainCertificateType addResult = bdmslAdminServiceWS.addSubDomainCertificate(addRequest);

        DeleteSubDomainCertificateRequest deleteRequest = new DeleteSubDomainCertificateRequest();
        deleteRequest.setCertificateDomainId(addResult.getCertificateDomainId() + "-INVALID");
        deleteRequest.setSubDomainName(subdomain.getSubDomainName());

        BadRequestFault badRequestFault = assertThrows(BadRequestFault.class, () -> bdmslAdminServiceWS.deleteSubDomainCertificate(deleteRequest));
        assertThat(badRequestFault.getFaultInfo().getFaultMessage(), containsString("[ERR-106] Domain certificate [CN=SMP_receiverCN,O=DIGIT,C=BE-INVALID] does not exist!"));
    }

    @Test
    void testDeleteSubDomainCertificatePositiveCase() throws Exception {
        SubDomainType subdomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(subdomain);
        AddSubDomainCertificateRequest addRequest = createAddDomainCertificate(subdomain.getSubDomainName());
        DomainCertificateType addResult = bdmslAdminServiceWS.addSubDomainCertificate(addRequest);

        DeleteSubDomainCertificateRequest deleteRequest = new DeleteSubDomainCertificateRequest();
        deleteRequest.setCertificateDomainId(addResult.getCertificateDomainId());
        deleteRequest.setSubDomainName(subdomain.getSubDomainName());

        DomainCertificateType domainCertificateType = bdmslAdminServiceWS.deleteSubDomainCertificate(deleteRequest);
        assertThat(domainCertificateType.getCertificateDomainId(), containsString("CN=SMP_receiverCN,O=DIGIT,C=BE"));
    }

    @Test
    void testDeleteSubDomainCertificateCannotDeleteCurrentAuthenticationCertificate() {
        DeleteSubDomainCertificateRequest deleteRequest = new DeleteSubDomainCertificateRequest();
        deleteRequest.setCertificateDomainId(principal.getSubjectShortDN());

        BadRequestFault badRequestFault = assertThrows(BadRequestFault.class, () -> bdmslAdminServiceWS.deleteSubDomainCertificate(deleteRequest));
        assertThat(badRequestFault.getFaultInfo().getFaultMessage(), containsString("[ERR-106] Invalid domain certificate id! Certificate is currently being used so cannot be deleted!"));
    }

    private static AddSubDomainCertificateRequest createAddDomainCertificate(String domainName) throws Exception {
        String serial = "0123456789101112";
        String issuer = "CN=SMP_issuerCN,O=DIGIT,C=BE";
        String subject = "CN=SMP_receiverCN,O=DIGIT,C=BE";
        return createAddDomainCertificate(domainName, subject, issuer, serial, Collections.emptyList());
    }

    private static AddSubDomainCertificateRequest createAddDomainCertificate(String domainName, String subject, String issuer, String serial) throws Exception {
        return createAddDomainCertificate(domainName, subject, issuer, serial, Collections.emptyList());
    }

    private static AddSubDomainCertificateRequest createAddDomainCertificate(String domainName, String subject, String issuer, String serial, List<String> distributionList) throws Exception {
        AddSubDomainCertificateRequest request = new AddSubDomainCertificateRequest();
        request.setSubDomainName(domainName);
        request.setIsAdminCertificate(false);
        request.setIsRootCertificate(false);
        X509Certificate certificate = CommonTestUtils.createCertificate(serial, issuer, subject, new Date(), CommonTestUtils.getFutureYearDate(2).getTime(), distributionList);
        request.setCertificatePublicKey(certificate.getEncoded());
        return request;
    }


}
