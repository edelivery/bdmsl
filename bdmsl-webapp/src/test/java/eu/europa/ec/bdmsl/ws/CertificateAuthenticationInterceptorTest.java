/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.ws;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.test.CommonTestUtils;
import eu.europa.ec.bdmsl.security.CertificateAuthentication;
import eu.europa.ec.bdmsl.security.SMLAuthenticationProvider;
import eu.europa.ec.bdmsl.security.SecurityTokenAuthentication;
import eu.europa.ec.bdmsl.security.UnsecureAuthentication;
import eu.europa.ec.edelivery.security.ClientCertAuthenticationFilter;
import eu.europa.ec.edelivery.security.PreAuthenticatedCertificatePrincipal;
import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.message.MessageImpl;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.security.Principal;
import java.security.cert.X509Certificate;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Flavio SANTOS
 */
class CertificateAuthenticationInterceptorTest extends AbstractJUnit5Test {

    private final String CLIENT_CERT_HEADER_KEY = "Client-Cert";
    private final String ADMIN_HEADER_KEY = "Admin-Pwd";
    private final String CLIENT_CERT_ATTRIBUTE_KEY = "jakarta.servlet.request.X509Certificate";

    @Autowired
    private CertificateAuthenticationInterceptor testInstance;

    @Autowired
    private SMLAuthenticationProvider smlAuthenticationProvider;

    ClientCertAuthenticationFilter blueCoatAuthenticationFilter = new ClientCertAuthenticationFilter();


    @BeforeEach
    void before() throws Exception {
        Mockito.doReturn(true).when(configurationBusiness).isUnsecureLoginEnabled();
        Mockito.doReturn(true).when(configurationBusiness).isClientCertEnabled();
        ReflectionTestUtils.setField(smlAuthenticationProvider, "configurationBusiness", configurationBusiness);

        ReflectionTestUtils.setField(testInstance, "configurationBusiness", configurationBusiness);
        ReflectionTestUtils.setField(testInstance, "smlAuthenticationProvider", smlAuthenticationProvider);
    }

    @Test
    void testHttpAdminAuthenticationRequest() {
        //GIVEN
        Message message = prepareAdminRequest("http", ADMIN_HEADER_KEY, CommonTestUtils.ADMIN_PLAINTEXT_PASSWORD);

        //WHEN
        testInstance.handleMessage(message);

        //THEN
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        assertNotNull(authentication);
        assertEquals("MonitorUser", authentication.getName());
        assertEquals(SecurityTokenAuthentication.class, authentication.getClass());
    }

    @Test
    void testHttpsAdminAuthenticationRequest() {
        //GIVEN
        Message message = prepareAdminRequest("https", ADMIN_HEADER_KEY, CommonTestUtils.ADMIN_PLAINTEXT_PASSWORD);

        //WHEN
        testInstance.handleMessage(message);

        //THEN
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        assertNotNull(authentication);
        assertEquals("MonitorUser", authentication.getName());
        assertEquals(SecurityTokenAuthentication.class, authentication.getClass());
    }

    @Test
    void testAdminAccessByHTTPSCertificate() throws Exception {

        //GIVEN
        String subject = "CN=EHEALTH_SMP_77777777,O=DG-DIGIT,C=BE";
        String issuer = "CN=EHEALTH_CERTIFICATE_ISSUER_654321,O=DG-DIGIT,C=BE";
        X509Certificate certificate = createX509CertificateKeystoreTrusted(issuer, subject, false);
        String certificateId = "CN=EHEALTH_SMP_77777777,O=DG-DIGIT,C=BE:" + StringUtils.leftPad(certificate.getSerialNumber().toString(16), 32, '0');
        Message message = prepareX509CertRequest("https", CLIENT_CERT_ATTRIBUTE_KEY, new X509Certificate[]{certificate});
        message = prepareRequestForHeader(message, null, ADMIN_HEADER_KEY, CommonTestUtils.ADMIN_PLAINTEXT_PASSWORD);

        //WHEN
        testInstance.handleMessage(message);

        //THEN
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        assertNotNull(authentication);
        assertEquals(certificateId, authentication.getName());
        assertEquals(CertificateAuthentication.class, authentication.getClass());
    }

    @Test
    void testHttpsAdminAccessByBluecoat() {
        //ADMIN ACCESS is ignored by authenticating through BlueCoat

        testAdminAccessByBluecoat("https");
    }

    @Test
    void testHttpAdminAccessByBluecoat() {
        //ADMIN ACCESS is ignored by authenticating through BlueCoat

        testAdminAccessByBluecoat("http");
    }

    private void testAdminAccessByBluecoat(String protocol) {
        //GIVEN
        Message message = prepareAdminRequest(protocol, ADMIN_HEADER_KEY, CommonTestUtils.ADMIN_PLAINTEXT_PASSWORD);
        String certHeader = "sno=53%3Aef%3A79%3Ac3%3A54%3A98%3Abb%3A63%3A38%3A35%3A9a%3A19%3A5d%3A2d%3Ad8%3A8c&subject=C%3DBE%2C+O%3DDG-DIGIT%2C+CN%3DSMP_1000000007&validfrom=Oct+21+00%3A00%3A00+2014+GMT&validto=Oct+20+23%3A59%3A59+2199+GMT&issuer=CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA,O=NATIONAL AGENCY,C=BE";
        message = prepareRequestForHeader(message, null, CLIENT_CERT_HEADER_KEY, certHeader);

        //WHEN
        testInstance.handleMessage(message);

        //THEN
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        assertNotNull(authentication);
        assertEquals("CN=SMP_1000000007,O=DG-DIGIT,C=BE:53ef79c35498bb6338359a195d2dd88c", authentication.getName());
        assertEquals(CertificateAuthentication.class, authentication.getClass());
    }

    @Test
    void testHttpAuthenticationRequestWithoutParameters() {
        //GIVEN
        Message message = prepareRequestForHeader("http", null, null);

        //WHEN
        testInstance.handleMessage(message);

        //THEN
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        assertNotNull(authentication);
        assertEquals("unsecure-http-client", authentication.getName());
        assertEquals(UnsecureAuthentication.class, authentication.getClass());
    }

    @Test
    void testHttpsAuthenticationRequestUnsecured() {
        //GIVEN;
        Mockito.doReturn(true).when(configurationBusiness).isUnsecureLoginEnabled();
        Message message = prepareRequestForHeader("https", null, null);

        //WHEN - THEN
        testInstance.handleMessage(message);

        //THEN
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        assertNotNull(authentication);
        assertEquals("unsecure-http-client", authentication.getName());
        assertEquals(UnsecureAuthentication.class, authentication.getClass());
    }

    @Test
    void testHttpAuthenticationRequestUnsecured() {
        //GIVEN
        Mockito.doReturn(true).when(configurationBusiness).isUnsecureLoginEnabled();
        Message message = prepareRequestForHeader("http", null, null);

        //WHEN - THEN
        testInstance.handleMessage(message);

        //THEN
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        assertNotNull(authentication);
        assertEquals("unsecure-http-client", authentication.getName());
        assertEquals(UnsecureAuthentication.class, authentication.getClass());
    }

    @Test
    void testHttpsAuthenticationRequestWithoutParametersAndWithoutAuthenticationWaysAvailable() {
        //GIVEN
        Mockito.doReturn(false).when(configurationBusiness).isUnsecureLoginEnabled();
        Message message = prepareRequestForHeader("https", null, null);

        //WHEN - THEN
        assertThrows(Fault.class, () -> testInstance.handleMessage(message));
    }


    @Test
    void testHttpAuthenticationRequestWithoutParametersAndWithoutAuthenticationWaysAvailable() {
        //GIVEN
        Mockito.doReturn(false).when(configurationBusiness).isUnsecureLoginEnabled();

        Message message = prepareAdminRequest("http", null, null);

        //WHEN - THEN
        assertThrows(Fault.class, () -> testInstance.handleMessage(message));
    }

    @Test
    void testHttpClientCertAuthenticationRequest() {
        //GIVEN
        Mockito.doReturn(false).when(configurationBusiness).isUnsecureLoginEnabled();
        String certHeader = "sno=53%3Aef%3A79%3Ac3%3A54%3A98%3Abb%3A63%3A38%3A35%3A9a%3A19%3A5d%3A2d%3Ad8%3A8c&subject=C%3DBE%2C+O%3DDG-DIGIT%2C+CN%3DSMP_1000000007&validfrom=Oct+21+00%3A00%3A00+2014+GMT&validto=Oct+20+23%3A59%3A59+2199+GMT&issuer=CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA,O=NATIONAL AGENCY,C=BE";
        Message message = prepareBlueCoatRequest("https", CLIENT_CERT_HEADER_KEY, certHeader);

        //WHEN
        testInstance.handleMessage(message);

        //THEN
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        assertNotNull(authentication);
        assertEquals("CN=SMP_1000000007,O=DG-DIGIT,C=BE:53ef79c35498bb6338359a195d2dd88c", authentication.getName());
        assertEquals(CertificateAuthentication.class, authentication.getClass());
    }

    @Test
    void testHttpsClientCertAuthenticationRequest() {
        //GIVEN
        String certHeader = "sno=53%3Aef%3A79%3Ac3%3A54%3A98%3Abb%3A63%3A38%3A35%3A9a%3A19%3A5d%3A2d%3Ad8%3A8c&subject=C%3DBE%2C+O%3DDG-DIGIT%2C+CN%3DSMP_1000000007&validfrom=Oct+21+00%3A00%3A00+2014+GMT&validto=Oct+20+23%3A59%3A59+2199+GMT&issuer=CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA,O=NATIONAL AGENCY,C=BE";
        Message message = prepareBlueCoatRequest("https", CLIENT_CERT_HEADER_KEY, certHeader);

        //WHEN
        testInstance.handleMessage(message);

        //THEN
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        assertNotNull(authentication);
        assertEquals("CN=SMP_1000000007,O=DG-DIGIT,C=BE:53ef79c35498bb6338359a195d2dd88c", authentication.getName());
        assertEquals(CertificateAuthentication.class, authentication.getClass());
    }

    @Test
    void testHttpsClientCertAuthenticationRequestWithBlueCoatDisabled() {
        //GIVEN
        String certHeader = "sno=53%3Aef%3A79%3Ac3%3A54%3A98%3Abb%3A63%3A38%3A35%3A9a%3A19%3A5d%3A2d%3Ad8%3A8c&subject=C%3DBE%2C+O%3DDG-DIGIT%2C+CN%3DSMP_1000000007&validfrom=Oct+21+00%3A00%3A00+2014+GMT&validto=Oct+20+23%3A59%3A59+2199+GMT&issuer=CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA,O=NATIONAL AGENCY,C=BE";
        Message message = prepareBlueCoatRequest("https", CLIENT_CERT_HEADER_KEY, certHeader);
        Mockito.doReturn(false).when(configurationBusiness).isClientCertEnabled();

        //WHEN
        testInstance.handleMessage(message);

        //THEN
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        assertNotNull(authentication);
        assertEquals("unsecure-http-client", authentication.getName());
        assertEquals(UnsecureAuthentication.class, authentication.getClass());
    }

    @Test
    void testHttpClientCertAuthenticationRequestWithBlueCoatDisabled() {
        //GIVEN
        String certHeader = "sno=53%3Aef%3A79%3Ac3%3A54%3A98%3Abb%3A63%3A38%3A35%3A9a%3A19%3A5d%3A2d%3Ad8%3A8c&subject=C%3DBE%2C+O%3DDG-DIGIT%2C+CN%3DSMP_1000000007&validfrom=Oct+21+00%3A00%3A00+2014+GMT&validto=Oct+20+23%3A59%3A59+2199+GMT&issuer=CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA,O=NATIONAL AGENCY,C=BE";
        Message message = prepareBlueCoatRequest("http", CLIENT_CERT_HEADER_KEY, certHeader);
        Mockito.doReturn(false).when(configurationBusiness).isClientCertEnabled();

        //WHEN
        testInstance.handleMessage(message);

        //THEN
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        assertNotNull(authentication);
        assertEquals("unsecure-http-client", authentication.getName());
        assertEquals(UnsecureAuthentication.class, authentication.getClass());
    }

    @Test
    void testHttpsX509RootCACertificateAuthenticationRequest() throws Exception {
        //GIVEN
        String subject = "CN=SMP_SUBJECT_TEST_123456,O=DG-DIGIT,C=BE";
        String issuer = "CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA,O=NATIONAL AGENCY,C=BE";
        X509Certificate certificate = createX509CertificateKeystoreTrusted(issuer, subject, false);
        String certificateId = "CN=SMP_SUBJECT_TEST_123456,O=DG-DIGIT,C=BE:" + StringUtils.leftPad(certificate.getSerialNumber().toString(16), 32, '0');
        Message message = prepareX509CertRequest("https", CLIENT_CERT_ATTRIBUTE_KEY, new X509Certificate[]{certificate});

        //WHEN
        testInstance.handleMessage(message);

        //THEN
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        assertNotNull(authentication);
        assertEquals(certificateId, authentication.getName());
        assertEquals(CertificateAuthentication.class, authentication.getClass());
    }

    @Test
    void testHttpsX509CertificateNonRootCAAuthenticationRequest() throws Exception {
        //GIVEN
        String subject = "CN=EHEALTH_SMP_77777777,O=DG-DIGIT,C=BE";
        String issuer = "CN=EHEALTH_CERTIFICATE_ISSUER_654321,O=DG-DIGIT,C=BE";
        X509Certificate certificate = createX509CertificateKeystoreTrusted(issuer, subject, false);
        String certificateId = "CN=EHEALTH_SMP_77777777,O=DG-DIGIT,C=BE:" + StringUtils.leftPad(certificate.getSerialNumber().toString(16), 32, '0');
        Message message = prepareX509CertRequest("https", CLIENT_CERT_ATTRIBUTE_KEY, new X509Certificate[]{certificate});

        //WHEN
        testInstance.handleMessage(message);

        //THEN
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        assertNotNull(authentication);
        assertEquals(certificateId, authentication.getName());
        assertEquals(CertificateAuthentication.class, authentication.getClass());
    }

    @Test
    void testHttpsX509CertificateNonRootCAAndRootCAAuthenticationRequest() throws Exception {
        //GIVEN
        String subject = "CN=EHEALTH_SMP_77777777,O=DG-DIGIT,C=BE";
        String issuer = "CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA,O=NATIONAL AGENCY,C=BE";
        X509Certificate certificate = createX509CertificateKeystoreTrusted(issuer, subject, true);
        String certificateId = "CN=EHEALTH_SMP_77777777,O=DG-DIGIT,C=BE:" + StringUtils.leftPad(certificate.getSerialNumber().toString(16), 32, '0');
        Message message = prepareX509CertRequest("https", CLIENT_CERT_ATTRIBUTE_KEY, new X509Certificate[]{certificate});

        //WHEN
        testInstance.handleMessage(message);

        //THEN
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        assertNotNull(authentication);
        assertEquals(certificateId, authentication.getName());
        assertEquals(CertificateAuthentication.class, authentication.getClass());
    }

    @Test
    void testHttpsCertificateAuthenticationRequestWithWrongCertificateFormat() {
        //GIVEN
        Message message = prepareX509CertRequest("https", CLIENT_CERT_ATTRIBUTE_KEY, new Object());

        //WHEN THEN
        Fault result = assertThrows(Fault.class, () -> testInstance.handleMessage(message));
        assertTrue(result.getMessage().contains("java.lang.Object cannot be cast to"));
                CoreMatchers.containsString("java.lang.Object cannot be cast to");
    }

    @Test
    void testX509AuthenticationWithSpecialCharacters() throws Exception {
        //GIVEN
        X509Certificate certificate = CommonTestUtils.loadCertificate("CertificateWithSpecialCharacters.cer");
        addCertificateToTruststore(certificate);


        Message message = prepareX509CertRequest("https", CLIENT_CERT_ATTRIBUTE_KEY, new X509Certificate[]{certificate});
        message = prepareRequestForHeader(message, null, ADMIN_HEADER_KEY, CommonTestUtils.ADMIN_PLAINTEXT_PASSWORD);

        //WHEN
        testInstance.handleMessage(message);

        //THEN
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        assertNotNull(authentication);
        assertEquals("CN=slash/backslash\\\\quote\\\"colon:_rfc2253special_ampersand&comma\\,equals\\=plus\\+lessthan\\<greaterthan\\>hash\\#semicolon\\;end,O=DEẞßAaPLzołcNOÆæØøAa,C=PL:00000000000000000000000000001010", authentication.getName());
        assertEquals(CertificateAuthentication.class, authentication.getClass());
    }

    @Test
    void testBluecoatWithSpecialCharacters1() {
        //replace åöÿËStrålfors by aoyEStralfors
        // escape special characters = and ,
        String certHeader = "sno=12%3A89%3A7a%3A5y%3A97%3A99%3Ab1%3A17%3A40%3Ac6%3Ag8%3A53%3A04%3A14%3A42%3Add&subject=C%3DSE%2C+O%3D%5CxC3%5CxA5%5Cxc3%5Cxb6%5Cxc3%5Cxbf%5Cxc3%5Cx8bStr%5CxC3%5CxA5lfors%2C+CN%3DSMP_12=34,56&validfrom=Mar+20+00%3A00%3A00+2017+GMT&validto=Mar+20+23%3A59%3A59+2030+GMT&issuer=C%3DDK%2C+O%3DNATIONAL+IT";
        Principal principal = blueCoatAuthenticationFilter.buildPrincipalFromHeader(certHeader);
        assertEquals("CN=SMP_12\\=34\\,56,O=aoyEStralfors,C=SE:12897a5y9799b11740c6g853041442dd", principal.getName());
    }

    @Test
    void testBluecoatWithSpecialCharacters2() {
        String certHeader = "sno=4112+%280x1010%29&subject=C%3DPL%2C+O%3DDE%5CxE1%5CxBA%5Cx9E%5CxC3%5Cx9F%5CxC3%5Cx84%5CxC3%5CxA4PL%5CxC5%5CxBC%5CxC3%5CxB3%5CxC5%5Cx82%5CxC4%5Cx87NO%5CxC3%5Cx86%5CxC3%5CxA6%5CxC3%5Cx98%5CxC3%5CxB8%5CxC3%5Cx85%5CxC3%5CxA5%2C+CN%3Dslash%2Fbackslash%5Cquote%22colon%3A_rfc2253special_ampersand%26comma%2Cequals%3Dplus%2Blessthan%3Cgreaterthan%3Ehash%23semicolon%3Bend&validfrom=Oct+12+12%3A57%3A42+2017+GMT&validto=Jan+18+12%3A57%3A42+2028+GMT&issuer=C%3DBE%2C+ST%3DBelgium%2C+O%3DConnectivity+Test%2C+OU%3DConnecting+Europe+Facility%2C+CN%3DConnectivity+Test+Component+CA%2FemailAddress%3DCEF-EDELIVERY-SUPPORT%40ec.europa.eu";
        PreAuthenticatedCertificatePrincipal principal = blueCoatAuthenticationFilter.buildPrincipalFromHeader(certHeader);
        assertEquals("CN=slash/backslash\\\\quote\\\"colon:_rfc2253special_ampersand&comma\\,equals\\=plus\\+lessthan\\<greaterthan\\>hash\\#semicolon\\;end,O=DEẞßAaPLzołcNOÆæØøAa,C=PL:00000000000000000000000000001010", principal.getName(32));
        assertEquals("CN=Connectivity Test Component CA,OU=Connecting Europe Facility,O=Connectivity Test,ST=Belgium,C=BE", principal.getIssuerDN());
    }

    @Test
    void testBluecoatWithSpecialCharacters3() {
        String certHeader = "sno=48%3Ab6%3A81%3Aee%3A8e%3A0d%3Acc%3A08&subject=C%3DBE%2C+O%3DEuropean+Commission%2C+OU%3DCEF_eDelivery.europa.eu%2C+OU%3DeHealth%2C+CN%3DEHEALTH_SMP_TEST_BRAZIL%2FemailAddress%3DCEF-EDELIVERY-SUPPORT%40ec.europa.eu&validfrom=Feb++1+14%3A20%3A18+2017+GMT&validto=Jul++9+23%3A59%3A00+2019+GMT&issuer=C%3DDE%2C+O%3DT-Systems+International+GmbH%2C+OU%3DT-Systems+Trust+Center%2C+ST%3DNordrhein+Westfalen%2FpostalCode%3D57250%2C+L%3DNetphen%2Fstreet%3DUntere+Industriestr.+20%2C+CN%3DShared+Business+CA+4&policy_oids=1.3.6.1.4.1.7879.13.25";
        PreAuthenticatedCertificatePrincipal principal = blueCoatAuthenticationFilter.buildPrincipalFromHeader(certHeader);
        assertEquals("CN=EHEALTH_SMP_TEST_BRAZIL,O=European Commission,C=BE:000000000000000048b681ee8e0dcc08", principal.getName(32));
        assertEquals("CN=Shared Business CA 4,OU=T-Systems Trust Center,O=T-Systems International GmbH,L=Netphen,ST=Nordrhein Westfalen,C=DE", principal.getIssuerDN());

    }

    @Test
    void testBluecoatWithSpecialCharacters4() {
        String certHeader = "sno=f7%3A1e%3Ae8%3Ab1%3A1c%3Ab3%3Ab7%3A87&subject=C%3DBE%2C+O%3DEuropean+Commission%2C+OU%3DCEF_eDelivery.europa.eu%2C+OU%3DeHealth%2C+OU%3DSMP_TEST%2C+CN%3DEHEALTH_SMP_EC%2FemailAddress%3DCEF-EDELIVERY-SUPPORT%40ec.europa.eu&validfrom=Dec++6+17%3A41%3A42+2016+GMT&validto=Jul++9+23%3A59%3A00+2019+GMT&issuer=C%3DDE%2C+O%3DT-Systems+International+GmbH%2C+OU%3DT-Systems+Trust+Center%2C+ST%3DNordrhein+Westfalen%2FpostalCode%3D57250%2C+L%3DNetphen%2Fstreet%3DUntere+Industriestr.+20%2C+CN%3DShared+Business+CA+4&policy_oids=1.3.6.1.4.1.7879.13.25";
        PreAuthenticatedCertificatePrincipal principal = blueCoatAuthenticationFilter.buildPrincipalFromHeader(certHeader);
        assertEquals("CN=EHEALTH_SMP_EC,O=European Commission,C=BE:0000000000000000f71ee8b11cb3b787", principal.getName(32));
        assertEquals("CN=Shared Business CA 4,OU=T-Systems Trust Center,O=T-Systems International GmbH,L=Netphen,ST=Nordrhein Westfalen,C=DE", principal.getIssuerDN());
    }

    @Test
    void testBluecoatWithSpecialCharacters5() {
        String certHeader = "sno=4112+%280x1010%29&subject=C%3DPL%2C+O%3DDE%5CxE1%5CxBA%5Cx9E%5CxC3%5Cx9F%5CxC3%5Cx84%5CxC3%5CxA4PL%5CxC5%5CxBC%5CxC3%5CxB3%5CxC5%5Cx82%5CxC4%5Cx87NO%5CxC3%5Cx86%5CxC3%5CxA6%5CxC3%5Cx98%5CxC3%5CxB8%5CxC3%5Cx85%5CxC3%5CxA5%2C+CN%3Dslash%2Fback%5Cslash%5Cquote%22colon%3A_rfc2253special_ampersand%26comma%2Cequals%3Dplus%2Blessthan%3Cgreaterthan%3Ehash%23semicolon%3Bend&validfrom=Oct+12+12%3A57%3A42+2017+GMT&validto=Jan+18+12%3A57%3A42+2028+GMT&issuer=C%3DBE%2C+ST%3DBelgium%2C+O%3DConnectivity+Test%2C+OU%3DConnecting+Europe+Facility%2C+CN%3DConnectivity+Test+Component+CA%2FemailAddress%3DCEF-EDELIVERY-SUPPORT%40ec.europa.eu";
        PreAuthenticatedCertificatePrincipal principal = blueCoatAuthenticationFilter.buildPrincipalFromHeader(certHeader);

        assertEquals("CN=slash/back\\\\slash\\\\quote\\\"colon:_rfc2253special_ampersand&comma\\,equals\\=plus\\+lessthan\\<greaterthan\\>hash\\#semicolon\\;end,O=DEẞßAaPLzołcNOÆæØøAa,C=PL:00000000000000000000000000001010", principal.getName(32));
        assertEquals("CN=Connectivity Test Component CA,OU=Connecting Europe Facility,O=Connectivity Test,ST=Belgium,C=BE", principal.getIssuerDN());
    }

    @Test
    void testBlueCoatWithEMAILADDRESS() {
        //GIVEN
        String certHeader = "subject=L=Brussels,O=DIGIT,EMAILADDRESS=receiver@test.be, CN=SMP_receiverCN, OU=B4, ST=BE, C=BE&sno=0001&validto=Jun 1 10:37:53 2035 CEST&issuer=O=DIGIT,CN=rootCN,EMAILADDRESS=root@test.be,OU=B4,ST=BE,C=BE,L=Brussels&validfrom=Jun 1 10:37:53 2015 CEST";
        Message message = prepareBlueCoatRequest("http", CLIENT_CERT_HEADER_KEY, certHeader);
        Mockito.doReturn(true).when(configurationBusiness).isClientCertEnabled();

        //WHEN
        testInstance.handleMessage(message);

        //THEN
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        assertNotNull(authentication);
        assertEquals("CN=SMP_receiverCN,O=DIGIT,C=BE:00000000000000000000000000000001", authentication.getName());
        assertEquals("CN=rootCN,OU=B4,O=DIGIT,L=Brussels,ST=BE,C=BE", ((PreAuthenticatedCertificatePrincipal) authentication.getPrincipal()).getIssuerDN());
        assertEquals("CN=SMP_receiverCN,O=DIGIT,C=BE", ((PreAuthenticatedCertificatePrincipal) authentication.getPrincipal()).getSubjectShortDN());

        assertEquals(CertificateAuthentication.class, authentication.getClass());
    }

    private Message prepareAdminRequest(String protocol, String key, Object value) {
        return prepareRequestForHeader(protocol, key, value);
    }

    private Message prepareBlueCoatRequest(String protocol, String key, String value) {
        return prepareRequestForHeader(protocol, key, value);
    }

    private Message prepareX509CertRequest(String protocol, String key, Object value) {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setAttribute(key, value);
        return prepareRequest(request, protocol);

    }

    private Message prepareRequestForHeader(String protocol, String key, Object value) {
        return prepareRequestForHeader(null, protocol, key, value);
    }

    private Message prepareRequestForHeader(Message message, String protocol, String key, Object value) {
        MockHttpServletRequest request = new MockHttpServletRequest();
        if (message != null) {
            request = (MockHttpServletRequest) message.get("HTTP.REQUEST");
            request.addHeader(key, value);
            return message;
        }

        if (!StringUtils.isEmpty(key)) {
            request.addHeader(key, value);
        }

        return prepareRequest(request, protocol);
    }

    private Message prepareRequest(MockHttpServletRequest request, String protocol) {
        request.setScheme(protocol);
        Message message = new MessageImpl();
        message.put("HTTP.REQUEST", request);
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        return message;
    }
}

