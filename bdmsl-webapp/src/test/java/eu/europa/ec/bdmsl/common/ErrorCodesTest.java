/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2017 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.exception.ErrorCode;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.EnumSet;

import static org.junit.jupiter.api.Assertions.assertTrue;
/**
 * @author Flavio SANTOS
 */
class ErrorCodesTest extends AbstractJUnit5Test {
    static final Logger LOG = LoggerFactory.getLogger(ErrorCodesTest.class);

    @Test
    void testErrorCodes() {
        for (ErrorCode errorCode : EnumSet.allOf(ErrorCode.class)) {
            assertTrue(isCodeValid(errorCode, errorCode.getErrorCode()));
        }
    }

    private boolean isCodeValid(ErrorCode errorCode, int value) {
        LOG.info("Test code: " + errorCode + "with value: " + value);
        switch (errorCode) {
            case SMP_NOT_FOUND_ERROR:
                return value == 100;
            case UNAUTHORIZED_ERROR:
                return value == 101;
            case CERTIFICATE_AUTHENTICATION_ERROR:
                return value == 102;
            case ROOT_CERTIFICATE_ALIAS_NOT_FOUND_ERROR:
                return value == 103;
            case CERTIFICATE_REVOKED_ERROR:
                return value == 104;
            case GENERIC_TECHNICAL_ERROR:
                return value == 105;
            case BAD_REQUEST_ERROR:
                return value == 106;
            case DNS_CLIENT_ERROR:
                return value == 107;
            case SIG0_ERROR:
                return value == 108;
            case BAD_CONFIGURATION_ERROR:
                return value == 109;
            case PARTICIPANT_NOT_FOUND_ERROR:
                return value == 110;
            case MIGRATION_NOT_FOUND_ERROR:
                return value == 111;
            case DUPLICATE_PARTICIPANT_ERROR:
                return value == 112;
            case SMP_DELETE_ERROR:
                return value == 113;
            case MIGRATION_PLANNED_ERROR:
                return value == 114;
            case CERTIFICATE_NOT_FOUND_ERROR:
                return value == 115;
            case CERTIFICATE_NOT_YET_VALID:
                return value == 116;
            case CERTIFICATE_EXPIRED:
                return value == 117;
            case NOT_FOUND_ERROR:
                return value == 118;
            case INVALID_ARGUMENT_ERROR:
                return value == 119;
            case SMP_OUT_OF_PARTICIPANT_QUOTA_EXCEPTION:
                return value == 120;
            case SUBDOMAIN_OUT_OF_PARTICIPANT_QUOTA_EXCEPTION:
                return value == 121;
            case SMP_MANAGE_ERROR:
                return value == 122;
            default:
                LOG.warn("Unknown error code: " + errorCode);
                return false;
        }
    }
}
