/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.impl;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.bo.DNSRecordBO;
import eu.europa.ec.bdmsl.dao.IDNSRecordDAO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import jakarta.transaction.Transactional;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class DNSRecordDAOImplTest extends AbstractJUnit5Test {

    @Autowired
    private IDNSRecordDAO testInstance;


    @Test
    @Transactional
    void getDNSRecordNameAndTypeCaseInsensitive() {
        // given
        DNSRecordBO dnsRecordBO = createDnsRecordBO("naPTr", "my.EUROPA.eu", "europa.eu", ".*", "Meta:SMP");
        // then
        testInstance.addDNSRecord(dnsRecordBO);

        Optional<DNSRecordBO> resultOpt = testInstance.getDNSRecord("naptr", "my.europa.eu", "europa.eu", dnsRecordBO.getValue());
        assertTrue(resultOpt.isPresent());
        DNSRecordBO result = resultOpt.get();
        assertEquals(dnsRecordBO.getType().toUpperCase(), result.getType());
        assertEquals(dnsRecordBO.getValue(), result.getValue());
        assertEquals(dnsRecordBO.getName().toLowerCase(), result.getName());
        assertEquals(dnsRecordBO.getService(), result.getService());
    }

    @Test
    @Transactional
    void deleteDNSRecord() {
        // given
        DNSRecordBO dnsRecordBO = createDnsRecordBO("A", "smp.europa.eu", "europa.eu", "127.0.0.1", null);
        testInstance.addDNSRecord(dnsRecordBO);
        Optional<DNSRecordBO> resultOpt = testInstance.getDNSRecord("a", "smp.europa.eu", "europa.eu", dnsRecordBO.getValue());
        assertTrue(resultOpt.isPresent());
        // when
        Optional<DNSRecordBO> resultDeleteOpt = testInstance.deleteDNSRecord("a", "smp.europa.eu", "europa.eu", dnsRecordBO.getValue());
        // then
        assertTrue(resultDeleteOpt.isPresent());
        assertEquals(resultDeleteOpt.get().getType(), resultOpt.get().getType());
        assertEquals(resultDeleteOpt.get().getValue(), resultOpt.get().getValue());
        assertEquals(resultDeleteOpt.get().getName(), resultOpt.get().getName());
        assertEquals(resultDeleteOpt.get().getService(), resultOpt.get().getService());

        Optional<DNSRecordBO> resultAfterDeleteOpt = testInstance.getDNSRecord("a", "smp.europa.eu", "europa.eu", dnsRecordBO.getValue());
        assertFalse(resultAfterDeleteOpt.isPresent());
    }

    @Test
    @Transactional
    void deleteDNSRecordsByName() {

        // given
        String domainName = "smp.delete.europa.eu";
        String domainZone = "europa.eu";
        DNSRecordBO dnsRecordBO1 = createDnsRecordBO("A", domainName, "europa.eu", "127.0.0.1", null);
        DNSRecordBO dnsRecordBO2 = createDnsRecordBO("A", domainName, "europa.eu", "127.0.0.2", null);
        DNSRecordBO dnsRecordBO3 = createDnsRecordBO("NAPTR", domainName, "europa.eu", ".*", "Meta:SMP");

        testInstance.addDNSRecord(dnsRecordBO1);
        testInstance.addDNSRecord(dnsRecordBO2);
        testInstance.addDNSRecord(dnsRecordBO3);

        // when
        List<DNSRecordBO> recordBOList = testInstance.deleteDNSRecords(domainName, domainZone);

        // then
        assertFalse(recordBOList.isEmpty());
        assertEquals(3, recordBOList.size());


        assertFalse(testInstance.getDNSRecord(dnsRecordBO1.getType(), dnsRecordBO1.getName(), dnsRecordBO1.getDNSZone(), dnsRecordBO1.getValue()).isPresent());
        assertFalse(testInstance.getDNSRecord(dnsRecordBO2.getType(), dnsRecordBO2.getName(), dnsRecordBO1.getDNSZone(), dnsRecordBO2.getValue()).isPresent());
        assertFalse(testInstance.getDNSRecord(dnsRecordBO3.getType(), dnsRecordBO3.getName(), dnsRecordBO1.getDNSZone(), dnsRecordBO3.getValue()).isPresent());
    }

    @Test
    @Transactional
    void addDNSRecord() {
        // given
        DNSRecordBO dnsRecordBO = createDnsRecordBO("NAPTR", "my.europa.eu", "europa.eu", ".*", "Meta:SMP");

        // then
        testInstance.addDNSRecord(dnsRecordBO);
        // when
        DNSRecordBO result = testInstance.getDNSRecord(dnsRecordBO.getType(), dnsRecordBO.getName(), dnsRecordBO.getDNSZone(), dnsRecordBO.getValue()).get();
        assertNotNull(result);
        assertNotNull(result.getCreationDate());
        assertNotNull(result.getLastUpdateDate());
        assertEquals(dnsRecordBO.getType(), result.getType());
        assertEquals(dnsRecordBO.getValue(), result.getValue());
        assertEquals(dnsRecordBO.getName(), result.getName());
        assertEquals(dnsRecordBO.getDNSZone(), result.getDNSZone());
        assertEquals(dnsRecordBO.getService(), result.getService());
    }

    private static DNSRecordBO createDnsRecordBO(String type, String name, String dnsZone, String value, String service) {
        DNSRecordBO dnsRecordBO = new DNSRecordBO();
        dnsRecordBO.setType(type);
        dnsRecordBO.setDNSZone(dnsZone);
        dnsRecordBO.setValue(value);
        dnsRecordBO.setService(service);
        dnsRecordBO.setName(name);
        return dnsRecordBO;

    }
}
