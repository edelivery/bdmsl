/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.impl;


import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.service.dns.impl.DnsZone;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


/**
 * @author Flavio SANTOS
 */
class DnsZoneTest extends AbstractJUnit5Test {

    @Test
    void testeCreateDnsZone() {
        DnsZone dnsZone = new DnsZone("test.DOMAIN.com", "Subdomain.test.domain.com");
        assertEquals("test.DOMAIN.com", dnsZone.getDomain());
        assertEquals("Subdomain.test.domain.com", dnsZone.getSubdomain());
    }

    @Test
    void testeDnsZoneEqual() {
        DnsZone dnsZone1 = new DnsZone("test.DOMAIN.com", "Subdomain.test.domain.com");
        DnsZone dnsZone2 = new DnsZone("test.DOMAIN.com", "Subdomain.test.domain.com");

        assertTrue(dnsZone1.equals(dnsZone2));
    }
}
