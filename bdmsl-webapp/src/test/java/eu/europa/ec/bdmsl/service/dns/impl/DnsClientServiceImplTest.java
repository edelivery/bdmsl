/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.dns.impl;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.business.IManageParticipantIdentifierBusiness;
import eu.europa.ec.bdmsl.business.IManageServiceMetadataBusiness;
import eu.europa.ec.bdmsl.business.ISubdomainBusiness;
import eu.europa.ec.bdmsl.common.bo.CertificateDomainBO;
import eu.europa.ec.bdmsl.common.bo.ParticipantBO;
import eu.europa.ec.bdmsl.common.bo.ServiceMetadataPublisherBO;
import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import eu.europa.ec.bdmsl.common.enums.DNSSubSomainRecordTypeEnum;
import eu.europa.ec.bdmsl.common.exception.CertificateAuthenticationException;
import eu.europa.ec.bdmsl.common.exception.DNSClientException;
import eu.europa.ec.bdmsl.common.exception.DnsRecordBuildException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.common.util.Constant;
import eu.europa.ec.bdmsl.test.DnsMessageSenderServiceMock;
import eu.europa.ec.bdmsl.security.ICertificateAuthentication;
import eu.europa.ec.bdmsl.service.IManageServiceMetadataService;
import eu.europa.ec.dynamicdiscovery.util.HashUtil;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.ArgumentCaptor;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.xbill.DNS.*;
import org.xbill.DNS.Record;

import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;


/**
 * @author Flavio SANTOS
 * @since 06/12/2016
 */
class DnsClientServiceImplTest extends AbstractJUnit5Test {

    private static final String DOMAIN_ISALIVE_VALUE = "test.isalive.test";

    private static final String DOMAIN_VALUE = "acc.edelivery.tech.ec.europa.eu";

    private static final String DOMAIN_PROD_VALUE = "edelivery.tech.ec.europa.eu";

    private static final String SUBDOMAIN_EHEALTH_VALUE = "ehealth.acc.edelivery.tech.ec.europa.eu";

    private static final String SUBDOMAIN_EHEALTH_PROD_VALUE = "ehealth.edelivery.tech.ec.europa.eu";

    private static final String NAPTR_REC_FORMAT = "%s.%s.%s.\t60\tIN\tNAPTR\t100 10 \"U\" \"Meta:SMP\" \"!.*!%s!\" .";

    private static final String NAPTR_ANY_FORMAT = "%s.%s.%s.\t0\tANY\tANY";

    private static final String CNAME_REC_FORMAT = "B-%s.%s.%s.\t60\tIN\tCNAME\ttoBeUpdated.publisher.acc.edelivery.tech.ec.europa.eu.";

    private static final String CNAME_ANY_FORMAT = "B-%s.%s.%s.\t0\tANY\tANY";


    @Autowired
    private IManageParticipantIdentifierBusiness manageParticipantIdentifierBusiness;

    @Autowired
    private IManageServiceMetadataBusiness manageServiceMetadataBusiness;

    @Autowired
    private ICertificateAuthentication customAuthentication;

    @Autowired
    private IManageServiceMetadataService manageServiceMetadataService;

    @Autowired
    private ISubdomainBusiness subdomainBusiness;

    @BeforeEach
    void before() throws Exception {
        customAuthentication.unsecuredAuthentication();

        subdomainBusiness = Mockito.spy(subdomainBusiness);

        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        ReflectionTestUtils.setField(dnsClientService, "dnsMessageSenderService", dnsMessageSenderService);
        ReflectionTestUtils.setField(dnsClientService, "configurationBusiness", configurationBusiness);
        ReflectionTestUtils.setField(dnsClientService, "subdomainBusiness", subdomainBusiness);

    }

    private SubdomainBO createSubDomain(String subdomain, String domainzone) {
        SubdomainBO subdomainBO = new SubdomainBO();
        subdomainBO.setSubdomainName(subdomain);
        subdomainBO.setDnsZone(domainzone);
        return subdomainBO;
    }

    @Test
    void testDnsZoneNameForEHealthAcc() throws Exception {
        CertificateDomainBO certificateDomainBO = new CertificateDomainBO();
        certificateDomainBO.setSubdomain(createSubDomain(SUBDOMAIN_EHEALTH_VALUE, DOMAIN_VALUE));
        certificateDomainBO.setCertificate("CN=SMP_123456789101112,O=DG-DIGIT,C=BE");
        DnsZone dnsZone = dnsClientService.getDnsZoneName(certificateDomainBO);
        assertNotNull(dnsZone);
        assertEquals("ehealth.acc.edelivery.tech.ec.europa.eu.", dnsZone.getSubdomain());
        assertEquals("acc.edelivery.tech.ec.europa.eu.", dnsZone.getDomain());
    }

    @Test
    void testDnsZoneNameForEhealthProd() throws Exception {
        CertificateDomainBO certificateDomainBO = new CertificateDomainBO();
        certificateDomainBO.setSubdomain(createSubDomain(SUBDOMAIN_EHEALTH_PROD_VALUE, DOMAIN_PROD_VALUE));
        certificateDomainBO.setCertificate("CN=SMP_123456789101112,O=DG-DIGIT,C=BE");
        DnsZone dnsZone = dnsClientService.getDnsZoneName(certificateDomainBO);
        assertNotNull(dnsZone);
        assertEquals("ehealth.edelivery.tech.ec.europa.eu.", dnsZone.getSubdomain());
        assertEquals("edelivery.tech.ec.europa.eu.", dnsZone.getDomain());
    }

    @Test
    void testDnsZoneNameForPeppolProd() throws Exception {
        CertificateDomainBO certificateDomainBO = new CertificateDomainBO();
        certificateDomainBO.setSubdomain(createSubDomain(DOMAIN_PROD_VALUE, DOMAIN_PROD_VALUE));
        certificateDomainBO.setCertificate("CN=SMP_123456789101112,O=DG-DIGIT,C=BE");
        DnsZone dnsZone = dnsClientService.getDnsZoneName(certificateDomainBO);
        assertNotNull(dnsZone);
        assertEquals("edelivery.tech.ec.europa.eu.", dnsZone.getSubdomain());
        assertEquals("edelivery.tech.ec.europa.eu.", dnsZone.getDomain());
    }

    @Test
    void testDnsZoneNameForPeppolAcc() throws Exception {
        CertificateDomainBO certificateDomainBO = new CertificateDomainBO();
        certificateDomainBO.setSubdomain(createSubDomain(DOMAIN_VALUE, DOMAIN_VALUE));
        certificateDomainBO.setCertificate("CN=SMP_123456789101112,O=DG-DIGIT,C=BE");
        DnsZone dnsZone = dnsClientService.getDnsZoneName(certificateDomainBO);
        assertNotNull(dnsZone);
        assertEquals("acc.edelivery.tech.ec.europa.eu.", dnsZone.getSubdomain());
        assertEquals("acc.edelivery.tech.ec.europa.eu.", dnsZone.getDomain());
    }

    @Test
    void testDnsZoneNameNotExistDomain() {
        CertificateDomainBO certificateDomainBO = new CertificateDomainBO();
        certificateDomainBO.setSubdomain(createSubDomain("dummy.digit.edelivery.tech.ec.europa.eu", null));
        certificateDomainBO.setCertificate("CN=SMP_123456789101112,O=DG-DIGIT,C=BE");
        // when
        DNSClientException result = assertThrows(DNSClientException.class, () -> dnsClientService.getDnsZoneName(certificateDomainBO));
        // then
        assertThat(result.getMessage(), CoreMatchers.containsString("DNS zone for subdomain [dummy.digit.edelivery.tech.ec.europa.eu] must be configured in the database"));
    }

    @Test
    void testDnsZoneNameCertificateUnknown() {
        // when
        DNSClientException result = assertThrows(DNSClientException.class, () -> dnsClientService.getDnsZoneName(null));
        // then
        assertThat(result.getMessage(), CoreMatchers.containsString("Certificate domain can not be null"));
    }

    @Test
    void testDnsZoneNameDomainUnknown() {
        CertificateDomainBO certificateDomainBO = new CertificateDomainBO();
        certificateDomainBO.setSubdomain(null);
        certificateDomainBO.setCertificate("CN=SMP_123456789101112,O=DG-DIGIT,C=BE");
        // then
        DNSClientException result = assertThrows(DNSClientException.class, () -> dnsClientService.getDnsZoneName(certificateDomainBO));
        // then
        assertThat(result.getMessage(), CoreMatchers.containsString(" DNS client subdomain must not be null"));
    }

    @Test
    void testVerifyDNSAccessExceptionForCreatingRecord() throws Exception {
        // given
        Mockito.doReturn(true).when(configurationBusiness).isDNSEnabled();
        Mockito.doThrow(new DNSClientException("Failed to create DNS records")).when(dnsClientService).createEntriesIsAliveDNS(any(ParticipantBO.class), any(String.class));

        // when
        DNSClientException result = assertThrows(DNSClientException.class, () -> dnsClientService.verifyDNSAccess());
        // then
        assertThat(result.getMessage(), CoreMatchers.containsString("Failed to create DNS records"));
    }

    @Test
    void testVerifyDNSAccessExceptionForDeletingRecord() throws Exception {
        // given
        Mockito.doReturn(true).when(configurationBusiness).isDNSEnabled();
        Mockito.doThrow(new DNSClientException("Failed to delete DNS records")).when(dnsClientService).deleteDNSRecordsForParticipantIsAlive(any(ParticipantBO.class));

        // when
        DNSClientException result = assertThrows(DNSClientException.class, () -> dnsClientService.verifyDNSAccess());
        // then
        assertThat(result.getMessage(), CoreMatchers.containsString("Impossible to delete from DNS"));
    }

    @Test
    void testVerifyDNSAccessExceptionForLookingUpRecord() throws Exception {
        // given
        Mockito.doReturn(true).when(configurationBusiness).isDNSEnabled();
        Mockito.doThrow(new DNSClientException("Impossible to lookup")).when(dnsClientService).lookup(any(String.class), any(Integer.class));

        // when
        DNSClientException result = assertThrows(DNSClientException.class, () -> dnsClientService.verifyDNSAccess());
        // then
        assertThat(result.getMessage(), CoreMatchers.containsString("Impossible to lookup"));
    }

    @Test
    void testCreateDNSRecordsForParticipantIsAlive() throws TechnicalException {
        // given
        ParticipantBO participantBO = createParticipant("isAliveId", "isAliveSmpId", "iso6523-actorid-upis");

        // when
        DnsClientServiceImpl.ParticipantDnsRecord dnsRecordsForParticipantIsAlive = dnsClientService.createEntriesIsAliveDNS(participantBO, "http://logicaladress");

        // then
        assertEquals("B-99a6e1812465e2e976e32d3915c50733.iso6523-actorid-upis.test.isalive.test.", dnsRecordsForParticipantIsAlive.getCnameRecord().getName().toString());
        assertEquals("D3XXVTWM4ORC23VMFY5JGRHB2GCEVZUG2PWVGWLJ5HRBNVKMJSDA.iso6523-actorid-upis.test.isalive.test.", dnsRecordsForParticipantIsAlive.getNaptrRecord().getName().toString());
    }

    @Test
    void testCreateDNSRecordsForNullLogicalSmpAddress() {
        // given
        ParticipantBO participantBO = createParticipant("isAliveId", "no-existent-smp", "iso6523-actorid-upis");

        // when - then
        DnsRecordBuildException result = assertThrows(DnsRecordBuildException.class, () -> dnsClientService.createEntriesIsAliveDNS(participantBO, null));
        // then
        assertThat(result.getMessage(), CoreMatchers.containsString("SMP Address was not found for creating NAPTR record"));
    }

    @Test
    void testCreateDNSRecordsWithExtraDomainDot() throws TechnicalException {
        // given
        ParticipantBO participantBO = createParticipant("isaliveid", "smp-sample", "iso6523-actorid-upis");

        // when
        DnsClientServiceImpl.ParticipantDnsRecord dnsRecordsForParticipantIsAlive = dnsClientService.createEntriesIsAliveDNS(participantBO, "https://domain.sample.edelivery.be.");

        // then
        assertEquals("B-99a6e1812465e2e976e32d3915c50733.iso6523-actorid-upis.test.isalive.test.", dnsRecordsForParticipantIsAlive.getCnameRecord().getName().toString());
        assertEquals("D3XXVTWM4ORC23VMFY5JGRHB2GCEVZUG2PWVGWLJ5HRBNVKMJSDA.iso6523-actorid-upis.test.isalive.test.", dnsRecordsForParticipantIsAlive.getNaptrRecord().getName().toString());
        assertEquals("D3XXVTWM4ORC23VMFY5JGRHB2GCEVZUG2PWVGWLJ5HRBNVKMJSDA.iso6523-actorid-upis.test.isalive.test.\t60\tIN\tNAPTR\t100 10 \"U\" \"Meta:SMP\" \"!.*!https://domain.sample.edelivery.be!\" .", dnsRecordsForParticipantIsAlive.getNaptrRecord().toString());
    }

    @Test
    void testDeleteDNSRecordsForParticipantIsAlive() throws TechnicalException {
        // given
        ParticipantBO participantBO = createParticipant("isAliveId", "isAliveSmpId", "iso6523-actorid-upis");

        // when
        assertDoesNotThrow(() -> dnsClientService.deleteDNSRecordsForParticipantIsAlive(participantBO));
    }

    @Test
    void testGetDnsZoneNameIsAlive() throws TechnicalException {
        // given

        // when
        DnsZone dnsZoneNameIsAlive = dnsClientService.getDnsZoneNameIsAlive();

        // then
        assertEquals(DOMAIN_ISALIVE_VALUE + ".", dnsZoneNameIsAlive.getDomain());
        assertEquals(DOMAIN_ISALIVE_VALUE + ".", dnsZoneNameIsAlive.getSubdomain());
    }

    @Test
    void testIsParticipantNotCreated() {
        ParticipantBO participantBO = createParticipant("9957:" + System.currentTimeMillis(), "smpId", "iso6523-actorid-upis");
        boolean isParticipantCreated = dnsClientService.isParticipantAlreadyCreated(participantBO, DNSSubSomainRecordTypeEnum.CNAME);
        assertFalse(isParticipantCreated);
    }

    @Test
    void testIsParticipantAlreadyCreated_CNAME() throws Exception {
        //GIVEN
        ParticipantBO participantBO = createParticipant("9957:995799579957", "smpId", "iso6523-actorid-upis");
        List<Record> records = new ArrayList<Record>() {
            {
                add(new CNAMERecord(new Name("B-1fc581f80eac72200fbbe4cf71e90340.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu."), DClass.IN, 60, new Name("smp.test.com.eu" + ".")));
            }
        };
        Mockito.doReturn(records).when(dnsClientService).lookup("B-1fc581f80eac72200fbbe4cf71e90340.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu", Type.CNAME);

        //WHEN
        boolean isParticipantCreated = dnsClientService.isParticipantAlreadyCreated(participantBO, DNSSubSomainRecordTypeEnum.CNAME);

        //THEN
        assertTrue(isParticipantCreated);
    }

    @Test
    void testIsParticipantAlreadyCreated_NAPTR() throws Exception {
        //GIVEN
        ParticipantBO participantBO = createParticipant("9957:995799579957", "smpId", "iso6523-actorid-upis");
        List<Record> records = new ArrayList<Record>() {
            {
                add(new NAPTRRecord(new Name("FHPBJPVOMWUDRMAKII4TXJPUM5PL3JSCNCK3Z2JDAYTEIRSG24IA.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu."), DClass.IN, 60, 100, 10, "U", "Meta:SMP", "!^.*$!http://localhost:8080/smp!", Name.fromString(".")));
            }
        };
        Mockito.doReturn(records).when(dnsClientService).lookup("FHPBJPVOMWUDRMAKII4TXJPUM5PL3JSCNCK3Z2JDAYTEIRSG24IA.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu", Type.NAPTR);

        //WHEN
        boolean isParticipantCreated = dnsClientService.isParticipantAlreadyCreated(participantBO, DNSSubSomainRecordTypeEnum.NAPTR);

        //THEN
        assertTrue(isParticipantCreated);
    }

    @Test
    void testGetAllParticipantRecordsDNSNotEnabled() throws Exception {
        //GIVEN
        Mockito.doReturn(false).when(configurationBusiness).isDNSEnabled();

        //WHEN
        dnsClientService.getAllParticipantRecords("participant", "scheme");

        //THEN
        Mockito.verify(subdomainBusiness, Mockito.never()).findAll();
    }

    @Test
    void testGetAllParticipantRecordsDNSEnabled() throws Exception {
        //GIVEN
        List<Record> records = new ArrayList<Record>() {
            {
                add(new CNAMERecord(new Name("B-1fc581f80eac72200fbbe4cf71e90340.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu."), DClass.IN, 60, new Name("smp.test.com.eu" + ".")));
            }
        };
        List<SubdomainBO> subdomainBOList = Collections.singletonList(createSubDomain("my.subdomain.eu", "subdomain.eu"));
        Mockito.doReturn(true).when(configurationBusiness).isDNSEnabled();
        Mockito.doReturn(records).when(dnsClientService).lookupAny(any());
        Mockito.doReturn(subdomainBOList).when(subdomainBusiness).findAll();
        ArgumentCaptor<String> parameter1 = ArgumentCaptor.forClass(String.class);
        //WHEN
        Map<String, Collection<Record>> map = dnsClientService.getAllParticipantRecords("participant", "scheme-test-test");

        //THEN
        Mockito.verify(subdomainBusiness, Mockito.times(1)).findAll();
        Mockito.verify(dnsClientService, Mockito.times(2)).lookupAny(parameter1.capture());
        assertEquals("B-e42db0f9a183ac066bdec45b0c0cf2c7.scheme-test-test.my.subdomain.eu", parameter1.getAllValues().get(0));
        assertEquals("YDG6A47H4X6BZBZMH6EYMBDGGZ6TUDNDDJ6EXBMIE5L2NN65JORQ.scheme-test-test.my.subdomain.eu", parameter1.getAllValues().get(1));
        assertEquals(1, map.size());
        assertEquals("my.subdomain.eu", map.keySet().toArray()[0]);
        Collection<Record> recordsResult = map.get("my.subdomain.eu");
        assertEquals(2, recordsResult.size());
    }

    @Test
    void testGetAllDNSDataForInconsistencyReport() throws Exception {
        //GIVEN
        List<String> zones = Arrays.asList("zone1.europa.eu", "zone2.europa.eu");
        Mockito.doReturn(zones).when(dnsClientService).getDistDomainZones();
        Mockito.doReturn(2).when(dnsClientService)
                .getDNSDataForInconsistencyReport(anyString(), anyString(), any(), any(), any());

        IRDnsRangeData smpData = Mockito.mock(IRDnsRangeData.class);
        IRDnsRangeData cnameData = Mockito.mock(IRDnsRangeData.class);
        IRDnsRangeData naptrData = Mockito.mock(IRDnsRangeData.class);

        ArgumentCaptor<String> parameter1 = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> parameter2 = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<IRDnsRangeData> parameter3 = ArgumentCaptor.forClass(IRDnsRangeData.class);
        ArgumentCaptor<IRDnsRangeData> parameter4 = ArgumentCaptor.forClass(IRDnsRangeData.class);
        ArgumentCaptor<IRDnsRangeData> parameter5 = ArgumentCaptor.forClass(IRDnsRangeData.class);
        //WHEN
        int iSize = dnsClientService.getAllDNSDataForInconsistencyReport(".publisher.", smpData, cnameData, naptrData);

        //THEN
        Mockito.verify(dnsClientService, Mockito.times(2))
                .getDNSDataForInconsistencyReport(parameter1.capture()
                        , parameter2.capture(), parameter3.capture(),
                        parameter4.capture(), parameter5.capture());

        assertEquals(4, iSize);
        assertEquals(".publisher.", parameter1.getAllValues().get(0));
        assertEquals(".publisher.", parameter1.getAllValues().get(1));
        assertEquals("zone1.europa.eu", parameter2.getAllValues().get(0));
        assertEquals("zone2.europa.eu", parameter2.getAllValues().get(1));

        assertEquals(smpData, parameter3.getAllValues().get(0));
        assertEquals(smpData, parameter3.getAllValues().get(1));
        assertEquals(cnameData, parameter4.getAllValues().get(0));
        assertEquals(cnameData, parameter4.getAllValues().get(1));
        assertEquals(naptrData, parameter5.getAllValues().get(0));
        assertEquals(naptrData, parameter5.getAllValues().get(1));

    }

    @Test
    void testGetDnsZoneNameWithMatchForIssuerAndSubjectBothOk() throws TechnicalException {
        //Subject should be prioritized
        String issuer = "C=BE, O=Subdomain-Entity, OU=Subdomain-Entity, ST=Nordrhein Westfalen/postalCode\\=57250, L=Netphen/street\\=Untere Industriestr, CN=Subdomain Issuer";
        String subject = "C=BE, O=Subdomain-Entity, CN=Subdomain Subject";
        customAuthentication.blueCoatAuthentication("123456789", issuer, subject, true);

        // when
        DnsZone dnsZone = dnsClientService.getDnsZoneName();

        // then
        assertEquals("acc.edelivery.tech.ec.europa.eu.", dnsZone.getDomain());
        assertEquals("delta.acc.edelivery.tech.ec.europa.eu.", dnsZone.getSubdomain());
    }


    @Test
    void testGetDnsZoneNameWithMatchForIssuerAndSubjectBothIncorrect() {
        String issuer = "C=BE, O=Subdomain, OU=Subdomain-Entity, ST=Nordrhein Westfalen/postalCode\\=57250, L=Netphen/street\\=Untere Industriestr, CN=Subdomain Issuer 1";
        String subject = "C=BE, O=Subdomain-Entity, CN=Subdomain Subject 1";

        CertificateAuthenticationException result = assertThrows(CertificateAuthenticationException.class, () ->
                customAuthentication.blueCoatAuthentication("123456789", issuer, subject, true));

        // then
        assertThat(result.getMessage(), CoreMatchers.containsString("does not match or was not found."));
    }

    @Test
    void testGetDnsZoneNameWithIssuerOkAndSubjectNotSMPOk() {
        String issuer = "C=BE, O=Subdomain-Entity, OU=Subdomain-Entity, ST=Nordrhein Westfalen/postalCode\\=57250, L=Netphen/street\\=Untere Industriestr, CN=Subdomain Issuer";
        String subject = "C=BE, O=Subdomain-Entity, CN=Subdomain Subject 1";

        // when
        CertificateAuthenticationException result = assertThrows(CertificateAuthenticationException.class, () ->
                customAuthentication.blueCoatAuthentication("123456789", issuer, subject, true));
        // then
        assertThat(result.getMessage(), CoreMatchers.containsString("does not match or was not found."));
    }

    @Test
    void testGetDnsZoneNameWithIssuerNotOkAndSubjectOk() throws TechnicalException {
        String issuer = "C=BE, O=Subdomain, OU=Subdomain-Entity, ST=Nordrhein Westfalen/postalCode\\=57250, L=Netphen/street\\=Untere Industriestr, CN=Subdomain Issuer 1";
        String subject = "C=BE, O=Subdomain-Entity, CN=Subdomain Subject";
        customAuthentication.blueCoatAuthentication("123456789", issuer, subject, true);

        // when
        DnsZone dnsZone = dnsClientService.getDnsZoneName();

        // then
        assertEquals("acc.edelivery.tech.ec.europa.eu.", dnsZone.getDomain());
        assertEquals("delta.acc.edelivery.tech.ec.europa.eu.", dnsZone.getSubdomain());
    }


    @Test
    void testCreateParticipantWithAllDNSRecords() throws TechnicalException {
        // given
        ParticipantBO participantBO = createParticipant("isAliveId", "smp-sample", "iso6523-actorid-upis");

        // when
        DnsClientServiceImpl.ParticipantDnsRecord dnsRecordsForParticipantIsAlive = dnsClientService.createEntriesIsAliveDNS(participantBO, "https://domain.sample.edelivery.be.");

        // then
        assertEquals("B-99a6e1812465e2e976e32d3915c50733.iso6523-actorid-upis.test.isalive.test.", dnsRecordsForParticipantIsAlive.getCnameRecord().getName().toString());
        assertEquals("D3XXVTWM4ORC23VMFY5JGRHB2GCEVZUG2PWVGWLJ5HRBNVKMJSDA.iso6523-actorid-upis.test.isalive.test.", dnsRecordsForParticipantIsAlive.getNaptrRecord().getName().toString());
        assertEquals("D3XXVTWM4ORC23VMFY5JGRHB2GCEVZUG2PWVGWLJ5HRBNVKMJSDA.iso6523-actorid-upis.test.isalive.test.\t60\tIN\tNAPTR\t100 10 \"U\" \"Meta:SMP\" \"!.*!https://domain.sample.edelivery.be!\" .", dnsRecordsForParticipantIsAlive.getNaptrRecord().toString());
    }

    @Test
    void testCreateParticipantIsAliveWithCname() throws TechnicalException {
        // given
        // when
        DnsClientServiceImpl.ParticipantDnsRecord dnsRecordsForParticipantIsAlive = createParticipantIsAlive("test1.isalive.test.");

        // then
        assertEquals("B-ad6469cdf052fe8a2e0b2a1e9c1373aa.iso6523-actorid-upis.test1.isalive.test.", dnsRecordsForParticipantIsAlive.getCnameRecord().getName().toString());
        assertNull(dnsRecordsForParticipantIsAlive.getNaptrRecord());
    }

    @Test
    void testCreateParticipantIsAliveWithNaptr() throws TechnicalException {
        // given
        // when
        DnsClientServiceImpl.ParticipantDnsRecord dnsRecordsForParticipantIsAlive = createParticipantIsAlive("test2.isalive.test.");

        // then
        assertEquals("KQ3LDSWBNL26ICWW24BDFZLUIVUJSGZXAVAIKRHBLAEVVXOLMORQ.iso6523-actorid-upis.test2.isalive.test.", dnsRecordsForParticipantIsAlive.getNaptrRecord().getName().toString());
        assertNull(dnsRecordsForParticipantIsAlive.getCnameRecord());
    }

    @Test
    void testUpdateDNSRecordsForSMP() throws Exception {
        // given
        String smpId = "toBeUpdated";
        ServiceMetadataPublisherBO serviceMetadataPublisherBO = manageServiceMetadataBusiness.read(smpId);
        // set new values
        String newLogicalAddress = "http://"+UUID.randomUUID().toString()+".techteam.eu";
        serviceMetadataPublisherBO.setLogicalAddress(newLogicalAddress);
        serviceMetadataPublisherBO.setPhysicalAddress("1.1.1.1");
        List<ParticipantBO> participantBOS = manageParticipantIdentifierBusiness.getAllParticipantsForSMP(serviceMetadataPublisherBO);
        DnsMessageSenderServiceMock dnsMessageSenderService = (DnsMessageSenderServiceMock) this.dnsMessageSenderService;
        assertTrue(dnsMessageSenderService.getMessages().isEmpty());
        manageServiceMetadataService.read(smpId);

        // when
        dnsClientService.updateDNSRecordsForSMP(serviceMetadataPublisherBO, participantBOS, manageServiceMetadataBusiness.isLogicalAddressToBeUpdated(serviceMetadataPublisherBO));

        String messages = dnsMessageSenderService.getMessages();
        assertFalse(messages.contains("B-"));
        String expectedRecord = String.format("B4UGYBIZS6GY5I5TVZXZWUQGCVT3G7BQ4BYTNNCWANSQQCZNS6JA.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tNAPTR\t100 10 \"U\" \"Meta:SMP\" \"!.*!%s!\" ", newLogicalAddress);
        assertThat(messages, CoreMatchers.containsString(expectedRecord));

    }

    @Test
    void testCreateDNSRecordsForParticipantsForNaptrAndCname() throws TechnicalException, NoSuchAlgorithmException {

        // given
        String smpId = "toBeUpdated";
        ServiceMetadataPublisherBO serviceMetadataPublisherBO = manageServiceMetadataBusiness.read(smpId);

        List<ParticipantBO> participantBOS = manageParticipantIdentifierBusiness.getAllParticipantsForSMP(serviceMetadataPublisherBO);
        assertFalse(participantBOS.isEmpty());
        DnsMessageSenderServiceMock dnsMessageSenderService = (DnsMessageSenderServiceMock) this.dnsMessageSenderService;
        assertTrue(dnsMessageSenderService.getMessages().isEmpty());

        ServiceMetadataPublisherBO resultBO = manageServiceMetadataService.read(smpId);


        // when
        dnsClientService.createDNSRecordsForParticipants(participantBOS, serviceMetadataPublisherBO, SupportedDnsRecordType.NAPTR, SupportedDnsRecordType.CNAME);

        //then
        String messages = dnsMessageSenderService.getMessages();

        for (ParticipantBO partc : participantBOS) {

            assertTrue(messages.contains(String.format(NAPTR_ANY_FORMAT,
                    HashUtil.getSHA256HashBase32(partc.getParticipantId().toLowerCase(Constant.LOCALE)), partc.getScheme(),
                    resultBO.getSubdomain().getSubdomainName())));

            assertTrue(messages.contains(String.format(NAPTR_REC_FORMAT,
                    HashUtil.getSHA256HashBase32(partc.getParticipantId().toLowerCase(Constant.LOCALE)), partc.getScheme(),
                    resultBO.getSubdomain().getSubdomainName(), resultBO.getLogicalAddress())));

            assertTrue(messages.contains(String.format(CNAME_ANY_FORMAT,
                    HashUtil.getMD5Hash(partc.getParticipantId().toLowerCase(Constant.LOCALE)), partc.getScheme(),
                    resultBO.getSubdomain().getSubdomainName())));

            assertTrue(messages.contains(String.format(CNAME_REC_FORMAT,
                    HashUtil.getMD5Hash(partc.getParticipantId().toLowerCase(Constant.LOCALE)), partc.getScheme(),
                    resultBO.getSubdomain().getSubdomainName())));
        }
    }

    @Test
    void testCreateDNSRecordsForParticipantsForNaptr() throws TechnicalException, NoSuchAlgorithmException {


        // given
        String smpId = "toBeUpdated";
        ServiceMetadataPublisherBO serviceMetadataPublisherBO = manageServiceMetadataBusiness.read(smpId);
        List<ParticipantBO> participantBOS = manageParticipantIdentifierBusiness.getAllParticipantsForSMP(serviceMetadataPublisherBO);
        DnsMessageSenderServiceMock dnsMessageSenderService = (DnsMessageSenderServiceMock) this.dnsMessageSenderService;
        assertTrue(dnsMessageSenderService.getMessages().isEmpty());
        ServiceMetadataPublisherBO resultBO = manageServiceMetadataService.read(smpId);

        // when
        dnsClientService.createDNSRecordsForParticipants(participantBOS, serviceMetadataPublisherBO, SupportedDnsRecordType.NAPTR);

        //then
        String messages = dnsMessageSenderService.getMessages();
        for (ParticipantBO partc : participantBOS) {
            assertTrue(messages.contains(String.format(NAPTR_ANY_FORMAT,
                    HashUtil.getSHA256HashBase32(partc.getParticipantId().toLowerCase(Constant.LOCALE)), partc.getScheme(),
                    resultBO.getSubdomain().getSubdomainName())));

            assertTrue(messages.contains(String.format(NAPTR_REC_FORMAT,
                    HashUtil.getSHA256HashBase32(partc.getParticipantId().toLowerCase(Constant.LOCALE)), partc.getScheme(),
                    resultBO.getSubdomain().getSubdomainName(), resultBO.getLogicalAddress())));

            assertFalse(messages.contains(String.format(CNAME_ANY_FORMAT,
                    HashUtil.getMD5Hash(partc.getParticipantId().toLowerCase(Constant.LOCALE)), partc.getScheme(),
                    resultBO.getSubdomain().getSubdomainName())));

            assertFalse(messages.contains(String.format(CNAME_REC_FORMAT,
                    HashUtil.getMD5Hash(partc.getParticipantId().toLowerCase(Constant.LOCALE)), partc.getScheme(),
                    resultBO.getSubdomain().getSubdomainName())));
        }
        assertFalse(messages.contains("B-"));
    }

    @Test
    void testCreateDNSRecordsForParticipantsForCname() throws TechnicalException, NoSuchAlgorithmException {

        // given
        String smpId = "toBeUpdated";
        ServiceMetadataPublisherBO serviceMetadataPublisherBO = manageServiceMetadataBusiness.read(smpId);

        List<ParticipantBO> participantBOS = manageParticipantIdentifierBusiness.getAllParticipantsForSMP(serviceMetadataPublisherBO);
        DnsMessageSenderServiceMock dnsMessageSenderService = (DnsMessageSenderServiceMock) this.dnsMessageSenderService;
        assertTrue(dnsMessageSenderService.getMessages().isEmpty());
        ServiceMetadataPublisherBO resultBO = manageServiceMetadataService.read(smpId);

        // when
        dnsClientService.createDNSRecordsForParticipants(participantBOS, serviceMetadataPublisherBO, SupportedDnsRecordType.CNAME);

        //then
        String messages = dnsMessageSenderService.getMessages();
        for (ParticipantBO partc : participantBOS) {
            assertFalse(messages.contains(String.format(NAPTR_ANY_FORMAT,
                    HashUtil.getSHA256HashBase32(partc.getParticipantId().toLowerCase(Constant.LOCALE)), partc.getScheme(),
                    resultBO.getSubdomain().getSubdomainName())));

            assertFalse(messages.contains(String.format(NAPTR_REC_FORMAT,
                    HashUtil.getSHA256HashBase32(partc.getParticipantId().toLowerCase(Constant.LOCALE)), partc.getScheme(),
                    resultBO.getSubdomain().getSubdomainName(), resultBO.getLogicalAddress())));

            assertTrue(messages.contains(String.format(CNAME_ANY_FORMAT,
                    HashUtil.getMD5Hash(partc.getParticipantId().toLowerCase(Constant.LOCALE)), partc.getScheme(),
                    resultBO.getSubdomain().getSubdomainName())));

            assertTrue(messages.contains(String.format(CNAME_REC_FORMAT,
                    HashUtil.getMD5Hash(partc.getParticipantId().toLowerCase(Constant.LOCALE)), partc.getScheme(),
                    resultBO.getSubdomain().getSubdomainName())));
        }
        assertFalse(messages.contains("NAPTR"));
    }

    @Test
    void testCreateEntriesIsAliveDNS() throws TechnicalException {
        // given
        ServiceMetadataPublisherBO serviceMetadataPublisherBO = new ServiceMetadataPublisherBO();
        serviceMetadataPublisherBO.setSmpId("toBeUpdated");
        List<ParticipantBO> participantBOS = manageParticipantIdentifierBusiness.getAllParticipantsForSMP(serviceMetadataPublisherBO);
        DnsMessageSenderServiceMock dnsMessageSenderService = (DnsMessageSenderServiceMock) this.dnsMessageSenderService;
        assertTrue(dnsMessageSenderService.getMessages().isEmpty());

        // when
        DnsClientServiceImpl.ParticipantDnsRecord participantDnsRecord = dnsClientService.createEntriesIsAliveDNS(participantBOS.get(0), "http://isalive.acc.ec.europa.eu");

        //then
        String messages = dnsMessageSenderService.getMessages();
        assertNotNull(participantDnsRecord);
        assertNotNull(participantDnsRecord.getNaptrRecord());
        assertNotNull(participantDnsRecord.getCnameRecord());
        assertEquals("B4UGYBIZS6GY5I5TVZXZWUQGCVT3G7BQ4BYTNNCWANSQQCZNS6JA.iso6523-actorid-upis.test.isalive.test.\t60\tIN\tNAPTR\t100 10 \"U\" \"Meta:SMP\" \"!.*!http://isalive.acc.ec.europa.eu!\" .", participantDnsRecord.getNaptrRecord().toString());
        assertEquals("B-374b348ec5f5c9c7d57641c25b21c46d.iso6523-actorid-upis.test.isalive.test.\t60\tIN\tCNAME\ttoBeUpdated.publisher.test.isalive.test.", participantDnsRecord.getCnameRecord().toString());
        assertTrue(messages.contains("B-374b348ec5f5c9c7d57641c25b21c46d.iso6523-actorid-upis.test.isalive.test.\t0\tANY\tANY"));
        assertTrue(messages.contains("B-374b348ec5f5c9c7d57641c25b21c46d.iso6523-actorid-upis.test.isalive.test.\t60\tIN\tCNAME\ttoBeUpdated.publisher.test.isalive.test."));
        assertTrue(messages.contains("B4UGYBIZS6GY5I5TVZXZWUQGCVT3G7BQ4BYTNNCWANSQQCZNS6JA.iso6523-actorid-upis.test.isalive.test.\t0\tANY\tANY"));
        assertTrue(messages.contains("B4UGYBIZS6GY5I5TVZXZWUQGCVT3G7BQ4BYTNNCWANSQQCZNS6JA.iso6523-actorid-upis.test.isalive.test.\t60\tIN\tNAPTR\t100 10 \"U\" \"Meta:SMP\" \"!.*!http://isalive.acc.ec.europa.eu!\" ."));
    }

    @Test
    void testDeleteParticipantFromDNS() throws TechnicalException {
        // given
        ServiceMetadataPublisherBO serviceMetadataPublisherBO = manageServiceMetadataBusiness.read("toBeUpdated");

        List<ParticipantBO> participantBOS = manageParticipantIdentifierBusiness.getAllParticipantsForSMP(serviceMetadataPublisherBO);
        DnsMessageSenderServiceMock dnsMessageSenderService = (DnsMessageSenderServiceMock) this.dnsMessageSenderService;
        assertTrue(dnsMessageSenderService.getMessages().isEmpty());

        // when
        dnsClientService.deleteDNSRecordsForParticipants(participantBOS, serviceMetadataPublisherBO);

        //then
        String messages = dnsMessageSenderService.getMessages();
        assertTrue(messages.contains("B-374b348ec5f5c9c7d57641c25b21c46d.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu.\t0\tANY\tANY"));
        assertTrue(messages.contains("B4UGYBIZS6GY5I5TVZXZWUQGCVT3G7BQ4BYTNNCWANSQQCZNS6JA.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu.\t0\tANY\tANY"));
        assertTrue(messages.contains("B-a0f6ae7abfcdb56112394172d5ec4a5b.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu.\t0\tANY\tANY"));
        assertTrue(messages.contains("AFEIEROAFXXWWTGTRN3APKMKKARGBIXZLNJOHEHYFS4B35ORKYHA.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu.\t0\tANY\tANY"));
    }

    @Test
    void testGetAllRecords() throws Exception {
        ZoneTransferIn ztInstance = Mockito.mock(ZoneTransferIn.class);
        List<Record> records = getAllRecords();
        try (MockedStatic<ZoneTransferIn> utilities = Mockito.mockStatic(ZoneTransferIn.class)) {
            utilities.when(() -> ZoneTransferIn.newAXFR(any(Name.class), anyString(), any())).thenReturn(ztInstance);
            Mockito.when(ztInstance.getAXFR()).thenReturn(records);

            List<Record> result = dnsClientService.getAllRecords("test.domain.local");
            assertEquals(records.size(), result.size());
        }
    }

    @Test
    void testGetAllRecordsThrowsException() throws Exception {
        ZoneTransferIn ztInstance = Mockito.mock(ZoneTransferIn.class);
        ZoneTransferException exception = new ZoneTransferException("Test exception");
        String domain = "test.domain.local";

        try (MockedStatic<ZoneTransferIn> utilities = Mockito.mockStatic(ZoneTransferIn.class)) {
            utilities.when(() -> ZoneTransferIn.newAXFR(any(Name.class), anyString(), any())).thenReturn(ztInstance);
            Mockito.doThrow(exception).when(ztInstance).run();

            DNSClientException result = assertThrows(DNSClientException.class,
                    () -> dnsClientService.getAllRecords(domain));

            assertEquals(result.getCause(), exception);
            assertEquals("[ERR-107] Impossible to retrieve all records for zone " + domain, result.getMessage());
        }
    }


    @Test
    void testGetDNSDataForInconsistencyReport() throws Exception {
        ZoneTransferIn ztInstance = Mockito.mock(ZoneTransferIn.class);
        try (MockedStatic<ZoneTransferIn> utilities = Mockito.mockStatic(ZoneTransferIn.class)) {
            utilities.when(() -> ZoneTransferIn.newAXFR(any(Name.class), anyString(), any())).thenReturn(ztInstance);


            int result = dnsClientService.getDNSDataForInconsistencyReport("smp.publisher",
                    "test.domain.local", null, null, null);
            assertEquals(0, result);
        }
    }

    @Test
    void testGetDistDomainZones() throws TechnicalException {
        List<String> domainZones = dnsClientService.getDistDomainZones();
        assertEquals(6, domainZones.size());
    }

    private DnsClientServiceImpl.ParticipantDnsRecord createParticipantIsAlive(String domain) throws TechnicalException {
        // given
        customAuthentication.blueCoatAuthentication("22592", "CN=SMP_TEST_FOR_EDELIVERY_22592,O=DG-DIGIT,C=BE", "CN=SMP_TEST_FOR_EDELIVERY_22592,O=DG-DIGIT,C=BE", true);
        final String partId = "0009:EDELIVERY-24092";
        final String smpId = "SMPTESTFOREDELIVERY22592";
        final String scheme = "iso6523-actorid-upis";
        ParticipantBO participantBO = createParticipant(partId, smpId, scheme);

        DnsZone dnsZone = new DnsZone(domain, domain);
        Mockito.doReturn(dnsZone).when(dnsClientService).getDnsZoneNameIsAlive();

        // when
        return dnsClientService.createEntriesIsAliveDNS(participantBO, "http://logicaladress");
    }

    private List<Record> getAllRecords() throws Exception {
        List<Record> records = new ArrayList<Record>() {{
            add(new CNAMERecord(Name.fromString("smp.publisher.domain.edelivery.com."), DClass.IN, 60, Name.fromString("smp.domain.com.")));
            add(new CNAMERecord(Name.fromString("B-1234563a5dd2d6671ea8c5bb61987654.iso6523-actorid-upis.edelivery.tech.ec.europa.eu."), DClass.IN, 60, Name.fromString("smp.domain.com.")));
            add(new NAPTRRecord(Name.fromString("3AX3654321LG7XWIZR5123456BWVJNJJLY2TFVEGWCIQ7N5YLPUA.ehealth-participantid-qns.ehealth.edelivery.tech.ec.europa.eu."), DClass.IN, 60, 100, 10, "U", "Meta:SMP", "!.*!smp.domain.edelivery.com!", Name.fromString(".")));
            add(new NAPTRRecord(Name.fromString("XHPBWT7TYYTVWKLYGUJGOPAM6IJ7ANOM3THUGLZZOTPZ74AFR6EA.ehealth-participantid-qns.ehealth.edelivery.tech.ec.europa.eu."), DClass.IN, 60, 100, 10, "U", "Meta:SMP", "!.*!smp.domain.edelivery.com!", Name.fromString(".")));
            add(new NAPTRRecord(Name.fromString("B4UGYBIZS6GY5I5TVZXZWUQGCVT3G7BQ4BYTNNCWANSQQCZNS6JA.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu."), DClass.IN, 60, 100, 10, "U", "Meta:SMP", "!.*!smp.domain.edelivery.com!", Name.fromString(".")));
            add(new NAPTRRecord(Name.fromString("AFEIEROAFXXWWTGTRN3APKMKKARGBIXZLNJOHEHYFS4B35ORKYHA.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu."), DClass.IN, 60, 100, 10, "U", "Meta:SMP", "!.*!smp.domain.edelivery.com!", Name.fromString(".")));

        }};

        return records;
    }

    @ParameterizedTest
    @CsvSource({
            "CNAME, true, false, 0",
            "CNAME, false, false, 1",
            "CNAME, true, true, 1",
            "CNAME, false, true, 2",
            "NAPTR, false, true, 0",
            "NAPTR, false, false, 1",
            "NAPTR, true, true, 1",
            "NAPTR, true, false, 2",
            "ALL, true, true, 0",
            "ALL, true, false, 1",
            "ALL, false, true, 1",
            "ALL, false, false, 2",
    })
    void testSynchronizeDNSRecordsForSMP(String domainRecordTypes, boolean cnameExists, boolean naotrExists, int expectedRecords) throws TechnicalException {
        // Given
        ParticipantBO participantBO = createParticipant();
        ServiceMetadataPublisherBO smpBO = new ServiceMetadataPublisherBO();
        smpBO.setSmpId(participantBO.getSmpId());
        smpBO.setLogicalAddress("http://logicaladdress");
        smpBO.setSubdomain(createSubDomain("ecosystem01", "Subdomain.test.domain.eu"));
        smpBO.getSubdomain().setDnsRecordTypes(domainRecordTypes);
        List<ParticipantBO> participants = Collections.singletonList(participantBO);
        Mockito.doNothing().when(dnsClientService).sendAndValidateMessage(Mockito.any(Update.class));
        Mockito.doReturn(cnameExists).when(dnsClientService).recordExists(anyString(), Mockito.same(Type.CNAME));
        Mockito.doReturn(naotrExists).when(dnsClientService).recordExists(anyString(), Mockito.same(Type.NAPTR));
        // When
        dnsClientService.synchronizeDNSRecordsForSMP(smpBO, participants);

        // Then
        ArgumentCaptor<Update> updateCaptor = ArgumentCaptor.forClass(Update.class);
        Mockito.verify(dnsClientService).sendAndValidateMessage(updateCaptor.capture());
        Update capturedUpdate = updateCaptor.getValue();
        List<Record> updateRecords =  capturedUpdate.getSection(Section.UPDATE);
        assertEquals(expectedRecords, updateRecords.size());
    }

    private ParticipantBO createParticipant() {
        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId("0088:1234656789");
        participantBO.setScheme("iso6523-actorid-upis");
        participantBO.setSmpId("SMP_publisher");


        return participantBO;
    }

    private ParticipantBO createParticipant(String participantId, String smpId, String scheme) {
        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId(participantId);
        participantBO.setSmpId(smpId);
        participantBO.setScheme(scheme);
        return participantBO;
    }


}
