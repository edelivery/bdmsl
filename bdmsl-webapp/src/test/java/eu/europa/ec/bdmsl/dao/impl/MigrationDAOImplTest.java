/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */

/**
 * @author Flavio SANTOS
 */
package eu.europa.ec.bdmsl.dao.impl;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.bo.MigrationRecordBO;
import eu.europa.ec.bdmsl.common.bo.ParticipantBO;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.dao.IMigrationDAO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MigrationDAOImplTest extends AbstractJUnit5Test {

    @Autowired
    private IMigrationDAO migrationDAO;

    @Test
    void testFindNonMigratedRecord() throws TechnicalException {
        //GIVEN
        MigrationRecordBO migrationRecordBO = createMigrateRecord("urn:ehealth:lu:ncpb-txb", "ehealth-ncp-ids", null, null, null, false);

        //WHEN
        migrationRecordBO = migrationDAO.findNonMigratedRecord(migrationRecordBO);

        //THEN
        assertNotNull(migrationRecordBO);
        assertEquals("azsxdc123", migrationRecordBO.getMigrationCode());
    }

    @Test
    void testFindNonMigratedRecordMissingScheme() throws TechnicalException {
        //GIVEN
        MigrationRecordBO migrationRecordBO = createMigrateRecord("urn:ehealth:lu:ncpb-txb", null, null, null, null, false);

        //WHEN
        migrationRecordBO = migrationDAO.findNonMigratedRecord(migrationRecordBO);

        //THEN
        assertNull(migrationRecordBO);
    }

    @Test
    void testFindMigratedRecord() throws TechnicalException {
        //GIVEN
        MigrationRecordBO migrationRecordBO = createMigrateRecord("0037:01571593999", "iso6523-actorid-upis", "SMP2016", "newSmpId", "66fac918-0f4f-4fc5", true);

        //WHEN
        migrationRecordBO = migrationDAO.findMigratedRecord(migrationRecordBO);

        //THEN
        assertNotNull(migrationRecordBO);
        assertEquals("66fac918-0f4f-4fc5", migrationRecordBO.getMigrationCode());
    }

    @Test
    void testFindMigratedRecordMissingMigrationCode() throws TechnicalException {
        //GIVEN
        MigrationRecordBO migrationRecordBO = createMigrateRecord("0037:01571593999", "iso6523-actorid-upis", "SMP2016", "newSmpId", null, true);

        //WHEN
        migrationRecordBO = migrationDAO.findMigratedRecord(migrationRecordBO);

        //THEN
        assertNull(migrationRecordBO);
    }

    @Test
    void testFindMigrationRecordByCompositePrimaryKey() throws TechnicalException {
        //GIVEN
        MigrationRecordBO migrationRecordBO = createMigrateRecord("0037:01571593999", "iso6523-actorid-upis", null, null, "66fac918-0f4f-4fc5", false);

        //WHEN
        migrationRecordBO = migrationDAO.findMigrationRecordByCompositePrimaryKey(migrationRecordBO);

        //THEN
        assertNotNull(migrationRecordBO);
        assertEquals("66fac918-0f4f-4fc5", migrationRecordBO.getMigrationCode());
    }

    @Test
    void testFindMigrationRecordByCompositePrimaryKeyWrongMigrateCode() throws TechnicalException {
        //GIVEN
        MigrationRecordBO migrationRecordBO = createMigrateRecord("0037:01571593999", "iso6523-actorid-upis", null, null, "66fac918-0f4f-123456", false);

        //WHEN
        migrationRecordBO = migrationDAO.findMigrationRecordByCompositePrimaryKey(migrationRecordBO);

        //THEN
        assertNull(migrationRecordBO);
    }

    @Test
    @Transactional
    void testUpdateMigrationRecord() throws TechnicalException {
        //GIVEN
        MigrationRecordBO migrationRecordBO = createMigrateRecord("urn:ehealth:lu:ncpb-txb", "ehealth-ncp-ids", null, null, null, false);
        String migrateCode = migrationRecordBO.getMigrationCode();

        //WHEN
        MigrationRecordBO updatedMigrationRecordBO = migrationDAO.findNonMigratedRecord(migrationRecordBO);
        updatedMigrationRecordBO.setMigrationCode("123");
        migrationDAO.updateMigrationRecord(updatedMigrationRecordBO);

        //THEN
        migrationRecordBO = migrationDAO.findNonMigratedRecord(migrationRecordBO);
        assertNull(migrateCode);
        assertEquals("123", migrationRecordBO.getMigrationCode());
    }

    @Test
    @Transactional
    void testPerformMigration() throws TechnicalException {
        //GIVEN
        MigrationRecordBO migrationRecordBO = createMigrateRecord("0009:tobeMigrated123456", "iso6523-actorid-upis", null, null, null, false);
        migrationRecordBO = migrationDAO.findNonMigratedRecord(migrationRecordBO);

        //WHEN
        migrationRecordBO.setMigrated(true);
        migrationDAO.performMigration(migrationRecordBO);

        //THEN
        migrationRecordBO.setMigrated(false);
        assertNull(migrationDAO.findNonMigratedRecord(migrationRecordBO));

        migrationRecordBO = createMigrateRecord("0009:tobeMigrated123456", "iso6523-actorid-upis", "toBeDeletedWithMigrationPlanned", "SMP2016", "@Df1Og2#9875", true);
        migrationRecordBO = migrationDAO.findMigratedRecord(migrationRecordBO);
        assertNotNull(migrationRecordBO);
        assertEquals(true, migrationRecordBO.isMigrated());
    }

    @Test
    @Transactional
    void testCreateMigrationRecord() throws TechnicalException {
        //GIVEN
        MigrationRecordBO migrationRecordBO = createMigrateRecord("0088:newMigrationRecord", "iso6523-actorid-upis", "toBeDeletedWithMigrationPlanned", "SMP2016", "@D_543f1Og2#9875", false);

        //WHEN
        migrationDAO.createMigrationRecord(migrationRecordBO);

        //THEN
        migrationRecordBO.setMigrated(false);
        assertNotNull(migrationDAO.findNonMigratedRecord(migrationRecordBO));
    }

    @Test
    void testFindMigrationsRecordsForParticipants() throws TechnicalException {
        //GIVEN
        String smpId = "SMP2016";
        List<ParticipantBO> participants = createParticipant(smpId, "urn:ehealth:lu:ncpb-txb");

        //WHEN
        List<MigrationRecordBO> result = migrationDAO.findMigrationsRecordsForParticipants(smpId, participants);

        //THEN
        assertFalse(result.isEmpty());
    }

    @Test
    void testFindMigrationsRecordsForParticipantsWrongId() throws TechnicalException {
        //GIVEN
        String smpId = "SMP2016";
        List<ParticipantBO> participants = createParticipant(smpId, "urn:ehealth:lu:ncpb-txb-not-found");

        //WHEN
        List<MigrationRecordBO> result = migrationDAO.findMigrationsRecordsForParticipants(smpId, participants);

        //THEN
        assertTrue(result.isEmpty());
    }

    @Test
    void testFindMigrationsRecordsForSMP() throws TechnicalException {
        //GIVEN
        String smpId = "SMP2016";

        //WHEN
        List<MigrationRecordBO> result = migrationDAO.findMigrationsRecordsForSMP(smpId);

        //THEN
        assertFalse(result.isEmpty());
    }

    @Test
    void testFindMigrationsRecordsForNotFoundSMP() throws TechnicalException {
        //GIVEN
        String smpId = "SMP1999";

        //WHEN
        List<MigrationRecordBO> result = migrationDAO.findMigrationsRecordsForSMP(smpId);

        //THEN
        assertTrue(result.isEmpty());
    }

    private MigrationRecordBO createMigrateRecord(String participantId, String scheme, String oldSmpId, String newSmpId, String migrationCode,
                                                  boolean migrated) {
        MigrationRecordBO migrationRecordBO = new MigrationRecordBO();
        migrationRecordBO.setParticipantId(participantId);
        migrationRecordBO.setOldSmpId(oldSmpId);
        migrationRecordBO.setNewSmpId(newSmpId);
        migrationRecordBO.setScheme(scheme);
        migrationRecordBO.setMigrationCode(migrationCode);
        migrationRecordBO.setMigrated(migrated);
        return migrationRecordBO;
    }

    List<ParticipantBO> createParticipant(String smpId, String... ids) {
        List<ParticipantBO> participantBOS = new ArrayList<>();
        for (String id : ids) {
            ParticipantBO participantBO = new ParticipantBO();
            participantBO.setScheme("ehealth-ncp-ids");
            participantBO.setParticipantId(id);
            participantBO.setSmpId(smpId);
            participantBOS.add(participantBO);
        }
        return participantBOS;
    }
}

