/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.security.impl;

import eu.europa.ec.bdmsl.common.enums.SMLRoleEnum;
import eu.europa.ec.bdmsl.common.exception.CertificateAuthenticationException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.common.util.Constant;
import eu.europa.ec.bdmsl.test.CommonTestUtils;
import eu.europa.ec.bdmsl.security.CertificateAuthentication;
import eu.europa.ec.bdmsl.security.ICertificateAuthentication;
import eu.europa.ec.bdmsl.security.SMLAuthenticationProvider;
import eu.europa.ec.bdmsl.security.UnsecureAuthentication;
import eu.europa.ec.edelivery.security.ClientCertAuthenticationFilter;
import eu.europa.ec.edelivery.security.EDeliveryX509AuthenticationFilter;
import eu.europa.ec.edelivery.security.PreAuthenticatedCertificatePrincipal;
import jakarta.servlet.http.HttpServletRequest;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.security.Principal;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

/**
 * @author Flavio SANTOS
 * @since 28/10/2016
 */
@Component
public class CustomAuthenticationMock implements ICertificateAuthentication {

    @Autowired
    SMLAuthenticationProvider smlAuthenticationProvider;
    ClientCertAuthenticationFilter blueCoatAuthenticationFilter = new ClientCertAuthenticationFilter();
    EDeliveryX509AuthenticationFilter eDeliveryX509AuthenticationFilter = new EDeliveryX509AuthenticationFilter();

    public void blueCoatAuthentication() throws TechnicalException {
        blueCoatAuthentication("123456789");
    }

    @Override
    public void x509Authentication(String issuer, String subject, Date validFrom, Date validTo) throws Exception {
        X509Certificate certificate =CommonTestUtils.createCertificate(issuer, subject, validFrom, validTo, false);
        X509Certificate[] certificates = {certificate};

        //Injecting the dummy certificate as an authenticated user
        Authentication authentication = createX509Authentication(certificates);
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Override
    public void unsecuredAuthentication() {
        UnsecureAuthentication authentication = new UnsecureAuthentication();
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Override
    public void adminAuthentication() {
        // this certificate is also persisted in the database and has a subdomain attached to it
        String certHeaderValue = "serial=48:b6:81:ee:8e:0d:cc:08" +
                "&subject=EMAILADDRESS=receiver@test.be,C=BE, O=DIGIT, OU=FOR TEST ONLY,CN=DIGIT_SMP_TECH_TEAM_3" +
                "&validfrom=Feb  1 14:20:18 2017 GMT" +
                "&validto=Jul  9 23:59:00 2019 GMT" +
                "&issuer=C=BE, O=DIGIT, OU=FOR TEST ONLY, CN=DIGIT_SMP_TECH_TEAM_2";
        adminAuthenticationInternal(certHeaderValue);
    }

    @Override
    public void adminTransientAuthentication() {
        // this certificate does not exist at the database level
        String certHeaderValue = "serial=48:b6:81:ee:8e:0d:cc:09" +
                "&subject=EMAILADDRESS=receiver@test.be,C=BE, O=DIGIT, OU=FOR TEST ONLY,CN=DIGIT_SMP_TECH_TEAM_4" +
                "&validfrom=Feb  1 14:20:18 2017 GMT" +
                "&validto=Jul  9 23:59:00 2019 GMT" +
                "&issuer=C=BE, O=DIGIT, OU=FOR TEST ONLY, CN=DIGIT_SMP_TECH_TEAM_2";
        adminAuthenticationInternal(certHeaderValue);
    }

    private void adminAuthenticationInternal(String certHeaderValue) {
        Principal principal = blueCoatAuthenticationFilter.buildPrincipalFromHeader(certHeaderValue);
        Authentication authentication = new CertificateAuthentication((PreAuthenticatedCertificatePrincipal) principal, Collections.singletonList(SMLRoleEnum.ROLE_ADMIN.getAuthority()));
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Override
    public void blueCoatAuthentication(String serialId) throws TechnicalException {
        String issuer = "CN=SMP_" + serialId + ",O=DG-DIGIT,C=BE";
        String subject = "CN=SMP_" + serialId + ",O=DG-DIGIT,C=BE";
        blueCoatAuthentication(serialId, issuer, subject, null);
    }

    @Override
    public void blueCoatAuthentication(String serialId, String issuer, String subject, Boolean isNonRootCA) throws TechnicalException {
        try {

            DateFormat df = new SimpleDateFormat("MMM d hh:mm:ss yyyy zzz", Constant.LOCALE);
            Calendar validFrom = CommonTestUtils.getPastYearDate(2);
            Calendar validTo = CommonTestUtils.getFutureYearDate(5);

            String certHeaderValue = "serial=" + serialId + "&subject=" + subject + "&validFrom=" + df.format(validFrom.getTime()) + "&validTo=" + df.format(validTo.getTime()) + "&issuer=" + issuer;

            Principal principal = blueCoatAuthenticationFilter.buildPrincipalFromHeader(certHeaderValue);
            Authentication authentication = new CertificateAuthentication((PreAuthenticatedCertificatePrincipal) principal, Collections.emptyList());


            Authentication bcAuth = smlAuthenticationProvider.authenticate(authentication);

            if (bcAuth.isAuthenticated()) {
                SecurityContextHolder.getContext().setAuthentication(bcAuth);
            } else {
                throw new CertificateAuthenticationException("The certificate is not valid or is not present or the Admin credentials are invalid");
            }
        } catch (Exception exc) {
            throw new CertificateAuthenticationException(exc.getMessage(), exc);
        }
    }

    public CertificateAuthentication createX509Authentication(X509Certificate[] certificates) throws Exception {
        HttpServletRequest mockedRequest = Mockito.mock(HttpServletRequest.class);
        Mockito.when(mockedRequest.getAttribute("jakarta.servlet.request.X509Certificate")).thenReturn(certificates);

        Principal principal = eDeliveryX509AuthenticationFilter.buildDetails(mockedRequest);
        return new CertificateAuthentication((PreAuthenticatedCertificatePrincipal) principal, Collections.emptyList());
    }
}
