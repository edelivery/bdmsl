/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl;

import eu.europa.ec.bdmsl.business.ICertificateDomainBusiness;
import eu.europa.ec.bdmsl.business.IConfigurationBusiness;
import eu.europa.ec.bdmsl.common.bo.CertificateDomainBO;
import eu.europa.ec.bdmsl.common.exception.CertificateNotFoundException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.common.logging.ILoggingService;
import eu.europa.ec.bdmsl.common.util.Constant;
import eu.europa.ec.bdmsl.config.BDMSLWebAppConfig;
import eu.europa.ec.bdmsl.config.PropertyUtils;
import eu.europa.ec.bdmsl.dao.ISubdomainDAO;
import eu.europa.ec.bdmsl.test.CommonTestUtils;
import eu.europa.ec.bdmsl.test.DnsMessageSenderServiceMock;
import eu.europa.ec.bdmsl.test.MockInitialContextFactory;
import eu.europa.ec.bdmsl.security.CertificateAuthentication;
import eu.europa.ec.bdmsl.security.CertificateDetails;
import eu.europa.ec.bdmsl.security.SMLAuthenticationProvider;
import eu.europa.ec.bdmsl.service.*;
import eu.europa.ec.bdmsl.service.dns.IDnsMessageSenderService;
import eu.europa.ec.bdmsl.service.dns.impl.DnsClientServiceImpl;
import eu.europa.ec.bdmsl.service.impl.BDMSLMonitoringServiceImpl;
import eu.europa.ec.bdmsl.service.impl.BDMSLServiceImpl;
import eu.europa.ec.bdmsl.service.impl.ManageParticipantIdentifierServiceImpl;
import eu.europa.ec.edelivery.security.ClientCertAuthenticationFilter;
import eu.europa.ec.edelivery.security.EDeliveryTokenAuthenticationFilter;
import eu.europa.ec.edelivery.security.EDeliveryX509AuthenticationFilter;
import eu.europa.ec.edelivery.security.PreAuthenticatedCertificatePrincipal;
import eu.europa.ec.edelivery.security.utils.X509CertificateUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.xbill.DNS.Record;

import javax.naming.Context;
import javax.naming.InitialContext;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.KeyStore;
import java.security.Principal;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.mockito.ArgumentMatchers.any;

/**
 * @author Joze Rihtarsic
 * @since 4.3
 */

@ExtendWith(SpringExtension.class)
@WebAppConfiguration
@ContextConfiguration(classes = {BDMSLWebAppConfig.class})
@DirtiesContext
@Sql(scripts = {"classpath:/init-test-database.sql"},
        executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
public abstract class AbstractJUnit5Test {

    static {
        initClassContext();
    }

    // issue with locking database in H2 199 and 200 on one of the bamboo CI
    private static final String CURRENT_DATABASE_FILE = "./target/mySMLDb-" + UUID.randomUUID();

    protected static DataSource dataSource;

    @Autowired
    protected ILoggingService loggingService;

    @Autowired
    protected IBDMSLService bdmslService;

    @Autowired
    protected IBDMSLMonitoringService bdmslMonitoringService;

    @Autowired
    public IConfigurationBusiness configurationBusiness;

    @Autowired
    protected ICertificateDomainBusiness certificateDomainBusiness;

    @Autowired
    protected IBDMSLAdminService bdmslAdminService;

    @Autowired
    protected IDnsMessageSenderService dnsMessageSenderService;

    @Autowired
    protected DnsClientServiceImpl dnsClientService;

    @Autowired
    protected SMLAuthenticationProvider smlAuthenticationProvider;

    @Autowired
    protected ITruststoreService truststoreService;

    protected static Path resourceDirectory = Paths.get("src", "test", "resources");
    protected static Path targetDirectory = Paths.get("target");


    protected ClientCertAuthenticationFilter blueCoatAuthenticationFilter = new ClientCertAuthenticationFilter();
    protected EDeliveryX509AuthenticationFilter eDeliveryX509AuthenticationFilter = new EDeliveryX509AuthenticationFilter();
    protected EDeliveryTokenAuthenticationFilter eDeliveryTokenAuthenticationFilter = new EDeliveryTokenAuthenticationFilter();


    @Autowired
    @Qualifier(value = "manageParticipantIdentifierServiceImpl")
    protected IManageParticipantIdentifierService manageParticipantIdentifierService;

    @Autowired
    protected ISubdomainDAO subdomainDAO;


    public static void initClassContext() {
        if (Security.getProvider("BC") == null) {
            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        }
    }

    /**
     * this method initialize / generates  (h2) database objects from entity annotations.
     * This is done this way because PropertiesConfig (implementation of PropertySourcesPlaceholderConfigurer)
     * class already expects initialized database in order to read properties from bdmsl_configuration table.
     */

    private static void initializeTestDatabase(DataSource datasource, String entityPackage) {
        // create database
        HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
        hibernateJpaVendorAdapter.setShowSql(true);
        hibernateJpaVendorAdapter.setGenerateDdl(true);

        LocalContainerEntityManagerFactoryBean lef = new LocalContainerEntityManagerFactoryBean();
        lef.setEntityManagerFactoryInterface(jakarta.persistence.EntityManagerFactory.class);

        lef.setDataSource(datasource);

        lef.setJpaVendorAdapter(hibernateJpaVendorAdapter);
        lef.getJpaPropertyMap().put("org.hibernate.envers.store_data_at_delete", true);
        lef.getJpaPropertyMap().put("org.hibernate.envers.audit_table_suffix", "_aud"); // use small caps - mysql issue
        lef.getJpaPropertyMap().put("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        lef.setPackagesToScan(entityPackage);
        lef.afterPropertiesSet();
        EntityManagerFactory enf = lef.getObject();
        // this triggers creation of database objects for all tests where just connection is used!
        EntityManager entityManager = enf.createEntityManager();
        entityManager.close();

    }


    @BeforeAll
    public static void classInitialization() throws IOException {
        initDatabase();
        initCryptography();
    }

    private static void initDatabase() {
        try {
            DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
            driverManagerDataSource.setDriverClassName("org.h2.Driver");
            driverManagerDataSource.setUrl("jdbc:h2:file:" + CURRENT_DATABASE_FILE + ";MODE=MySQL;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=TRUE;LOCK_TIMEOUT=10000;NON_KEYWORDS=VALUE");
            driverManagerDataSource.setUsername("sml-dev");
            driverManagerDataSource.setPassword("sml-dev");
            initializeTestDatabase(driverManagerDataSource, PropertyUtils.DAO_PACKAGE);

           // register datasource in mock JNDI env.
            System.setProperty(Context.INITIAL_CONTEXT_FACTORY, MockInitialContextFactory.class.getName());

            Hashtable<String, Object> ctxEnv = new Hashtable<>();
            Context ctx = new InitialContext(ctxEnv);
            ctx.bind("java:comp/env/jdbc/edelivery", driverManagerDataSource);


            // setup datasource
            dataSource = driverManagerDataSource;

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static void initCryptography() throws IOException {
        // copy test configuration files to target
        Files.copy(resourceDirectory.resolve("keystore.jks"), targetDirectory.resolve("keystore.jks"), StandardCopyOption.REPLACE_EXISTING);
        Files.copy(resourceDirectory.resolve("bdmsl-truststore.p12"), targetDirectory.resolve("bdmsl-truststore.p12"), StandardCopyOption.REPLACE_EXISTING);
        Files.copy(resourceDirectory.resolve("masterKey.key"), targetDirectory.resolve("masterKey.key"), StandardCopyOption.REPLACE_EXISTING);
        Files.copy(resourceDirectory.resolve("encryptedPasswordKey.key"), targetDirectory.resolve("encryptedPasswordKey.key"), StandardCopyOption.REPLACE_EXISTING);
    }

    @AfterAll
    public static void deleteDatabase() {
        (new File(CURRENT_DATABASE_FILE)).delete();
    }

    @BeforeEach
    protected void beforeTestInit() throws Exception {

        MockitoAnnotations.initMocks(this);
        dnsClientService = Mockito.spy(dnsClientService);
        ManageParticipantIdentifierServiceImpl manageParticipantIdentifierServiceImpl = CommonTestUtils.getTargetObject(manageParticipantIdentifierService);
        BDMSLServiceImpl bdmslServiceImpl = CommonTestUtils.getTargetObject(bdmslService);
        BDMSLMonitoringServiceImpl bdmslMonitoringServiceImpl = CommonTestUtils.getTargetObject(bdmslMonitoringService);

        configurationBusiness = Mockito.spy(configurationBusiness);
        certificateDomainBusiness = Mockito.spy(certificateDomainBusiness);
        Mockito.doReturn(false).when(configurationBusiness).isDNSEnabled();
        Mockito.doReturn(CommonTestUtils.ADMIN_HASHED_PASSWORD).when(configurationBusiness).getMonitorToken();
        Mockito.doReturn(targetDirectory.toFile().getAbsolutePath()).when(configurationBusiness).getConfigurationFolder();
        Mockito.doReturn(false).when(configurationBusiness).isCertRevocationValidationGraceful();

        ReflectionTestUtils.setField(manageParticipantIdentifierServiceImpl, "dnsClientService", dnsClientService);
        ReflectionTestUtils.setField(bdmslServiceImpl, "dnsClientService", dnsClientService);
        ReflectionTestUtils.setField(bdmslMonitoringServiceImpl, "dnsClientService", dnsClientService);

        ReflectionTestUtils.setField(bdmslAdminService, "configurationBusiness", configurationBusiness);
        ReflectionTestUtils.setField(certificateDomainBusiness, "configurationBusiness", configurationBusiness);
        ReflectionTestUtils.setField(smlAuthenticationProvider, "configurationBusiness", configurationBusiness);
        ReflectionTestUtils.setField(truststoreService, "configurationBusiness", configurationBusiness);
        ReflectionTestUtils.setField(smlAuthenticationProvider, "certificateDomainBusiness", certificateDomainBusiness);
        ReflectionTestUtils.setField(smlAuthenticationProvider, "truststoreService", truststoreService);

        Mockito.doReturn(new ArrayList<Record>()).when(dnsClientService).lookup(any(String.class), any(Integer.class));
        ((DnsMessageSenderServiceMock) dnsMessageSenderService).reset();
        configurationBusiness.forceRefreshProperties();
    }

    public void addCertificateToTruststore(X509Certificate certificate) throws TechnicalException {
        truststoreService.addCertificate(certificate, null);
    }

    public X509Certificate createX509CertificateKeystoreTrusted(String issuer, String subject, boolean trustedOnlyByIssuer) throws Exception {


        List<String> aliases = X509CertificateUtils.createAndStoreCertificateWithChain(new String[]{issuer, subject}, null,
                truststoreService.getTruststore(),
                configurationBusiness.getTruststorePassword());
        String leafAlias = aliases.get(aliases.size() - 1);

        // get leaf certificate
        X509Certificate certificate = truststoreService.getCertificate(leafAlias);

        if (trustedOnlyByIssuer) {
            truststoreService.deleteCertificate(leafAlias);
        }
        return certificate;
    }

    public Authentication createX509AuthenticateKeystoreTrusted(String issuer, String subject, boolean trustedOnlyByIssuer) throws Exception {

        X509Certificate certificate = createX509CertificateKeystoreTrusted(issuer, subject, trustedOnlyByIssuer);
        X509Certificate[] certificates = {certificate};
        HttpServletRequest mockedRequest = Mockito.mock(HttpServletRequest.class);
        Mockito.when(mockedRequest.getAttribute("jakarta.servlet.request.X509Certificate")).thenReturn(certificates);

        Principal principal = eDeliveryX509AuthenticationFilter.buildDetails(mockedRequest);
        return new CertificateAuthentication((PreAuthenticatedCertificatePrincipal) principal, Collections.emptyList());
    }


    public X509Certificate createX509CertificateKeystoreNotTrusted(String issuer, String subject) throws Exception {
        String password = "test123";
        KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
        ks.load(null, password.toCharArray());

        List<String> aliases = X509CertificateUtils.createAndStoreCertificateWithChain(new String[]{issuer, subject}, null,
                ks,
                password);
        String leafAlias = aliases.get(aliases.size() - 1);

        // get leaf certificate
        return (X509Certificate) ks.getCertificate(leafAlias);
    }

    public Authentication createX509AuthenticateNotTrusted(String issuer, String subject) throws Exception {

        // get leaf certificate
        X509Certificate certificate = createX509CertificateKeystoreNotTrusted(issuer, subject);
        X509Certificate[] certificates = {certificate};

        HttpServletRequest mockedRequest = Mockito.mock(HttpServletRequest.class);
        Mockito.when(mockedRequest.getAttribute("jakarta.servlet.request.X509Certificate")).thenReturn(certificates);


        Principal principal = eDeliveryX509AuthenticationFilter.buildDetails(mockedRequest);
        return new CertificateAuthentication((PreAuthenticatedCertificatePrincipal) principal, Collections.emptyList());
    }

    public CertificateDetails createDetails(PreAuthenticatedCertificatePrincipal principal) {
        CertificateDetails details = new CertificateDetails();
        details.setCertificateId(principal.getName(32));
        details.setSerial(principal.getCertSerial());
        details.setIssuer(principal.getIssuerDN());
        details.setRootCertificateDN(principal.getIssuerDN());
        details.setSubject(principal.getSubjectShortDN());
        details.setValidFrom(DateUtils.toCalendar(principal.getNotBefore()));
        details.setValidTo(DateUtils.toCalendar(principal.getNotAfter()));
        return details;
    }


    public Authentication createClientCertPrincipal(String issuer, String subject, String serialNumber) {
        DateFormat df = new SimpleDateFormat("MMM d hh:mm:ss yyyy zzz", Constant.LOCALE);
        Calendar validFrom = CommonTestUtils.getPastYearDate(2);
        Calendar validTo = CommonTestUtils.getFutureYearDate(5);
        String certHeaderValue = "serial=" + serialNumber + "&subject=" + subject + "&validFrom=" + df.format(validFrom.getTime()) + "&validTo=" + df.format(validTo.getTime()) + "&issuer=" + issuer;


        blueCoatAuthenticationFilter.setClientCertAuthenticationEnabled(true);
        Principal principal = blueCoatAuthenticationFilter.buildPrincipalFromHeader(certHeaderValue);
        return new CertificateAuthentication((PreAuthenticatedCertificatePrincipal) principal, Collections.emptyList());
    }


    public CertificateAuthentication createX509Authentication(X509Certificate[] certificates) throws Exception {

        HttpServletRequest mockedRequest = Mockito.mock(HttpServletRequest.class);
        Mockito.when(mockedRequest.getAttribute("jakarta.servlet.request.X509Certificate")).thenReturn(certificates);


        PreAuthenticatedCertificatePrincipal principal = eDeliveryX509AuthenticationFilter.buildDetails(mockedRequest);
        CertificateDomainBO domainBO;
        try {
            domainBO = certificateDomainBusiness.findDomain(createDetails(principal));
        } catch (CertificateNotFoundException e) {
            domainBO = null;
        }
        return new CertificateAuthentication(domainBO, principal, Collections.emptyList());
    }

    public Authentication createClientCertPrincipal(String issuer, String subject) throws Exception {
        return createClientCertPrincipal(issuer, subject, "123456789");
    }


}
