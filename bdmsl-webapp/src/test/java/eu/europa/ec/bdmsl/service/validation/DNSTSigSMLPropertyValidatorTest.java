/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.validation;

import eu.europa.ec.bdmsl.common.enums.SMLPropertyEnum;
import eu.europa.ec.bdmsl.common.exception.BadConfigurationException;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Base64;

import static org.junit.jupiter.api.Assertions.*;

class DNSTSigSMLPropertyValidatorTest {
    DNSTSigSMLPropertyValidator testInstance = new DNSTSigSMLPropertyValidator();

    @ParameterizedTest
    @CsvSource({
            "DNS_TSIG_HASH_ALGORITHM, true",
            "DNS_TSIG_KEY_NAME, true",
            "DNS_TSIG_KEY_NAME, true",
            "DNS_SIG0_ENABLED, false",
    })
    void supports(SMLPropertyEnum property, boolean expected) {
        boolean result = testInstance.supports(property);
        assertEquals(expected, result);
    }

    @ParameterizedTest
    @CsvSource({
            "DNS_TSIG_HASH_ALGORITHM, hmac-md5, true",
            "DNS_TSIG_HASH_ALGORITHM, invalid-algorithm, false",
            "DNS_TSIG_KEY_VALUE, dGVzdC1zdHJpbmc=, true",
            "DNS_TSIG_KEY_VALUE, dGVzdC1zdHJpbmc, true",
            "DNS_TSIG_KEY_VALUE, invalidBas.*-e64String, false"
    })
    void testValidate(SMLPropertyEnum property, String value, boolean expected) {
        System.out.println(Base64.getEncoder().encodeToString("test-string".getBytes()));
        if (expected) {
            assertDoesNotThrow(() -> testInstance.validate(property, value, null));
        } else {
            BadConfigurationException result = assertThrows(BadConfigurationException.class,
                    () -> testInstance.validate(property, value, null));
            MatcherAssert.assertThat(result.getMessage(), CoreMatchers.containsString("Property"));
        }
    }
}
