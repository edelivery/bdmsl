/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.test;

/**
 * Test constants for the integration testing with init sql script "/init-test-data.sql"
 *
 * @author Joze RIHTARSIC
 * @since 4.2
 */
public class TestConstants {

    public static final String DOMAIN_PEPPOL = "acc.edelivery.tech.ec.europa.eu";

    // CertificateBased authorization with certificate
    public static final String CERTIFICATE_01_ISSUER = "CN=SMP_XPTO2016112,O=DG-DIGIT,C=BE";
    public static final String CERTIFICATE_01_SUBJECT = "CN=createSmpOk,O=Subdomain-Entity-EU,C=BE";
    public static final String CERTIFICATE_01_SERIAL = "asdadf44878wqeadfs54";
    // SMP_1000000007
    public static final String CERTIFICATE_02_ISSUER = "C=DK, CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA, O=NATIONAL IT AND TELECOM AGENCY, OU=FOR TEST PURPOSES ONLY";
    public static final String CERTIFICATE_02_SUBJECT = "C=BE, O=DG-DIGIT, CN=SMP_1000000007";
    public static final String CERTIFICATE_02_SERIAL = "123ABCD";
    // SMP_DUMMY_123ABCD must have only https allowed
    public static final String CERTIFICATE_03_ISSUER = "C=DK, CN=TEST_LOGICAL_ADDRESS,O=DIGIT";
    public static final String CERTIFICATE_03_SUBJECT = "C=BE, O=DG-DIGIT, CN=SMP_DUMMY_123ABCD";
    public static final String CERTIFICATE_03_SERIAL = "123ABCD";
    // SMP_TEST_FOR_EDELIVERY_22595 -bad logical configuration
    public static final String CERTIFICATE_04_ISSUER = "CN=Issuer for SMP_TEST_FOR_EDELIVERY_22595,O=DG-DIGIT,C=BE";
    public static final String CERTIFICATE_04_SUBJECT = "CN=SMP_TEST_FOR_EDELIVERY_22595,O=DG-DIGIT,C=BE";
    public static final String CERTIFICATE_04_SERIAL = "22595";

    // SMP_TEST_FOR_EDELIVERY_22595 -bad logical configuration
    public static final String CERTIFICATE_05_ISSUER = "CN=Issuer for SMP_TEST_FOR_EDELIVERY_22591,O=DG-DIGIT,C=BE";
    public static final String CERTIFICATE_05_SUBJECT = "CN=SMP_TEST_FOR_EDELIVERY_22591,O=DG-DIGIT,C=BE";
    public static final String CERTIFICATE_05_SERIAL = "22591";
    //
    public static final String CERTIFICATE_06_ISSUER = "C=BE, O=DIGIT, OU=FOR TEST ONLY, CN=DIGIT_SMP_TECH_TEAM_2";
    public static final String CERTIFICATE_06_SUBJECT = "EMAILADDRESS=receiver@test.be,C=BE, O=DIGIT, OU=FOR TEST ONLY,CN=DIGIT_SMP_TECH_TEAM_3";
    public static final String CERTIFICATE_06_SERIAL = "48:b6:81:ee:8e:0d:cc:08";


    // SMP ids
    public static final String SMP_ID_ONLY_CNAME = "foundUnsecure";
    public static final String SMP_ID_ONLY_NAPTR = "SMP123456789";
    public static final String SMP_ID_DISABLED = "SMP-DISABLED";
    public static final String SMP_ID_001 = "SMP-TO-BE-FOUND";
    public static final String SMP_ID_002 = "SMP-TO-BE-DELETED";
    public static final String SMP_ID_003 = "SMP-TO-BE-UPDATED";


    public static final String SMP_LOGICAL_ADDRESS_001 = "http://logicalAddress";
    public static final String SMP_LOGICAL_ADDRESS_002 = "http://localhost";

    public static final String TRUSTSTORE_DEF_SMP_ALIAS = "smp_test_change (test intermediate issuer 01)";


    public static final int DB_SMP_COUNT = 33;
    public static final int DB_PARTICIPANT_COUNT = 34;
    public static final int DB_DISABLED_SMP_COUNT = 1;
    public static final int DB_DISABLED_PARTICIPANT_COUNT = 1;


}

