/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.security;

import eu.europa.ec.bdmsl.business.IConfigurationBusiness;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author Joze RIHTARSIC
 * @since 4.1
 */
class HttpCrlProxyDataServiceTest {

    IConfigurationBusiness mockConfiguration = Mockito.mock(IConfigurationBusiness.class);
    HttpCrlProxyDataService testInstance = new HttpCrlProxyDataService(mockConfiguration);

    @Test
    void getUsername() {
        //given
        String username = UUID.randomUUID().toString();
        Mockito.doReturn(username).when(mockConfiguration).getHttpProxyUsername();
        //when
        String value = testInstance.getUsername();
        // then
        Assertions.assertEquals(username, value);
    }

    @Test
    void getSecurityToken() {
        //given
        String password = UUID.randomUUID().toString();
        Mockito.doReturn(password).when(mockConfiguration).getHttpProxyPassword();
        //when
        String value = testInstance.getSecurityToken();
        // then
        Assertions.assertEquals(password, value);
    }

    @Test
    void getHttpProxyHost() {
        //given
        String host = UUID.randomUUID().toString();
        Mockito.doReturn(host).when(mockConfiguration).getHttpProxyHost();
        //when
        String value = testInstance.getHttpProxyHost();
        // then
        Assertions.assertEquals(host, value);
    }

    @Test
    void getHttpProxyPort() {
        //given
        int port = 87889;
        Mockito.doReturn(port).when(mockConfiguration).getHttpProxyPort();
        //when
        Integer value = testInstance.getHttpProxyPort();
        // then
        Assertions.assertNotNull(value);
        Assertions.assertEquals(port, value.intValue());
    }

    @Test
    void getHttpNoProxyHosts() {
        //given
        String noProxyHost = UUID.randomUUID().toString();
        Mockito.doReturn(noProxyHost).when(mockConfiguration).getHttpNoProxyHosts();
        //when
        String value = testInstance.getHttpNoProxyHosts();
        // then
        Assertions.assertEquals(noProxyHost, value);
    }

    @Test
    void isProxyEnabledTrue() {
        testIsProxyEnabledTruePrivate(true);
    }

    @Test
    void isProxyEnabledFalse() {
        testIsProxyEnabledTruePrivate(false);
    }

    void testIsProxyEnabledTruePrivate(boolean isProxyEnabled) {
        //given
        Mockito.doReturn(isProxyEnabled).when(mockConfiguration).isProxyEnabled();
        //when
        boolean value = testInstance.isProxyEnabled();
        // then
        Assertions.assertEquals(isProxyEnabled, value);
    }

    @Test
    void testGetCertRevocationValidationAllowedUrlProtocols() {
        //given
        List<String> proxyProtocols = Arrays.asList("http", "https", "file");
        Mockito.doReturn(proxyProtocols).when(mockConfiguration).getCertRevocationValidationAllowedUrlProtocols();
        //when
        List<String> value = testInstance.getAllowedURLProtocols();
        // then
        Assertions.assertArrayEquals(proxyProtocols.toArray(), value.toArray());
    }
}
