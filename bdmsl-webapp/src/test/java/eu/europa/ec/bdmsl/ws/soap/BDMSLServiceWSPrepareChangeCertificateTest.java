/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2017 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.ws.soap;

import ec.services.wsdl.bdmsl.data._1.PrepareChangeCertificate;
import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.enums.SMLRoleEnum;
import eu.europa.ec.bdmsl.common.util.Constant;
import eu.europa.ec.bdmsl.dao.ICertificateDAO;
import eu.europa.ec.bdmsl.test.CommonTestUtils;
import eu.europa.ec.bdmsl.security.CertificateAuthentication;
import eu.europa.ec.bdmsl.security.ICertificateAuthentication;
import eu.europa.ec.bdmsl.security.UnsecureAuthentication;
import eu.europa.ec.edelivery.security.PreAuthenticatedCertificatePrincipal;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import jakarta.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Adrien FERIAL
 * @since 09/07/2015
 */
class BDMSLServiceWSPrepareChangeCertificateTest extends AbstractJUnit5Test {

    @Autowired
    private IBDMSLServiceWS bdmslServiceWS;

    @Autowired
    private ICertificateDAO certificateDAO;

    @Autowired
    private ICertificateAuthentication customAuthentication;


    private String newCertPublicKey = "-----BEGIN CERTIFICATE-----\n" +
            "MIICpTCCAg6gAwIBAgIBATANBgkqhkiG9w0BAQUFADB4MQswCQYDVQQGEwJCRTEL\n" +
            "MAkGA1UECAwCQkUxETAPBgNVBAcMCEJydXNzZWxzMQ4wDAYDVQQKDAVESUdJVDEL\n" +
            "MAkGA1UECwwCQjQxDzANBgNVBAMMBnJvb3RDTjEbMBkGCSqGSIb3DQEJARYMcm9v\n" +
            "dEB0ZXN0LmJlMB4XDTE1MDMxNzE2MTkwN1oXDTI1MDMxNDE2MTkwN1owfDELMAkG\n" +
            "A1UEBhMCQkUxCzAJBgNVBAgMAkJFMREwDwYDVQQHDAhCcnVzc2VsczEOMAwGA1UE\n" +
            "CgwFRElHSVQxCzAJBgNVBAsMAkI0MREwDwYDVQQDDAhzZW5kZXJDTjEdMBsGCSqG\n" +
            "SIb3DQEJARYOc2VuZGVyQHRlc3QuYmUwgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJ\n" +
            "AoGBANxLUPjIn7R0CsHf86kIwNzCu+6AdmWM8fBLUHL+VXT6ayr1kwgGbFMb/vUU\n" +
            "X6a46jRCiZBM+9IK1Hpjg9QX/QIQiWtvD+yDr6jUxahZ/w13kqFG/K81IVu9DwLB\n" +
            "oiNwDvQ6l6UbvMvV+1nWy3gjRcKlFs/C+E2uybgJxSM/sMkbAgMBAAGjOzA5MB8G\n" +
            "A1UdIwQYMBaAFHCVSh4WnWR8MGBGedr+bJH96tc4MAkGA1UdEwQCMAAwCwYDVR0P\n" +
            "BAQDAgTwMA0GCSqGSIb3DQEBBQUAA4GBAK6idNRxyeBmqPoSKxq7Ck3ej6R2QPyW\n" +
            "bwZ+6/S7iCRt8PfgOu++Yu5YEjlUX1hlkbQKF/JuKTLqxNnKIE6Ef65+JP2ZaI9O\n" +
            "2wdzpRclAhAd00XbNKpyipr4jMdWmu2U8vyBBwn/utG1ZrLhAUiqnPvmaQrResiG\n" +
            "HM2xzCmVwtse\n" +
            "-----END CERTIFICATE-----\n";

    private String newRootCAKeyExample = "-----BEGIN CERTIFICATE-----\n" +
            "MIIDiDCCAnCgAwIBAgIIUmOibrDR2Z4wDQYJKoZIhvcNAQELBQAweDELMAkGA1UE\n" +
            "BhMCQkUxCzAJBgNVBAgTAkJFMREwDwYDVQQHEwhCcnVzc2VsczEOMAwGA1UEChMF\n" +
            "RElHSVQxCzAJBgNVBAsTAkI0MQ8wDQYDVQQDEwZyb290Q04xGzAZBgkqhkiG9w0B\n" +
            "CQEWDHJvb3RAdGVzdC5iZTAeFw0yMzA2MjkwMDAwMDBaFw0zMzA2MjgyMzU5NTla\n" +
            "MIGAMQswCQYDVQQGEwJCRTELMAkGA1UECBMCQkUxETAPBgNVBAcTCEJydXNzZWxz\n" +
            "MQ4wDAYDVQQKEwVESUdJVDELMAkGA1UECxMCQjQxFTATBgNVBAMMDFNNUF9zZW5k\n" +
            "ZXJDTjEdMBsGCSqGSIb3DQEJARYOc2VuZGVyQHRlc3QuYmUwggEiMA0GCSqGSIb3\n" +
            "DQEBAQUAA4IBDwAwggEKAoIBAQDmS0tbSrJ04mUa3oAkkbQeYin10I3KkTUWZlse\n" +
            "k5+q/OT3L3sXIr7KwGLMqVXcsUcOmYLGWPzD6BY7lej3ypYXTU99wCZQy07MGEF1\n" +
            "9uyl0u12Qt9eA7gwF7vhB3CwrXmU2Q/LGJu5R0brVuZzpGlgFMEwcjZUFB7uBbMl\n" +
            "IoF+v76Er3+l8BjwFseRS7G1zJnerISOOsEZkRnYXQFhT07wjsnpKURuldtuYfdW\n" +
            "4JXjSlSrygLavOPRfp65vSBZB3T75qS2RHjqLpgimpyhvvMgoKJEdy3BFNiLlmYX\n" +
            "tORqOXDZpV9u7VMDKer1igIsTx6d7LQhThojI3T25zXtMbq9AgMBAAGjDTALMAkG\n" +
            "A1UdEwQCMAAwDQYJKoZIhvcNAQELBQADggEBACnk86TLSDU2Ggy8SBSDy+J2z3cx\n" +
            "cMDIFBLYaOaT6B2YPg+4hDIF1f/qZJldEjqf248yBlajn/Hv/2FRNwrJpeH6COfU\n" +
            "AaKAo1XPwqt2AC2DfAkU6unAPNOXA6QWCvetx0N5ELhSqf6fEy5O+1efa5ZK+IF+\n" +
            "n1Yg1MXtPNztkNjQ0wIM6g/VaptQS5gxzJ+mDDfHx8Nzk+2BcExseUmwBd4ywMVy\n" +
            "x/PoYH0Mznzb9dtvYtKM+5SKQ+JN4ZHmDzkhED5hk2++fH95oYP/9oOnyuaB0cy+\n" +
            "cg0ml38SxLururtnYq4JzBpmUhKX/8NhQSjVyL+Ab3zlklDsqzynYa9RwA8=\n" +
            "-----END CERTIFICATE-----\n";


    private String nonRootCAPEM = "-----BEGIN CERTIFICATE-----\n" +
            "MIICozCCAgygAwIBAgIEWRxDLjANBgkqhkiG9w0BAQsFADCBlTExMC8GCSqGSIb3\n" +
            "DQEJARYiQ0VGLUVERUxJVkVSWS1TVVBQT1JUQGVjLmV1cm9wYS5ldTELMAkGA1UE\n" +
            "BhMCQkUxCzAJBgNVBAgMAkJFMQ4wDAYDVQQKDAVESUdJVDEWMBQGA1UECwwNRk9S\n" +
            "IFRFU1QgT05MWTEeMBwGA1UEAwwVRElHSVRfU01QX1RFQ0hfVEVBTV8yMB4XDTE3\n" +
            "MDUxNzEyMzQzM1oXDTE4MDUxNzEyMzQzM1owgZUxMTAvBgkqhkiG9w0BCQEWIkNF\n" +
            "Ri1FREVMSVZFUlktU1VQUE9SVEBlYy5ldXJvcGEuZXUxCzAJBgNVBAYTAkJFMQsw\n" +
            "CQYDVQQIDAJCRTEOMAwGA1UECgwFRElHSVQxFjAUBgNVBAsMDUZPUiBURVNUIE9O\n" +
            "TFkxHjAcBgNVBAMMFURJR0lUX1NNUF9URUNIX1RFQU1fMjCBnzANBgkqhkiG9w0B\n" +
            "AQEFAAOBjQAwgYkCgYEAvIVWP/Vme4H27WiR5HnFDlRMkRmI9dD8uTq/VPixYK9Y\n" +
            "OQO6CmJllC2whZ8rM28pFKU2utQ7SoAbixbqPaMEyZ5/dt146i6DJddj/R9Fx5iW\n" +
            "01BQy6XVZMgkGerhxTmqFRWsQP3x6BASKsaMxlnK/36P4KJP/z/A4VMUH1sO8KcC\n" +
            "AwEAATANBgkqhkiG9w0BAQsFAAOBgQC73loeyRarbpC/bgqKTQsSNcbhLsSIgLQK\n" +
            "Dz+DA0TbOTEdcNvshcVL63RiO1JQDWIeVvOzGIIYlNsI4oIg2uvGlcgJH1dpQtxS\n" +
            "PVhpZqulko6CwNOQVFQtbGcABfHsP4Gik4xTXXK2KG1FUlvA1wr+G6QDFqw9mN0Y\n" +
            "df7cxLFokA==\n" +
            "-----END CERTIFICATE-----";
    //
    String certificateId = "CN=SMP_TEST_CHANGE_CERTIFICATE_6,O=DG-DIGIT,C=BE:00000000000000000000000001234567";
    String serial = "0000000001234567";
    String issuer = "CN=rootCN,O=DIGIT,C=BE";
    String subject = "CN=SMP_TEST_CHANGE_CERTIFICATE_6,O=DG-DIGIT,C=BE";


    @BeforeEach
    void setup() {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        DateFormat df = new SimpleDateFormat("MMM d hh:mm:ss yyyy zzz", Constant.LOCALE);

        Calendar validFrom = CommonTestUtils.getPastYearDate(2);
        Calendar validTo = CommonTestUtils.getFutureYearDate(5);

        String certHeaderValue = "serial=" + serial + "&subject=" + subject + "&validFrom=" + df.format(validFrom.getTime()) + "&validTo=" + df.format(validTo.getTime()) + "&issuer=" + issuer;
        PreAuthenticatedCertificatePrincipal principal = blueCoatAuthenticationFilter.buildPrincipalFromHeader(certHeaderValue);
        Authentication bcAuth = new CertificateAuthentication(principal,
                Collections.singletonList(SMLRoleEnum.ROLE_SMP.getAuthority()));
        bcAuth.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(bcAuth);
    }

    @Test
    void prepareChangeCertificateOk() throws Exception {
        assertNull(certificateDAO.findCertificateByCertificateId(certificateId).getMigrationDate());
        assertNull(certificateDAO.findCertificateByCertificateId(certificateId).getNewCertificateId());

        PrepareChangeCertificate preparePrepareChangeCertificate = new PrepareChangeCertificate();
        preparePrepareChangeCertificate.setNewCertificatePublicKey(newRootCAKeyExample);
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR) + 1, 1, 1);
        preparePrepareChangeCertificate.setMigrationDate(cal);
        bdmslServiceWS.prepareChangeCertificate(preparePrepareChangeCertificate);

        assertTrue(DateUtils.isSameDay(cal, certificateDAO.findCertificateByCertificateId(certificateId).getMigrationDate()));
        assertNotNull(certificateDAO.findCertificateByCertificateId(certificateId).getNewCertificateId());
    }

    @Test
    void prepareChangeCertificateWithItSelf() throws Exception {
        assertNull(certificateDAO.findCertificateByCertificateId(certificateId).getMigrationDate());
        assertNull(certificateDAO.findCertificateByCertificateId(certificateId).getNewCertificateId());
        // get the certificate
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR) + 10, 1, 1);
        X509Certificate cert = CommonTestUtils.createCertificate(serial, issuer, subject, Calendar.getInstance().getTime(), cal.getTime(), false);

        PrepareChangeCertificate preparePrepareChangeCertificate = new PrepareChangeCertificate();
        preparePrepareChangeCertificate.setNewCertificatePublicKey(Base64.getEncoder().encodeToString(cert.getEncoded()));
        Calendar calMigrate = Calendar.getInstance();
        calMigrate.set(cal.get(Calendar.YEAR) - 2, 1, 1);
        preparePrepareChangeCertificate.setMigrationDate(calMigrate);
        assertThrows(BadRequestFault.class, () -> {
            bdmslServiceWS.prepareChangeCertificate(preparePrepareChangeCertificate);
            assertTrue(DateUtils.isSameDay(cal, certificateDAO.findCertificateByCertificateId(certificateId).getMigrationDate()));
            assertNotNull(certificateDAO.findCertificateByCertificateId(certificateId).getNewCertificateId());
        });


    }


    @Test
    void prepareChangeCertificateNull() {
        assertThrows(BadRequestFault.class, () -> bdmslServiceWS.prepareChangeCertificate(null));
    }

    @Test
    void prepareChangeCertificateEmpty() {
        assertThrows(BadRequestFault.class, () -> bdmslServiceWS.prepareChangeCertificate(new PrepareChangeCertificate()));
    }

    /**
     * If the migrationDate is missing, then the "Valid From" date is extracted from the certificate and is used as the migrationDate.
     *
     */
    @Test
    void prepareChangeCertificateMissingMigrationDate() {
        PrepareChangeCertificate preparePrepareChangeCertificate = new PrepareChangeCertificate();
        preparePrepareChangeCertificate.setNewCertificatePublicKey(newCertPublicKey);
        // The "not before" date of the certificate is in March 2015: it's in the past so an exception is excepted
        assertThrows(BadRequestFault.class, () -> bdmslServiceWS.prepareChangeCertificate(preparePrepareChangeCertificate));
    }

    @Test
    void prepareChangeCertificateMigrationDateInThePast() {
        PrepareChangeCertificate preparePrepareChangeCertificate = new PrepareChangeCertificate();
        preparePrepareChangeCertificate.setNewCertificatePublicKey(newCertPublicKey);
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2010);
        preparePrepareChangeCertificate.setMigrationDate(cal);
        assertThrows(BadRequestFault.class, () -> bdmslServiceWS.prepareChangeCertificate(preparePrepareChangeCertificate));
    }

    @Test
    void prepareChangeCertificateNewCertFormatInvalid() {
        PrepareChangeCertificate preparePrepareChangeCertificate = new PrepareChangeCertificate();
        preparePrepareChangeCertificate.setNewCertificatePublicKey("InvalidFormat");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, 1);
        preparePrepareChangeCertificate.setMigrationDate(cal);
        assertThrows(BadRequestFault.class, () -> bdmslServiceWS.prepareChangeCertificate(preparePrepareChangeCertificate));
    }

    @Test
    void prepareChangeCertificateNewCertNotTrusted() throws Exception {
        String subject = "E=test@ext.ec.europe.eu,CN=Edelivery-NotTrusted,OU=DIGIT,O=EU,L=Brussels,ST=Brussel,C=BE";
        String serial = "10494798188523090317";
        X509Certificate certificate = CommonTestUtils.createCertificate(serial, subject, subject, false);
        String base64String = Base64.getEncoder().encodeToString(certificate.getEncoded());

        PrepareChangeCertificate preparePrepareChangeCertificate = new PrepareChangeCertificate();
        preparePrepareChangeCertificate.setNewCertificatePublicKey(base64String);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, 1);
        preparePrepareChangeCertificate.setMigrationDate(cal);
        assertThrows(UnauthorizedFault.class, () -> bdmslServiceWS.prepareChangeCertificate(preparePrepareChangeCertificate));
    }

    @Test
    void prepareChangeCertificateMigrationDateNotWithinValidFromAndValidToDatesFromOfNewCertificate() throws Exception {
        PrepareChangeCertificate preparePrepareChangeCertificate = new PrepareChangeCertificate();
        preparePrepareChangeCertificate.setNewCertificatePublicKey(newCertPublicKey);
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR) + 25, 1, 1);
        preparePrepareChangeCertificate.setMigrationDate(cal);
        assertThrows(BadRequestFault.class, () -> bdmslServiceWS.prepareChangeCertificate(preparePrepareChangeCertificate));
    }

    @Test
    void prepareChangeCertificateCertNotFound() throws Exception {
        String serial = "0000000000000000000000000123ABCD";
        String issuer = "CN=rootCN,OU=B4,O=DIGIT,L=Brussels,ST=BE,C=BE";
        String subject = "CN=SMP_TEST_CHANGE_CERTIFICATE_NOT_FOUND,O=DG-DIGIT,C=BE";
        DateFormat df = new SimpleDateFormat("MMM d hh:mm:ss yyyy zzz", Constant.LOCALE);

        Calendar validFrom = CommonTestUtils.getPastYearDate(3);
        Calendar validTo = CommonTestUtils.getFutureYearDate(5);

        String certHeaderValue = "serial=" + serial + "&subject=" + subject + "&validFrom=" + df.format(validFrom.getTime()) + "&validTo=" + df.format(validTo.getTime()) + "&issuer=" + issuer;

        Principal principal = blueCoatAuthenticationFilter.buildPrincipalFromHeader(certHeaderValue);
        Authentication bcAuth = new CertificateAuthentication((PreAuthenticatedCertificatePrincipal) principal,
                Collections.singletonList(SMLRoleEnum.ROLE_SMP.getAuthority()));
        bcAuth.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(bcAuth);
        PrepareChangeCertificate preparePrepareChangeCertificate = new PrepareChangeCertificate();
        preparePrepareChangeCertificate.setNewCertificatePublicKey(newCertPublicKey);
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR) + 5, 1, 1);
        preparePrepareChangeCertificate.setMigrationDate(cal);
        assertThrows(UnauthorizedFault.class, () -> bdmslServiceWS.prepareChangeCertificate(preparePrepareChangeCertificate));
    }

    @Test
    void prepareChangeCertificateForNonRootCAOk() throws Exception {
        String newCertificateSubject = "CN=EHEALTH_SMP_PREPARE_CHANGE,O= European Commission,C=EU";
        String subject = "CN=SMP_123456789101112,O=DG-DIGIT,C=BE";
        String issuer = "CN=rootCN_non_root,OU=B4,O=DIGIT,L=Brussels,ST=BE,C=BE";
        customAuthentication.blueCoatAuthentication("123456789101112", issuer, subject, false);
        Calendar validFrom = Calendar.getInstance();
        validFrom.set(validFrom.get(Calendar.YEAR) - 5, 1, 1);

        Calendar validTo = Calendar.getInstance();
        validTo.set(validTo.get(Calendar.YEAR) + 5, 1, 1);

        X509Certificate cert = CommonTestUtils.createCertificate(newCertificateSubject, newCertificateSubject, false);
        String newCertString = Base64.getEncoder().encodeToString(cert.getEncoded());

        String certificateId = "CN=SMP_123456789101112,O=DG-DIGIT,C=BE:00000000000000000123456789101112";
        assertNull(certificateDAO.findCertificateByCertificateId(certificateId).getMigrationDate());
        assertNull(certificateDAO.findCertificateByCertificateId(certificateId).getNewCertificateId());

        PrepareChangeCertificate preparePrepareChangeCertificate = new PrepareChangeCertificate();
        preparePrepareChangeCertificate.setNewCertificatePublicKey(newCertString);

        Calendar migrateDate = Calendar.getInstance();
        migrateDate.set(migrateDate.get(Calendar.YEAR), migrateDate.get(Calendar.MONTH) + 1, 1);
        preparePrepareChangeCertificate.setMigrationDate(migrateDate);
        bdmslServiceWS.prepareChangeCertificate(preparePrepareChangeCertificate);

        assertTrue(DateUtils.isSameDay(migrateDate, certificateDAO.findCertificateByCertificateId(certificateId).getMigrationDate()));
        assertNotNull(certificateDAO.findCertificateByCertificateId(certificateId).getNewCertificateId());
    }

    @Test
    void testPrepareChangeCertificateForNonRootCADomainByAdmin() throws Exception {
        //GIVEN
        String serial = "0000000001234567";
        String issuer = "CN=rootCN,OU=B4,O=DIGIT,L=Brussels,ST=BE,C=BE";
        String subject = "CN=SMP_TEST_CHANGE_CERTIFICATE_6,O=DG-DIGIT,C=BE";
        DateFormat df = new SimpleDateFormat("MMM d hh:mm:ss yyyy zzz", Constant.LOCALE);

        Calendar validFrom = CommonTestUtils.getPastYearDate(2);
        Calendar validTo = CommonTestUtils.getFutureYearDate(5);
        String certHeaderValue = "serial=" + serial + "&subject=" + subject + "&validFrom=" + df.format(validFrom.getTime()) + "&validTo=" + df.format(validTo.getTime()) + "&issuer=" + issuer;
        PreAuthenticatedCertificatePrincipal principal = blueCoatAuthenticationFilter.buildPrincipalFromHeader(certHeaderValue);
        Authentication bcAuth = new CertificateAuthentication(principal,
                Collections.singletonList(SMLRoleEnum.ROLE_ADMIN.getAuthority()));
        bcAuth.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(bcAuth);


        //WHEN-THEN
        PrepareChangeCertificate preparePrepareChangeCertificate = new PrepareChangeCertificate();
        preparePrepareChangeCertificate.setNewCertificatePublicKey(newCertPublicKey);
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR) + 5, 1, 1);
        preparePrepareChangeCertificate.setMigrationDate(cal);
        assertThrows(UnauthorizedFault.class, () -> bdmslServiceWS.prepareChangeCertificate(preparePrepareChangeCertificate));
    }

    @Test
    void testPrepareChangeCertificateForRootCADomainByUnsecuredUser() {
        //GIVEN Unsecured is issuer based authentication
        UnsecureAuthentication authentication = new UnsecureAuthentication();
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        //WHEN-THEN
        PrepareChangeCertificate preparePrepareChangeCertificate = new PrepareChangeCertificate();
        preparePrepareChangeCertificate.setNewCertificatePublicKey(newRootCAKeyExample);
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR) + 1, 1, 1);
        preparePrepareChangeCertificate.setMigrationDate(cal);
        assertDoesNotThrow(() -> bdmslServiceWS.prepareChangeCertificate(preparePrepareChangeCertificate));
        ;
    }

    @Test
    void testPrepareChangeCertificateForNonRootCADomainByX509Certificate() throws Exception {
        //GIVEN
        //Generating a dummy certificate for authentication
        X509Certificate certificate = CommonTestUtils.createCertificate("CN=EHEALTH_SMP_ISSUER,O=DG-DIGIT,C=BE,E=CEF-EDELIVERY-SUPPORT@ec.europa.eu",
                "CN=EHEALTH_SMP_1000000200,O=DG-DIGIT,C=BE");
        X509Certificate[] certificates = {certificate};

        //Injecting the dummy certificate as an authenticated user
        Authentication authentication = createX509Authentication(certificates);
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        //WHEN-THEN
        PrepareChangeCertificate preparePrepareChangeCertificate = new PrepareChangeCertificate();
        preparePrepareChangeCertificate.setNewCertificatePublicKey(nonRootCAPEM);
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR) + 5, 1, 1);
        preparePrepareChangeCertificate.setMigrationDate(cal);
        assertThrows(UnauthorizedFault.class, () -> bdmslServiceWS.prepareChangeCertificate(preparePrepareChangeCertificate));
    }

    @Test
    void testPrepareChangeCertificateForNonRootCADomainByHeaderCertificate() throws Exception {
        //GIVEN
        //Generating a dummy certificate for authentication
        String certHeaderValue = "serial=6caa15ffed56ad049dd2b9bc835d2e46&subject=CN=EHEALTH_SMP_100000100,O=DG-DIGIT,C=BE&validfrom=Feb  1 14:20:18 2017 GMT&validto=Jul  9 23:59:00 2599 GMT&issuer=CN=EHEALTH_SMP_ISSUER,O=DG-DIGIT,C=BE,E=CEF-EDELIVERY-SUPPORT@ec.europa.eu";
        Principal principal = blueCoatAuthenticationFilter.buildPrincipalFromHeader(certHeaderValue);
        CertificateAuthentication result = new CertificateAuthentication((PreAuthenticatedCertificatePrincipal) principal, Arrays.asList(SMLRoleEnum.ROLE_SMP.getAuthority()));
        result.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(result);

        //WHEN-THEN
        PrepareChangeCertificate preparePrepareChangeCertificate = new PrepareChangeCertificate();
        preparePrepareChangeCertificate.setNewCertificatePublicKey(nonRootCAPEM);
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR) + 5, 1, 1);
        preparePrepareChangeCertificate.setMigrationDate(cal);
        assertThrows(UnauthorizedFault.class, () -> bdmslServiceWS.prepareChangeCertificate(preparePrepareChangeCertificate));
    }

    public CertificateAuthentication createX509Authentication(X509Certificate[] certificates) throws Exception {

        HttpServletRequest mockedRequest = Mockito.mock(HttpServletRequest.class);
        Mockito.when(mockedRequest.getAttribute("jakarta.servlet.request.X509Certificate")).thenReturn(certificates);


        Principal principal = eDeliveryX509AuthenticationFilter.buildDetails(mockedRequest);
        return new CertificateAuthentication((PreAuthenticatedCertificatePrincipal) principal, Collections.emptyList());
    }
}
