/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.ws.soap;

import ec.services.wsdl.bdmsl.admin.data._1.ManageParticipantIdentifierResponse;
import ec.services.wsdl.bdmsl.admin.data._1.ManageParticipantIdentifierType;
import ec.services.wsdl.bdmsl.admin.data._1.ManageServiceMetadataPublisherResponse;
import ec.services.wsdl.bdmsl.admin.data._1.ManageServiceMetadataPublisherType;
import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.enums.SMLRoleEnum;
import eu.europa.ec.bdmsl.test.SecurityTestUtils;
import org.busdox.transport.identifiers._1.ParticipantIdentifierType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.UUID;

import static eu.europa.ec.bdmsl.util.DomiSMLAssertions.assertThrowsWithContainsMessage;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Test class for BDMSLAdminServiceWS to test Managing the  Publisher
 * and its resource/participant identifiers.
 *
 * @author Joze RIHTARSIC
 * @since 5.0
 */
class BDMSLAdminServiceWSManagePublisherAndIdentifiersTest extends AbstractJUnit5Test {

    @Autowired
    private IBDMSLAdminServiceWS testInstance;

    @Autowired
    private ThreadPoolTaskExecutor smlReportTaskExecutor;


    @BeforeEach
    void before() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        SecurityTestUtils.authenticateWithRoleClientCert06(SMLRoleEnum.ROLE_ADMIN);
    }

    @AfterEach
    void after() {
        SecurityContextHolder.clearContext();
    }

    @ParameterizedTest
    @CsvSource({
            "false, '[ERR-106] The input values must not be null!'",
            "true, '[ERR-106] Unknown action: [null]. Action names are: [DELETE, ENABLE, DISABLE, UPDATE, SYNC_DNS]'"
    })
    void testManageServiceMetadataPublisherNull(boolean isEmpty, String errorMessage) {
        // given
        ManageServiceMetadataPublisherType value = isEmpty ?
                new ManageServiceMetadataPublisherType() : null;
        // when
        assertThrowsWithContainsMessage(BadRequestFault.class,
                () -> testInstance.manageServiceMetadataPublisher(value),
                errorMessage);
    }

    @ParameterizedTest
    @CsvSource({"'',[ERR-106] Unknown [Action]: [null].Valid values are: [CREATE, DELETE] "
            , ",[ERR-106] The input values must not be null!"})
    void testManageParticipantIdentifierNull(String parameter, String message) {
        // given
        ManageParticipantIdentifierType value = parameter == null ?
                null : new ManageParticipantIdentifierType();
        // when-then
        assertThrowsWithContainsMessage(BadRequestFault.class,
                () -> testInstance.manageParticipantIdentifier(value),
                message);
    }


    @ParameterizedTest
    @CsvSource({
            "'','', '', '', '', '', '', '', '[ERR-106] The input values must not be null!'",
            "DELETE, '', '', '', '', '', '', '[ERR-106] Email address [] is not valid!'",
            "DELETE, 'test', '', '', '', '', '', '[ERR-106] Email address [test] is not valid!'",
            "DELETE, 'test@test.local', '', '', '', '', '', '[ERR-106] Publisher ID: [] does not exist!'",
            "DELETE, 'test@test.local', 'test', '', '', '', '', '[ERR-106] Publisher ID: [test] does not exist!'",
            "DELETE, 'test@test.local', 'SMP123456789', '', '', '', '', 'ERR-106] Publisher ID: [SMP123456789] is not bound to certificateId: []!'",
            "DELETE, 'test@test.local', 'SMP123456789', 'CN=SMP_123456789,O=DG-DIGIT,C=BE:00000000000000000000000123456789', '', '', '', '', '[ERR-106] Publisher ID: [smp-sample] is not bound to certificateId: []!'",
            "DELETE, 'test@test.local', 'SMP123456789', 'CN=SMP_123456789,O=DG-DIGIT,C=BE:00000000000000000000000123456789', 'test', '', '', '[ERR-106] Publisher ID: [SMP123456789] is not registered to domain zone: [test]!'",
            "DELETE, 'test@test.local', 'SMP123456789', 'CN=SMP_123456789,O=DG-DIGIT,C=BE:00000000000000000000000123456789', 'ehealth.acc.edelivery.tech.ec.europa.eu', '', '', '[SMP123456789] is not disabled and can not be deleted!'",
            "UPDATE, 'test@test.local', 'SMP123456789', 'CN=SMP_123456789,O=DG-DIGIT,C=BE:00000000000000000000000123456789', 'ehealth.acc.edelivery.tech.ec.europa.eu', 'test', '', 'Logical address is invalid [test].'",
            "ENABLE, 'test@test.local', 'SMP123456789', 'CN=SMP_123456789,O=DG-DIGIT,C=BE:00000000000000000000000123456789', 'ehealth.acc.edelivery.tech.ec.europa.eu', 'http://test/smp', '', '[ERR-106] Publisher ID: [SMP123456789] is already enabled'",
            "DELETE, 'test@test.local', 'SMP123456789', 'CN=SMP_123456789,O=DG-DIGIT,C=BE:00000000000000000000000123456789', 'ehealth.acc.edelivery.tech.ec.europa.eu', 'http://test/smp', '', '[ERR-106] Publisher ID: [SMP123456789] is not disabled and can not be deleted!'",
    })
    void testManageServiceMetadataPublisherFail(String action, String recipientEmailAddress, String serviceMetadataPublisherId,
                                                String certificateOwnerId, String domainZone,
                                                String logicalAddress, String physicalAddress, String message) throws BadRequestFault, UnauthorizedFault, InternalErrorFault {
        // given
        ManageServiceMetadataPublisherType value = buildManageServiceMetadataPublisherType(action, recipientEmailAddress, serviceMetadataPublisherId,
                domainZone, certificateOwnerId, logicalAddress, physicalAddress);
        // when-then
        assertThrowsWithContainsMessage(BadRequestFault.class,
                () -> testInstance.manageServiceMetadataPublisher(value),
                message);
    }


    @ParameterizedTest
    @CsvSource({
            "UPDATE, 'test@test.local', 'SMP123456789', 'CN=SMP_123456789,O=DG-DIGIT,C=BE:00000000000000000000000123456789', 'ehealth.acc.edelivery.tech.ec.europa.eu', 'http://test/smp', '', 'Start executing the smp management task [UPDATE]!'",
            "DISABLE, 'test@test.local', 'SMP123456789', 'CN=SMP_123456789,O=DG-DIGIT,C=BE:00000000000000000000000123456789', 'ehealth.acc.edelivery.tech.ec.europa.eu', 'http://test/smp', '', 'Start executing the smp management task [DISABLE]!'",
    })
    void testManageServiceMetadataPublisherSuccess(String action, String recipientEmailAddress, String serviceMetadataPublisherId,
                                                   String certificateOwnerId, String domainZone,
                                                   String logicalAddress, String physicalAddress, String message) throws BadRequestFault, UnauthorizedFault, InternalErrorFault, InterruptedException {


        if (smlReportTaskExecutor.getActiveCount() !=0) {
            // wait for the task to finish
            Thread.sleep(1000);
        }

        // given
        // change user to wrong one. This is needed to test the exception
        SecurityTestUtils.authenticateWithRoleClientCert06(SMLRoleEnum.ROLE_ADMIN);
        ManageServiceMetadataPublisherType value = buildManageServiceMetadataPublisherType(action, recipientEmailAddress, serviceMetadataPublisherId,
                domainZone, certificateOwnerId, logicalAddress, physicalAddress);

        // when-then
        ManageServiceMetadataPublisherResponse response = Assertions.assertDoesNotThrow(() -> testInstance.manageServiceMetadataPublisher(value));
        assertNotNull(response);
        assertEquals(message, response.getStatus());
    }

    @ParameterizedTest
    @CsvSource({"ROLE_ADMIN, BadRequestFault, CREATE, '', '', '', [ERR-106] Parameter [ServiceMetadataPublisherID] cannot be 'null' or empty!",
            "ROLE_ADMIN, NotFoundFault, CREATE, 'test', '', '', [ERR-100] The Publisher identifier [test] doesn't exist",
            "ROLE_ADMIN, BadRequestFault, CREATE, 'SMP123456789', 'test', '',[ERR-106] Identifier value test is illegal",
            "ROLE_ADMIN, NotFoundFault, DELETE, 'test', '', '', [ERR-100] The Publisher identifier [test] doesn't exist",
            "ROLE_SMP, UnauthorizedFault, CREATE, 'SMP123456789', 'test', 'test-test-test',Access is denied",
            "ROLE_SMP, UnauthorizedFault, DELETE, 'SMP123456789', 'test', 'test-test-test',Access is denied",

    })
    void testManageParticipantIdentifierFail(SMLRoleEnum role, String exceptionClassName, String action, String publisherId, String participantId,
                                             String participantScheme,
                                             String message) throws ClassNotFoundException {
        // given
        // change user to wrong one. This is needed to test the exception
        SecurityTestUtils.authenticateWithRoleClientCert02(role);
        ManageParticipantIdentifierType value = buildManageParticipantIdentifierType(action, publisherId,
                participantId, participantScheme);
        // when-then
        Class<? extends Throwable> exceptionClass = (Class<? extends Throwable>) Class.forName("eu.europa.ec.bdmsl.ws.soap." + exceptionClassName);

        assertThrowsWithContainsMessage(exceptionClass,
                () -> testInstance.manageParticipantIdentifier(value),
                message);
    }

    @Test
    void testManageParticipantIdentifierCreate() {
        // given
        ManageParticipantIdentifierType value = buildManageParticipantIdentifierType("CREATE", "SMP123456789", "0088:" + UUID.randomUUID().toString(), "test-test-test");
        // when-then
        ManageParticipantIdentifierResponse response = Assertions.assertDoesNotThrow(() -> testInstance.manageParticipantIdentifier(value));
        assertNotNull(response);
        assertEquals("Participant identifier count: [1] created!", response.getStatus());
    }

    @Test
    void testManageParticipantIdentifierDelete() {
        // given
        ManageParticipantIdentifierType value = buildManageParticipantIdentifierType("CREATE", "SMP123456789", "0088:" + UUID.randomUUID().toString(), "test-test-test");
        Assertions.assertDoesNotThrow(() -> testInstance.manageParticipantIdentifier(value));
        value.setAction("DELETE");
        // when-then
        ManageParticipantIdentifierResponse response = Assertions.assertDoesNotThrow(() -> testInstance.manageParticipantIdentifier(value));
        assertNotNull(response);
        assertEquals("Participant identifier count: [1] deleted!", response.getStatus());
    }

    protected ManageServiceMetadataPublisherType buildManageServiceMetadataPublisherType(String action, String recipientEmailAddress, String serviceMetadataPublisherId,
                                                                                         String domainZone, String certificateOwnerId,
                                                                                         String logicalAddress, String physicalAddress) {
        ManageServiceMetadataPublisherType value = new ManageServiceMetadataPublisherType();
        value.setAction(action);
        value.setReceiverEmailAddress(recipientEmailAddress);
        value.setServiceMetadataPublisherID(serviceMetadataPublisherId);
        value.setDNSZone(domainZone);
        value.setSmpOwnerCertificateId(certificateOwnerId);
        value.setLogicalAddress(logicalAddress);
        value.setPhysicalAddress(physicalAddress);
        return value;
    }

    protected ManageParticipantIdentifierType buildManageParticipantIdentifierType(String action, String publisherId, String participantId,
                                                                                   String participantScheme) {

        ParticipantIdentifierType participantIdentifierType = new ParticipantIdentifierType();
        participantIdentifierType.setValue(participantId);
        participantIdentifierType.setScheme(participantScheme);


        ManageParticipantIdentifierType value = new ManageParticipantIdentifierType();
        value.setAction(action);
        value.setServiceMetadataPublisherID(publisherId);
        value.getParticipantIdentifiers().add(participantIdentifierType);

        return value;
    }

}

