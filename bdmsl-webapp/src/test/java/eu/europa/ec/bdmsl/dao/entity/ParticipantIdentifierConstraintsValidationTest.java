/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.entity;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;

import static eu.europa.ec.bdmsl.util.Constant.NEW_CERT_PUBLIC_KEY;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * This test class validates the data model {@link eu.europa.ec.bdmsl.dao.entity.ParticipantIdentifierEntity)} and its constraints
 * <p>
 *
 * @author Flavio SANTOS
 * @since 22/09/2016
 */
class ParticipantIdentifierConstraintsValidationTest extends AbstractJUnit5Test {

    public static final String INSERT_PARTICIPANT_IDENTIFIER = "INSERT INTO bdmsl_participant_identifier (id, participant_id,scheme,fk_smp_id,created_on,last_updated_on) values (?, ?,?,?,?,?)";

    private final CertificateConstraintsValidationTest certificateConstraintsValidationTest = new CertificateConstraintsValidationTest();

    private final SMPConstraintsValidationTest smpConstraintsValidationTest = new SMPConstraintsValidationTest();

    @Test
    void persistIntoParticipantIdentifierTableOk() throws SQLException {
        String sqlForCertificate = "INSERT INTO bdmsl_certificate(id,certificate_id,valid_from,valid_until,pem_encoding,new_cert_change_date,new_cert_id,created_on,last_updated_on) values (?,?,?,?,?,?,?,?,?)";
        String sqlForSmp = "INSERT INTO bdmsl_smp (id, smp_id,fk_certificate_id,endpoint_physical_address,endpoint_logical_address,fk_subdomain_id,created_on,last_updated_on) values (1000, ?,?,?,?,?,?,?)";
        Timestamp date = new Timestamp(Calendar.getInstance().getTimeInMillis());
        ByteArrayInputStream pem = new ByteArrayInputStream(NEW_CERT_PUBLIC_KEY.getBytes());

        certificateConstraintsValidationTest.persist(dataSource.getConnection(), sqlForCertificate, "1234", "CN=SMP_receiverCN,O=DIGIT_TEST,C=BE:9876543210", date, date, pem, null, null, date, date);
        smpConstraintsValidationTest.persist(dataSource.getConnection(), sqlForSmp, "SML00001", "1234", "10.0.0.1", "test.eu", 1, date, date);
        assertDoesNotThrow(() -> persist(1000L,"0088:12345678910", "iso6523-actorid-upis", 1000L, date, date));
    }

    @Test
    void persistIntoParticipantIdentifierTablePKNotOk() throws SQLException {

        String sqlForCertificate = "INSERT INTO bdmsl_certificate(id,certificate_id,valid_from,valid_until,pem_encoding,new_cert_change_date,new_cert_id,created_on,last_updated_on) values (?,?,?,?,?,?,?,?,?)";
        String sqlForSmp = "INSERT INTO bdmsl_smp (smp_id,fk_certificate_id,endpoint_physical_address,endpoint_logical_address,fk_subdomain_id,created_on,last_updated_on) values (?,?,?,?,?,?,?)";

        ByteArrayInputStream pem = new ByteArrayInputStream(NEW_CERT_PUBLIC_KEY.getBytes());
        Timestamp date = new Timestamp(Calendar.getInstance().getTimeInMillis());

        certificateConstraintsValidationTest.persist(dataSource.getConnection(), sqlForCertificate, "12345", "CN=SMP_receiverCN,O=DIGIT_TEST,C=BE:12345", date, date, pem, null, null, date, date);
        smpConstraintsValidationTest.persist(dataSource.getConnection(), sqlForSmp, "SML00002", "12345", "10.0.0.1", "test.eu", 1, date, date);

        SQLException result = assertThrows(SQLException.class,
                () -> persist(1000L,"0088:159486357", "iso6523-actorid-upis", 9000L, date, date));

        MatcherAssert.assertThat(result.getMessage(), CoreMatchers.containsString("constraint violation"));
        MatcherAssert.assertThat(result.getMessage(), CoreMatchers.containsString("FK_SMP_ID"));
    }


    private void persist(Long id, String participantId, String scheme, Long fkSmpId, Timestamp createdOn, Timestamp updatedOn) throws SQLException {
        try (Connection connection = dataSource.getConnection();
            PreparedStatement pstmt = connection.prepareStatement(INSERT_PARTICIPANT_IDENTIFIER)) {
            pstmt.setLong(1, id);
            pstmt.setString(2, participantId);
            pstmt.setString(3, scheme);
            pstmt.setLong(4, fkSmpId);
            pstmt.setTimestamp(5, createdOn);
            pstmt.setTimestamp(6, updatedOn);
            pstmt.executeUpdate();
            connection.commit();
        }
    }

}
