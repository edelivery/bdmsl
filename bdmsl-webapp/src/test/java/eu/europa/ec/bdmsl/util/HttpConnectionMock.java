/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.util;

import eu.europa.ec.edelivery.security.cert.URLFetcher;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

/**
 * @author Flavio SANTOS
 */

public class HttpConnectionMock implements URLFetcher {
    private String resource;
    private InputStream certificate;

    public HttpConnectionMock(String resource) {
        this.resource = resource;
    }

    public HttpConnectionMock(InputStream certificate) {
        this.certificate = certificate;
    }

    @Override
    public InputStream fetchURL(String crlURL) throws IOException {
        try {
            if (certificate == null) {
                if (resource == null) {
                    throw new FileNotFoundException("Resource file was not defined");
                }
                return Thread.currentThread().getContextClassLoader().getResource(resource).openStream();
            } else {
                return certificate;
            }
        } catch (Exception exc) {
            throw new IOException(exc.getMessage(), exc);
        }
    }

    @Override
    public InputStream fetchURL(String crlURL, Map<String, String> map, InputStream inputStream) throws IOException {
        return fetchURL(crlURL);
    }
}
