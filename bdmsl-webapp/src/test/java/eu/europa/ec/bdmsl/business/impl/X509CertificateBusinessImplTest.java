/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.business.impl;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.business.IX509CertificateBusiness;
import eu.europa.ec.bdmsl.test.CommonTestUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.security.Security;
import java.security.cert.X509Certificate;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Flavio SANTOS
 * @since 24/02/2017
 */
class X509CertificateBusinessImplTest extends AbstractJUnit5Test {

    @Autowired
    private IX509CertificateBusiness x509CertificateBusiness;

    @BeforeAll
    static void beforeClass() {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    @Test
    void testGetTrustedRootCertificateDNOrderedForIssuerOk() throws Exception {

        X509Certificate certificate = CommonTestUtils.createCertificate("CN=GlobalSign Root CA,OU=Root CA,O=GlobalSign nv-sa,C=BE", "C=BE, O=GlobalSign nv-sa, OU=Root CA, CN=SMP_000199999999999");

        String certificateStr = x509CertificateBusiness.getTrustedRootCertificateDN(new X509Certificate[]{certificate});
        assertEquals("CN=GlobalSign Root CA,OU=Root CA,O=GlobalSign nv-sa,C=BE", certificateStr);
    }

    @Test
    void testGetTrustedRootCertificateDNForIssuerToBeOrderedOk() throws Exception {

        X509Certificate certificate = CommonTestUtils.createCertificate("O=NATIONAL IT AND TELECOM AGENCY,CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA,C=DK,OU=FOR TEST PURPOSES ONLY", "C=BE, O=GlobalSign nv-sa, OU=Root CA, CN=SMP_000199999999999");

        String certificateStr = x509CertificateBusiness.getTrustedRootCertificateDN(new X509Certificate[]{certificate});
        assertEquals("CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA,OU=FOR TEST PURPOSES ONLY,O=NATIONAL IT AND TELECOM AGENCY,C=DK", certificateStr);
    }

    @Test
    void testGetTrustedRootCertificateDNOrderedForSubjectOk() throws Exception {

        X509Certificate certificate = CommonTestUtils.createCertificate("CN=DigitalSign Corporation CA,OU=Root CA,O=GlobalSign nv-sa,C=UK", "CN=SMP_357951852456,O=DG-DIGIT,C=BE");

        String certificateStr = x509CertificateBusiness.getTrustedRootCertificateDN(new X509Certificate[]{certificate});
        assertEquals("CN=SMP_357951852456,O=DG-DIGIT,C=BE", certificateStr);
    }

    @Test
    void testGetTrustedRootCertificateDNOrderedForSubjectToBeOrderedOk() throws Exception {

        X509Certificate certificate = CommonTestUtils.createCertificate("CN=DigitalSign Corporation CA,OU=Root CA,O=GlobalSign nv-sa,C=UK", "C=BE,CN=SMP_357951852456,O=DG-DIGIT");

        String certificateStr = x509CertificateBusiness.getTrustedRootCertificateDN(new X509Certificate[]{certificate});
        assertEquals("CN=SMP_357951852456,O=DG-DIGIT,C=BE", certificateStr);
    }

    @Test
    void testGetTrustedRootCertificateDNOrderedForFullSubjectOk() throws Exception {

        X509Certificate certificate = CommonTestUtils.createCertificate("CN=DigitalSign Corporation CA,OU=Root CA,O=GlobalSign nv-sa,C=UK", "OU=eHealth, O=European Commission, OU=CEF_eDelivery.europa.eu,C=BE, CN=DUMMY_SMP_TEST, emailAddress=CEF-EDELIVERY-SUPPORT@ec.europa.eu");

        String certificateStr = x509CertificateBusiness.getTrustedRootCertificateDN(new X509Certificate[]{certificate});
        assertEquals("CN=DUMMY_SMP_TEST,OU=CEF_eDelivery.europa.eu,OU=eHealth,O=European Commission,C=BE", certificateStr);
    }

    @Test
    void testGetTrustedRootCertificateDNOrderedForDefaultSubjectOk() throws Exception {
        //NON ROOT CA MODEL stores only CN, C,O for subject, so we compare only this 3 metadata

        X509Certificate certificate = CommonTestUtils.createCertificate("CN=DigitalSign Corporation CA,OU=Root CA,O=GlobalSign nv-sa,C=UK", "OU=eHealth, O=DG-DIGIT, OU=CEF_eDelivery.europa.eu,C=BE, CN=COMMON_TRUSTED_CERTIFICATE_77777777");

        String certificateStr = x509CertificateBusiness.getTrustedRootCertificateDN(new X509Certificate[]{certificate});
        assertEquals("CN=COMMON_TRUSTED_CERTIFICATE_77777777,O=DG-DIGIT,C=BE", certificateStr);
    }

    @Test
    void testGetTrustedRootCertificateDNOrderedForDefaultIssue() throws Exception {
        //ROOT CA MODEL does not store only CN, C AND O but the entire issuer metatada, so we must compare the entire issuer metadata

        X509Certificate certificate = CommonTestUtils.createCertificate("CN=SMP_XPTO2016112,OU=Root CA,O=DG-DIGIT,C=BE", "OU=eHealth, O=DG-DIGIT, OU=CEF_eDelivery.europa.eu,C=BE, CN=COMMON_SMP_15577");

        String certificateStr = x509CertificateBusiness.getTrustedRootCertificateDN(new X509Certificate[]{certificate});
        assertNull(certificateStr);
    }

    @Test
    void testGetTrustedRootCertificateDNToBeOrderedForFullSubjectOk() throws Exception {

        X509Certificate certificate = CommonTestUtils.createCertificate("O=GlobalSign nv-sa,OU=Root CA,C=UK,CN=DigitalSign Corporation CA", "OU=eHealth, O=European Commission, OU=CEF_eDelivery.europa.eu,C=BE, CN=DUMMY_SMP_TEST,emailAddress=CEF-EDELIVERY-SUPPORT@ec.europa.eu");

        String certificateStr = x509CertificateBusiness.getTrustedRootCertificateDN(new X509Certificate[]{certificate});
        assertEquals("CN=DUMMY_SMP_TEST,OU=CEF_eDelivery.europa.eu,OU=eHealth,O=European Commission,C=BE", certificateStr);
    }

    @Test
    void testGetTrustedRootCertificateDNOrderedForFullIssuerOk() throws Exception {

        X509Certificate certificate = CommonTestUtils.createCertificate("C=PT, O=T-Systems GmbH, OU=T-Systems Center, ST=Nordrhein Westfalen,postalCode=57250, L=Netphen,street=Untere Industriestr, CN=Business CA 4", "OU=eHealth, O=European Commission, OU=CEF_eDelivery.europa.eu,C=IT, CN=SMP_TEST,emailAddress=CEF-EDELIVERY-SUPPORT@ec.europa.eu");

        String certificateStr = x509CertificateBusiness.getTrustedRootCertificateDN(new X509Certificate[]{certificate});
        assertEquals("CN=Business CA 4,OU=T-Systems Center,O=T-Systems GmbH,L=Netphen,ST=Nordrhein Westfalen,C=PT", certificateStr);
    }

    @Test
    void testGetTrustedRootCertificateDNToBeOrderedForFullIssuerOk() throws Exception {

        X509Certificate certificate = CommonTestUtils.createCertificate("ST=Nordrhein Westfalen, postalCode=57250,L=Netphen, street=Untere Industriestr,O=T-Systems GmbH, C=PT,OU=T-Systems Center,   CN=Business CA 4", "OU=eHealth, O=European Commission, OU=CEF_eDelivery.europa.eu,C=IT, CN=SMP_TEST,emailAddress=CEF-EDELIVERY-SUPPORT@ec.europa.eu");

        String certificateStr = x509CertificateBusiness.getTrustedRootCertificateDN(new X509Certificate[]{certificate});
        assertEquals("CN=Business CA 4,OU=T-Systems Center,O=T-Systems GmbH,L=Netphen,ST=Nordrhein Westfalen,C=PT", certificateStr);
    }

    @Test
    void testGetTrustedRootCertificateDNNotFound() throws Exception {

        X509Certificate certificate = CommonTestUtils.createCertificate("C=PT, O=Systems GmbH, OU=T-Systems Center, L=Netphen/street\\=Untere Industriestr, CN=Business CA 85", "OU=eHealth, O=European Commission, OU=CEF_eDelivery.europa.eu,C=IT, CN=SMP_TEST_PRIME/emailAddress\\=CEF-EDELIVERY-SUPPORT@ec.europa.eu");

        String certificateStr = x509CertificateBusiness.getTrustedRootCertificateDN(new X509Certificate[]{certificate});
        assertNull(certificateStr);
    }

    @Test
    void getTrustedRootCertificateDNForIssuerAndSubjectMatch() throws Exception {

        X509Certificate certificate = CommonTestUtils.createCertificate("C=BE, O=Subdomain-Entity, OU=Subdomain-Entity, ST=Nordrhein Westfalen/postalCode\\=57250, L=Netphen/street\\=Untere Industriestr, CN=Subdomain Issuer", "C=BE, O=Subdomain-Entity, OU=Subdomain-Entity, CN=Subdomain Subject");

        //If there is a match for Issuer and Subject, Subject must be prioritized.
        String certificateStr = x509CertificateBusiness.getTrustedRootCertificateDN(new X509Certificate[]{certificate});
        assertEquals("CN=Subdomain Subject,O=Subdomain-Entity,C=BE", certificateStr);
    }

    @Test
    void getTrustedRootCertificateDNForIssuerOkAndSubjectNotOk() throws Exception {

        X509Certificate certificate = CommonTestUtils.createCertificate("C=BE, O=Subdomain-Entity, OU=Subdomain-Entity, ST=Nordrhein Westfalen, postalCode=57250, L=Netphen,street=Untere Industriestr, CN=Subdomain Issuer", "C=BE, O=Subdomain-Entityåãä, OU=Subdomain-Entity, CN=SMP_Subdomain Subject 1");

        //If there is a match for Issuer and Subject, Subject must be prioritized.
        String certificateStr = x509CertificateBusiness.getTrustedRootCertificateDN(new X509Certificate[]{certificate});
        assertEquals("CN=Subdomain Issuer,OU=Subdomain-Entity,O=Subdomain-Entity,L=Netphen,ST=Nordrhein Westfalen,C=BE", certificateStr);
    }

    @Test
    void getTrustedRootCertificateDNForIssuerOkAndSubjectNotOk1() throws Exception {

        X509Certificate certificate = CommonTestUtils.createCertificate("C=BE, O=Subdomain-Entityåãä, CN=Subdomain issuer", "C=BE, O=Subdomain-Entityåãä, OU=Subdomain-Entity, CN=SMP_Subdomain Subject 1");

        //If there is a match for Issuer and Subject, Subject must be prioritized.
        String certificateStr = x509CertificateBusiness.getTrustedRootCertificateDN(new X509Certificate[]{certificate});
        assertEquals("CN=Subdomain issuer,O=Subdomain-Entityaaa,C=BE", certificateStr);
    }

    @Test
    void getTrustedRootCertificateDNForIssuerNotOkAndSubjectOk() throws Exception {

        X509Certificate certificate = createX509CertificateKeystoreNotTrusted("C=BE, O=Subdomain-Entityåã, OU=Subdomain-Entity, ST=Nordrhein Westfalen/postalCode\\=57250, L=Netphen/street\\=Untere Industriestr, CN=Subdomain Issuer 1",
                "C=BE, O=Subdomain-Entityåã, OU=Subdomain-Entity, CN=SMP_Subdomain Subject");

        //If there is a match for Issuer and Subject, Subject must be prioritized.
        String certificateStr = x509CertificateBusiness.getTrustedRootCertificateDN(new X509Certificate[]{certificate});
        assertEquals("CN=SMP_Subdomain Subject,O=Subdomain-Entityaa,C=BE", certificateStr);
    }

    @Test
    void getTrustedRootCertificateDNForIssuerNotOkAndSubjectNotOk() throws Exception {

        X509Certificate certificate = CommonTestUtils.createCertificate("C=BE, O=Subdomain-Entity, OU=Subdomain-Entity, ST=Nordrhein Westfalen/postalCode\\=57250, L=Netphen/street\\=Untere Industriestr, CN=Subdomain Issuer 1", "C=BE, O=Subdomain-Entity, OU=Subdomain-Entity, CN=Subdomain Subject 1");

        //If there is a match for Issuer and Subject, Subject must be prioritized.
        String certificateStr = x509CertificateBusiness.getTrustedRootCertificateDN(new X509Certificate[]{certificate});
        assertNull(certificateStr);
    }

    @Test
    void testSmartEscapeDistinguishedName() throws Exception {

        X509Certificate certificate = CommonTestUtils.createCertificate("C=BE, O=Subdomain-Entityåãä-Special-Chars, CN=Subdomain issuer", "C=BE, O=Special-Chars, CN=SMP_Subdomain subject");

        //If there is a match for Issuer and Subject, Subject must be prioritized.
        String certificateStr = x509CertificateBusiness.getTrustedRootCertificateDN(new X509Certificate[]{certificate});
        assertEquals("CN=Subdomain issuer,O=Subdomain-Entityaaa-Special-Chars,C=BE", certificateStr);
    }

    @Test
    void testSmartEscapeDistinguishedName1() throws Exception {

        X509Certificate certificate = CommonTestUtils.createCertificate("CN=issuer-slash/backslash\\\\quote\\\"colon:_rfc2253special_ampersand&comma\\,equals\\=plus\\+lessthan\\<greaterthan\\>hash\\#semicolon\\;end, O=DEẞßAaPLzołcNOÆæØøAa, C=PL", "C=BE, O=Special-Chars, CN=SMP_Subdomain subject");

        //If there is a match for Issuer and Subject, Subject must be prioritized.
        String certificateStr = x509CertificateBusiness.getTrustedRootCertificateDN(new X509Certificate[]{certificate});
        assertEquals("CN=issuer-slash/backslash\\\\quote\\\"colon:_rfc2253special_ampersand&comma\\,equals\\=plus\\+lessthan\\<greaterthan\\>hash\\#semicolon\\;end,O=DEẞßAaPLzołcNOÆæØøAa,C=PL", certificateStr);
    }
}
