/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.impl;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.exception.BadConfigurationException;
import eu.europa.ec.bdmsl.common.exception.KeyException;
import eu.europa.ec.bdmsl.common.util.KeyUtil;
import eu.europa.ec.bdmsl.test.CommonTestUtils;
import eu.europa.ec.bdmsl.service.ITruststoreService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;



class TruststoreServiceTest extends AbstractJUnit5Test {

    @Autowired
    ITruststoreService testInstance;

    @BeforeEach
    protected void testSetup() throws Exception {
        String encryptKey = "encryptedPasswordKey.key";
        String truststore = "bdmsl-truststore.p12";
        String truststoreNew = "bdmsl-truststore_" + UUID.randomUUID() + ".p12";
        Files.copy(resourceDirectory.resolve(truststore), targetDirectory.resolve(truststoreNew), StandardCopyOption.REPLACE_EXISTING);


        String resourcesPath = targetDirectory.toFile().getAbsolutePath();

        Mockito.doReturn(false).when(configurationBusiness).isLegacyDomainAuthorizationEnabled();
        Mockito.doReturn(truststoreNew).when(configurationBusiness).getTruststoreFilename();
        Mockito.doReturn(resourcesPath).when(configurationBusiness).getConfigurationFolder();
        Mockito.doReturn(encryptKey).when(configurationBusiness).getEncryptionFilename();
        Mockito.doReturn(KeyUtil.encrypt(Paths.get(resourcesPath, encryptKey).toFile().getAbsolutePath(), "test123"))
                .when(configurationBusiness).getTruststorePassword();
    }

    @Test
    void testGetTruststoreOK() {

        KeyStore keyStore = testInstance.getTruststore();
        assertNotNull(keyStore);
    }

    @Test
    void testGetTruststoreFileNotExists() {

        Mockito.doReturn("bdmsl-truststore-notExits.p12").when(configurationBusiness).getTruststoreFilename();
        KeyStore keyStore = testInstance.getTruststore();

        assertNull(keyStore);
    }

    @Test
    void testGetAllAliases() throws Exception {
        //when
        List<String> aliases = testInstance.getAllAliases(null);
        // then
        assertEquals( 3, aliases.size());
    }

    @Test
    void testGetAllAliasesFilter() throws Exception {
        List<String> aliases = testInstance.getAllAliases("inter");

        assertEquals(2, aliases.size());
    }

    @Test
    void testGetAllAliasesNoTruststore() throws Exception {

        Mockito.doReturn("bdmsl-truststore-notExits.p12").when(configurationBusiness).getTruststoreFilename();
        //WHEN
        BadConfigurationException exception = assertThrows(BadConfigurationException.class,
                () -> testInstance.getAllAliases(""));

        assertEquals("[ERR-109] Truststore is not configured!", exception.getMessage());
    }

    @Test
    void testGetCertificate() throws Exception {
        X509Certificate certificate = testInstance.getCertificate("smp_test_change (test intermediate issuer 01)");

        assertNotNull(certificate);
    }

    @Test
    void testGetCertificateNotExits() throws Exception {
        X509Certificate certificate = testInstance.getCertificate("Not exists");

        assertNull(certificate);
    }

    @Test
    void testAddCertificate() throws Exception {
        // given
        String testCertificateAlias = "addCertificateAlias";
        X509Certificate certificate = CommonTestUtils.loadCertificate("auth-smp-test-change.cer");
        testInstance.deleteCertificate(testCertificateAlias);
        List<String> lst = testInstance.getAllAliases(null);
        assertFalse(lst.contains(testCertificateAlias));
        // when
        testInstance.addCertificate(certificate, testCertificateAlias);

        // then
        List<String> lstAfterAdd = testInstance.getAllAliases(null);
        assertEquals(lst.size() + 1, lstAfterAdd.size());
        assertTrue(lstAfterAdd.contains(testCertificateAlias.toLowerCase()));
        assertEquals(certificate, testInstance.getCertificate(testCertificateAlias));
    }

    @Test
    void testDeleteCertificate() throws Exception {
        // given
        String testCertificateAlias = "deleteCertificateAlias";
        X509Certificate certificate = CommonTestUtils.loadCertificate("auth-smp-test-change.cer");
        testInstance.addCertificate(certificate, testCertificateAlias);
        List<String> lst = testInstance.getAllAliases(null);
        assertTrue(lst.contains(testCertificateAlias.toLowerCase()));
        // when
        X509Certificate deleted = testInstance.deleteCertificate(testCertificateAlias);

        // then
        List<String> lstAfterAdd = testInstance.getAllAliases(null);
        assertEquals(lst.size() - 1, lstAfterAdd.size());
        assertFalse(lstAfterAdd.contains(testCertificateAlias.toLowerCase()));
        assertEquals(certificate, deleted);
    }


}
