/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.ws.soap;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.security.UnsecureAuthentication;
import org.busdox.servicemetadata.locator._1.PageRequest;
import org.busdox.servicemetadata.locator._1.ParticipantIdentifierPageType;
import org.busdox.servicemetadata.locator._1.ServiceMetadataPublisherServiceForParticipantType;
import org.busdox.transport.identifiers._1.ParticipantIdentifierType;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.security.Security;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Adrien FERIAL
 * @since 16/06/2015
 */
class ManageParticipantIdentifierListTest extends AbstractJUnit5Test {

    @Autowired
    private IManageParticipantIdentifierWS manageParticipantIdentifierWS;

    private static boolean initialized;

    @BeforeAll
    static void beforeClass() throws TechnicalException {
        UnsecureAuthentication authentication = new UnsecureAuthentication();
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    @BeforeEach
    protected void testSetup() throws Exception {
        if (!initialized) {
            final String smpId = "smpForListTest";
            // First insert 15 participants
            for (int i = 0; i < 15; i++) {
                ServiceMetadataPublisherServiceForParticipantType participantType = new ServiceMetadataPublisherServiceForParticipantType();
                ParticipantIdentifierType partIdType = new ParticipantIdentifierType();
                partIdType.setScheme("iso6523-actorid-upis");
                partIdType.setValue("0088:" + i);
                participantType.setParticipantIdentifier(partIdType);
                participantType.setServiceMetadataPublisherID(smpId);
                manageParticipantIdentifierWS.create(participantType);
            }
            initialized = true;
        }
    }

    @Test
    void testListEmpty() throws Exception {
        PageRequest PageRequest = new PageRequest();
        assertThrows(BadRequestFault.class, () -> manageParticipantIdentifierWS.list(PageRequest));
    }

    @Test
    void testListNull() throws Exception {
        assertThrows(BadRequestFault.class, () -> manageParticipantIdentifierWS.delete(null));
    }

    @Test
    void testListParticipantOk() throws Exception {
        final String smpId = "smpForListTest";
        // Then retrieve the data
        PageRequest PageRequest = new PageRequest();
        PageRequest.setServiceMetadataPublisherID(smpId);

        // no pageNumber -> pageNumber = 1 : the first page is page number 1
        ParticipantIdentifierPageType result = manageParticipantIdentifierWS.list(PageRequest);
        int count = 0;
        int pageNumber = 2;
        while (result != null && result.getParticipantIdentifiers() != null && result.getParticipantIdentifiers().size() > 0) {
            for (ParticipantIdentifierType part : result.getParticipantIdentifiers()) {
                count++;
            }
            PageRequest.setNextPageIdentifier(String.valueOf(pageNumber));
            pageNumber++;
            result = manageParticipantIdentifierWS.list(PageRequest);
        }

        assertEquals(17, count);
    }

    @Test
    void testListParticipantPageRequestTooHigh() throws Exception {
        final String smpId = "smpForListTest";
        // Then retrieve the data
        PageRequest PageRequest = new PageRequest();
        PageRequest.setServiceMetadataPublisherID(smpId);
        PageRequest.setNextPageIdentifier(String.valueOf(5000));
        ParticipantIdentifierPageType result = manageParticipantIdentifierWS.list(PageRequest);
        assertNull(result);
    }
}

