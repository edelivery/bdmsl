/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2017 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.security;

import eu.europa.ec.bdmsl.business.IConfigurationBusiness;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.Mockito.*;

class HSTSFilterTest {

    @Mock
    private IConfigurationBusiness configurationBusiness;

    @Mock
    private HttpServletResponse response;

    @Mock
    private FilterChain chain;

    @Mock
    private ServletRequest request;

    @Mock
    private ServletContext servletContext;

    @InjectMocks
    private HSTSFilter hstsFilter;

    @BeforeEach
    void setUp() throws ServletException {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void doFilter_HSTSEnabledWithIncludeSubDomainsAndPreload() throws IOException, ServletException {
        when(configurationBusiness.isHSTSEnabled()).thenReturn(true);
        when(configurationBusiness.getHSTSMaxAge()).thenReturn(31536000);
        when(configurationBusiness.isHSTSConfiguredWithIncludeSubDomain()).thenReturn(true);
        when(configurationBusiness.isHSTSConfiguredWithPreload()).thenReturn(true);

        hstsFilter.doFilter(request, response, chain);

        verify(response).setHeader("Strict-Transport-Security", "max-age=31536000;includeSubDomains; preload");
        verify(chain).doFilter(request, response);
    }

    @Test
    void doFilter_HSTSEnabledWithoutIncludeSubDomainsAndPreload() throws IOException, ServletException {
        when(configurationBusiness.isHSTSEnabled()).thenReturn(true);
        when(configurationBusiness.getHSTSMaxAge()).thenReturn(31536000);
        when(configurationBusiness.isHSTSConfiguredWithIncludeSubDomain()).thenReturn(false);
        when(configurationBusiness.isHSTSConfiguredWithPreload()).thenReturn(false);

        hstsFilter.doFilter(request, response, chain);

        verify(response).setHeader("Strict-Transport-Security", "max-age=31536000");
        verify(chain).doFilter(request, response);
    }

    @Test
    void doFilter_HSTSDisabled() throws IOException, ServletException {
        when(configurationBusiness.isHSTSEnabled()).thenReturn(false);

        hstsFilter.doFilter(request, response, chain);

        verify(response, never()).setHeader(eq("Strict-Transport-Security"), anyString());
        verify(chain).doFilter(request, response);
    }
}
