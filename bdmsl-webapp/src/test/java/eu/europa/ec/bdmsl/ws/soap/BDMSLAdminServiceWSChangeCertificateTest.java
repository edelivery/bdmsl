/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.ws.soap;

import ec.services.wsdl.bdmsl.admin.data._1.ChangeCertificate;
import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.business.ICertificateDomainBusiness;
import eu.europa.ec.bdmsl.common.enums.SMLRoleEnum;
import eu.europa.ec.bdmsl.dao.ISmpDAO;
import eu.europa.ec.bdmsl.test.CommonTestUtils;
import eu.europa.ec.bdmsl.security.CertificateAuthentication;
import eu.europa.ec.bdmsl.security.UnsecureAuthentication;
import eu.europa.ec.edelivery.security.PreAuthenticatedCertificatePrincipal;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.io.StringWriter;
import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import static eu.europa.ec.bdmsl.util.Constant.NEW_CERT_PUBLIC_KEY;
import static org.junit.jupiter.api.Assertions.*;


class BDMSLAdminServiceWSChangeCertificateTest extends AbstractJUnit5Test {

    @Autowired
    private IBDMSLAdminServiceWS bdmslServiceWS;

    @Autowired
    private ICertificateDomainBusiness certificateDomainBusiness;


    @Autowired
    @Qualifier("SmpDAOImpl")
    private ISmpDAO smpDAO;

    private static String originalCertId = "CN=SMP_TEST_CHANGE_CERTIFICATE1,O=DG-DIGIT,C=BE:0000000000000000110000000123abcd";
    private static String targetCertId = "CN=receiverCN,O=DIGIT,C=BE:00000000000000000000000000000001";
    private static final String DIGIT_SMP_TECH_TEAM = "CN=DIGIT_SMP_TECH_TEAM,O=DIGIT,C=BE:0000000000000000000000005afeb65a";

    @BeforeEach
    void before() throws Exception {
        setCertAuthentication("48:b6:81:ee:8e:0d:cc:08", "EMAILADDRESS=receiver@test.be,C=BE, O=DIGIT, OU=FOR TEST ONLY,CN=DIGIT_SMP_TECH_TEAM_3", "C=BE, O=DIGIT, OU=FOR TEST ONLY, CN=DIGIT_SMP_TECH_TEAM_2");
    }

    void setCertAuthentication(String serialNumber, String subject, String issuer) {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        String certHeaderValue = "serial=" + serialNumber + "&subject=" + subject + "&validfrom=Feb  1 14:20:18 2017 GMT&validto=Jul  9 23:59:00 2099 GMT&issuer=" + issuer;
        PreAuthenticatedCertificatePrincipal principal = blueCoatAuthenticationFilter.buildPrincipalFromHeader(certHeaderValue);
        Authentication authentication = new CertificateAuthentication(principal, Collections.singletonList(SMLRoleEnum.ROLE_ADMIN.getAuthority()));
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);

    }

    @Test
    void changeCertificateNullRequest() {
        assertThrows(BadRequestFault.class, () -> bdmslServiceWS.changeCertificate(null));
    }

    @Test
    void changeCertificateEmptyRequest() {
        assertThrows(BadRequestFault.class, () -> bdmslServiceWS.changeCertificate(new ChangeCertificate()));
    }

    @Test
    void changeCertificateValidSMPAndNoCertificate() {
        ChangeCertificate cct = new ChangeCertificate();
        cct.setServiceMetadataPublisherID("found");
        assertThrows(BadRequestFault.class, () -> bdmslServiceWS.changeCertificate(cct));
    }

    @Test
    void changeCertificateValidSMPAndValidCertificateOK() throws Exception {
        String oldCert = smpDAO.findSMP("TESTsmpChangeCertificate").getCertificateId();
        assertEquals(originalCertId, oldCert);

        ChangeCertificate cct = new ChangeCertificate();
        cct.setServiceMetadataPublisherID("TESTsmpChangeCertificate");
        cct.setNewCertificatePublicKey(NEW_CERT_PUBLIC_KEY.getBytes());
        bdmslServiceWS.changeCertificate(cct);

        String newCert = smpDAO.findSMP("TESTsmpChangeCertificate").getCertificateId();
        assertEquals(targetCertId, newCert);
    }

    @Test
    void changeCertificateValidSMPAndValidCertificateTwiceWithError() throws Exception {
        String oldCert = smpDAO.findSMP("TESTsmpChangeCertificate").getCertificateId();
        assertEquals(originalCertId, oldCert);

        ChangeCertificate cct = new ChangeCertificate();
        cct.setServiceMetadataPublisherID("TESTsmpChangeCertificate");
        cct.setNewCertificatePublicKey(NEW_CERT_PUBLIC_KEY.getBytes());
        bdmslServiceWS.changeCertificate(cct);

        String newCert = smpDAO.findSMP("TESTsmpChangeCertificate").getCertificateId();
        assertEquals(targetCertId, newCert);

        ChangeCertificate cct2 = new ChangeCertificate();
        cct2.setServiceMetadataPublisherID("TESTsmpChangeCertificate");
        cct2.setNewCertificatePublicKey(NEW_CERT_PUBLIC_KEY.getBytes());
        assertThrows(BadRequestFault.class, () -> bdmslServiceWS.changeCertificate(cct2));

    }

    @Test
    void changeCertificateAlreadyUsed() throws UnauthorizedFault, BadRequestFault, InternalErrorFault {
        ChangeCertificate cct = new ChangeCertificate();
        cct.setServiceMetadataPublisherID("smpSameCertificate1");
        cct.setNewCertificatePublicKey(NEW_CERT_PUBLIC_KEY.getBytes());
        // change it twice
        bdmslServiceWS.changeCertificate(cct);

        BadRequestFault result = assertThrows(BadRequestFault.class, () -> bdmslServiceWS.changeCertificate(cct));
        assertTrue(result.getMessage().startsWith("[ERR-106] Can not update certificate with itself:"));
    }

    @Test
    void changeCertificateForNonRootCADomainByAdminCertificateExpired() throws Exception {

        //GIVEN
        Calendar c = Calendar.getInstance();
        c.add(Calendar.YEAR, -1);
        Date dateValidFrom = c.getTime();
        c.add(Calendar.DAY_OF_MONTH, 2); // expired
        Date dateValidTo = c.getTime();
        ChangeCertificate cct = createCertificateChangeRequest("5AFEAFBE",
                "CN=DIGIT_SMP_TECH_TEAM_3, OU=FOR TEST ONLY, O=DIGIT, L=BE, C=BE, EMAILADDRESS=CEF-EDELIVERY-SUPPORT@ec.europa.eu",
                "CN=DIGIT_SMP_TECH_TEAM_3, OU=FOR TEST ONLY, O=DIGIT, L=BE, C=BE, EMAILADDRESS=CEF-EDELIVERY-SUPPORT@ec.europa.eu",
                dateValidFrom, dateValidTo, false, true);
        //WHEN-THEN
        assertThrows(BadRequestFault.class, () -> bdmslServiceWS.changeCertificate(cct));
    }

    @Test
    void changeCertificateForNonRootCADomainByAdminCertificateNotValidYet() throws Exception {

        //GIVEN
        Calendar c = Calendar.getInstance();
        c.add(Calendar.YEAR, +1);
        Date dateValidFrom = c.getTime();
        c.add(Calendar.DAY_OF_MONTH, 200); // expired
        Date dateValidTo = c.getTime();
        ChangeCertificate cct = createCertificateChangeRequest("5AFEAFBE",
                "EMAILADDRESS=CEF-EDELIVERY-SUPPORT@ec.europa.eu, C=BE, ST=BE, O=DIGIT, OU=FOR TEST ONLY, CN=DIGIT_SMP_TECH_TEAM_3",
                "C=BE, ST=BE, O=DIGIT, OU=FOR TEST ONLY, CN=DIGIT_SMP_TECH_TEAM_3",
                dateValidFrom, dateValidTo, false, true);


        //WHEN-THEN
        assertThrows(BadRequestFault.class, () -> bdmslServiceWS.changeCertificate(cct));
    }

    @Test
    void changeCertificateForNonRootCADomainByUnsecuredUserDer() throws Exception {
        //GIVEN
        Calendar c = Calendar.getInstance();
        c.add(Calendar.YEAR, -1);
        Date dateValidFrom = c.getTime();
        c.add(Calendar.YEAR, 2);
        Date dateValidTo = c.getTime();
        ChangeCertificate cct = createCertificateChangeRequest("5AFEAFBE",
                "CN=DIGIT_SMP_TECH_TEAM_2, OU=FOR TEST ONLY, O=DIGIT, L=BE, C=BE, EMAILADDRESS=CEF-EDELIVERY-SUPPORT@ec.europa.eu",
                "CN=DIGIT_SMP_TECH_TEAM_2, OU=FOR TEST ONLY, O=DIGIT, L=BE, C=BE, EMAILADDRESS=CEF-EDELIVERY-SUPPORT@ec.europa.eu",
                dateValidFrom, dateValidTo, false, true);

        UnsecureAuthentication authentication = new UnsecureAuthentication();
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        //WHEN-THEN
        assertDoesNotThrow(() -> bdmslServiceWS.changeCertificate(cct));
    }

    @Test
    void changeCertificateForNonRootCADomainByUnsecuredUserPem() throws Exception {
        //GIVEN
        Calendar c = Calendar.getInstance();
        c.add(Calendar.YEAR, -1);
        Date dateValidFrom = c.getTime();
        c.add(Calendar.YEAR, 2);
        Date dateValidTo = c.getTime();
        ChangeCertificate cct = createCertificateChangeRequest("5AFEAFBE",
                "CN=DIGIT_SMP_TECH_TEAM_2, OU=FOR TEST ONLY, O=DIGIT, L=BE, C=BE, EMAILADDRESS=CEF-EDELIVERY-SUPPORT@ec.europa.eu",
                "CN=DIGIT_SMP_TECH_TEAM_2, OU=FOR TEST ONLY, O=DIGIT, L=BE, C=BE, EMAILADDRESS=CEF-EDELIVERY-SUPPORT@ec.europa.eu",
                dateValidFrom, dateValidTo, false, false);

        UnsecureAuthentication authentication = new UnsecureAuthentication();
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        //WHEN-THEN
        assertDoesNotThrow(() -> bdmslServiceWS.changeCertificate(cct));

    }

    @Test
    void changeCertificateForNonRootCADomainByX509Certificate() throws Exception {
        //GIVEN
        X509Certificate certificate = CommonTestUtils.createCertificate("CN=DIGIT_SMP_TECH_TEAM_2,OU=FOR TEST ONLY,O=DIGIT,ST=BE,C=BE,E=CEF-EDELIVERY-SUPPORT@ec.europa.eu",
                "CN=DIGIT_SMP_TECH_TEAM_2,OU=FOR TEST ONLY,O=DIGIT,ST=BE,C=BE");
        X509Certificate[] certificates = {certificate};

        //Injecting the dummy certificate as an authenticated user
        Authentication authentication = createX509Authentication(certificates);
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        //WHEN-THEN
        //Calling Operation Change Certificate
        ChangeCertificate cct = new ChangeCertificate();
        cct.setServiceMetadataPublisherID("smpSameCertificate1");
        cct.setNewCertificatePublicKey(certificate.getEncoded());
        assertThrows(UnauthorizedFault.class, () -> bdmslServiceWS.changeCertificate(cct));
    }

    @Test
    void changeCertificateForNonRootCADomainByHeaderCertificate() throws Exception {
        //GIVEN
        //New certificate
        //GIVEN
        Calendar c = Calendar.getInstance();
        c.add(Calendar.YEAR, -1);
        Date dateValidFrom = c.getTime();
        c.add(Calendar.YEAR, 2);
        Date dateValidTo = c.getTime();
        ChangeCertificate cct = createCertificateChangeRequest("5AFEB65A",
                "CN=DIGIT_SMP_TECH_TEAM, OU=FOR TEST ONLY, O=DIGIT, ST=BE, C=BE, EMAILADDRESS=CEF-EDELIVERY-SUPPORT@ec.europa.eu",
                "CN=DIGIT_SMP_TECH_TEAM, OU=FOR TEST ONLY, O=DIGIT, ST=BE, C=BE, EMAILADDRESS=CEF-EDELIVERY-SUPPORT@ec.europa.eu",
                dateValidFrom, dateValidTo, false, false);


        //Injecting the dummy certificate as an authenticated user
        String certHeaderValue = "serial=48:b6:81:ee:8e:0d:cc:08&subject=EMAILADDRESS=receiver@test.be,C=BE, O=DIGIT, OU=FOR TEST ONLY,CN=DIGIT_SMP_TECH_TEAM_3&validfrom=Feb  1 14:20:18 2017 GMT&validto=Jul  9 23:59:00 2019 GMT&issuer=C=BE, O=DIGIT, OU=FOR TEST ONLY, CN=DIGIT_SMP_TECH_TEAM_2";
        PreAuthenticatedCertificatePrincipal principal = blueCoatAuthenticationFilter.buildPrincipalFromHeader(certHeaderValue);
        Authentication bcAuth = new CertificateAuthentication(principal, Collections.singletonList(SMLRoleEnum.ROLE_SMP.getAuthority()));
        bcAuth.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(bcAuth);

        //WHEN-THEN
        //Calling Operation Change Certificate
        assertThrows(UnauthorizedFault.class, () -> bdmslServiceWS.changeCertificate(cct));
    }

    @Test
    void testChangeCertificateByAdmin() throws Exception {
        testChangeCertificateByAdminForNonRoot();

        BadRequestFault result = assertThrows(BadRequestFault.class, () -> testChangeCertificateByAdminForRoot2());
        assertTrue(result.getMessage().startsWith("[ERR-106] It is not allowed to replace a Root CA certificate by a Non Root CA certificate"));
    }

    @Test
    void testChangeCertificateByAdminNonPKI() throws Exception {
        // given
        setCertAuthentication("4", "CN=SMP-ChangeNonPKICert,O=DIGIT, C=BE", "CN=Issuer of nonpki cert,O=DIGIT, C=BE");
        String smpId = "SMP-CHANGE-CERT-BY-ADMIN-3";
        //NON ROOT CA
        String currentCertificate = "CN=SMP-ChangeNonPKICert,O=DIGIT,C=BE:00000000000000000000000000000004";
        String newCertificate = "CN=NEW SMP ChangeNonPKICert,O=DIGIT,C=BE";
        String newCertificateIssuer = "CN=NEW SMP Issuer Cert,O=DIGIT,C=BE";
        // when - then
        changeCertificateByAdmin(smpId, "123456", newCertificateIssuer, newCertificate, currentCertificate, newCertificate + ":00000000000000000000000000123456");
    }

    @Test
    void testChangeCertificateByAdminNonPKIWithSameCNOCWithOU() throws Exception {
        // given
        setCertAuthentication("4", "CN=SMP-ChangeNonPKICert,O=DIGIT, C=BE", "CN=Issuer of nonpki cert,O=DIGIT, C=BE");
        String smpId = "SMP-CHANGE-CERT-BY-ADMIN-3";
        //NON ROOT CA
        String currentCertificate = "CN=SMP-ChangeNonPKICert,O=DIGIT,C=BE:00000000000000000000000000000004";
        String newCertificateLongSubject = "CN=SMP-ChangeNonPKICert,OU=NEWCERT,O=DIGIT,C=BE";
        String newCertificateExpectedId = "CN=SMP-ChangeNonPKICert,O=DIGIT,C=BE";
        String newCertificateIssuer = "CN=NEW SMP Issuer Cert,O=DIGIT,C=BE";
        // when - then
        changeCertificateByAdmin(smpId, "123456", newCertificateIssuer, newCertificateLongSubject, currentCertificate, newCertificateExpectedId + ":00000000000000000000000000123456");
    }

    void testChangeCertificateByAdminNonPKIWithSameCNOC() throws Exception {
        // given
        setCertAuthentication("4", "CN=SMP-ChangeNonPKICert,O=DIGIT, C=BE", "CN=Issuer of nonpki cert,O=DIGIT, C=BE");
        String smpId = "SMP-CHANGE-CERT-BY-ADMIN-3";
        //NON ROOT CA
        String currentCertificate = "CN=SMP-ChangeNonPKICert,O=DIGIT,C=BE:00000000000000000000000000000004";
        String newCertificateLongSubject = "CN=SMP-ChangeNonPKICert,O=DIGIT,C=BE";
        String newCertificateExpectedId = "CN=SMP-ChangeNonPKICert,O=DIGIT,C=BE";
        String newCertificateIssuer = "CN=NEW SMP Issuer Cert,O=DIGIT,C=BE";
        // when - then
        changeCertificateByAdmin(smpId, "123456", newCertificateIssuer, newCertificateLongSubject, currentCertificate, newCertificateExpectedId + ":00000000000000000000000000123456");
    }

    private void testChangeCertificateByAdminForNonRoot() throws Exception {
        String smpId = "SMP-CHANGE-CERT-BY-ADMIN";

        //CHANGING FROM NON ROOT CA TO NON ROOT CA
        String currentCertificate = "CN=SMP_TEST_CHANGE_CERTIFICATE,O=DG-DIGIT,C=BE";
        String newCertificate = "CN=DIGIT_SMP_TECH_TEAM,O=DIGIT,C=BE";
        changeCertificateByAdminForNonRoot(smpId, "5AFEB65A",
                "CN=DIGIT_SMP_TECH_TEAM, OU=FOR TEST ONLY, O=DIGIT, ST=BE, C=BE, EMAILADDRESS=CEF-EDELIVERY-SUPPORT@ec.europa.eu",
                "CN=DIGIT_SMP_TECH_TEAM, OU=FOR TEST ONLY, O=DIGIT, ST=BE, C=BE, EMAILADDRESS=CEF-EDELIVERY-SUPPORT@ec.europa.eu",
                currentCertificate, newCertificate,
                "CN=SMP_TEST_CHANGE_CERTIFICATE,O=DG-DIGIT,C=BE:0000000000000000000000000123abcd", DIGIT_SMP_TECH_TEAM,
                currentCertificate, newCertificate);
    }

    private void testChangeCertificateByAdminForRoot1() throws Exception {
        String smpId = "SMP-CHANGE-CERT-BY-ADMIN";

        //CHANGING FROM NON ROOT CA TO ROOT CA
        String currentCertificate = "CN=DIGIT_SMP_TECH_TEAM,O=DIGIT,C=BE";
        String newCertificate = "CN=senderCN-Test-Edelivery,O=DIGIT,C=BE";
        testChangeCertificateByAdminForRoot(smpId, "595B5BF0",
                "CN=rootCN, OU=B4, O=DIGIT, L=Brussels, ST=BE, C=BE, EMAILADDRESS=root@test.be",
                "CN=senderCN-Test-Edelivery, OU=B4, O=DIGIT, L=Brussels, ST=BE, C=BE",
                currentCertificate, newCertificate,
                DIGIT_SMP_TECH_TEAM, "CN=senderCN-Test-Edelivery,O=DIGIT,C=BE:000000000000000000000000595b5bf0",
                newCertificate, currentCertificate);
    }

    private void testChangeCertificateByAdminForRoot2() throws Exception {
        String smpId = "SMP-CHANGE-CERT-BY-ADMIN-2";

        //ROOT CA TO NON ROOT CA
        String currentCertificate = "CN=senderCN,O=DIGIT,C=BE";
        String newCertificate = "CN=DIGIT_SMP_TECH_TEAM_,O=DIGIT,C=BE";
        changeCertificateByAdminForRoot2(smpId, "5B1A52B0",
                "CN=senderCN_1, O=DIGIT, C=BE",
                "CN=senderCN_1, O=DIGIT, C=BE", currentCertificate, newCertificate,
                "CN=SMP_ROOTED,O=DIGIT,C=BE:0000000000000000000000005b1a52b0", DIGIT_SMP_TECH_TEAM,
                currentCertificate, newCertificate);
    }

    private void changeCertificateByAdminForNonRoot(String smpId, String serialNumber, String issuer, String subject, String... certificates) throws Exception {
        assertNotNull(certificateDomainBusiness.findDomain(certificates[0]));
        assertNull(certificateDomainBusiness.findDomain(certificates[1]));
        changeCertificateByAdmin(smpId, serialNumber, issuer, subject, certificates[2], certificates[3]);
        assertNull(certificateDomainBusiness.findDomain(certificates[4]));
        assertNotNull(certificateDomainBusiness.findDomain(certificates[5]));
    }

    private void testChangeCertificateByAdminForRoot(String smpId, String serialNumber, String issuer, String subject, String... certificates) throws Exception {
        assertNotNull(certificateDomainBusiness.findDomain(certificates[0]));
        assertNull(certificateDomainBusiness.findDomain(certificates[1]));
        changeCertificateByAdmin(smpId, serialNumber, issuer, subject, certificates[2], certificates[3]);
        assertNull(certificateDomainBusiness.findDomain(certificates[4]));
        assertNull(certificateDomainBusiness.findDomain(certificates[5]));
    }

    private void changeCertificateByAdminForRoot2(String smpId, String serialNumber, String issuer, String subject, String... certificates) throws Exception {
        assertNull(certificateDomainBusiness.findDomain(certificates[0]));
        assertNull(certificateDomainBusiness.findDomain(certificates[1]));
        changeCertificateByAdmin(smpId, serialNumber, issuer, subject, certificates[2], certificates[3]);
        assertNull(certificateDomainBusiness.findDomain(certificates[4]));
        assertNull(certificateDomainBusiness.findDomain(certificates[5]));
    }

    private void changeCertificateByAdmin(String smpId, String serialNumber, String issuer, String subject,
                                          String currentCertificate, String newCertificate) throws Exception {
        String oldCert = smpDAO.findSMP(smpId).getCertificateId();
        assertEquals(currentCertificate, oldCert);

        Calendar c = Calendar.getInstance();
        c.add(Calendar.YEAR, -1);
        Date dateValidFrom = c.getTime();
        c.add(Calendar.YEAR, 2);
        Date dateValidTo = c.getTime();

        ChangeCertificate cct = createCertificateChangeRequest(serialNumber, issuer, subject, dateValidFrom, dateValidTo, false, true);
        cct.setServiceMetadataPublisherID(smpId);
        bdmslServiceWS.changeCertificate(cct);

        String newCert = smpDAO.findSMP(smpId).getCertificateId();
        assertEquals(newCertificate, newCert);
    }


    private ChangeCertificate createCertificateChangeRequest(String serialNumber, String issuer, String subject, Date validFrom, Date validTo, boolean hasCRL, boolean useDer) throws Exception {
        X509Certificate cert = CommonTestUtils.createCertificate(serialNumber, issuer, subject, validFrom, validTo, hasCRL);
        byte[] certBuff;
        if (!useDer) {
            final StringWriter writer = new StringWriter();
            final JcaPEMWriter pemWriter = new JcaPEMWriter(writer);
            pemWriter.writeObject(cert);
            pemWriter.flush();
            pemWriter.close();
            certBuff = writer.toString().getBytes();

        } else {
            certBuff = cert.getEncoded();
        }

        ChangeCertificate cct = new ChangeCertificate();
        cct.setServiceMetadataPublisherID("smpSameCertificate1");
        cct.setNewCertificatePublicKey(certBuff);

        return cct;
    }

}
