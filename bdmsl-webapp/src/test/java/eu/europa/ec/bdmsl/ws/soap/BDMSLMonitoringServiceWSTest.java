/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.ws.soap;

import ec.services.wsdl.bdmsl.monitoring.data._1.IsAlive;
import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.exception.DNSClientException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.security.UnsecureAuthentication;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.xbill.DNS.CNAMERecord;
import org.xbill.DNS.DClass;
import org.xbill.DNS.Name;
import org.xbill.DNS.Record;

import java.security.Security;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

/**
 * @author Sebastian-Ion TINCU
 * @since 5.0
 */
class BDMSLMonitoringServiceWSTest extends AbstractJUnit5Test {

    @Autowired
    private IBDMSLMonitoringServiceWS bdmslMonitoringService;

    @BeforeAll
    static void beforeClass() throws TechnicalException {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        UnsecureAuthentication authentication = new UnsecureAuthentication();
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    @Test
    void testIsAliveLookupError() throws Exception {
        // given
        Mockito.doThrow(new DNSClientException("Lookup error.")).
                when(dnsClientService).lookup(any(String.class), any(Integer.class));

        // when then
        assertThrows(InternalErrorFault.class, () -> bdmslMonitoringService.isAlive(new IsAlive()));
    }

    @Test
    void testIsAliveLookupOk() throws Exception {
        // given
        List<Record> records = new ArrayList<Record>() {
            {
                add(new CNAMERecord(new Name("B-13b33c58a8e2a50a65af43d990c3bb5b.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu."), DClass.IN, 60, new Name("smp.test.com.eu" + ".")));
            }
        };
        Mockito.doReturn(records).when(dnsClientService).lookup("B-13b33c58a8e2a50a65af43d990c3bb5b.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu", 5);

        // when then
        assertDoesNotThrow(() -> bdmslMonitoringService.isAlive(new IsAlive()));
    }
}
