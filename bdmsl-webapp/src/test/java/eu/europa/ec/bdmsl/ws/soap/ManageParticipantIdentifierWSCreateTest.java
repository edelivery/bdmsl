/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.ws.soap;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.business.IManageParticipantIdentifierBusiness;
import eu.europa.ec.bdmsl.common.bo.ParticipantBO;
import eu.europa.ec.bdmsl.common.enums.SMLRoleEnum;
import eu.europa.ec.bdmsl.dao.IParticipantDAO;
import eu.europa.ec.bdmsl.test.DnsMessageSenderServiceMock;
import eu.europa.ec.bdmsl.security.ICertificateAuthentication;
import eu.europa.ec.bdmsl.security.UnsecureAuthentication;
import eu.europa.ec.bdmsl.service.dns.IDnsMessageSenderService;
import eu.europa.ec.bdmsl.test.SecurityTestUtils;
import org.apache.commons.lang3.StringUtils;
import org.busdox.servicemetadata.locator._1.ServiceMetadataPublisherServiceForParticipantType;
import org.busdox.transport.identifiers._1.ParticipantIdentifierType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import static eu.europa.ec.bdmsl.test.TestConstants.*;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Adrien FERIAL
 * @since 16/06/2015
 */
class ManageParticipantIdentifierWSCreateTest extends AbstractJUnit5Test {

    @Autowired
    private ICertificateAuthentication customAuthentication;

    @Autowired
    private IManageParticipantIdentifierWS manageParticipantIdentifierWS;

    @Autowired
    private IParticipantDAO participantDAO;

    @Autowired
    private IDnsMessageSenderService dnsMessageSenderService;

    @Autowired
    private IManageParticipantIdentifierBusiness manageParticipantIdentifierBusiness;

    @BeforeEach
    void before() throws Exception {
        this.beforeTestInit();
        UnsecureAuthentication authentication = new UnsecureAuthentication();
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
    }

    @Test
    void testCreateEmpty() {
        ServiceMetadataPublisherServiceForParticipantType participantType = new ServiceMetadataPublisherServiceForParticipantType();

        BadRequestFault result = assertThrows(BadRequestFault.class,
                () -> manageParticipantIdentifierWS.create(participantType));
        assertThat(result.getMessage(),
                org.hamcrest.Matchers.containsString("[ERR-106] The input values must not be null"));
    }

    @Test
    void testCreateNull() {
        BadRequestFault result = assertThrows(BadRequestFault.class,
                () -> manageParticipantIdentifierWS.create(null));
        assertThat(result.getMessage(),
                org.hamcrest.Matchers.containsString("[ERR-106] The input values must not be null"));
    }

    @Test
    void testCreateParticipantWithoutExistingSMP() {
        ServiceMetadataPublisherServiceForParticipantType participantType = createParticipantMetadata();

        NotFoundFault result = assertThrows(NotFoundFault.class,
                () -> manageParticipantIdentifierWS.create(participantType));
        assertThat(result.getMessage(),
                org.hamcrest.Matchers.containsString("[ERR-100] The Publisher identifier [NotFound] doesn't exist"));

    }

    private ServiceMetadataPublisherServiceForParticipantType createParticipantMetadata() {
        return createParticipantMetadata("iso6523-actorid-upis", "0088:123456789", "NotFound");
    }

    private ServiceMetadataPublisherServiceForParticipantType createParticipantMetadata(String scheme, String participantId, String smpId) {
        ServiceMetadataPublisherServiceForParticipantType participantType = new ServiceMetadataPublisherServiceForParticipantType();
        ParticipantIdentifierType partIdType = new ParticipantIdentifierType();
        partIdType.setScheme(scheme);
        partIdType.setValue(participantId);
        participantType.setParticipantIdentifier(partIdType);
        participantType.setServiceMetadataPublisherID(smpId);
        return participantType;
    }

    @Test
    void testCreateParticipantWithSMPNotCreatedByUser() {
        SecurityTestUtils.authenticateWithRoleClientCert02(SMLRoleEnum.ROLE_SMP);
        ServiceMetadataPublisherServiceForParticipantType participantType = createParticipantMetadata();
        participantType.getParticipantIdentifier().setValue("0007:223456789");
        participantType.setServiceMetadataPublisherID("found");

        UnauthorizedFault result = assertThrows(UnauthorizedFault.class,
                () -> manageParticipantIdentifierWS.create(participantType));
        assertThat(result.getMessage(),
                org.hamcrest.Matchers.containsString("[ERR-101] The SMP [found] was not created with the certificate"));
    }

    @ParameterizedTest
    @CsvSource({"NotMatchParticipantRegExp,iso6523-actorid-upis, 0008:123456789, " + SMP_ID_ONLY_CNAME + ", is illegal.",
            "AlreadyRegistered, iso6523-actorid-upis, 0009:123456789AlwaysPresent, " + SMP_ID_ONLY_CNAME + ", already exist on subdomain",
            "NullOrEmpty, '' , '', " + SMP_ID_ONLY_CNAME + ", 'Identifier must not be ''null'' or empty'",
            "NullOrEmptyValue, iso6523-actorid-upis, '', " + SMP_ID_ONLY_CNAME + ", 'Identifier must not be ''null'' or empty'",
            "AlreadyRegisteredCaseInsensitive, iso6523-actorid-upis, 0009:223456789, " + SMP_ID_ONLY_CNAME + ", 'already exist on subdomain'",
            "InvalidSchema, 123, 0008:223456789, " + SMP_ID_ONLY_CNAME + ", '[ERR-106] The Scheme Identifier MUST take the form {domain}-{identifierArea}-{identifierType}'",
            "ToLongSchema, 12345-12345-123456789012345, 0009:223456789-a, " + SMP_ID_ONLY_CNAME + ", '[ERR-106] A scheme identifier MUST NOT exceed 25 characters'",
            "SchemeNotAscii, iso6523-äctorid-upis, 0009:223456789-a, " + SMP_ID_ONLY_CNAME + ", '[ERR-106] The Scheme Identifier MUST take the form {domain}-{identifierArea}-{identifierType}'",
            "SchemeNotAlphaNumeric, ##iso6523-actorid-upis, 0009:123456789AlwaysPresent, " + SMP_ID_ONLY_CNAME + ", '[ERR-106] The Scheme Identifier MUST take the form {domain}-{identifierArea}-{identifierType}'",
            "InvalidEBCoreIdentifier, urn:oasis:names:tc:ebcore:partyid-type:notValid-unregistered, domain:ec.europa.eu, " + SMP_ID_ONLY_CNAME + ", '[ERR-106] Invalid ebCore id [urn:oasis:names:tc:ebcore:partyid-type:notValid-unregistered:domain:ec.europa.eu] ebcoreId <scheme-in-catalog> must be  must one from the list [iso6523, unregistered]'",
            "InvalidEBCoreIdentifier2, urn:oasis:names:tc:ebcore:partyid-type:iso2992, domain:ec.europa.eu, " + SMP_ID_ONLY_CNAME + ", '[ERR-106] Invalid ebCore id [urn:oasis:names:tc:ebcore:partyid-type:iso2992:domain:ec.europa.eu] ebcoreId <scheme-in-catalog> must be  must one from the list [iso6523, unregistered]'",
    })
    void testCreateParticipantBadIdentifiers(String name, String identifierScheme, String identifierValue, String smpIdentifier, String message) {
        System.out.println("testCreateParticipantBadIdentifiers " + name);
        // given
        ServiceMetadataPublisherServiceForParticipantType participantType =
                createParticipantMetadata(identifierScheme, identifierValue, smpIdentifier);
        // when then
        BadRequestFault result = assertThrows(BadRequestFault.class,
                () -> manageParticipantIdentifierWS.create(participantType));
        assertThat(result.getMessage(),
                org.hamcrest.Matchers.containsString(message));
    }

    @Test
    @Transactional
    void testCreateParticipantOkCnameMD5() throws Exception {
        final String partId = "0009:12345SHA256base32";
        final String smpId = SMP_ID_ONLY_CNAME;
        final String scheme = "iso6523-actorid-upis";

        String messages = createParticipant(partId, smpId, scheme);

        assertDNSCnameRecord("B-981e9a49ffa4b520643ea14cd172e101.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu", messages, smpId);
    }

    @Test
    @Transactional
    void testCreateParticipantCnameCaseSensitiveOK() throws Exception {
        final String partId = "0009:12345SHA256Test32";
        final String partIdLowerCase = StringUtils.lowerCase(partId);
        String caseSensitiveScheme = "sensitive-participant-sc1";
        final String smpId = SMP_ID_ONLY_CNAME;

        String messages = createParticipantAndResetDNS(partId, smpId, caseSensitiveScheme);
        String messagesLowerCase = createParticipantAndResetDNS(partIdLowerCase, smpId, caseSensitiveScheme);

        // then
        String expected1 = "B-f2bc7bd9e00b5323fd90b97fc547d4c2.sensitive-participant-sc1.acc.edelivery.tech.ec.europa.eu";
        String expected2 = "B-c78bb145a078b66e700fa24c38f287fe.sensitive-participant-sc1.acc.edelivery.tech.ec.europa.eu";

        assertNotEquals(expected1, expected2);

        assertDNSCnameRecord(expected1, messages, smpId);
        assertDNSCnameRecord(expected2, messagesLowerCase, smpId);
    }

    @Test
    @Transactional
    void testCreateParticipantOkSHA256Base32() throws Exception {
        customAuthentication.blueCoatAuthentication();
        final String partId = "0009:12345SHA256base32";
        final String scheme = "iso6523-actorid-upis";

        String messages = createParticipant(partId, SMP_ID_ONLY_NAPTR, scheme);

        assertDNSNAPTRRecord("PKDPMKVQJYUDLJQCI2KZ4UHOKZZM7JV44OJ7W2OPTUV75RKVO5DA.iso6523-actorid-upis.ehealth.acc.edelivery.tech.ec.europa.eu", messages, SMP_LOGICAL_ADDRESS_001);
    }

    @Test
    @Transactional
    void testCreateParticipantNAPTRCaseSensitiveOK() throws Exception {
        customAuthentication.blueCoatAuthentication();
        final String partId = "0009:12345SHA256Test32";
        final String partIdLowerCase = StringUtils.lowerCase(partId);
        String caseSensitiveScheme = "sensitive-participant-sc1";
        final String smpId = SMP_ID_ONLY_NAPTR;

        String messages = createParticipantAndResetDNS(partId, smpId, caseSensitiveScheme);
        String messagesLowerCase = createParticipantAndResetDNS(partIdLowerCase, smpId, caseSensitiveScheme);
        // then
        String expected1 = "NU3FN3G3JVG55BWUWFOHYP25TVA2SGZZ3PZE4OBXCIFXUCOELZKA.sensitive-participant-sc1.ehealth.acc.edelivery.tech.ec.europa.eu";
        String expected2 = "FJTMTXCNN7H2NJSA22EYKHRNTHUE45O7XE6BTCUVEREUYU36WZXQ.sensitive-participant-sc1.ehealth.acc.edelivery.tech.ec.europa.eu";

        assertNotEquals(expected1, expected2);
        assertDNSNAPTRRecord(expected1, messages, SMP_LOGICAL_ADDRESS_001);
        assertDNSNAPTRRecord(expected2, messagesLowerCase, SMP_LOGICAL_ADDRESS_001);
    }


    @Test
    @Transactional
    void testCreateParticipantOkSHA256Base32NullScheme() throws Exception {
        customAuthentication.blueCoatAuthentication();
        final String partId = "urn:oasis:names:tc:ebcore:partyid-type:iso6523:0088:4035811991021";
        final String scheme = null;

        String messages = createParticipant(partId, SMP_ID_ONLY_NAPTR, scheme);

        assertDNSNAPTRRecord("I3QYB36CTAYRFGTHBYCQZDTOJFHGAZJEGLFOOE7727EGVDWRK5QQ.ehealth.acc.edelivery.tech.ec.europa.eu", messages, SMP_LOGICAL_ADDRESS_001);
    }

    @Test
    @Transactional
    void testCreateParticipantOkSHA256Base32ebCoreId() throws Exception {
        customAuthentication.blueCoatAuthentication();
        final String partId = "4035811991021";
        final String scheme = "urn:oasis:names:tc:ebcore:partyid-type:iso6523:0088";

        String messages = createParticipant(partId, SMP_ID_ONLY_NAPTR, scheme);

        assertDNSNAPTRRecord("I3QYB36CTAYRFGTHBYCQZDTOJFHGAZJEGLFOOE7727EGVDWRK5QQ.ehealth.acc.edelivery.tech.ec.europa.eu", messages, SMP_LOGICAL_ADDRESS_001);
    }

    @Test
    void testCreateParticipantIllegalScheme2() {
        ServiceMetadataPublisherServiceForParticipantType participantType =
                createParticipantMetadata("domain", "0008:123456789", SMP_ID_ONLY_CNAME);

        BadRequestFault result = assertThrows(BadRequestFault.class,
                () -> manageParticipantIdentifierWS.create(participantType));
        assertThat(result.getMessage(),
                org.hamcrest.Matchers.containsString("[ERR-106] The Scheme Identifier MUST take the form {domain}-{identifierArea}-{identifierType}"));
    }


    @Test
    void testCreateParticipantForDifferentDomainCertificateNotOK() throws Exception {
        customAuthentication.blueCoatAuthentication();

        final String partId = "urn:ehealth:be:ncpb-idp";
        final String smpId = SMP_ID_ONLY_CNAME;
        final String scheme = "ehealth-ncp-ids";
        ServiceMetadataPublisherServiceForParticipantType participantType = createParticipantMetadata(scheme, partId, smpId);

        DnsMessageSenderServiceMock dnsMessageSenderService = (DnsMessageSenderServiceMock) this.dnsMessageSenderService;
        assertTrue(dnsMessageSenderService.getMessages().isEmpty());

        // First we ensure the participant doesn't exist
        ParticipantBO partBO = new ParticipantBO();
        partBO.setSmpId(smpId);
        partBO.setScheme(scheme);
        partBO.setParticipantId(partId);
        ParticipantBO found = participantDAO.findParticipant(partBO);
        assertNull(found);

        UnauthorizedFault result = assertThrows(UnauthorizedFault.class,
                () -> manageParticipantIdentifierWS.create(participantType));
        assertThat(result.getMessage(),
                org.hamcrest.Matchers.containsString("was not created with the certificate"));
    }

    @Test
    @Transactional
    void testCreateParticipantForDifferentDomainOK() throws Exception {
        customAuthentication.blueCoatAuthentication();

        final String partId = "urn:ehealth:be:ncpb-idp";
        final String scheme = "ehealth-ncp-ids";

        String messages = createParticipant(partId, SMP_ID_ONLY_NAPTR, scheme);

        assertThat(messages, containsString("L6WYVSCV5TIX6VAH5YLULNSJJMKR2ZHM65J7RRCFZ6JYYCV7JXGQ.ehealth-ncp-ids.ehealth.acc.edelivery.tech.ec.europa.eu.\t0\tANY\tANY"));
        assertThat(messages, containsString("L6WYVSCV5TIX6VAH5YLULNSJJMKR2ZHM65J7RRCFZ6JYYCV7JXGQ.ehealth-ncp-ids.ehealth.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tNAPTR\t100 10 \"U\" \"Meta:SMP\" \"!.*!http://logicalAddress!\" ."));
    }

    @Test
    @Transactional
    void testCreateParticipantWithBothDNSRecords() throws Exception {
        customAuthentication.blueCoatAuthentication("22591", "CN=SMP_TEST_FOR_EDELIVERY_22591,O=DG-DIGIT,C=BE", "CN=SMP_TEST_FOR_EDELIVERY_22591,O=DG-DIGIT,C=BE", true);

        final String partId = "0009:EDELIVERY-24091";
        final String smpId = "SMPTESTFOREDELIVERY22591";
        final String scheme = "iso6523-actorid-upis";

        String messages = createParticipant(partId, smpId, scheme);

        assertThat(messages, containsString("ITXKXIPDIDBT743GRXQGKICVZPITF5C423JJYX2FWABPSNT7OTBA.iso6523-actorid-upis.22591.acc.edelivery.tech.ec.europa.eu.\t0\tANY\tANY"));
        assertThat(messages, containsString("ITXKXIPDIDBT743GRXQGKICVZPITF5C423JJYX2FWABPSNT7OTBA.iso6523-actorid-upis.22591.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tNAPTR\t100 10 \"U\" \"Meta:SMP\" \"!.*!http://logicalAddress!\" ."));
        assertThat(messages, containsString("B-abe58f04d34e59cc6c3b130ca520ae0e.iso6523-actorid-upis.22591.acc.edelivery.tech.ec.europa.eu.\t0\tANY\tANY"));
        assertThat(messages, containsString("B-abe58f04d34e59cc6c3b130ca520ae0e.iso6523-actorid-upis.22591.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tCNAME\tSMPTESTFOREDELIVERY22591.publisher.22591.acc.edelivery.tech.ec.europa.eu."));
    }

    @Test
    @Transactional
    void testCreateParticipantWithCnameRecordOnly() throws Exception {
        customAuthentication.blueCoatAuthentication("22592", "CN=SMP_TEST_FOR_EDELIVERY_22592,O=DG-DIGIT,C=BE", "CN=SMP_TEST_FOR_EDELIVERY_22592,O=DG-DIGIT,C=BE", true);

        final String partId = "0009:EDELIVERY-24092";
        final String smpId = "SMPTESTFOREDELIVERY22592";
        final String scheme = "iso6523-actorid-upis";

        String messages = createParticipant(partId, smpId, scheme);

        assertFalse(messages.contains("NAPTR"));
        assertThat(messages, containsString("B-ad6469cdf052fe8a2e0b2a1e9c1373aa.iso6523-actorid-upis.22592.acc.edelivery.tech.ec.europa.eu.\t0\tANY\tANY"));
        assertThat(messages, containsString("B-ad6469cdf052fe8a2e0b2a1e9c1373aa.iso6523-actorid-upis.22592.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tCNAME\tSMPTESTFOREDELIVERY22592.publisher.22592.acc.edelivery.tech.ec.europa.eu."));
    }

    @Test
    @Transactional
    void testCreateParticipantWithNaptrRecordOnly() throws Exception {
        customAuthentication.blueCoatAuthentication("22593", "CN=SMP_TEST_FOR_EDELIVERY_22593,O=DG-DIGIT,C=BE", "CN=SMP_TEST_FOR_EDELIVERY_22593,O=DG-DIGIT,C=BE", true);

        final String partId = "0009:EDELIVERY-24093";
        final String smpId = "SMPTESTFOREDELIVERY22593";
        final String scheme = "iso6523-actorid-upis";

        String messages = createParticipant(partId, smpId, scheme);

        assertFalse(messages.contains("CNAME"));
        assertThat(messages, containsString("5WIV2CKKT65IJ2H6QMCZBVDWLF5EJACKFUGP4D26W44TREB3RXUQ.iso6523-actorid-upis.22593.acc.edelivery.tech.ec.europa.eu.\t0\tANY\tANY"));
        assertThat(messages, containsString("5WIV2CKKT65IJ2H6QMCZBVDWLF5EJACKFUGP4D26W44TREB3RXUQ.iso6523-actorid-upis.22593.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tNAPTR\t100 10 \"U\" \"Meta:SMP\" \"!.*!http://logicalAddress!\" ."));
    }

    private void resetDNSService() throws Exception {
        DnsMessageSenderServiceMock dnsMessageSenderService = (DnsMessageSenderServiceMock) this.dnsMessageSenderService;
        dnsMessageSenderService.reset();
    }

    private String createParticipantAndResetDNS(String partId, String smpId, String scheme) throws Exception {
        String messages = createParticipant(partId, smpId, scheme);
        resetDNSService();
        return messages;
    }

    private String createParticipant(String partId, String smpId, String scheme) throws Exception {
        ServiceMetadataPublisherServiceForParticipantType participantType = createParticipantMetadata(scheme, partId, smpId);

        DnsMessageSenderServiceMock dnsMessageSenderService = (DnsMessageSenderServiceMock) this.dnsMessageSenderService;
        assertTrue(dnsMessageSenderService.getMessages().isEmpty());

        // First we ensure the participant doesn't exist
        ParticipantBO partBO = new ParticipantBO();
        partBO.setSmpId(smpId);
        partBO.setScheme(scheme);
        partBO.setParticipantId(partId);

        manageParticipantIdentifierBusiness.normalizeAndValidateParticipant(partBO);


        ParticipantBO found = participantDAO.findParticipant(partBO);
        assertNull(found);

        manageParticipantIdentifierWS.create(participantType);

        // Finally we verify that the participant has been created
        found = participantDAO.findParticipant(partBO);
        assertNotNull(found);
        participantDAO.deleteParticipant(found);

        return dnsMessageSenderService.getMessages();
    }


    void assertDNSCnameRecord(String domain, String messages, String smpId) {
        assertThat(messages, containsString(domain + ".\t0\tANY\tANY"));
        assertThat(messages, containsString(domain + ".\t60\tIN\tCNAME\t" + smpId + ".publisher.acc.edelivery.tech.ec.europa.eu."));
    }

    void assertDNSNAPTRRecord(String domain, String messages, String smpLogicalAddress) {
        assertThat(messages, containsString(domain + ".\t0\tANY\tANY"));
        assertThat(messages, containsString(domain + ".\t60\tIN\tNAPTR\t100 10 \"U\" \"Meta:SMP\" \"!.*!" + smpLogicalAddress + "!\" ."));
    }
}

