/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.business.impl;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.business.IManageParticipantIdentifierBusiness;
import eu.europa.ec.bdmsl.common.bo.ParticipantBO;
import eu.europa.ec.bdmsl.common.enums.SMLRoleEnum;
import eu.europa.ec.bdmsl.common.exception.BadRequestException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.test.CommonTestUtils;
import eu.europa.ec.bdmsl.security.CertificateAuthentication;
import eu.europa.ec.bdmsl.security.SMLAuthenticationProvider;
import eu.europa.ec.bdmsl.security.UnsecureAuthentication;
import eu.europa.ec.edelivery.security.ClientCertAuthenticationFilter;
import eu.europa.ec.edelivery.security.PreAuthenticatedCertificatePrincipal;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;


/**
 * @author Adrien FERIAL
 * @since 26/06/2015
 */
class ManageParticipantIdentifierBusinessImplTest extends AbstractJUnit5Test {

    @Autowired
    private IManageParticipantIdentifierBusiness testInstance;

    @Autowired
    private IdentifierFormatterBusinessImpl identifierFormatterBusiness;

    @Autowired
    private SMLAuthenticationProvider smlAuthenticationProvider;

    ClientCertAuthenticationFilter blueCoatAuthenticationFilter = new ClientCertAuthenticationFilter();

    @BeforeEach
    void before() {
        UnsecureAuthentication authentication = new UnsecureAuthentication();
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
    }

    private void loginToEHealthDomain() throws Exception {
        String certHeaderValue = CommonTestUtils.createHeaderCertificateForBlueCoat("CN=EHEALTH_SMP_77777777,O=DG-DIGIT,C=BE", false);

        PreAuthenticatedCertificatePrincipal principal = blueCoatAuthenticationFilter.buildPrincipalFromHeader(certHeaderValue);
        Authentication bcAuth = new CertificateAuthentication(principal, Collections.singletonList(SMLRoleEnum.ROLE_SMP.getAuthority()));

        Authentication authentication = smlAuthenticationProvider.authenticate(bcAuth);

        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Test
    void testValidateParticipantEmpty() {
        BadRequestException result = assertThrows(BadRequestException.class,
                () -> testInstance.normalizeAndValidateParticipant(new ParticipantBO()));
        assertThat(result.getMessage(),
                org.hamcrest.Matchers.containsString("[ERR-106] Identifier must not be 'null' or empty"));
    }

    @Test
    void testValidateParticipantWrongScheme() {
        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId("participantId");
        participantBO.setSmpId("smpId");
        participantBO.setScheme("&é'"); // must be like '<domain>-<identifierArea>-<identifier type>'

        BadRequestException result = assertThrows(BadRequestException.class,
                () -> testInstance.normalizeAndValidateParticipant(participantBO));
        assertThat(result.getMessage(),
                org.hamcrest.Matchers.containsString("[ERR-106] The Scheme Identifier MUST take the form {domain}-{identifierArea}-{identifierType} such as for example 'busdox-actorid-upis'. It may only contain the following characters: [a-z0-9]+-[a-z0-9]+-[a-z0-9]+"));

    }

    @Test
    void testNormalizeParticipantEbCoreSchemeCanBeEmpty() throws Exception {
        loginToEHealthDomain();

        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId("urn:oasis:names:tc:ebcore:partyid-type:iso6523:0088:4035811991021");
        participantBO.setSmpId("smpId");
        participantBO.setScheme("");
        testInstance.normalizeAndValidateParticipant(participantBO);

        assertEquals("urn:oasis:names:tc:ebcore:partyid-type:iso6523:0088", participantBO.getScheme());
        assertEquals("4035811991021", participantBO.getParticipantId());
    }

    @Test
    void testNormalizeParticipantEbCoreSchemeCanBeNull() throws Exception {
        loginToEHealthDomain();

        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId("urn:oasis:names:tc:ebcore:partyid-type:iso6523:0088:4035811991021");
        participantBO.setSmpId("smpId");
        participantBO.setScheme(null);
        testInstance.normalizeAndValidateParticipant(participantBO);

        assertEquals("urn:oasis:names:tc:ebcore:partyid-type:iso6523:0088", participantBO.getScheme());
        assertEquals("4035811991021", participantBO.getParticipantId());

    }

    @Test
    void testValidateParticipantWrongParticipantIdentifierForDefaultScheme() {

        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId("participantId"); // must be like '0010:5798000000001'
        participantBO.setSmpId("smpId");
        participantBO.setScheme("iso6523-actorid-upis");

        assertThrows(BadRequestException.class,
                () -> testInstance.normalizeAndValidateParticipant(participantBO));
    }

    @Test
    void testValidateParticipantIdValueTooLong() {
        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId("9952:DE12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890" +
                "123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
        participantBO.setSmpId("smpId");
        participantBO.setScheme("iso6523-actorid-upis");

        assertThrows(BadRequestException.class,
                () -> testInstance.normalizeAndValidateParticipant(participantBO));
    }

    @Test
    void testValidateParticipantIdNonAscii() {
        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId("9956:DE:EPROC:BMIEVG:BeschéA");
        participantBO.setSmpId("smpId");
        participantBO.setScheme("iso6523-actorid-upis");

        assertThrows(BadRequestException.class,
                () -> testInstance.normalizeAndValidateParticipant(participantBO));
    }

    @Test
    void testValidateParticipantOk() {
        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId("9954:5798000000001");
        participantBO.setSmpId("smpId");
        participantBO.setScheme("iso6523-actorid-upis");
        assertDoesNotThrow(() -> testInstance.normalizeAndValidateParticipant(participantBO));
    }

    @Test
    void testValidateParticipantDefaultSchemeWrongParticipantIdentifier() {
        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId("unknown:5798000000001");
        participantBO.setSmpId("smpId");
        participantBO.setScheme("iso6523-actorid-upis");

        assertThrows(BadRequestException.class,
                () -> testInstance.normalizeAndValidateParticipant(participantBO));
    }

    /**
     * This method tests participant id for SE:VAT PEPPOL code.
     * <p>
     * TODO validate the meaning of BMIEVG:BeschA
     */
    @Test
    void testValidateParticipantSEVAT() {
        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId("9955:SE:VAT:BMIEVG:BeschA");
        participantBO.setSmpId("smpId");
        participantBO.setScheme("iso6523-actorid-upis");
        assertDoesNotThrow(() -> testInstance.normalizeAndValidateParticipant(participantBO));
    }

    /**
     * This method tests participant id for BE:CBE PEPPOL code.
     * <p>
     * TODO validate the meaning of BMIEVG:BeschA
     */
    @Test
    void testValidateParticipantBECBE() {
        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId("9956:BE:CBE:BMIEVG:BeschA");
        participantBO.setSmpId("smpId");
        participantBO.setScheme("iso6523-actorid-upis");
        assertDoesNotThrow(() -> testInstance.normalizeAndValidateParticipant(participantBO));
    }

    /**
     * This method tests participant id for FR:VAT PEPPOL code.
     * <p>
     * TODO validate the meaning of BMIEVG:BeschA
     */
    @Test
    void testValidateParticipantFRVAT() {
        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId("9957:FR:VAT:BMIEVG:BeschA");
        participantBO.setSmpId("smpId");
        participantBO.setScheme("iso6523-actorid-upis");
        assertDoesNotThrow(() -> testInstance.normalizeAndValidateParticipant(participantBO));
    }

    /**
     * This method tests participant id for DIGST PEPPOL code.
     * <p>
     * TODO validate the meaning of BMIEVG:BeschA
     */
    @Test
    void testValidateParticipantDIGST() {
        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId("0184:DIGSTORG:BMIEVG:BeschA");
        participantBO.setSmpId("smpId");
        participantBO.setScheme("iso6523-actorid-upis");
        assertDoesNotThrow(() -> testInstance.normalizeAndValidateParticipant(participantBO));
    }

    @ParameterizedTest
    @NullAndEmptySource
    void testValidateParticipantNullSchemeOk(String scheme) {

        identifierFormatterBusiness.setSchemeMandatory(false);

        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId("0184:DIGSTORG:BMIEVG:BeschA");
        participantBO.setSmpId("smpId");
        participantBO.setScheme(scheme);
        // then
        assertDoesNotThrow(() -> testInstance.normalizeAndValidateParticipant(participantBO));
    }

    @ParameterizedTest
    @NullAndEmptySource
    void testValidateParticipantNullSchemeNotOk(String scheme) {

        identifierFormatterBusiness.setSchemeMandatory(true);

        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId("0184:DIGSTORG:BMIEVG:BeschA");
        participantBO.setSmpId("smpId");
        participantBO.setScheme(scheme);
        // then
        BadRequestException result = assertThrows(BadRequestException.class, () -> testInstance.normalizeAndValidateParticipant(participantBO));
        assertThat(result.getMessage(), CoreMatchers.containsString("[ERR-106] Invalid Identifier:"));
    }

    @ParameterizedTest
    @MethodSource("eu.europa.ec.bdmsl.util.ConstraintsUtilTest#participantIdsOkStream")
    void testIdentifierRegexValidationOk(String identifier) {
        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setScheme("iso6523-actorid-upis");
        participantBO.setParticipantId(identifier);
        assertDoesNotThrow(() -> testInstance.normalizeAndValidateParticipant(participantBO));
    }

    @ParameterizedTest
    @MethodSource("eu.europa.ec.bdmsl.util.ConstraintsUtilTest#participantIdsNotOkStream")
    void testIdentifierRegexValidationNotOk2(String identifier) {
        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setScheme("iso6523-actorid-upis");
        participantBO.setParticipantId(identifier);

        BadRequestException result = assertThrows(BadRequestException.class,
                () -> testInstance.normalizeAndValidateParticipant(participantBO));
        assertEquals("[ERR-106] Identifier value " + identifier + " is illegal.", result.getMessage());
    }

    @ParameterizedTest
    @CsvSource({
            "urn:oasis:names:tc:ebcore:partyid-type:unregistered:domain:ec.europa.eu, urn:oasis:names:tc:ebcore:partyid-type:unregistered:domain, ec.europa.eu",
            "urn:oasis:names:tc:ebcore:partyid-type:iso6523:0088:123456789, urn:oasis:names:tc:ebcore:partyid-type:iso6523:0088, 123456789",
            "urn:oasis:names:tc:ebcore:partyid-type:unregistered:ehealth:urn:ehealth:pl:ncp-idp, urn:oasis:names:tc:ebcore:partyid-type:unregistered:ehealth, urn:ehealth:pl:ncp-idp",
            "urn:oasis:names:tc:ebcore:partyid-type:unregistered:ehealth:pl:ncp-idp, urn:oasis:names:tc:ebcore:partyid-type:unregistered:ehealth, pl:ncp-idp",
            "urn:oasis:names:tc:ebcore:partyid-type:unregistered:blue-gw, urn:oasis:names:tc:ebcore:partyid-type:unregistered, blue-gw",
            "urn:oasis:names:tc:ebcore:partyid-type:unregistered:ec.europa.eu, urn:oasis:names:tc:ebcore:partyid-type:unregistered, ec.europa.eu",
            "' urn:oasis:names:tc:ebcore:partyid-type:iso6523:0088:123456789', urn:oasis:names:tc:ebcore:partyid-type:iso6523:0088, 123456789",
            "'urn:oasis:names:tc:ebcore:partyid-type:iso6523:0088:123456789 ', urn:oasis:names:tc:ebcore:partyid-type:iso6523:0088, 123456789",
    })
    void normalizeEBCoreIdentifiers(String input, String scheme, String value) throws TechnicalException {
        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId(input);
        participantBO.setSmpId("smpId");
        testInstance.normalizeAndValidateParticipant(participantBO);

        //then
        assertEquals(scheme, participantBO.getScheme());
        assertEquals(value, participantBO.getParticipantId());
    }

}

