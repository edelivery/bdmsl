/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.util;

import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.function.Executable;

import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 *  The various assertions for DomiSML project.
 * @author Joze RIHTARSIC
 * @since 5.0
 */
public class DomiSMLAssertions {



    public static void assertThrowsWithContainsMessage(Class<? extends Throwable>  exceptionClass, Executable executable, String message) {
        Throwable exception = assertThrows(exceptionClass, executable);
        MatcherAssert.assertThat(exception.getMessage(), containsString(message));
    }
}
