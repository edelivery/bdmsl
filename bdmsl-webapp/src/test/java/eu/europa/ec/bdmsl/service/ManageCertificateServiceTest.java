/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.bo.CertificateBO;

import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.dao.ICertificateDAO;
import eu.europa.ec.bdmsl.dao.ISmpDAO;
import eu.europa.ec.bdmsl.dao.IWildcardDAO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Adrien FERIAL
 * @since 16/06/2015
 */
class ManageCertificateServiceTest extends AbstractJUnit5Test {

    @Autowired
    private IManageCertificateService manageCertificateService;

    @Autowired
    private ICertificateDAO certificateDAO;

    @Autowired
    private ISmpDAO smpDAO;

    @Autowired
    private IWildcardDAO wildcardDAO;

    @Test
    void testChangeCertificates() throws TechnicalException {
        assertEquals("CN=SMP_TEST_CHANGE_CERTIFICATE_SCHEDULER,O=DG-DIGIT,C=BE:0000000000000000000000000456efgh", smpDAO.findSMP("smpForChangeCertificateTest").getCertificateId());
        CertificateBO certificateBO = new CertificateBO();
        certificateBO.setId(4l);
        assertEquals("CN=SMP_TEST_CHANGE_CERTIFICATE_SCHEDULER,O=DG-DIGIT,C=BE:0000000000000000000000000456efgh", wildcardDAO.findWildcard("iso6523-actorid-upis", certificateBO).getCertificateId());
        assertNotNull(certificateDAO.findCertificateByCertificateId("CN=SMP_TEST_CHANGE_CERTIFICATE_SCHEDULER,O=DG-DIGIT,C=BE:0000000000000000000000000456efgh"));

        manageCertificateService.changeCertificates();
        assertEquals("CN=test:123", smpDAO.findSMP("smpForChangeCertificateTest").getCertificateId());
        certificateBO.setId(2l);
        assertEquals("CN=test:123", wildcardDAO.findWildcard("iso6523-actorid-upis", certificateBO).getCertificateId());
        assertNull(certificateDAO.findCertificateByCertificateId("CN=SMP_TEST_CHANGE_CERTIFICATE_SCHEDULER,O=DG-DIGIT,C=BE:0000000000000000000000000456efgh"));
    }
}
