/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.ws.soap;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.bo.ParticipantBO;
import eu.europa.ec.bdmsl.common.enums.SMLRoleEnum;
import eu.europa.ec.bdmsl.dao.IParticipantDAO;
import eu.europa.ec.bdmsl.test.DnsMessageSenderServiceMock;
import eu.europa.ec.bdmsl.security.ICertificateAuthentication;
import eu.europa.ec.bdmsl.security.UnsecureAuthentication;
import eu.europa.ec.bdmsl.service.dns.IDnsMessageSenderService;
import eu.europa.ec.bdmsl.test.SecurityTestUtils;
import org.busdox.servicemetadata.locator._1.ParticipantIdentifierPageType;
import org.busdox.transport.identifiers._1.ParticipantIdentifierType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.security.Security;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Adrien FERIAL
 * @since 16/06/2015
 */
class ManageParticipantIdentifierWSCreateListTest extends AbstractJUnit5Test {

    @Autowired
    private IManageParticipantIdentifierWS manageParticipantIdentifierWS;

    @Autowired
    private IParticipantDAO participantDAO;

    @Autowired
    private ICertificateAuthentication customAuthentication;

    @Autowired
    private IDnsMessageSenderService dnsMessageSenderService;

    @BeforeEach
    void before() throws Exception {
        UnsecureAuthentication authentication = new UnsecureAuthentication();
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    @Test
    void testCreateListEmpty() {
        ParticipantIdentifierPageType participantType = new ParticipantIdentifierPageType();
        assertThrows(BadRequestFault.class, () -> manageParticipantIdentifierWS.createList(participantType));
    }

    @Test
    void testCreateListNull() {
        assertThrows(BadRequestFault.class, () -> manageParticipantIdentifierWS.createList(null));
    }

    @Test
    void testCreateListParticipantWithoutExistingSMP() {
        ParticipantIdentifierPageType participantType = createParticipant();
        participantType.getParticipantIdentifiers().get(0).setValue("0009:723456789");
        participantType.getParticipantIdentifiers().get(1).setValue("0009:823456789");
        assertThrows(NotFoundFault.class, () -> manageParticipantIdentifierWS.createList(participantType));
    }

    private ParticipantIdentifierPageType createParticipant() {
        ParticipantIdentifierType participantType1 = new ParticipantIdentifierType();
        participantType1.setScheme("iso6523-actorid-upis");

        ParticipantIdentifierType participantType2 = new ParticipantIdentifierType();
        participantType2.setScheme("iso6523-actorid-upis");

        ParticipantIdentifierPageType participantIdentifierPageType = new ParticipantIdentifierPageType();
        participantIdentifierPageType.getParticipantIdentifiers().add(participantType1);
        participantIdentifierPageType.getParticipantIdentifiers().add(participantType2);
        participantIdentifierPageType.setServiceMetadataPublisherID("NotFound");

        return participantIdentifierPageType;
    }

    @Test
    void testCreateListParticipantWithSMPNotCreatedByUser() {
        SecurityTestUtils.authenticateWithRoleClientCert02(SMLRoleEnum.ROLE_SMP);
        ParticipantIdentifierPageType participantType = createParticipant();
        participantType.getParticipantIdentifiers().get(0).setValue("0009:523456789");
        participantType.getParticipantIdentifiers().get(1).setValue("0009:623456789");
        participantType.setServiceMetadataPublisherID("found");

        assertThrows(UnauthorizedFault.class, () -> manageParticipantIdentifierWS.createList(participantType));
    }

    @Test
    void testCreateListParticipantAlreadyExist() {
        ParticipantIdentifierPageType participantType = createParticipant();
        participantType.setServiceMetadataPublisherID("foundUnsecure");
        assertThrows(BadRequestFault.class, () -> manageParticipantIdentifierWS.createList(participantType));
    }

    @Test
    void testCreateListParticipantOk() throws Exception {
        //GIVEN
        // First we ensure the participants don't exist
        final String partId1 = "0009:323456789";
        final String partId2 = "0009:423456789";
        final String smpId = "foundUnsecure";
        ParticipantBO partBO = new ParticipantBO();
        partBO.setSmpId(smpId);
        partBO.setScheme("iso6523-actorid-upis");
        partBO.setParticipantId(partId1);
        ParticipantBO found = participantDAO.findParticipant(partBO);
        assertNull(found);
        partBO.setParticipantId(partId2);
        found = participantDAO.findParticipant(partBO);
        assertNull(found);

        // Then we create the participants
        ParticipantIdentifierPageType participantType = createParticipant();
        participantType.getParticipantIdentifiers().get(0).setValue(partId1);
        participantType.getParticipantIdentifiers().get(1).setValue(partId2);
        participantType.setServiceMetadataPublisherID(smpId);

        //WHEN
        manageParticipantIdentifierWS.createList(participantType);

        //THEN
        // Finally we verify that the participants have been created
        found = participantDAO.findParticipant(partBO);
        assertNotNull(found);
        partBO.setParticipantId(partId1);
        found = participantDAO.findParticipant(partBO);
        assertNotNull(found);
    }

    @Test
    void testCreateListParticipantIllegalIssuingAgency() {
        ParticipantIdentifierPageType participantType = createParticipant();
        participantType.setServiceMetadataPublisherID("foundUnsecure");
        assertThrows(BadRequestFault.class, () -> manageParticipantIdentifierWS.createList(participantType));
    }

    @Test
    void testCreateListParticipantForDifferentDomainOk() throws Exception {
        //GIVEN
        customAuthentication.blueCoatAuthentication("123456789101112");

        final String partId1 = "urn:ehealth:fr:ncpb-idp";
        final String partId2 = "urn:ehealth:pt:ncpb-idp";
        final String smpId = "SMP123456789101112";
        final String scheme = "ehealth-ncp-ids";
        ParticipantBO partBO = new ParticipantBO();
        partBO.setSmpId(smpId);
        partBO.setScheme(scheme);
        partBO.setParticipantId(partId1);
        ParticipantBO found = participantDAO.findParticipant(partBO);
        assertNull(found);
        partBO.setParticipantId(partId2);
        found = participantDAO.findParticipant(partBO);
        assertNull(found);

        ParticipantIdentifierPageType participantType = createParticipant();
        participantType.getParticipantIdentifiers().get(0).setValue(partId1);
        participantType.getParticipantIdentifiers().get(0).setScheme(scheme);
        participantType.getParticipantIdentifiers().get(1).setValue(partId2);
        participantType.getParticipantIdentifiers().get(1).setScheme(scheme);
        participantType.setServiceMetadataPublisherID(smpId);

        DnsMessageSenderServiceMock dnsMessageSenderService = (DnsMessageSenderServiceMock) this.dnsMessageSenderService;
        assertTrue(dnsMessageSenderService.getMessages().isEmpty());

        //WHEN
        manageParticipantIdentifierWS.createList(participantType);

        //THEN
        found = participantDAO.findParticipant(partBO);
        assertNotNull(found);
        partBO.setParticipantId(partId1);
        found = participantDAO.findParticipant(partBO);
        assertNotNull(found);

        String messages = dnsMessageSenderService.getMessages();
        assertFalse(messages.contains("B-7064f0a40b0562e3460ce40bb53c5b2a."));
        assertTrue(messages.contains("CM3OMYVFXRSRNDRPEROD6DZVB6MXHYQ64XEFCQF27XNC55NVVHVQ.ehealth-ncp-ids.ehealth.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tNAPTR\t100 10 \"U\" \"Meta:SMP\" \"!.*!https://logicalAddress.com.eu!\" ."));
    }

    @Test
    void testCreateListParticipantForDifferentDomainCertificateNotOk() throws Exception {
        //GIVEN
        customAuthentication.blueCoatAuthentication();
        final String partId1 = "urn:ehealth:uk:ncpb-idp";
        final String partId2 = "urn:ehealth:es:ncpb-idp";
        final String smpId = "SMP123456789101112";
        final String scheme = "ehealth-ncp-ids";
        ParticipantBO partBO = new ParticipantBO();
        partBO.setSmpId(smpId);
        partBO.setScheme(scheme);
        partBO.setParticipantId(partId1);
        ParticipantBO found = participantDAO.findParticipant(partBO);
        assertNull(found);
        partBO.setParticipantId(partId2);
        found = participantDAO.findParticipant(partBO);
        assertNull(found);

        ParticipantIdentifierPageType participantType = createParticipant();
        participantType.getParticipantIdentifiers().get(0).setValue(partId1);
        participantType.getParticipantIdentifiers().get(0).setScheme(scheme);
        participantType.getParticipantIdentifiers().get(1).setValue(partId2);
        participantType.getParticipantIdentifiers().get(1).setScheme(scheme);
        participantType.setServiceMetadataPublisherID(smpId);

        //WHEN THEN
        assertThrows(UnauthorizedFault.class, () -> manageParticipantIdentifierWS.createList(participantType));
    }
}

