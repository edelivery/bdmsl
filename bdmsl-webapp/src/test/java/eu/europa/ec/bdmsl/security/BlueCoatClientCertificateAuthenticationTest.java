/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.security;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.enums.SMLRoleEnum;
import eu.europa.ec.bdmsl.common.util.Constant;
import eu.europa.ec.bdmsl.test.CommonTestUtils;
import eu.europa.ec.bdmsl.util.HttpConnectionMock;
import eu.europa.ec.edelivery.exception.ClientCertParseException;
import eu.europa.ec.edelivery.security.ClientCertAuthenticationFilter;
import eu.europa.ec.edelivery.security.PreAuthenticatedCertificatePrincipal;
import eu.europa.ec.edelivery.security.cert.IRevocationValidator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.test.util.ReflectionTestUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Adrien FERIAL
 * @since 22/06/2015
 */
class BlueCoatClientCertificateAuthenticationTest extends AbstractJUnit5Test {


    private final ClientCertAuthenticationFilter blueCoatAuthenticationFilter = new ClientCertAuthenticationFilter();

    @Autowired
    IRevocationValidator crlVerifierService;

    @BeforeEach
    protected void initTest() {
        crlVerifierService = Mockito.spy(crlVerifierService);
        ReflectionTestUtils.setField(smlAuthenticationProvider, "crlVerifierService", crlVerifierService);
        // set test mock for testing crl lists
        HttpConnectionMock httpConnectionMock = new HttpConnectionMock("test.crl");
        crlVerifierService.setHttpConnection(httpConnectionMock);
    }

    private Authentication getAuthenticationFromHeader(String certHeaderValue) {
        PreAuthenticatedCertificatePrincipal principal = blueCoatAuthenticationFilter.buildPrincipalFromHeader(certHeaderValue);
        return new CertificateAuthentication(principal, Collections.emptyList());
    }

    @Test
    void testRead() {
        String serial = "123ABCD";
        String issuer = "CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA,OU=FOR TEST PURPOSES ONLY,O=NATIONAL IT AND TELECOM AGENCY,C=DK";
        String subject = "O=DG-DIGIT,CN=SMP_1000000007,C=BE";
        DateFormat df = new SimpleDateFormat("MMM d hh:mm:ss yyyy zzz", Constant.LOCALE);

        Calendar validFrom = CommonTestUtils.getPastYearDate(2);
        Calendar validTo = CommonTestUtils.getFutureYearDate(20);

        String certHeaderValue = "serial=" + serial + "&subject=" + subject + "&validFrom=" + df.format(validFrom.getTime()) + "&validTo=" + df.format(validTo.getTime()) + "&issuer=" + issuer;
        Authentication authentication = getAuthenticationFromHeader(certHeaderValue);
        // then
        Authentication bcAuth = smlAuthenticationProvider.authenticate(authentication);


        assertEquals(serial.toLowerCase(), ((CertificateDetails) bcAuth.getDetails()).getSerial());
        assertEquals(issuer, ((CertificateDetails) bcAuth.getDetails()).getIssuer());
        assertEquals("CN=SMP_1000000007,O=DG-DIGIT,C=BE", ((CertificateDetails) bcAuth.getDetails()).getSubject());
        assertEquals(df.format(validFrom.getTime()), df.format(((CertificateDetails) bcAuth.getDetails()).getValidFrom().getTime()));
        assertEquals(df.format(validTo.getTime()), df.format(((CertificateDetails) bcAuth.getDetails()).getValidTo().getTime()));

        assertEquals("CN=SMP_1000000007,O=DG-DIGIT,C=BE:0000000000000000000000000123abcd", bcAuth.getName());
        assertEquals("ROLE_SMP", ((List<?>) bcAuth.getAuthorities()).get(0).toString());

    }

    /**
     * The order of the certificate attributes is different and there are spaces after the commas. The certificate must be valid anyway
     */
    @Test
    void testReadDifferentOrderWithSpaces() {
        String serial = "123ABCD";
        String issuer = "C=DK, CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA, O=NATIONAL IT AND TELECOM AGENCY, OU=FOR TEST PURPOSES ONLY";
        String issuerNormalized = "CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA,OU=FOR TEST PURPOSES ONLY,O=NATIONAL IT AND TELECOM AGENCY,C=DK";
        String subject = "C=BE, O=DG-DIGIT, CN=SMP_1000000007";
        DateFormat df = new SimpleDateFormat("MMM d hh:mm:ss yyyy zzz", Constant.LOCALE);

        Calendar validFrom = CommonTestUtils.getPastYearDate(2);
        Calendar validTo = CommonTestUtils.getFutureYearDate(20);

        String certHeaderValue = "serial=" + serial + "&subject=" + subject + "&validFrom=" + df.format(validFrom.getTime()) + "&validTo=" + df.format(validTo.getTime()) + "&issuer=" + issuer;
        Authentication authentication = getAuthenticationFromHeader(certHeaderValue);
        // then
        Authentication bcAuth = smlAuthenticationProvider.authenticate(authentication);


        assertEquals(serial.toLowerCase(), ((CertificateDetails) bcAuth.getDetails()).getSerial());
        assertEquals(issuerNormalized, ((CertificateDetails) bcAuth.getDetails()).getIssuer());
        assertEquals("CN=SMP_1000000007,O=DG-DIGIT,C=BE", ((CertificateDetails) bcAuth.getDetails()).getSubject());
        assertEquals(df.format(validFrom.getTime()), df.format(((CertificateDetails) bcAuth.getDetails()).getValidFrom().getTime()));
        assertEquals(df.format(validTo.getTime()), df.format(((CertificateDetails) bcAuth.getDetails()).getValidTo().getTime()));
        assertEquals("CN=SMP_1000000007,O=DG-DIGIT,C=BE:0000000000000000000000000123abcd", bcAuth.getName());
        assertEquals("ROLE_SMP", ((List<?>) bcAuth.getAuthorities()).get(0).toString());
    }

    /**
     * The order of the header (serial, subject, etc.) is different
     */
    @Test
    void testReadDifferentOrderWithSpaces2() {
        String serial = "123ABCD";
        // different order for the issuer certificate with extra spaces
        String issuer = "CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA,  C=DK, O=NATIONAL IT AND TELECOM AGENCY,    OU=FOR TEST PURPOSES ONLY";
        String issuerNormalized = "CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA,OU=FOR TEST PURPOSES ONLY,O=NATIONAL IT AND TELECOM AGENCY,C=DK";
        String subject = "C=BE, O=DG-DIGIT, CN=SMP_1000000007";
        DateFormat df = new SimpleDateFormat("MMM d hh:mm:ss yyyy zzz", Constant.LOCALE);

        Calendar validFrom = CommonTestUtils.getPastYearDate(2);
        Calendar validTo = CommonTestUtils.getFutureYearDate(20);

        // case insensitivity test
        String certHeaderValue = "iSsUeR=" + issuer + "&VaLiDFrOm=" + df.format(validFrom.getTime()) + "&sUbJecT=" + subject + "&VALidTo=" + df.format(validTo.getTime()) + "&serIAL=" + serial;
        Authentication authentication = getAuthenticationFromHeader(certHeaderValue);
        // then
        Authentication bcAuth = smlAuthenticationProvider.authenticate(authentication);


        assertEquals(serial.toLowerCase(), ((CertificateDetails) bcAuth.getDetails()).getSerial());
        assertEquals(issuerNormalized, ((CertificateDetails) bcAuth.getDetails()).getIssuer());
        assertEquals("CN=SMP_1000000007,O=DG-DIGIT,C=BE", ((CertificateDetails) bcAuth.getDetails()).getSubject());
        assertEquals(df.format(validFrom.getTime()), df.format(((CertificateDetails) bcAuth.getDetails()).getValidFrom().getTime()));
        assertEquals(df.format(validTo.getTime()), df.format(((CertificateDetails) bcAuth.getDetails()).getValidTo().getTime()));

        assertEquals("CN=SMP_1000000007,O=DG-DIGIT,C=BE:0000000000000000000000000123abcd", bcAuth.getName());
        assertEquals("ROLE_SMP", ((List<?>) bcAuth.getAuthorities()).get(0).toString());
    }

    /**
     * Test the construction of the Authenticator from real values from the BlueCoat proxy
     */
    @Test
    void calculateCertificateId() {
        String certHeaderValue = "sno=53%3Aef%3A79%3Ac3%3A54%3A98%3Abb%3A63%3A38%3A35%3A9a%3A19%3A5d%3A2d%3Ad8%3A8c&subject=C%3DBE%2C+O%3DDG-DIGIT%2C+CN%3DSMP_1000000007&validfrom=Oct+21+00%3A00%3A00+2014+GMT&validto=Oct+20+23%3A59%3A59+2030+GMT&issuer=C%3DDK%2C+O%3DNATIONAL+IT+AND+TELECOM+AGENCY%2C+OU%3DFOR+TEST+PURPOSES+ONLY%2C+CN%3DPEPPOL+SERVICE+METADATA+PUBLISHER+TEST+CA";
        Authentication authentication = getAuthenticationFromHeader(certHeaderValue);
        // then
        Authentication bcAuth = smlAuthenticationProvider.authenticate(authentication);

        assertEquals("CN=SMP_1000000007,O=DG-DIGIT,C=BE:53ef79c35498bb6338359a195d2dd88c", bcAuth.getName());
    }

    @Test
    void calculateCertificateIdImpossibleToDetermineTheCertificateIdentifier() {
        String certHeaderValue = "snf=53%3Aef%3A79%3Ac3%3A54%3A98%3Abb%3A63%3A38%3A35%A9a%3A19%3A5d%3A2d%3Ad8%3A8c&subject=C%3DBE%2C+O%3DDG-DIGIT%2C+CN%3DSMP_1000000007&validfrom=Oct+21+00%3A00%3A00+2014+GMT&validto=Oct+20+23%3A59%3A59+2016+GMT&issuer=C%3DDK%2C+O%3DNATIONAL+IT+AND+TELECOM+AGENCY%2C+OU%3DFOR+TEST+PURPOSES+ONLY%2C+CN%3DPEPPOL+SERVICE+METADATA+PUBLISHER+TEST+CA";
        assertThrows(ClientCertParseException.class, () -> {
            Authentication authentication = getAuthenticationFromHeader(certHeaderValue);
            Authentication bcAuth = smlAuthenticationProvider.authenticate(authentication);
            // then
            assertEquals("CN=SMP_1000000007,O=DG-DIGIT,C=BE:53ef79c35498bb6338359a195d2dd88c", bcAuth.getName());
        });


    }

    @Test
    void calculateCertificateIdImpossibleToIdentifyAuthoritiesForCertificate() {
        String certHeaderValue = "sno=53%3Aef%3A79%3Ac3%3A54%3A98%3Abb%3A63%3A38%3A35%3A9a%3A19%3A5d%3A2d%3Ad8%3A8c&subject=C%3FPT%2C+O%3DDG-DIGIT%2C+CN%3DSMP_1000000007&validfrom=Oct+21+00%3A00%3A00+2014+GMT&validto=Oct+20+23%3A59%3A59+2016+GMT&issuer=C%3DDK%2C+O%3DNATIONAL+IT+AND+TELECOM+AGENCY%2C+OU%3DFOR+TEST+PURPOSES+ONLY%2C+CN%3DPEPPOL+SERVICE+METADATA+PUBLISHER+TEST+CA";
        assertThrows(ClientCertParseException.class, () -> getAuthenticationFromHeader(certHeaderValue));
    }

    @Test
    void grantSMPRoleForNonRootCATrustedCertificate() throws Exception {
        String certHeaderValue = CommonTestUtils.createHeaderCertificateForBlueCoat();
        Authentication preAuth = getAuthenticationFromHeader(certHeaderValue);
        CertificateAuthentication authentication = (CertificateAuthentication) smlAuthenticationProvider.authenticate(preAuth);
        PreAuthenticatedCertificatePrincipal principal = (PreAuthenticatedCertificatePrincipal) authentication.getPrincipal();
        // then
        assertFalse(authentication.isRootPKICertificate());
        assertEquals("CN=SMP_123456789,O=DG-DIGIT,C=BE:0000000000000000000000000123abcd", authentication.getName());
        assertEquals(1, authentication.getAuthorities().size());
        assertEquals(SMLRoleEnum.ROLE_SMP.getAuthority(), authentication.getAuthorities().iterator().next());
        assertEquals("CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA,OU=FOR TEST PURPOSES ONLY,O=NATIONAL IT AND TELECOM AGENCY,C=DK", principal.getIssuerDN());
        assertEquals("CN=SMP_123456789,O=DG-DIGIT,C=BE", principal.getSubjectShortDN());
        assertEquals("123abcd", principal.getCertSerial());
    }

    @Test
    void grantSMPRoleForRootCA_TrustedCertificateNewPeppolPKINoRegExp() throws Exception {
        String certHeaderValue = CommonTestUtils.createNewPKIHeaderCertificateForPeppolForBlueCoat("C=BE, O=European Commission, OU=PEPPOL TEST SMP, CN=POP000004");
        Authentication preAuth = getAuthenticationFromHeader(certHeaderValue);
        // for null PKI regexp no certificate should be authenticated
        Mockito.doReturn(null).when(configurationBusiness).getSMPCertRegularExpression();

        assertThrows(AuthenticationServiceException.class, () -> smlAuthenticationProvider.authenticate(preAuth));
    }


    @Test
    void grantSMPRoleRootCA_TrustedCertificateNewPeppolPKIWithRegExp() throws Exception {

        String certHeaderValue = CommonTestUtils.createNewPKIHeaderCertificateForPeppolForBlueCoat("C=BE, O=European Commission, OU=PEPPOL TEST SMP, CN=POP000004");
        Authentication preAuth = getAuthenticationFromHeader(certHeaderValue);
        // for null PKI regexp no certificate should be authenticated
        Mockito.doReturn("^.*(CN=SMP_|OU=PEPPOL TEST SMP).*$").when(configurationBusiness).getSMPCertRegularExpression();


        CertificateAuthentication authentication = (CertificateAuthentication) smlAuthenticationProvider.authenticate(preAuth);

        PreAuthenticatedCertificatePrincipal principal = (PreAuthenticatedCertificatePrincipal) authentication.getPrincipal();

        assertTrue(authentication.isRootPKICertificate());
        assertEquals("CN=POP000004,O=European Commission,C=BE:0000000000000000000000000123abcd", authentication.getName());
        assertEquals(1, authentication.getAuthorities().size());
        assertEquals(SMLRoleEnum.ROLE_SMP.getAuthority(), authentication.getAuthorities().iterator().next());
        assertEquals("CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA - G2,OU=FOR TEST ONLY,O=OpenPEPPOL AISBL,C=BE", principal.getIssuerDN());
        assertEquals("CN=POP000004,O=European Commission,C=BE", principal.getSubjectShortDN());

    }


    @Test
    void grantSMPRoleForNonRootCATrustedCertificateScrabbledRDNs1() throws Exception {
        String subject = "O=DG-DIGIT,C=BE,CN=EHEALTH_SMP_77777777";
        String certHeaderValue = CommonTestUtils.createHeaderCertificateForBlueCoat(subject, false);
        Authentication preAuth = getAuthenticationFromHeader(certHeaderValue);

        //when
        CertificateAuthentication authentication = (CertificateAuthentication) smlAuthenticationProvider.authenticate(preAuth);

        PreAuthenticatedCertificatePrincipal principal = (PreAuthenticatedCertificatePrincipal) authentication.getPrincipal();

        assertFalse(authentication.isRootPKICertificate());
        assertEquals("CN=EHEALTH_SMP_77777777,O=DG-DIGIT,C=BE:0000000000000000000000000123abcd", authentication.getName());
        assertEquals(1, authentication.getAuthorities().size());
        assertEquals(SMLRoleEnum.ROLE_SMP.getAuthority(), authentication.getAuthorities().iterator().next());
        assertEquals("CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA,OU=FOR TEST PURPOSES ONLY,O=NATIONAL IT AND TELECOM AGENCY,C=DK", principal.getIssuerDN());

        assertEquals("CN=EHEALTH_SMP_77777777,O=DG-DIGIT,C=BE", principal.getSubjectShortDN());
        assertEquals("123abcd", principal.getCertSerial());
    }

    @Test
    void grantSMPRoleForNonRootCATrustedCertificateScrabbledRDNs2() throws Exception {
        String subject = "O=DG-DIGIT,C=BE,CN=COMMON_TRUSTED_CERTIFICATE_77777777";
        String certHeaderValue = CommonTestUtils.createHeaderCertificateForBlueCoat(subject, false);
        Authentication preAuth = getAuthenticationFromHeader(certHeaderValue);

        //when
        CertificateAuthentication authentication = (CertificateAuthentication) smlAuthenticationProvider.authenticate(preAuth);

        PreAuthenticatedCertificatePrincipal principal = (PreAuthenticatedCertificatePrincipal) authentication.getPrincipal();


        assertFalse(authentication.isRootPKICertificate());
        assertEquals("CN=COMMON_TRUSTED_CERTIFICATE_77777777,O=DG-DIGIT,C=BE:0000000000000000000000000123abcd", authentication.getName());
        assertEquals(1, authentication.getAuthorities().size());
        assertEquals(SMLRoleEnum.ROLE_SMP.getAuthority(), authentication.getAuthorities().iterator().next());
        assertEquals("CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA,OU=FOR TEST PURPOSES ONLY,O=NATIONAL IT AND TELECOM AGENCY,C=DK", principal.getIssuerDN());

        assertEquals("CN=COMMON_TRUSTED_CERTIFICATE_77777777,O=DG-DIGIT,C=BE", principal.getSubjectShortDN());
        assertEquals("123abcd", principal.getCertSerial());
    }

    @Test
    void grantSMPRoleForNonRootCATrustedCertificateNotOk() throws Exception {
        String subject = "O=DG-DIGIT,C=BE,CN=COMMON_NOT_TRUSTED_CERTIFICATE_77777777";
        String certHeaderValue = CommonTestUtils.createHeaderCertificateForBlueCoat(subject, false);
        Authentication preAuth = getAuthenticationFromHeader(certHeaderValue);

        //when
        assertThrows(AuthenticationServiceException.class, () -> smlAuthenticationProvider.authenticate(preAuth));
    }

    /**
     * Test the construction of the Authenticator from real values from the BlueCoat proxy
     */
    @Test
    void calculateCertificateIdForEhealth1() {
        String certHeader = "sno=48%3Ab6%3A81%3Aee%3A8e%3A0d%3Acc%3A08&subject=C%3DBE%2C+O%3DEuropean+Commission%2C+OU%3DCEF_eDelivery.europa.eu%2C+OU%3DeHealth%2C+CN%3DEHEALTH_SMP_TEST_BRAZIL%2FemailAddress%3DCEF-EDELIVERY-SUPPORT%40ec.europa.eu&validfrom=Feb++1+14%3A20%3A18+2017+GMT&validto=Jul++9+23%3A59%3A00+2019+GMT&issuer=C%3DDE%2C+O%3DT-Systems+International+GmbH%2C+OU%3DT-Systems+Trust+Center%2C+ST%3DNordrhein+Westfalen%2FpostalCode%3D57250%2C+L%3DNetphen%2Fstreet%3DUntere+Industriestr.+20%2C+CN%3DShared+Business+CA+4&policy_oids=1.3.6.1.4.1.7879.13.25";
        PreAuthenticatedCertificatePrincipal principal = blueCoatAuthenticationFilter.buildPrincipalFromHeader(certHeader);


        assertEquals("CN=EHEALTH_SMP_TEST_BRAZIL,O=European Commission,C=BE:000000000000000048b681ee8e0dcc08", principal.getName(32));
        assertEquals("CN=EHEALTH_SMP_TEST_BRAZIL,OU=CEF_eDelivery.europa.eu,OU=eHealth,O=European Commission,C=BE", principal.getSubjectCommonDN());
    }

    /**
     * Test the construction of the Authenticator from real values from the BlueCoat proxy
     */
    @Test
    void calculateCertificateIdForEhealth2() {
        String certHeaderValue = "sno=f7%3A1e%3Ae8%3Ab1%3A1c%3Ab3%3Ab7%3A87&subject=C%3DBE%2C+O%3DEuropean+Commission%2C+OU%3DCEF_eDelivery.europa.eu%2C+OU%3DeHealth%2C+OU%3DSMP_TEST%2C+CN%3DEHEALTH_SMP_EC%2FemailAddress%3DCEF-EDELIVERY-SUPPORT%40ec.europa.eu&validfrom=Dec++6+17%3A41%3A42+2016+GMT&validto=Jul++9+23%3A59%3A00+2019+GMT&issuer=C%3DDE%2C+O%3DT-Systems+International+GmbH%2C+OU%3DT-Systems+Trust+Center%2C+ST%3DNordrhein+Westfalen%2FpostalCode%3D57250%2C+L%3DNetphen%2Fstreet%3DUntere+Industriestr.+20%2C+CN%3DShared+Business+CA+4&policy_oids=1.3.6.1.4.1.7879.13.25";
        CertificateAuthentication preAuth = (CertificateAuthentication) getAuthenticationFromHeader(certHeaderValue);

        assertEquals("CN=EHEALTH_SMP_EC,O=European Commission,C=BE:0000000000000000f71ee8b11cb3b787", preAuth.getName());
        assertEquals("CN=EHEALTH_SMP_EC,O=European Commission,C=BE:0000000000000000f71ee8b11cb3b787", ((CertificateDetails) preAuth.getDetails()).getCertificateId());
        assertEquals("CN=EHEALTH_SMP_EC,O=European Commission,C=BE", ((CertificateDetails) preAuth.getDetails()).getSubject());
    }

    /**
     * Test the construction of the Authenticator from real values from the BlueCoat proxy
     */
    @Test
    void calculateCertificateIdForEhealth3() {
        String certHeaderValue = "sno=48%3Ab6%3A81%3Aee%3A8e%3A0d%3Acc%3A08&subject=C%3DBE%2C+O%3DEuropean+Commission%2C+OU%3DCEF_eDelivery.europa.eu%2C+OU%3DeHealth%2C+ST%3DNordrhein+Westfalen%2FpostalCode%3D57250%2C+L%3DNetphen%2Fstreet%3DUntere+Industriestr.+20%2C+CN%3DEHEALTH_SMP_TEST_BRAZIL%2FemailAddress%3DCEF-EDELIVERY-SUPPORT%40ec.europa.eu&validfrom=Feb++1+14%3A20%3A18+2017+GMT&validto=Jul++9+23%3A59%3A00+2019+GMT&issuer=C%3DDE%2C+O%3DT-Systems+International+GmbH%2C+OU%3DT-Systems+Trust+Center%2C+ST%3DNordrhein+Westfalen%2FpostalCode%3D57250%2C+L%3DNetphen%2Fstreet%3DUntere+Industriestr.+20%2C+CN%3DShared+Business+CA+4&policy_oids=1.3.6.1.4.1.7879.13.25";
        CertificateAuthentication preAuth = (CertificateAuthentication) getAuthenticationFromHeader(certHeaderValue);

        assertEquals("CN=EHEALTH_SMP_TEST_BRAZIL,O=European Commission,C=BE:000000000000000048b681ee8e0dcc08", preAuth.getName());
        assertEquals("CN=EHEALTH_SMP_TEST_BRAZIL,O=European Commission,C=BE:000000000000000048b681ee8e0dcc08", ((CertificateDetails) preAuth.getDetails()).getCertificateId());
        assertEquals("CN=EHEALTH_SMP_TEST_BRAZIL,O=European Commission,C=BE", ((CertificateDetails) preAuth.getDetails()).getSubject());
    }

    @Test
    void calculateCertificateIdForEhealthWithoutEmail() {
        String certHeaderValue = "sno=48%3Ab6%3A81%3Aee%3A8e%3A0d%3Acc%3A08&subject=C%3DBE%2C+O%3DEuropean+Commission%2C+OU%3DCEF_eDelivery.europa.eu%2C+OU%3DeHealth%2C+ST%3DNordrhein+Westfalen%2FpostalCode%3D57250%2C+L%3DNetphen%2Fstreet%3DUntere+Industriestr.+20%2C+CN%3DEHEALTH_SMP_TEST_BRAZIL&validfrom=Feb++1+14%3A20%3A18+2017+GMT&validto=Jul++9+23%3A59%3A00+2019+GMT&issuer=C%3DDE%2C+O%3DT-Systems+International+GmbH%2C+OU%3DT-Systems+Trust+Center%2C+ST%3DNordrhein+Westfalen%2FpostalCode%3D57250%2C+L%3DNetphen%2Fstreet%3DUntere+Industriestr.+20%2C+CN%3DShared+Business+CA+4&policy_oids=1.3.6.1.4.1.7879.13.25";
        CertificateAuthentication preAuth = (CertificateAuthentication) getAuthenticationFromHeader(certHeaderValue);

        assertEquals("CN=EHEALTH_SMP_TEST_BRAZIL,O=European Commission,C=BE:000000000000000048b681ee8e0dcc08", preAuth.getName());
        assertEquals("CN=EHEALTH_SMP_TEST_BRAZIL,O=European Commission,C=BE:000000000000000048b681ee8e0dcc08", ((CertificateDetails) preAuth.getDetails()).getCertificateId());
        assertEquals("CN=EHEALTH_SMP_TEST_BRAZIL,O=European Commission,C=BE", ((CertificateDetails) preAuth.getDetails()).getSubject());
    }

    @Test
    void calculateCertificateIdForEhealthAlreadyDecoded() {
        String certHeader = "serial=48:b6:81:ee:8e:0d:cc:08&subject=C=BE, O=European Commission, OU=CEF_eDelivery.europa.eu, OU=eHealth, CN=EHEALTH_SMP_TEST_BRAZIL&validfrom=Feb  1 14:20:18 2017 GMT&validto=Jul  9 23:59:00 2019 GMT&issuer=C=DE, O=T-Systems International GmbH, OU=T-Systems Trust Center, ST=Nordrhein Westfalen/postalCode=57250, L=Netphen/street=Untere Industriestr. 20, CN=Shared Business CA 4&policy_oids=1.3.6.1.4.1.7879.13.25";
        CertificateAuthentication auth = (CertificateAuthentication) getAuthenticationFromHeader(certHeader);
        assertEquals("CN=EHEALTH_SMP_TEST_BRAZIL,O=European Commission,C=BE:000000000000000048b681ee8e0dcc08", auth.getName());
        assertEquals("CN=EHEALTH_SMP_TEST_BRAZIL,O=European Commission,C=BE:000000000000000048b681ee8e0dcc08", ((CertificateDetails) auth.getDetails()).getCertificateId());
        assertEquals("CN=EHEALTH_SMP_TEST_BRAZIL,O=European Commission,C=BE", ((CertificateDetails) auth.getDetails()).getSubject());
    }

    @Test
    void calculateCertificateIdForEhealthWithEmailAlreadyDecoded() {
        String certHeader = "serial=48:b6:81:ee:8e:0d:cc:08&subject=EMAILADDRESS=receiver@test.be,C=BE, O=European Commission, OU=CEF_eDelivery.europa.eu, OU=eHealth, CN=EHEALTH_SMP_TEST_BRAZIL&validfrom=Feb  1 14:20:18 2017 GMT&validto=Jul  9 23:59:00 2019 GMT&issuer=C=DE, O=T-Systems International GmbH, OU=T-Systems Trust Center, ST=Nordrhein Westfalen/postalCode=57250, L=Netphen/street=Untere Industriestr. 20, CN=Shared Business CA 4&policy_oids=1.3.6.1.4.1.7879.13.25";
        CertificateAuthentication auth = (CertificateAuthentication) getAuthenticationFromHeader(certHeader);
        assertEquals("CN=EHEALTH_SMP_TEST_BRAZIL,O=European Commission,C=BE:000000000000000048b681ee8e0dcc08", auth.getName());
        assertEquals("CN=EHEALTH_SMP_TEST_BRAZIL,O=European Commission,C=BE:000000000000000048b681ee8e0dcc08", ((CertificateDetails) auth.getDetails()).getCertificateId());
        assertEquals("CN=EHEALTH_SMP_TEST_BRAZIL,O=European Commission,C=BE", ((CertificateDetails) auth.getDetails()).getSubject());
    }

    @Test
    void calculateCertificateIdForPeppol() {
        String certHeader = "sno=0001&subject=EMAILADDRESS=receiver@test.be%2C+CN=SMP_receiverCN%2C+OU=B4%2C+O=DIGIT%2C+L=Brussels%2C+ST=BE%2C+C=BE&validfrom=Jun 1 10:37:53 2015 CEST&validto=Jun 1 10:37:53 2035 CEST&issuer=EMAILADDRESS=root@test.be%2C+CN=rootCN%2C+OU=B4%2C+O=DIGIT%2C+L=Brussels%2C+ST=BE%2C+C=BE";
        CertificateAuthentication auth = (CertificateAuthentication) getAuthenticationFromHeader(certHeader);
        assertEquals("CN=SMP_receiverCN,O=DIGIT,C=BE:00000000000000000000000000000001", auth.getName());
        assertEquals("CN=SMP_receiverCN,O=DIGIT,C=BE:00000000000000000000000000000001", ((CertificateDetails) auth.getDetails()).getCertificateId());
        assertEquals("CN=SMP_receiverCN,O=DIGIT,C=BE", ((CertificateDetails) auth.getDetails()).getSubject());
    }

    @Test
    void tesCertificatePKIAsloWithMatchForSubject() {

        String subject = "C=BE, O=Subdomain-Entity, OU=Subdomain-Entity, ST=Nordrhein Westfalen/postalCode\\=57250, L=Netphen Untere Industriestr, CN=Subdomain Subject";
        String issuer = "C=BE, O=Subdomain-Entity, OU=Subdomain-Entity, ST=Nordrhein Westfalen/postalCode\\=57250, L=Netphen/street\\=Untere Industriestr, CN=Subdomain Issuer";

        String certHeaderValue = CommonTestUtils.createHeaderCertificateForBlueCoat(subject, issuer, false);
        Authentication preAuth = getAuthenticationFromHeader(certHeaderValue);

        //when
        CertificateAuthentication authentication = (CertificateAuthentication) smlAuthenticationProvider.authenticate(preAuth);

        CertificateDetails details = (CertificateDetails) authentication.getDetails();


        //must match data ('C=BE, O=Subdomain-Entity, CN=Subdomain Subject', NULL, TIMESTAMP '2019-01-24 19:09:06.427', TIMESTAMP '2019-01-24 19:09:06.427', 0, 6, 0),
        assertFalse(authentication.isRootPKICertificate());
        assertEquals("CN=Subdomain Subject,O=Subdomain-Entity,C=BE:0000000000000000000000000123abcd", authentication.getName());
        assertEquals(1, authentication.getAuthorities().size());
        assertEquals("CN=Subdomain Issuer,OU=Subdomain-Entity,O=Subdomain-Entity,L=Netphen,ST=Nordrhein Westfalen,C=BE", details.getIssuer());
        assertEquals("CN=Subdomain Subject,O=Subdomain-Entity,C=BE", details.getRootCertificateDN());
        assertEquals("CN=Subdomain Subject,O=Subdomain-Entity,C=BE", details.getSubject());
        assertEquals("123abcd", details.getSerial());
    }

    @Test
    void testCertificateValidWithMatchForIssuerOkAndSubjectRegExp() {
        String subject = "C=BE, O=Subdomain-Entity, OU=Subdomain-Entity, ST=Nordrhein Westfalen/postalCode\\=57250, L=Netphen Untere Industriestr, CN=SMP_Subdomain Subject 1";
        String issuer = "C=BE, O=Subdomain-Entity, OU=Subdomain-Entity, ST=Nordrhein Westfalen/postalCode\\=57250, L=Netphen/street\\=Untere Industriestr, CN=Subdomain Issuer";

        String certHeaderValue = CommonTestUtils.createHeaderCertificateForBlueCoat(subject, issuer, false);
        Authentication preAuth = getAuthenticationFromHeader(certHeaderValue);

        //when
        CertificateAuthentication authentication = (CertificateAuthentication) smlAuthenticationProvider.authenticate(preAuth);

        CertificateDetails details = (CertificateDetails) authentication.getDetails();

        assertNotNull(details);
        assertTrue(authentication.isRootPKICertificate());
        assertEquals("CN=SMP_Subdomain Subject 1,O=Subdomain-Entity,C=BE:0000000000000000000000000123abcd", authentication.getName());

    }

    @Test
    void testCertificateValidWithMatchForIssuerOkAndSubjectNotOk() {
        String subject = "C=BE, O=Subdomain-Entity, OU=Subdomain-Entity, ST=Nordrhein Westfalen/postalCode\\=57250, L=Netphen Untere Industriestr, CN=Subdomain Subject 1";
        String issuer = "C=BE, O=Subdomain-Entity, OU=Subdomain-Entity, ST=Nordrhein Westfalen/postalCode\\=57250, L=Netphen/street\\=Untere Industriestr, CN=Subdomain Issuer";

        String certHeaderValue = CommonTestUtils.createHeaderCertificateForBlueCoat(subject, issuer, false);
        Authentication preAuth = getAuthenticationFromHeader(certHeaderValue);

        //when
        assertThrows(AuthenticationServiceException.class, () -> smlAuthenticationProvider.authenticate(preAuth));
    }

    @Test
    void testCertificateValidWithMatchSubjectOk() {
        String subject = "C=BE, O=Subdomain-Entity, OU=Subdomain-Entity, ST=Nordrhein Westfalen/postalCode\\=57250, L=Netphen Untere Industriestr, CN=Subdomain Subject";
        String issuer = "C=BE, O=Subdomain-Entity, OU=Subdomain-Entity, ST=Nordrhein Westfalen/postalCode\\=57250, L=Netphen/street\\=Untere Industriestr, CN=Subdomain Issuer 1";

        String certHeaderValue = CommonTestUtils.createHeaderCertificateForBlueCoat(subject, issuer, false);
        Authentication preAuth = getAuthenticationFromHeader(certHeaderValue);

        //when
        CertificateAuthentication authentication = (CertificateAuthentication) smlAuthenticationProvider.authenticate(preAuth);

        assertFalse(authentication.isRootPKICertificate());

    }

    @Test
    void testCertificateWithUnicodeCharacters() {
        //replace åöÿËStrålfors by aoyEStralfors
        String certHeader = "sno=12%3A89%3A7a%3A5y%3A97%3A99%3Ab1%3A17%3A40%3Ac6%3Ag8%3A53%3A04%3A14%3A42%3Add&subject=C%3DSE%2C+O%3D%5CxC3%5CxA5%5Cxc3%5Cxb6%5Cxc3%5Cxbf%5Cxc3%5Cx8bStr%5CxC3%5CxA5lfors%2C+CN%3DSMP_123456&validfrom=Mar+20+00%3A00%3A00+2017+GMT&validto=Mar+20+23%3A59%3A59+2019+GMT&issuer=C%3DDK%2C+O%3DNATIONAL+IT";
        PreAuthenticatedCertificatePrincipal principal = blueCoatAuthenticationFilter.buildPrincipalFromHeader(certHeader);

        assertEquals("CN=SMP_123456,O=aoyEStralfors,C=SE:12897a5y9799b11740c6g853041442dd", principal.getName(32));
    }

    @Test
    void testCertificateWithSlashCharacters() {
        String[] values = new String[]{"emailAddress\\=CEF-EDELIVERY-SUPPORT@ec.europa.eu", "emailAddress=CEF-EDELIVERY-SUPPORT@ec.europa.eu"};
        for (String value : values) {
            String certHeader = "serial=000000000000000000000000591ae689&subject=CN=digit_smp_cefsupport/" + value + ",OU=cef,O=digit,C=BE&validFrom=Dec 06 18:41:42 2016 CET&validTo=Jul 10 01:59:00 2040 CEST&issuer=CN=digit_smp_cefsupport/emailAddress\\=CEF-EDELIVERY-SUPPORT@ec.europa.eu,OU=cef,O=digit,C=BE";
            PreAuthenticatedCertificatePrincipal principal = blueCoatAuthenticationFilter.buildPrincipalFromHeader(certHeader);
            assertEquals("CN=digit_smp_cefsupport,O=digit,C=BE:000000000000000000000000591ae689", principal.getName(32));
        }
    }

    @Test
    void testCertificateHeaderSubjectAmpersandIssuerOk() {
        String certHeaderValue = "sno=99%3A9f%3Ad4%3Ade%3AYY%3Aa4%3A12%3A99%3Ad5%3A20%3A68%3Ac8%3A71%3A29%3A18%3A8c&subject=C%3DCY%2C+CN%3DSMP_123456%2C+O%3DAC+GOLDMAN+SOLUTIONS+%26+SERVICES+LTD&validfrom=Jul+01+00%3A00%3A00+2015+GMT&validto=Jul+01+23%3A59%3A59+2037+GMT&issuer=C%3DDK%2C+O%3DNATIONAL+IT+AND+TELECOM+AGENCY%2C+CN%3DPEPPOL+SERVICE+METADATA+PUBLISHER+CA";

        Authentication preAuth = getAuthenticationFromHeader(certHeaderValue);

        //when
        CertificateAuthentication authentication = (CertificateAuthentication) smlAuthenticationProvider.authenticate(preAuth);


        assertEquals("ROLE_SMP", ((List<?>) authentication.getAuthorities()).get(0).toString());
    }

    @Test
    void testCertificateHeaderAmpersand2() {
        String certHeaderValue = "sno=4848584&subject=C=BE,O=European+%26+Commission,OU=CEF_eDelivery.europa.eu,OU=Health,CN=SMP_EHEALTH_SMP_TEST_BRAZIL&validfrom=Feb++1+14%3A20%3A18+2017+GMT&validto=Jul++9+23%3A59%3A00+2030+GMT&issuer=C%3DDK%2C+O%3DNATIONAL+IT+AND+TELECOM+AGENCY%2C+CN%3DPEPPOL+SERVICE+METADATA+PUBLISHER+CA";
        Authentication preAuth = getAuthenticationFromHeader(certHeaderValue);

        //when
        CertificateAuthentication authentication = (CertificateAuthentication) smlAuthenticationProvider.authenticate(preAuth);
        assertEquals("ROLE_SMP", ((List<?>) authentication.getAuthorities()).get(0).toString());

    }

    @Test
    void testCertificateWithSpecialCharacters() {
        //replace åöÿËStrålfors by aoyEStralfors
        // escape special characters = and ,
        String certHeader = "sno=12%3A89%3A7a%3A5y%3A97%3A99%3Ab1%3A17%3A40%3Ac6%3Ag8%3A53%3A04%3A14%3A42%3Add&subject=C%3DSE%2C+O%3D%5CxC3%5CxA5%5Cxc3%5Cxb6%5Cxc3%5Cxbf%5Cxc3%5Cx8bStr%5CxC3%5CxA5lfors%2C+CN%3DSMP_12=34,56&validfrom=Mar+20+00%3A00%3A00+2017+GMT&validto=Mar+20+23%3A59%3A59+2029+GMT&issuer=C%3DDK%2C+O%3DNATIONAL+IT";
        PreAuthenticatedCertificatePrincipal principal = blueCoatAuthenticationFilter.buildPrincipalFromHeader(certHeader);

        assertEquals("CN=SMP_12\\=34\\,56,O=aoyEStralfors,C=SE:12897a5y9799b11740c6g853041442dd", principal.getName(32));
        assertEquals("O=NATIONAL IT,C=DK", principal.getIssuerDN());
    }

    @Test
    void testSmartEscapeDistinguishedName() {
        String certHeaderValue = "sno=0997+%280x3e5%29&subject=C%3DBE%2C+ST%3DBelgium%2C+L%3DBrussels%2C+O%3DAC+GOLDMAN+SOLUTIONS=%26+SERVICES+LTD%2C+OU%3DAC+GOLDMAN+SOLUTIONS+%26+SERVICES+LTD%2C+CN%3DSMP5Connectivity_test%2FemailAddress%3Damar.deep%40ext.ec.europa.eu&validfrom=Oct++2+14%3A53%3A58+2017+GMT&validto=Jan++8+14%3A53%3A58+2028+GMT&issuer=C%3DBE%2C+ST%3DBelgium%2C+O%3DConnectivity+Test%2C+OU%3DConnecting+Europe+Facility%2C+CN%3DConnectivity+Test+Component+CA%2FemailAddress%3DCEF-EDELIVERY-SUPPORT%40ec.europa.eu";
        PreAuthenticatedCertificatePrincipal principal = blueCoatAuthenticationFilter.buildPrincipalFromHeader(certHeaderValue);

        assertEquals("CN=SMP5Connectivity_test,O=AC GOLDMAN SOLUTIONS\\=& SERVICES LTD,C=BE:000000000000000000000000000003e5", principal.getName(32));
        assertEquals("CN=Connectivity Test Component CA,OU=Connecting Europe Facility,O=Connectivity Test,ST=Belgium,C=BE", principal.getIssuerDN());
    }


    @Test
    void testCertificateWithSpecialCharacters2() {
        String certHeaderValue = "sno=4112+%280x1010%29&subject=C%3DPL%2C+O%3DDE%5CxE1%5CxBA%5Cx9E%5CxC3%5Cx9F%5CxC3%5Cx84%5CxC3%5CxA4PL%5CxC5%5CxBC%5CxC3%5CxB3%5CxC5%5Cx82%5CxC4%5Cx87NO%5CxC3%5Cx86%5CxC3%5CxA6%5CxC3%5Cx98%5CxC3%5CxB8%5CxC3%5Cx85%5CxC3%5CxA5%2C+CN%3Dslash%2Fbackslash%5Cquote%22colon%3A_rfc2253special_ampersand%26comma%2Cequals%3Dplus%2Blessthan%3Cgreaterthan%3Ehash%23semicolon%3Bend&validfrom=Oct+12+12%3A57%3A42+2017+GMT&validto=Jan+18+12%3A57%3A42+2028+GMT&issuer=C%3DBE%2C+ST%3DBelgium%2C+O%3DConnectivity+Test%2C+OU%3DConnecting+Europe+Facility%2C+CN%3DConnectivity+Test+Component+CA%2FemailAddress%3DCEF-EDELIVERY-SUPPORT%40ec.europa.eu";
        PreAuthenticatedCertificatePrincipal principal = blueCoatAuthenticationFilter.buildPrincipalFromHeader(certHeaderValue);
        assertEquals("CN=slash/backslash\\\\quote\\\"colon:_rfc2253special_ampersand&comma\\,equals\\=plus\\+lessthan\\<greaterthan\\>hash\\#semicolon\\;end,O=DEẞßAaPLzołcNOÆæØøAa,C=PL:00000000000000000000000000001010", principal.getName(32));
        assertEquals("CN=Connectivity Test Component CA,OU=Connecting Europe Facility,O=Connectivity Test,ST=Belgium,C=BE", principal.getIssuerDN());
    }

    @Test
    void testCertificateWithSpecialCharacters3() {
        String certHeaderValue = "sno=0997+%280x3e5%29&subject=C%3DBE%2C+ST%3DBelgium%2C+L%3DBrussels%2C+O%3DAC+GOLDMAN+SOLUTIONS=%26+SERVICES+LTD%2C+OU%3DAC+GOLDMAN+SOLUTIONS+%26+SERVICES+LTD%2C+CN%3DSMP_5Connectivity_test%2FemailAddress%3Damar.deep%40ext.ec.europa.eu&validfrom=Oct++2+14%3A53%3A58+2017+GMT&validto=Jan++8+14%3A53%3A58+2028+GMT&issuer=C%3DBE%2C+ST%3DBelgium%2C+O%3DConnectivity+Test%2C+OU%3DConnecting+Europe+Facility%2C+CN%3DConnectivity+Test+Component+CA%2FemailAddress%3DCEF-EDELIVERY-SUPPORT%40ec.europa.eu";
        CertificateAuthentication bcAuth = checkBlueCoatMetadata(certHeaderValue, "3e5",
                "CN=Connectivity Test Component CA,OU=Connecting Europe Facility,O=Connectivity Test,ST=Belgium,C=BE",
                "CN=SMP_5Connectivity_test,O=AC GOLDMAN SOLUTIONS\\=& SERVICES LTD,C=BE",
                "CN=SMP_5Connectivity_test,O=AC GOLDMAN SOLUTIONS\\=& SERVICES LTD,C=BE:000000000000000000000000000003e5");

        assertNotNull(bcAuth);
    }

    private CertificateAuthentication checkBlueCoatMetadata(String certHeaderValue, String... params) {
        Authentication preAuth = getAuthenticationFromHeader(certHeaderValue);

        //when
        CertificateAuthentication authentication = (CertificateAuthentication) smlAuthenticationProvider.authenticate(preAuth);

        assertEquals(params[0], ((CertificateDetails) authentication.getDetails()).getSerial());
        assertEquals(params[1], ((CertificateDetails) authentication.getDetails()).getIssuer());
        assertEquals(params[2], ((CertificateDetails) authentication.getDetails()).getSubject());
        assertEquals(params[3], authentication.getName());

        return authentication;
    }


}
