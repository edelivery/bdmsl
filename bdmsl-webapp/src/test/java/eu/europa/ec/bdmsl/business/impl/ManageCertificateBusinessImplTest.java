/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.business.impl;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.business.IManageCertificateBusiness;
import eu.europa.ec.bdmsl.business.IX509CertificateBusiness;
import eu.europa.ec.bdmsl.common.bo.CertificateBO;
import eu.europa.ec.bdmsl.common.exception.*;
import eu.europa.ec.bdmsl.test.CommonTestUtils;
import eu.europa.ec.bdmsl.security.ICertificateAuthentication;
import eu.europa.ec.bdmsl.service.dns.IDnsClientService;
import eu.europa.ec.bdmsl.service.dns.impl.DnsZone;
import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.security.Security;
import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;


/**
 * @author Tiago MIGUEL
 * @since 02/12/2016
 */
class ManageCertificateBusinessImplTest extends AbstractJUnit5Test {
    @Autowired
    private ICertificateAuthentication customAuthentication;

    @Autowired
    private IManageCertificateBusiness manageCertificateBusiness;

    @Autowired
    private IDnsClientService dnsClientService;

    @Autowired
    IX509CertificateBusiness x509CertificateBusiness;


    @BeforeAll
    static void beforeClass() {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    @Test
    void testValidateCurrentCertificate() throws TechnicalException {
        Calendar validFrom = GregorianCalendar.getInstance();
        validFrom.set(validFrom.get(Calendar.YEAR) - 1, 1, 1);
        Calendar validTo = GregorianCalendar.getInstance();
        validTo.set(validTo.get(Calendar.YEAR) + 1, 1, 1);
        CertificateBO validCertificate = new CertificateBO();
        validCertificate.setValidFrom(validFrom);
        validCertificate.setValidTo(validTo);
        manageCertificateBusiness.validateCurrentCertificate(validCertificate);
    }

    @Test
    void testValidateCurrentCertificateExpired() throws TechnicalException {
        Calendar validFrom = GregorianCalendar.getInstance();
        validFrom.set(validFrom.get(Calendar.YEAR) - 1, 1, 1);
        Calendar validTo = GregorianCalendar.getInstance();
        validTo.set(validTo.get(Calendar.YEAR), validTo.get(Calendar.MONTH) - 1, 1);
        CertificateBO validCertificate = new CertificateBO();
        validCertificate.setValidFrom(validFrom);
        validCertificate.setValidTo(validTo);
        Assertions.assertThrows(CertificateExpiredException.class, () -> {
            manageCertificateBusiness.validateCurrentCertificate(validCertificate);
        });

    }

    @Test
    void testValidateCurrentCertificateNotYetValid() throws TechnicalException {
        Calendar validFrom = GregorianCalendar.getInstance();
        validFrom.set(validFrom.get(Calendar.YEAR) + 1, 1, 1);
        Calendar validTo = GregorianCalendar.getInstance();
        validTo.set(validTo.get(Calendar.YEAR) + 2, 1, 1);
        CertificateBO validCertificate = new CertificateBO();
        validCertificate.setValidFrom(validFrom);
        validCertificate.setValidTo(validTo);
        Assertions.assertThrows(CertificateNotYetValidException.class, () -> {
            manageCertificateBusiness.validateCurrentCertificate(validCertificate);
        });

    }

    @Test
    void testFindDomainForFullIssuer() throws Exception {
        String subject = "OU=eHealth, O=European Commission, OU=CEF_eDelivery.europa.eu,C=IT, CN=SMP_TEST/emailAddress\\=CEF-EDELIVERY-SUPPORT@ec.europa.eu";
        String issuer = "ST=Nordrhein Westfalen/postalCode\\=57250,L=Netphen/street\\=Untere Industriestr,O=T-Systems GmbH, C=PT,OU=T-Systems Center,   CN=Business CA 4";
        createStringFromX509Certificate(issuer, subject);

        DnsZone dnsZone = dnsClientService.getDnsZoneName();
        assertEquals("sea.acc.edelivery.tech.ec.europa.eu.", dnsZone.getDomain());
    }

    @Test
    void testFindDomainForDefaultSubject() throws Exception {
        String subject = "OU=eHealth, O=DG-DIGIT, OU=CEF_eDelivery.europa.eu,C=BE, CN=COMMON_TRUSTED_CERTIFICATE_77777777";
        String issuer = "CN=DigitalSign Corporation CA,OU=Root CA,O=GlobalSign nv-sa,C=UK";
        String certificateStr = createStringFromX509Certificate(issuer, subject);

        DnsZone dnsZone = dnsClientService.getDnsZoneName();

        assertEquals("edelivery.tech.ec.europa.eu.", dnsZone.getDomain());
        assertEquals("CN=COMMON_TRUSTED_CERTIFICATE_77777777,O=DG-DIGIT,C=BE", certificateStr);
    }

    @Test
    void testFindDomainForFullSubject() throws Exception {
        String subject = "OU=eHealth, O=European Commission, OU=CEF_eDelivery.europa.eu,C=BE, CN=DUMMY_SMP_TEST,emailAddress=CEF-EDELIVERY-SUPPORT@ec.europa.eu";
        String issuer = "CN=DigitalSign Corporation CA,OU=Root CA,O=GlobalSign nv-sa,C=UK";
        String certificateStr = createStringFromX509Certificate(issuer, subject);

        DnsZone dnsZone = dnsClientService.getDnsZoneName();

        assertEquals("edelivery.tech.ec.europa.eu.", dnsZone.getDomain());
        assertEquals("CN=DUMMY_SMP_TEST,OU=CEF_eDelivery.europa.eu,OU=eHealth,O=European Commission,C=BE", certificateStr);
    }

    @Test
    void testNewCertificateDomainForPrepareChangeCertificateByX509CertificateAuthenticationOk() throws Exception {
        String subject = "CN=SMP_CHANGE_CERT_CHECK_DOMAIN-2,O=DG-DIGIT,C=BE";
        String issuer = "CN=SMP_12345678910111277,O=DG-DIGIT,C=BE";
        Calendar validTo = Calendar.getInstance();
        validTo.set(validTo.get(Calendar.YEAR) + 1, 1, 1);
        customAuthentication.x509Authentication(issuer, subject, new Date(), validTo.getTime());

        testNewCertificateDomainValidationForPrepareChangeCertificate();
    }

    @Test
    void testNewCertificateDomainForPrepareChangeCertificateDomainNotFound() throws Exception {
        String subject = "CN=SMP_CHANGE_CERT_CHECK_DOMAIN-2,O=DG-DIGIT,C=BE";
        String issuer = "CN=SMP_NOT_FOUND_12345678910111277000001,O=DG-DIGIT,C=BE";

        Calendar validTo = Calendar.getInstance();
        validTo.set(validTo.get(Calendar.YEAR) + 1, 1, 1);
        customAuthentication.x509Authentication(issuer, subject, new Date(), validTo.getTime());

        try {
            testNewCertificateDomainValidationForPrepareChangeCertificate();
            fail();
        } catch (GenericTechnicalException e) {
            assertEquals("[ERR-105] Impossible to find out the domain of either the current certificate or new certificate.", e.getMessage());
        }
    }

    @Test
    void testNewCertificateDomainForPrepareChangeCertificateByX509CertificateAuthenticationDifferentDomain() throws Exception {
        String subject = "CN=SMP_CHANGE_CERT_CHECK_DOMAIN-2,O=DG-DIGIT,C=BE";
        String issuer = "CN=SMP_DISCOVERY_ISSUER_4,O=DG-DIGIT,C=BE";

        Calendar validTo = Calendar.getInstance();
        validTo.set(validTo.get(Calendar.YEAR) + 1, 1, 1);
        customAuthentication.x509Authentication(issuer, subject, new Date(), validTo.getTime());

        try {
            testNewCertificateDomainValidationForPrepareChangeCertificate();
            fail();
        } catch (Exception exc) {
            assertEquals("[ERR-106] The new certificate 'CN=SMP_12345678910111277,O=DG-DIGIT,C=BE' with domain '3' does not belong to the same subdomain '6' as the current certificate CN=SMP_DISCOVERY_ISSUER_4,O=DG-DIGIT,C=BE.", exc.getMessage());
        }
    }

    @Test
    void testNewCertificateDomainForPrepareChangeCertificateByBlueCoatAuthenticationOk() throws Exception {
        String subject = "C=BE,CN=Changing Certificates 1,O=DIGIT-B28";
        String issuer = "CN=SMP_" + System.currentTimeMillis() + ",O=DG-DIGIT,C=BE";
        customAuthentication.blueCoatAuthentication("001", issuer, subject, false);

        String subjectForNewCertificate = "C=BE,CN=Changing Certificates 2,O=DIGIT-B28";
        String issuerForNewCertificate = "CN=SMP_" + System.currentTimeMillis() + 1 + ",O=DG-DIGIT,C=BE";

        testNewCertificateDomainValidationForPrepareChangeCertificate(subjectForNewCertificate, issuerForNewCertificate);
    }

    @Test
    void testNewCertificateDomainForPrepareChangeCertificateByBlueCoatAuthenticationCertificateNotFound() throws Exception {
        String subject = "C=BE,CN=Changing Certificates 1,O=DIGIT-B28";
        String issuer = "CN=SMP_" + System.currentTimeMillis() + ",O=DG-DIGIT,C=BE";
        customAuthentication.blueCoatAuthentication("001", issuer, subject, false);

        String subjectForNewCertificate = "C=BE,CN=Changing Certificates 3,O=DIGIT-B28";
        String issuerForNewCertificate = "CN=SMP_" + System.currentTimeMillis() + 1 + ",O=DG-DIGIT,C=BE";

        Assertions.assertThrows(CertificateNotFoundException.class, () -> {
            testNewCertificateDomainValidationForPrepareChangeCertificate(subjectForNewCertificate, issuerForNewCertificate);
        });

    }

    @Test
    void testNewCertificateDomainForPrepareChangeCertificateByAdminAuthenticationNotOk() throws Exception {
        customAuthentication.adminTransientAuthentication();

        try {
            testNewCertificateDomainValidationForPrepareChangeCertificate();
            fail();
        } catch (Exception exc) {
            assertEquals("[ERR-105] Impossible to find out the domain of either the current certificate or new certificate.", exc.getMessage());
        }
    }

    @Test
    void testNewCertificateDomainForPrepareChangeCertificateByUnsecureAuthenticationDifferentDomain() throws Exception {
        customAuthentication.unsecuredAuthentication();
        try {
            testNewCertificateDomainValidationForPrepareChangeCertificate();
            fail();
        } catch (Exception exc) {
            assertEquals("[ERR-106] The new certificate 'CN=SMP_12345678910111277,O=DG-DIGIT,C=BE' with domain '3' does not belong to the same subdomain '1' as the current certificate CN=unsecure_root,O=delete_in_production,C=only_for_testing.", exc.getMessage());
        }
    }

    @Test
    void testNewCertificateDomainForPrepareChangeCertificateByUnsecureAuthenticationOk() throws Exception {
        String subject = "CN=SMP_CHANGE_CERT_CHECK_DOMAIN-2,O=DG-DIGIT,C=BE";
        String issuer = "CN=unsecure_root,O=delete_in_production,C=only_for_testing";
        customAuthentication.unsecuredAuthentication();
        testNewCertificateDomainValidationForPrepareChangeCertificate(subject, issuer);
    }

    @Test
    void testNewCertificateDomainForPrepareChangeCertificateByNotFoundNewCertificate() throws Exception {
        String subject = "CN=__NOT_FOUND_SUBJECT__,O=DG-DIGIT,C=BE";
        String issuer = "CN=__NOT_FOUND_ISSUER__,O=delete_in_production,C=only_for_testing";
        customAuthentication.unsecuredAuthentication();

        Assertions.assertThrows(CertificateNotFoundException.class, () -> {
            testNewCertificateDomainValidationForPrepareChangeCertificate(subject, issuer);
        });

    }

    private void testNewCertificateDomainValidationForPrepareChangeCertificate() throws Exception {
        String subjectForNewCertificate = "CN=SMP_CHANGE_CERT_CHECK_DOMAIN-6,O=DG-DIGIT,C=BE";
        String issuerForNewCertificate = "CN=SMP_12345678910111277,O=DG-DIGIT,C=BE";

        testNewCertificateDomainValidationForPrepareChangeCertificate(subjectForNewCertificate, issuerForNewCertificate);
    }

    private void testNewCertificateDomainValidationForPrepareChangeCertificate(String subjectForNewCertificate, String issuerForNewCertificate) throws Exception {
        X509Certificate newCertificate = CommonTestUtils.createCertificate(issuerForNewCertificate, subjectForNewCertificate, false);

        manageCertificateBusiness.domainValidationForNewCertificate(newCertificate);
    }

    private String createStringFromX509Certificate(String issuer, String subject) throws Exception {
        X509Certificate certificate = CommonTestUtils.createCertificate(issuer, subject, false);
        X509Certificate[] certificates = new X509Certificate[]{certificate};

        HttpServletRequest mockedRequest = Mockito.mock(HttpServletRequest.class);
        Mockito.when(mockedRequest.getAttribute("jakarta.servlet.request.X509Certificate")).thenReturn(certificates);

        Authentication authentication = createX509Authentication(certificates);
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        String certificateStr = x509CertificateBusiness.getTrustedRootCertificateDN(certificates);


        return certificateStr;
    }

}
