/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.impl;

import eu.europa.ec.bdmsl.business.IConfigurationBusiness;
import eu.europa.ec.bdmsl.common.logging.ILoggingService;
import eu.europa.ec.bdmsl.test.CommonTestUtils;
import eu.europa.ec.edelivery.security.cert.URLFetcher;
import org.apache.hc.client5.http.HttpHostConnectException;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.math.BigInteger;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.CertificateRevokedException;
import java.security.cert.X509Certificate;
import java.util.Collections;

import static eu.europa.ec.edelivery.security.cert.impl.CertificateRVStrategyEnum.CRL_ONLY;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class RevocationValidatorServiceImplTest {

    URLFetcher mockConnection = Mockito.mock(URLFetcher.class);
    ILoggingService mockLogger = Mockito.mock(ILoggingService.class);
    IConfigurationBusiness mockConfiguration = Mockito.mock(IConfigurationBusiness.class);

    private final RevocationValidatorServiceImpl testInstance = new RevocationValidatorServiceImpl(mockConnection, mockConfiguration, mockLogger);

    @BeforeAll
    static void beforeClass() {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    @Test
    void verifyCertificateCRLsTest() throws CertificateException, IOException {
        // given
        X509Certificate certificate = CommonTestUtils.loadCertificate("smp-crl-test-all.pem");
        Mockito.doReturn(false).when(mockConfiguration).isCertRevocationValidationGraceful();
        Mockito.doReturn(CRL_ONLY).when(mockConfiguration).getCertRevocationValidationStrategy();

        Mockito.doReturn(RevocationValidatorServiceImplTest.class.getResourceAsStream("/test.crl")).when(mockConnection).fetchURL(Mockito.anyString());

        Assertions.assertDoesNotThrow(() -> testInstance.isCertificateRevoked(certificate, null));
    }

    @Test
    void verifyCertificateCRLRevokedTest() throws CertificateException, IOException {
        // given
        X509Certificate certificate = CommonTestUtils.loadCertificate("smp-crl-revoked.pem");

        Mockito.doReturn(false).when(mockConfiguration).isCertRevocationValidationGraceful();
        Mockito.doReturn(RevocationValidatorServiceImplTest.class.getResourceAsStream("/certificates/smp-crl-test.crl"))
                .when(mockConnection).fetchURL(Mockito.anyString());
        Mockito.doReturn(CRL_ONLY).when(mockConfiguration).getCertRevocationValidationStrategy();

        // when-then
        CertificateRevokedException result = assertThrows(CertificateRevokedException.class,
                () -> testInstance.isCertificateRevoked(certificate, null));
        assertThat(result.getMessage(), CoreMatchers.startsWith("Certificate has been revoked"));
    }

    @Test
    void verifyCertificateCRLsX509FailsToConnectTest() throws Exception {
        // given
        X509Certificate certificate = CommonTestUtils.loadCertificate("smp-crl-test-all.pem");

        Mockito.doReturn(false).when(mockConfiguration).isCertRevocationValidationGraceful();
        Mockito.doThrow(new HttpHostConnectException("Connection refused"))
                .when(mockConnection).fetchURL(Mockito.anyString());
        Mockito.doReturn(CRL_ONLY).when(mockConfiguration).getCertRevocationValidationStrategy();

        CertificateException result = assertThrows(CertificateException.class,
                () -> testInstance.isCertificateRevoked(certificate, null));
        assertThat(result.getMessage(), CoreMatchers.startsWith("Could not get CRL from non of the CLR distribution points: "));
    }

    @Test
    void verifyCertificateWithTwoCRLsX509FailsToConnectTest() throws Exception {
        // given
        X509Certificate certificate = CommonTestUtils.loadCertificate("smp-crl-test-2crl.pem");

        Mockito.doReturn(false).when(mockConfiguration).isCertRevocationValidationGraceful();
        Mockito.doThrow(new HttpHostConnectException("Connection refused"))
                .when(mockConnection).fetchURL(Mockito.anyString());
        Mockito.doReturn(CRL_ONLY).when(mockConfiguration).getCertRevocationValidationStrategy();

        // when-then
        CertificateException result = assertThrows(CertificateException.class,
                () -> testInstance.isCertificateRevoked(certificate, null));
        assertEquals("Could not get CRL from non of the CLR distribution points: [http://sml-crl-hostname/sml-test-soapui-not-exists-03.crl,http://sml-crl-hostname/sml-test-soapui-not-exists-04.crl]!", result.getMessage());
    }


    @Test
    void verifyCertificateCRLsRevokedSerialTest() throws IOException {

        Mockito.doReturn(RevocationValidatorServiceImplTest.class.getResourceAsStream("/certificates/smp-crl-test.crl"))
                .when(mockConnection).fetchURL(Mockito.anyString());
        Mockito.doReturn(CRL_ONLY).when(mockConfiguration).getCertRevocationValidationStrategy();

        // when-then
        CertificateRevokedException result = assertThrows(CertificateRevokedException.class,
                () -> testInstance.isSerialNumberRevoked(new BigInteger("11", 16), null, Collections.singletonList("https://localhost/crl")));
        assertThat(result.getMessage(), CoreMatchers.startsWith("Certificate has been revoked"));
    }

    @Test
    void verifyCertificateCRLsRevokedSerialTestThrowIOExceptionHttps() throws IOException {

        Mockito.doReturn(false).when(mockConfiguration).isCertRevocationValidationGraceful();
        Mockito.doReturn(CRL_ONLY).when(mockConfiguration).getCertRevocationValidationStrategy();
        Mockito.doThrow(new HttpHostConnectException("Connection refused"))
                .when(mockConnection).fetchURL(Mockito.anyString());

        // when-then
        CertificateException result = assertThrows(CertificateException.class,
                () -> testInstance.isSerialNumberRevoked(new BigInteger("11", 16), null, Collections.singletonList("https://localhost/crl")));
        assertEquals("Could not get CRL from non of the CLR distribution points: [https://localhost/crl]!", result.getMessage());
    }

}
