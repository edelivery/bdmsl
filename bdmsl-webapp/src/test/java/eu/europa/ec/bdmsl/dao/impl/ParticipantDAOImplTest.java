/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.impl;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.bo.ParticipantBO;
import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import eu.europa.ec.bdmsl.common.exception.BadRequestException;
import eu.europa.ec.bdmsl.common.exception.GenericTechnicalException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.dao.IParticipantDAO;
import eu.europa.ec.bdmsl.dao.ISubdomainDAO;
import eu.europa.ec.bdmsl.test.TestConstants;
import eu.europa.ec.dynamicdiscovery.util.HashUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;

import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.util.AssertionErrors.assertTrue;

/**
 * @author Tiago MIGUEL
 * @since 30/03/2017
 */
class ParticipantDAOImplTest extends AbstractJUnit5Test {

    @Autowired
    private IParticipantDAO participantDAO;

    @Autowired
    ISubdomainDAO subdomainDAO;

    @Test
    void testGetAllParticipants() {
        // given
        // when
        List<ParticipantBO> participantBOList = participantDAO.getAllParticipants();

        // then
        for (ParticipantBO participantBO : participantBOList) {
            assertNull(participantBO.getSubdomain());

        }
        assertNull(participantBOList.get(0).getSubdomain());
        assertEquals("0009:123456789", participantBOList.get(0).getParticipantId());
    }

    @Test
    void getParticipantForDomain() throws TechnicalException {
        // given
        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId("0009:987testDifDomain");
        participantBO.setScheme("iso6523-actorid-upis");
        SubdomainBO existOn1 = subdomainDAO.getSubDomain("acc.edelivery.tech.ec.europa.eu");
        SubdomainBO existOn2 = subdomainDAO.getSubDomain("delta.acc.edelivery.tech.ec.europa.eu");

        SubdomainBO notExistOn2 = subdomainDAO.getSubDomain("ehealth.edelivery.tech.ec.europa.eu");
        // when-then
        Optional<ParticipantBO> res = participantDAO.getParticipantForDomain(participantBO, existOn1);
        assertTrue("Is not present", res.isPresent());
        res = participantDAO.getParticipantForDomain(participantBO, existOn2);
        assertTrue("Is not present", res.isPresent());
        res = participantDAO.getParticipantForDomain(participantBO, notExistOn2);
        assertFalse(res.isPresent());

    }


    @Test
    @Transactional
    void getCreateParticipantBasic() throws TechnicalException, NoSuchAlgorithmException {
        // given
        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId("0009:" + UUID.randomUUID());
        participantBO.setScheme("iso6523-actorid-upis");
        participantBO.setSmpId("smp-sample");
        // when
        participantDAO.createParticipant(participantBO);

        ParticipantBO result = participantDAO.findParticipant(participantBO);

        assertNotNull(result);
        assertEquals(participantBO.getSmpId(), result.getSmpId());
        assertEquals(participantBO.getParticipantId(), result.getParticipantId());
        assertEquals(participantBO.getScheme(), result.getScheme());
        assertEquals(HashUtil.getMD5Hash(participantBO.getParticipantId()), result.getCnameHash());
        assertEquals(HashUtil.getSHA256HashBase32(participantBO.getParticipantId()), result.getNaptrHash());
    }

    @Test
    @Transactional
    void findParticipantForDomainBasic() throws TechnicalException, NoSuchAlgorithmException {
        // given
        String smpOriginal = "smp-sample";
        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId("0009:" + UUID.randomUUID());
        participantBO.setScheme("iso6523-actorid-upis");
        participantBO.setSmpId(smpOriginal);

        // when
        participantDAO.createParticipant(participantBO);
        // search for other participant
        participantBO.setSmpId(UUID.randomUUID().toString());
        ParticipantBO result = participantDAO.findParticipant(participantBO);

        assertNotNull(result);
        assertNotEquals(participantBO.getSmpId(), result.getSmpId());
        assertEquals(smpOriginal, result.getSmpId());
        assertEquals(participantBO.getParticipantId(), result.getParticipantId());
        assertEquals(participantBO.getScheme(), result.getScheme());
        assertEquals(HashUtil.getMD5Hash(participantBO.getParticipantId()), result.getCnameHash());
        assertEquals(HashUtil.getSHA256HashBase32(participantBO.getParticipantId()), result.getNaptrHash());
    }

    @Test
    @Transactional
    void createParticipantThrowExists() throws TechnicalException {
        // given
        String smpOriginal = "smp-sample";
        String participantID = "0009:" + UUID.randomUUID();
        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId(participantID);
        participantBO.setScheme("iso6523-actorid-upis");
        participantBO.setSmpId(smpOriginal);

        // when
        participantDAO.createParticipant(participantBO);

        assertThrows(BadRequestException.class,
                () ->
                        participantDAO.createParticipant(participantBO),"[ERR-106] The participant identifier: [" + participantID + "] and scheme [iso6523-actorid-upis] is already registered"
        );
    }

    @Test
    @Transactional
    void createParticipantSMPDoesNotExists() {
        // given
        String participantID = "0009:" + UUID.randomUUID();
        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId(participantID);
        participantBO.setScheme("iso6523-actorid-upis");
        participantBO.setSmpId("smp-sample-not-exists");


        GenericTechnicalException genericTechnicalException = assertThrows(GenericTechnicalException.class,
                () ->
                        participantDAO.createParticipant(participantBO)
        );
        assertEquals("[ERR-105] Error occurred while creating participant id [" + participantID + "] and scheme [iso6523-actorid-upis]. Missing smp [smp-sample-not-exists]", genericTechnicalException.getMessage());
    }

    @Test
    @Transactional
    void getCreateParticipantEmptySchemaBasic() throws TechnicalException, NoSuchAlgorithmException {
        // given
        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId("iso6523-actorid-upis::0009:" + UUID.randomUUID());
        participantBO.setScheme("");
        participantBO.setSmpId("smp-sample");
        // when
        participantDAO.createParticipant(participantBO);
        ParticipantBO result = participantDAO.findParticipant(participantBO);

        assertNotNull(result);
        assertEquals(participantBO.getSmpId(), result.getSmpId());
        assertEquals(participantBO.getParticipantId(), result.getParticipantId());
        assertEquals(participantBO.getScheme(), result.getScheme());
        assertEquals(HashUtil.getMD5Hash(participantBO.getParticipantId()), result.getCnameHash());
        assertEquals(HashUtil.getSHA256HashBase32(participantBO.getParticipantId()), result.getNaptrHash());
    }

    @Test
    @Transactional
    void getCreateParticipantNullSchemaBasic() throws TechnicalException, NoSuchAlgorithmException {
        // given
        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId("iso6523-actorid-upis::0009:" + UUID.randomUUID());
        participantBO.setScheme(null);
        participantBO.setSmpId("smp-sample");
        // when
        participantDAO.createParticipant(participantBO);

        ParticipantBO result = participantDAO.findParticipant(participantBO);

        assertNotNull(result);
        assertEquals(participantBO.getSmpId(), result.getSmpId());
        assertEquals(participantBO.getParticipantId(), result.getParticipantId());
        assertEquals(participantBO.getScheme(), result.getScheme());
        assertEquals(HashUtil.getMD5Hash(participantBO.getParticipantId()), result.getCnameHash());
        assertEquals(HashUtil.getSHA256HashBase32(participantBO.getParticipantId()), result.getNaptrHash());
    }

    @Test
    @Transactional
    void getCreateParticipantDifferentDomainBasic() throws TechnicalException {
        // given
        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId("0009:" + UUID.randomUUID());
        participantBO.setScheme("iso6523-actorid-upis");
        participantBO.setSmpId("smp-sample"); // acc.edelivery.tech.ec.europa.eu
        // when
        participantDAO.createParticipant(participantBO);
        participantBO.setSmpId("SMP2017"); //delta.acc.edelivery.tech.ec.europa.eu
        assertDoesNotThrow(() -> participantDAO.createParticipant(participantBO));
    }

    @Test
    void getParticipantCount() {
        long res = participantDAO.getParticipantCount();
        assertEquals(TestConstants.DB_PARTICIPANT_COUNT, res);
    }

    @Test
    @Transactional
    void getParticipantCountForSMP() throws TechnicalException {
        // given
        long res = participantDAO.getParticipantCountForSMP("smp-sample");
        assertEquals(0, res);
        for (int i = 0; i < 10; i++) {
            ParticipantBO participantBO = new ParticipantBO();
            participantBO.setParticipantId("0009:" + UUID.randomUUID());
            participantBO.setScheme("iso6523-actorid-upis");
            participantBO.setSmpId("smp-sample"); // acc.edelivery.tech.ec.europa.eu
            participantDAO.createParticipant(participantBO);
        }
        //when then
        res = participantDAO.getParticipantCountForSMP("smp-sample");
        assertEquals(10, res);
    }

    @Test
    @Transactional
    void testUpdateNextBatchHashValuesForParticipants() throws Exception {
        int iSize = participantDAO.getAllParticipants().size();
        int iVal = participantDAO.updateNextBatchHashValuesForParticipants();

        assertEquals(0, iVal);
        assertEquals(TestConstants.DB_PARTICIPANT_COUNT, iSize);
    }

    @Test
    @Transactional
    @Sql("/init-test-database.sql")
    @Sql("/participatsNullHash.sql")
    void testUpdateNextBatchHashValuesForParticipantsNewParticipants() throws Exception {
        int iNullHashParticipants = 11;
        int iSize = participantDAO.getAllParticipants().size();
        int iVal = participantDAO.updateNextBatchHashValuesForParticipants();

        assertEquals(iNullHashParticipants, iVal);
        assertEquals(TestConstants.DB_PARTICIPANT_COUNT + iNullHashParticipants, iSize);

        iVal = participantDAO.updateNextBatchHashValuesForParticipants();
        assertEquals(iNullHashParticipants, iVal);
    }
}
