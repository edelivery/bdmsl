/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */

/**
 * @author Flavio SANTOS
 */
package eu.europa.ec.bdmsl.security;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.util.KeyUtil;
import eu.europa.ec.bdmsl.test.CommonTestUtils;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import java.nio.file.Paths;
import java.security.Security;
import java.security.cert.X509Certificate;

import static org.junit.jupiter.api.Assertions.*;


class SMLAuthenticationProviderTest extends AbstractJUnit5Test {

    static {
        // init Merlin library
        org.apache.xml.security.Init.init();
    }

    @BeforeAll
    static void beforeClass() {
        Security.insertProviderAt(new org.bouncycastle.jce.provider.BouncyCastleProvider(), 1);

    }

    @Test
    void testClientCertAuthenticateDisableLegacyFail() throws Exception {
        //given
        Mockito.doReturn(true).when(configurationBusiness).isLegacyDomainAuthorizationEnabled();

        String issuer = "CN=SMP_123456789,O=DG-DIGIT,C=BE";
        String subject = "CN=SMP_123456789,O=DG-DIGIT,C=BE";
        Authentication authentication = createClientCertPrincipal(issuer, subject);
        Authentication result = smlAuthenticationProvider.authenticate(authentication);
        assertNotNull(result);
        assertTrue(result.isAuthenticated());
        assertEquals("ROLE_SMP", result.getAuthorities().iterator().next().toString());
        // when
        Mockito.doReturn(false).when(configurationBusiness).isLegacyDomainAuthorizationEnabled();

        //when
        AuthenticationException exception = assertThrows(AuthenticationException.class,
                () -> smlAuthenticationProvider.authenticate(authentication));

        // then
        assertEquals("Certificate [Issuer: CN=SMP_123456789,O=DG-DIGIT,C=BE, Subject: CN=SMP_123456789,O=DG-DIGIT,C=BE] is not authorized!", exception.getMessage());
    }

    @Test
    void testCertificateAuthenticationWithTruststoreNotValid() throws Exception {
        //given
        String folderDir = targetDirectory.toFile().getAbsolutePath();
        String encryptKey = "encryptedPasswordKey.key";
        Mockito.doReturn(false).when(configurationBusiness).isLegacyDomainAuthorizationEnabled();
        Mockito.doReturn("bdmsl-truststore.p12").when(configurationBusiness).getTruststoreFilename();
        Mockito.doReturn(folderDir).when(configurationBusiness).getConfigurationFolder();
        Mockito.doReturn(encryptKey).when(configurationBusiness).getEncryptionFilename();
        Mockito.doReturn(KeyUtil.encrypt(Paths.get(folderDir, encryptKey).toFile().getAbsolutePath(), "test123"))
                .when(configurationBusiness).getTruststorePassword();

        configurationBusiness.forceRefreshProperties();
        X509Certificate certificate = CommonTestUtils.loadCertificate("smp-crl-test-nolist.pem"); // not valid certificate
        Authentication authentication = createX509Authentication(new X509Certificate[]{certificate});

        //when
        AuthenticationException exception = assertThrows(AuthenticationException.class,
                () -> smlAuthenticationProvider.authenticate(authentication));

        // then
        MatcherAssert.assertThat(exception.getMessage(),
                CoreMatchers.containsStringIgnoringCase("CertificateException: Certificate with [Issuer: CN=SMP CRL NoList,OU=CEF,O=DIGIT,C=EU, Subject: CN=SMP CRL NoList,O=DIGIT,C=EU] is not valid!"));
    }

    @Test
    void testCertificateAuthenticationWithTruststoreNotFound() throws Exception {
        //given
        String folderDir = targetDirectory.toFile().getAbsolutePath();
        String encryptKey = "encryptedPasswordKey.key";
        Mockito.doReturn(false).when(configurationBusiness).isLegacyDomainAuthorizationEnabled();
        Mockito.doReturn("bdmsl-truststore.p12").when(configurationBusiness).getTruststoreFilename();
        Mockito.doReturn(folderDir).when(configurationBusiness).getConfigurationFolder();
        Mockito.doReturn(encryptKey).when(configurationBusiness).getEncryptionFilename();
        Mockito.doReturn(KeyUtil.encrypt(Paths.get(folderDir, encryptKey).toFile().getAbsolutePath(), "test123"))
                .when(configurationBusiness).getTruststorePassword();

        configurationBusiness.forceRefreshProperties();

        String subject = "CN=ValidCertNotAuthorized,O=DG-DIGIT,C=BE";
        X509Certificate certificate = CommonTestUtils.createCertificate(subject, subject, false);

        Authentication authentication = createX509Authentication(new X509Certificate[]{certificate});

        //when
        AuthenticationException exception = assertThrows(AuthenticationException.class,
                () -> smlAuthenticationProvider.authenticate(authentication));

        // then
        assertEquals("Certificate [Issuer: CN=ValidCertNotAuthorized,O=DG-DIGIT,C=BE, Subject: CN=ValidCertNotAuthorized,O=DG-DIGIT,C=BE] does not match or was not found.",
                exception.getMessage());
    }

    @Test
    void testCertificateAuthenticationWithTruststoreIssuerOK() throws Exception {
        //given
        String folderDir = targetDirectory.toFile().getAbsolutePath();
        String encryptKey = "encryptedPasswordKey.key";
        Mockito.doReturn(false).when(configurationBusiness).isLegacyDomainAuthorizationEnabled();
        Mockito.doReturn("bdmsl-truststore.p12").when(configurationBusiness).getTruststoreFilename();
        Mockito.doReturn(folderDir).when(configurationBusiness).getConfigurationFolder();
        Mockito.doReturn(encryptKey).when(configurationBusiness).getEncryptionFilename();
        Mockito.doReturn(KeyUtil.encrypt(Paths.get(folderDir, encryptKey).toFile().getAbsolutePath(), "test123"))
                .when(configurationBusiness).getTruststorePassword();

        configurationBusiness.forceRefreshProperties();
        X509Certificate certificate = CommonTestUtils.loadCertificate("auth-smp-test-change.cer");
        Authentication authentication = createX509Authentication(new X509Certificate[]{certificate});

        //when
        CertificateAuthentication authenticated = (CertificateAuthentication) smlAuthenticationProvider.authenticate(authentication);

        // then
        assertTrue(authenticated.isAuthenticated());
        assertFalse(authenticated.getAuthorities().isEmpty());
        assertEquals("acc.edelivery.tech.ec.europa.eu", authenticated.getGrantedSubDomain().getSubdomainName());
    }

    @Test
    void testCertificateAuthenticationWithTruststoreMultipleDomainIssuerOK() throws Exception {
        //given
        String folderDir = targetDirectory.toFile().getAbsolutePath();
        String encryptKey = "encryptedPasswordKey.key";
        Mockito.doReturn(false).when(configurationBusiness).isLegacyDomainAuthorizationEnabled();
        Mockito.doReturn("bdmsl-truststore.p12").when(configurationBusiness).getTruststoreFilename();
        Mockito.doReturn(folderDir).when(configurationBusiness).getConfigurationFolder();
        Mockito.doReturn(encryptKey).when(configurationBusiness).getEncryptionFilename();
        Mockito.doReturn(KeyUtil.encrypt(Paths.get(folderDir, encryptKey).toFile().getAbsolutePath(), "test123"))
                .when(configurationBusiness).getTruststorePassword();

        configurationBusiness.forceRefreshProperties();
        X509Certificate certificate = CommonTestUtils.loadCertificate("second_domain_for-issuer.crt");
        Authentication authentication = createX509Authentication(new X509Certificate[]{certificate});

        //when
        CertificateAuthentication authenticated = (CertificateAuthentication) smlAuthenticationProvider.authenticate(authentication);

        // then
        assertTrue(authenticated.isAuthenticated());
        assertFalse(authenticated.getAuthorities().isEmpty());
        assertEquals("test-domain.acc.edelivery.tech.ec.europa.eu", authenticated.getGrantedSubDomain().getSubdomainName());

    }

    @Test
    void testClientCertAuthenticateCertBasedAuthenticationOK() {

        String issuer = "CN=SMP_123456789,O=DG-DIGIT,C=BE";
        String subject = "CN=SMP_123456789,O=DG-DIGIT,C=BE";
        String serialNumber = "1234";
        Authentication authentication = createClientCertPrincipal(issuer, subject, serialNumber);
        Mockito.doReturn(true).when(configurationBusiness).isLegacyDomainAuthorizationEnabled();

        // when
        Authentication result = smlAuthenticationProvider.authenticate(authentication);
        // then
        assertNotNull(result);
        assertTrue(result.isAuthenticated());
        assertEquals("ROLE_SMP", result.getAuthorities().iterator().next().toString());
    }

    @Test
    void testClientCertAuthenticateIssuerBasedAuthenticationOK() {
        //given
        Mockito.doReturn(false).when(configurationBusiness).isLegacyDomainAuthorizationEnabled();

        String issuer = "CN=Trusted Issuer CA,O=DIGIT,C=BE";
        String subject = "CN=SMP_Certificate,O=DIGIT,C=BE";
        String serialNumber = "1234";
        Authentication authentication = createClientCertPrincipal(issuer, subject, serialNumber);
        Mockito.doReturn(true).when(configurationBusiness).isLegacyDomainAuthorizationEnabled();

        // when
        Authentication result = smlAuthenticationProvider.authenticate(authentication);
        // then
        assertNotNull(result);
        assertTrue(result.isAuthenticated());
        assertEquals("ROLE_SMP", result.getAuthorities().iterator().next().toString());
    }

}
