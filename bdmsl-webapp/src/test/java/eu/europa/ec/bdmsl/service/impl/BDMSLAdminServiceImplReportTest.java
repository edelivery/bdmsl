/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.impl;

import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;
import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.enums.SMLReportEnum;
import eu.europa.ec.bdmsl.common.exception.BadRequestException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.security.UnsecureAuthentication;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import jakarta.mail.MessagingException;
import java.io.IOException;
import java.net.ServerSocket;
import java.security.Security;

import static org.hamcrest.CoreMatchers.containsStringIgnoringCase;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


class BDMSLAdminServiceImplReportTest extends AbstractJUnit5Test {
    @Autowired
    private JavaMailSenderImpl mailSender;

    private static GreenMail mockSmtp;
    private static int availablePort;

    @BeforeAll
    static void beforeClass() throws IOException {
        UnsecureAuthentication authentication = new UnsecureAuthentication();
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        // just get available port
        ServerSocket serverSocket = new ServerSocket(0);
        availablePort = serverSocket.getLocalPort();
        serverSocket.close();
        // setup mock smtp server
        ServerSetup SMTP = new ServerSetup(availablePort, null, ServerSetup.PROTOCOL_SMTP);
        mockSmtp = new GreenMail(SMTP);
        mockSmtp.start();

    }

    @AfterAll
    static void afterClass() {
        mockSmtp.stop();
    }

    @BeforeEach
    void setup() {
        mockSmtp.reset();
        mailSender.setPort(availablePort);
        mailSender.setHost("localhost");
    }

    @Test
    void generateReportFailEmptyReportName() {

        BadRequestException result = assertThrows(BadRequestException.class, () ->
                bdmslAdminService.generateReport("", "test@mail.com")
        );

        assertThat(result.getMessage(),
                CoreMatchers.startsWith("[ERR-106] Report name cannot be 'null' or empty! Allowed values:"));
        for (SMLReportEnum reportEnum : SMLReportEnum.values()) {
            assertThat(result.getMessage(),
                    containsStringIgnoringCase(reportEnum.name()));
        }
    }

    @Test
    void generateReportFailEmptyMail() {

        BadRequestException result = assertThrows(BadRequestException.class, () ->
                bdmslAdminService.generateReport(SMLReportEnum.SMP_WITH_EXPIRED_CERTIFICATE.name(), "")
        );

        assertThat(result.getMessage(),
                CoreMatchers.startsWith("[ERR-106] Recipient email address cannot be 'null' or empty!"));
    }

    @Test
    void generateReportFailInvalidMail() {

        BadRequestException result = assertThrows(BadRequestException.class, () ->
                bdmslAdminService.generateReport(SMLReportEnum.SMP_WITH_EXPIRED_CERTIFICATE.name(), "test#Mail@not!@Valid.eu")
        );

        assertThat(result.getMessage(),
                CoreMatchers.startsWith("[ERR-106] Email address [test#Mail@not!@Valid.eu] is not valid!"));
    }

    @Test
    void generateReportFailInvalidReportName() {

        BadRequestException result = assertThrows(BadRequestException.class, () ->
                bdmslAdminService.generateReport("InvalidReport", "test@mail.com")
        );

        assertThat(result.getMessage(),
                CoreMatchers.startsWith("[ERR-106] Invalid report name! Allowed values:"));
        for (SMLReportEnum reportEnum : SMLReportEnum.values()) {
            assertThat(result.getMessage(),
                    containsStringIgnoringCase(reportEnum.name()));
        }
    }

    @Test
    void generateSMPExpiredCertReportSuccess() throws TechnicalException, IOException, MessagingException {
        bdmslAdminService.generateReport(SMLReportEnum.SMP_WITH_EXPIRED_CERTIFICATE.name().toLowerCase(),
                "test@mail.com");
        mockSmtp.waitForIncomingEmail(1000, 1);

        assertEquals(1, mockSmtp.getReceivedMessages().length);
        String messageContent = (String) mockSmtp.getReceivedMessages()[0].getContent();
        assertThat(messageContent,
                containsStringIgnoringCase("## BDMSL REPORT: SMP list with an expired certificates! ##"));
        assertThat(messageContent,
                containsStringIgnoringCase("There are 2 SMP(s) with expired certificate"));
        assertThat(messageContent,
                containsStringIgnoringCase("SMP-EXPIRED-CERT-01"));
        assertThat(messageContent,
                containsStringIgnoringCase("SMP-EXPIRED-CERT-02"));
    }

    @Test
    void generateSMPExpiredCertReportSuccessCaseInsensitive() throws TechnicalException, IOException, MessagingException {
        bdmslAdminService.generateReport(SMLReportEnum.SMP_WITH_EXPIRED_CERTIFICATE.name().toLowerCase(),
                "test@mail.com");
        mockSmtp.waitForIncomingEmail(1000, 1);

        assertEquals(1, mockSmtp.getReceivedMessages().length);

        String messageContent = (String) mockSmtp.getReceivedMessages()[0].getContent();
        assertThat(messageContent,
                containsStringIgnoringCase("## BDMSL REPORT: SMP list with an expired certificates! ##"));
    }

    @Test
    void generateInconsistencyReportSuccess() throws TechnicalException, IOException, MessagingException {
        String result = bdmslAdminService.generateReport(SMLReportEnum.DATABASE_DNS_INCONSISTENCY.name().toLowerCase(),
                "test@mail.com");

        assertEquals("DNS server is not enabled. Request for Inconsistency report is ignored!", result);
    }
}
