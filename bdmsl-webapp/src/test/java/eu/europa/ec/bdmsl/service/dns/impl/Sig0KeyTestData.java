/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.dns.impl;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xbill.DNS.*;
import org.xbill.DNS.Record;

import java.io.*;
import java.net.InetAddress;
import java.security.Security;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The class contains Sig0KeyTestData for testing parsing the keys and for signing the responses.
 *
 * @author Joze RIHTARSIC
 * @since 4.3
 */

public class Sig0KeyTestData {
    private static final Logger LOG = LoggerFactory.getLogger(Sig0KeyTestData.class);

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    private static final String KEY_DSA = "Kdomisml.edelivery.eu.local.+003+44508";
    private static final String KEY_RSASHA256 = "Kdomisml.edelivery.eu.local.+008+54766";
    private static final String KEY_RSASHA512 = "Kdomisml.edelivery.eu.local.+010+59424";
    private static final String KEY_ECDSAP256SHA256 = "Kdomisml.edelivery.eu.local.+013+25493";
    private static final String KEY_ECDSAP384SHA384 = "Kdomisml.edelivery.eu.local.+014+24903";
    private static final String KEY_ED25519 = "Kdomisml.edelivery.eu.local.+015+29297";
    private static final String KEY_ED448 = "Kdomisml.edelivery.eu.local.+016+62139";

    protected static InputStream getKeyAsInputStream(String name) {
        return Bind9PrivateKeyParserTest.class.getResourceAsStream("/bind9-keys/" + name);
    }

    private static Object[] keyExamples() {
        return new Object[][]{
                {KEY_DSA, "DSA", "3"},
                {KEY_RSASHA256, "RSA", "8"},
                {KEY_RSASHA512, "RSA", "10"},
                {KEY_ECDSAP256SHA256, "EC", "13"},
                {KEY_ECDSAP384SHA384, "EC", "14"},
                {KEY_ED25519, "Ed25519", "15"},
                {KEY_ED448, "Ed448", "16"}
        };
    }

    public static Message readMessage(Reader in) throws IOException {
        BufferedReader r;
        if (in instanceof BufferedReader) {
            r = (BufferedReader) in;
        } else {
            r = new BufferedReader(in);
        }

        Message m = null;
        String line = null;
        int section = 103;
        while ((line = r.readLine()) != null) {
            String[] data;
            if (line.startsWith(";; ->>HEADER<<- ")) {
                section = 101;
                m = new Message();
            } else if (line.startsWith(";; QUESTIONS:")) {
                section = 102;
            } else if (line.startsWith(";; ANSWERS:")) {
                section = Section.ANSWER;
                line = r.readLine();
            } else if (line.startsWith(";; AUTHORITY RECORDS:")) {
                section = Section.AUTHORITY;
                line = r.readLine();
            } else if (line.startsWith(";; ADDITIONAL RECORDS:")) {
                section = 100;
            } else if (line.startsWith("####")) {
                return m;
            } else if (line.startsWith("#")) {
                continue;
            }

            switch (section) {
                case 100: // ignore
                    break;

                case 101: // header
                    section = 100;
                    data = line.substring(";; ->>HEADER<<- ".length()).split(",");
                    m.getHeader().setRcode(Rcode.value(data[1].split(":\\s*")[1]));
                    m.getHeader().setID(Integer.parseInt(data[2].split(":\\s*")[1]));
                    break;

                case 102: // question
                    line = r.readLine();
                    data = line.split(",");
                    Record q =
                            Record.newRecord(
                                    Name.fromString(data[0].replaceAll(";;\\s*", "")),
                                    Type.value(data[1].split("\\s*=\\s*")[1]),
                                    DClass.value(data[2].split("\\s*=\\s*")[1]));
                    m.addRecord(q, Section.QUESTION);
                    section = 100;
                    break;

                default:
                    if (line != null && !line.isEmpty()) {
                        Master ma = new Master(new ByteArrayInputStream(line.getBytes()));
                        Record record = ma.nextRecord();
                        if (record != null) {
                            m.addRecord(record, section);
                        }
                    }
            }
        }

        r.close();
        return m;
    }

    /**
     * Read record from stream and use first line as record
     *
     * @param in stream
     * @return Record from stream
     * @throws IOException
     */
    public static Record fromString(Reader in) throws IOException {
        BufferedReader r;
        if (in instanceof BufferedReader) {
            r = (BufferedReader) in;
        } else {
            r = new BufferedReader(in);
        }

        String line = r.readLine();
        LOG.info("Reading record from: [{}]", line);
        String[] data = line.split(" ");
        AtomicInteger my_index = new AtomicInteger();
        String skipFirstTokens = Stream.of(data).filter(s -> my_index.getAndIncrement() > 2)
                .collect(Collectors.joining(" "));

        return Record.fromString(Name.fromString(data[0]),
                Type.value(data[2]),
                DClass.value(data[1]),
                600L, skipFirstTokens, null);

    }

    public static Message createTestMessage(Name recordName) throws IOException {
        ARecord txtRecord = new ARecord(recordName, DClass.IN, 60, InetAddress.getByName("192.168.1.2"));
        Update updateMessage = new Update(recordName);
        updateMessage.add(txtRecord);
        return updateMessage;
    }
}
