package eu.europa.ec.bdmsl.service.validation;

import eu.europa.ec.bdmsl.common.enums.SMLPropertyEnum;
import eu.europa.ec.bdmsl.common.exception.BadConfigurationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @since 5.0
 * @author Sebastian-Ion TINCU
 */
public class NonEmptySMLPropertyValidatorTest {

    private NonEmptySMLPropertyValidator validator = new NonEmptySMLPropertyValidator();

    @Test
    public void ignoresSMLPropertyBOHavingNonEmptyValues() {
        Assertions.assertDoesNotThrow(() -> validator.validate(SMLPropertyEnum.AUTH_BLUE_COAT_ENABLED, "true", null));
    }

    @Test
    public void ignoresSMLPropertyBOHavingTypesOtherThanKeystoreType() {
        BadConfigurationException validationException = Assertions.assertThrows(BadConfigurationException.class, () -> validator.validate(SMLPropertyEnum.AUTH_BLUE_COAT_ENABLED, null, null));

        Assertions.assertEquals("[ERR-109] Property: [authentication.bluecoat.enabled] cannot be null or empty!", validationException.getMessage());
    }
}
