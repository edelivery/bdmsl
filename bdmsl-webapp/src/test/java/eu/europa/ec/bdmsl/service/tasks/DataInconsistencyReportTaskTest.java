/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.tasks;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.service.IDataQualityInconsistencyAnalyzerService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.UUID;




class DataInconsistencyReportTaskTest extends AbstractJUnit5Test {

    IDataQualityInconsistencyAnalyzerService dataQualityInconsistencyAnalyzerService;

    @Autowired
    DataInconsistencyReportTask dataInconsistencyReportTask;

    @BeforeEach
    protected void testSetup() {
        dataQualityInconsistencyAnalyzerService = Mockito.mock(IDataQualityInconsistencyAnalyzerService.class);
        ReflectionTestUtils.setField(dataInconsistencyReportTask, "dataQualityInconsistencyAnalyzerService", dataQualityInconsistencyAnalyzerService);
    }

    @Test
    void testReceiverAddress() {
        String testAddress = UUID.randomUUID()+ "@test.eu";
        dataInconsistencyReportTask.setReceiverAddress(testAddress);

        Assertions.assertEquals(testAddress, dataInconsistencyReportTask.getReceiverAddress());
    }

    @Test
    void testRun() throws TechnicalException {
        // given
        String testAddress = UUID.randomUUID() + "@test.eu";
        dataInconsistencyReportTask.setReceiverAddress(testAddress);
        Mockito.when(dataQualityInconsistencyAnalyzerService.updateNextBatchHashValuesForParticipants()).
                thenReturn(30)
                .thenReturn(0);

        // when
        dataInconsistencyReportTask.run();

        // then
        Mockito.verify(dataQualityInconsistencyAnalyzerService, Mockito.times(2)).updateNextBatchHashValuesForParticipants();
        Mockito.verify(dataQualityInconsistencyAnalyzerService, Mockito.times(1)).generateDataInconsistencyReport(testAddress);
    }

    @Test
    void updateHashValuesForParticipantsNothingToUpdate() throws TechnicalException {

        Mockito.when(dataQualityInconsistencyAnalyzerService.updateNextBatchHashValuesForParticipants()).
                thenReturn(0);

        dataInconsistencyReportTask.updateHashValuesForParticipants();
        Mockito.verify(dataQualityInconsistencyAnalyzerService, Mockito.times(1)).updateNextBatchHashValuesForParticipants();
    }

    @Test
    void updateHashValuesForParticipantsThreeTimes() throws TechnicalException {

        Mockito.when(dataQualityInconsistencyAnalyzerService.updateNextBatchHashValuesForParticipants()).
                thenReturn(30)
                .thenReturn(15)
                .thenReturn(0);

        dataInconsistencyReportTask.updateHashValuesForParticipants();
        Mockito.verify(dataQualityInconsistencyAnalyzerService,
                Mockito.times(3)).updateNextBatchHashValuesForParticipants();
    }
}
