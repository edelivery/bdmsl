/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.util;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.bo.CertificateBO;
import eu.europa.ec.bdmsl.common.exception.BadConfigurationException;
import eu.europa.ec.bdmsl.common.exception.CertificateAuthenticationException;
import eu.europa.ec.bdmsl.common.exception.GenericTechnicalException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.test.CommonTestUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.naming.ldap.LdapName;
import javax.naming.ldap.Rdn;
import javax.security.auth.x500.X500Principal;
import java.io.File;
import java.math.BigInteger;
import java.security.Key;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


/**
 * @author Tiago MIGUEL
 * @since 06/12/2016
 */
class CertificateUtilsTest extends AbstractJUnit5Test {

    @BeforeAll
    static void beforeClass() {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }


    @Test
    void testConvertBigIntToHexString() {
        assertEquals("000000018ee90ff6c373e0ee4e3f0ad2",
                CertificateUtils.convertBigIntToHexString(new BigInteger("123456789012345678901234567890")));
    }

    @Test
    void testReturnCertificateIdBigInteger() {
        assertEquals("TEST SUBJECT:0000000c7748819dffb62438d1c67eea",
                CertificateUtils.returnCertificateId("TEST SUBJECT", new BigInteger("987654321098765432109876543210")));
    }

    @Test
    void testReturnCertificateIdString() {
        assertEquals("TEST SUBJECT:0000000000000000000000008f658712",
                CertificateUtils.returnCertificateId("TEST SUBJECT", "0000000000000000000000008f658712"));
    }

    @Test
    void testReturnCertificateIdStringWithHexaHeader() {
        assertEquals("TEST SUBJECT:0000000000000000000000008f658712",
                CertificateUtils.returnCertificateId("TEST SUBJECT", "0x8f658712"));
    }


    @Test
    void hasDNRequiredValues() throws TechnicalException {
        // given
        String testDN = "CN=test,OU=My Unit,O=Organization,C=EU";
        // when then
        CertificateUtils.validateSubjectDNRequiredValues(testDN);
    }

    @Test
    void hasDNRequiredValuesMultiValue() throws TechnicalException {
        // given
        String testDN = "SerialNumber=234546546+CN=test,OU=My Unit,O=Organization,C=EU";
        // when then
        CertificateUtils.validateSubjectDNRequiredValues(testDN);
    }

    @Test
    void hasDNRequiredValuesFails() throws TechnicalException {
        // given
        String testDN = "CN=test,OU=My Unit,O=Organization";

        //WHEN
        CertificateAuthenticationException exception = assertThrows(CertificateAuthenticationException.class,
                () -> CertificateUtils.validateSubjectDNRequiredValues(testDN));
        // THEN
        assertEquals("[ERR-102] Certificate Subject DN [" + testDN + "] for required values: [" + String.join(",", CertificateUtils.getSubjectRequiredDnValues()) + "].", exception.getMessage());

    }

    @Test
    void testRemoveHexHeader() {
        assertEquals("1234567890", CertificateUtils.removeHexHeader("0x1234567890"));
    }

    @Test
    void testOrder() throws Exception {
        X509Certificate certificate = CommonTestUtils.createCertificate("C=PT, O=Systems GmbH, OU=T-Systems Center, L=Netphen/street\\=Untere Industriestr, CN=Business CA 85",
                "OU=eHealth, O=European Commission, OU=CEF_eDelivery.europa.eu,C=IT, CN=SMP_TEST_PRIME/emailAddress\\=CEF-EDELIVERY-SUPPORT@ec.europa.eu");

        String certificateDetails = CertificateUtils.order(certificate.getSubjectX500Principal().toString());
        assertEquals("C=IT,CN=SMP_TEST_PRIME/emailAddress\\=CEF-EDELIVERY-SUPPORT@ec.europa.eu,O=European Commission,OU=CEF_eDelivery.europa.eu", certificateDetails);
    }

    @Test
    void testOrderSubjectByDefaultMetadata() throws Exception {
        X509Certificate certificate = CommonTestUtils.createCertificate("C=PT, O=Systems GmbH, OU=T-Systems Center, L=Netphen/street\\=Untere Industriestr, CN=Business CA 85",
                "OU=eHealth, O=European Commission, OU=CEF_eDelivery.europa.eu,C=IT, CN=SMP_TEST_PRIME/emailAddress\\=CEF-EDELIVERY-SUPPORT@ec.europa.eu");

        String certificateDetails = CertificateUtils.orderSubjectByDefaultMetadata(certificate.getSubjectX500Principal().toString());
        assertEquals("CN=SMP_TEST_PRIME/emailAddress\\=CEF-EDELIVERY-SUPPORT@ec.europa.eu,O=European Commission,C=IT", certificateDetails);
    }

    @Test
    void testRemoveSerialFromSubject() throws Exception {
        String certificateId = "CN=SMP_123456,O=aoyEStralfors,C=SE:12897a5y9799b11740c6g853041442dd";
        String result = CertificateUtils.removeSerialFromSubject(certificateId);
        assertEquals("CN=SMP_123456,O=aoyEStralfors,C=SE", result);
    }

    @Test
    void testRemoveSerialFromSubjectWithColon() throws Exception {
        String certificateId = "CN=GPR:TEST_123456,O=aoyEStralfors,C=SE:12897a5y9799b11740c6g853041442dd";
        String result = CertificateUtils.removeSerialFromSubject(certificateId);
        assertEquals("CN=GPR:TEST_123456,O=aoyEStralfors,C=SE", result);
    }

    @Test
    void testRemoveSerialFromSubjectWrongFormat1() throws Exception {
        String certificateId = "CN=SMP_123456,O=aoyEStralfors,C=SE12897a5y9799b11740c6g853041442dd";
        assertThrows(GenericTechnicalException.class, () -> CertificateUtils.removeSerialFromSubject(certificateId));
    }


    @Test
    void testRemoveShorterDecimalFromHexSerialNumberString() throws Exception {
        String[][] serialNumbers = serialNumbersOk();
        for (int i = 0; i < serialNumbers.length; i++) {
            assertEquals(serialNumbers[i][1], CertificateUtils.removeShorterDecimalFromHexSerialNumberString(serialNumbers[i][0]));
        }
    }

    @Test
    void testRemoveShorterDecimalFromHexSerialNumberStringNotOk() throws Exception {
        String[][] serialNumbers = serialNumbersNotOk();
        for (int i = 0; i < serialNumbers.length; i++) {
            assertEquals(serialNumbers[i][1], CertificateUtils.removeShorterDecimalFromHexSerialNumberString(serialNumbers[i][0]));
        }
    }

    @Test
    void testNormalizeUnescape1() throws Exception {
        for (int i = 0; i < decodedUnicodes().length; i++) {
            assertEquals(decodedUnicodes()[i][0].toString(), CertificateUtils.normalizeUnescape(decodedUnicodes()[i][1].toString()));
        }
    }

    @Test
    void testNormalizeUnescape2() throws Exception {
        String certHeader = "sno=12%3A89%3A7a%3A5y%3A97%3A99%3Ab1%3A17%3A40%3Ac6%3Ag8%3A53%3A04%3A14%3A42%3Add&subject=C%3DSE%2C+O%3D%5CxC3%5CxA5%5Cxc3%5Cxb6%5Cxc3%5Cxbf%5Cxc3%5Cx8bStr%5CxC3%5CxA5lfors%2C+CN%3DSMP_123456&validfrom=Mar+20+00%3A00%3A00+2017+GMT&validto=Mar+20+23%3A59%3A59+2019+GMT&issuer=C%3DDK%2C+O%3D%5CxC3%5CxA5%5Cxc3%5Cxb6%5Cxc3%5Cxbf%5Cxc3%5Cx8bStr%5CxC3%5CxA5,L=TESTE%5C%3D12345";

        String expected = "sno=12:89:7a:5y:97:99:b1:17:40:c6:g8:53:04:14:42:dd&subject=C=SE, O=åöÿËStrålfors, CN=SMP_123456&validfrom=Mar 20 00:00:00 2017 GMT&validto=Mar 20 23:59:59 2019 GMT&issuer=C=DK, O=åöÿËStrå,L=TESTE\\=12345";
        assertEquals(expected, CertificateUtils.normalizeUnescape(certHeader));
    }

    @Test
    void testNormalizeUnicode1() {
        for (int i = 0; i < encodedUnicodes().length; i++) {
            assertEquals(encodedUnicodes()[i][0].toString(), CertificateUtils.normalizeUnicode((encodedUnicodes()[i][1].toString())));
        }
    }

    @Test
    void testDecodeUtfHexLiterals() {
        String encodedheader = "subject=C=PL, O=DE\\xE1\\xBA\\x9E\\xC3\\x9F\\xC3\\x84\\xC3\\xA4PL\\xC5\\xBC\\xC3\\xB3\\xC5\\x82\\xC4\\x87NO\\xC3\\x86\\xC3\\xA6\\xC3\\x98\\xC3\\xB8\\xC3\\x85\\xC3\\xA5, CN=slash/back\\slash\\quote\"colon:_rfc2253special_ampersand&comma,equals=plus+lessthan<greaterthan>hash#semicolon;end";
        assertEquals("subject=C=PL, O=DEẞßÄäPLżółćNOÆæØøÅå, CN=slash/back\\slash\\quote\"colon:_rfc2253special_ampersand&comma,equals=plus+lessthan<greaterthan>hash#semicolon;end", CertificateUtils.decodeUtfHexLiterals(encodedheader));
    }

    @Test
    void testNormalizeUnicode2() {
        String original = "äåãaaáeéiïíoóöőuúüűńñÿ AÁEËÉIÍOÓÖŐUŪÚÜŰŘ";
        assertEquals("aaaaaaeeiiioooouuuunny AAEEEIIOOOOUUUUUR", CertificateUtils.normalizeUnicode(original));
    }

    @Test
    void testNormalizeDistinguishedName1() {
        String original = "CN=\"slash/backslash\\\\quote\\\"colon:_rfc2253special_ampersand&comma,equals=plus+lessthan<greaterthan>hash#semicolon;end\", O=DEẞßÄäPLżółćNOÆæØøÅå, C=PL";
        assertEquals("CN=slash/backslash\\\\quote\\\"colon:_rfc2253special_ampersand&comma\\,equals\\=plus\\+lessthan\\<greaterthan\\>hash\\#semicolon\\;end, O=DEẞßÄäPLżółćNOÆæØøÅå, C=PL", CertificateUtils.normalizeDistinguishedName(original));
    }

    @Test
    void testNormalizeDistinguishedName2() {
        String[][] tests = allSmartEscapingTestCases();
        for (int i = 0; i < tests.length; i++) {
            assertEquals(tests[i][1], CertificateUtils.normalizeDistinguishedName(tests[i][0]));
        }
    }
/*
    @Test
    void testNormalizeForX509() throws Exception {
        String original = "CN=lash/backslash\\\\quote\"colon:_rfc2253special_ampersand&comma,equals=plus+lessthan<greaterthan>hash#semicolon;end\", O=DEẞßÄäPLżółćNOÆæØøÅås, C=PL";
        assertEquals("CN=lash/backslash\\\\quote\\\"colon:_rfc2253special_ampersand&comma\\,equals\\=plus\\+lessthan\\<greaterthan\\>hash\\#semicolon\\;end\\\", O=DEẞßAaPLzołcNOÆæØøAas, C=PL", CertificateUtils.normalizeForX509(original));
    }
    */


    @Test
    void testNormalizeForBlueCoat() throws Exception {
        String original = "sno=4112+%280x1010%29&subject=C%3DPL%2C+O%3DDE%5CxE1%5CxBA%5Cx9E%5CxC3%5Cx9F%5CxC3%5Cx84%5CxC3%5CxA4PL%5CxC5%5CxBC%5CxC3%5CxB3%5CxC5%5Cx82%5CxC4%5Cx87NO%5CxC3%5Cx86%5CxC3%5CxA6%5CxC3%5Cx98%5CxC3%5CxB8%5CxC3%5Cx85%5CxC3%5CxA5%2C+CN%3Dslash%2Fbackslash%5Cquote%22colon%3A_rfc2253special_ampersand%26comma%2Cequals%3Dplus%2Blessthan%3Cgreaterthan%3Ehash%23semicolon%3Bend&validfrom=Oct+12+12%3A57%3A42+2017+GMT&validto=Jan+18+12%3A57%3A42+2028+GMT&issuer=C%3DBE%2C+ST%3DBelgium%2C+O%3DConnectivity+Test%2C+OU%3DConnecting+Europe+Facility%2C+CN%3DConnectivity+Test+Component+CA%2FemailAddress%3DCEF-EDELIVERY-SUPPORT%40ec.europa.eu";
        assertEquals("sno=4112 (0x1010)&subject=C=PL, O=DEẞßAaPLzołcNOÆæØøAa, CN=slash/backslash\\quote\"colon:_rfc2253special_ampersand&comma,equals=plus+lessthan<greaterthan>hash#semicolon;end&validfrom=Oct 12 12:57:42 2017 GMT&validto=Jan 18 12:57:42 2028 GMT&issuer=C=BE, ST=Belgium, O=Connectivity Test, OU=Connecting Europe Facility, CN=Connectivity Test Component CA/emailAddress=CEF-EDELIVERY-SUPPORT@ec.europa.eu", CertificateUtils.normalizeForBlueCoat(original));
    }


    @Test
    void testGetNotCACertificate() throws Exception {

        String targetCert = "CN=SMP_receiverCN,O=DIGIT,C=BE";
        String issuerInterCA = "CN=Chain4 IntermediateCA,O=DIGIT-B28, C=BE";
        X509Certificate[] certLst = CommonTestUtils.createCertificateChain(new String[]{"CN=Chain4 RootCA,O=DIGIT-B28, C=BE",
                issuerInterCA,
                targetCert}, new Date(), CommonTestUtils.getFutureYearDate(2).getTime());
        X509Certificate crt = CertificateUtils.getNotCACertificate(certLst);

        List<Rdn> expected = new LdapName(targetCert).getRdns();
        List<Rdn> rdnSubject = new LdapName(crt.getSubjectX500Principal().getName(X500Principal.RFC1779)).getRdns();

        List<Rdn> expectedIssuer = new LdapName(issuerInterCA).getRdns();
        List<Rdn> rdnIssuer = new LdapName(crt.getIssuerX500Principal().getName(X500Principal.RFC1779)).getRdns();

        assertTrue(expected.containsAll(rdnSubject));
        assertTrue(expectedIssuer.containsAll(rdnIssuer));
    }

    @Test
    void testGetNotCACertificateSelfSigned() throws Exception {

        String serial = "0123456789101112";
        String issuer = "SERIALNUMBER=1+CN=SMP_receiverCN+GivenName=john,O=DIGIT,C=BE";
        String subject = "SERIALNUMBER=1+CN=SMP_receiverCN+GivenName=john,O=DIGIT,C=BE";
        X509Certificate certificate = CommonTestUtils.createCertificate(serial, issuer, subject, new Date(), CommonTestUtils.getFutureYearDate(2).getTime());
        X509Certificate crt = CertificateUtils.getNotCACertificate(new X509Certificate[]{certificate});
        assertEquals(certificate, crt);

    }

    @Test
    void testGetIssuer() throws Exception {

        String targetCert = "CN=SMP_receiverCN,O=DIGIT,C=BE";
        String issuerInterCA = "CN=Chain4 IntermediateCA,O=DIGIT-B28, C=BE";
        X509Certificate[] certLst = CommonTestUtils.createCertificateChain(new String[]{"CN=Chain4 RootCA,O=DIGIT-B28, C=BE",
                issuerInterCA,
                targetCert}, new Date(), CommonTestUtils.getFutureYearDate(2).getTime());

        X509Certificate crt = CertificateUtils.getNotCACertificate(certLst);
        X509Certificate crtIssuer = CertificateUtils.getIssuerCertificate(crt, certLst);

        assertEquals(crt.getIssuerX500Principal().toString(), crtIssuer.getSubjectX500Principal().toString());

    }


    String[][] allSmartEscapingTestCases() {
        return new String[][]{{
                "CN=names that do not need escaping stay untouched,C=PL",
                "CN=names that do not need escaping stay untouched,C=PL"
        }, {
                "CN=comma needz , to be escaped,C=PL",
                "CN=comma needz \\, to be escaped,C=PL"
        }, {
                "CN=already escaped\\, are not escaped again,C=PL",
                "CN=already escaped\\, are not escaped again,C=PL",
        }, {
                "CN=comma needz to be escaped even at the end but not the last one,,C=PL",
                "CN=comma needz to be escaped even at the end but not the last one\\,,C=PL",
        }, {
                "CN=allRDNs,,OU=,,O=,,L=,,ST=,,C=,,E=,",
                "CN=allRDNs\\,,OU=\\,,O=\\,,L=\\,,ST=\\,,C=\\,,E=\\,"
        }, {
                " CN= trailing and leading spaces of RDN values are ignored by RFC2253 , C= PL ",
                " CN=trailing and leading spaces of RDN values are ignored by RFC2253, C=PL",
        }};
    }

    private String[][] serialNumbersOk() throws Exception {
        return new String[][]{
                {"400004584 (0x17d795e8)", "0x17d795e8"},
                {"0012 (c)", "c"},
                {"0016 (0x10)", "0x10"},
                {"1393 (0x571)", "0x571"},
                {"1393 (0x989898)", "0x989898"},
                {"0997 (0x3e5)", "0x3e5"}};
    }

    private String[][] serialNumbersNotOk() throws Exception {
        return new String[][]{
                {"400004584 0x17d795e8)", "400004584 0x17d795e8)"},
                {"(0x26)0038", "(0x26)0038"},
                {"123456 (0x26) 0038", "123456 (0x26) 0038"},
                {"0012", "0012"},
                {"(0x989898) 1393 (0x989898)", "(0x989898) 1393 (0x989898)"},
                {"00161524545454578787", "00161524545454578787"},
                {"1393 (0x571", "1393 (0x571"},
                {"0997 0x3e5", "0997 0x3e5"}};
    }

    private String[][] encodedUnicodes() {
        return new String[][]{{"a", "ä"}, {"a", "å"}, {"e", "é"}, {"y", "ÿ"}, {"o", "ö"}, {"R", "Ř"}};

    }

    private String[][] decodedUnicodes() {
        return new String[][]{{"å", "\\xc3\\xa5"}, {"ä", "\\xc3\\xa4"}, {"ï", "\\xc3\\xaf"}, {"ö", "\\xc3\\xb6"}, {"ÿ", "\\xc3\\xbf"},
                {"ñ", "\\xc3\\xb1"}, {"Ë", "\\xc3\\x8b"}, {"ń", "\\xc5\\x84"}, {"Ř", "\\xc5\\x98"}, {"Ū", "\\xc5\\xaa"}, {"Ł", "\\xc5\\x81"}, {"L", "\\x4c"}, {"d", "\\x64"}};

    }

    @Test
    void extractCertificateTest() throws CertificateException, TechnicalException {
        //given
        X509Certificate certificate = CommonTestUtils.loadCertificate("smp-crl-test-all.pem");
        String certificateId = CertificateUtils.calculateCertificateId(certificate);
        // when
        CertificateBO certificateBO = CertificateUtils.extractCertificate(certificate);

        assertEquals(0, DateUtils.toCalendar(certificate.getNotBefore()).compareTo(certificateBO.getValidFrom()));
        assertEquals(0, DateUtils.toCalendar(certificate.getNotAfter()).compareTo(certificateBO.getValidTo()));

        assertNotNull(certificateBO.getPemEncoding());
        assertEquals(certificateId, certificateBO.getCertificateId());
    }

    @Test
    void getSigningKey() throws Exception {
        // GIVEN
        String keystoreType = "JKS";
        File keystorePath = new File(resourceDirectory.toAbsolutePath() + File.separator + "keystore.jks");
        String keystorePassword = "test";
        String alias = "receiveralias";

        // WHEN
        Key signingKey = CertificateUtils.getSigningKey(keystoreType, keystorePath, keystorePassword, alias);

        // THEN
        assertNotNull( signingKey,"Should have loaded the signing key from the keystore");
    }

    @Test
    void getSigningKey_invalidKeystoreType() {
        // GIVEN
        String keystoreType = "INVALID";

        // WHEN
        assertThrows(BadConfigurationException.class,
                () -> CertificateUtils.getSigningKey(keystoreType, new File(resourceDirectory.toAbsolutePath() + File.separator + "keystore.jks"), "test", "receiveralias"),
                "[ERR-109] Can not open keystore");
    }
}
