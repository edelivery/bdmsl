/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.entity;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * This test class validates the data model {@link eu.europa.ec.bdmsl.dao.entity.ConfigurationEntity)} and its constraints
 *
 * @author Flavio SANTOS
 * @since 22/09/2016
 */
class ConfigurationConstraintsValidationTest extends AbstractJUnit5Test {

    @Test
    void persistIntoConfigurationTableOk() throws SQLException {
        String sql = "INSERT INTO bdmsl_configuration (property,value,description,created_on,last_updated_on) values (?,?,?,?,?)";
        Timestamp date = new Timestamp(Calendar.getInstance().getTimeInMillis());

        assertDoesNotThrow(() -> persist(sql, "testConfig", "doNothing", "doNothing description", date, date));
    }

    @Test
    void persistIntoConfigurationTablePKNotOk() throws SQLException {
        String sql = "INSERT INTO bdmsl_configuration (property,value,description,created_on,last_updated_on) values (?,?,?,?,?)";
        Timestamp date = new Timestamp(Calendar.getInstance().getTimeInMillis());
        assertThrows(SQLException.class, () -> persistTestDirectoryTwice(sql, date));
    }

    private void persistTestDirectoryTwice(String sql, Timestamp date) throws SQLException {
        persist(sql, "testDir", "server/path/app", "The absolute path to the folder containing all the configuration files (keystore and sig0 key)", date, date);
        persist(sql, "testDir", "server/path/app", "The absolute path to the folder containing all the configuration files (keystore and sig0 key)", date, date);
    }

    @Test
    void persistIntoConfigurationTablePropertyNull() throws SQLException {
        String sql = "INSERT INTO bdmsl_configuration (property,value,description,created_on,last_updated_on) values (?,?,?,?,?)";
        Timestamp date = new Timestamp(Calendar.getInstance().getTimeInMillis());
        assertThrows(SQLException.class,
                () -> persist(sql, null, "doNothing", "doNothing description", date, date));

    }

    @Test
    void persistIntoConfigurationTableDescriptionNull() throws SQLException {
        String sql = "INSERT INTO bdmsl_configuration (property,value,description,created_on,last_updated_on) values (?,?,?,?,?)";
        Timestamp date = new Timestamp(Calendar.getInstance().getTimeInMillis());

        assertDoesNotThrow(() -> persist(sql, "testDir2", "doNothing description", null, date, date));
    }

    private void persist(String sqlQuery, String property, String value, String description, Timestamp createdOn, Timestamp updatedOn) throws SQLException {
        try (Connection connection = dataSource.getConnection();
                PreparedStatement pstmt = connection.prepareStatement(sqlQuery)) {
            pstmt.setString(1, property);
            pstmt.setString(2, value);
            pstmt.setString(3, description);
            pstmt.setTimestamp(4, createdOn);
            pstmt.setTimestamp(5, updatedOn);
            pstmt.executeUpdate();
            connection.commit();
        }
    }
}
