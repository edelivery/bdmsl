/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.*;
import javax.naming.spi.InitialContextFactory;
import java.util.Hashtable;

/**
 * MockInitialContextFactory is used to create a mock JNDI context for unit testing.
 * It initializes a JNDI context with a Hashtable of environment properties.
 *
 * @author Joze Rihtarsic
 * @since 5.0
 */
public class MockInitialContextFactory implements InitialContextFactory {
    protected static final Logger LOG = LoggerFactory.getLogger(MockInitialContextFactory.class);

    Context initialContext;
    private static final Hashtable<String, Object> environment = new Hashtable<>();


    @Override
    public Context getInitialContext(Hashtable<?, ?> environment) {
        LOG.debug("MockInitialContextFactory.getInitialContext [{}]", environment);
        if (this.initialContext == null) {
            createInitialContext(environment);
        }
        return initialContext;
    }

    private void createInitialContext(Hashtable<?, ?> environment) {
        LOG.debug("MockInitialContextFactory.createInitialContext [{}]", environment);
        this.initialContext = new Context() {
            @Override
            public Object lookup(Name name) {
                return lookup(name.toString());
            }

            @Override
            public Object lookup(String name) {
                LOG.info("lookup JNDI name: [{}]", name);
                return MockInitialContextFactory.environment.get(name);
            }

            @Override
            public void bind(Name name, Object obj) {
                bind(name.toString(), obj);
            }

            @Override
            public void bind(String name, Object obj) {
                LOG.debug("bind [{}], object [{}]", name, obj);
                MockInitialContextFactory.environment.put(name, obj);
            }

            @Override
            public void rebind(Name name, Object obj) {
                rebind(name.toString(), obj);

            }

            @Override
            public void rebind(String name, Object obj) {
                LOG.debug("rebind [{}], object [{}]", name, obj);
                MockInitialContextFactory.environment.put(name, obj);
            }

            @Override
            public void unbind(Name name) {
                unbind(name.toString());
            }

            @Override
            public void unbind(String name) {
                LOG.debug("unbind [{}]", name);
                MockInitialContextFactory.environment.remove(name);
            }

            @Override
            public void rename(Name oldName, Name newName) {
                // do nothing
            }

            @Override
            public void rename(String oldName, String newName) {
                // do nothing
            }

            @Override
            public NamingEnumeration<NameClassPair> list(Name name) {
                return null;
            }

            @Override
            public NamingEnumeration<NameClassPair> list(String name) {
                return null;
            }

            @Override
            public NamingEnumeration<Binding> listBindings(Name name) {
                return null;
            }

            @Override
            public NamingEnumeration<Binding> listBindings(String name) {
                return null;
            }

            @Override
            public void destroySubcontext(Name name) {
                // do nothing
            }

            @Override
            public void destroySubcontext(String name) {
                // do nothing
            }

            @Override
            public Context createSubcontext(Name name) {
                return null;
            }

            @Override
            public Context createSubcontext(String name) {
                return null;
            }

            @Override
            public Object lookupLink(Name name) {
                return null;
            }

            @Override
            public Object lookupLink(String name) {
                return null;
            }

            @Override
            public NameParser getNameParser(Name name) {
                return null;
            }

            @Override
            public NameParser getNameParser(String name) {
                return null;
            }

            @Override
            public Name composeName(Name name, Name prefix) {
                return null;
            }

            @Override
            public String composeName(String name, String prefix) {
                return "";
            }

            @Override
            public Object addToEnvironment(String propName, Object propVal) {
                return null;
            }

            @Override
            public Object removeFromEnvironment(String propName) {
                return null;
            }

            @Override
            public Hashtable<?, ?> getEnvironment() {
                return null;
            }

            @Override
            public void close() {
                // do nothing
            }

            @Override
            public String getNameInNamespace() {
                return "";
            }
        };

    }
}
