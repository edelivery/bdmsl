/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.impl;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.bo.CertificateDomainBO;
import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import eu.europa.ec.bdmsl.common.bo.TruststoreCertificateBO;
import eu.europa.ec.bdmsl.common.exception.BadRequestException;
import eu.europa.ec.bdmsl.common.exception.NotFoundException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.common.util.KeyUtil;
import eu.europa.ec.bdmsl.test.CommonTestUtils;
import eu.europa.ec.bdmsl.security.UnsecureAuthentication;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StreamUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;


class BDMSLAdminServiceImplTest extends AbstractJUnit5Test {



    @BeforeAll
    static void beforeClass() {
        UnsecureAuthentication authentication = new UnsecureAuthentication();
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    @BeforeEach
    protected void testSetup() throws Exception {
        String encryptKey = "encryptedPasswordKey.key";
        String truststore = "bdmsl-truststore.p12";
        String truststoreNew = "bdmsl-truststore_" + UUID.randomUUID().toString() + ".p12";
        Files.copy(resourceDirectory.resolve(truststore), targetDirectory.resolve(truststoreNew), StandardCopyOption.REPLACE_EXISTING);


        String resourcesPath = targetDirectory.toFile().getAbsolutePath();

        Mockito.doReturn(false).when(configurationBusiness).isLegacyDomainAuthorizationEnabled();
        Mockito.doReturn(truststoreNew).when(configurationBusiness).getTruststoreFilename();
        Mockito.doReturn(resourcesPath).when(configurationBusiness).getConfigurationFolder();
        Mockito.doReturn(encryptKey).when(configurationBusiness).getEncryptionFilename();
        Mockito.doReturn(KeyUtil.encrypt(Paths.get(resourcesPath, encryptKey).toFile().getAbsolutePath(), "test123"))
                .when(configurationBusiness).getTruststorePassword();
    }

    @AfterEach
    void clean() throws Exception {
        File file = configurationBusiness.getTruststoreFile();
        file.delete();
    }

    @Test
    void createSubDomainTest() throws TechnicalException {
        SubdomainBO subdomainBO = CommonTestUtils.createSubDomainBO("domain.ec.TEST.eu", "ec.TEST.eu"
                , ".*", "all", "all");
        SubdomainBO res = bdmslAdminService.createSubDomain(subdomainBO);

        assertNotNull(res.getSubdomainId());
        assertEquals(subdomainBO.getSubdomainName(), res.getSubdomainName());
        assertEquals(subdomainBO.getSmpUrlSchemas(), res.getSmpUrlSchemas());
        assertEquals(subdomainBO.getDnsRecordTypes(), res.getDnsRecordTypes());
        assertEquals(subdomainBO.getParticipantIdRegexp(), res.getParticipantIdRegexp());
        assertEquals(subdomainBO.getDnsZone(), res.getDnsZone());
        assertEquals(subdomainBO.getDescription(), res.getDescription());
        assertNotNull(res.getCreationDate());
        assertNotNull(res.getLastUpdateDate());
    }

    @Test
    void createSubDomaiCaseTest() throws TechnicalException {
        SubdomainBO subdomainBO = CommonTestUtils.createSubDomainBO("domain.ec.TEST.eu", "ec.TEST.eu"
                , ".*", "ALL", "ALL");
        SubdomainBO res = bdmslAdminService.createSubDomain(subdomainBO);

        assertNotNull(res.getSubdomainId());
        assertEquals(subdomainBO.getSubdomainName(), res.getSubdomainName());
        assertEquals(subdomainBO.getSmpUrlSchemas().toLowerCase(), res.getSmpUrlSchemas());
        assertEquals(subdomainBO.getDnsRecordTypes().toLowerCase(), res.getDnsRecordTypes());
        assertEquals(subdomainBO.getDnsZone().toLowerCase(), res.getDnsZone());

    }

    @Test
    void updateSubDomain() throws TechnicalException {
        // given
        BigInteger maxParticipantCountForDomain = BigInteger.valueOf(1000000);
        BigInteger maxParticipantCountForSMP = BigInteger.valueOf(99999);
        SubdomainBO subdomainBO = CommonTestUtils.createSubDomainBO("domain.ec.TEST.eu", "ec.TEST.eu"
                , ".*", "all", "all");
        bdmslAdminService.createSubDomain(subdomainBO);
        // when
        SubdomainBO res = bdmslAdminService.updateSubDomain(subdomainBO.getSubdomainName(),
                "test", "cname", "https",
                null, null, maxParticipantCountForDomain, maxParticipantCountForSMP);
        // then
        assertNotNull(res.getSubdomainId());
        assertEquals(subdomainBO.getSubdomainName(), res.getSubdomainName());
        assertEquals("https", res.getSmpUrlSchemas());
        assertEquals("cname", res.getDnsRecordTypes());
        assertEquals("test", res.getParticipantIdRegexp());
        assertEquals(subdomainBO.getDnsZone(), res.getDnsZone());
        assertEquals(subdomainBO.getDescription(), res.getDescription());
        assertEquals(maxParticipantCountForDomain, res.getMaxParticipantCountForDomain());
        assertEquals(maxParticipantCountForSMP, res.getMaxParticipantCountForSMP());
        assertNotNull(res.getCreationDate());
        assertNotNull(res.getLastUpdateDate());
    }

    @Test
    void getSubDomain() throws TechnicalException {
        // given
        SubdomainBO subdomainBO = CommonTestUtils.createSubDomainBO("domain.ec.TEST.eu", "ec.TEST.eu"
                , ".*", "all", "all");
        bdmslAdminService.createSubDomain(subdomainBO);

        SubdomainBO res = bdmslAdminService.getSubDomain(subdomainBO.getSubdomainName());

        assertNotNull(res.getSubdomainId());
        assertEquals(subdomainBO.getSubdomainName(), res.getSubdomainName());
        assertEquals(subdomainBO.getSmpUrlSchemas(), res.getSmpUrlSchemas());
        assertEquals(subdomainBO.getDnsRecordTypes(), res.getDnsRecordTypes());
        assertEquals(subdomainBO.getParticipantIdRegexp(), res.getParticipantIdRegexp());
        assertEquals(subdomainBO.getDnsZone(), res.getDnsZone());
        assertEquals(subdomainBO.getDescription(), res.getDescription());
        assertEquals(subdomainBO.getMaxParticipantCountForDomain(), res.getMaxParticipantCountForDomain());
        assertEquals(subdomainBO.getMaxParticipantCountForSMP(), res.getMaxParticipantCountForSMP());
        assertNotNull(res.getCreationDate());
        assertNotNull(res.getLastUpdateDate());
    }

    @Test
    void deleteSubDomain() throws TechnicalException {
        // given
        SubdomainBO subdomainBO = CommonTestUtils.createSubDomainBO("domain.ec.TEST.eu", "ec.TEST.eu"
                , ".*", "all", "all");
        bdmslAdminService.createSubDomain(subdomainBO);
        // when
        SubdomainBO res = bdmslAdminService.deleteSubDomain(subdomainBO.getSubdomainName());
        assertNotNull(res.getSubdomainId());
        assertEquals(subdomainBO.getSubdomainName(), res.getSubdomainName());

        // get
        assertThrows(NotFoundException.class, () -> bdmslAdminService.getSubDomain(subdomainBO.getSubdomainName()),"[ERR-118] SubDomain does not exist: domain");
    }


    @Test
    void addCertificateDomainRootTest() throws IOException, TechnicalException {
        // given
        byte[] xCertBuff = StreamUtils.copyToByteArray(BDMSLAdminServiceImpl.class
                .getResourceAsStream("/certificates/smp-crl-test-all.pem"));
        SubdomainBO subdomainBO = CommonTestUtils.createSubDomainBO("domain.ec.TEST.eu", "ec.TEST.eu"
                , ".*", "all", "all");
        bdmslAdminService.createSubDomain(subdomainBO);

        // when
        CertificateDomainBO res = bdmslAdminService.addCertificateDomain(xCertBuff, subdomainBO.getSubdomainName(), true, false, null);

        // Then
        assertNotNull(res);
        assertEquals("CN=SMP CRL TEST ALL,O=DIGIT,C=EU", res.getCertificate());
        assertEquals("https://localhost/clr", res.getCrl());
        assertEquals(subdomainBO.getSubdomainName(), res.getSubdomain().getSubdomainName());
        assertFalse(res.isAdmin());
        assertTrue(res.isRootCA());
    }

    @Test
    void addCertificateDomainNotRootTest() throws IOException, TechnicalException {
        // given
        byte[] xCertBuff = StreamUtils.copyToByteArray(BDMSLAdminServiceImpl.class
                .getResourceAsStream("/certificates/smp-crl-test-all.pem"));
        SubdomainBO subdomainBO = CommonTestUtils.createSubDomainBO("domain.ec.test.eu", "ec.test.eu"
                , ".*", "all", "all");
        bdmslAdminService.createSubDomain(subdomainBO);

        // when
        CertificateDomainBO res = bdmslAdminService.addCertificateDomain(xCertBuff, subdomainBO.getSubdomainName(), false, false, null);

        // Then
        assertNotNull(res);
        assertEquals("CN=SMP CRL TEST ALL,O=DIGIT,C=EU", res.getCertificate());
        assertEquals("https://localhost/clr", res.getCrl());
        assertEquals(subdomainBO.getSubdomainName(), res.getSubdomain().getSubdomainName());
        assertFalse(res.isAdmin());
        assertFalse(res.isRootCA());
    }

    @Test
    void addCertificateDomainNotExistsTest() throws IOException {
        // given
        byte[] xCertBuff = StreamUtils.copyToByteArray(BDMSLAdminServiceImpl.class
                .getResourceAsStream("/certificates/smp-crl-test-all.pem"));

        // when
        NotFoundException notFoundException = assertThrows(NotFoundException.class, () -> bdmslAdminService.addCertificateDomain(xCertBuff, "NotExists.ec.test.eu", false, false, null));
        assertEquals("[ERR-118] SubDomain does not exist: notexists.ec.test.eu", notFoundException.getMessage());

    }

    @Test
    void testListAllTrustedCertificates() throws TechnicalException {
        // given
        // when
        List<String> aliases = bdmslAdminService.listTruststoreCertificateAliases(null);
        // then
        assertEquals(3, aliases.size());
        System.out.println(aliases);
    }

    @Test
    void testListAllTrustedCertificateFilter() throws TechnicalException {
        // given
        // when
        List<String> aliases = bdmslAdminService.listTruststoreCertificateAliases("smp");
        // then
        assertEquals( 1, aliases.size(),"Expected 2 certificates with smp but get:[" + aliases + "]!");
    }

    @Test
    void testGetTrustedCertificate() throws TechnicalException {
        // given
        String alias = "smp_test_change (test intermediate issuer 01)";
        // when
        TruststoreCertificateBO certificateBO = bdmslAdminService.getCertificateFromTruststore(alias);
        // then
        assertNotNull(certificateBO);
        assertEquals(alias, certificateBO.getAlias());
        assertNotNull(certificateBO.getCertificatePublicKey());
    }

    @Test
    void testGetTrustedCertificateFailInvalidAlias() {
        // given
        String alias = "";
        // when
        BadRequestException exception = assertThrows(BadRequestException.class, () -> bdmslAdminService.getCertificateFromTruststore(alias));
        // then
        assertEquals(exception.getMessage(), "[ERR-106] Invalid alias [" + alias + "]!");
    }

    @Test
    void testGetTrustedCertificateFailCertificateNotExistsForAlias() {
        // given
        String alias = "smpNotExists";
        // when
        NotFoundException exception = assertThrows(NotFoundException.class, () -> bdmslAdminService.getCertificateFromTruststore(alias));
        // then
        assertEquals(exception.getMessage(), "[ERR-118] The certificate with alias [" + alias + "] does not exist!");
    }

    @Test
    void testAddTrustedCertificate() throws Exception {
        // given
        String alias = "new test certificate";
        X509Certificate certificate = CommonTestUtils.createCertificate("CN=Keystore test 01,O=Digit,C=BE", "CN=Keystore test 01,O=Digit,C=BE", false);
        List<String> aliases = bdmslAdminService.listTruststoreCertificateAliases(null);
        // when
        TruststoreCertificateBO certificateBO = bdmslAdminService.addCertificateToTruststore(certificate.getEncoded(), alias);
        // then
        List<String> aliasesNew = bdmslAdminService.listTruststoreCertificateAliases(null);

        assertEquals(aliases.size() + 1, aliasesNew.size());
    }

    @Test
    void testAddTrustedCertificateFailAliasExists() throws Exception {
        // given
        String alias = "smp_test_change (test intermediate issuer 01)";
        X509Certificate certificate = CommonTestUtils.createCertificate("CN=Keystore test 01,O=Digit,C=BE", "CN=Keystore test 01,O=Digit,C=BE", false);
        // when
        BadRequestException exception = assertThrows(BadRequestException.class, () ->
                bdmslAdminService.addCertificateToTruststore(certificate.getEncoded(), alias));
        // then
        assertEquals(exception.getMessage(), "[ERR-106] Certificate with alias [" + alias + "] already exists!");
    }

    @Test
    void testAddTrustedCertificateFailCertificateExists() throws Exception {
        // given
        String alias = "smp_test_change (test intermediate issuer 01)";
        TruststoreCertificateBO certificateBO = bdmslAdminService.getCertificateFromTruststore(alias);
        assertNotNull(certificateBO);
        // when
        BadRequestException exception = assertThrows(BadRequestException.class, () ->
                bdmslAdminService.addCertificateToTruststore(certificateBO.getCertificatePublicKey(), "New certificateAlias"));
        // then
        assertEquals("[ERR-106] The certificate already exists with alias [smp_test_change (test intermediate issuer 01)].", exception.getMessage());
    }

    @Test
    void testAddTrustedCertificateFailInvalidCertificate() throws Exception {
        // given
        String alias = "new smp_test_change";
        byte[] array = alias.getBytes();

        // when
        BadRequestException exception = assertThrows(BadRequestException.class, () ->
                bdmslAdminService.addCertificateToTruststore(array, alias));
        // then
        MatcherAssert.assertThat(exception.getMessage(),
                CoreMatchers.startsWithIgnoringCase("[ERR-106] The certificate is not valid - [Could not parse certificate: "));

    }

    @Test
    void testDeleteTrustedCertificate() throws Exception {
        // given
        String alias = "smp_test_change (test intermediate issuer 01)";
        List<String> aliases = bdmslAdminService.listTruststoreCertificateAliases(null);
        // when
        TruststoreCertificateBO certificateBO = bdmslAdminService.deleteCertificateFromTruststore(alias);
        // then
        List<String> aliasesNew = bdmslAdminService.listTruststoreCertificateAliases(null);
        assertNotNull(certificateBO);
        assertNotNull(certificateBO.getCertificatePublicKey());
        assertEquals(alias, certificateBO.getAlias());
        assertEquals(aliases.size() - 1, aliasesNew.size());
    }

    @Test
    void testDeleteTrustedCertificateFailInvalidAlias() throws Exception {
        // given
        String alias = "";
        // when
        BadRequestException exception = assertThrows(BadRequestException.class, () ->
                bdmslAdminService.deleteCertificateFromTruststore(alias));
        // then
        assertEquals(exception.getMessage(), "[ERR-106] Invalid alias [" + alias + "]!");
    }

    @Test
    void testDeleteTrustedCertificateFailCertForAliasNotExists() throws Exception {
        // given
        String alias = "aliasNotExists";
        // when
        NotFoundException exception = assertThrows(NotFoundException.class, () ->
                bdmslAdminService.deleteCertificateFromTruststore(alias));
        // then
        assertEquals(exception.getMessage(), "[ERR-118] The certificate with alias [" + alias + "] does not exist!");
    }
}
