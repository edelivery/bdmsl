/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.ws.soap;

import ec.services.wsdl.bdmsl.data._1.SMPAdvancedServiceForParticipantService;
import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.bo.ParticipantBO;
import eu.europa.ec.bdmsl.dao.IParticipantDAO;
import eu.europa.ec.bdmsl.test.DnsMessageSenderServiceMock;
import eu.europa.ec.bdmsl.security.ICertificateAuthentication;
import eu.europa.ec.bdmsl.security.UnsecureAuthentication;
import eu.europa.ec.bdmsl.service.dns.IDnsMessageSenderService;
import org.busdox.servicemetadata.locator._1.ServiceMetadataPublisherServiceForParticipantType;
import org.busdox.transport.identifiers._1.ParticipantIdentifierType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.security.Security;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Adrien FERIAL
 * @since 09/07/2015
 */
class BDMSLServiceWSCreateParticipantIdentifierTest extends AbstractJUnit5Test {

    @Autowired
    private IBDMSLServiceWS bdmslServiceWS;

    @Autowired
    private IParticipantDAO participantDAO;

    @Autowired
    private IDnsMessageSenderService dnsMessageSenderService;

    @Autowired
    private ICertificateAuthentication customAuthentication;

    @BeforeEach
    void before() throws Exception {
        UnsecureAuthentication authentication = new UnsecureAuthentication();
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    @Test
    void testCreateParticipantIdentifierEmpty() {
        SMPAdvancedServiceForParticipantService participantType = new SMPAdvancedServiceForParticipantService();
        assertThrows(BadRequestFault.class, () -> bdmslServiceWS.createParticipantIdentifier(participantType));
    }

    @Test
    void testCreateParticipantIdentifierNull() {
        assertThrows(BadRequestFault.class, () -> bdmslServiceWS.createParticipantIdentifier(null));
    }

    @Test
    void testCreateParticipantIdentifierWithoutExistingSMP() {
        SMPAdvancedServiceForParticipantService participantType = createParticipantMetadata();
        assertThrows(NotFoundFault.class, () -> bdmslServiceWS.createParticipantIdentifier(participantType));
    }

    @Test
    @Transactional
    void testCreateParticipanOMd5() throws Exception {
        final String partId = "0009:123456789786Base32";
        final String smpId = "foundUnsecure";
        final String scheme = "iso6523-actorid-upis";

        String messages = createParticipant(partId, smpId, scheme);

        assertThat(messages, containsString("B-a90c5371d6a8f5a6cde3eacae4ae7389.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu.\t0\tANY\tANY"));
        assertThat(messages, containsString("B-a90c5371d6a8f5a6cde3eacae4ae7389.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tCNAME\tfoundUnsecure.publisher.acc.edelivery.tech.ec.europa.eu."));
    }

    @Test
    @Transactional
    void testCreateParticipantOK() throws Exception {

        customAuthentication.blueCoatAuthentication("123456789101112");

        final String partId = "urn:ehealth:de:ncpb-idp";
        final String smpId = "SMP123456789101112";
        final String scheme = "ehealth-ncp-ids";

        String messages = createParticipant(partId, smpId, scheme);
        System.out.println(messages);
        assertThat(messages, containsString("XKKICGUA73FA67J7OK7QC43N7VQXEUGRSMRCRG4YUKWFTFNY3UVA.ehealth-ncp-ids.ehealth.acc.edelivery.tech.ec.europa.eu.\t0\tANY\tANY"));
        assertThat(messages, containsString("XKKICGUA73FA67J7OK7QC43N7VQXEUGRSMRCRG4YUKWFTFNY3UVA.ehealth-ncp-ids.ehealth.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tNAPTR\t100 10 \"U\" \"META:SMP\" \"!.*!https://logicalAddress.com.eu!\" ."));
    }

    @Test
    void testCreateParticipantForDifferentDomainCertificateNotOK() throws Exception {
        //GIVEN
        customAuthentication.blueCoatAuthentication();

        final String partId = "urn:ehealth:de:ncpb-idp";
        final String smpId = "SMP123456789101112";
        final String scheme = "ehealth-ncp-ids";
        SMPAdvancedServiceForParticipantService participantType = createParticipantMetadata();
        participantType.getCreateParticipantIdentifier().getParticipantIdentifier().setValue(partId);
        participantType.getCreateParticipantIdentifier().getParticipantIdentifier().setScheme(scheme);
        participantType.getCreateParticipantIdentifier().setServiceMetadataPublisherID(smpId);
        // First we ensure the participant doesn't exist
        ParticipantBO partBO = new ParticipantBO();
        partBO.setSmpId(smpId);
        partBO.setScheme(scheme);
        partBO.setParticipantId(partId);
        ParticipantBO found = participantDAO.findParticipant(partBO);
        assertNull(found);

        //WHEN THEN
        assertThrows(UnauthorizedFault.class, () -> bdmslServiceWS.createParticipantIdentifier(participantType));
    }

    @Test
    @Transactional
    void testCreateParticipantWithAllDnsREcords() throws Exception {
        customAuthentication.blueCoatAuthentication("22591", "CN=SMP_TEST_FOR_EDELIVERY_22591,O=DG-DIGIT,C=BE", "CN=SMP_TEST_FOR_EDELIVERY_22591,O=DG-DIGIT,C=BE", true);

        final String partId = "0009:EDELIVERY-24091";
        final String smpId = "SMPTESTFOREDELIVERY22591";
        final String scheme = "iso6523-actorid-upis";

        String messages = createParticipant(partId, smpId, scheme);

        assertThat(messages, containsString("B-abe58f04d34e59cc6c3b130ca520ae0e.iso6523-actorid-upis.22591.acc.edelivery.tech.ec.europa.eu.\t0\tANY\tANY"));
        assertThat(messages, containsString("B-abe58f04d34e59cc6c3b130ca520ae0e.iso6523-actorid-upis.22591.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tCNAME\tSMPTESTFOREDELIVERY22591.publisher.22591.acc.edelivery.tech.ec.europa.eu."));
        assertThat(messages, containsString("ITXKXIPDIDBT743GRXQGKICVZPITF5C423JJYX2FWABPSNT7OTBA.iso6523-actorid-upis.22591.acc.edelivery.tech.ec.europa.eu.	0	ANY	ANY"));
        assertThat(messages, containsString("ITXKXIPDIDBT743GRXQGKICVZPITF5C423JJYX2FWABPSNT7OTBA.iso6523-actorid-upis.22591.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tNAPTR\t100 10 \"U\" \"META:SMP\" \"!.*!http://logicalAddress!\" ."));
    }

    @Test
    @Transactional
    void testCreateParticipantWithCNAMEOnly() throws Exception {
        customAuthentication.blueCoatAuthentication("22592", "CN=SMP_TEST_FOR_EDELIVERY_22592,O=DG-DIGIT,C=BE", "CN=SMP_TEST_FOR_EDELIVERY_22592,O=DG-DIGIT,C=BE", true);

        final String partId = "0009:EDELIVERY-24092";
        final String smpId = "SMPTESTFOREDELIVERY22592";
        final String scheme = "iso6523-actorid-upis";

        String messages = createParticipant(partId, smpId, scheme);

        assertFalse(messages.contains("NAPTR"));
        assertThat(messages, containsString("B-ad6469cdf052fe8a2e0b2a1e9c1373aa.iso6523-actorid-upis.22592.acc.edelivery.tech.ec.europa.eu.\t0\tANY\tANY"));
        assertThat(messages, containsString("B-ad6469cdf052fe8a2e0b2a1e9c1373aa.iso6523-actorid-upis.22592.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tCNAME\tSMPTESTFOREDELIVERY22592.publisher.22592.acc.edelivery.tech.ec.europa.eu."));
    }

    @Test
    @Transactional
    void testCreateParticipantWithNAPTROnly() throws Exception {
        customAuthentication.blueCoatAuthentication("22593", "CN=SMP_TEST_FOR_EDELIVERY_22593,O=DG-DIGIT,C=BE", "CN=SMP_TEST_FOR_EDELIVERY_22593,O=DG-DIGIT,C=BE", true);

        final String partId = "0009:EDELIVERY-24093";
        final String smpId = "SMPTESTFOREDELIVERY22593";
        final String scheme = "iso6523-actorid-upis";

        String messages = createParticipant(partId, smpId, scheme);

        assertFalse(messages.contains("CNAME"));
        assertThat(messages, containsString("5WIV2CKKT65IJ2H6QMCZBVDWLF5EJACKFUGP4D26W44TREB3RXUQ.iso6523-actorid-upis.22593.acc.edelivery.tech.ec.europa.eu.\t0\tANY\tANY"));
        assertThat(messages, containsString("5WIV2CKKT65IJ2H6QMCZBVDWLF5EJACKFUGP4D26W44TREB3RXUQ.iso6523-actorid-upis.22593.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tNAPTR\t100 10 \"U\" \"META:SMP\" \"!.*!http://logicalAddress!\" ."));
    }

    @Test
    @Transactional
    void testCreateParticipantWithWrongDNSTypeParamenter() throws Exception {

        customAuthentication.blueCoatAuthentication("22594", "CN=SMP_TEST_FOR_EDELIVERY_22594,O=DG-DIGIT,C=BE", "CN=SMP_TEST_FOR_EDELIVERY_22594,O=DG-DIGIT,C=BE", true);

        final String partId = "0009:EDELIVERY-24094";
        final String smpId = "SMPTESTFOREDELIVERY22594";
        final String scheme = "iso6523-actorid-upis";
        InternalErrorFault result = assertThrows(InternalErrorFault.class, () -> createParticipant(partId, smpId, scheme));
        assertThat(result.getMessage(), containsString("Subdomain [22594.acc.edelivery.tech.ec.europa.eu] has illegal dns type: [wrong parameter]."));
    }


    @Test
    @Transactional
    void testCreateParticipantWithEmptyParameter() throws Exception {

        customAuthentication.blueCoatAuthentication("22595", "CN=SMP_TEST_FOR_EDELIVERY_22595,O=DG-DIGIT,C=BE", "CN=SMP_TEST_FOR_EDELIVERY_22595,O=DG-DIGIT,C=BE", true);

        final String partId = "0009:EDELIVERY-24095";
        final String smpId = "SMPTESTFOREDELIVERY22595";
        final String scheme = "iso6523-actorid-upis";

        InternalErrorFault result = assertThrows(InternalErrorFault.class, () -> createParticipant(partId, smpId, scheme));
        assertThat(result.getMessage(), containsString("Record types for subdomain 22595.acc.edelivery.tech.ec.europa.eu by name must not be empty."));
    }

    private SMPAdvancedServiceForParticipantService createParticipantMetadata() {
        ServiceMetadataPublisherServiceForParticipantType participantType = new ServiceMetadataPublisherServiceForParticipantType();
        ParticipantIdentifierType partIdType = new ParticipantIdentifierType();
        partIdType.setScheme("iso6523-actorid-upis");
        partIdType.setValue("0088:123456789");
        participantType.setParticipantIdentifier(partIdType);
        participantType.setServiceMetadataPublisherID("NotFound");
        SMPAdvancedServiceForParticipantService advancedParticipantType = new SMPAdvancedServiceForParticipantService();
        advancedParticipantType.setCreateParticipantIdentifier(participantType);
        advancedParticipantType.setServiceName("META:SMP_TEST");
        return advancedParticipantType;
    }

    private String createParticipant(String partId, String smpId, String scheme) throws Exception {
        SMPAdvancedServiceForParticipantService participantType = createParticipantMetadata();
        participantType.getCreateParticipantIdentifier().getParticipantIdentifier().setValue(partId);
        participantType.getCreateParticipantIdentifier().setServiceMetadataPublisherID(smpId);
        participantType.getCreateParticipantIdentifier().getParticipantIdentifier().setScheme(scheme);
        participantType.setServiceName("META:SMP");

        // First we ensure the participant doesn't exist
        ParticipantBO partBO = new ParticipantBO();
        partBO.setSmpId(smpId);
        partBO.setScheme(scheme);
        partBO.setParticipantId(partId);
        ParticipantBO found = participantDAO.findParticipant(partBO);
        assertNull(found);

        DnsMessageSenderServiceMock dnsMessageSenderService = (DnsMessageSenderServiceMock) this.dnsMessageSenderService;
        assertTrue(dnsMessageSenderService.getMessages().isEmpty());

        bdmslServiceWS.createParticipantIdentifier(participantType);

        // Finally we verify that the participant has been created
        found = participantDAO.findParticipant(partBO);
        assertNotNull(found);
        participantDAO.deleteParticipant(found);

        return dnsMessageSenderService.getMessages();
    }
}
