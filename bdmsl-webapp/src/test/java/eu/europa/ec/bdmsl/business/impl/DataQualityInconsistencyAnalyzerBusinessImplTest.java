/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.business.impl;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.business.IDataQualityInconsistencyAnalyzerBusiness;
import eu.europa.ec.bdmsl.common.bo.ServiceMetadataPublisherBO;
import eu.europa.ec.bdmsl.common.enums.DNSSubSomainRecordTypeEnum;
import eu.europa.ec.bdmsl.common.exception.InconsistencyReportException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.dao.ISmpDAO;
import eu.europa.ec.bdmsl.dao.entity.reports.InconsistencyDbCNameEntity;
import eu.europa.ec.bdmsl.dao.entity.reports.InconsistencyDbNaptrEntity;
import eu.europa.ec.bdmsl.test.TestConstants;
import eu.europa.ec.bdmsl.service.dns.DataInconsistencyAnalyzer;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;
import org.xbill.DNS.IRDnsRangeData;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.spy;

/**
 * @author Flavio SANTOS
 */
class DataQualityInconsistencyAnalyzerBusinessImplTest extends AbstractJUnit5Test {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private ISmpDAO smpDAO;

    @Autowired
    private IDataQualityInconsistencyAnalyzerBusiness dataQualityInconsistencyAnalyzerBusiness;

    DataInconsistencyAnalyzer dataInconsistencyAnalyzer = new DataInconsistencyAnalyzer();

    @BeforeEach
    protected void initTest() throws Exception {
        ReflectionTestUtils.setField(dataQualityInconsistencyAnalyzerBusiness, "dataInconsistencyAnalyzer", dataInconsistencyAnalyzer);
        dnsClientService = Mockito.spy(dnsClientService);
        ReflectionTestUtils.setField(dataQualityInconsistencyAnalyzerBusiness, "dnsClientService", dnsClientService);
    }


    @Test
    void testCheckInconsistenciesEmptyDNS() throws Exception {
        DataInconsistencyAnalyzer dataInconsistencyAnalyzer
                = (DataInconsistencyAnalyzer) ReflectionTestUtils.getField(dataQualityInconsistencyAnalyzerBusiness, "dataInconsistencyAnalyzer");

        IDataQualityInconsistencyAnalyzerBusiness qualityInconsistencyAnalyzerBusinessMock = spy(dataQualityInconsistencyAnalyzerBusiness);
        // not return any DNS
        Mockito.doReturn(0).when(dnsClientService).getAllDNSDataForInconsistencyReport(any(), any(), any(), any());
        qualityInconsistencyAnalyzerBusinessMock.checkDataInconsistencies();

        assertEquals(TestConstants.DB_PARTICIPANT_COUNT, dataInconsistencyAnalyzer.getDbTotalOfParticipants());
        assertEquals(TestConstants.DB_SMP_COUNT, dataInconsistencyAnalyzer.getDbTotalOfSMPs());
        assertEquals(73, dataInconsistencyAnalyzer.getDataInconsistencyEntries().size());
        assertEquals(29, dataInconsistencyAnalyzer.getDbTotalOfParticipantsCNAME());
        assertEquals(11, dataInconsistencyAnalyzer.getDbTotalOfParticipantsNAPTR());
        assertEquals(0, dataInconsistencyAnalyzer.getDnsTotalOfParticipantsCNAME());
        assertEquals(0, dataInconsistencyAnalyzer.getDnsTotalOfParticipantsNAPTR());
        assertEquals(0, dataInconsistencyAnalyzer.getDnsTotalOfSMPs());
    }

    @Test
    void testCheckInconsistenciesWithError() throws Exception {
        DataInconsistencyAnalyzer dataInconsistencyAnalyzer
                = (DataInconsistencyAnalyzer) ReflectionTestUtils.getField(dataQualityInconsistencyAnalyzerBusiness, "dataInconsistencyAnalyzer");

        DataQualityInconsistencyAnalyzerBusinessImpl qualityInconsistencyAnalyzerBusinessMock = spy((DataQualityInconsistencyAnalyzerBusinessImpl) dataQualityInconsistencyAnalyzerBusiness);
        // not return any DNS
        InconsistencyReportException exception = new InconsistencyReportException("Inconsistency Report Exception");
        Mockito.doThrow(exception).when(qualityInconsistencyAnalyzerBusinessMock).analyzeCNameRecords(any(), any(), any());

        Mockito.doReturn(0).when(dnsClientService).getAllDNSDataForInconsistencyReport(any(), any(), any(), any());
        qualityInconsistencyAnalyzerBusinessMock.checkDataInconsistencies();

        assertEquals(1, dataInconsistencyAnalyzer.getDataInconsistencyEntries().size());
        assertEquals(DataInconsistencyAnalyzer.DataInconsistencyEntry.IssueType.ERROR, dataInconsistencyAnalyzer.getDataInconsistencyEntries().get(0).getMissingType());
        assertEquals("Error occurred while analysing data for inconsistencies: Inconsistency Report Exception", dataInconsistencyAnalyzer.getDataInconsistencyEntries().get(0).getErrorMessage());
    }


    @Test
    void analyzeCNameRecordsOk() throws Exception {
        // given
        String publisherString = ".publisher.";
        IRDnsRangeData irDnsRangeData = new IRDnsRangeData(1);

        List<String> inconsistency = getAllCNameDataAsDNSFromDatabase(publisherString, irDnsRangeData, 0);
        assertEquals(0, inconsistency.size());
        DataInconsistencyAnalyzer dataInconsistencyAnalyzer
                = (DataInconsistencyAnalyzer) ReflectionTestUtils.getField(dataQualityInconsistencyAnalyzerBusiness, "dataInconsistencyAnalyzer");
        dataInconsistencyAnalyzer.clear();
        // when
        ((DataQualityInconsistencyAnalyzerBusinessImpl) dataQualityInconsistencyAnalyzerBusiness).analyzeCNameRecords(publisherString,
                irDnsRangeData,
                Calendar.getInstance().getTime());


        // then
        assertEquals(irDnsRangeData.getAddedRowsDNS(), dataInconsistencyAnalyzer.getDbTotalOfParticipantsCNAME());
        assertNotEquals(0, dataInconsistencyAnalyzer.getDbTotalOfParticipantsCNAME());
        // there should be no inconsistencies
        assertEquals(0, dataInconsistencyAnalyzer.getParticipantCNAMEDataInconsistencies().size());
    }

    @Test
    void analyzeNaptrRecordsOk() {
        IRDnsRangeData irDnsRangeData = new IRDnsRangeData(1);

        List<String> inconsistency = getAllNaptrDataAsDNSFromDatabase(irDnsRangeData, 0);
        assertEquals(0, inconsistency.size());
        DataInconsistencyAnalyzer dataInconsistencyAnalyzer
                = (DataInconsistencyAnalyzer) ReflectionTestUtils.getField(dataQualityInconsistencyAnalyzerBusiness, "dataInconsistencyAnalyzer");
        dataInconsistencyAnalyzer.clear();
        // when
        ((DataQualityInconsistencyAnalyzerBusinessImpl) dataQualityInconsistencyAnalyzerBusiness).analyzeNaptrRecords(
                irDnsRangeData,
                Calendar.getInstance().getTime());
        // then
        assertEquals(irDnsRangeData.getAddedRowsDNS(), dataInconsistencyAnalyzer.getDbTotalOfParticipantsNAPTR());
        assertNotEquals(0, dataInconsistencyAnalyzer.getDbTotalOfParticipantsNAPTR());
        // there should be no inconsistencies
        assertEquals(0, dataInconsistencyAnalyzer.getParticipantNAPTRDataInconsistencies().size());
    }

    @Test
    void analyzeSMPRecordsOk() throws TechnicalException {
        IRDnsRangeData irDnsRangeData = new IRDnsRangeData(1);

        List<String> inconsistency = getAllSMPDataAsDNSFromDatabase(irDnsRangeData, 0);
        assertEquals(0, inconsistency.size());
        DataInconsistencyAnalyzer dataInconsistencyAnalyzer
                = (DataInconsistencyAnalyzer) ReflectionTestUtils.getField(dataQualityInconsistencyAnalyzerBusiness, "dataInconsistencyAnalyzer");
        dataInconsistencyAnalyzer.clear();
        // when
        ((DataQualityInconsistencyAnalyzerBusinessImpl) dataQualityInconsistencyAnalyzerBusiness).analyzeSMPRecords(
                irDnsRangeData);
        // then
        assertEquals(irDnsRangeData.getAddedRowsDNS(), dataInconsistencyAnalyzer.getDbTotalOfSMPs());
        assertNotEquals(0, dataInconsistencyAnalyzer.getDbTotalOfSMPs());
        // there should be no inconsistencies
        assertEquals(0, dataInconsistencyAnalyzer.getSmpsDataInconsistencies().size());
    }

    @Test
    void analyzeCNameRecordsMissingInDNS() throws Exception {
        // given
        String publisherString = ".publisher.";
        IRDnsRangeData irDnsRangeData = new IRDnsRangeData(1);
        int missingCountInDNS = 5;

        List<String> inconsistency = getAllCNameDataAsDNSFromDatabase(publisherString, irDnsRangeData, missingCountInDNS);
        assertEquals(missingCountInDNS, inconsistency.size());
        DataInconsistencyAnalyzer dataInconsistencyAnalyzer
                = (DataInconsistencyAnalyzer) ReflectionTestUtils.getField(dataQualityInconsistencyAnalyzerBusiness, "dataInconsistencyAnalyzer");
        dataInconsistencyAnalyzer.clear();
        // when
        ((DataQualityInconsistencyAnalyzerBusinessImpl) dataQualityInconsistencyAnalyzerBusiness).analyzeCNameRecords(publisherString,
                irDnsRangeData,
                Calendar.getInstance().getTime());

        // then
        assertEquals(irDnsRangeData.getAddedRowsDNS() + missingCountInDNS, dataInconsistencyAnalyzer.getDbTotalOfParticipantsCNAME());
        assertNotEquals(0, dataInconsistencyAnalyzer.getDbTotalOfParticipantsCNAME());
        // there should be no inconsistencies
        assertEquals(missingCountInDNS, dataInconsistencyAnalyzer.getParticipantCNAMEDataInconsistencies().size());

        for (DataInconsistencyAnalyzer.DataInconsistencyEntry entry : dataInconsistencyAnalyzer.getParticipantCNAMEDataInconsistencies()) {
            assertEquals(DataInconsistencyAnalyzer.DataInconsistencyEntry.IssueType.CNAME, entry.getMissingType());
            assertEquals(DataInconsistencyAnalyzer.DataInconsistencyEntry.MissingSourceType.DNS, entry.getMissingSourceType());
            assertEquals(DataInconsistencyAnalyzer.DataInconsistencyEntry.DataType.PARTICIPANT, entry.getDataType());
            assertTrue(inconsistency.contains(entry.getDomainName() + "|" + entry.getTarget()));
        }
    }

    @Test
    void analyzeCNameRecordsMissingInDB() throws Exception {
        // given
        String publisherString = ".publisher.";
        IRDnsRangeData irDnsRangeData = new IRDnsRangeData(1);
        int missingCountInDB = 7;
        getAllCNameDataAsDNSFromDatabase(publisherString, irDnsRangeData, 0);
        List<String> inconsistency = new ArrayList<>();
        // add additional records
        for (int i = 0; i < missingCountInDB; i++) {
            String record = "b-" + UUID.randomUUID() + ".eu|http://test.eu";
            irDnsRangeData.addDNSEntry(record);
            inconsistency.add(record);
        }

        DataInconsistencyAnalyzer dataInconsistencyAnalyzer
                = (DataInconsistencyAnalyzer) ReflectionTestUtils.getField(dataQualityInconsistencyAnalyzerBusiness, "dataInconsistencyAnalyzer");
        dataInconsistencyAnalyzer.clear();
        // when
        ((DataQualityInconsistencyAnalyzerBusinessImpl) dataQualityInconsistencyAnalyzerBusiness).analyzeCNameRecords(publisherString,
                irDnsRangeData,
                Calendar.getInstance().getTime());

        // then
        assertEquals(irDnsRangeData.getAddedRowsDNS() - missingCountInDB, dataInconsistencyAnalyzer.getDbTotalOfParticipantsCNAME());
        assertNotEquals(0, dataInconsistencyAnalyzer.getDbTotalOfParticipantsCNAME());
        // there should be no inconsistencies
        assertEquals(missingCountInDB, dataInconsistencyAnalyzer.getParticipantCNAMEDataInconsistencies().size());

        for (DataInconsistencyAnalyzer.DataInconsistencyEntry entry : dataInconsistencyAnalyzer.getParticipantCNAMEDataInconsistencies()) {
            assertEquals(DataInconsistencyAnalyzer.DataInconsistencyEntry.IssueType.CNAME, entry.getMissingType());
            assertEquals(DataInconsistencyAnalyzer.DataInconsistencyEntry.MissingSourceType.DATABASE, entry.getMissingSourceType());
            assertEquals(DataInconsistencyAnalyzer.DataInconsistencyEntry.DataType.PARTICIPANT, entry.getDataType());
            assertTrue(inconsistency.contains(entry.getDomainName() + "|" + entry.getTarget()));

        }
    }

    @Test
    void analyzeNaptrRecordsMissingInDNS() {
        // given

        IRDnsRangeData irDnsRangeData = new IRDnsRangeData(1);
        int missingCountInDNS = 3;

        List<String> inconsistency = getAllNaptrDataAsDNSFromDatabase(irDnsRangeData, missingCountInDNS);
        assertEquals(missingCountInDNS, inconsistency.size());
        DataInconsistencyAnalyzer dataInconsistencyAnalyzer
                = (DataInconsistencyAnalyzer) ReflectionTestUtils.getField(dataQualityInconsistencyAnalyzerBusiness, "dataInconsistencyAnalyzer");
        dataInconsistencyAnalyzer.clear();
        // when
        ((DataQualityInconsistencyAnalyzerBusinessImpl) dataQualityInconsistencyAnalyzerBusiness).analyzeNaptrRecords(
                irDnsRangeData,
                Calendar.getInstance().getTime());

        // then
        assertEquals(irDnsRangeData.getAddedRowsDNS() + missingCountInDNS, dataInconsistencyAnalyzer.getDbTotalOfParticipantsNAPTR());
        assertNotEquals(0, dataInconsistencyAnalyzer.getDbTotalOfParticipantsNAPTR());
        // there should be no inconsistencies
        assertEquals(missingCountInDNS, dataInconsistencyAnalyzer.getParticipantNAPTRDataInconsistencies().size());

        for (DataInconsistencyAnalyzer.DataInconsistencyEntry entry : dataInconsistencyAnalyzer.getParticipantCNAMEDataInconsistencies()) {
            assertEquals(DataInconsistencyAnalyzer.DataInconsistencyEntry.IssueType.NAPTR, entry.getMissingType());
            assertEquals(DataInconsistencyAnalyzer.DataInconsistencyEntry.MissingSourceType.DNS, entry.getMissingSourceType());
            assertEquals(DataInconsistencyAnalyzer.DataInconsistencyEntry.DataType.PARTICIPANT, entry.getDataType());
            assertTrue(inconsistency.contains(entry.getDomainName() + "|" + entry.getTarget()));

        }
    }

    @Test
    void analyzeNaptrRecordsMissingInDB() throws Exception {
        // given
        IRDnsRangeData irDnsRangeData = new IRDnsRangeData(1);
        int missingCountInDB = 7;
        getAllNaptrDataAsDNSFromDatabase(irDnsRangeData, 0);
        List<String> inconsistency = new ArrayList<>();
        // add additional records
        for (int i = 0; i < missingCountInDB; i++) {
            String record = UUID.randomUUID() + ".eu|http://test.eu";
            irDnsRangeData.addDNSEntry(record);
            inconsistency.add(record);
        }

        DataInconsistencyAnalyzer dataInconsistencyAnalyzer
                = (DataInconsistencyAnalyzer) ReflectionTestUtils.getField(dataQualityInconsistencyAnalyzerBusiness, "dataInconsistencyAnalyzer");
        dataInconsistencyAnalyzer.clear();
        // when
        ((DataQualityInconsistencyAnalyzerBusinessImpl) dataQualityInconsistencyAnalyzerBusiness).analyzeNaptrRecords(
                irDnsRangeData,
                Calendar.getInstance().getTime());

        // then
        assertEquals(irDnsRangeData.getAddedRowsDNS() - missingCountInDB, dataInconsistencyAnalyzer.getDbTotalOfParticipantsNAPTR());
        assertNotEquals(0, dataInconsistencyAnalyzer.getDbTotalOfParticipantsNAPTR());
        // there should be no inconsistencies
        assertEquals(missingCountInDB, dataInconsistencyAnalyzer.getParticipantNAPTRDataInconsistencies().size());

        for (DataInconsistencyAnalyzer.DataInconsistencyEntry entry : dataInconsistencyAnalyzer.getParticipantNAPTRDataInconsistencies()) {
            assertEquals(DataInconsistencyAnalyzer.DataInconsistencyEntry.IssueType.NAPTR, entry.getMissingType());
            assertEquals(DataInconsistencyAnalyzer.DataInconsistencyEntry.MissingSourceType.DATABASE, entry.getMissingSourceType());
            assertEquals(DataInconsistencyAnalyzer.DataInconsistencyEntry.DataType.PARTICIPANT, entry.getDataType());
            assertTrue(inconsistency.contains(entry.getDomainName() + "|" + entry.getTarget()));

        }
    }

    @Test
    void analyzeSMPRecordsMissingInDNS() throws Exception {
        // given

        IRDnsRangeData irDnsRangeData = new IRDnsRangeData(1);
        int missingCountInDNS = 3;

        List<String> inconsistency = getAllSMPDataAsDNSFromDatabase(irDnsRangeData, missingCountInDNS);
        assertEquals(missingCountInDNS, inconsistency.size());
        DataInconsistencyAnalyzer dataInconsistencyAnalyzer
                = (DataInconsistencyAnalyzer) ReflectionTestUtils.getField(dataQualityInconsistencyAnalyzerBusiness, "dataInconsistencyAnalyzer");
        dataInconsistencyAnalyzer.clear();
        // when
        ((DataQualityInconsistencyAnalyzerBusinessImpl) dataQualityInconsistencyAnalyzerBusiness).analyzeSMPRecords(
                irDnsRangeData);

        // then
        assertEquals(irDnsRangeData.getAddedRowsDNS() + missingCountInDNS, dataInconsistencyAnalyzer.getDbTotalOfSMPs());
        assertNotEquals(0, dataInconsistencyAnalyzer.getDbTotalOfSMPs());
        // there should be no inconsistencies
        assertEquals(missingCountInDNS, dataInconsistencyAnalyzer.getSmpsDataInconsistencies().size());

        for (DataInconsistencyAnalyzer.DataInconsistencyEntry entry : dataInconsistencyAnalyzer.getSmpsDataInconsistencies()) {
            assertEquals(DataInconsistencyAnalyzer.DataInconsistencyEntry.IssueType.CNAME, entry.getMissingType());
            assertEquals(DataInconsistencyAnalyzer.DataInconsistencyEntry.MissingSourceType.DNS, entry.getMissingSourceType());
            assertEquals(DataInconsistencyAnalyzer.DataInconsistencyEntry.DataType.SMP, entry.getDataType());
            assertTrue(inconsistency.contains(entry.getDomainName() + "|" + entry.getTarget()));

        }
    }


    @Test
    void analyzeSMPRecordsMissingInDB() throws Exception {
        // given
        IRDnsRangeData irDnsRangeData = new IRDnsRangeData(1);
        int missingCountInDB = 7;
        getAllSMPDataAsDNSFromDatabase(irDnsRangeData, 0);
        List<String> inconsistency = new ArrayList<>();
        // add additional records
        for (int i = 0; i < missingCountInDB; i++) {
            String record = UUID.randomUUID() + ".eu|http://test.eu";
            irDnsRangeData.addDNSEntry(record);
            inconsistency.add(record);
        }

        DataInconsistencyAnalyzer dataInconsistencyAnalyzer
                = (DataInconsistencyAnalyzer) ReflectionTestUtils.getField(dataQualityInconsistencyAnalyzerBusiness, "dataInconsistencyAnalyzer");
        dataInconsistencyAnalyzer.clear();
        // when
        ((DataQualityInconsistencyAnalyzerBusinessImpl) dataQualityInconsistencyAnalyzerBusiness).analyzeSMPRecords(
                irDnsRangeData);

        // then
        assertEquals(irDnsRangeData.getAddedRowsDNS() - missingCountInDB, dataInconsistencyAnalyzer.getDbTotalOfSMPs());
        assertNotEquals(0, dataInconsistencyAnalyzer.getDbTotalOfSMPs());
        // there should be no inconsistencies
        assertEquals(missingCountInDB, dataInconsistencyAnalyzer.getSmpsDataInconsistencies().size());

        for (DataInconsistencyAnalyzer.DataInconsistencyEntry entry : dataInconsistencyAnalyzer.getSmpsDataInconsistencies()) {
            assertEquals(DataInconsistencyAnalyzer.DataInconsistencyEntry.IssueType.CNAME, entry.getMissingType());
            assertEquals(DataInconsistencyAnalyzer.DataInconsistencyEntry.MissingSourceType.DATABASE, entry.getMissingSourceType());
            assertEquals(DataInconsistencyAnalyzer.DataInconsistencyEntry.DataType.SMP, entry.getDataType());
            assertTrue(inconsistency.contains(entry.getDomainName() + "|" + entry.getTarget()));
        }
    }

    List<String> getAllCNameDataAsDNSFromDatabase(String publisherString, IRDnsRangeData irDnsRangeData, int iRemoveFromDNs) {
        List<String> lstInconsistencies = new ArrayList<>();
        TypedQuery<InconsistencyDbCNameEntity> query = entityManager.createNamedQuery("InconsistencyDbCNameEntity.getAllDNSCNameRecords",
                InconsistencyDbCNameEntity.class);
        query.setParameter("dnsAllType", DNSSubSomainRecordTypeEnum.ALL.getCode());
        query.setParameter("dnsCnameType", DNSSubSomainRecordTypeEnum.CNAME.getCode());
        query.setParameter("createDate", Calendar.getInstance().getTime());

        int iRemoved = iRemoveFromDNs;

        List<InconsistencyDbCNameEntity> result = query.getResultList();
        for (InconsistencyDbCNameEntity idbCname : result) {
            String subDomain = idbCname.getSubdomain().toLowerCase();
            String target = idbCname.getSmpId().toLowerCase().concat(publisherString).concat(subDomain);

            String record = idbCname.getDnsRecordName().concat("|").concat(target);
            if (iRemoved-- > 0) {
                lstInconsistencies.add(record);
            } else {
                irDnsRangeData.addDNSEntry(record);
            }
        }
        return lstInconsistencies;

    }

    List<String> getAllSMPDataAsDNSFromDatabase(IRDnsRangeData irDnsRangeData, int iRemoveFromDNs) throws TechnicalException {
        List<String> lstInconsistencies = new ArrayList<>();
        List<ServiceMetadataPublisherBO> result = smpDAO.listSMPs();
        int iRemoved = iRemoveFromDNs;

        for (ServiceMetadataPublisherBO idb : result) {
            if (idb.isDisabled()) {
                continue;
            }
            String host;
            // smp record points to host of logical address!
            try {
                host = new URL(idb.getLogicalAddress()).getHost();
            } catch (MalformedURLException e) {
                throw new InconsistencyReportException("Logical address " + idb.getLogicalAddress() + " of SMP: " + idb.getSmpId() + " is malformed: " + e.getMessage(), e);
            }
            // domain name and hosts(cname or A target) are case insensitive Put it to lowercase for comparing
            // same is done when receiving dns records.
            String record = StringUtils.lowerCase(idb.getDnsRecord()) + "|" + StringUtils.lowerCase(host);
            if (iRemoved-- > 0) {
                lstInconsistencies.add(record);
            } else {
                irDnsRangeData.addDNSEntry(record);
            }
        }
        return lstInconsistencies;
    }

    List<String> getAllNaptrDataAsDNSFromDatabase(IRDnsRangeData irDnsRangeData, int iRemoveFromDNs) {
        List<String> lstInconsistencies = new ArrayList<>();
        TypedQuery<InconsistencyDbNaptrEntity> query = entityManager.createNamedQuery("InconsistencyDbNaptrEntity.getAllDNSNaptrRecords",
                InconsistencyDbNaptrEntity.class);
        query.setParameter("dnsAllType", DNSSubSomainRecordTypeEnum.ALL.getCode());
        query.setParameter("dnsNaptrType", DNSSubSomainRecordTypeEnum.NAPTR.getCode());
        query.setParameter("createDate", Calendar.getInstance().getTime());

        int iRemoved = iRemoveFromDNs;

        List<InconsistencyDbNaptrEntity> result = query.getResultList();
        for (InconsistencyDbNaptrEntity idb : result) {
            if (idb.isDisabled()) {
                continue;
            }
            String record = idb.getDnsRecordName().concat("|").concat(idb.getLogicalAddress());

            if (iRemoved-- > 0) {
                lstInconsistencies.add(record);
            } else {
                irDnsRangeData.addDNSEntry(record);
            }
        }
        return lstInconsistencies;
    }
}

