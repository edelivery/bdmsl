/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.exception.*;
import eu.europa.ec.bdmsl.test.CommonTestUtils;
import eu.europa.ec.bdmsl.util.CertificateUtils;
import eu.europa.ec.bdmsl.util.HttpConnectionMock;
import eu.europa.ec.edelivery.security.cert.IRevocationValidator;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.*;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.FileInputStream;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;

import static eu.europa.ec.edelivery.security.cert.impl.CertificateRVStrategyEnum.CRL_ONLY;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;


/**
 * X509 Certificate Service Implementation Test
 *
 * @author Adrien FERIAL
 * @since 22/06/2015
 */
class X509CertificateServiceImplTest extends AbstractJUnit5Test {

    @Autowired
    private IX509CertificateService x509CertificateService;

    private final String keystorePath = Thread.currentThread().getContextClassLoader().getResource("keystore.jks").getFile();
    private final String keystorePassword = "test";

    private X509Certificate certificate;

    @BeforeAll
    static void beforeClass() {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    @BeforeEach
    protected void testSetup() {

        IRevocationValidator crlVerifierService = (IRevocationValidator) ReflectionTestUtils.getField(x509CertificateService, "crlVerifierService");
        // set configuration for mocking different settings.
        ReflectionTestUtils.setField(crlVerifierService, "configurationBusiness", configurationBusiness);
    }

    /**
     * Valid certificate
     *
     * @throws TechnicalException Technical exception
     */
    @Test
    void testIsX509ClientCertificateValid() throws Exception {
        KeyStore ks = KeyStore.getInstance("JKS");
        ks.load(new FileInputStream(keystorePath), keystorePassword.toCharArray());
        X509Certificate cert = (X509Certificate) ks.getCertificate("smp_sendercn");
        x509CertificateService.validateClientX509Certificate(new X509Certificate[]{cert});
    }

    X509Certificate createCertificate(String issuerName, String subject, Date startDate, Date expiryDate, boolean hasDistribuitionList) throws Exception {
        KeyStore ks = KeyStore.getInstance("JKS");
        ks.load(Files.newInputStream(Paths.get(keystorePath)), keystorePassword.toCharArray());
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(1024);
        KeyPair key = keyGen.generateKeyPair();
        String serialNumber = "0400000000011E44A5E404";
        X509v3CertificateBuilder certBuilder = new X509v3CertificateBuilder(new X500Name(issuerName), new BigInteger(serialNumber, 16), startDate, expiryDate, new X500Name(subject), SubjectPublicKeyInfo.getInstance(key.getPublic().getEncoded()));

        if (hasDistribuitionList) {
            DistributionPointName distPointOne = new DistributionPointName(new GeneralNames(
                    new GeneralName(GeneralName.uniformResourceIdentifier, Thread.currentThread().getContextClassLoader().getResource("test.crl").getFile())));
            DistributionPoint[] distPoints = new DistributionPoint[]{new DistributionPoint(distPointOne, null, null)};
            certBuilder.addExtension(Extension.cRLDistributionPoints, false, new CRLDistPoint(distPoints));
        }

        ContentSigner sigGen = new JcaContentSignerBuilder("SHA1WithRSAEncryption").setProvider("BC").build(key.getPrivate());
        return new JcaX509CertificateConverter().setProvider("BC").getCertificate(certBuilder.build(sigGen));
    }

    @Test
    void testIsX509ClientCertificateExpired() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        Date notAfter = calendar.getTime();

        calendar.add(Calendar.YEAR, -2);
        Date notBefore = calendar.getTime();

        certificate = CommonTestUtils.createCertificate("C=BE, O=GlobalSign nv-sa, OU=Root CA, CN=GlobalSign Root CA",
                "CN=SMP_RevokedSubject,O=European Commission,C=PT", notBefore, notAfter,
                false);

        assertThrows(CertificateExpiredException.class,
                () -> x509CertificateService.validateClientX509Certificate(new X509Certificate[]{certificate}));
    }

    @Test
    void testIsX509ClientCertificateRevokedException() throws Exception {

        HttpConnectionMock httpConnectionMock = new HttpConnectionMock("test.crl");

        x509CertificateService.getCrlVerifierService().setHttpConnection(httpConnectionMock);
        certificate = CommonTestUtils.createCertificate("C=BE, O=GlobalSign nv-sa, OU=Root CA, CN=GlobalSign Root CA", "CN=RevokedSubject,O=European Commission,C=PT",
                true);

        CertificateNotFoundException result = assertThrows(CertificateNotFoundException.class,
                () -> x509CertificateService.validateClientX509Certificate(new X509Certificate[]{certificate})
        );

        assertTrue(result.getMessage().startsWith("[ERR-115] Unknown or not trusted certificate CN=RevokedSubject,O=European Commission,C=PT"));
    }

    @Test
    void testIsX509ClientCertificateExpiredException() throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        Calendar date = new GregorianCalendar();
        int year = date.get(Calendar.YEAR);
        Date startDate = formatter.parse((year - 1) + "/01/01");
        Date expiryDate = formatter.parse(year + "/01/01");
        certificate = CommonTestUtils.createCertificate("C=BE, O=GlobalSign nv-sa, OU=Root CA, CN=GlobalSign Root CA", "CN=SMP_RevokedSubject,O=European Commission,C=PT", startDate, expiryDate, false);

        assertThrows(CertificateExpiredException.class,
                () -> x509CertificateService.validateClientX509Certificate(new X509Certificate[]{certificate}));
    }

    @Test
    void testIsX509ClientCertificateNotYetException() throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        Calendar date = new GregorianCalendar();
        int year = date.get(Calendar.YEAR);
        Date startDate = formatter.parse((year + 1) + "/01/01");
        Date expiryDate = formatter.parse((year + 2) + "/01/01");
        certificate = CommonTestUtils.createCertificate("C=BE, O=GlobalSign nv-sa, OU=Root CA, CN=GlobalSign Root CA", "CN=SMP_RevokedSubject,O=European Commission,C=PT", startDate, expiryDate, false);

        assertThrows(CertificateNotYetValidException.class,
                () -> x509CertificateService.validateClientX509Certificate(new X509Certificate[]{certificate}));
    }

    @Test
    void testIsX509ClientCertificateNotFoundException() throws Exception {
        certificate = CommonTestUtils.createCertificate("C=BP, O=GlobalSign nv-sa, OU=Root CA, CN=GlobalSign Root CA", "CN=RevokedSubject,O=European Commission,C=PT", new Date(), new Date(), false);
        // then
        assertThrows(CertificateNotFoundException.class,
                () -> x509CertificateService.validateClientX509Certificate(new X509Certificate[]{certificate}));
    }

    @Test
    void testIsX509ClientCertificateAuthenticationException() throws Exception {

        HttpConnectionMock httpConnectionMock = new HttpConnectionMock("testNotExistent.crl");
        // Must throw error if CRL can not be fetched!
        Mockito.doReturn(false).when(configurationBusiness).isCertRevocationValidationGraceful();
        Mockito.doReturn(CRL_ONLY).when(configurationBusiness).getCertRevocationValidationStrategy();
        Mockito.doReturn(Collections.singletonList("file:/")).when(configurationBusiness).getCertRevocationValidationAllowedUrlProtocols();

        x509CertificateService.getCrlVerifierService().setHttpConnection(httpConnectionMock);
        certificate = CommonTestUtils.createCertificate("C=BE, O=GlobalSign nv-sa, OU=Issuer CA, CN=GlobalSign Root CA",
                "CN=SMP_CRLStrictValidation,O=European Commission,C=EU", true);

        GenericTechnicalException result = assertThrows(GenericTechnicalException.class,
                () -> x509CertificateService.validateClientX509Certificate(new X509Certificate[]{certificate})
        );

        assertThat(result.getMessage(), CoreMatchers.startsWith("[ERR-105] Error occurred while validating the certificate revocation list with error "));
    }

    @Test
    void testGetCertificate() throws Exception {
        certificate = CommonTestUtils.createCertificate("C=BE, O=GlobalSign nv-sa, OU=Root CA, CN=GlobalSign Root CA", "CN=RevokedSubject", new Date(), new Date(), false);
        assertEquals(x509CertificateService.getCertificate(new X509Certificate[]{certificate}), certificate);
    }

    @Test
    void testIsX509ClientTwoCertificateNotFoundException() throws Exception {
        certificate = CommonTestUtils.createCertificate("C=BP, O=GlobalSign nv-sa, OU=Root CA, CN=GlobalSign Root CA", "CN=RevokedSubject,O=European Commission,C=PT", new Date(), new Date(), false);
        assertThrows(CertificateNotFoundException.class,
                () -> x509CertificateService.validateClientX509Certificate(new X509Certificate[]{certificate, certificate}));
    }

    @Test
    void testIsBlueCoatClientCertificateRevoked() throws Exception {
        String serialNumber = "0400000000011E44A5E404";
        String issuer = "C=BE, O=GlobalSign nv-sa, OU=Root CA, CN=GlobalSign Root CA";
        String subject = "CN=SMP_RevokedSubject,O=DIGIT,C=BE";
        String startDateStr = "2015/01/01";
        String endDateStr = "2016/01/01";
        Date startDate = CommonTestUtils.convertStringToDate(startDateStr);
        Date expiryDate = CommonTestUtils.convertStringToDate(endDateStr);
        X509Certificate certificate = CommonTestUtils.createCertificate(serialNumber, issuer, subject, startDate, expiryDate, false);
        // then
        assertThrows(CertificateExpiredException.class,
                () -> x509CertificateService.validateClientX509Certificate(new X509Certificate[]{certificate}));
    }

    @Test
    void testNonRootCASubjectOk() throws Exception {
        String serialNumber = "0123456789101112";
        String issuer = "CN=SMP_123456789101112,O=DG-DIGIT,C=BE";
        String subject = "CN=SMP_123456789101112,O=DG-DIGIT,C=BE";
        X509Certificate certificate = CommonTestUtils.createCertificate(serialNumber, issuer, subject, false);
        x509CertificateService.validateClientX509Certificate(new X509Certificate[]{certificate});
    }

    @Test
    void testNonRootCASubjectOkButCrlNotOk() throws Exception {
        String serialNumber = "0123456789101112";
        String issuer = "CN=SMP_123456789101112951544545454,O=DG-DIGIT,C=BE";
        String subject = "CN=SMP_123456789101112,O=DG-DIGIT,C=BE";
        X509Certificate certificate = CommonTestUtils.createCertificate(serialNumber, issuer, subject, new Date(), CommonTestUtils.getFutureYearDate(2).getTime(), false);
        x509CertificateService.validateClientX509Certificate(new X509Certificate[]{certificate});
    }

    @Test
    void testNonRootCASubjectNotOk1() throws Exception {
        String serialNumber = "0123456789101112";
        String issuer = "CN=SMP_123456789101112,O=DG-DIGIT,C=BE";
        String subject = "CN=SMP_357951852456NotExist,O=DG-DIGIT,C=BE";

        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, 2);

        X509Certificate certificate = CommonTestUtils.createCertificate(serialNumber, issuer, subject, new Date(), c.getTime());

        assertThrows(CertificateNotFoundException.class,
                () -> x509CertificateService.validateClientX509Certificate(new X509Certificate[]{certificate}));
    }

    @Test
    void testNonRootCASubjectNotOk2() throws Exception {
        String serialNumber = "012345678910111213141516";
        String issuer = "CN=SMP_123456789101112,O=DG-DIGIT,C=BE";
        String subject = "CN=SMP_1234567891011121314,O=DG-DIGIT,C=BE";
        X509Certificate certificate = CommonTestUtils.createCertificate(serialNumber, issuer, subject, new Date(), new Date());
        x509CertificateService.getCrlVerifierService().setHttpConnection(new HttpConnectionMock("test.crl"));

        assertThrows(CertificateNotFoundException.class,
                () -> x509CertificateService.validateClientX509Certificate(new X509Certificate[]{certificate}));
    }

    @Test
    void testNonRootCACRLNotOk() throws Exception {
        String serialNumber = "0123456789101112";
        String issuer = "CN=SMP_123456789101112,O=DG-DIGIT,C=BE";
        String subject = "CN=SMP_123456789101112,O=DG-DIGIT,C=BE";
        X509Certificate certificate = CommonTestUtils.createCertificate(serialNumber, issuer, subject, new Date(), CommonTestUtils.getFutureYearDate(2).getTime(), false);

        x509CertificateService.validateClientX509Certificate(new X509Certificate[]{certificate});
    }

    @Test
    void testRootCAOK() throws Exception {
        String serialNumber = "2016112131415";
        String issuer = "CN=SMP_XPISSUER,O=DG-DIGIT,C=BE";
        String subject = "CN=SMP_XPTO2016112131415,O=DG-DIGIT,C=BE";
        X509Certificate certificate = CommonTestUtils.createCertificate(serialNumber, issuer, subject, false);
        x509CertificateService.validateClientX509Certificate(new X509Certificate[]{certificate});
    }

    @Test
    void testNonRootCAOk() throws Exception {
        String serialNumber = "2016112131415";
        String issuer = "CN=SMP_XPTO2016112985977,O=DG-DIGIT,C=BE";
        String subject = "CN=SMP_123456789101112,O=DG-DIGIT,C=BE";

        X509Certificate certificate = CommonTestUtils.createCertificate(serialNumber, issuer, subject, false);
        x509CertificateService.validateNonRootCA(certificate);
    }

    @Test
    void testNonRootCACertificateNotFound() throws Exception {
        String serialNumber = "2016112131415";
        String issuer = "CN=SMP_XPTO2016112985977,O=DG-DIGIT,C=BE";
        String subject = "CN=SMP_12345678910111278787,O=DG-DIGIT,C=BE";
        X509Certificate certificate = CommonTestUtils.createCertificate(serialNumber, issuer, subject, new Date(), CommonTestUtils.getFutureYearDate(2).getTime());

        assertThrows(CertificateNotFoundException.class,
                () -> x509CertificateService.validateClientX509Certificate(new X509Certificate[]{certificate}));
    }


    @Test
    void testImportCertificateCalculateCertificateId() throws Exception {
        X509Certificate cert = CommonTestUtils.loadCertificate("test_certificate-expired.cer");

        assertEquals("CN=test_certificate,O=digit,C=BE:3c2ccd7cb0a1faeac47a1291b00e921b4976f8cf76", CertificateUtils.calculateCertificateId(cert));
    }

    @Test
    void testCertificateCalculateCertificateIdWithSpaces() throws Exception {
        String serialNumber = "2016112131415";
        String subject = "CN=\\ SMP_StartEndSpace\\ ,O=DG-DIGIT\\ ,C=BE";
        X509Certificate certificate = CommonTestUtils.createCertificate(serialNumber, subject, subject, new Date(), CommonTestUtils.getFutureYearDate(2).getTime());

        assertEquals("CN=SMP_StartEndSpace,O=DG-DIGIT,C=BE:00000000000000000002016112131415", CertificateUtils.calculateCertificateId(certificate));
    }

    @Test
    void testImportCertificateCalculateCertificateId2() throws Exception {
        Certificate cert = CommonTestUtils.loadCertificate("CertificateWithSpecialCharacters.cer");

        assertEquals("CN=slash/backslash\\\\quote\\\"colon:_rfc2253special_ampersand&comma\\,equals\\=plus\\+lessthan\\<greaterthan\\>hash\\#semicolon\\;end,O=DEẞßAaPLzołcNOÆæØøAa,C=PL:00000000000000000000000000001010",
                CertificateUtils.calculateCertificateId((X509Certificate) cert));
    }

    /**
     * Valid certificate for RootCA
     */
    @Test
    void testRootCANotOK() throws Exception {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        String serial = "0123456789101112";
        String issuer = "CN=PEPPOL SERVICE METADATA PUBLISHER,O=NATIONAL IT AND TELECOM AGENCY,C=BE";
        String subject = "O=DG-DIGIT,C=BE,CN=SMP_9999999999999991525";
        X509Certificate certificate = CommonTestUtils.createCertificate(serial, issuer, subject, new Date(), CommonTestUtils.getFutureYearDate(2).getTime());
        x509CertificateService.getCrlVerifierService().setHttpConnection(new HttpConnectionMock("test.crl"));

        assertThrows(CertificateNotFoundException.class,
                () -> x509CertificateService.validateClientX509Certificate(new X509Certificate[]{certificate}));
    }

    @Test
    void testNonRootCASubjectOkButRoot() throws Exception {
        String serialNumber = "0123456789101112";
        String issuer = "CN=SMP_12345678910111299898,O=DG-DIGIT,C=BE";
        String subject = "CN=SMP_12345678910111277,O=DG-DIGIT,C=BE";
        X509Certificate certificate = CommonTestUtils.createCertificate(serialNumber, issuer, subject, new Date(), CommonTestUtils.getFutureYearDate(2).getTime());

        assertThrows(CertificateNotFoundException.class,
                () -> x509CertificateService.validateClientX509Certificate(new X509Certificate[]{certificate}));
    }

    @Test
    void testRootCAIssuerOkButNonRoot() throws Exception {
        String serialNumber = "0123456789101112";
        String issuer = "CN=SMP_0000011113333377777,O=DG-DIGIT,C=BE";
        String subject = "CN=SMP_1298765432166666,O=DG-DIGIT,C=BE";
        X509Certificate certificate = CommonTestUtils.createCertificate(serialNumber, issuer, subject, new Date(), CommonTestUtils.getFutureYearDate(2).getTime());
        x509CertificateService.getCrlVerifierService().setHttpConnection(new HttpConnectionMock("test.crl"));

        assertThrows(CertificateNotFoundException.class,
                () -> x509CertificateService.validateClientX509Certificate(new X509Certificate[]{certificate}));
    }


    @Test
    void testRootCAChainAll() throws Exception {
        X509Certificate[] certLst = CommonTestUtils.createCertificateChain(new String[]{"CN=Chain1 RootCA,O=DIGIT-B28, C=BE",
                "CN=Chain1 IntermediateCA,O=DIGIT-B28, C=BE",
                "CN=SMP_CERT Chain1,O=DIGIT-B28, C=BE"}, new Date(), CommonTestUtils.getFutureYearDate(2).getTime());

        x509CertificateService.validateClientX509Certificate(certLst);
    }

    @Test
    void testRootCAChainIntermediate() throws Exception {
        X509Certificate[] certLst = CommonTestUtils.createCertificateChain(new String[]{"CN=Chain2 RootCA,O=DIGIT-B28, C=BE",
                "CN=Chain2 IntermediateCA,O=DIGIT-B28, C=BE",
                "CN=SMP_CERT Chain2,O=DIGIT-B28, C=BE"}, new Date(), CommonTestUtils.getFutureYearDate(2).getTime());

        x509CertificateService.validateClientX509Certificate(certLst);
    }

    @Test
    void testRootCAChainOnlyRootCA() throws Exception {
        X509Certificate[] certLst = CommonTestUtils.createCertificateChain(new String[]{"CN=Chain3 RootCA,O=DIGIT-B28, C=BE",
                "CN=Chain3 IntermediateCA,O=DIGIT-B28, C=BE",
                "CN=SMP_CERT Chain3,O=DIGIT-B28, C=BE"}, new Date(), CommonTestUtils.getFutureYearDate(2).getTime());

        assertThrows(CertificateNotFoundException.class,
                () -> x509CertificateService.validateClientX509Certificate(certLst));
    }

    @Test
    void testRootCAChainNotExists() throws Exception {
        X509Certificate[] certLst = CommonTestUtils.createCertificateChain(new String[]{"CN=Chain4 RootCA,O=DIGIT-B28, C=BE",
                "CN=Chain4 IntermediateCA,O=DIGIT-B28, C=BE",
                "CN=SMP_CERT Chain4,O=DIGIT-B28, C=BE"}, new Date(), CommonTestUtils.getFutureYearDate(2).getTime());


        assertThrows(CertificateNotFoundException.class,
                () -> x509CertificateService.validateClientX509Certificate(certLst));
    }

    @Test
    void testNonRootCASubjectOkButRootFlagActive() throws Exception {
        try {
            String serialNumber = "0123456789101112";
            String issuer = "CN=SMP_12345678910111299898,O=DG-DIGIT,C=BE";
            String subject = "CN=SMP_12345678910111277,O=DG-DIGIT,C=BE";
            X509Certificate certificate = CommonTestUtils.createCertificate(serialNumber, issuer, subject, new Date(), CommonTestUtils.getFutureYearDate(2).getTime());

            x509CertificateService.validateClientX509Certificate(new X509Certificate[]{certificate});

            fail("This certificate is not trusted. An exception should be thrown.");
        } catch (CertificateNotFoundException exc) {
            assertEquals("[ERR-115] Unknown or not trusted certificate CN=SMP_12345678910111277,O=DG-DIGIT,C=BE", exc.getMessage());
        }
    }

    @Test
    void testRootCAIssuerOkButRootFlagNotActive() throws Exception {

        String serialNumber = "0123456789101112";
        String issuer = "CN=SMP_0000011113333377777,O=DG-DIGIT,C=BE";
        String subject = "CN=SMP_12345678910111277,O=DG-DIGIT,C=BE";
        X509Certificate certificate = CommonTestUtils.createCertificate(serialNumber, issuer, subject, new Date(), CommonTestUtils.getFutureYearDate(2).getTime());

        CertificateNotFoundException result = assertThrows(CertificateNotFoundException.class,
                () -> x509CertificateService.validateClientX509Certificate(new X509Certificate[]{certificate}));
        assertThat(result.getMessage(), CoreMatchers.containsString("[ERR-115] Unknown or not trusted certificate"));
    }

    @Test
    void testIsX509ClientNonRootCertificateValidWithNullCRL() throws Exception {
        String serialNumber = "0123456789101112";
        String issuer = "CN=TRUTED_CERTIFICATE,O=DG-DIGIT,C=BE";
        String subject = "CN=COMMON_TRUSTED_CERTIFICATE_77777777,O=DG-DIGIT,C=BE";

        X509Certificate certificate = CommonTestUtils.createCertificate(serialNumber, issuer, subject, new Date(), CommonTestUtils.getFutureYearDate(2).getTime(), false);

        x509CertificateService.validateNonRootCA(certificate);
    }

    @Test
    void testIsX509ClientRootCertificateValidWithNullCRL() throws Exception {
        String serialNumber = "0123456789101112";
        String issuer = "C=PT, O=T-Systems GmbH, OU=T-Systems Center, ST=Nordrhein Westfalen/postalCode\\=57250, L=Netphen/street\\=Untere Industriestr, CN=Business CA 4";
        String subject = "CN=COMMON_77777777,O=DGDIGIT,C=BE";

        X509Certificate certificate = CommonTestUtils.createCertificate(serialNumber, issuer, subject, new Date(), CommonTestUtils.getFutureYearDate(2).getTime(), false);

        x509CertificateService.validateRootCA(certificate);
    }

    @ParameterizedTest
    @ValueSource(strings = {"12346", "00000000000000000000000000000000000000000000000144", "0a787884b0e45f2a3cb22b26634ddb5a", "a787884b0e45f2a3cb22b26634ddb5a"})
    void testSerialNumbersCalculation(String sNumber) throws Exception {

        calculateCertificateId(sNumber.toString(), 32);

    }

    private void calculateCertificateId(String serialNumber, int length) throws Exception {
        X509Certificate certificate = CommonTestUtils.createCertificate(serialNumber, "CN=SMP_0000011113333377777,O=DG-DIGIT,C=BE", "CN=SMP_12345678910111277,O=DG-DIGIT,C=BE", new Date(), CommonTestUtils.getFutureYearDate(2).getTime());
        String certificateId = CertificateUtils.calculateCertificateId(certificate);
        assertEquals(certificateId, CertificateUtils.calculateCertificateId(certificate));
        assertEquals(length, certificateId.replace("CN=SMP_12345678910111277,O=DG-DIGIT,C=BE:", "").length());
    }

    @ParameterizedTest
    @CsvSource({"5a585fFDFDFfdfdfdfdf787884b0e45f2a3cb22b26634ddb5a, 50",
            "10000000000000000000000000000000000000000000000000000144, 56"})
    void testBiggerSerialNumbersCalculation(String value, Integer length) throws Exception {

        calculateCertificateId(value, length);
    }

    @Test
    void testCertificateWithUnicodeCharacters() throws Exception {
        X509Certificate certificate = CommonTestUtils.createCertificate("123456", "CN=SMP_123456,O=DG-DIGIT-åüĝğ,C=BE", "CN=SMP_654321,O=DG-DIGIT-åüĝğ,C=BE", new Date(), CommonTestUtils.getFutureYearDate(2).getTime());
        String certificateId = CertificateUtils.calculateCertificateId(certificate);

        assertEquals("CN=SMP_654321,O=DG-DIGIT-augg,C=BE:00000000000000000000000000123456", certificateId);
    }

    @Test
    void testCertificateWithSpecialCharacters() throws Exception {
        X509Certificate certificate = CommonTestUtils.createCertificate("123456", "CN=SMP_123456,O=DG-D\\,IG\\=I;T-åüĝğ,C=BE", "CN=SMP_654321,O=DG-D\\,IGI;T-åüĝğ,C=BE", new Date(), CommonTestUtils.getFutureYearDate(2).getTime());
        String certificateId = CertificateUtils.calculateCertificateId(certificate);

        assertEquals("CN=SMP_654321,O=DG-D\\,IGI\\;T-augg,C=BE:00000000000000000000000000123456", certificateId);
    }

    @Test
    void testCertificateWithUnicodeCharactersNotHandled() throws Exception {
        ///Non-Diacritical are not handled
        X509Certificate certificate = CommonTestUtils.createCertificate("123456", "CN=SMP_123456,O=DG-DIGIT-åðüĝŉğ,C=BE", "CN=SMP_654321,O=DG-DIGIT-åðüĝŉğ,C=BE", new Date(), CommonTestUtils.getFutureYearDate(2).getTime());
        String certificateId = CertificateUtils.calculateCertificateId(certificate);

        assertEquals("CN=SMP_654321,O=DG-DIGIT-aðugŉg,C=BE:00000000000000000000000000123456", certificateId);
    }

    @Test
    void isClientX509CertificateValidTrustedNonRootCACertificate() throws Exception {
        testTrustabilityForIsClientX509CertificateValidWithNonRootCA(true);
    }

    @Test
    void isClientX509CertificateValidNotTrustedNonRootCACertificate() {

        assertThrows(CertificateNotFoundException.class,
                () -> testTrustabilityForIsClientX509CertificateValidWithNonRootCA(false));
    }

    @Test
    void testValidateClientX509Certificate() throws Exception {
        Certificate cert = CommonTestUtils.loadCertificate("CertificateWithSpecialCharacters.cer");

        x509CertificateService.validateClientX509Certificate(new X509Certificate[]{(X509Certificate) cert});
    }

    @Test
    void testValidateClientX509CertificateNotFound() throws Exception {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        X509Certificate certificate = CommonTestUtils.createCertificate("123456", "CN=SMP_123456,O=DG-DIGIT-åðüĝŉğ,C=BE", "CN=SMP_654321,O=DG-DIG\\,IT-åðüĝŉğ<greaterthan>hash#semicolon;end,C=BE", new Date(), CommonTestUtils.getFutureYearDate(2).getTime());

        try {
            x509CertificateService.validateClientX509Certificate(new X509Certificate[]{certificate});
            fail();
        } catch (Exception exc) {
            assertEquals("[ERR-115] Unknown or not trusted certificate CN=SMP_654321,O=DG-DIG\\,IT-aðugŉg\\<greaterthan\\>hash\\#semicolon\\;end,C=BE", exc.getMessage());
        }
    }

    void testTrustabilityForIsClientX509CertificateValidWithNonRootCA(boolean isCertificateTrusted) throws Exception {
        String serialNumber = String.valueOf(System.currentTimeMillis());
        String issuer = "CN=NEWDOMAIN_SMP_" + serialNumber + ",O=DG-DIGIT,C=BE";
        String subject = "CN=ISSUER_SMP_" + serialNumber + ",O=DG-DIGIT,C=BE";
        X509Certificate certificate = CommonTestUtils.createCertificate(serialNumber, issuer, subject, new Date(), CommonTestUtils.getFutureYearDate(2).getTime());
        x509CertificateService.getCrlVerifierService().setHttpConnection(new HttpConnectionMock("test.crl"));

        x509CertificateService.validateClientX509Certificate(new X509Certificate[]{certificate}, isCertificateTrusted);
    }
}
