/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.ws.soap;

import ec.services.wsdl.bdmsl.admin.data._1.DNSRecordType;
import ec.services.wsdl.bdmsl.admin.data._1.DeleteDNSRecordRequest;
import ec.services.wsdl.bdmsl.admin.data._1.DeleteDNSRecordResponse;
import ec.services.wsdl.bdmsl.admin.data._1.SubDomainType;
import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.enums.DNSRecordEnum;
import eu.europa.ec.bdmsl.common.enums.SMLRoleEnum;
import eu.europa.ec.bdmsl.common.exception.Messages;
import eu.europa.ec.bdmsl.security.CertificateAuthentication;
import eu.europa.ec.edelivery.security.PreAuthenticatedCertificatePrincipal;

import org.junit.jupiter.api.BeforeEach;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.security.Principal;
import java.util.Collections;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class BDMSLAdminServiceWSDnsRecordTest extends AbstractJUnit5Test {

    @Autowired
    private IBDMSLAdminServiceWS bdmslAdminServiceWS;
    
    @BeforeEach
    void before() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        String certHeaderValue = "serial=48:b6:81:ee:8e:0d:cc:08&subject=EMAILADDRESS=receiver@test.be,C=BE, O=DIGIT, OU=FOR TEST ONLY,CN=DIGIT_SMP_TECH_TEAM_3&validfrom=Feb  1 14:20:18 2017 GMT&validto=Jul  9 23:59:00 2019 GMT&issuer=C=BE, O=DIGIT, OU=FOR TEST ONLY, CN=DIGIT_SMP_TECH_TEAM_2";
        Principal principal = blueCoatAuthenticationFilter.buildPrincipalFromHeader(certHeaderValue);
        Authentication authentication = new CertificateAuthentication((PreAuthenticatedCertificatePrincipal) principal, Collections.singletonList(SMLRoleEnum.ROLE_ADMIN.getAuthority()));
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }


    @Test
    void testAddDNSRecordNullRequest() throws Exception {

        assertThrows(BadRequestFault.class, () -> bdmslAdminServiceWS.addDNSRecord(null),"[ERR-106] The input values must not be null!");
    }

    @Test
    void testDeleteDNSRecordNullRequest() throws Exception {
        assertThrows(BadRequestFault.class, () -> bdmslAdminServiceWS.deleteDNSRecord(null),"[ERR-106] The input values must not be null!");
    }

    @Test
    void testAddDNSRecordDNSDisabled() throws Exception {

        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        Mockito.doReturn(false).when(configurationBusiness).isDNSEnabled();

        DNSRecordType recordType = createDNSRecord(createSubDomain.getDNSZone());

        assertThrows(InternalErrorFault.class, () -> bdmslAdminServiceWS.addDNSRecord(recordType),"[ERR-109] DNS server is not enabled!");
    }

    @Test
    void testAddDNSRecordPositiveCaseNaptr() throws Exception {

        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        Mockito.doReturn(true).when(configurationBusiness).isDNSEnabled();
        DNSRecordType recordType = createDNSRecord(createSubDomain.getDNSZone());
        recordType.setType(DNSRecordEnum.NAPTR.getCode());
        recordType.setValue(".*");
        recordType.setService("Meta:SMP");

        DNSRecordType response = bdmslAdminServiceWS.addDNSRecord(recordType);

        assertNotNull(response);
        assertEquals(recordType.getDNSZone(), response.getDNSZone());
        assertEquals(recordType.getName(), response.getName());
        assertEquals(recordType.getService(), response.getService());
        assertEquals(recordType.getType(), response.getType());
        assertEquals(recordType.getValue(), response.getValue());
    }

    @Test
    void testAddDNSRecordPositiveCaseA() throws Exception {

        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        Mockito.doReturn(true).when(configurationBusiness).isDNSEnabled();
        DNSRecordType recordType = createDNSRecord(createSubDomain.getDNSZone());
        recordType.setType(DNSRecordEnum.A.getCode());
        recordType.setValue("192.168.1.5");
        recordType.setService(null);

        DNSRecordType response = bdmslAdminServiceWS.addDNSRecord(recordType);

        assertNotNull(response);
        assertEquals(recordType.getDNSZone(), response.getDNSZone());
        assertEquals(recordType.getName(), response.getName());
        assertEquals(recordType.getService(), response.getService());
        assertEquals(recordType.getType(), response.getType());
        assertEquals(recordType.getValue(), response.getValue());
    }

    @Test
    void testAddDNSRecordPositiveCaseCName() throws Exception {

        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        Mockito.doReturn(true).when(configurationBusiness).isDNSEnabled();
        DNSRecordType recordType = createDNSRecord(createSubDomain.getDNSZone());
        recordType.setType(DNSRecordEnum.CNAME.getCode());
        recordType.setValue("mydomain.test.eu");
        recordType.setService(null);

        DNSRecordType response = bdmslAdminServiceWS.addDNSRecord(recordType);

        assertNotNull(response);
        assertEquals(recordType.getDNSZone(), response.getDNSZone());
        assertEquals(recordType.getName(), response.getName());
        assertEquals(recordType.getService(), response.getService());
        assertEquals(recordType.getType(), response.getType());
        assertEquals(recordType.getValue(), response.getValue());
    }

    @Test
    void testAddDNSRecordEmptyDNSType() throws Exception {

        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        Mockito.doReturn(true).when(configurationBusiness).isDNSEnabled();


        DNSRecordType recordType = createDNSRecord(createSubDomain.getDNSZone());
        recordType.setType(null);


        assertThrows(BadRequestFault.class, () -> bdmslAdminServiceWS.addDNSRecord(recordType),Messages.BAD_REQUEST_PARAMETER_EMPTY_DNS_ENTRY_RT);

    }

    @Test
    void testAddDNSRecordEmptyDNSZone() throws Exception {

        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        Mockito.doReturn(true).when(configurationBusiness).isDNSEnabled();


        DNSRecordType recordType = createDNSRecord(createSubDomain.getDNSZone());
        recordType.setDNSZone(null);




        assertThrows(BadRequestFault.class, () -> bdmslAdminServiceWS.addDNSRecord(recordType),Messages.BAD_REQUEST_PARAMETER_EMPTY_DNSZONE);
    }

    @Test
    void testAddDNSRecordEmptyName() throws Exception {

        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        Mockito.doReturn(true).when(configurationBusiness).isDNSEnabled();


        DNSRecordType recordType = createDNSRecord(createSubDomain.getDNSZone());
        recordType.setName(null);



        assertThrows(BadRequestFault.class, () -> bdmslAdminServiceWS.addDNSRecord(recordType),Messages.BAD_REQUEST_PARAMETER_EMPTY_DNS_NAME);

    }

    @Test
    void testAddDNSRecordEmptyValue() throws Exception {

        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        Mockito.doReturn(true).when(configurationBusiness).isDNSEnabled();


        DNSRecordType recordType = createDNSRecord(createSubDomain.getDNSZone());
        recordType.setValue(null);

        assertThrows(BadRequestFault.class, () -> bdmslAdminServiceWS.addDNSRecord(recordType),Messages.BAD_REQUEST_PARAMETER_EMPTY_DNS_VALUE);

    }

    @Test
    void testAddDNSRecordInvalidDNSType() throws Exception {

        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        Mockito.doReturn(true).when(configurationBusiness).isDNSEnabled();


        DNSRecordType recordType = createDNSRecord(createSubDomain.getDNSZone());
        recordType.setType("KarNeki");

        assertThrows(BadRequestFault.class, () -> bdmslAdminServiceWS.addDNSRecord(recordType),Messages.BAD_REQUEST_PARAMETER_INVALID_DNS_ENTRY_RT);

    }

    @Test
    void testAddDNSRecordInvalidDNSZone() throws Exception {

        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        Mockito.doReturn(true).when(configurationBusiness).isDNSEnabled();


        DNSRecordType recordType = createDNSRecord(createSubDomain.getDNSZone());
        recordType.setDNSZone("karnekizone.ec.europa.eu");

        assertThrows(BadRequestFault.class, () -> bdmslAdminServiceWS.addDNSRecord(recordType),Messages.BAD_REQUEST_PARAMETER_INVALID_DNSZONE);

    }

    @Test
    void testAddDNSRecordInvalidDNSName() throws Exception {

        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        Mockito.doReturn(true).when(configurationBusiness).isDNSEnabled();


        DNSRecordType recordType = createDNSRecord(createSubDomain.getDNSZone());
        recordType.setName("karneki.ec.badname.eu");

        assertThrows(BadRequestFault.class, () -> bdmslAdminServiceWS.addDNSRecord(recordType),Messages.BAD_REQUEST_PARAMETER_INVALID_DNS_NAME);
    }

    @Test
    void testAddDNSRecordInvalidDNSNameInvalidDomainName() throws Exception {

        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        Mockito.doReturn(true).when(configurationBusiness).isDNSEnabled();


        DNSRecordType recordType = createDNSRecord(createSubDomain.getDNSZone());
        // label larger then 64 chars
        recordType.setName("test-123456789012345678901234567890123456789012345678901234567890." + recordType.getDNSZone());

        assertThrows(BadRequestFault.class, () -> bdmslAdminServiceWS.addDNSRecord(recordType),Messages.BAD_REQUEST_PARAMETER_INVALID_DNS_NAME);

    }

    @Test
    void testAddDNSRecordInvalidDNSNameInvalidValueForAType1() throws Exception {

        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        Mockito.doReturn(true).when(configurationBusiness).isDNSEnabled();


        DNSRecordType recordType = createDNSRecord(createSubDomain.getDNSZone());
        recordType.setType(DNSRecordEnum.A.getCode());
        recordType.setValue("256.1.1.1");

        assertThrows(BadRequestFault.class, () -> bdmslAdminServiceWS.addDNSRecord(recordType),"[ERR-106] Value: [256.1.1.1] is not valid IP address!");
    }

    @Test
    void testAddDNSRecordInvalidDNSNameInvalidValueForAType2() throws Exception {

        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        Mockito.doReturn(true).when(configurationBusiness).isDNSEnabled();


        DNSRecordType recordType = createDNSRecord(createSubDomain.getDNSZone());
        recordType.setType(DNSRecordEnum.A.getCode());
        recordType.setValue("10.48.1.1.5");


        assertThrows(BadRequestFault.class, () -> bdmslAdminServiceWS.addDNSRecord(recordType),"[ERR-106] Value: [10.48.1.1.5] is not valid IP address!");

    }

    @Test
    void testAddDNSRecordInvalidDNSNameInvalidValueForAType3() throws Exception {

        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        Mockito.doReturn(true).when(configurationBusiness).isDNSEnabled();


        DNSRecordType recordType = createDNSRecord(createSubDomain.getDNSZone());
        recordType.setType("A");
        recordType.setValue("www.ec.europa.eu");

        assertThrows(BadRequestFault.class, () -> bdmslAdminServiceWS.addDNSRecord(recordType), "[ERR-106] Value: [www.ec.europa.eu] is not valid IP address!");

    }

    @Test
    void testAddDNSRecordInvalidDNSNameInvalidValueForNaptr() throws Exception {

        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        Mockito.doReturn(true).when(configurationBusiness).isDNSEnabled();


        DNSRecordType recordType = createDNSRecord(createSubDomain.getDNSZone());
        recordType.setType(DNSRecordEnum.NAPTR.getCode());
        recordType.setService("Meta:SMP");
        recordType.setValue("..==++**");
        assertThrows(BadRequestFault.class, () -> bdmslAdminServiceWS.addDNSRecord(recordType), "[ERR-106] Value: [..==++**] is invalid regular expression!");
    }

    @Test
    void testAddDNSRecordInvalidDNSNameInvalidValueForCNAME1() throws Exception {

        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        Mockito.doReturn(true).when(configurationBusiness).isDNSEnabled();


        DNSRecordType recordType = createDNSRecord(createSubDomain.getDNSZone());
        recordType.setType(DNSRecordEnum.CNAME.getCode());
        recordType.setValue("192.168.56.2");

        assertThrows(BadRequestFault.class, () -> bdmslAdminServiceWS.addDNSRecord(recordType),"[ERR-106] Value: [192.168.56.2] is not valid domain address!");
    }


    @Test
    void testAddDNSRecordInvalidDNSNameInvalidValueForCNAME2() throws Exception {

        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        Mockito.doReturn(true).when(configurationBusiness).isDNSEnabled();


        DNSRecordType recordType = createDNSRecord(createSubDomain.getDNSZone());
        recordType.setType(DNSRecordEnum.CNAME.getCode());

        // label larger than 64 chars
        recordType.setValue("test-123456789012345678901234567890123456789012345678901234567890.test.eu");
        assertThrows(BadRequestFault.class, () -> bdmslAdminServiceWS.addDNSRecord(recordType),"[ERR-106] Value: [test-123456789012345678901234567890123456789012345678901234567890.test.eu] is not valid domain address!");
    }


    @Test
    void testAddDNSRecordEmptyServiceForNantprValue() throws Exception {

        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        Mockito.doReturn(true).when(configurationBusiness).isDNSEnabled();


        DNSRecordType recordType = createDNSRecord(createSubDomain.getDNSZone());
        recordType.setType(DNSRecordEnum.NAPTR.getCode());
        recordType.setValue(".*");
        recordType.setService(null);

        assertThrows(BadRequestFault.class, () -> bdmslAdminServiceWS.addDNSRecord(recordType),Messages.BAD_REQUEST_PARAMETER_EMPTY_DNS_SERVICE);

    }


    @Test
    void testDeleteDNSRecordNotExists() throws Exception {

        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        Mockito.doReturn(true).when(configurationBusiness).isDNSEnabled();


        DNSRecordType recordType = createDNSRecord(createSubDomain.getDNSZone());
        DeleteDNSRecordRequest request = new DeleteDNSRecordRequest();
        request.setName(recordType.getName());
        request.setDNSZone(recordType.getDNSZone());

        DeleteDNSRecordResponse result = bdmslAdminServiceWS.deleteDNSRecord(request);

        assertNotNull(result);
        assertTrue(result.getDNSRecords().isEmpty());

    }

    @Test
    void testDeleteDNSRecordPositiveCaseNaptr() throws Exception {

        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        Mockito.doReturn(true).when(configurationBusiness).isDNSEnabled();
        DNSRecordType recordType = createDNSRecord(createSubDomain.getDNSZone());
        recordType.setType(DNSRecordEnum.NAPTR.getCode());
        recordType.setValue(".*");
        recordType.setService("Meta:SMP");
        bdmslAdminServiceWS.addDNSRecord(recordType);

        DeleteDNSRecordRequest request = new DeleteDNSRecordRequest();
        request.setName(recordType.getName());
        request.setDNSZone(recordType.getDNSZone());

        DeleteDNSRecordResponse result = bdmslAdminServiceWS.deleteDNSRecord(request);


        assertNotNull(result);
        assertEquals(1, result.getDNSRecords().size());
        assertEquals(recordType.getDNSZone(), result.getDNSRecords().get(0).getDNSZone());
        assertEquals(recordType.getName(), result.getDNSRecords().get(0).getName());
        assertEquals(recordType.getService(), result.getDNSRecords().get(0).getService());
        assertEquals(recordType.getType(), result.getDNSRecords().get(0).getType());
        assertEquals(recordType.getValue(), result.getDNSRecords().get(0).getValue());
    }

    @Test
    void testDeleteDNSRecordPositiveCaseA() throws Exception {

        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        createSubDomain.setCertPolicyOIDs("0.4.0.194112.1.2");
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        Mockito.doReturn(true).when(configurationBusiness).isDNSEnabled();
        DNSRecordType recordType = createDNSRecord(createSubDomain.getDNSZone());
        recordType.setType(DNSRecordEnum.A.getCode());
        recordType.setValue("192.168.1.5");
        recordType.setService(null);
        bdmslAdminServiceWS.addDNSRecord(recordType);

        DeleteDNSRecordRequest request = new DeleteDNSRecordRequest();
        request.setName(recordType.getName());
        request.setDNSZone(recordType.getDNSZone());

        DeleteDNSRecordResponse result = bdmslAdminServiceWS.deleteDNSRecord(request);


        assertNotNull(result);
        assertEquals(1, result.getDNSRecords().size());
        assertEquals(recordType.getDNSZone(), result.getDNSRecords().get(0).getDNSZone());
        assertEquals(recordType.getName(), result.getDNSRecords().get(0).getName());
        assertEquals(recordType.getService(), result.getDNSRecords().get(0).getService());
        assertEquals(recordType.getType(), result.getDNSRecords().get(0).getType());
        assertEquals(recordType.getValue(), result.getDNSRecords().get(0).getValue());
    }

    @Test
    void testDeleteDNSRecordPositiveCaseCName() throws Exception {

        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        Mockito.doReturn(true).when(configurationBusiness).isDNSEnabled();
        DNSRecordType recordType = createDNSRecord(createSubDomain.getDNSZone());
        recordType.setType(DNSRecordEnum.CNAME.getCode());
        recordType.setValue("mydomain.test.eu");
        recordType.setService(null);
        bdmslAdminServiceWS.addDNSRecord(recordType);

        DeleteDNSRecordRequest request = new DeleteDNSRecordRequest();
        request.setName(recordType.getName());
        request.setDNSZone(recordType.getDNSZone());

        DeleteDNSRecordResponse result = bdmslAdminServiceWS.deleteDNSRecord(request);


        assertNotNull(result);
        assertEquals(1, result.getDNSRecords().size());
        assertEquals(recordType.getDNSZone(), result.getDNSRecords().get(0).getDNSZone());
        assertEquals(recordType.getName(), result.getDNSRecords().get(0).getName());
        assertEquals(recordType.getService(), result.getDNSRecords().get(0).getService());
        assertEquals(recordType.getType(), result.getDNSRecords().get(0).getType());
        assertEquals(recordType.getValue(), result.getDNSRecords().get(0).getValue());
    }


    @Test
    void testDeleteDNSRecordPositiveCaseDeleteAll() throws Exception {

        SubDomainType createSubDomain = BDMSLAdminServiceWSSubDomainTest.generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(createSubDomain);

        Mockito.doReturn(true).when(configurationBusiness).isDNSEnabled();
        DNSRecordType recordType = createDNSRecord(createSubDomain.getDNSZone());
        recordType.setType(DNSRecordEnum.CNAME.getCode());
        recordType.setValue("mydomain.test.eu");
        recordType.setService(null);
        bdmslAdminServiceWS.addDNSRecord(recordType);

        recordType.setValue("mydomain-two.test.eu");
        bdmslAdminServiceWS.addDNSRecord(recordType);

        recordType.setType(DNSRecordEnum.A.getCode());
        recordType.setValue("10.48.0.100");
        bdmslAdminServiceWS.addDNSRecord(recordType);

        recordType.setType(DNSRecordEnum.NAPTR.getCode());
        recordType.setValue(".*");
        recordType.setService("Meta:SMP");
        bdmslAdminServiceWS.addDNSRecord(recordType);

        DeleteDNSRecordRequest request = new DeleteDNSRecordRequest();
        request.setName(recordType.getName());
        request.setDNSZone(recordType.getDNSZone());

        DeleteDNSRecordResponse result = bdmslAdminServiceWS.deleteDNSRecord(request);


        assertNotNull(result);
        assertEquals(4, result.getDNSRecords().size()); // two CName + A + NAPTR
    }

    static DNSRecordType createDNSRecord(String dnsZone) {
        DNSRecordType recordType = new DNSRecordType();

        recordType.setDNSZone(dnsZone);
        recordType.setName(UUID.randomUUID().toString() + "." + dnsZone);
        recordType.setValue("10.48.0.101");
        recordType.setType("A");


        return recordType;

    }

}
