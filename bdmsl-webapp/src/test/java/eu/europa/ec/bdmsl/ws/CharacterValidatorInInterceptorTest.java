/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.ws;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import org.apache.cxf.databinding.DataBinding;
import org.apache.cxf.databinding.DataReader;
import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.jaxb.JAXBDataBinding;
import org.apache.cxf.jaxb.io.DataReaderImpl;
import org.apache.cxf.message.Exchange;
import org.apache.cxf.message.ExchangeImpl;
import org.apache.cxf.message.Message;
import org.apache.cxf.message.MessageImpl;
import org.apache.cxf.service.Service;
import org.apache.cxf.service.model.*;
import org.busdox.servicemetadata.locator._1.ServiceMetadataPublisherServiceForParticipantType;
import org.hamcrest.CoreMatchers;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import jakarta.xml.bind.JAXBContext;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;


/**
 * @author Flavio SANTOS
 */
class CharacterValidatorInInterceptorTest extends AbstractJUnit5Test {

    @Autowired
    private CharacterValidatorInInterceptor characterValidatorInInterceptor;

    private static Message message;

    @BeforeAll
    static void before() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        message = new MessageImpl();
        MessagePartInfo messagePartInfo = new MessagePartInfo(new QName("http://busdox.org/serviceMetadata/ManageBusinessIdentifierService/1.0/}messagePart"), null);
        messagePartInfo.setConcreteName(new QName("http://busdox.org/serviceMetadata/locator/1.0/}CreateParticipantIdentifier"));
        messagePartInfo.setTypeClass(ServiceMetadataPublisherServiceForParticipantType.class);
        JAXBDataBinding jaxbDataBinding = new JAXBDataBinding(JAXBContext.newInstance(ServiceMetadataPublisherServiceForParticipantType.class));
        DataReader dataReader = new DataReaderImpl(jaxbDataBinding, true);

        Exchange exchange = Mockito.mock(ExchangeImpl.class);
        Service mockService = Mockito.mock(Service.class);
        DataBinding mockDataBinding = Mockito.mock(DataBinding.class);
        Endpoint mockEndpoint = Mockito.mock(Endpoint.class);
        EndpointInfo mockEndpointInfo = Mockito.mock(EndpointInfo.class);
        BindingInfo mockBindingInfo = Mockito.mock(BindingInfo.class);
        ServiceInfo serviceInfo = Mockito.mock(ServiceInfo.class);
        InterfaceInfo mockInterfaceInfo = Mockito.mock(InterfaceInfo.class);
        BindingOperationInfo mockBindingOperationInfo = Mockito.mock(BindingOperationInfo.class);
        BindingMessageInfo mockBindingMessageInfo = Mockito.mock(BindingMessageInfo.class);
        OperationInfo mockOperationInfo = Mockito.mock(OperationInfo.class);

        Mockito.when(mockService.getDataBinding()).thenReturn(jaxbDataBinding);
        Mockito.when(mockDataBinding.createReader(javax.xml.stream.XMLStreamReader.class)).thenReturn(dataReader);
        Mockito.when(exchange.getEndpoint()).thenReturn(mockEndpoint);
        Mockito.when(exchange.getService()).thenReturn(mockService);
        Mockito.when(mockEndpoint.getEndpointInfo()).thenReturn(mockEndpointInfo);
        Mockito.when(mockEndpointInfo.getBinding()).thenReturn(mockBindingInfo);
        Mockito.when(mockEndpoint.getEndpointInfo().getService()).thenReturn(serviceInfo);
        Mockito.when(serviceInfo.getInterface()).thenReturn(mockInterfaceInfo);
        Mockito.when(mockInterfaceInfo.getOperations()).thenReturn(new ArrayList<OperationInfo>());
        Mockito.when(exchange.getBindingOperationInfo()).thenReturn(mockBindingOperationInfo);
        Mockito.when(mockBindingOperationInfo.getBinding()).thenReturn(mockBindingInfo);
        Mockito.when(mockBindingInfo.getService()).thenReturn(serviceInfo);
        Mockito.when(serviceInfo.getProperty("soap.force.doclit.bare")).thenReturn(Boolean.TRUE);
        Mockito.when(mockBindingOperationInfo.isUnwrappedCapable()).thenReturn(Boolean.FALSE);
        Mockito.when(mockBindingOperationInfo.getInput()).thenReturn(mockBindingMessageInfo);
        Mockito.when(mockBindingOperationInfo.getOutput()).thenReturn(mockBindingMessageInfo);
        Mockito.when(mockBindingOperationInfo.getOperationInfo()).thenReturn(mockOperationInfo);
        Mockito.when(mockOperationInfo.isOneWay()).thenReturn(false);
        Mockito.when(mockBindingMessageInfo.getMessageParts()).thenReturn(Arrays.asList(new MessagePartInfo[]{messagePartInfo}));

        message.setExchange(exchange);
    }

    @Test
    void testHandleMessageMalformedXML() {
        Fault fault = assertThrows(Fault.class, () -> handle("xml/malformedRequest.xml"));
        assertThat(fault.getMessage(), CoreMatchers.startsWith("[106] Request values should only contain ASCII characters and must be well-formed."));
    }

    @Test
    void testHandleMessageWellFormedXMLValidBySchema() throws Exception {
        assertDoesNotThrow(() -> handle("xml/well-formedRequest.xml"));
    }

    private void handle(String filePath) throws Exception {
        //GIVEN
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        xmlInputFactory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
        xmlInputFactory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, false);
        XMLStreamReader xmlStreamReader = xmlInputFactory.createXMLStreamReader(Thread.currentThread().getContextClassLoader().getResource(filePath).openStream());
        message.setContent(XMLStreamReader.class, xmlStreamReader);

        //WHEN THEN
        characterValidatorInInterceptor.handleMessage(message);
    }
}
