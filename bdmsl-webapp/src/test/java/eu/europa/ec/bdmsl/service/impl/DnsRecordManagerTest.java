/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.impl;

import eu.europa.ec.bdmsl.business.impl.IdentifierFormatterBusinessImpl;
import eu.europa.ec.bdmsl.common.exception.BadRequestException;
import eu.europa.ec.bdmsl.common.exception.DnsRecordBuildException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.common.logging.ILoggingService;
import eu.europa.ec.bdmsl.service.dns.impl.DnsRecordManager;
import org.apache.commons.lang3.StringUtils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.xbill.DNS.Name;
import org.xbill.DNS.Record;
import org.xbill.DNS.Type;

import java.util.Collections;
import java.util.UUID;

import static eu.europa.ec.bdmsl.common.util.Constant.FAILED_TO_BUILD_DNS_NAME_FROM;
import static org.junit.jupiter.api.Assertions.*;


/**
 * @author Flavio SANTOS
 */
final class DnsRecordManagerTest {

    ILoggingService mockILoggingService = Mockito.mock(ILoggingService.class);
    IdentifierFormatterBusinessImpl mockIIdentifierFormatterBusiness =
            new IdentifierFormatterBusinessImpl();

    DnsRecordManager dnsRecordManager = new DnsRecordManager(mockILoggingService, mockIIdentifierFormatterBusiness);

    @Test
    void testCreateSMP() throws TechnicalException {
        String smpId = "SMPID";
        String subdomain = "subdomain.com.";
        String logicalAddress = "http://logical.address";
        String dnsPublisherPrefix = "publisher";
        Record dnsRecord = dnsRecordManager.createSMP(smpId, subdomain, logicalAddress, dnsPublisherPrefix);

        assertEquals("SMPID.publisher.subdomain.com.", dnsRecord.getName().toString());
        assertEquals("SMPID.publisher.subdomain.com.\t60\tIN\tCNAME\tlogical.address.", dnsRecord.toString());
    }

    @Test
    void testCreateSMPWithWrongLogicalAddress() {
        String smpId = "SMPID";
        String subdomain = "subdomain.com.";
        String logicalAddress = "logicaladdress";
        String dnsPublisherPrefix = "publisher";

        assertThrows(BadRequestException.class, () -> {
            Record dnsRecord = dnsRecordManager.createSMP(smpId, subdomain, logicalAddress, dnsPublisherPrefix);
        });
    }


    @Test
    void testCreateSMPWithWrongSubdomain() {
        String smpId = "SMPID";
        String subdomain = "subdomain.com";
        String logicalAddress = "http://logical.address";
        String dnsPublisherPrefix = "publisher";
        assertThrows(DnsRecordBuildException.class, () -> dnsRecordManager.createSMP(smpId, subdomain, logicalAddress, dnsPublisherPrefix));
    }

    @Test
    void testCreateSMPWithWrongSmpId() {
        String smpId = "SMP-ID";
        String subdomain = "subdomain.com";
        String logicalAddress = "http://logical.address";
        String dnsPublisherPrefix = "publisher";

        assertThrows(DnsRecordBuildException.class, () -> dnsRecordManager.createSMP(smpId, subdomain, logicalAddress, dnsPublisherPrefix), "[ERR-107] "+FAILED_TO_BUILD_DNS_NAME_FROM+"logical.address'");
    }

    @Test
    void testCreateSMPWithNullPrefix() {
        String smpId = "SMP-ID";
        String subdomain = "subdomain.com";
        String logicalAddress = "http://logical.address";

        assertThrows(DnsRecordBuildException.class, () -> {
            dnsRecordManager.createSMP(smpId, subdomain, logicalAddress, null);
        }, "[ERR-107] Failed to build DNS object, SMP data must be not null.");
    }

    @Test
    void testCreateSMPWithNullLogicalAddress() {
        String smpId = "SMP-ID";
        String subdomain = "subdomain.com";
        String dnsPublisherPrefix = "publisher";


        assertThrows(BadRequestException.class, () -> dnsRecordManager.createSMP(smpId, subdomain, null, dnsPublisherPrefix), "[ERR-106] Logical address is malformed: null");
    }

    @Test
    void testCreateSMPWithNullSubdomain() {
        String smpId = "SMP-ID";
        String logicalAddress = "http://logical.address";
        String dnsPublisherPrefix = "publisher";

        assertThrows(DnsRecordBuildException.class, () -> dnsRecordManager.createSMP(smpId, null, logicalAddress, dnsPublisherPrefix), "[ERR-107] Failed to build DNS object, SMP data must be not null.");
    }

    @Test
    public void testGenerateDnsQuery() {
        String participantId = "participantId";
        String subdomain = "subdomain.com.";
        String scheme = "edelivery-scheme-test";
        int participantType = Type.A;

        DnsRecordBuildException dnsRecordBuildException = assertThrows(DnsRecordBuildException.class,
                () -> dnsRecordManager.generateDnsQuery(participantId, scheme, subdomain, participantType));

        Mockito.verify(mockILoggingService).warn("Cannot generate participant DNS Name for participant 'participantId' having scheme 'edelivery-scheme-test' because of unsupported DNS type: 'A'. Possible values: CNAME and NAPTR");
        Assertions.assertEquals("[ERR-107] Failed to build DNS Name from 'A' type", dnsRecordBuildException.getMessage());
    }

    @Test
    public void testGenerateDnsQuery_CNAME() throws Exception {
        String participantId = "participantId";
        String subdomain = "subdomain.com.";
        String scheme = "edelivery-scheme-test";
        int participantType = Type.CNAME;
        String generatedNAPTR = UUID.randomUUID().toString();

        dnsRecordManager = Mockito.spy(dnsRecordManager);
        Mockito.doReturn(Name.fromString(generatedNAPTR)).when(dnsRecordManager).generateParticipantCNAME(participantId, scheme, subdomain);

        Name name = dnsRecordManager.generateDnsQuery(participantId, scheme, subdomain, participantType);
        Assertions.assertEquals(generatedNAPTR, name.toString());
    }

    @Test
    public void testGenerateDnsQuery_NAPTR() throws Exception {
        String participantId = "participantId";
        String subdomain = "subdomain.com.";
        String scheme = "edelivery-scheme-test";
        int participantType = Type.NAPTR;
        String generatedNAPTR = UUID.randomUUID().toString();

        dnsRecordManager = Mockito.spy(dnsRecordManager);
        Mockito.doReturn(Name.fromString(generatedNAPTR)).when(dnsRecordManager).generateParticipantNAPTR(participantId, scheme, subdomain);

        Name name = dnsRecordManager.generateDnsQuery(participantId, scheme, subdomain, participantType);
        Assertions.assertEquals(generatedNAPTR, name.toString());
    }


    @Test
    void testCreateParticipantCNAME() throws TechnicalException {
        String participantId = "participantId";
        String subdomain = "subdomain.com.";
        String scheme = "edelivery-scheme-test";
        String smpId = "SMPID";
        String dnsPublisherPrefix = "publisher";

        Record dnsRecord = dnsRecordManager.createParticipantCNAME(participantId, scheme, subdomain, smpId, dnsPublisherPrefix);

        assertEquals("B-689672748466206a411a188adb5e9d3f.edelivery-scheme-test.subdomain.com.\t60\tIN\tCNAME\tSMPID.publisher.subdomain.com.", dnsRecord.toString());
        assertEquals("B-689672748466206a411a188adb5e9d3f.edelivery-scheme-test.subdomain.com.", dnsRecord.getName().toString());
    }

    @Test
    void testCreateCaseInsensitiveParticipantCNAME() throws TechnicalException {
        String participantId = "0099:UpperAndLowerCAseIDENTIFIER";
        String subdomain = "subdomain.com.";
        String scheme1 = "delivery-scheme-test";
        String caseSensitiveScheme = "sensitive-participant-sc1";
        String smpId = "SMPID";
        String dnsPublisherPrefix = "publisher";
        // set case sensitive schemes
        mockIIdentifierFormatterBusiness.setCaseSensitiveSchemes(Collections.singletonList(caseSensitiveScheme));
        // then

        Record dnsRecord = dnsRecordManager.createParticipantCNAME(participantId, scheme1, subdomain, smpId, dnsPublisherPrefix);
        Record dnsRecordLowerCase = dnsRecordManager.createParticipantCNAME(StringUtils.lowerCase(participantId), scheme1, subdomain, smpId, dnsPublisherPrefix);
        Record dnsSensitiveRecord = dnsRecordManager.createParticipantCNAME(participantId, caseSensitiveScheme, subdomain, smpId, dnsPublisherPrefix);

        assertEquals(dnsRecordLowerCase.getName().toString(), dnsRecord.getName().toString());
        assertEquals("B-5c94805c84a7597ff761266d0db6b0bc.delivery-scheme-test.subdomain.com.", dnsRecord.getName().toString());
        assertEquals("B-b15e25d6bdc3961292cce07110490381.sensitive-participant-sc1.subdomain.com.", dnsSensitiveRecord.getName().toString());
    }

    @Test
    void testCreateParticipantWithNullSmpId() {
        String participantId = "participantId";
        String subdomain = "subdomain.com.";
        String scheme = "edelivery-scheme-test";
        String dnsPublisherPrefix = "publisher";

        assertThrows(DnsRecordBuildException.class, () -> dnsRecordManager.createParticipantCNAME(participantId, scheme, subdomain, null, dnsPublisherPrefix), "[ERR-107] Failed to build DNS object, SMP data must be not null.");
    }

    @Test
    void testCreateParticipantNAPTR() throws TechnicalException {
        String participantId = "participantId";
        String subdomain = "subdomain.com.";
        String scheme = "edelivery-scheme-test";
        String smpHost = "http://smp.address";
        String type = "META:SMP";

        Record dnsRecord = dnsRecordManager.createParticipantNAPTR(participantId, scheme, subdomain, type, smpHost);

        assertEquals("CFD3FVB4OCDKWDVXSAYZOVAUCXWFMKBBQMVQPOBXNPXEN7VQ7JNA.edelivery-scheme-test.subdomain.com.\t60\tIN\tNAPTR\t100 10 \"U\" \"META:SMP\" \"!.*!http://smp.address!\" .", dnsRecord.toString());
        assertEquals("CFD3FVB4OCDKWDVXSAYZOVAUCXWFMKBBQMVQPOBXNPXEN7VQ7JNA.edelivery-scheme-test.subdomain.com.", dnsRecord.getName().toString());
    }

    @Test
    void testCreateCaseInsensitiveParticipantNAPTR() throws TechnicalException {
        String participantId = "participantId";
        String subdomain = "subdomain.com.";
        String scheme1 = "delivery-scheme-test";
        String caseSensitiveScheme = "sensitive-participant-sc1";
        String smpId = "SMPID";
        String dnsPublisherPrefix = "publisher";
        // set case sensitive schemes
        mockIIdentifierFormatterBusiness.setCaseSensitiveSchemes(Collections.singletonList(caseSensitiveScheme));
        // then

        Record dnsRecord = dnsRecordManager.createParticipantNAPTR(participantId, scheme1, subdomain, smpId, dnsPublisherPrefix);
        Record dnsRecordLowerCase = dnsRecordManager.createParticipantNAPTR(StringUtils.lowerCase(participantId), scheme1, subdomain, smpId, dnsPublisherPrefix);
        Record dnsSensitiveRecord = dnsRecordManager.createParticipantNAPTR(participantId, caseSensitiveScheme, subdomain, smpId, dnsPublisherPrefix);

        assertEquals(dnsRecordLowerCase.getName().toString(), dnsRecord.getName().toString());
        assertEquals("CFD3FVB4OCDKWDVXSAYZOVAUCXWFMKBBQMVQPOBXNPXEN7VQ7JNA.delivery-scheme-test.subdomain.com.", dnsRecord.getName().toString());
        assertEquals("PKYXNHXCSLAZWLSY3HJZJ37L6LWBAXUSAU57OQ7X5E2ZTVVTKT6A.sensitive-participant-sc1.subdomain.com.", dnsSensitiveRecord.getName().toString());
    }

    @Test
    void testGenerateUNaptrValueLegacyFalse() {
        String smpHost = "http://smp.address";

        String uNaptrValue = dnsRecordManager.generateUNaptrValue(smpHost);

        assertEquals("!.*!http://smp.address!", uNaptrValue);
    }

    @Test
    void testCreateParticipantNAPTRWithNullSmpAddress() {
        String participantId = "participantId";
        String subdomain = "subdomain.com.";
        String scheme = "edelivery-scheme";
        String type = "META:SMP";

        assertThrows(DnsRecordBuildException.class, () -> dnsRecordManager.createParticipantNAPTR(participantId, scheme, subdomain, type, null), "[ERR-107] SMP Address was not found for creating NAPTR record.");
    }

    @Test
    void testCreateParticipantNAPTRWithNullParticipant() {
        String subdomain = "subdomain.com.";
        String scheme = "edelivery-scheme";
        String type = "META:SMP";
        String smpHost = "http://smp.address";

        assertThrows(DnsRecordBuildException.class, () -> dnsRecordManager.createParticipantNAPTR(null, scheme, subdomain, type, smpHost), "[ERR-107] Failed to build DNS object, Participant data must be not null.");
    }

    @Test
    void testCreateParticipantNAPTRWithNullType() {
        String participantId = "participantId";
        String subdomain = "subdomain.com";
        String scheme = "edelivery-scheme-test";
        String smpHost = "http://smp.address";

        assertThrows(DnsRecordBuildException.class, () -> dnsRecordManager.createParticipantNAPTR(participantId, scheme, subdomain, null, smpHost));

    }
}
