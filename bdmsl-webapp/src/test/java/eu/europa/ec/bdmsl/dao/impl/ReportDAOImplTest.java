/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.impl;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.dao.entity.reports.ExpiredSMPEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ReportDAOImplTest extends AbstractJUnit5Test {

    @Autowired
    ReportDAOImpl testInstance;

    @Test
    void getListOfSMPsWithExpiredCertificates() {

        List<ExpiredSMPEntity> result = testInstance.getListOfSMPsWithExpiredCertificates();
        assertNotNull(result);
        assertEquals(2, result.size());
        assertEquals("CN=test_expired_cert,O=DG-DIGIT,C=BE:0000000000000001", result.get(0).getCertificateId());
        assertEquals("SMP-EXPIRED-CERT-01", result.get(0).getSmpId());
        assertEquals("ehealth.edelivery.tech.ec.europa.eu", result.get(0).getSubdomainName());
        assertEquals(1, result.get(0).getRegisteredParticipantsCount().intValue());
        assertNotNull(result.get(0).getLastParticipantAddedOn());
        assertNotNull(result.get(0).getSmpCreatedOn());
        assertNotNull(result.get(0).getCertificateValidFrom());
        assertNotNull(result.get(0).getCertificateValidTo());


        assertEquals("CN=test_expired_cert,O=DG-DIGIT,C=BE:0000000000000001", result.get(1).getCertificateId());
        assertEquals("SMP-EXPIRED-CERT-02", result.get(1).getSmpId());
        assertEquals("ehealth.edelivery.tech.ec.europa.eu", result.get(1).getSubdomainName());
        assertEquals(0, result.get(1).getRegisteredParticipantsCount().intValue());
        assertNull(result.get(1).getLastParticipantAddedOn());
        assertNotNull(result.get(1).getSmpCreatedOn());
        assertNotNull(result.get(1).getCertificateValidFrom());
        assertNotNull(result.get(1).getCertificateValidTo());
    }
}
