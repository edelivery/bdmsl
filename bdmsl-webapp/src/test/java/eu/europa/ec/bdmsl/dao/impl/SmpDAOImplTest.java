/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.impl;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.bo.ServiceMetadataPublisherBO;
import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import eu.europa.ec.bdmsl.common.exception.InvalidArgumentException;
import eu.europa.ec.bdmsl.common.exception.NotFoundException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.dao.ICertificateDAO;
import eu.europa.ec.bdmsl.dao.ISmpDAO;
import eu.europa.ec.bdmsl.test.CommonTestUtils;
import eu.europa.ec.bdmsl.security.CertificateDetails;
import eu.europa.ec.bdmsl.security.UnsecureAuthentication;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

import static eu.europa.ec.bdmsl.test.TestConstants.*;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Tiago MIGUEL
 * @since 30/03/2017
 */
class SmpDAOImplTest extends AbstractJUnit5Test {

    @Autowired
    ISmpDAO testInstance;

    @Autowired
    ICertificateDAO certificateDAO;

    @Test
    @Transactional
    void createSmpOKUnsecure() throws TechnicalException {
        // given
        SubdomainBO authorizedSubDomain = subdomainDAO.getSubDomain(DOMAIN_PEPPOL);
        String smpID = UUID.randomUUID().toString();
        CertificateDetails certificate = CommonTestUtils.createCertificateDetails(
                UnsecureAuthentication.UNSECURE_HTTP_SERIAL,
                UnsecureAuthentication.UNSECURE_HTTP_CLIENT,
                UnsecureAuthentication.UNSECURE_HTTP_ROOT_CA);
        // dummy unsecure certificate in database
        certificate.setCertificateId(UnsecureAuthentication.UNSECURE_HTTP_CLIENT);
        // create smp for UnsecureAuthentication.UNSECURE_HTTP_CLIENT
        ServiceMetadataPublisherBO smpBO = createSmpBO(smpID, UnsecureAuthentication.UNSECURE_HTTP_CLIENT);

        // when
        testInstance.createSMP(smpBO, certificate, authorizedSubDomain);

        // then
        ServiceMetadataPublisherBO result = testInstance.findSMP(smpID);
        assertNotNull(result);
        assertNotSame(smpBO, result); // just make sure different objects
        assertEquals(smpID, result.getSmpId());
        assertEquals(smpBO.getCertificateId(), result.getCertificateId());
        assertEquals(smpBO.getLogicalAddress(), result.getLogicalAddress());
        assertEquals(smpBO.getPhysicalAddress(), result.getPhysicalAddress());
        assertEquals(authorizedSubDomain.getSubdomainName(), result.getSubdomain().getSubdomainName());
    }

    @Test
    @Transactional
    void createSmpOkCertBaseAuth() throws Exception {
        // given
        SubdomainBO authorizedSubDomain = subdomainDAO.getSubDomain(DOMAIN_PEPPOL);
        String smpID = UUID.randomUUID().toString();
        CertificateDetails certificateDetails = CommonTestUtils.createCertificateDetails(
                CERTIFICATE_01_SERIAL,
                CERTIFICATE_01_ISSUER,
                CERTIFICATE_01_SUBJECT);

        ServiceMetadataPublisherBO smpBO = createSmpBO(smpID, certificateDetails.getCertificateId());

        // when
        testInstance.createSMP(smpBO, certificateDetails, authorizedSubDomain);
        // then
        ServiceMetadataPublisherBO result = testInstance.findSMP(smpID);
        assertNotNull(result);
        assertNotSame(smpBO, result); // just make sure different objects
        assertEquals(smpID, result.getSmpId());
        assertEquals(smpBO.getCertificateId(), result.getCertificateId());
        assertEquals(smpBO.getLogicalAddress(), result.getLogicalAddress());
        assertEquals(smpBO.getPhysicalAddress(), result.getPhysicalAddress());
        assertEquals(authorizedSubDomain.getSubdomainName(), result.getSubdomain().getSubdomainName());
    }

    @Test
    void createSmpFailCertificateWithIdDoesNotExist() throws TechnicalException {
        // given
        SubdomainBO authorizedSubDomain = subdomainDAO.getSubDomain(DOMAIN_PEPPOL);
        String smpID = UUID.randomUUID().toString();
        String certificateId = "CertificateDoesNotExists" + UUID.randomUUID();
        ServiceMetadataPublisherBO smpBO = createSmpBO(smpID, certificateId);

        CertificateDetails certificate = new CertificateDetails();
        certificate.setCertificateId(certificateId);

        //when
        NotFoundException exception = assertThrows(NotFoundException.class, () ->
                testInstance.createSMP(smpBO, certificate, authorizedSubDomain));
        // then
        assertEquals("[ERR-118] Certificate with id [" + certificateId + "] does not exist!", exception.getMessage());
    }

    @Test
    void createSmpFailCertificateWithNullIdInDetails() throws TechnicalException {
        // given
        SubdomainBO authorizedSubDomain = subdomainDAO.getSubDomain(DOMAIN_PEPPOL);
        String smpID = UUID.randomUUID().toString();
        String certificateId = "CertificateDoesNotExists" + UUID.randomUUID();
        ServiceMetadataPublisherBO smpBO = createSmpBO(smpID, certificateId);

        CertificateDetails certificate = new CertificateDetails();
        certificate.setCertificateId(null);

        //when
        InvalidArgumentException exception = assertThrows(InvalidArgumentException.class, () ->
                testInstance.createSMP(smpBO, certificate, authorizedSubDomain));
        // then
        assertEquals("[ERR-119] Invalid certificateId! Certificate Id must not be Null/Empty!", exception.getMessage());
    }

    @Test
    void testListSMPs() throws TechnicalException {
        // given
        //when
        List<ServiceMetadataPublisherBO> listSMPs = testInstance.listSMPs();
        // then
        assertFalse(listSMPs.isEmpty());
    }

    @Test
    void findSMP() throws TechnicalException {
        // given / when
        String smpID = SMP_ID_001;
        ServiceMetadataPublisherBO smp = testInstance.findSMP(smpID);
        // then
        assertNotNull(smp);
        assertEquals(smpID, smp.getSmpId());
        assertNotNull(smp.getSubdomain());
        assertNotNull(smp.getCertificateId());
    }

    @Test
    void findSMPNotExists() throws TechnicalException {
        // given / when
        String smpID = UUID.randomUUID().toString();
        ServiceMetadataPublisherBO smp = testInstance.findSMP(smpID);
        // then
        assertNull(smp);
    }

    @Test
    @Transactional
    void deleteSMP() throws TechnicalException {
        // given
        String smpID = SMP_ID_002;
        ServiceMetadataPublisherBO smp = testInstance.findSMP(smpID);
        assertNotNull(smp);
        // when
        testInstance.deleteSMP(smp);
        // then
        ServiceMetadataPublisherBO result = testInstance.findSMP(smpID);
        assertNull(result);
    }

    @Test
    @Transactional
    void deleteSMPNotExists() throws TechnicalException {
        // given
        String smpID = UUID.randomUUID().toString();
        ServiceMetadataPublisherBO smp = createSmpBO(smpID, UnsecureAuthentication.UNSECURE_HTTP_CLIENT);
        // when
        testInstance.deleteSMP(smp);
        // then
        ServiceMetadataPublisherBO result = testInstance.findSMP(smpID);
        assertNull(result);
    }

    @Test
    @Transactional
    void updateSMP() throws TechnicalException {
        // given
        String smpID = SMP_ID_003;

        ServiceMetadataPublisherBO smp = testInstance.findSMP(smpID);
        assertNotNull(smp);
        assertFalse(smp.isDisabled());
        ServiceMetadataPublisherBO smpOld =
                new ServiceMetadataPublisherBO(smp.getSmpId(), smp.getCertificateId(), smp.getPhysicalAddress(),
                        smp.getLogicalAddress(), smp.getSubdomain(), smp.getDnsRecord(),false);
        smp.setCertificateId(UUID.randomUUID().toString());
        smp.setPhysicalAddress(UUID.randomUUID().toString());
        smp.setLogicalAddress(UUID.randomUUID().toString());
        smp.setDisabled(true);
        smp.setSubdomain(null);
        // when
        testInstance.updateSMP(smp);
        // then
        ServiceMetadataPublisherBO result = testInstance.findSMP(smpID);
        assertNotNull(result);
        assertNotNull(result.getSubdomain());
        // only physical address and local address are allowed to change
        assertEquals(smpOld.getSmpId(), result.getSmpId());
        assertEquals(smpOld.getDnsRecord(), result.getDnsRecord());
        assertEquals(smpOld.getCertificateId(), result.getCertificateId());
        assertEquals(smpOld.getSubdomain().getSubdomainName(), result.getSubdomain().getSubdomainName());
        assertEquals(smpOld.isDisabled(), result.isDisabled()); // only admin service can change this

        assertEquals(smp.getLogicalAddress(), result.getLogicalAddress());
        assertEquals(smp.getPhysicalAddress(), result.getPhysicalAddress());
    }

    @Test
    @Transactional
    void updateSMPNotExists() throws TechnicalException {
        // given
        String smpID = SMP_ID_003;
        ServiceMetadataPublisherBO smp = testInstance.findSMP(smpID);
        assertNotNull(smp);
        ServiceMetadataPublisherBO smpOld =
                new ServiceMetadataPublisherBO(smp.getSmpId(),
                        smp.getCertificateId(),
                        smp.getPhysicalAddress(),
                        smp.getLogicalAddress(),
                        smp.getSubdomain(),
                        smp.getDnsRecord(), false);
        smp.setSmpId(UUID.randomUUID().toString());
        smp.setCertificateId(UUID.randomUUID().toString());
        smp.setPhysicalAddress(UUID.randomUUID().toString());
        smp.setLogicalAddress(UUID.randomUUID().toString());
        smp.setDisabled(true);
        smp.setSubdomain(null);

        // when
        testInstance.updateSMP(smp);
        // then
        ServiceMetadataPublisherBO result = testInstance.findSMP(smpID);
        assertNotNull(result);
        assertNotNull(result.getSubdomain());
        // only physical address and local address are allowed to change
        assertEquals(smpOld.getSmpId(), result.getSmpId());
        assertEquals(smpOld.getDnsRecord(), result.getDnsRecord());
        assertEquals(smpOld.getCertificateId(), result.getCertificateId());
        assertEquals(smpOld.getSubdomain().getSubdomainName(), result.getSubdomain().getSubdomainName());
        assertEquals(smpOld.isDisabled(), result.isDisabled()); // only admin service can change this
        // nothing changed because id changed which does not exist smpOld.getSmpId()
        assertEquals(smpOld.getLogicalAddress(), result.getLogicalAddress());
        assertEquals(smpOld.getPhysicalAddress(), result.getPhysicalAddress());
    }

    @Test
    @Transactional
    void changeCertificateForSMP() throws TechnicalException {
        // given
        String smpID = SMP_ID_003;
        ServiceMetadataPublisherBO smp = testInstance.findSMP(smpID);
        assertNotNull(smp);
        String smpOldCertificateID = smp.getCertificateId();
        String smpNewCertificateID = "CN=SMP-ChangeNonPKICert,O=DIGIT,C=BE:00000000000000000000000000000004";
        Long certIdOld = certificateDAO.findCertificateByCertificateId(smpOldCertificateID).getId();
        Long certIdNew = certificateDAO.findCertificateByCertificateId(smpNewCertificateID).getId();
        assertNotEquals(certIdNew, certIdOld);
        // when
        testInstance.changeCertificateForSMP(certIdOld, certIdNew);
        // then
        ServiceMetadataPublisherBO result = testInstance.findSMP(smpID);
        assertEquals(smpNewCertificateID, result.getCertificateId());
    }

    private ServiceMetadataPublisherBO createSmpBO(String id, String certificateId) {
        ServiceMetadataPublisherBO smpBO = new ServiceMetadataPublisherBO();
        smpBO.setSmpId(id);
        smpBO.setLogicalAddress("http://some.smp.ey/smp");
        smpBO.setPhysicalAddress("0.0.0.0");
        smpBO.setCertificateId(certificateId);
        return smpBO;
    }
}
