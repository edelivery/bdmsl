/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.business.impl;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.business.ICertificateDomainBusiness;
import eu.europa.ec.bdmsl.common.bo.CertificateBO;
import eu.europa.ec.bdmsl.common.bo.CertificateDomainBO;
import eu.europa.ec.bdmsl.common.exception.BadRequestException;
import eu.europa.ec.bdmsl.common.exception.GenericTechnicalException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.dao.ICertificateDomainDAO;
import eu.europa.ec.bdmsl.test.CommonTestUtils;
import eu.europa.ec.bdmsl.security.CertificateDetails;
import eu.europa.ec.bdmsl.util.CertificateUtils;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import jakarta.transaction.Transactional;
import org.springframework.test.util.ReflectionTestUtils;

import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.text.MatchesPattern.matchesPattern;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Tiago MIGUEL
 * @since 02/12/2016
 */

class CertificateDomainBusinessImplTest extends AbstractJUnit5Test {

    private static final String SERIAL_NUMBER = "2016112131415";

    private static final String OLD_DOMAIN_CERTIFICATE_1 = "CN=SMP_DISCOVERY_1,O=DG-DIGIT,C=BE";
    private static final String OLD_DOMAIN_CERTIFICATE_3 = "CN=SMP_DISCOVERY_3,O=DG-DIGIT,C=BE";
    private static final String DOMAIN_CERTIFICATE_ISSUER_2 = "CN=SMP_DISCOVERY_ISSUER_2,O=DG-DIGIT,C=BE";

    private static final String OLD_CERTIFICATE_ID_1 = OLD_DOMAIN_CERTIFICATE_1 + ":" + SERIAL_NUMBER;
    private static final String OLD_CERTIFICATE_ID_4 = "CN=SMP_DISCOVERY_4,O=DG-DIGIT,C=BE" + ":" + SERIAL_NUMBER;

    @Autowired
    private ICertificateDomainDAO certificateDomainDAO;

    @Autowired
    private ICertificateDomainBusiness certificateDomainBusiness;

    @BeforeEach
    public void setUp() {
        loggingService = Mockito.spy(loggingService);
        ReflectionTestUtils.setField(certificateDomainBusiness, "loggingService", loggingService);
    }

    @Test
    @Transactional
    @Disabled("Not working on Windows OS. See the EDELIVERY-13185")
    void testCreateNewCertificateDomain() throws Exception {
        //GIVEN
        String newCommonCN = "CN=DIGIT_SMP_DUMMY_2,O=DIGIT,C=BE";
        String newIssuerCN = "CN=DIGIT_SMP_DUMMY_2,O=DIGIT,C=BE";
        String newSerialNumber = "012345678910121345678";

        CertificateBO oldCertificateBO = createCertificate("CN=EHEALTH_SMP_1000000032,O=DG-DIGIT,C=BE:6caa15ffed56ad049dd2b9bc835d2e46", CommonTestUtils.getPastYearDate(1), CommonTestUtils.getFutureYearDate(2));
        CertificateBO newCertificateBO = createCertificate(newCommonCN + ":" + newSerialNumber, CommonTestUtils.getPastYearDate(1), CommonTestUtils.getFutureYearDate(2));
        X509Certificate x509Certificate = CommonTestUtils.createCertificate(newSerialNumber, newIssuerCN, newCommonCN, new Date(), CommonTestUtils.getFutureYearDate(1).getTime());

        //WHEN-THEN
        //Checking current CertificateDomain
        CertificateDomainBO certificateDomain = certificateDomainDAO.findCertificateDomainByCertificate(CertificateUtils.removeSerialFromSubject(oldCertificateBO.getCertificateId()));
        assertEquals("CN=EHEALTH_SMP_1000000032,O=DG-DIGIT,C=BE", certificateDomain.getCertificate());

        //Checking new CertificateDomain before changing
        certificateDomain = certificateDomainDAO.findCertificateDomainByCertificate(CertificateUtils.removeSerialFromSubject(newCertificateBO.getCertificateId()));
        assertNull(certificateDomain);

        //Change CertificateDomain
        certificateDomainBusiness.changeCertificateDomain(oldCertificateBO, newCertificateBO, x509Certificate, true);

        //Checking current CertificateDomain after changing
        certificateDomain = certificateDomainDAO.findCertificateDomainByCertificate(CertificateUtils.removeSerialFromSubject(oldCertificateBO.getCertificateId()));
        assertNull(certificateDomain);

        //Checking new CertificateDomain after changing
        certificateDomain = certificateDomainDAO.findCertificateDomainByCertificate(CertificateUtils.removeSerialFromSubject(newCertificateBO.getCertificateId()));
        assertEquals("CN=DIGIT_SMP_DUMMY_2,O=DIGIT,C=BE", certificateDomain.getCertificate());
    }

    @Test
    @Transactional
    void testChangeCertificateDomainNonRootCaAlreadyExists() {
        //GIVEN OLD_DOMAIN_CERTIFICATE_3  is on different domain than OLD_CERTIFICATE_ID_1
        String issuer = "CN=SMP_DISCOVERY_ISSUER_3,O=DG-DIGIT,C=BE";
        String subject = OLD_DOMAIN_CERTIFICATE_3;
        String newCertificateId = subject + ":" + SERIAL_NUMBER;

        //WHEN
        GenericTechnicalException result = assertThrows(GenericTechnicalException.class,
                () -> testChangeCertificateDomain(issuer, subject, OLD_CERTIFICATE_ID_1,
                        newCertificateId));
        // then
        assertThat(result.getMessage(),
                matchesPattern("\\[ERR-105\\] The new certificate domain .*" + OLD_DOMAIN_CERTIFICATE_3 + ".* already exists in the database on different domain."));
    }

    @Test
    @Transactional
    void testChangeCertificateDomainForNonRootCAOk() throws Exception {
        //GIVEN
        String issuer = "CN=SMP_DISCOVERY_8_ISSUER,O=DG-DIGIT,C=BE";
        String subject = "CN=SMP_DISCOVERY_8,O=DG-DIGIT,C=BE";

        //WHEN - THEN
        testChangeCertificateDomain(issuer, subject,
                OLD_CERTIFICATE_ID_1,
                "CN=SMP_DISCOVERY_8,O=DG-DIGIT,C=BE:2016112131415");
    }

    @Test
    void testChangeCertificateRootCaDomainNoMatch() {
        //GIVEN
        String issuer = DOMAIN_CERTIFICATE_ISSUER_2;
        String subject = "CN=SMP_DISCOVERY_0,O=DG-DIGIT,C=BE";
        String newCertificateId = subject + ":" + SERIAL_NUMBER;
        //WHEN
        BadRequestException result = assertThrows(BadRequestException.class,
                () -> testChangeCertificateDomain(issuer, subject, OLD_CERTIFICATE_ID_4, newCertificateId));
        assertThat(result.getMessage(),
                matchesPattern("\\[ERR-106\\] The new IssuerBasedAuthorized certificate .* belongs to domain: .* but old belongs to domain: .*"));
    }

    @Test
    void testChangeCertificateDomainBelongsToDifferentPKIModel() {
        //GIVEN
        String issuer = "CN=SMP_DISCOVERY_NEW_CERT_0_ISSUER,O=DG-DIGIT,C=BE";
        String subject = "CN=SMP_DISCOVERY_NEW_CERT_0,O=DG-DIGIT,C=BE";
        String newCertificateId = subject + ":" + SERIAL_NUMBER;
        //WHEN
        BadRequestException result = assertThrows(BadRequestException.class,
                () -> testChangeCertificateDomain(issuer, subject, newCertificateId,
                        OLD_CERTIFICATE_ID_4));
        // then
        assertEquals("[ERR-106] It is not allowed to replace a Root CA certificate by a Non Root CA certificate.", result.getMessage());
    }

    @Test
    @Transactional
    void testChangeLeafCertificateWhereIssuerBelongsToDifferentDomain() throws Exception {
        //GIVEN
        String issuer = DOMAIN_CERTIFICATE_ISSUER_2;
        String subject = "CN=SMP_DISCOVERY_NEW_CERT_0,O=DG-DIGIT,C=BE";
        String newCertificateId = subject + ":" + SERIAL_NUMBER;

        //WHEN - THEN
        testChangeCertificateDomain(issuer, subject,
                OLD_CERTIFICATE_ID_1,
                newCertificateId);
    }

    @Test
    void testFindDomain1() throws TechnicalException {
        //GIVEN
        CertificateDomainBO certificateDomainBO = certificateDomainBusiness.findDomain("CN=rootCN,OU=B4,O=DIGIT,L=Brussels,ST=BE,C=BE");

        //WHEN THEN
        assertNotNull(certificateDomainBO);
    }

    @Test
    void testFindDomain2() throws Exception {
        //GIVEN
        String subject = "O=DG-DIGIT,C=BE,CN=EHEALTH_SMP_77777777";
        CertificateDetails certificateDetails = CommonTestUtils.createCertificateDetails("12345", subject, subject, Calendar.getInstance().getTime(), Calendar.getInstance().getTime());
        CertificateDomainBO certificateDomainBO = certificateDomainBusiness.findDomain(certificateDetails);

        //WHEN THEN
        assertNotNull(certificateDomainBO);
    }

    @Test
    void testFindAllDomains() throws Exception {
        //GIVEN
        List<CertificateDomainBO> certificateDomainBOs = certificateDomainBusiness.findAll();

        //WHEN THEN
        assertFalse(certificateDomainBOs.isEmpty());
    }

    @Test
    @Transactional
    void testDeleteCertificateDomain1() throws Exception {
        //GIVEN
        CertificateDomainBO certificateDomainBO = certificateDomainBusiness.findDomain("CN=TEST_LOGICAL_ADDRESS_12345,O=DIGIT,C=DK");

        //WHEN
        certificateDomainBusiness.deleteCertificateDomain(certificateDomainBO);

        // THEN
        certificateDomainBO = certificateDomainBusiness.findDomain("C=DK,CN=TEST_LOGICAL_ADDRESS_12345,O=DIGIT");
        assertNull(certificateDomainBO);
    }

    @Test
    @Transactional
    void testDeleteCertificateDomain2() throws Exception {
        //GIVEN
        CertificateBO certificateBO = new CertificateBO();
        certificateBO.setCertificateId("CN=test-discovery-2017,O=DIGIT,C=PT:123456789123456789");
        CertificateDomainBO certificateDomainBO = certificateDomainBusiness.findDomain("CN=test-discovery-2017,O=DIGIT,C=PT");

        //WHEN
        certificateDomainBusiness.deleteCertificateDomain(certificateBO);

        // THEN
        assertNotNull(certificateDomainBO);
        certificateDomainBO = certificateDomainBusiness.findDomain("CN=test-discovery-2017,O=DIGIT,C=PT");
        assertNull(certificateDomainBO);
    }

    @Test
    void testIsCertificateDomainNonRootCA() throws Exception {
        CertificateBO certificateBO = new CertificateBO();
        certificateBO.setCertificateId("CN=test-discovery-2017,O=DIGIT,C=PT:123456789123456789");

        boolean isNonRootCA = certificateDomainBusiness.isCertificateDomainNonRootCA(certificateBO);

        assertTrue(isNonRootCA);
    }

    @Test
    public void testChangeCertificateDomainRootCertificate() throws Exception {
        CertificateBO existingRootCertificate = new CertificateBO();
        existingRootCertificate.setCertificateId("CN=GlobalSign Root CA,OU=Root CA,O=GlobalSign nv-sa,C=BE:1");

        certificateDomainBusiness.changeCertificateDomain(existingRootCertificate, null, null, true);

        Mockito.verify(loggingService).info("Changing or creating a non-root certificate is not allowed");
    }

    @Test
    public void testChangeCertificateDomain_RootCertificate() throws Exception {
        CertificateBO existingLeafCertificate = new CertificateBO();
        existingLeafCertificate.setCertificateId("CN=test-discovery-2017,O=DIGIT,C=PT:123456789123456789");
        Mockito.doNothing().when(loggingService).info("Changing or creating a non-root certificate is not allowed");
        boolean isToCreateNewCertificateDomain = false;

        certificateDomainBusiness.changeCertificateDomain(existingLeafCertificate, null, null, isToCreateNewCertificateDomain);

        Mockito.verify(loggingService).info("Creating a new certificate domain is not allowed");
    }

    private CertificateBO createCertificate(String certificateId, Calendar validFrom, Calendar validTo) {
        CertificateBO newCertificateBO = new CertificateBO();
        newCertificateBO.setValidFrom(validFrom);
        newCertificateBO.setValidTo(validTo);
        newCertificateBO.setCertificateId(certificateId);
        newCertificateBO.setId(System.currentTimeMillis());
        return newCertificateBO;
    }

    private void testChangeCertificateDomain(String issuer, String subject, String oldCertificateId, String newCertificateId) throws Exception {
        X509Certificate certificate = CommonTestUtils.createCertificate(SERIAL_NUMBER, issuer, subject, new Date(), CommonTestUtils.getFutureYearDate(1).getTime(), false);
        CertificateBO oldCertificateBO = new CertificateBO();
        oldCertificateBO.setCertificateId(oldCertificateId);
        CertificateBO newCertificateBO = new CertificateBO();
        newCertificateBO.setCertificateId(newCertificateId);

        certificateDomainBusiness.changeCertificateDomain(certificate, oldCertificateBO, newCertificateBO, true);
    }

}
