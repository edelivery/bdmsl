/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.util;

import eu.europa.ec.bdmsl.common.enums.SMLRoleEnum;
import eu.europa.ec.bdmsl.common.exception.CertificateAuthenticationException;

import org.junit.jupiter.api.Test;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


class RoleUtilsTest {

    @Test
    void testGetRolesForCertSubject_RootCAStartSMP() throws Exception {
        String certSubject = "CN=SMP_123456789101112,C=BE,O=DG-DIGIT";
        boolean isNonRootCACertificate = false;
        RoleUtils.getsInstance().setSmpCertSubjectRegex(null);

        List<GrantedAuthority> lstAuth = RoleUtils.getsInstance().getRolesForCertSubject(certSubject, isNonRootCACertificate);

        assertNotNull(lstAuth);
        assertEquals(1, lstAuth.size());
        assertEquals(SMLRoleEnum.ROLE_SMP.getAuthority(), lstAuth.iterator().next());

        // test with new reg exp
        RoleUtils.getsInstance().setSmpCertSubjectRegex("^.*(CN=SMP_|OU=PEPPOL TEST SMP).*$");

        RoleUtils.getsInstance().getRolesForCertSubject(certSubject, isNonRootCACertificate);

        assertNotNull(lstAuth);
        assertEquals(1, lstAuth.size());
        assertEquals(SMLRoleEnum.ROLE_SMP.getAuthority(), lstAuth.iterator().next());

    }

    @Test
    void testGetRolesForCertSubject_BadCertSubject() throws Exception {
        boolean isNonRootCACertificate = false;
        String certSubject = "OU=SMP_123456789101112,C=BE,O=DG-DIGIT";
        assertThrows(CertificateAuthenticationException.class, () ->RoleUtils.getsInstance().getRolesForCertSubject(certSubject, isNonRootCACertificate));


    }


    @Test
    void testGetRolesForCertSubject_NonRootCAStartSMP() throws Exception {
        String certSubject = "CN=SMP_123456789101112,C=BE,O=DG-DIGIT";
        boolean isNonRootCACertificate = true;
        RoleUtils.getsInstance().setSmpCertSubjectRegex(null);

        List<GrantedAuthority> lstAuth = RoleUtils.getsInstance().getRolesForCertSubject(certSubject, isNonRootCACertificate);

        assertNotNull(lstAuth);
        assertEquals(1, lstAuth.size());
        assertEquals(SMLRoleEnum.ROLE_SMP.getAuthority(), lstAuth.iterator().next());

        // test with new reg exp
        RoleUtils.getsInstance().setSmpCertSubjectRegex("^.*(CN=SMP_|OU=PEPPOL TEST SMP).*$");

        RoleUtils.getsInstance().getRolesForCertSubject(certSubject, isNonRootCACertificate);

        assertNotNull(lstAuth);
        assertEquals(1, lstAuth.size());
        assertEquals(SMLRoleEnum.ROLE_SMP.getAuthority(), lstAuth.iterator().next());
    }

    @Test
    void testGetRolesForCertSubject_RootCAStartNoSMP() throws Exception {
        String certSubject = "CN=123456789101112,C=BE,O=DG-DIGIT";
        boolean isNonRootCACertificate = false;
        RoleUtils.getsInstance().setSmpCertSubjectRegex(null);

        List<GrantedAuthority> lstAuth = RoleUtils.getsInstance().getRolesForCertSubject(certSubject, isNonRootCACertificate);

        assertNotNull(lstAuth);
        assertTrue(lstAuth.isEmpty());

        // test with new reg exp
        RoleUtils.getsInstance().setSmpCertSubjectRegex("^.*(CN=SMP_|OU=PEPPOL TEST SMP).*$");

        RoleUtils.getsInstance().getRolesForCertSubject(certSubject, isNonRootCACertificate);

        assertNotNull(lstAuth);
        assertTrue(lstAuth.isEmpty());

    }

    @Test
    void testGetRolesForCertSubject_EmptySubject() throws Exception {
        String certSubject = null;
        boolean isNonRootCACertificate = false;
        RoleUtils.getsInstance().setSmpCertSubjectRegex(null);

        List<GrantedAuthority> lstAuth = RoleUtils.getsInstance().getRolesForCertSubject(certSubject, isNonRootCACertificate);

        assertNotNull(lstAuth);
        assertEquals(AuthorityUtils.NO_AUTHORITIES, lstAuth);

        // test with new reg exp
        certSubject = "";

        RoleUtils.getsInstance().getRolesForCertSubject(certSubject, isNonRootCACertificate);

        assertNotNull(lstAuth);
        assertEquals(AuthorityUtils.NO_AUTHORITIES, lstAuth);

    }

    @Test
    void testGetRolesForCertSubject_NonRootCAStartNoSMP() throws Exception {
        String certSubject = "CN=123456789101112,C=BE,O=DG-DIGIT";
        boolean isNonRootCACertificate = true;
        RoleUtils.getsInstance().setSmpCertSubjectRegex(null);

        List<GrantedAuthority> lstAuth = RoleUtils.getsInstance().getRolesForCertSubject(certSubject, isNonRootCACertificate);

        assertNotNull(lstAuth);
        assertEquals(1, lstAuth.size());
        assertEquals(SMLRoleEnum.ROLE_SMP.getAuthority(), lstAuth.iterator().next());

        // test with new reg exp
        RoleUtils.getsInstance().setSmpCertSubjectRegex("^.*(CN=SMP_|OU=PEPPOL TEST SMP).*$");

        RoleUtils.getsInstance().getRolesForCertSubject(certSubject, isNonRootCACertificate);

        assertNotNull(lstAuth);
        assertEquals(1, lstAuth.size());
        assertEquals(SMLRoleEnum.ROLE_SMP.getAuthority(), lstAuth.iterator().next());

    }


    @Test
    void testGetRolesForCertSubject_RootCAStartNoSMPRexExpNewPKI() throws Exception {
        String certSubject = "C=BE, O=European Commission, OU=PEPPOL TEST SMP, CN=POP000004";
        boolean isNonRootCACertificate = false;
        RoleUtils.getsInstance().setSmpCertSubjectRegex(null);

        List<GrantedAuthority> lstAuth = RoleUtils.getsInstance().getRolesForCertSubject(certSubject, isNonRootCACertificate);

        assertNotNull(lstAuth);
        assertEquals(0, lstAuth.size());
    }

   // @Test
    void testGetRolesForCertSubject_RootCAStartNoSMPRexExpNewPKIRegExp() throws Exception {
        String certSubject = "C=BE, O=European Commission, OU=PEPPOL TEST SMP, CN=POP000004";
        boolean isNonRootCACertificate = false;
        RoleUtils.getsInstance().setSmpCertSubjectRegex("^.*(CN=SMP_|OU=PEPPOL TEST SMP).*$");

        List<GrantedAuthority> lstAuth = RoleUtils.getsInstance().getRolesForCertSubject(certSubject, isNonRootCACertificate);

        assertNotNull(lstAuth);
        assertEquals(0, lstAuth.size());
    }
}
