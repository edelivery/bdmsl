/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.test;

import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.service.dns.IDnsMessageSenderService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.xbill.DNS.Header;
import org.xbill.DNS.Message;
import org.xbill.DNS.Rcode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Adrien FERIAL
 * @since 22/06/2015
 */
@Service
@Primary
public class DnsMessageSenderServiceMock implements IDnsMessageSenderService {

    final private List<Message> messages = new ArrayList<>();

    @Override
    public Message sendMessage(Message message) {
        Message response = new Message();
        Header header = new Header();
        header.setRcode(Rcode.NOERROR);
        messages.add(message);
        return response;
    }

    public void reset()  {
        messages.clear();
    }

    public String getMessages() {
        StringBuilder result = new StringBuilder();
        for (Message message : messages) {
            result.append(message).append("\n");
        }
        return result.toString();
    }
}
