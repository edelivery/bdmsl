/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.tasks;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.business.IManageParticipantIdentifierBusiness;
import eu.europa.ec.bdmsl.business.IManageServiceMetadataBusiness;
import eu.europa.ec.bdmsl.common.bo.ParticipantBO;
import eu.europa.ec.bdmsl.common.bo.ServiceMetadataPublisherBO;
import eu.europa.ec.bdmsl.common.enums.AdminSMPManageActionEnum;
import eu.europa.ec.bdmsl.common.exception.BadRequestException;
import eu.europa.ec.bdmsl.common.exception.SmpNotFoundException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.service.IMailSenderService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.List;
import java.util.UUID;

import static eu.europa.ec.bdmsl.test.TestConstants.*;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;

class AdminSmpManagementTaskTest extends AbstractJUnit5Test {

    @Autowired
    AdminSmpManagementTask testInstance;

    @Autowired
    IManageServiceMetadataBusiness manageServiceMetadataBusiness;

    @BeforeEach
    public void before() throws Exception {
        configurationBusiness = Mockito.spy(configurationBusiness);
        ReflectionTestUtils.setField(ReflectionTestUtils.getField(testInstance, "dnsClientService"), "dnsMessageSenderService", dnsMessageSenderService);
        ReflectionTestUtils.setField(testInstance, "configurationBusiness", configurationBusiness);
    }

    @Test
    void testSetterGetterMethods() {
        AdminSMPManageActionEnum action = AdminSMPManageActionEnum.DELETE;
        String receiverAddress = UUID.randomUUID() + "@test.eu";
        String logicalAddress = UUID.randomUUID().toString();
        String physicalAddress = UUID.randomUUID().toString();
        ServiceMetadataPublisherBO smp = new ServiceMetadataPublisherBO();
        boolean setDisabled = true;
        // when
        testInstance.setAction(action);
        testInstance.setReceiverAddress(receiverAddress);
        testInstance.setUpdateData(logicalAddress, physicalAddress);
        testInstance.setMetadataPublisherBO(smp);
        // then
        assertEquals(action, testInstance.getAction());
        assertEquals(receiverAddress, testInstance.getReceiverAddress());
        assertEquals(logicalAddress, testInstance.getLogicalAddress());
        assertEquals(physicalAddress, testInstance.getPhysicalAddress());
        assertEquals(smp, testInstance.getMetadataPublisherBO());
    }

    @Test
    public void testRun() throws TechnicalException {
        AdminSmpManagementTask smpManagementTask = Mockito.spy(new AdminSmpManagementTask(null, null, null, null, null));
        smpManagementTask.setAction(AdminSMPManageActionEnum.DELETE);
        smpManagementTask.setUpdateData(UUID.randomUUID().toString(), UUID.randomUUID().toString());
        ServiceMetadataPublisherBO smp = Mockito.mock(ServiceMetadataPublisherBO.class);
        smpManagementTask.setMetadataPublisherBO(smp);
        String testResult = UUID.randomUUID().toString();

        ArgumentCaptor<AdminSMPManageActionEnum> actionCaptor = ArgumentCaptor.forClass(AdminSMPManageActionEnum.class);
        ArgumentCaptor<String> mailContentCaptor = ArgumentCaptor.forClass(String.class);

        doReturn(testResult).when(smpManagementTask).executeAction(actionCaptor.capture());
        doNothing().when(smpManagementTask).submitMailWithContent(mailContentCaptor.capture());

        // when
        smpManagementTask.run();
        //then
        assertEquals(testResult, mailContentCaptor.getValue());
        assertEquals(smpManagementTask.getAction(), actionCaptor.getValue());
    }

    @ParameterizedTest
    @EnumSource(AdminSMPManageActionEnum.class)
    public void testExecuteAction(AdminSMPManageActionEnum testAction) throws TechnicalException {
        AdminSmpManagementTask smpManagementTask = Mockito.spy(new AdminSmpManagementTask(null, null, null, null, null));
        smpManagementTask.setUpdateData(UUID.randomUUID().toString(), UUID.randomUUID().toString());
        ServiceMetadataPublisherBO smp = Mockito.mock(ServiceMetadataPublisherBO.class);
        smpManagementTask.setMetadataPublisherBO(smp);
        String testResult = UUID.randomUUID().toString();
        ArgumentCaptor<ServiceMetadataPublisherBO> parameterCaptor = ArgumentCaptor.forClass(ServiceMetadataPublisherBO.class);
        ArgumentCaptor<String> parameterCaptor2 = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> parameterCaptor3 = ArgumentCaptor.forClass(String.class);
        switch (testAction) {
            case DELETE:
                doReturn(testResult).when(smpManagementTask).deleteSMP(parameterCaptor.capture());
                break;
            case UPDATE:
                doReturn(testResult).when(smpManagementTask).updateSMP(parameterCaptor.capture(),
                        parameterCaptor2.capture(), parameterCaptor3.capture());
                break;
            case ENABLE:
                doReturn(testResult).when(smpManagementTask).enableSMP(parameterCaptor.capture());
                break;
            case DISABLE:
                doReturn(testResult).when(smpManagementTask).disableSMP(parameterCaptor.capture());
                break;
            case SYNC_DNS:
                doReturn(testResult).when(smpManagementTask).synchronizeDNSRecordsForSMP(parameterCaptor.capture());
                break;
        }
        // when
        String result = smpManagementTask.executeAction(testAction);
        // then
        assertEquals(testResult, result);
        assertEquals(smp, parameterCaptor.getValue());
        if (testAction == AdminSMPManageActionEnum.UPDATE) {
            assertEquals(smpManagementTask.getLogicalAddress(), parameterCaptor2.getValue());
            assertEquals(smpManagementTask.getPhysicalAddress(), parameterCaptor3.getValue());
        }
    }

    @Test
    void testDeleteSMPFailNotDisabled() {
        ServiceMetadataPublisherBO smp = new ServiceMetadataPublisherBO();
        smp.setSmpId(UUID.randomUUID().toString());
        smp.setDisabled(false);

        BadRequestException result = assertThrows(BadRequestException.class, () -> testInstance.deleteSMP(smp));
        assertThat(result.getMessage(), containsString("is not disabled and can not be deleted"));
    }

    @Test
    void testDeleteSMPFailNotDisabledParticipants() {
        ServiceMetadataPublisherBO smp = new ServiceMetadataPublisherBO();
        smp.setSmpId(SMP_ID_ONLY_CNAME);
        smp.setDisabled(true);

        BadRequestException result = assertThrows(BadRequestException.class, () -> testInstance.deleteSMP(smp));
        assertThat(result.getMessage(), Matchers.matchesRegex(".*has \\[\\d*\\] enabled participant! Please disable them first.*"));
    }


    @Test
    void testDisableSMPFailAlreadyDisabled() {
        ServiceMetadataPublisherBO smp = new ServiceMetadataPublisherBO();
        smp.setSmpId(SMP_ID_ONLY_CNAME);
        smp.setDisabled(true);

        BadRequestException result = assertThrows(BadRequestException.class, () -> testInstance.disableSMP(smp));
        assertThat(result.getMessage(), containsString("is already disabled"));
    }

    @Test
    void testEnableSMPFailAlreadyEnabled() {
        ServiceMetadataPublisherBO smp = new ServiceMetadataPublisherBO();
        smp.setSmpId(SMP_ID_ONLY_CNAME);
        smp.setDisabled(false);

        BadRequestException result = assertThrows(BadRequestException.class, () -> testInstance.enableSMP(smp));
        assertThat(result.getMessage(), containsString("is already enabled"));
    }


    @Test
    void testDisableSMPOk() throws TechnicalException {
        // find
        ServiceMetadataPublisherBO smp = manageServiceMetadataBusiness.verifySMPExist(SMP_ID_ONLY_CNAME);
        assertFalse(smp.isDisabled());
        List<ParticipantBO> participantBOList = manageParticipantIdentifierService.getManageParticipantIdentifierBusiness().getAllParticipantsForSMP(smp);
        assertFalse(participantBOList.isEmpty());
        assertTrue(participantBOList.stream().allMatch(p -> !p.isDisabled()));
        // get
        testInstance.disableSMP(smp);
        // then
        ServiceMetadataPublisherBO result = manageServiceMetadataBusiness.verifySMPExist(SMP_ID_ONLY_CNAME);
        List<ParticipantBO> resultParticipantBOList = manageParticipantIdentifierService.getManageParticipantIdentifierBusiness().getAllParticipantsForSMP(smp);
        assertTrue(result.isDisabled());
        assertTrue(resultParticipantBOList.stream().allMatch(p -> p.isDisabled()));

    }

    @Test
    void testEnableSMPOk() throws TechnicalException {
        // find
        ServiceMetadataPublisherBO smp = manageServiceMetadataBusiness.verifySMPExist(SMP_ID_DISABLED);
        assertTrue(smp.isDisabled());
        List<ParticipantBO> participantBOList = manageParticipantIdentifierService.getManageParticipantIdentifierBusiness().getAllParticipantsForSMP(smp);
        assertFalse(participantBOList.isEmpty());
        assertTrue(participantBOList.stream().allMatch(p -> p.isDisabled()));
        // get
        testInstance.enableSMP(smp);
        // then
        ServiceMetadataPublisherBO result = manageServiceMetadataBusiness.verifySMPExist(SMP_ID_DISABLED);
        List<ParticipantBO> resultParticipantBOList = manageParticipantIdentifierService.getManageParticipantIdentifierBusiness().getAllParticipantsForSMP(smp);
        assertFalse(result.isDisabled());
        assertTrue(resultParticipantBOList.stream().allMatch(p -> !p.isDisabled()));
    }

    @Test
    void testDeleteSMPOk() throws TechnicalException {
        // find
        ServiceMetadataPublisherBO smp = manageServiceMetadataBusiness.verifySMPExist(SMP_ID_DISABLED);
        assertTrue(smp.isDisabled());
        List<ParticipantBO> participantBOList = manageParticipantIdentifierService.getManageParticipantIdentifierBusiness().getAllParticipantsForSMP(smp);
        assertFalse(participantBOList.isEmpty());
        assertTrue(participantBOList.stream().allMatch(p -> p.isDisabled()));
        // get
        testInstance.deleteSMP(smp);
        // then
        SmpNotFoundException result = assertThrows(SmpNotFoundException.class, () -> manageServiceMetadataBusiness.verifySMPExist(SMP_ID_DISABLED));
        assertThat(result.getMessage(), containsString("[ERR-100] The Publisher identifier [SMP-DISABLED] doesn't exist"));

    }

    @Test
    void testUpdateSMPCnameOk() throws TechnicalException {
        // find
        String newLogicalAddress = "http://newPhysicalAddress/smp/";
        String newPhysicalAddress = "1.2.3.4";

        ServiceMetadataPublisherBO smp = manageServiceMetadataBusiness.verifySMPExist(SMP_ID_ONLY_CNAME);
        List<ParticipantBO> participantBOList = manageParticipantIdentifierService.getManageParticipantIdentifierBusiness().getAllParticipantsForSMP(smp);
        assertFalse(participantBOList.isEmpty());

        // then
        testInstance.updateSMP(smp, newLogicalAddress, newPhysicalAddress);
        // then
        ServiceMetadataPublisherBO result = manageServiceMetadataBusiness.verifySMPExist(SMP_ID_ONLY_CNAME);
        assertEquals(newLogicalAddress, result.getLogicalAddress());
        assertEquals(newPhysicalAddress, result.getPhysicalAddress());
    }

    @Test
    void testUpdateSMPNaptrOk() throws TechnicalException {
        // find
        String newLogicalAddress = "http://newPhysicalAddress/smp/";
        String newPhysicalAddress = "1.2.3.4";

        ServiceMetadataPublisherBO smp = manageServiceMetadataBusiness.verifySMPExist(SMP_ID_ONLY_NAPTR);
        List<ParticipantBO> participantBOList = manageParticipantIdentifierService.getManageParticipantIdentifierBusiness().getAllParticipantsForSMP(smp);
        assertFalse(participantBOList.isEmpty());

        // then
        testInstance.updateSMP(smp, newLogicalAddress, newPhysicalAddress);
        // then
        ServiceMetadataPublisherBO result = manageServiceMetadataBusiness.verifySMPExist(SMP_ID_ONLY_NAPTR);
        assertEquals(newLogicalAddress, result.getLogicalAddress());
        assertEquals(newPhysicalAddress, result.getPhysicalAddress());
    }

    @Test
    void testSynchronizeDNSRecordsForSMP() throws TechnicalException {
        ServiceMetadataPublisherBO smp = new ServiceMetadataPublisherBO();
        smp.setSmpId(UUID.randomUUID().toString());
        long participantCount = 2L;
        IManageParticipantIdentifierBusiness manageParticipantIdentifierBusiness = Mockito.mock(IManageParticipantIdentifierBusiness.class);
        IMailSenderService mailSenderService = Mockito.mock(IMailSenderService.class);

        AdminSmpManagementTask task = new AdminSmpManagementTask(
                manageServiceMetadataBusiness, manageParticipantIdentifierBusiness, mailSenderService, configurationBusiness, dnsClientService);
        task.setAction(AdminSMPManageActionEnum.SYNC_DNS);
        task.setMetadataPublisherBO(smp);
        AdminSmpManagementTask smpManagementTask = Mockito.spy(task);


        Mockito.doReturn(true).when(configurationBusiness).isDNSEnabled();
        Mockito.doReturn(participantCount).when(manageParticipantIdentifierBusiness).getParticipantCountForSMP(smp);
        Mockito.doReturn(List.of(new ParticipantBO(), new ParticipantBO())).when(manageParticipantIdentifierBusiness).getParticipantsForSMP(Mockito.eq(smp), Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(dnsClientService).synchronizeDNSRecordsForSMP(Mockito.eq(smp), Mockito.anyList());

        String result = smpManagementTask.synchronizeDNSRecordsForSMP(smp);

        assertThat(result, containsString("Action [SYNC_DNS] executed on ["+participantCount+"] participants for SMP"));
        Mockito.verify(dnsClientService, Mockito.times(1)).synchronizeDNSRecordsForSMP(Mockito.eq(smp), Mockito.anyList());
    }

}
