/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.entity;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import org.hibernate.Session;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;


/**
 * Thanks to  Vlad Mihalcea
 */
class EntityEqualityHashCheckTest extends AbstractJUnit5Test {


    @Autowired
    EntityManagerFactory entityManagerFactory;


    @Test
    void certificateEntityEqualityConsistencyTest() {
        // given - when
        CertificateEntity entity = new CertificateEntity();
        entity.setCertificateId("CN=SMP_TEST_ID_CERTIFICATE,O=DG-DIGIT,C=BE:0000000000000000000000000123ABCD");
        entity.setValidFrom(Calendar.getInstance());
        entity.setValidTo(Calendar.getInstance());

        // then
        assertEqualityConsistency(entity);
    }

    @Test
    void dnsRecordEntityEqualityConsistencyTest() {
        // given - when
        DNSRecordEntity bdsmlEntity = new DNSRecordEntity();
        bdsmlEntity.setName("smp.ec.europa.eu");
        bdsmlEntity.setType("naptr");
        bdsmlEntity.setValue("test.ec.europa.eu");
        bdsmlEntity.setService("test");
        bdsmlEntity.setDNSZone("ec.europa.eu");

        // then
        assertEqualityConsistency(bdsmlEntity);
    }

    @Test
    void allowedWildcardEntityEqualityConsistencyTest() throws Exception {
        //GIVEN - WHEN
        CertificateEntity certificateEntity = getEntityById(CertificateEntity.class, (long) 4);
        assertNotNull(certificateEntity);
        AllowedWildcardEntity entity = new AllowedWildcardEntity();
        entity.setCertificate(certificateEntity);
        entity.setScheme(UUID.randomUUID().toString());

        // then
        assertEqualityConsistency(entity);
    }

    @Test
    void certificateDomainEntityEqualityConsistencyTest() throws Exception {
        //GIVEN - WHEN
        SubdomainEntity subdomainEntity = getEntityById(SubdomainEntity.class, (long) 1);
        assertNotNull(subdomainEntity);

        CertificateDomainEntity entity = new CertificateDomainEntity();
        entity.setCertificate(UUID.randomUUID().toString());
        entity.setSubdomain(subdomainEntity);

        // then
        assertEqualityConsistency(entity);
    }

    @Test
    void configurationEntityEqualityConsistencyTest() throws Exception {
        //GIVEN - WHEN
        ConfigurationEntity entity = new ConfigurationEntity();
        entity.setProperty("my.nice.property");
        entity.setValue("new value");

        // then
        assertEqualityConsistency(entity);
    }

    @Test
    void subdomainEntityEntityEqualityConsistencyTest() throws Exception {
        //GIVEN - WHEN

        SubdomainEntity entity = new SubdomainEntity();
        entity.setDnsRecordTypes("all");
        entity.setDnsZone("acc.ec.int.eu");
        entity.setParticipantIdRegexp("*");
        entity.setSmpUrlSchemas("all");
        entity.setSubdomainName("test");

        // then
        assertEqualityConsistency(entity);
    }


    private <T> T getEntityById(Class<T> clazz, Object id) {
        EntityManager entityManager = null;
        T entity = null;
        try {
            entityManager = entityManagerFactory.createEntityManager();
            entity = entityManager.find(clazz, id);
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }
        return entity;

    }


    /*
     * Method was heavily inspired by Vlad Mihalcea blog
     * https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
     */

    void assertEqualityConsistency(AbstractEntity entity) {
        Set<AbstractEntity> tuples = new HashSet<>();
        assertFalse(tuples.contains(entity));
        tuples.add(entity);
        assertTrue(tuples.contains(entity));

        doInJPA(entityManager -> {
            entityManager.persist(entity);
            entityManager.flush();
            assertTrue(tuples.contains(entity), "The entity is not found in the Set after it's persisted.");
            return null;
        });

        assertTrue(tuples.contains(entity));

        doInJPA(entityManager -> {
            AbstractEntity _entity = entityManager.merge(entity);
            assertTrue(tuples.contains(_entity), "The entity is not found in the Set after it's merged.");
            return _entity;
        });

        doInJPA(entityManager -> {
            entityManager.unwrap(Session.class).update(entity);
            assertTrue(tuples.contains(entity), "The entity is not found in the Set after it's reattached.");
            return null;
        });

        doInJPA(entityManager -> {
            AbstractEntity _entity = entityManager.find(entity.getClass(), entity.getId());
            assertTrue(tuples.contains(_entity), "The entity is not found in the Set after it's loaded in a subsequent Persistence Context.");
            return _entity;
        });

        doInJPA(entityManager -> {
            AbstractEntity _entity = entityManager.getReference(entity.getClass(), entity.getId());
            assertTrue(tuples.contains(_entity), "The entity is not in the Set found after it's loaded as a Proxy in an other Persistence Context.");
            return _entity;
        });

        AbstractEntity deletedEntity = doInJPA(entityManager -> {
            AbstractEntity _entity = entityManager.getReference(entity.getClass(), entity.getId());
            entityManager.remove(_entity);
            return _entity;
        });

        assertTrue(tuples.contains(deletedEntity), "The entity is found in not the Set even after it's deleted.");
    }


    protected AbstractEntity doInJPA(JPATransactionFunction<AbstractEntity> function) {
        AbstractEntity result = null;
        EntityManager entityManager = null;
        EntityTransaction txn = null;
        try {
            entityManager = entityManagerFactory.createEntityManager();
            function.beforeTransactionCompletion();
            txn = entityManager.getTransaction();
            txn.begin();
            result = function.apply(entityManager);
            if (!txn.getRollbackOnly()) {
                txn.commit();
            } else {
                try {
                    txn.rollback();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Throwable t) {
            if (txn != null && txn.isActive()) {
                try {
                    txn.rollback();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            throw t;
        } finally {
            function.afterTransactionCompletion();
            if (entityManager != null) {
                entityManager.close();
            }
        }
        return result;
    }


}
