/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.ws.soap;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.bo.MigrationRecordBO;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.dao.IMigrationDAO;
import eu.europa.ec.bdmsl.security.ICertificateAuthentication;
import eu.europa.ec.bdmsl.security.UnsecureAuthentication;
import org.busdox.servicemetadata.locator._1.MigrationRecordType;
import org.busdox.transport.identifiers._1.ParticipantIdentifierType;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.security.Security;

import static org.junit.jupiter.api.Assertions.*;


/**
 * @author Adrien FERIAL
 * @since 16/06/2015
 */
class ManageParticipantIdentifierWSPrepareToMigrateTest extends AbstractJUnit5Test {
    @Autowired
    private IManageParticipantIdentifierWS manageParticipantIdentifierWS;

    @Autowired
    private IMigrationDAO migrationDAO;

    @Autowired
    private ICertificateAuthentication customAuthentication;

    @BeforeEach
    void before() throws TechnicalException {
        UnsecureAuthentication authentication = new UnsecureAuthentication();
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    @Test
    void testPrepareToMigrateEmpty() throws Exception {
        MigrationRecordType migrationRecordType = new MigrationRecordType();
        assertThrows(BadRequestFault.class, () -> manageParticipantIdentifierWS.prepareToMigrate(migrationRecordType));
    }

    @Test
    void testPrepareToMigrateNull() throws Exception {
        assertThrows(BadRequestFault.class, () -> manageParticipantIdentifierWS.prepareToMigrate(null));
    }

    @Test
    void testPrepareToMigrateParticipantWithSMPNotCreatedByUser() throws Exception {
        MigrationRecordType migrationRecordType = createMigrationRecord();
        migrationRecordType.getParticipantIdentifier().setValue("0007:123456789");
        migrationRecordType.setServiceMetadataPublisherID("found");
        assertThrows(UnauthorizedFault.class, () -> manageParticipantIdentifierWS.prepareToMigrate(migrationRecordType));
    }

    @Test
    void testPrepareToMigrateSMPNotExist() throws Exception {
        MigrationRecordType migrationRecordType = createMigrationRecord();
        migrationRecordType.getParticipantIdentifier().setValue("0007:123456789");
        migrationRecordType.setServiceMetadataPublisherID("NotFound");
        assertThrows(NotFoundFault.class, () -> manageParticipantIdentifierWS.prepareToMigrate(migrationRecordType));
    }

    @Test
    void testPrepareToMigrateParticipantNotExist() throws Exception {
        MigrationRecordType migrationRecordType = createMigrationRecord();
        migrationRecordType.getParticipantIdentifier().setValue("0007:notAlreadyExist");
        migrationRecordType.setServiceMetadataPublisherID("foundUnsecure");
        assertThrows(NotFoundFault.class, () -> manageParticipantIdentifierWS.prepareToMigrate(migrationRecordType));
    }

    @Test
    void testPrepareToMigrateParticipantCreateOk() throws Exception {
        MigrationRecordType migrationRecordType = createMigrationRecord();
        migrationRecordType.setServiceMetadataPublisherID("foundUnsecure");
        migrationRecordType.getParticipantIdentifier().setValue("0009:123456789");
        manageParticipantIdentifierWS.prepareToMigrate(migrationRecordType);

        // Finally we verify that the migrationRecord exists
        MigrationRecordBO migrationRecordBO = new MigrationRecordBO();
        migrationRecordBO.setOldSmpId(migrationRecordType.getServiceMetadataPublisherID());
        migrationRecordBO.setParticipantId(migrationRecordType.getParticipantIdentifier().getValue());
        migrationRecordBO.setScheme(migrationRecordType.getParticipantIdentifier().getScheme());
        MigrationRecordBO found = migrationDAO.findNonMigratedRecord(migrationRecordBO);
        assertNotNull(found);
    }

    @Test
    void testPrepareToMigrateMigrationCodeTooLong() throws Exception {
        MigrationRecordType migrationRecordType = createMigrationRecord();
        migrationRecordType.getParticipantIdentifier().setValue("0007:123456789");
        migrationRecordType.setServiceMetadataPublisherID("foundUnsecure");
        migrationRecordType.setMigrationKey("TooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLong");
        assertThrows(BadRequestFault.class, () -> manageParticipantIdentifierWS.prepareToMigrate(migrationRecordType));
    }

    private MigrationRecordType createMigrationRecord() {
        MigrationRecordType migrationRecordType = new MigrationRecordType();
        migrationRecordType.setMigrationKey("AB12aa#$");
        ParticipantIdentifierType participantIdentifierType = new ParticipantIdentifierType();
        participantIdentifierType.setValue("0007:123456789");
        participantIdentifierType.setScheme("iso6523-actorid-upis");
        migrationRecordType.setParticipantIdentifier(participantIdentifierType);
        migrationRecordType.setServiceMetadataPublisherID("found");
        return migrationRecordType;
    }

    @Test
    void testPrepareToMigrateParticipantUpdateOk() throws Exception {
        // first we ensure that the migration record exists
        MigrationRecordBO migrationRecordBO = new MigrationRecordBO();
        // Ids must be case insensitive so we put them in upper case to test the case insensitivity
        String smpId = "FOUNDUNSECURE";
        migrationRecordBO.setOldSmpId(smpId);
        String participantId = "0009:223456789MIGRATETEST";
        migrationRecordBO.setParticipantId(participantId);
        migrationRecordBO.setScheme("iso6523-actorid-upis");
        MigrationRecordBO found = migrationDAO.findNonMigratedRecord(migrationRecordBO);
        assertEquals("@Df1Og2#", found.getMigrationCode());

        // update the migration key
        MigrationRecordType migrationRecordType = createMigrationRecord();
        migrationRecordType.getParticipantIdentifier().setValue(participantId);
        migrationRecordType.setServiceMetadataPublisherID(smpId);
        migrationRecordType.setMigrationKey("@_123BgBgGg");
        manageParticipantIdentifierWS.prepareToMigrate(migrationRecordType);

        // check that the migration key is updated
        found = migrationDAO.findNonMigratedRecord(migrationRecordBO);
        assertEquals("@_123BgBgGg", found.getMigrationCode());
    }

    @Test
    void testPrepareToMigrateParticipantNotLinkedToSMP() throws Exception {
        MigrationRecordType migrationRecordType = createMigrationRecord();
        migrationRecordType.getParticipantIdentifier().setValue("0009:123456789");
        migrationRecordType.setServiceMetadataPublisherID("found");
        assertThrows(UnauthorizedFault.class, () -> manageParticipantIdentifierWS.prepareToMigrate(migrationRecordType));
    }


    /**
     * Call the function PrepareToMigrate twice (but with a different Migration Key) for the same ParticipantIdentifier with a PrepareMigrationRecord (which contains the Migration Key and the Participant Identifier which is about to be migrated from one SMP to another).
     * The result should be that the Migration Key has been updated to the Migration Key of the second request
     *
     * @throws Exception
     */
    @Test
    void testPrepareToMigrateCalledTwice() throws Exception {
        MigrationRecordType migrationRecordType = createMigrationRecord();
        migrationRecordType.setServiceMetadataPublisherID("foundUnsecure");
        migrationRecordType.getParticipantIdentifier().setValue("0009:123456789");
        manageParticipantIdentifierWS.prepareToMigrate(migrationRecordType);

        // Finally we verify that the migrationRecord exists
        MigrationRecordBO migrationRecordBO = new MigrationRecordBO();
        migrationRecordBO.setOldSmpId(migrationRecordType.getServiceMetadataPublisherID());
        migrationRecordBO.setParticipantId(migrationRecordType.getParticipantIdentifier().getValue());
        migrationRecordBO.setScheme(migrationRecordType.getParticipantIdentifier().getScheme());
        MigrationRecordBO found = migrationDAO.findNonMigratedRecord(migrationRecordBO);
        assertNotNull(found);

        found = migrationDAO.findNonMigratedRecord(migrationRecordBO);
        assertEquals("AB12aa#$", found.getMigrationCode());

        migrationRecordType.setMigrationKey("AB12aa#$");
        manageParticipantIdentifierWS.prepareToMigrate(migrationRecordType);

        found = migrationDAO.findNonMigratedRecord(migrationRecordBO);
        assertEquals("AB12aa#$", found.getMigrationCode());
    }

    @Test
    void testPrepareToMigrateDifferentDomainOk() throws Exception {
        customAuthentication.blueCoatAuthentication("123456789101112");

        MigrationRecordType migrationRecordType = createMigrationRecord();
        migrationRecordType.setServiceMetadataPublisherID("SMP2016");
        migrationRecordType.getParticipantIdentifier().setValue("urn:ehealth:sw:ncpb-ics");
        migrationRecordType.getParticipantIdentifier().setScheme("ehealth-ncp-ids");
        manageParticipantIdentifierWS.prepareToMigrate(migrationRecordType);

        MigrationRecordBO migrationRecordBO = new MigrationRecordBO();
        migrationRecordBO.setOldSmpId(migrationRecordType.getServiceMetadataPublisherID());
        migrationRecordBO.setParticipantId(migrationRecordType.getParticipantIdentifier().getValue());
        migrationRecordBO.setScheme(migrationRecordType.getParticipantIdentifier().getScheme());
        MigrationRecordBO found = migrationDAO.findNonMigratedRecord(migrationRecordBO);
        assertNotNull(found);
    }

    @Test
    @Disabled("The test needs to be redesigned")
    void testPrepareToMigrateCertificateNotOk() throws Exception {
        customAuthentication.blueCoatAuthentication();

        MigrationRecordType migrationRecordType = createMigrationRecord();
        migrationRecordType.setServiceMetadataPublisherID("SMP2016");
        migrationRecordType.getParticipantIdentifier().setValue("urn:ehealth:sw:ncpb-ics");
        migrationRecordType.getParticipantIdentifier().setScheme("ehealth-ncp-ids");

        manageParticipantIdentifierWS.prepareToMigrate(migrationRecordType);
        MigrationRecordBO migrationRecordBO = new MigrationRecordBO();
        migrationRecordBO.setOldSmpId(migrationRecordType.getServiceMetadataPublisherID());
        migrationRecordBO.setParticipantId(migrationRecordType.getParticipantIdentifier().getValue());
        migrationRecordBO.setScheme(migrationRecordType.getParticipantIdentifier().getScheme());

        MigrationRecordBO found = migrationDAO.findNonMigratedRecord(migrationRecordBO);
        assertThrows(UnauthorizedFault.class, () -> {
                    assertNotNull(found);
                }
        );


    }

    @Test
    void testPrepareToMigrateParticipantWithDuplicatedKey() throws Exception {

        String participantId = "0002:12345678";
        String smpId = "SMP2017";

        // create the migration key
        MigrationRecordType migrationRecordType = createMigrationRecord();
        migrationRecordType.setServiceMetadataPublisherID(smpId);
        migrationRecordType.getParticipantIdentifier().setValue(participantId);
        migrationRecordType.getParticipantIdentifier().setScheme("domain-ncp-ids");
        manageParticipantIdentifierWS.prepareToMigrate(migrationRecordType);

        // check if migration key exists
        MigrationRecordBO migrationRecordBO = new MigrationRecordBO();
        migrationRecordBO.setOldSmpId(smpId);
        migrationRecordBO.setParticipantId(participantId);
        migrationRecordBO.setScheme(migrationRecordType.getParticipantIdentifier().getScheme());
        MigrationRecordBO found = migrationDAO.findNonMigratedRecord(migrationRecordBO);
        assertEquals("AB12aa#$", found.getMigrationCode());

        // migrate the participant in order to use and invalid this key for new migrations
        manageParticipantIdentifierWS.migrate(migrationRecordType);

        // Make sure the key is tagged as MIGRATED
        found = migrationDAO.findNonMigratedRecord(migrationRecordBO);
        assertNull(found);

        try {
            // try to create the same key
            manageParticipantIdentifierWS.prepareToMigrate(migrationRecordType);
        } catch (Exception exc) {
            assertTrue(exc.getMessage().startsWith("[ERR-106] The migration key AB12aa#$ was already used to migrate participant (domain-ncp-ids,0002:12345678)."));
            return;
        }
        fail();
    }
}




