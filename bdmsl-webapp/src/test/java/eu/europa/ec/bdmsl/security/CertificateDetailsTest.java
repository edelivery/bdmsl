/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.security;


import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import org.junit.jupiter.api.Test;


import java.util.Calendar;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Flavio SANTOS
 */
class CertificateDetailsTest extends AbstractJUnit5Test {

    @Test
    void equal() {
        CertificateDetails certificateDetails1 = new CertificateDetails();
        certificateDetails1.setCertificateId("CertificateId");
        certificateDetails1.setSubject("Subject");
        certificateDetails1.setRootCertificateDN("RootCertificateDN");
        certificateDetails1.setIssuer("issuer");
        certificateDetails1.setSerial("123456");
        Calendar date = Calendar.getInstance();
        certificateDetails1.setValidFrom(date);
        certificateDetails1.setValidTo(date);
        certificateDetails1.setPemEncoding("PemEncoding");
        certificateDetails1.setPolicyOids("PolicyOids");

        CertificateDetails certificateDetails2 = new CertificateDetails();
        certificateDetails2.setCertificateId("CertificateId");
        certificateDetails2.setSubject("Subject");
        certificateDetails2.setRootCertificateDN("RootCertificateDN");
        certificateDetails2.setIssuer("issuer");
        certificateDetails2.setSerial("123456");
        certificateDetails2.setValidFrom(date);
        certificateDetails2.setValidTo(date);
        certificateDetails2.setPemEncoding("PemEncoding");
        certificateDetails2.setPolicyOids("PolicyOids");

        assertTrue(certificateDetails1.equals(certificateDetails2));
    }

    @Test
    void notEqual() {
        CertificateDetails certificateDetails1 = new CertificateDetails();
        certificateDetails1.setCertificateId("CertificateId");
        certificateDetails1.setSubject("Subject 1");
        certificateDetails1.setRootCertificateDN("RootCertificateDN");
        certificateDetails1.setIssuer("issuer");
        certificateDetails1.setSerial("123456");
        Calendar date = Calendar.getInstance();
        certificateDetails1.setValidFrom(date);
        certificateDetails1.setValidTo(date);
        certificateDetails1.setPemEncoding("PemEncoding");
        certificateDetails1.setPolicyOids("PolicyOids");

        CertificateDetails certificateDetails2 = new CertificateDetails();
        certificateDetails2.setCertificateId("CertificateId");
        certificateDetails2.setSubject("Subject");
        certificateDetails2.setRootCertificateDN("RootCertificateDN");
        certificateDetails2.setIssuer("issuer");
        certificateDetails2.setSerial("123456");
        certificateDetails2.setValidFrom(date);
        certificateDetails2.setValidTo(date);
        certificateDetails2.setPemEncoding("PemEncoding");
        certificateDetails2.setPolicyOids("PolicyOids");

        assertFalse(certificateDetails1.equals(certificateDetails2));
    }
}

