/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.presentation.controller;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.business.IConfigurationBusiness;
import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import eu.europa.ec.bdmsl.common.exception.ConfigurationException;
import eu.europa.ec.bdmsl.common.exception.GenericTechnicalException;
import eu.europa.ec.bdmsl.presentation.ListDNSController;
import eu.europa.ec.bdmsl.presentation.SMLSearchParams;
import eu.europa.ec.bdmsl.service.IBDMSLService;
import eu.europa.ec.bdmsl.service.dns.IDnsClientService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.xbill.DNS.*;
import org.xbill.DNS.Record;

import java.net.InetAddress;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.spy;

/**
 * @author Flavio SANTOS
 */
class ListDNSControllerTest extends AbstractJUnit5Test {

    @Autowired
    private ListDNSController listDNSController;

    @Autowired
    private IDnsClientService dnsClientService;

    IConfigurationBusiness cf;

    @BeforeEach
    void before() throws Exception {
        dnsClientService = spy(dnsClientService);
        ReflectionTestUtils.setField(listDNSController, "dnsClientService", dnsClientService);
        ReflectionTestUtils.setField(listDNSController, "bdmslService", bdmslService);

        cf = spy((IConfigurationBusiness) ReflectionTestUtils.getField(listDNSController, "configurationBusiness"));
        ReflectionTestUtils.setField(listDNSController, "configurationBusiness", cf);

    }

    @Test
    void testListAllDomains() throws Exception {
        //GIVEN
        Model model = new ExtendedModelMap();
        Mockito.doReturn(new ArrayList<Record>()).when(dnsClientService).getAllRecords(anyString());
        Mockito.doReturn(getRecords()).when(dnsClientService).getAllRecords("sea.acc.edelivery.tech.ec.europa.eu.");
        Mockito.doReturn(new ArrayList<Record>()).when(dnsClientService).getAllRecords("acc.edelivery.tech.ec.europa.eu.");
        Mockito.doReturn(new ArrayList<Record>()).when(dnsClientService).getAllRecords("edelivery.tech.ec.europa.eu.");

        //WHEN
        listDNSController.listDNS(model);
        Map<String, Collection<Record>> recordMap = (Map<String, Collection<Record>>) model.asMap().get("recordMap");

        //THEN
        assertNotNull(recordMap);
        assertEquals(5, recordMap.get("sea.acc.edelivery.tech.ec.europa.eu.").size());
        assertEquals(0, recordMap.get("edelivery.tech.ec.europa.eu.").size());
        assertEquals(0, recordMap.get("acc.edelivery.tech.ec.europa.eu.").size());
    }

    @Test
    void testListAllDomainsEmptyDNS() throws Exception {
        //GIVEN
        Model model = new ExtendedModelMap();
        Mockito.doReturn(new ArrayList<Record>()).when(dnsClientService).getAllRecords(any(String.class));

        //WHEN
        listDNSController.listDNS(model);
        Map<String, Collection<Record>> recordMap = (Map<String, Collection<Record>>) model.asMap().get("recordMap");

        //THEN
        assertNotNull(recordMap);
        assertEquals(0, ((Collection<Record>) recordMap.get("sea.acc.edelivery.tech.ec.europa.eu.")).size());
        assertEquals(0, ((Collection<Record>) recordMap.get("edelivery.tech.ec.europa.eu.")).size());
        assertEquals(0, ((Collection<Record>) recordMap.get("acc.edelivery.tech.ec.europa.eu.")).size());
    }

    @Test
    void testListAllDomainsException1() throws Exception {
        //GIVEN
        Model model = new ExtendedModelMap();
        IBDMSLService bdmslService = Mockito.mock(IBDMSLService.class);
        ReflectionTestUtils.setField(listDNSController, "bdmslService", bdmslService);
        Mockito.doThrow(new GenericTechnicalException("Error")).when(bdmslService).findAllSubdomains();

        //WHEN
        listDNSController.listDNS(model);

        //THEN
        Map<String, Collection<Record>> recordMap = (Map<String, Collection<Record>>) model.asMap().get("recordMap");
        assertNull(recordMap);
    }

    @Test
    void testListAllDomainsException2() throws Exception {
        //GIVEN
        Model model = new ExtendedModelMap();
        ReflectionTestUtils.setField(listDNSController, "dnsClientService", dnsClientService);
        Mockito.doThrow(new GenericTechnicalException("Error")).when(dnsClientService).getDnsZoneNameForSubDomain(any(SubdomainBO.class));

        //WHEN
        assertThrows(ConfigurationException.class, () -> listDNSController.listDNS(model));

        //THEN
        Map<String, Collection<Record>> recordMap = (Map<String, Collection<Record>>) model.asMap().get("recordMap");
        assertNull(recordMap);
    }

    @Test
    void testSearchPage() {
        //GIVEN
        Model model = new ExtendedModelMap();
        //WHEN
        String val = listDNSController.search(model);
        //THEN
        assertEquals("search", val);
    }

    @Test
    @Disabled("Fail on CITNET")
    void testSearchResultPageNoDomainConfiguration() {
        //GIVEN
        ModelMap model = new ExtendedModelMap();
        SMLSearchParams searchParams = new SMLSearchParams();
        searchParams.setSearchType("domain");
        searchParams.setDomainName("localhost-NotExist");
        //WHEN
        String val = listDNSController.searchResult(searchParams, null, model);
        //THEN
        String errMsg = (String) model.get("error");
        assertEquals("searchError", val);
        assertEquals("Error occurred while resolving domain: 'localhost-NotExist'", errMsg);
    }

    @Test
    void testSearchResultNotEnabled() {
        //GIVEN
        ModelMap model = new ExtendedModelMap();
        SMLSearchParams searchParams = new SMLSearchParams();
        searchParams.setSearchType("domain");
        searchParams.setDomainName("localhost");
        Mockito.doReturn(false).when(cf).isDNSEnabled();
        //WHEN
        String val = listDNSController.searchResult(searchParams, null, model);
        //THEN
        String errMsg = (String) model.get("error");
        assertEquals("searchError", val);
        assertEquals("DNS is not enabled!", errMsg);
    }

    @Test
    void testSearchResultNullDomain() {
        //GIVEN
        ModelMap model = new ExtendedModelMap();
        SMLSearchParams searchParams = new SMLSearchParams();
        searchParams.setSearchType("domain");
        searchParams.setDomainName("");

        //WHEN
        String val = listDNSController.searchResult(searchParams, null, model);
        //THEN
        String errMsg = (String) model.get("error");
        assertEquals("searchError", val);
        assertEquals("DNS domain must not be null!", errMsg);
    }

    @Test
    void testSearchResultDomain() throws Exception {
        //GIVEN
        ModelMap model = new ExtendedModelMap();
        SMLSearchParams searchParams = new SMLSearchParams();
        searchParams.setSearchType("domain");
        searchParams.setDomainName("localhost");
        List<Record> testRecords = getRecords();
        ReflectionTestUtils.setField(listDNSController, "dnsClientService", dnsClientService);
        Mockito.doReturn(testRecords).when(dnsClientService).lookupAny("localhost");

        //WHEN
        String val = listDNSController.searchResult(searchParams, null, model);
        //THEN
        Collection<Record> lst = (Collection<Record>) model.get("recordList");
        assertEquals("searchResult", val);
        assertEquals(testRecords.size(), lst.size());
    }

    @Test
    void testSearchResultIdentifier() throws Exception {
        //GIVEN
        ModelMap model = new ExtendedModelMap();
        SMLSearchParams searchParams = new SMLSearchParams();
        searchParams.setSearchType("identifier");
        searchParams.setIdentifier("myidentifier");
        searchParams.setScheme("myscheme");
        List<Record> testRecords = getRecords();
        Map<String, Collection<Record>> mp = new HashMap<>();
        mp.put("test", testRecords);
        mp.put("test2", Collections.emptyList());
        ReflectionTestUtils.setField(listDNSController, "dnsClientService", dnsClientService);
        Mockito.doReturn(mp).when(dnsClientService).getAllParticipantRecords("myidentifier", "myscheme");

        //WHEN
        String val = listDNSController.searchResult(searchParams, null, model);
        //THEN
        Map<String, Collection<Record>> map = (Map<String, Collection<Record>>) model.get("recordMap");
        assertEquals("searchResult", val);
        assertEquals(2, map.size());
        assertTrue(map.containsKey("test"));
        assertTrue(map.containsKey("test2"));
        assertEquals(testRecords.size(), map.get("test").size());
        assertEquals(0, map.get("test2").size());
    }

    List<Record> getRecords() throws Exception {
        List<Record> records = new ArrayList<Record>() {
            {
                add(new CNAMERecord(new Name("B-40ffaa7d142dbc3f620da712bf95da37.iso6523-actorid-upis.sea.acc.edelivery.tech.ec.europa.eu."), DClass.IN, 60, new Name("smp.publisher.test.com.eu" + ".")));
                add(new CNAMERecord(new Name("B-40ffaa7d142dbc3f620da712bf95da37.iso6523-actorid-upis.sea.acc.edelivery.tech.ec.europa.eu."), DClass.IN, 60, new Name("\"smp.publisher.test.com.eu" + ".")));

                add(new NAPTRRecord(new Name("QCVBIAWN2LUPPDWW7DVX4XZHIM3RXN35JV7RBID5FKKKYZSYBS5A.busdox-actorid-upis.sea.acc.edelivery.tech.ec.europa.eu."), DClass.IN, 60, 100, 10, "U", "Meta:SMP", "!^.*$!smp.publisher.test.com.eu!", Name.fromString(".")));
                add(new NAPTRRecord(new Name("VXZM7SAKBMBJUOKFL5LHUN553KFMVGKKILSEMAWTEBDGIXSLF25A.busdox-actorid-upis.sea.acc.edelivery.tech.ec.europa.eu."), DClass.IN, 60, 100, 10, "U", "Meta:SMP", "!^.*$!smp.publisher.test.com.eu!", Name.fromString(".")));

                byte[] ipAddressBytes = Address.toByteArray("127.0.0.1", Address.IPv4);
                add(new ARecord(new Name("smp.publisher.test.com.eu."), DClass.IN, 60, InetAddress.getByAddress(ipAddressBytes)));
            }
        };
        return records;
    }
}
