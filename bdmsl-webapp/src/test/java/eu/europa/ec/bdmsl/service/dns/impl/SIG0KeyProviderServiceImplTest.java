/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */

/**
 * @author Flavio SANTOS
 */
package eu.europa.ec.bdmsl.service.dns.impl;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.exception.SIG0Exception;
import eu.europa.ec.bdmsl.test.CommonTestUtils;
import eu.europa.ec.bdmsl.service.dns.ISIG0KeyProviderService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.File;
import java.security.PrivateKey;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.spy;

class SIG0KeyProviderServiceImplTest extends AbstractJUnit5Test {

    @Autowired
    private ISIG0KeyProviderService sig0KeyProviderService;

    @BeforeEach
    void before() {
        ReflectionTestUtils.setField(sig0KeyProviderService, "configurationBusiness", configurationBusiness);
    }

    @Test
    void testGetPrivateSIG0KeyForPrivateKeyOk() throws Exception {
        // test get file with for without folder separator
        String folderDir = targetDirectory.toFile().getAbsolutePath();
        if (!folderDir.endsWith(File.separator)) {
            folderDir += File.separator;
        }
        Mockito.doReturn(folderDir).when(configurationBusiness).getConfigurationFolder();
        Mockito.doReturn("Kexample.com.+003+25813.private").when(configurationBusiness).getDNSSig0KeyFilename();


        PrivateKey privateKey = sig0KeyProviderService.getPrivateSIG0Key();
        assertNotNull(privateKey);


        // remove last folder separator
        folderDir = folderDir.substring(0, folderDir.length() - 1);
        Mockito.doReturn(folderDir).when(configurationBusiness).getConfigurationFolder();
        Mockito.doReturn("Kexample.com.+003+25813.private").when(configurationBusiness).getDNSSig0KeyFilename();

        privateKey = sig0KeyProviderService.getPrivateSIG0Key();
        assertNotNull(privateKey);
    }


    @Test
    void testGetPrivateSIG0KeyForPrivateKeyNotOk() {
        Mockito.doReturn("Kexample.com.+003+25820.private").when(configurationBusiness).getDNSSig0KeyFilename();
        assertThrows(RuntimeException.class, () -> sig0KeyProviderService.getPrivateSIG0Key());
    }

    @Test
    void testGetPrivateSIG0KeyForFileNotFound() {
        Mockito.doReturn("Kexample.com.+003+258355.private").when(configurationBusiness).getDNSSig0KeyFilename();
        assertThrows(SIG0Exception.class, () -> sig0KeyProviderService.getPrivateSIG0Key());
    }

    @Test
    void testGetPrivateSIG0KeyForException() throws Exception {
        final SIG0KeyProviderServiceImpl sig0KeyProviderServiceImplMock = spy((SIG0KeyProviderServiceImpl) CommonTestUtils.getTargetObject(sig0KeyProviderService));
        Mockito.doReturn("Kexample.com.+003+25813.private").when(configurationBusiness).getDNSSig0KeyFilename();

        Mockito.doThrow(new RuntimeException("RuntimeException( Dummy Exception")).when(sig0KeyProviderServiceImplMock).getStream(anyString());

        assertThrows(RuntimeException.class, sig0KeyProviderServiceImplMock::getPrivateSIG0Key);
    }
}
