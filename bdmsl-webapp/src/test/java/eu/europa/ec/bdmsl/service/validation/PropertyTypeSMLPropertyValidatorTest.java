/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.validation;

import eu.europa.ec.bdmsl.common.enums.SMLPropertyEnum;
import eu.europa.ec.bdmsl.common.enums.SMLPropertyTypeEnum;
import eu.europa.ec.bdmsl.common.exception.BadConfigurationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.File;

import static eu.europa.ec.bdmsl.common.enums.SMLPropertyTypeEnum.*;

/**
 * @since 5.0
 * @author Sebastian-Ion TINCU
 */
public class PropertyTypeSMLPropertyValidatorTest {

    private PropertyTypeSMLPropertyValidator validator = new PropertyTypeSMLPropertyValidator();

    private static Object[] testValidValues() {
        return new Object[][]{
                {STRING, "this is a string", true},
                {INTEGER, "1345", true},
                {BOOLEAN, "true", true},
                {BOOLEAN, "false", true},
                {BOOLEAN, "fALse", true},
                {REGEXP, ".*", true},
                {EMAIL, "test@mail.com", true},
                {EMAIL, "test@MAIL.com", true},
                {EMAIL, "tEST@mail.com", true},
                {EMAIL, "DIGIT-CEF-EDELIVERY-SMP-SML@ec.europa.com", true},
                {FILENAME, "myfilename.txt", true},
                {PATH, "./", true},
        };
    }

    private static Object[] testInvalidValues() {
        return new Object[][]{
                {INTEGER, " 1e345", false},
                {BOOLEAN, "fale ", false},
                {REGEXP, ".*(**]", false},
        };
    }

    @ParameterizedTest
    @MethodSource("testValidValues")
    void testValidPropertyType(SMLPropertyTypeEnum propertyType, String value) {
        Assertions.assertDoesNotThrow(() -> validator.validatePropertyType(propertyType, value, new File("./target")));
    }

    @ParameterizedTest
    @MethodSource("testInvalidValues")
    void testInvalidPropertyType(SMLPropertyTypeEnum propertyType, String value) {
        Assertions.assertThrows(BadConfigurationException.class, () -> validator.validatePropertyType(propertyType, value, new File("./target")));
    }

    @ParameterizedTest
    @EnumSource(SMLPropertyEnum.class)
    public void testDefaultValuesMustBeValid(SMLPropertyEnum prop) {
        File rootFolder = new File("./target");

        Assertions.assertDoesNotThrow(() -> validator.validate(prop, prop.getDefValue(), rootFolder));
    }
}