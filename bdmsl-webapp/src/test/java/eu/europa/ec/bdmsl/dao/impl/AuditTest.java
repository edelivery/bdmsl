/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.impl;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.dao.entity.*;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.PersistenceUnit;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;


/**
 * Purpose of class is to test all Audit classes and  methods with database.
 *
 * @author Joze RIHTARSIC
 * @since 4.1
 */
class AuditTest extends AbstractJUnit5Test {

    // because envers creates audit on commit we user PersistenceUnit to control commit...
    // (instead of  PersistenceContext and transaction annotations... )
    @PersistenceUnit
    EntityManagerFactory emf;

    @Test
    void testClassesForAudit() {
        EntityManager em = emf.createEntityManager();
        try {
            AuditReader ar = AuditReaderFactory.get(em);
            assertTrue(ar.isEntityClassAudited(AllowedWildcardEntity.class));
            assertTrue(ar.isEntityClassAudited(CertificateDomainEntity.class));
            assertTrue(ar.isEntityClassAudited(CertificateEntity.class));
            assertTrue(ar.isEntityClassAudited(ConfigurationEntity.class));
            assertTrue(ar.isEntityClassAudited(MigrateEntity.class));
            assertTrue(ar.isEntityClassAudited(ParticipantIdentifierEntity.class));
            assertTrue(ar.isEntityClassAudited(DNSRecordEntity.class));
        } finally {
            em.close();
        }
    }

    @Test
    void testDNSRecordEntity() {

        DNSRecordEntity bdsmlEntity = new DNSRecordEntity();
        bdsmlEntity.setName("setName");
        bdsmlEntity.setType("setType");
        bdsmlEntity.setValue("setValue");
        bdsmlEntity.setService("setService");
        bdsmlEntity.setDNSZone("dnszone");

        Map<String, Object> alterVal = new HashMap<>();
        alterVal.put("name", UUID.randomUUID().toString());
        alterVal.put("type", "NewType");
        alterVal.put("value", UUID.randomUUID().toString());
        alterVal.put("service", "Newservice");
        //  alterVal.put("DNSZone", UUID.randomUUID().toString());
        testAuditEntity(bdsmlEntity, alterVal);
    }


    @Test
    void testAuditSubdomainEntity() {

        SubdomainEntity bdsmlEntity = new SubdomainEntity();
        bdsmlEntity.setSmpUrlSchemas("setSmpUrlSchemas");
        bdsmlEntity.setDescription("setDescription");
        bdsmlEntity.setDnsRecordTypes("setDnsRecordTypes");
        bdsmlEntity.setDnsZone("setDnsZone");
        bdsmlEntity.setParticipantIdRegexp("setParticipantIdRegexp");
        bdsmlEntity.setSubdomainName("setSubdomainName");


        Map<String, Object> alterVal = new HashMap<>();
        alterVal.put("smpUrlSchemas", UUID.randomUUID().toString());
        alterVal.put("description", UUID.randomUUID().toString());
        alterVal.put("dnsRecordTypes", UUID.randomUUID().toString());
        alterVal.put("participantIdRegexp", UUID.randomUUID().toString());
        alterVal.put("subdomainName", UUID.randomUUID().toString());


        testAuditEntity(bdsmlEntity, alterVal);
    }

    @Test
    void testAllowedWildcardEntity() {
        // given
        EntityManager em = emf.createEntityManager();
        CertificateEntity c = em.find(CertificateEntity.class, 1l);
        assertNotNull(c);

        AllowedWildcardEntity bdsmlEntity = new AllowedWildcardEntity();
        bdsmlEntity.setScheme("testscheme");
        bdsmlEntity.setCertificate(c);

        // cannot change properties because they are key... only delete...
        Map<String, Object> alterVal = new HashMap<>();
        em.close();

        testAuditEntity(bdsmlEntity, alterVal);
    }

    @Test
    void testCertificateDomainEntity() {
        // given
        EntityManager em = emf.createEntityManager();
        SubdomainEntity subdomainEntity = em.find(SubdomainEntity.class, 1L);
        SubdomainEntity subdomainEntity2 = em.find(SubdomainEntity.class, 2L);
        assertNotNull(subdomainEntity);

        // given
        CertificateDomainEntity bdsmlEntity = new CertificateDomainEntity();
        bdsmlEntity.setCertificate("CN=SMP_TEST_CHANGE_CERTIFICATE_6,O=DG-DIGIT,C=BE");
        bdsmlEntity.setSubdomain(subdomainEntity);
        bdsmlEntity.setRootCA(true);
        bdsmlEntity.setCrl("http://mycrl.crl");

        // cannot change properties because they are key... only delete...
        Map<String, Object> alterVal = new HashMap<>();
        alterVal.put("subdomain", subdomainEntity2);
        alterVal.put("crl", "ldap://" + UUID.randomUUID().toString());
        em.close();
        testAuditEntity(bdsmlEntity, alterVal);
    }

    @Test
    void testMigrateEntity() {

        // given
        MigrateEntity bdsmlEntity = new MigrateEntity();
        bdsmlEntity.setParticipantId(UUID.randomUUID().toString());
        bdsmlEntity.setScheme(UUID.randomUUID().toString());
        bdsmlEntity.setMigrationKey(UUID.randomUUID().toString());
        bdsmlEntity.setNewSmpId(UUID.randomUUID().toString());
        bdsmlEntity.setOldSmpId(UUID.randomUUID().toString());


        // cannot change properties because they are key... only delete...
        Map<String, Object> alterVal = new HashMap<>();
        alterVal.put("newSmpId", UUID.randomUUID().toString());
        alterVal.put("oldSmpId", UUID.randomUUID().toString());
        testAuditEntity(bdsmlEntity, alterVal);
    }

    @Test
    void testParticipantIdentifierEntity() {
        EntityManager em = emf.createEntityManager();
        SmpEntity smpEntity = em.find(SmpEntity.class, 1L);
        SmpEntity smpEntity2 = em.find(SmpEntity.class, 2L);
        //
        ParticipantIdentifierEntity bdsmlEntity = new ParticipantIdentifierEntity();
        bdsmlEntity.setParticipantId(UUID.randomUUID().toString());
        bdsmlEntity.setScheme(UUID.randomUUID().toString());
        bdsmlEntity.setSmp(smpEntity);


        // cannot change properties because they are key... only delete...
        Map<String, Object> alterVal = new HashMap<>();
        alterVal.put("smp", smpEntity2);
        em.close();

        testAuditEntity(bdsmlEntity, alterVal);
    }

    @Test
    void testSMPEntity() {
        EntityManager em = emf.createEntityManager();
        CertificateEntity certificateEntity = em.find(CertificateEntity.class, 1L);
        CertificateEntity CertificateEntit2 = em.find(CertificateEntity.class, 2L);
        SubdomainEntity subdomainEntity = em.find(SubdomainEntity.class, 1L);
        SubdomainEntity subdomainEntity2 = em.find(SubdomainEntity.class, 2L);
        //
        SmpEntity bdsmlEntity = new SmpEntity();
        bdsmlEntity.setSmpId(UUID.randomUUID().toString());
        bdsmlEntity.setCertificate(certificateEntity);
        bdsmlEntity.setSubdomain(subdomainEntity);
        bdsmlEntity.setEndpointLogicalAddress(UUID.randomUUID().toString());
        bdsmlEntity.setEndpointPhysicalAddress(UUID.randomUUID().toString());


        // cannot change properties because they are key... only delete...
        Map<String, Object> alterVal = new HashMap<>();
        alterVal.put("certificate", CertificateEntit2);
        alterVal.put("subdomain", subdomainEntity2);
        alterVal.put("endpointLogicalAddress", UUID.randomUUID().toString());
        alterVal.put("endpointPhysicalAddress", UUID.randomUUID().toString());
        em.close();

        testAuditEntity(bdsmlEntity, alterVal);
    }

    @Test
    void testCertificateEntity() {

        // given
        CertificateEntity bdsmlEntity = new CertificateEntity();
        bdsmlEntity.setCertificateId("CN=SMP_TEST_CHANGE_CERTIFICATE_6,O=DG-DIGIT-" + UUID.randomUUID().toString() + ",C=BE:00000000000000000000000001234567");
        bdsmlEntity.setValidFrom(Calendar.getInstance());
        bdsmlEntity.setValidTo(Calendar.getInstance());
        bdsmlEntity.setPemEncoding("test");

        // cannot change properties because they are key... only delete...
        Map<String, Object> alterVal = new HashMap<>();
        alterVal.put("pemEncoding", UUID.randomUUID().toString());

        testAuditEntity(bdsmlEntity, alterVal);
    }


    /**
     * Method updates value in Map, then checks if revision increased. Last testi in removing the entity.
     *
     * @param entity
     * @param alterValues
     */
    private void testAuditEntity(AbstractEntity entity, Map<String, Object> alterValues) {
        testAuditSubEntity(entity, entity, alterValues);
    }

    /**
     * Method tests altering of subentity parameters. Update and remove is done on master entity
     *
     * @param entity
     * @param subEntity
     * @param alterValues
     */
    private void testAuditSubEntity(AbstractEntity entity, AbstractEntity subEntity, Map<String, Object> alterValues) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            AuditReader ar = AuditReaderFactory.get(em);
            // persist
            persist(em, entity);
            Object dbId = subEntity.getId();

            int iRevSize = ar.getRevisions(subEntity.getClass(), dbId).size();
            // update
            if (alterValues != null && !alterValues.isEmpty()) { // set value to detail
                alterValues.forEach((prop, val) -> {
                    ReflectionTestUtils.invokeSetterMethod(subEntity, prop, val, val.getClass());
                });
                update(em, entity); // master
                assertEquals(++iRevSize, ar.getRevisions(subEntity.getClass(), dbId).size());
            }

            // remove master
            remove(em, entity.getClass(), dbId);
            assertEquals(++iRevSize, ar.getRevisions(subEntity.getClass(), dbId).size());
        } finally {
            em.close();
        }

    }

    private void persist(EntityManager em, Object dbEnetity) {
        em.getTransaction().begin();
        em.persist(dbEnetity);
        em.getTransaction().commit();
    }

    private void update(EntityManager em, Object dbEntity) {
        em.getTransaction().begin();
        em.merge(dbEntity);
        em.getTransaction().commit();
    }

    private void remove(EntityManager em, Class cls, Object dbId) {
        em.getTransaction().begin();
        // get attached reference to delete it
        Object dbEntity = em.getReference(cls, dbId);

        em.remove(dbEntity);
        em.getTransaction().commit();
    }
}
