/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.business.impl;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.business.IReportBusiness;
import eu.europa.ec.bdmsl.common.reports.SMPExpiredCertificatesReport;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.io.StringWriter;

import static org.hamcrest.CoreMatchers.containsStringIgnoringCase;
import static org.hamcrest.MatcherAssert.assertThat;

class ReportBusinessImplTest extends AbstractJUnit5Test {

    @Autowired
    IReportBusiness testInstance;

    @Test
    void getSMPsWithExpiredCertificateReport() throws IOException {

        SMPExpiredCertificatesReport report = testInstance.getSMPsWithExpiredCertificateReport("TestHostname", "TestServerName");
        StringWriter stringWriter = new StringWriter();

        report.serialize(stringWriter);
        String reportString = stringWriter.toString();

        assertThat(reportString,
                containsStringIgnoringCase("There are 2 SMP(s) with expired certificate"));
        assertThat(reportString,
                containsStringIgnoringCase("Server Local Host: TestHostname"));
        assertThat(reportString,
                containsStringIgnoringCase("Server Name: TestServerName"));
        assertThat(reportString,
                containsStringIgnoringCase("SMP-EXPIRED-CERT-01"));
        assertThat(reportString,
                containsStringIgnoringCase("SMP-EXPIRED-CERT-02"));

    }
}
