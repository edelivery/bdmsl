/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.dns.impl;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 * @author Flavio SANTOS
 */
class SupportedDnsRecordTypeTest extends AbstractJUnit5Test {

    @Test
    void testSupportedDnsRecordTypeValues() throws Exception {
        assertEquals(3, SupportedDnsRecordType.values().length);

        assertEquals("CNAME", SupportedDnsRecordType.values()[0].toString());
        assertEquals("NAPTR", SupportedDnsRecordType.values()[1].toString());
        assertEquals("ALL", SupportedDnsRecordType.values()[2].toString());
    }
}
