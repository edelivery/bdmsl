/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.security;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.bo.CertificateDomainBO;
import eu.europa.ec.bdmsl.common.enums.SMLRoleEnum;
import eu.europa.ec.bdmsl.service.IX509CertificateService;
import eu.europa.ec.edelivery.security.PreAuthenticatedCertificatePrincipal;
import org.apache.commons.lang3.StringUtils;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import jakarta.servlet.http.HttpServletRequest;
import java.security.*;
import java.security.cert.X509Certificate;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Tiago MIGUEL
 * @since 06/12/2016
 */
class X509CertificateAuthenticationTest extends AbstractJUnit5Test {

    private static final String CERT_01_NOT_ROOT_SMP_SUBJECT_01 = "C=BE, O=GlobalSign nv-sa, OU=Root CA, CN=SMP_0001";
    private static final String CERT_01_NOT_ROOT_SMP_SUBJECT_02 = " O=DG-DIGIT,C=BE,CN=SMP_123456789101112";

    private static final String CERT_01_NOT_ROOT_SMP_ISSUER = "C=BE, O=GlobalSign nv-sa, OU=Root CA, CN=GlobalSign Root CA";

    @Autowired
    IX509CertificateService ix509CertificateService;

    @BeforeAll
    static void beforeClass() {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    @Test
    void createX509CertificateAuthTest() throws Exception {
        X509Certificate certificate = createX509CertificateKeystoreNotTrusted(CERT_01_NOT_ROOT_SMP_ISSUER, CERT_01_NOT_ROOT_SMP_SUBJECT_01);
        String certificateId = "CN=SMP_0001,O=GlobalSign nv-sa,C=BE:"+ StringUtils.leftPad(certificate.getSerialNumber().toString(16), 32, '0');
        // create authentication
        X509Certificate[] certificates = {certificate};
        CertificateAuthentication x509Authentication = createX509Authentication(certificates);

        Calendar validFrom = Calendar.getInstance();
        validFrom.setTime(certificate.getNotBefore());
        Calendar validTo = Calendar.getInstance();
        validTo.setTime(certificate.getNotAfter());


        assertEquals(certificateId, x509Authentication.getName());
        assertNotNull(x509Authentication.getPrincipal());
        assertEquals(PreAuthenticatedCertificatePrincipal.class, x509Authentication.getPrincipal().getClass());
        assertEquals(certificate, x509Authentication.getCredentials());
        assertEquals(validFrom, ((CertificateDetails) x509Authentication.getDetails()).getValidFrom());
        assertEquals(validTo, ((CertificateDetails) x509Authentication.getDetails()).getValidTo());
    }

    @Test
    void createSSLClientCertAuthTest() throws Exception {
        eDeliveryX509AuthenticationFilter.setHttpHeaderAuthenticationEnabled(true);
        X509Certificate certificate = createX509CertificateKeystoreNotTrusted(CERT_01_NOT_ROOT_SMP_ISSUER, CERT_01_NOT_ROOT_SMP_SUBJECT_01);
        String certificateId = "CN=SMP_0001,O=GlobalSign nv-sa,C=BE:"+ StringUtils.leftPad(certificate.getSerialNumber().toString(16), 32, '0');
        // create authentication
        X509Certificate[] certificates = {certificate};
        CertificateAuthentication x509Authentication = createSSLClientCertAuthentication(certificates);

        Calendar validFrom = Calendar.getInstance();
        validFrom.setTime(certificate.getNotBefore());
        Calendar validTo = Calendar.getInstance();
        validTo.setTime(certificate.getNotAfter());

        assertEquals(certificateId, x509Authentication.getName());
        assertNotNull(x509Authentication.getPrincipal());
        assertEquals(PreAuthenticatedCertificatePrincipal.class, x509Authentication.getPrincipal().getClass());
        assertEquals(certificate, (X509Certificate) x509Authentication.getCredentials());
        assertEquals(validFrom, ((CertificateDetails) x509Authentication.getDetails()).getValidFrom());
        assertEquals(validTo, ((CertificateDetails) x509Authentication.getDetails()).getValidTo());
    }

    @Test
    void authenticateX509CertificateAuthTest() throws Exception {
        CertificateAuthentication x509Authentication = createDefaultX509Authentication();

        x509Authentication.setAuthenticated(true);
        assertTrue(x509Authentication.isAuthenticated());
    }

    @Test
    void authenticateSSLClientCertificateAuthTest() throws Exception {
        eDeliveryX509AuthenticationFilter.setHttpHeaderAuthenticationEnabled(true);
        CertificateAuthentication x509Authentication = createDefaultSSLClientCertAuthentication();

        x509Authentication.setAuthenticated(true);
        assertTrue(x509Authentication.isAuthenticated());
    }

    @Test
    void notAuthenticateX509CertificateAuthTest() throws Exception {
        CertificateAuthentication x509Authentication = createDefaultX509Authentication();

        x509Authentication.setAuthenticated(false);
        assertFalse(x509Authentication.isAuthenticated());
    }

    @Test
    void getAuthoritiesTest() throws Exception {
        CertificateAuthentication x509Authentication = createDefaultX509Authentication();

        List<SimpleGrantedAuthority> listAuths = new ArrayList<SimpleGrantedAuthority>();
        SimpleGrantedAuthority authority = new SimpleGrantedAuthority("ROLE_SMP");
        listAuths.add(authority);

        Authentication authenticated = smlAuthenticationProvider.authenticate(x509Authentication);
        assertEquals(listAuths, authenticated.getAuthorities());
    }


    @Test
    void noAuthTest() throws Exception {
        CertificateAuthentication x509Authentication = createDefaultX509Authentication();

        assertEquals(Collections.emptyList(), x509Authentication.getAuthorities());
    }

    @Test
    void grantSMPRoleForNonRootCATrustedCertificateNotYetAuthorized() throws Exception {
        X509Certificate x509Certificate = createX509CertificateKeystoreNotTrusted(CERT_01_NOT_ROOT_SMP_ISSUER, CERT_01_NOT_ROOT_SMP_SUBJECT_02);

        X509Certificate[] certificates = {x509Certificate};
        CertificateAuthentication x509Authentication = createX509Authentication(certificates);

        assertEquals(Collections.emptyList(), x509Authentication.getAuthorities());
    }

    @Test
    void grantSMPRoleForNonRootCATrustedCertificateAuthorized() throws Exception {

        X509Certificate certificate = createX509CertificateKeystoreTrusted(CERT_01_NOT_ROOT_SMP_ISSUER, CERT_01_NOT_ROOT_SMP_SUBJECT_02, false);
        String certificateId = "CN=SMP_123456789101112,O=DG-DIGIT,C=BE:"+ StringUtils.leftPad(certificate.getSerialNumber().toString(16), 32, '0');

        CertificateDomainBO nonRootCA = ix509CertificateService.validateNonRootCA(certificate);
        CertificateAuthentication x509Authentication = createX509Authentication(certificate);
        Authentication authenticated = smlAuthenticationProvider.authenticate(x509Authentication);

        assertNotNull(nonRootCA);
        assertEquals(certificateId, authenticated.getName());
        assertEquals(1, authenticated.getAuthorities().size());
        assertEquals(SMLRoleEnum.ROLE_SMP.getAuthority(), authenticated.getAuthorities().iterator().next());
    }

    CertificateAuthentication createDefaultX509Authentication() throws Exception {
        X509Certificate certificate = createX509CertificateKeystoreTrusted(CERT_01_NOT_ROOT_SMP_ISSUER, CERT_01_NOT_ROOT_SMP_SUBJECT_01, false);
        // create authentication
        X509Certificate[] certificates = {certificate};
        return createX509Authentication(certificates);
    }

    CertificateAuthentication createX509Authentication(X509Certificate certificate) throws Exception {
        return createX509Authentication(new X509Certificate[]{certificate});
    }

    public CertificateAuthentication createX509Authentication(X509Certificate[] certificate) throws Exception {

        HttpServletRequest mockedRequest = Mockito.mock(HttpServletRequest.class);
        Mockito.when(mockedRequest.getAttribute("jakarta.servlet.request.X509Certificate")).thenReturn(certificate);

        Principal principal = eDeliveryX509AuthenticationFilter.buildDetails(mockedRequest);
        return new CertificateAuthentication((PreAuthenticatedCertificatePrincipal) principal, Collections.emptyList());
    }

    CertificateAuthentication createDefaultSSLClientCertAuthentication() throws Exception {
        X509Certificate certificate = createX509CertificateKeystoreNotTrusted(CERT_01_NOT_ROOT_SMP_ISSUER, CERT_01_NOT_ROOT_SMP_SUBJECT_01);
        return createSSLClientCertAuthentication(certificate);
    }

    CertificateAuthentication createSSLClientCertAuthentication(X509Certificate[] certificate) throws Exception {
        return createSSLClientCertAuthentication(certificate[0]);
    }

    CertificateAuthentication createSSLClientCertAuthentication(X509Certificate certificate) throws Exception {

        MockHttpServletRequest mockedRequest = new MockHttpServletRequest();
        mockedRequest.addHeader("SSLClientCert", Base64.getEncoder()
                .encodeToString(certificate.getEncoded()));

        Principal principal = eDeliveryX509AuthenticationFilter.buildDetails(mockedRequest);
        return new CertificateAuthentication((PreAuthenticatedCertificatePrincipal) principal, Collections.emptyList());

    }
}
