/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.exception;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.security.UnsecureAuthentication;
import eu.europa.ec.bdmsl.util.ExceptionUtils;
import eu.europa.ec.bdmsl.ws.soap.BadRequestFault;
import eu.europa.ec.bdmsl.ws.soap.InternalErrorFault;
import eu.europa.ec.bdmsl.ws.soap.UnauthorizedFault;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import static org.junit.jupiter.api.Assertions.assertTrue;


/**
 * @author Flavio SANTOS
 * @since 16/02/2017
 */
class ExceptionUtilsTest extends AbstractJUnit5Test {

    private final String message = "[106] Request values should only contain ASCII characters  and Request must be well-formed.";

    @BeforeEach
    void before() {
        UnsecureAuthentication authentication = new UnsecureAuthentication();
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
    }

    @Test
    void buildUnauthorizedFault() {
        UnauthorizedFault unauthorizedFault = ExceptionUtils.buildUnauthorizedFault(message);

        assertTrue(unauthorizedFault.getMessage().startsWith("[106] Request values should only contain ASCII characters  and Request must be well-formed."));
        assertTrue(unauthorizedFault.getFaultInfo().getFaultMessage().startsWith("[106] Request values should only contain ASCII characters  and Request must be well-formed."));
    }

    @Test
    void buildBadRequestFault() {
        BadRequestFault badRequestFault = ExceptionUtils.buildBadRequestFault(message);

        assertTrue(badRequestFault.getMessage().startsWith("[106] Request values should only contain ASCII characters  and Request must be well-formed."));
        assertTrue(badRequestFault.getFaultInfo().getFaultMessage().startsWith("[106] Request values should only contain ASCII characters  and Request must be well-formed."));
    }

    @Test
    void buildInternalErrorFault() {
        InternalErrorFault internalErrorFault = ExceptionUtils.buildInternalErrorFault(message);

        assertTrue(internalErrorFault.getMessage().startsWith("[106] Request values should only contain ASCII characters  and Request must be well-formed."));
        assertTrue(internalErrorFault.getFaultInfo().getFaultMessage().startsWith("[106] Request values should only contain ASCII characters  and Request must be well-formed."));
    }

}
