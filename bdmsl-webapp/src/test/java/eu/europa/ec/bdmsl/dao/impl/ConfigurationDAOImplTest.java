/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.impl;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.bo.ConfigurationBO;
import eu.europa.ec.bdmsl.common.enums.SMLPropertyEnum;
import eu.europa.ec.bdmsl.common.exception.BadConfigurationException;
import eu.europa.ec.bdmsl.common.exception.KeyException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.dao.IConfigurationDAO;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Repository;
import org.springframework.test.util.ReflectionTestUtils;

import jakarta.persistence.NoResultException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;


/**
 * @author Flavio SANTOS
 */
@Repository
class ConfigurationDAOImplTest extends AbstractJUnit5Test {


    @Autowired
    private IConfigurationDAO configurationDAO;

    @Captor
    ArgumentCaptor<Properties> captorOfProperties;

    @Test
    void testFindConfigurationProperty() throws TechnicalException {
        //GIVE - WHEN
        ConfigurationBO resultBO = configurationDAO.findConfigurationProperty("adminPassword");

        //THEN
        assertNotNull(resultBO);
        assertEquals("$2a$10$tbHhDiojqNy33xOqeAPz3./r0dz4dR8ZPPAOfH7wWHfcC2I91V/zG", resultBO.getValue());
    }

    @Test
    void testFindConfigurationPropertyNotFound() throws TechnicalException {
        //GIVE - WHEN
        assertThrows(NoResultException.class, () -> {
            ConfigurationBO resultBO = configurationDAO.findConfigurationProperty("NotFoundProperty");
            //THEN
            assertNull(resultBO);
        });


    }

    @Test
    void testLastUpdateOK() throws TechnicalException {
        //GIVE - WHEN
        Calendar cal = configurationDAO.getLastUpdate();

        //THEN
        assertNotNull(cal);
        assertTrue(Calendar.getInstance().after(cal));
    }

    @Test
    void testRetrieveMailPassword() throws KeyException {
        Path resourceDirectory = Paths.get("src", "test", "resources");

        // given
        Properties testProperties = new Properties();
        testProperties.setProperty(SMLPropertyEnum.MAIL_SERVER_PASSWORD.getProperty(), "d2Hun4dq5VPXv6uQp/PFvA==");
        testProperties.setProperty(SMLPropertyEnum.CONFIGURATION_DIR.getProperty(), resourceDirectory.toAbsolutePath().toString());
        testProperties.setProperty(SMLPropertyEnum.ENCRYPTION_FILENAME.getProperty(), "masterKey.key");

        ConfigurationDAOImpl ci = new ConfigurationDAOImpl(Mockito.mock(ApplicationContext.class), Mockito.mock(JavaMailSenderImpl.class));
        String res = ci.retrieveMailPassword(testProperties);

        assertEquals("EDELIVERY", res);
    }

    @Test
    void testRetrieveMailPasswordNull() throws KeyException {
        Path resourceDirectory = Paths.get("src", "test", "resources");

        // given
        Properties testProperties = new Properties();
        testProperties.setProperty(SMLPropertyEnum.MAIL_SERVER_PASSWORD.getProperty(), "");
        testProperties.setProperty(SMLPropertyEnum.CONFIGURATION_DIR.getProperty(), resourceDirectory.toAbsolutePath().toString());
        testProperties.setProperty(SMLPropertyEnum.ENCRYPTION_FILENAME.getProperty(), "masterKey.key");
        // this is pojo method
        ConfigurationDAOImpl ci = new ConfigurationDAOImpl(Mockito.mock(ApplicationContext.class), Mockito.mock(JavaMailSenderImpl.class));
        String res = ci.retrieveMailPassword(testProperties);

        assertNull(res);
    }

    @Test
    void testGetMailPropertiesFromString() throws KeyException {


        // given
        String val = "mail.smtp.auth:true;mail.smtp.starttls.enable:true;mail.smtp.quitwait:false";
        // this is pojo method
        ConfigurationDAOImpl ci = new ConfigurationDAOImpl(Mockito.mock(ApplicationContext.class), Mockito.mock(JavaMailSenderImpl.class));

        Properties prop = ci.getMailPropertiesFromString(val);


        assertNotNull(prop);
        assertEquals(3, prop.size());
        assertTrue(prop.containsKey("mail.smtp.auth"));
        assertTrue(prop.containsKey("mail.smtp.starttls.enable"));
        assertTrue(prop.containsKey("mail.smtp.quitwait"));

        assertEquals("true", prop.getProperty("mail.smtp.auth"));
        assertEquals("true", prop.getProperty("mail.smtp.starttls.enable"));
        assertEquals("false", prop.getProperty("mail.smtp.quitwait"));
    }

    @Test
    void testSetupSMTPConfiguration() throws KeyException {

        ConfigurationDAOImpl testInstance = new ConfigurationDAOImpl(Mockito.mock(ApplicationContext.class), Mockito.mock(JavaMailSenderImpl.class));
        JavaMailSenderImpl mailSender = Mockito.mock(JavaMailSenderImpl.class);
        ReflectionTestUtils.setField(testInstance, "mailSender", mailSender);

        String val = "mail.smtp.auth:true;mail.smtp.starttls.enable:true;mail.smtp.quitwait:false";
        Properties testProperties = new Properties();
        testProperties.setProperty(SMLPropertyEnum.MAIL_SERVER_HOST.getProperty(), "mail.test-host.eu");
        testProperties.setProperty(SMLPropertyEnum.MAIL_SERVER_PORT.getProperty(), "8443");
        testProperties.setProperty(SMLPropertyEnum.MAIL_SERVER_PROTOCOL.getProperty(), "smtps");
        testProperties.setProperty(SMLPropertyEnum.MAIL_SERVER_PROPERTIES.getProperty(), val);


        testInstance.setupSMTPConfiguration(testProperties);

        Mockito.verify(mailSender, Mockito.times(0)).setUsername(Mockito.any());
        Mockito.verify(mailSender, Mockito.times(1)).setProtocol("smtps");
        Mockito.verify(mailSender, Mockito.times(1)).setHost("mail.test-host.eu");
        Mockito.verify(mailSender, Mockito.times(1)).setPort(8443);
        Mockito.verify(mailSender).setJavaMailProperties(captorOfProperties.capture());
        Properties prop = captorOfProperties.getValue();
        assertTrue(prop.containsKey("mail.smtp.auth"));
        assertTrue(prop.containsKey("mail.smtp.starttls.enable"));
        assertTrue(prop.containsKey("mail.smtp.quitwait"));

        assertEquals("true", prop.getProperty("mail.smtp.auth"));
        assertEquals("true", prop.getProperty("mail.smtp.starttls.enable"));
        assertEquals("false", prop.getProperty("mail.smtp.quitwait"));

    }


    @Test
    void testValidateConfigurationEmptyKeystoreAlias() {
        Properties testProperties = new Properties();
        testProperties.put("signResponse", "true");
        testProperties.put("dnsClient.enabled", "true");
        testProperties.put("keystoreAlias", "");

        whenThen(testProperties, "[ERR-109] If you enable response signing configuration, then you must set the 'keystoreAlias' property");
    }

    @Test
    void testValidateConfigurationEmptyKeystoreFileName() {
        Properties testProperties = new Properties();
        testProperties.put("signResponse", "true");
        testProperties.put("dnsClient.enabled", "true");
        testProperties.put("keystoreFileName", "");
        testProperties.put("keystoreAlias", "asdsd");

        whenThen(testProperties, "[ERR-109] If you enable response signing configuration, then you must set the 'keystoreFileName' property");
    }

    @Test
    void testValidateConfigurationEmptyencryptedPasswordKeykey() {
        Properties testProperties = new Properties();
        testProperties.put("signResponse", "true");
        testProperties.put("keystoreAlias", "sdfsd");
        testProperties.put("keystoreFileName", "sdfsdf");
        testProperties.put("keystorePassword", "");

        whenThen(testProperties, "[ERR-109] If you enable response signing configuration, then you must set the 'keystorePassword' property");
    }

    @Test
    void testValidateConfigurationMissingDirectory() {
        Properties testProperties = new Properties();
        testProperties.put("configurationDir", "/test");
        testProperties.put("signResponse", "true");
        testProperties.put("keystoreAlias", "sdfsd");
        testProperties.put("keystoreFileName", "sdfsdf");
        testProperties.put("keystorePassword", "d2Hun4dq5VPXv6uQp/PFvA==");
        testProperties.put("encriptionPrivateKey", "encriptionPrivateKey.key");


        whenThen(testProperties, "[ERR-109] The directory [/test] or file [sdfsdf] doesn't exist.");
    }

    @Test
    void testValidateConfigurationEmptyDnsClientPublisherPrefix() {
        Properties testProperties = new Properties();
        testProperties.put("signResponse", "false");
        testProperties.put("dnsClient.enabled", "true");
        testProperties.put("dnsClient.publisherPrefix", "");

        whenThen(testProperties, "[ERR-109] If you enable the dns client configuration, then you must set the 'dnsClient.publisherPrefix' property");
    }

    @Test
    void testValidateConfigurationEmptyDnsClientServer() {
        Properties testProperties = new Properties();
        testProperties.put("signResponse", "false");
        testProperties.put("dnsClient.server", "");
        testProperties.put("dnsClient.enabled", "true");
        testProperties.put("dnsClient.publisherPrefix", "publisher");

        whenThen(testProperties, "[ERR-109] If you enable the dns client configuration, then you must set the 'dnsClient.server' property");
    }

    @Test
    void testValidateConfigurationEmptydnsClientSIG0KeyFileName() {
        Properties testProperties = new Properties();
        testProperties.put("signResponse", "false");
        testProperties.put("dnsClient.enabled", "false");
        testProperties.put("dnsClient.SIG0Enabled", "true");
        testProperties.put("dnsClient.SIG0KeyFileName", "");

        whenThen(testProperties, "[ERR-109] If you enable DNSSEC configuration, then you must set the 'dnsClient.SIG0KeyFileName' property");
    }

    @Test
    void testValidateConfigurationEmptyDnsClientSIG0PublicKeyName() {
        Properties testProperties = new Properties();
        String tempDir = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        testProperties.put("configurationDir", tempDir);
        testProperties.put("mail.smtp.port", "25");
        testProperties.put("mail.smtp.host", "smtp.server.eu");
        testProperties.put("paginationListRequest", "100");
        testProperties.put("signResponse", "false");
        testProperties.put("dnsClient.enabled", "false");
        testProperties.put("dnsClient.SIG0Enabled", "true");
        testProperties.put("dnsClient.SIG0KeyFileName", "sdsd");
        testProperties.put("dnsClient.SIG0PublicKeyName", "");
        testProperties.put("encriptionPrivateKey", "masterKey.key");

        whenThen(testProperties, "[ERR-109] If you enable DNSSEC configuration, then you must set the 'dnsClient.SIG0PublicKeyName' property");
    }

    @Test
    void testValidateConfigurationProxy() throws BadConfigurationException {
        Properties testProperties = new Properties();
        String tempDir = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        testProperties.put("sml.property.refresh.cronJobExpression", "*/5 * * * * *");
        testProperties.put("certificateChangeCronExpression", "*/5 * * * * *");
        testProperties.put("dataInconsistencyAnalyzer.cronJobExpression", "*/5 * * * * *");
        testProperties.put("mail.smtp.port", "25");
        testProperties.put("mail.smtp.host", "smtp.server.eu");
        testProperties.put("paginationListRequest", "100");
        testProperties.put("signResponse", "false");
        testProperties.put("dnsClient.enabled", "false");
        testProperties.put("dnsClient.SIG0Enabled", "false");
        testProperties.put("useProxy", "true");
        testProperties.put("httpsProxyPort", "80");
        testProperties.put("httpProxyPort", "80");
        testProperties.put("httpProxyUser", "user123");
        testProperties.put("configurationDir", tempDir);
        testProperties.put("httpProxyPassword", "d2Hun4dq5VPXv6uQp/PFvA==");
        testProperties.put("encriptionPrivateKey", "masterKey.key");

        (new ConfigurationDAOImpl(Mockito.mock(ApplicationContext.class), Mockito.mock(JavaMailSenderImpl.class))).validateConfiguration(testProperties);
    }

    @Test
    void testValidateConfigurationProxyMissingUser() {
        Properties testProperties = new Properties();
        String tempDir = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        testProperties.put("sml.property.refresh.cronJobExpression", "*/5 * * * * *");
        testProperties.put("certificateChangeCronExpression", "*/5 * * * * *");
        testProperties.put("dataInconsistencyAnalyzer.cronJobExpression", "*/5 * * * * *");

        testProperties.put("signResponse", "false");
        testProperties.put("dnsClient.enabled", "false");
        testProperties.put("dnsClient.SIG0Enabled", "false");
        testProperties.put("useProxy", "true");
        testProperties.put("httpProxyUser", "");
        testProperties.put("configurationDir", tempDir);
        testProperties.put("httpProxyPassword", "d2Hun4dq5VPXv6uQp/PFvA==");

        whenThen(testProperties, "[ERR-109] If you enable proxy configuration, then you must set the 'httpProxyUser' property");
    }

    @Test
    void testValidateConfigurationProxyMissingPassword() {
        Properties testProperties = new Properties();
        String tempDir = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        testProperties.put("signResponse", "false");
        testProperties.put("dnsClient.enabled", "false");
        testProperties.put("dnsClient.SIG0Enabled", "false");
        testProperties.put("useProxy", "true");
        testProperties.put("httpProxyUser", "user123");
        testProperties.put("configurationDir", tempDir);
        testProperties.put("httpProxyPassword", "");
        testProperties.put("encriptionPrivateKey", "encriptionPrivateKey.key");

        whenThen(testProperties, "[ERR-109] If you enable proxy configuration, then you must set the 'httpProxyPassword' property");
    }

    @Test
    void testCheckIfExists() {
        Properties testProperties = new Properties();
        String tempDir = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        testProperties.put("signResponse", "false");
        testProperties.put("dnsClient.enabled", "false");
        testProperties.put("dnsClient.SIG0Enabled", "false");
        testProperties.put("useProxy", "true");
        testProperties.put("httpProxyUser", "user123");
        testProperties.put("configurationDir", tempDir);
        testProperties.put("httpProxyPassword", "");
        testProperties.put("encriptionPrivateKey", "encriptionPrivateKey.key");

        whenThen(testProperties, "[ERR-109] If you enable proxy configuration, then you must set the 'httpProxyPassword' property");
    }

    private void whenThen(Properties testProperties, String errorMessage) {
        try {
            (new ConfigurationDAOImpl(Mockito.mock(ApplicationContext.class), Mockito.mock(JavaMailSenderImpl.class))).validateConfiguration(testProperties);
            fail();
        } catch (Exception e) {
            assertEquals(errorMessage, e.getMessage());
        }
    }
}
