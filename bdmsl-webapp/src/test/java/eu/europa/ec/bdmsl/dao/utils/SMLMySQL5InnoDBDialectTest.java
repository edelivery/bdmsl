/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.sql.Types;

class SMLMySQL5InnoDBDialectTest {

    SMLMySQL5InnoDBDialect testInstance = new SMLMySQL5InnoDBDialect();

    @Test
    void getTableTypeString() {

        Assertions.assertEquals(" ENGINE=InnoDB DEFAULT CHARSET=utf8", testInstance.getTableTypeString());
    }
/*
    @Test
    void registerVarcharTypes() {

        Assertions.assertEquals("longtext", testInstance.getTypeName(Types.VARCHAR));
        Assertions.assertEquals("longtext", testInstance.getTypeName(Types.LONGVARCHAR));
        Assertions.assertEquals("varchar(65535)  CHARACTER SET utf8 COLLATE utf8_bin", testInstance.getTypeName(Types.VARCHAR, 65535, 0, 0));

    }

 */
}
