/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.test;

import eu.europa.ec.bdmsl.common.enums.SMLRoleEnum;
import eu.europa.ec.bdmsl.security.CertificateAuthentication;
import eu.europa.ec.edelivery.security.ClientCertAuthenticationFilter;
import eu.europa.ec.edelivery.security.PreAuthenticatedCertificatePrincipal;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Collections;

import static eu.europa.ec.bdmsl.test.TestConstants.*;

/**
 * Utility class for security setup for the tests.
 *
 * @author Joze RIHTARSIC
 * @since 5.0
 */
public class SecurityTestUtils {

    private static final ClientCertAuthenticationFilter CLIENT_CERT_PRINCIPAL_BUILDER = new ClientCertAuthenticationFilter();

    protected SecurityTestUtils() {
    }


    public static void authenticateWithRoleClientCert02(SMLRoleEnum role) {
        authenticateWithRoleClientCert(role,
                CERTIFICATE_02_SERIAL, CERTIFICATE_02_ISSUER, CERTIFICATE_02_SUBJECT);

    }

    public static void authenticateWithRoleClientCert06(SMLRoleEnum role) {
        authenticateWithRoleClientCert(role,
                CERTIFICATE_06_SERIAL, CERTIFICATE_06_ISSUER, CERTIFICATE_06_SUBJECT);

    }

    public static void authenticateWithRoleClientCert(SMLRoleEnum role, String serial, String issuer, String subject) {
        String certHeaderValue = CommonTestUtils.createHeaderCertificateForBlueCoat(
                serial, issuer, subject);
        authenticateWithRoleClientCert(certHeaderValue, role);
    }
    public static void authenticateWithRoleClientCert(String certHeaderValue, SMLRoleEnum role) {
        PreAuthenticatedCertificatePrincipal principal = CLIENT_CERT_PRINCIPAL_BUILDER.buildPrincipalFromHeader(certHeaderValue);
        Authentication bcAuth = new CertificateAuthentication(principal, Collections.singletonList(role.getAuthority()));
        bcAuth.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(bcAuth);
    }



    public static void clearAuthentication() {
        SecurityContextHolder.clearContext();
    }
}
