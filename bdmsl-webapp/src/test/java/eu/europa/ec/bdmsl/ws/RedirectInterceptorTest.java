/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.ws;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import org.apache.cxf.message.ExchangeImpl;
import org.apache.cxf.message.Message;
import org.apache.cxf.message.MessageImpl;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

/**
 * @author Flavio SANTOS
 */
class RedirectInterceptorTest extends AbstractJUnit5Test {

    @Autowired
    private RedirectInterceptor redirectInterceptor;

    @Test
    void testHandleMessageGETMethodOK() throws Exception {
        Message message = getSOAPMessage();
        setRequestMethod(message, "GET");
        redirectInterceptor.handleMessage(message);

        String query = (String) message.get(Message.QUERY_STRING);
       assertEquals("wsdl", query);
    }

    @Test
    void testHandleMessagePOSTMethodOK() {
        Message message = getSOAPMessage();
        setRequestMethod(message, "POST");
        redirectInterceptor.handleMessage(message);

        String query = (String) message.get(Message.QUERY_STRING);
        assertNull(query);
    }

    @Test
    void testHandleMessageGETMethodWithOtherQuery() throws Exception {
        Message message = getSOAPMessage();
        setRequestMethod(message, "GET");
        setQuery(message, "xsd");
        redirectInterceptor.handleMessage(message);

        String query = (String) message.get(Message.QUERY_STRING);
        assertEquals("xsd", query);
    }

    private void setRequestMethod(Message message, String requestMethod) {
        message.put(Message.HTTP_REQUEST_METHOD, requestMethod);
    }

    private void setPayload(Message message, Class clazz, Object payload) {
        message.setContent(clazz, payload);
    }

    private void setQuery(Message message, String query) {
        message.put(Message.QUERY_STRING, query);
    }

    private Message getSOAPMessage() {
        Message message = new MessageImpl();
        message.setExchange(new ExchangeImpl());

        return message;
    }
}
