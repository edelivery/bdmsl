/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.ws.soap;

import ec.services.wsdl.bdmsl.admin.data._1.*;
import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.enums.SMLRoleEnum;
import eu.europa.ec.bdmsl.common.util.KeyUtil;
import eu.europa.ec.bdmsl.test.CommonTestUtils;
import eu.europa.ec.bdmsl.security.CertificateAuthentication;
import eu.europa.ec.edelivery.security.PreAuthenticatedCertificatePrincipal;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.Principal;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.UUID;

import static eu.europa.ec.bdmsl.test.TestConstants.TRUSTSTORE_DEF_SMP_ALIAS;
import static org.hamcrest.CoreMatchers.startsWithIgnoringCase;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;


class BDMSLAdminServiceWSTruststoreCertificateTest extends AbstractJUnit5Test {


    static {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    @Autowired
    private IBDMSLAdminServiceWS bdmslAdminServiceWS;


    @BeforeEach
    protected void testSetup() throws Exception {
        String encryptKey = "encryptedPasswordKey.key";
        String truststore = "bdmsl-truststore.p12";
        String truststoreNew = "bdmsl-truststore_" + UUID.randomUUID() + ".p12";
        Files.copy(resourceDirectory.resolve(truststore), targetDirectory.resolve(truststoreNew), StandardCopyOption.REPLACE_EXISTING);

        String resourcesPath = targetDirectory.toFile().getAbsolutePath();

        Mockito.doReturn(false).when(configurationBusiness).isLegacyDomainAuthorizationEnabled();
        Mockito.doReturn(truststoreNew).when(configurationBusiness).getTruststoreFilename();
        Mockito.doReturn(resourcesPath).when(configurationBusiness).getConfigurationFolder();
        Mockito.doReturn(encryptKey).when(configurationBusiness).getEncryptionFilename();
        Mockito.doReturn(KeyUtil.encrypt(Paths.get(resourcesPath, encryptKey).toFile().getAbsolutePath(), "test123"))
                .when(configurationBusiness).getTruststorePassword();


        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        String certHeaderValue = "serial=48:b6:81:ee:8e:0d:cc:08&subject=EMAILADDRESS=receiver@test.be,C=BE, O=DIGIT, OU=FOR TEST ONLY,CN=DIGIT_SMP_TECH_TEAM_3&validfrom=Feb  1 14:20:18 2017 GMT&validto=Jul  9 23:59:00 2019 GMT&issuer=C=BE, O=DIGIT, OU=FOR TEST ONLY, CN=DIGIT_SMP_TECH_TEAM_2";
        Principal principal = blueCoatAuthenticationFilter.buildPrincipalFromHeader(certHeaderValue);
        Authentication authentication = new CertificateAuthentication((PreAuthenticatedCertificatePrincipal) principal, Collections.singletonList(SMLRoleEnum.ROLE_ADMIN.getAuthority()));
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @AfterEach
    void clean() throws Exception {
        File file = configurationBusiness.getTruststoreFile();
        file.delete();
    }


    @Test
    void testAddTruststoreCertificateNullRequest() {
        // given when
        BadRequestFault requestFault = assertThrows(BadRequestFault.class,
                () -> bdmslAdminServiceWS.addTruststoreCertificate(null));
        // then
        assertThat(requestFault.getMessage(),
                startsWithIgnoringCase("[ERR-106] The input values must not be null!"));
    }

    @Test
    void testListTruststoreCertificateAliasesNullRequest() {
        // given when
        BadRequestFault requestFault = assertThrows(BadRequestFault.class,
                () -> bdmslAdminServiceWS.listTruststoreCertificateAliases(null));
        // then
        assertThat(requestFault.getMessage(),
                startsWithIgnoringCase("[ERR-106] The input values must not be null!"));
    }

    @Test
    void testDeleteTruststoreCertificateNullRequest() {
        // when then
        BadRequestFault requestFault = assertThrows(BadRequestFault.class,
                () -> bdmslAdminServiceWS.deleteTruststoreCertificate(null));
        // then
        assertThat(requestFault.getMessage(),
                startsWithIgnoringCase("[ERR-106] The input values must not be null!"));
    }

    @Test
    void testDeleteTruststoreCertificateBlankRequest() {
        // when
        DeleteTruststoreCertificateRequest request = new DeleteTruststoreCertificateRequest();
        request.setAlias(" ");
        // then
        BadRequestFault requestFault = assertThrows(BadRequestFault.class,
                () -> bdmslAdminServiceWS.deleteTruststoreCertificate(request));
        // then
        assertThat(requestFault.getMessage(),
                startsWithIgnoringCase("[ERR-106] The input values must not be null!"));
    }

    @Test
    void testDeleteTruststoreCertificateNotExitsRequest() {
        // when
        DeleteTruststoreCertificateRequest request = new DeleteTruststoreCertificateRequest();
        request.setAlias("smpNotExits");
        // then
        NotFoundFault requestFault = assertThrows(NotFoundFault.class,
                () -> bdmslAdminServiceWS.deleteTruststoreCertificate(request));
        // then
        assertThat(requestFault.getMessage(),
                startsWithIgnoringCase("[ERR-118] The certificate with alias [smpNotExits] does not exist!"));
    }

    @Test
    void testGetTruststoreCertificateNullRequest() {
        // given when
        BadRequestFault requestFault = assertThrows(BadRequestFault.class,
                () -> bdmslAdminServiceWS.getTruststoreCertificate(null));
        // then
        assertThat(requestFault.getMessage(),
                startsWithIgnoringCase("[ERR-106] The input values must not be null!"));
    }

    @Test
    void testGetTruststoreCertificateBlankAliasRequest() {
        // given
        GetTruststoreCertificateRequest request = new GetTruststoreCertificateRequest();
        request.setAlias(" ");
        //when
        BadRequestFault requestFault = assertThrows(BadRequestFault.class,
                () -> bdmslAdminServiceWS.getTruststoreCertificate(request));
        // then
        assertThat(requestFault.getMessage(),
                startsWithIgnoringCase("[ERR-106] The input values must not be null!"));
    }

    @Test
    void testGetTruststoreCertificateNotExitsAliasRequest() {
        // given
        GetTruststoreCertificateRequest request = new GetTruststoreCertificateRequest();
        request.setAlias("smpNotExits");
        //when
        NotFoundFault requestFault = assertThrows(NotFoundFault.class,
                () -> bdmslAdminServiceWS.getTruststoreCertificate(request));
        // then
        assertThat(requestFault.getMessage(),
                startsWithIgnoringCase("[ERR-118] The certificate with alias [smpNotExits] does not exist!"));
    }

    @Test
    void testListTruststoreCertificateAliases() throws Exception {
        // given when
        ListTruststoreCertificateAliasesRequest request = new ListTruststoreCertificateAliasesRequest();
        request.setContainsStringInAlias(null);
        // when
        ListTruststoreCertificateAliasesResponse response =
                bdmslAdminServiceWS.listTruststoreCertificateAliases(request);

        // then
        assertNotNull(response);
        assertEquals(3, response.getAlias().size());
    }

    @Test
    void testListTruststoreCertificateAliasesFilter() throws Exception {
        // given when
        ListTruststoreCertificateAliasesRequest request = new ListTruststoreCertificateAliasesRequest();
        request.setContainsStringInAlias("smp");
        // when
        ListTruststoreCertificateAliasesResponse response =
                bdmslAdminServiceWS.listTruststoreCertificateAliases(request);

        // then
        assertNotNull(response);
        assertEquals(1, response.getAlias().size());
    }

    @Test
    void testGetTruststoreCertificateRequest() throws Exception {
        // given
        GetTruststoreCertificateRequest request = new GetTruststoreCertificateRequest();
        request.setAlias(TRUSTSTORE_DEF_SMP_ALIAS);
        //when
        TruststoreCertificateType result = bdmslAdminServiceWS.getTruststoreCertificate(request);
        // then
        assertNotNull(result);
        assertNotNull(TRUSTSTORE_DEF_SMP_ALIAS, result.getAlias());
        assertNotNull(result.getCertificatePublicKey());
    }

    @Test
    void testAddTruststoreCertificateRequest() throws Exception {
        // given
        X509Certificate certificate = CommonTestUtils.createCertificate("CN=Keystore test 01,O=Digit,C=BE", "CN=Keystore test 01,O=Digit,C=BE", false);

        TruststoreCertificateType request = new TruststoreCertificateType();
        request.setAlias(UUID.randomUUID().toString());
        request.setCertificatePublicKey(certificate.getEncoded());
        //when
        TruststoreCertificateType result = bdmslAdminServiceWS.addTruststoreCertificate(request);
        // then
        assertNotNull(result);
        assertNotNull(TRUSTSTORE_DEF_SMP_ALIAS, result.getAlias());
        assertNotNull(result.getCertificatePublicKey());
    }

    @Test
    void testAddTruststoreCertificateAliasAlreadyExistRequest() throws Exception {
        // given
        X509Certificate certificate = CommonTestUtils.createCertificate("CN=Keystore test 01,O=Digit,C=BE", "CN=Keystore test 01,O=Digit,C=BE", false);

        TruststoreCertificateType request = new TruststoreCertificateType();
        request.setAlias(TRUSTSTORE_DEF_SMP_ALIAS);
        request.setCertificatePublicKey(certificate.getEncoded());
        //when
        BadRequestFault requestFault = assertThrows(BadRequestFault.class,
                () -> bdmslAdminServiceWS.addTruststoreCertificate(request));
        // then
        assertThat(requestFault.getMessage(),
                startsWithIgnoringCase("[ERR-106] Certificate with alias [smp_test_change (test intermediate issuer 01)] already exists!"));

    }

    @Test
    void testAddTruststoreCertificateInvalidCertificateRequest() {
        // given
        String alias = UUID.randomUUID().toString();
        TruststoreCertificateType request = new TruststoreCertificateType();
        request.setAlias(alias);
        request.setCertificatePublicKey(alias.getBytes());
        //when
        BadRequestFault requestFault = assertThrows(BadRequestFault.class,
                () -> bdmslAdminServiceWS.addTruststoreCertificate(request));
        // then
        assertThat(requestFault.getMessage(),
                startsWithIgnoringCase("[ERR-106] The certificate is not valid - [Could not parse certificate:"));

    }

}
