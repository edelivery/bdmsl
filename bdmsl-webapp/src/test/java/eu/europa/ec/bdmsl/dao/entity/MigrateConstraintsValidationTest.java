/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.entity;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * This test class validates the data model {@link eu.europa.ec.bdmsl.dao.entity.MigrateEntity)} and its constraints
 *
 * @author Flavio SANTOS
 * @since 22/09/2016
 */
class MigrateConstraintsValidationTest extends AbstractJUnit5Test {

    @Test
    void persistIntoMigrateTableOk() throws SQLException {
        String sql = "INSERT INTO bdmsl_migrate (scheme,participant_id, migration_key,new_smp_id,old_smp_id,migrated,created_on,last_updated_on) values (?,?,?,?,?,?,?,?)";
        Timestamp date = new Timestamp(Calendar.getInstance().getTimeInMillis());

        assertDoesNotThrow(() -> persist(sql, "iso6523-actorid-upis", "0037:1234567891098754", "29270450dfc2a37dace1d0de8ab42c37", null, "OLD_SMP-BASWARE", false, date, date));
    }

    @Test
    void persistIntoMigrateTablePKNotOk() throws SQLException {
        String sql = "INSERT INTO bdmsl_migrate (scheme,participant_id, migration_key,new_smp_id,old_smp_id,migrated,created_on,last_updated_on) values (?,?,?,?,?,?,?,?)";
        Timestamp date = new Timestamp(Calendar.getInstance().getTimeInMillis());

        persist(sql, "iso6523-actorid-upis", "0037:987654321", "29270450dfc2a37dace1d0de8ab42c37", null, "SMP-BASWARE-SECOND-ONE", false, date, date);
        assertThrows(SQLException.class, () -> {
            persist(sql, "iso6523-actorid-upis", "0037:987654321", "29270450dfc2a37dace1d0de8ab42c37", null, "SMP-BASWARE-SECOND-ONE", false, date, date);
        });
    }

    @Test
    void persistIntoMigrateTableSchemeNull() {
        String sql = "INSERT INTO bdmsl_migrate (scheme,participant_id, migration_key,new_smp_id,old_smp_id,migrated,created_on,last_updated_on) values (?,?,?,?,?,?,?,?)";
        Timestamp date = new Timestamp(Calendar.getInstance().getTimeInMillis());
        assertThrows(SQLException.class, () -> {
            persist(sql, null, "0037:12345678910", "29270450dfc2a37dace1d0de8ab42c37", null, "OLD_SMP-BASWARE", false, date, date);
        });
    }

    @Test
    void persistIntoMigrateTableParticipantIdNull() {
        String sql = "INSERT INTO bdmsl_migrate (scheme,participant_id, migration_key,new_smp_id,old_smp_id,migrated,created_on,last_updated_on) values (?,?,?,?,?,?,?,?)";
        Timestamp date = new Timestamp(Calendar.getInstance().getTimeInMillis());
        assertThrows(SQLException.class, () -> {
            persist(sql, "iso6523-actorid-upis", null, "29270450dfc2a37dace1d0de8ab42c37", null, "OLD_SMP-BASWARE", false, date, date);
        });
    }

    @Test
    void persistIntoMigrateTableMigrationKeyNull() {
        String sql = "INSERT INTO bdmsl_migrate (scheme,participant_id, migration_key,new_smp_id,old_smp_id,migrated,created_on,last_updated_on) values (?,?,?,?,?,?,?,?)";
        Timestamp date = new Timestamp(Calendar.getInstance().getTimeInMillis());
        assertThrows(SQLException.class, () -> {
            persist(sql, "iso6523-actorid-upis", "0037:12345678910", null, null, "OLD_SMP-BASWARE", false, date, date);
        });
    }

    @Test
    void persistIntoMigrateTableOldMigrateKeyNull() {
        String sql = "INSERT INTO bdmsl_migrate (scheme,participant_id, migration_key,new_smp_id,old_smp_id,migrated,created_on,last_updated_on) values (?,?,?,?,?,?,?,?)";
        Timestamp date = new Timestamp(Calendar.getInstance().getTimeInMillis());
        assertThrows(SQLException.class, () -> {
            persist(sql, "iso6523-actorid-upis", "0037:12345678910", "29270450dfc2a37dace1d0de8ab42c37", "NEW-KEY-BASWARE", null, false, date, date);
        });
    }

    @Test
    void persistIntoMigrateTableCreatedOnNull() {
        String sql = "INSERT INTO bdmsl_migrate (scheme,participant_id, migration_key,new_smp_id,old_smp_id,migrated,created_on,last_updated_on) values (?,?,?,?,?,?,?,?)";
        Timestamp date = new Timestamp(Calendar.getInstance().getTimeInMillis());
        assertThrows(SQLException.class, () -> {
            persist(sql, "iso6523-actorid-upis", "0037:12345678910", "29270450dfc2a37dace1d0de8ab42c37", "NEW-KEY-BASWARE", null, false, null, date);
        });
    }

    @Test
    void persistIntoMigrateTableOldSMPIdNotExists() {
        //FK_keys are case sensitive and it must be insensitive
        String sql = "INSERT INTO bdmsl_migrate (scheme,participant_id, migration_key,new_smp_id,old_smp_id,migrated,created_on,last_updated_on) values (?,?,?,?,?,?,?,?)";
        Timestamp date = new Timestamp(Calendar.getInstance().getTimeInMillis());

        assertDoesNotThrow(() -> persist(sql, "iso6523-actorid-upis", "0037:12345678910", "29270450dfc2a37dace1d0de8ab42c37", "NEW-KEY-BASWARE-2010", "NEW-KEY-BASWARE-2017", false, date, date));
    }

    private void persist(String sqlQuery, String scheme, String participantId, String migrationKey, String newSmpId, String oldSmpId, boolean isMigrated, Timestamp createdOn, Timestamp updatedOn) throws SQLException {
        try (Connection connection = dataSource.getConnection();
            PreparedStatement pstmt = connection.prepareStatement(sqlQuery)) {
            pstmt.setString(1, scheme);
            pstmt.setString(2, participantId);
            pstmt.setString(3, migrationKey);
            pstmt.setString(4, newSmpId);
            pstmt.setString(5, oldSmpId);
            pstmt.setBoolean(6, isMigrated);
            pstmt.setTimestamp(7, createdOn);
            pstmt.setTimestamp(8, updatedOn);
            pstmt.executeUpdate();
            connection.commit();
        }
    }

}
