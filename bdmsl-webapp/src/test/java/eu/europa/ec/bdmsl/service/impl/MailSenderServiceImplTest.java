/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */

/**
 * @author Flavio SANTOS
 */
package eu.europa.ec.bdmsl.service.impl;

import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;
import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.exception.BadConfigurationException;
import eu.europa.ec.bdmsl.common.exception.GenericTechnicalException;
import eu.europa.ec.bdmsl.service.IMailSenderService;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.net.ServerSocket;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class MailSenderServiceImplTest extends AbstractJUnit5Test {

    @Autowired
    private IMailSenderService mailSenderService;

    @Autowired
    private JavaMailSenderImpl mailSender;

    private GreenMail mockSmtp;

    @BeforeEach
    void setup() throws Exception {
        ServerSocket serverSocket = new ServerSocket(0);
        int availablePort = serverSocket.getLocalPort();
        serverSocket.close();
        ServerSetup SMTP = new ServerSetup(availablePort, null, ServerSetup.PROTOCOL_SMTP);
        mockSmtp = new GreenMail(SMTP);
        mockSmtp.start();
        mailSender.setPort(availablePort);
        mailSender.setHost("localhost");
    }

    @Test
    void testSendMessageOk() throws Exception {
        Mockito.doReturn("smtp.server.eu").when(configurationBusiness).getMailSMPTHost();

        sendMessage("recipient@server.com", "sender@server.com", "TestSubject", "TestMessage");

        assertEquals(1, mockSmtp.getReceivedMessages().length);
    }

    @Test
    void testSendMessageIncorrectRecipientEmail() {

        BadConfigurationException result = assertThrows(BadConfigurationException.class,
                () -> sendMessage("recipient@server", "sender@server.com", "TestSubject", "TestMessage"));
        assertEquals("[ERR-109] Email address [recipient@server] is not valid.", result.getMessage());
    }

    @Test
    void testSendMessageIncorrectSenderEmail() {

        BadConfigurationException result = assertThrows(BadConfigurationException.class,
                () -> sendMessage("recipient@server.com", "sender@server", "TestSubject", "TestMessage"));
        assertEquals("[ERR-109] Email address [sender@server] is not valid.", result.getMessage());
    }

    @Test
    void testSendMessageEmptySubject() {

        GenericTechnicalException result = assertThrows(GenericTechnicalException.class,
                () -> sendMessage("recipient@server.com", "sender@server.com", "", "TestMessage"));
        assertEquals("[ERR-105] Email subject is null or empty.", result.getMessage());
    }

    @Test
    void testSendMessageNullSubject() {

        GenericTechnicalException result = assertThrows(GenericTechnicalException.class,
                () -> sendMessage("recipient@server.com", "sender@server.com", null, "TestMessage"));
        assertEquals("[ERR-105] Email subject is null or empty.", result.getMessage());
    }

    @Test
    void testSendMessageEmptyMessage() {

        GenericTechnicalException result = assertThrows(GenericTechnicalException.class,
                () -> sendMessage("recipient@server.com", "sender@server.com", "TestSubject", ""));
        assertEquals("[ERR-105] Email content is null or empty.", result.getMessage());
    }

    @Test
    void testSendMessageNullMessage() {

        GenericTechnicalException result = assertThrows(GenericTechnicalException.class,
                () -> sendMessage("recipient@server.com", "sender@server.com", "TestSubject", null));
        assertEquals("[ERR-105] Email content is null or empty.", result.getMessage());
    }

    private void sendMessage(String recipientEmail, String senderEmail, String subject, String message) throws Exception {

        mailSenderService.sendMessage(subject, message, senderEmail, recipientEmail);
    }

    @AfterEach
    void cleanup() throws Exception {
        mockSmtp.stop();
    }
}
