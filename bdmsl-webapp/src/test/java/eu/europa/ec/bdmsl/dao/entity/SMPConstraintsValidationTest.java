/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.entity;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;

import static eu.europa.ec.bdmsl.util.Constant.NEW_CERT_PUBLIC_KEY;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * This test class validates the data model {@link eu.europa.ec.bdmsl.dao.entity.ParticipantIdentifierEntity)} and its constraints
 *
 * @author Flavio SANTOS
 * @since 22/09/2016
 */
class SMPConstraintsValidationTest extends AbstractJUnit5Test {

    @Test
    void persistIntoSMPTableOk() throws SQLException {
        String sqlForSmp = "INSERT INTO bdmsl_smp (smp_id,fk_certificate_id,endpoint_physical_address,endpoint_logical_address,fk_subdomain_id,created_on,last_updated_on) values (?,?,?,?,?,?,?)";
        String sqlForCertificate = "INSERT INTO bdmsl_certificate(id,certificate_id,valid_from,valid_until,pem_encoding,new_cert_change_date,new_cert_id,created_on,last_updated_on) values (?,?,?,?,?,?,?,?,?)";
        Timestamp date = new Timestamp(Calendar.getInstance().getTimeInMillis());
        CertificateConstraintsValidationTest certificateConstraintsValidationTest = new CertificateConstraintsValidationTest();
        ByteArrayInputStream pem = new ByteArrayInputStream(NEW_CERT_PUBLIC_KEY.getBytes());

        certificateConstraintsValidationTest.persist(dataSource.getConnection(), sqlForCertificate, "753951852", "CN=SMP_receiverCN,O=DIGIT_TEST,C=BE:753951852", date, date, pem, null, null, date, date);
        assertDoesNotThrow(() -> persist(sqlForSmp, "SML753951852", "753951852", "10.0.0.1", "test.eu", 1, date, date));
    }

    @Test
    void persistIntoSMPTablePKNotOk() throws SQLException {
        String sqlForCertificate = "INSERT INTO bdmsl_certificate(id,certificate_id,valid_from,valid_until,pem_encoding,new_cert_change_date,new_cert_id,created_on,last_updated_on) values (?,?,?,?,?,?,?,?,?)";
        String sqlForSmp = "INSERT INTO bdmsl_smp (smp_id,fk_certificate_id,endpoint_physical_address,endpoint_logical_address,fk_subdomain_id,created_on,last_updated_on) values (?,?,?,?,?,?,?)";
        Timestamp date = new Timestamp(Calendar.getInstance().getTimeInMillis());
        CertificateConstraintsValidationTest certificateConstraintsValidationTest = new CertificateConstraintsValidationTest();
        ByteArrayInputStream pem = new ByteArrayInputStream(NEW_CERT_PUBLIC_KEY.getBytes());

        certificateConstraintsValidationTest.persist(dataSource.getConnection(), sqlForCertificate, "963852741", "CN=SMP_receiverCN,O=DIGIT_TEST,C=BE:753951852", date, date, pem, null, null, date, date);
        persist(sqlForSmp, "SML963852741", "963852741", "10.0.0.1", "test.eu", 1, date, date);
        assertThrows(SQLException.class,
                () -> persist(sqlForSmp, "SML963852741", "963852741", "10.0.0.1", "test.eu", 1, date, date));
    }

    @Test
    void persistIntoParticipantIdentifierTableFKNotOk() {
        String SQL = "INSERT INTO bdmsl_smp (smp_id,fk_certificate_id,endpoint_physical_address,endpoint_logical_address,fk_subdomain_id,created_on,last_updated_on) values (?,?,?,?,?,?,?)";
        Timestamp date = new Timestamp(Calendar.getInstance().getTimeInMillis());

        assertThrows(SQLException.class,
                () -> persist(SQL, "SML19738264", "1111111111111111111", "10.0.0.1", "test.eu", 1, date, date));
    }

    private void persist(String sqlQuery, String smpId, String fkCertificate, String physicalAddress, String logicalAddress, Integer subdomainId, Timestamp createdOn, Timestamp updatedOn) throws SQLException {
        persist(dataSource.getConnection(), sqlQuery, smpId, fkCertificate, physicalAddress, logicalAddress, subdomainId, createdOn, updatedOn);
    }

    void persist(Connection connection, String sqlQuery, String smpId, String fkCertificate, String physicalAddress, String logicalAddress, Integer subDomainId, Timestamp createdOn, Timestamp updatedOn) throws SQLException {
        try (connection;
             PreparedStatement pstmt = connection.prepareStatement(sqlQuery)) {
            pstmt.setString(1, smpId);
            pstmt.setString(2, fkCertificate);
            pstmt.setString(3, physicalAddress);
            pstmt.setString(4, logicalAddress);
            pstmt.setLong(5, subDomainId);
            pstmt.setTimestamp(6, createdOn);
            pstmt.setTimestamp(7, updatedOn);
            pstmt.executeUpdate();
            connection.commit();
        }
    }
}
