/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.dns;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.business.IDataQualityInconsistencyAnalyzerBusiness;
import eu.europa.ec.bdmsl.business.ISubdomainBusiness;
import eu.europa.ec.bdmsl.test.TestConstants;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.spy;

/**
 * @author Flavio SANTOS
 * @since 14/06/2017
 */
@Component
class DataInconsistencyAnalyzerTest extends AbstractJUnit5Test {

    @Autowired
    private IDataQualityInconsistencyAnalyzerBusiness qualityInconsistencyAnalyzerBusiness;

    @Autowired
    private ISubdomainBusiness subdomainBusiness;

    @BeforeEach
    void initMethod() {
        //given
        subdomainBusiness = Mockito.spy(subdomainBusiness);
        ReflectionTestUtils.setField(qualityInconsistencyAnalyzerBusiness, "dnsClientService", dnsClientService);
    }

    @Test
    void testCheckInconsistenciesEmptyDNS() throws Exception {
        DataInconsistencyAnalyzer dataInconsistencyAnalyzer
                = (DataInconsistencyAnalyzer) ReflectionTestUtils.getField(qualityInconsistencyAnalyzerBusiness, "dataInconsistencyAnalyzer");

        IDataQualityInconsistencyAnalyzerBusiness qualityInconsistencyAnalyzerBusinessMock = spy(qualityInconsistencyAnalyzerBusiness);
        // not return any DNS
        Mockito.doReturn(0).when(dnsClientService).getAllDNSDataForInconsistencyReport(any(), any(), any(), any());
        qualityInconsistencyAnalyzerBusinessMock.checkDataInconsistencies();

        assertEquals(TestConstants.DB_PARTICIPANT_COUNT, dataInconsistencyAnalyzer.getDbTotalOfParticipants());
        assertEquals(TestConstants.DB_SMP_COUNT, dataInconsistencyAnalyzer.getDbTotalOfSMPs());
        assertEquals(TestConstants.DB_DISABLED_SMP_COUNT, dataInconsistencyAnalyzer.getDbTotalInactiveSMPs());
        assertEquals(73, dataInconsistencyAnalyzer.getDataInconsistencyEntries().size());
        assertEquals(29, dataInconsistencyAnalyzer.getDbTotalOfParticipantsCNAME());
        assertEquals(11, dataInconsistencyAnalyzer.getDbTotalOfParticipantsNAPTR());
        assertEquals(0, dataInconsistencyAnalyzer.getDnsTotalOfParticipantsCNAME());
        assertEquals(0, dataInconsistencyAnalyzer.getDnsTotalOfParticipantsNAPTR());
        assertEquals(0, dataInconsistencyAnalyzer.getDnsTotalOfSMPs());
    }
}

