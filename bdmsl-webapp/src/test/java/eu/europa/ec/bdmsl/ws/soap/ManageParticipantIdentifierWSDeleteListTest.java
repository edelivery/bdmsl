/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.ws.soap;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.bo.ParticipantBO;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.dao.IParticipantDAO;
import eu.europa.ec.bdmsl.security.ICertificateAuthentication;
import eu.europa.ec.bdmsl.security.UnsecureAuthentication;
import org.busdox.servicemetadata.locator._1.ParticipantIdentifierPageType;
import org.busdox.transport.identifiers._1.ParticipantIdentifierType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.security.Security;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Adrien FERIAL
 * @since 16/06/2015
 */
class ManageParticipantIdentifierWSDeleteListTest extends AbstractJUnit5Test {

    @Autowired
    private IManageParticipantIdentifierWS manageParticipantIdentifierWS;

    @Autowired
    private IParticipantDAO participantDAO;

    @Autowired
    private ICertificateAuthentication customAuthentication;

    @BeforeEach
    void before() throws TechnicalException {
        UnsecureAuthentication authentication = new UnsecureAuthentication();
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    @Test
    void testDeleteListEmpty() throws Exception {
        ParticipantIdentifierPageType participantType = new ParticipantIdentifierPageType();
        assertThrows(Exception.class,
                () -> manageParticipantIdentifierWS.deleteList(participantType));
    }

    @Test
    void testDeleteListNull() throws Exception {
        assertThrows(Exception.class,
                () -> manageParticipantIdentifierWS.deleteList(null));
    }

    @Test
    void testDeleteListParticipantWithSMPNotCreatedByUser() throws Exception {
        ParticipantIdentifierPageType participantType = createParticipant();
        participantType.getParticipantIdentifiers().get(0).setValue("0007:123456789");
        participantType.getParticipantIdentifiers().get(1).setValue("0060:123456789");
        participantType.setServiceMetadataPublisherID("found");
        assertThrows(NotFoundFault.class,
                () -> manageParticipantIdentifierWS.deleteList(participantType));
    }

    @Test
    void testDeleteListParticipantNotAlreadyExist() throws Exception {
        ParticipantIdentifierPageType participantType = createParticipant();
        participantType.getParticipantIdentifiers().get(0).setValue("0009:523456789");
        participantType.getParticipantIdentifiers().get(1).setValue("0009:9923456789");
        participantType.setServiceMetadataPublisherID("foundUnsecure");
        assertThrows(NotFoundFault.class,
                () -> manageParticipantIdentifierWS.deleteList(participantType));
    }

    @Test
    void testDeleteListParticipantOk() throws Exception {
        // First we ensure the participants don't exist
        // Ids must be case insensitive so we put them in upper case to test the case insensitivity
        final String partId1 = "0009:123456789DELETELIST";
        final String partId2 = "0009:223456789DELETELIST";
        final String smpId = "FOUNDUNSECURE";
        ParticipantBO partBO = new ParticipantBO();
        partBO.setSmpId(smpId);
        partBO.setScheme("iso6523-actorid-upis");
        partBO.setParticipantId(partId1);
        ParticipantBO found = participantDAO.findParticipant(partBO);
        assertNotNull(found);
        partBO.setParticipantId(partId2);
        found = participantDAO.findParticipant(partBO);
        assertNotNull(found);

        // Then we create the participants
        ParticipantIdentifierPageType participantType = createParticipant();
        participantType.getParticipantIdentifiers().get(0).setValue(partId1);
        participantType.getParticipantIdentifiers().get(1).setValue(partId2);
        participantType.setServiceMetadataPublisherID(smpId);
        manageParticipantIdentifierWS.deleteList(participantType);

        // Finally we verify that the participants have been created
        found = participantDAO.findParticipant(partBO);
        assertNull(found);
        partBO.setParticipantId(partId1);
        found = participantDAO.findParticipant(partBO);
        assertNull(found);
    }

    @Test
    void testDeleteWithMigrationPlanned() throws Exception {
        ParticipantIdentifierType participantType1 = new ParticipantIdentifierType();
        participantType1.setScheme("iso6523-actorid-upis");
        participantType1.setValue("0009:toBeDeletedWithMigrationPlanned");
        ParticipantIdentifierPageType participantIdentifierPageType = new ParticipantIdentifierPageType();
        participantIdentifierPageType.getParticipantIdentifiers().add(participantType1);
        participantIdentifierPageType.setServiceMetadataPublisherID("toBeDeletedWithMigrationPlanned");
        assertThrows(UnauthorizedFault.class,
                () -> manageParticipantIdentifierWS.deleteList(participantIdentifierPageType));
    }

    private ParticipantIdentifierPageType createParticipant() {
        ParticipantIdentifierType participantType1 = new ParticipantIdentifierType();
        participantType1.setScheme("iso6523-actorid-upis");

        ParticipantIdentifierType participantType2 = new ParticipantIdentifierType();
        participantType2.setScheme("iso6523-actorid-upis");

        ParticipantIdentifierPageType participantIdentifierPageType = new ParticipantIdentifierPageType();
        participantIdentifierPageType.getParticipantIdentifiers().add(participantType1);
        participantIdentifierPageType.getParticipantIdentifiers().add(participantType2);
        participantIdentifierPageType.setServiceMetadataPublisherID("NotFound");

        return participantIdentifierPageType;
    }


    @Test
    void testDeleteListParticipantWrongSMP() throws Exception {
        // First we ensure the participants don't exist
        // Ids must be case insensitive so we put them in upper case to test the case insensitivity
        final String partId1 = "0009:123456789DELETELIST2";
        final String partId2 = "0009:223456789DELETELIST2";
        final String smpIdNotAuthorized = "foundUnsecure";
        final String smpIdTheRightSMP = "smpForListTest";
        ParticipantBO partBO = new ParticipantBO();
        partBO.setSmpId(smpIdTheRightSMP);
        partBO.setScheme("iso6523-actorid-upis");
        partBO.setParticipantId(partId1);
        ParticipantBO found = participantDAO.findParticipant(partBO);
        assertNotNull(found);
        partBO.setParticipantId(partId2);
        found = participantDAO.findParticipant(partBO);
        assertNotNull(found);

        // Then we create the participants
        ParticipantIdentifierPageType participantType = createParticipant();
        participantType.getParticipantIdentifiers().get(0).setValue(partId1);
        participantType.getParticipantIdentifiers().get(1).setValue(partId2);
        participantType.setServiceMetadataPublisherID(smpIdNotAuthorized);
        assertThrows(NotFoundFault.class,
                () -> manageParticipantIdentifierWS.deleteList(participantType));
    }

    @Test
    void testDeleteListParticipantDifferentDomainOk() throws Exception {
        customAuthentication.blueCoatAuthentication("123456789101112");

        final String partId1 = "urn:ehealth:mx:ncpb-idp";
        final String smpId = "SMP123456789101112";
        final String scheme = "ehealth-ncp-ids";
        ParticipantBO partBO = new ParticipantBO();
        partBO.setSmpId(smpId);
        partBO.setScheme(scheme);
        partBO.setParticipantId(partId1);
        ParticipantBO found = participantDAO.findParticipant(partBO);
        assertNull(found);

        ParticipantIdentifierType participantType1 = new ParticipantIdentifierType();
        participantType1.setScheme(scheme);
        participantType1.setValue(partId1);

        ParticipantIdentifierPageType participantType = new ParticipantIdentifierPageType();
        participantType.getParticipantIdentifiers().add(participantType1);
        participantType.setServiceMetadataPublisherID(smpId);

        manageParticipantIdentifierWS.createList(participantType);

        found = participantDAO.findParticipant(partBO);
        assertNotNull(found);

        manageParticipantIdentifierWS.deleteList(participantType);

        found = participantDAO.findParticipant(partBO);
        assertNull(found);
    }

    @Test
    void testDeleteParticipantOnMultipleDomainsOk() throws Exception {
        // customAuthentication.blueCoatAuthentication("123456789101112");
        // already registered on database
        final String partId = "0009:987testDifDomain";
        final String scheme = "iso6523-actorid-upis";
        final String smpId1 = "toBeUpdated";
        final String smpId2 = "SMP2017";

        ParticipantBO partBO = new ParticipantBO();
        partBO.setScheme(scheme);
        partBO.setParticipantId(partId);

        partBO.setSmpId(smpId1);
        ParticipantBO found1 = participantDAO.findParticipant(partBO);
        partBO.setSmpId(smpId2);
        ParticipantBO found2 = participantDAO.findParticipant(partBO);

        assertNotNull(found1);
        assertNotNull(found2);

        ParticipantIdentifierType participantType1 = new ParticipantIdentifierType();
        participantType1.setScheme(scheme);
        participantType1.setValue(partId);
        ParticipantIdentifierPageType participantType = new ParticipantIdentifierPageType();
        participantType.getParticipantIdentifiers().add(participantType1);
        // delete participant from first list
        participantType.setServiceMetadataPublisherID(smpId1);
        manageParticipantIdentifierWS.deleteList(participantType);
        // then deleted is only one
        partBO.setSmpId(smpId1);
        ParticipantBO resultFound1 = participantDAO.findParticipant(partBO);
        partBO.setSmpId(smpId2);
        ParticipantBO resultFound2 = participantDAO.findParticipant(partBO);
        assertNull(resultFound1);
        assertNotNull(resultFound2);
    }

    @Test
    @Disabled("The test needs to be redesigned")
    void testDeleteListParticipantCertificateNotOk() throws Exception {
        customAuthentication.blueCoatAuthentication();

        final String partId1 = "urn:ehealth:bg:ncpb-txb";
        final String smpId = "SMP123456789101112";
        final String scheme = "ehealth-ncp-ids";
        ParticipantBO partBO = new ParticipantBO();
        partBO.setSmpId(smpId);
        partBO.setScheme(scheme);
        partBO.setParticipantId(partId1);
        ParticipantBO found = participantDAO.findParticipant(partBO);
        assertNull(found);

        ParticipantIdentifierType participantType1 = new ParticipantIdentifierType();
        participantType1.setScheme(scheme);
        participantType1.setValue(partId1);

        ParticipantIdentifierPageType participantType = new ParticipantIdentifierPageType();
        participantType.getParticipantIdentifiers().add(participantType1);
        participantType.setServiceMetadataPublisherID(smpId);


        manageParticipantIdentifierWS.createList(participantType);
        ParticipantBO anotherFound = participantDAO.findParticipant(partBO);
        assertNotNull(anotherFound);
        customAuthentication.blueCoatAuthentication("123456789101112");
        assertThrows(UnauthorizedFault.class,
                () -> {
                    manageParticipantIdentifierWS.deleteList(participantType);
                });


    }
}

