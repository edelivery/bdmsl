/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.dns.impl;

import eu.europa.ec.bdmsl.common.exception.SIG0Exception;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.InputStream;
import java.security.PrivateKey;

import static org.junit.jupiter.api.Assertions.*;


/**
 * Tests for {@link Bind9PrivateKeyParser}
 *
 * @author Joze RIHTARSIC
 * @since 4.3
 */
class Bind9PrivateKeyParserTest extends Sig0KeyTestData {


    @ParameterizedTest
    @MethodSource("keyExamples")
    void testParseKeysType(String keyFileName, String keyAlgName, String keyAlgCode) throws SIG0Exception {
        //when
        InputStream keyStream = getKeyAsInputStream(keyFileName + ".private");
        PrivateKey privateKey = Bind9PrivateKeyParser.toPrivateKey(keyStream);
        assertNotNull(privateKey);
        assertEquals(keyAlgName, privateKey.getAlgorithm());
    }
}
