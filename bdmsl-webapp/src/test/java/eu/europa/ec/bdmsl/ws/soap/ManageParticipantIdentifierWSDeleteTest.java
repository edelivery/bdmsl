/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.ws.soap;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.bo.ParticipantBO;
import eu.europa.ec.bdmsl.dao.IParticipantDAO;
import eu.europa.ec.bdmsl.security.ICertificateAuthentication;
import eu.europa.ec.bdmsl.security.UnsecureAuthentication;
import org.busdox.servicemetadata.locator._1.ServiceMetadataPublisherServiceForParticipantType;
import org.busdox.transport.identifiers._1.ParticipantIdentifierType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.security.Security;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Adrien FERIAL
 * @since 16/06/2015
 */
class ManageParticipantIdentifierWSDeleteTest extends AbstractJUnit5Test {

    @Autowired
    private IManageParticipantIdentifierWS manageParticipantIdentifierWS;

    @Autowired
    private IParticipantDAO participantDAO;

    @Autowired
    private ICertificateAuthentication customAuthentication;

    @BeforeEach
    void testSetup() {
        UnsecureAuthentication authentication = new UnsecureAuthentication();
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    @Test
    void testDeleteEmpty() throws Exception {
        ServiceMetadataPublisherServiceForParticipantType participantType = new ServiceMetadataPublisherServiceForParticipantType();
        assertThrows(BadRequestFault.class, () -> manageParticipantIdentifierWS.delete(participantType));
    }

    @Test
    void testDeleteNull() throws Exception {
        assertThrows(BadRequestFault.class, () -> manageParticipantIdentifierWS.delete(null));
    }

    @Test
    void testDeleteParticipantWithSMPNotCreatedByUser() throws Exception {
        ServiceMetadataPublisherServiceForParticipantType participantType = createParticipant();
        participantType.getParticipantIdentifier().setValue("0007:123456789");
        participantType.setServiceMetadataPublisherID("found");
        assertThrows(NotFoundFault.class, () -> manageParticipantIdentifierWS.delete(participantType));
    }

    @Test
    void testDeleteParticipantNotAlreadyExist() throws Exception {
        ServiceMetadataPublisherServiceForParticipantType participantType = createParticipant();
        participantType.getParticipantIdentifier().setValue("0007:notAlreadyExist");
        participantType.setServiceMetadataPublisherID("foundUnsecure");
        assertThrows(NotFoundFault.class, () -> manageParticipantIdentifierWS.delete(participantType));
    }

    @Test
    void testDeleteParticipantOk() throws Exception {
        // First we ensure that the participant exists (the case is insensitive)
        final String partId = "0009:123456789aLwaySpreSenT";
        final String scheme = "iso6523-actorid-upis";
        final String smpId = "foundUnsecure";
        ParticipantBO partBO = new ParticipantBO();
        partBO.setSmpId(smpId);
        partBO.setScheme(scheme);
        partBO.setParticipantId(partId);
        ParticipantBO found = participantDAO.findParticipant(partBO);
        assertNotNull(found);

        // Then we delete the participant
        ServiceMetadataPublisherServiceForParticipantType participantType = createParticipant(smpId, scheme, partId);
        manageParticipantIdentifierWS.delete(participantType);

        // Finally we verify that the participant has been deleted
        found = participantDAO.findParticipant(partBO);
        assertNull(found);
    }

    /**
     * The SMP doesn't own the participant
     *
     * @throws Exception
     */
    @Test
    void testDeleteParticipantWrongSMP() throws Exception {
        // First we ensure that the participant exists (the case is insensitive)
        final String partId = "0009:123456789aLwaySpreSenT";
        final String scheme = "iso6523-actorid-upis";
        final String smpId = "smpForListTest";
        ParticipantBO partBO = new ParticipantBO();
        partBO.setSmpId(smpId);
        partBO.setScheme("iso6523-actorid-upis");
        partBO.setParticipantId(partId);
        ParticipantBO found = participantDAO.findParticipant(partBO);
        assertNotNull(found);
        // Then we delete the participant
        ServiceMetadataPublisherServiceForParticipantType participantType = createParticipant(smpId, scheme, partId);
        assertThrows(NotFoundFault.class, () -> manageParticipantIdentifierWS.delete(participantType));
    }

    /**
     * Delete participant with specifying the SMP
     *
     * @throws Exception
     */
    @Test
    void testDeleteParticipantWithSMPIdOk() throws Exception {
        // First we ensure that the participant exists (the case is insensitive)
        final String partId = "0009:123456789aLwaySpreSenT2";
        final String scheme = "iso6523-actorid-upis";
        final String smpId = "foundUnsecure";
        ParticipantBO partBO = new ParticipantBO();
        partBO.setSmpId(smpId);
        partBO.setScheme(scheme);
        partBO.setParticipantId(partId);
        ParticipantBO found = participantDAO.findParticipant(partBO);
        assertNotNull(found);

        // Then we delete the participant
        ServiceMetadataPublisherServiceForParticipantType participantType = createParticipant(smpId, scheme, partId);
        manageParticipantIdentifierWS.delete(participantType);

        // Finally we verify that the participant has been deleted
        found = participantDAO.findParticipant(partBO);
        assertNull(found);
    }

    @Test
    void testDeleteWithMigrationPlanned() throws Exception {
        ServiceMetadataPublisherServiceForParticipantType participantType = createParticipant("toBeDeletedWithMigrationPlanned",
                "iso6523-actorid-upis", "0009:toBeDeletedWithMigrationPlanned");
        assertThrows(UnauthorizedFault.class, () -> manageParticipantIdentifierWS.delete(participantType));
    }

    @Test
    void testDeleteParticipantDifferentDomainOk() throws Exception {
        customAuthentication.blueCoatAuthentication("123456789101112");

        final String partId1 = "urn:ehealth:us:ncpb-ed";
        final String smpId = "SMP123456789101112";
        final String scheme = "ehealth-ncp-ids";
        ParticipantBO partBO = new ParticipantBO();
        partBO.setSmpId(smpId);
        partBO.setScheme(scheme);
        partBO.setParticipantId(partId1);
        ParticipantBO found = participantDAO.findParticipant(partBO);
        assertNotNull(found);

        ServiceMetadataPublisherServiceForParticipantType participantType = createParticipant(smpId, scheme, partId1);
        manageParticipantIdentifierWS.delete(participantType);

        found = participantDAO.findParticipant(partBO);
        assertNull(found);
    }


    @Test
    void testDeleteParticipantDifferentDomainCertificateNotOk() throws Exception {
        customAuthentication.blueCoatAuthentication();

        final String partId1 = "urn:ehealth:lu:ncpb-txb";
        final String smpId = "SMP123456789101112";
        final String scheme = "ehealth-ncp-ids";
        ParticipantBO partBO = new ParticipantBO();
        partBO.setSmpId(smpId);
        partBO.setScheme(scheme);
        partBO.setParticipantId(partId1);

        ParticipantBO found = participantDAO.findParticipant(partBO);
        assertNotNull(found);

        ServiceMetadataPublisherServiceForParticipantType participantType = createParticipant(smpId, scheme, partId1);
        assertThrows(UnauthorizedFault.class, () -> manageParticipantIdentifierWS.delete(participantType));
    }


    private ServiceMetadataPublisherServiceForParticipantType createParticipant() {
        return createParticipant("NotFound", "iso6523-actorid-upis", "0088:123456789");
    }

    private ServiceMetadataPublisherServiceForParticipantType createParticipant(String smpId, String participantScheme, String participantIdentifier) {
        ServiceMetadataPublisherServiceForParticipantType participantType = new ServiceMetadataPublisherServiceForParticipantType();
        ParticipantIdentifierType partIdType = new ParticipantIdentifierType();
        partIdType.setScheme(participantScheme);
        partIdType.setValue(participantIdentifier);
        participantType.setParticipantIdentifier(partIdType);
        participantType.setServiceMetadataPublisherID(smpId);
        return participantType;
    }
}

