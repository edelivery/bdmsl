/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.util;


import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import eu.europa.ec.bdmsl.dao.ISubdomainDAO;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;


/**
 * @author Flavio SANTOS
 * @since 03/03/2017
 */
class ConstraintsUtilTest extends AbstractJUnit5Test {

    private static final String TEST_SUBDOMAIN_NAME = "acc.edelivery.tech.ec.europa.eu";
    @Autowired
    private ISubdomainDAO subdomainDAO;

    private static Object[][] loadRegex() {
        return new Object[][]{
                {"AB12aa#$", true},
                {"Aa1#Bb2$", true},
                {"#_ccCC12", true},
                {"LK08aa-_", true},
                {"[{ccCC12", true},
                {"Aa1(Bb2|", true},
                {"Aa1~Bb2}", true},
                {"AB12aa^+SDFGDF2432$fdfdf", true},
                {"AB12aa@1d2345678asdEdrf{", true},
                {"LK08aa8^*", true},
                {"AB12aa+=12345678asdEdrf*d", false},
                {"Aa1&Bb}2", false},
                {"A12aa+=", false},
                {"AB1aa+=", false},
                {"AB12a+=", false},
                {"AB12ab+", false},
                {"QWEQWEQWE()+", false},
                {"Ab1%", false},
                {"AB12aa+=                           DFGD", false},
                {"AB12aa+=   DFGD", false},
                {"AB12aa+= DFGD", false},
                {"AB12aa!?", false}};
    }

    static List<String> wildCardParticipantIdsOk() {
        String[] values = new String[]{"*"};
        return Arrays.asList(values);
    }

    static List<String> wildCardParticipantIdsNotOk() {
        String[] values = new String[]{"0002*",
                "*:1497879101474", "**", "*125254454598555"};
        return Arrays.asList(values);
    }

    static List<String> participantIdsOk() {
        String[] values = new String[]{"0002:1497879101474",
                "0007:1497879101474", "9952:DE123456789012345678901234567890123456789012", "9922:ASCVBNLJKGHJFHGJFGH", "9923:asdadsfSRDTRETFD",
                "0009:1497879101474", "0037:1497879101474", "0060:1497879101474", "0088:1497879101474", "0096:1497879101474", "0097:1497879101474", "0106:1497879101474", "0135:1497879101474", "0142:1497879101474",
                "9901:1497879101474", "9902:1497879101474", "9904:1497879101474", "9905:1497879101474", "9906:1497879101474", "9907:1497879101474", "9908:1497879101474", "9909:1497879101474", "9910:1497879101474",
                "9912:1497879101474", "9913:1497879101474", "9914:1497879101474", "9915:1497879101474", "9916:1497879101474", "9917:1497879101474", "9918:1497879101474", "9919:1497879101474", "9920:1497879101474",
                "9921:1497879101474", "9922:1497879101474", "9923:1497879101474", "9924:1497879101474", "9925:1497879101474", "9926:1497879101474", "9927:1497879101474", "9928:1497879101474", "9929:1497879101474",
                "9930:1497879101474", "9931:1497879101474", "9932:1497879101474", "9933:1497879101474", "9934:1497879101474", "9935:1497879101474", "9936:1497879101474", "9937:1497879101474", "9938:1497879101474",
                "9939:1497879101474", "9940:1497879101474", "9941:1497879101474", "9942:1497879101474", "9943:1497879101474", "9944:1497879101474", "9945:1497879101474", "9946:1497879101474", "9947:1497879101474",
                "9948:1497879101474", "9949:1497879101474", "9950:1497879101474", "9951:1497879101474", "9952:1497879101474", "9953:1497879101474", "9954:1497879101474", "9955:1497879101474", "9956:1497879101474",
                "9957:1497879101474", "0184:1497879101474"};
        return Arrays.asList(values);
    }

    static List<String> participantIdsNotOk() {
        String[] values = new String[]{"9547:1497879101474", "1234:1497879101474",
                "3216:1497879101474", "6549:1497879101474",
                "8523:1497879101474", "7412:1497879101474",
                "8523:1497879101474", "9632:1497879101474",
                "9988:1497879101474", "8527:1497879101474",
                "7417:1497879101474", "9875:1497879101474",
                "98751497879101474", "003751497879101474",
                "00:3751497879101474"};
        return Arrays.asList(values);
    }

    static Stream<String> participantIdsNotOkStream() {
       return participantIdsNotOk().stream();
    }

    static Stream<String> participantIdsOkStream() {
        return participantIdsOk().stream();
    }


    @Test
    void testMigrationKey() throws Exception {
        Object[][] objects = loadRegex();
        String regex = ConstraintsUtil.MIGRATION_KEY_REGEX;
        for (Object[] object : objects) {
            assertEquals(object[1], object[0].toString().matches(regex));
        }
    }

    @Test
    void testLimitOfParticipantList() {
        assertEquals(100, ConstraintsUtil.MAX_PARTICIPANT_LIST);
    }

    @Test
    void testMinMigrationKeyLength() {
        assertEquals(8, ConstraintsUtil.MIN_MIGRATION_KEY_LENGTH);
    }

    @Test
    void testMaxMigrationKeyLength() {
        assertEquals(24, ConstraintsUtil.MAX_MIGRATION_KEY_LENGTH);
    }

    @Test
    void testNumberOfParticipantsPerPage() {
        assertEquals(50, ConstraintsUtil.DEFAULT_PARTICIPANT_PER_PAGE);
    }

    @Test
    void testAddressProtocol() throws Exception {
        String domain = "test.edelivery.com";
        assertTrue(("http://" + domain).matches(ConstraintsUtil.LOGICAL_ADDRESS_PROTOCOL_REGEX));
        assertTrue(("https://" + domain).matches(ConstraintsUtil.LOGICAL_ADDRESS_PROTOCOL_REGEX));
        assertFalse(("http//" + domain).matches(ConstraintsUtil.LOGICAL_ADDRESS_PROTOCOL_REGEX));
        assertFalse(("http:/" + domain).matches(ConstraintsUtil.LOGICAL_ADDRESS_PROTOCOL_REGEX));
        assertFalse(("https" + domain).matches(ConstraintsUtil.LOGICAL_ADDRESS_PROTOCOL_REGEX));
        assertFalse(("http:" + domain).matches(ConstraintsUtil.LOGICAL_ADDRESS_PROTOCOL_REGEX));
        assertFalse(("ttp:" + domain).matches(ConstraintsUtil.LOGICAL_ADDRESS_PROTOCOL_REGEX));
        assertFalse(domain.matches(ConstraintsUtil.LOGICAL_ADDRESS_PROTOCOL_REGEX));
    }


    @Test
    void testParticipantIdsOkForPeppol() throws Exception {

        Optional<SubdomainBO> optSubdomainBO = subdomainDAO.getSubDomainByName(TEST_SUBDOMAIN_NAME);

        assertTrue(optSubdomainBO.isPresent());
        SubdomainBO subdomainBO = optSubdomainBO.get();
        for (String id : participantIdsOk()) {
            assertTrue(id.matches(subdomainBO.getParticipantIdRegexp()));
        }
    }

    @Test
    void testParticipantIdsNotOkForPeppol() throws Exception {
        Optional<SubdomainBO> optSubdomainBO = subdomainDAO.getSubDomainByName(TEST_SUBDOMAIN_NAME);

        assertTrue(optSubdomainBO.isPresent());
        SubdomainBO subdomainBO = optSubdomainBO.get();

        for (String id : participantIdsNotOk()) {
            assertFalse(id.matches(subdomainBO.getParticipantIdRegexp()));
        }
    }

    @Test
    void testWildCardParticipantIdsOk() throws Exception {
        Optional<SubdomainBO> optSubdomainBO = subdomainDAO.getSubDomainByName(TEST_SUBDOMAIN_NAME);

        assertTrue(optSubdomainBO.isPresent());
        SubdomainBO subdomainBO = optSubdomainBO.get();

        assertTrue("*".matches(subdomainBO.getParticipantIdRegexp()));
    }

    @Test
    void testWildCardParticipantIdsNotOk() throws Exception {
        Optional<SubdomainBO> optSubdomainBO = subdomainDAO.getSubDomainByName(TEST_SUBDOMAIN_NAME);

        assertTrue(optSubdomainBO.isPresent());
        SubdomainBO subdomainBO = optSubdomainBO.get();

        for (String id : wildCardParticipantIdsNotOk()) {
            assertFalse(id.matches(subdomainBO.getParticipantIdRegexp()));
        }
    }
}
