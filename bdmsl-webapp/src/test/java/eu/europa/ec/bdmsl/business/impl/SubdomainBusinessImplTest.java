/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.business.impl;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.business.ISubdomainBusiness;
import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import eu.europa.ec.bdmsl.common.exception.BadRequestException;
import eu.europa.ec.bdmsl.common.exception.NotFoundException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.dao.ISubdomainDAO;
import eu.europa.ec.bdmsl.test.CommonTestUtils;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import jakarta.transaction.Transactional;
import java.math.BigInteger;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Tiago MIGUEL
 * @since 29/03/2017
 */
class SubdomainBusinessImplTest extends AbstractJUnit5Test {

    @Autowired
    ISubdomainBusiness subdomainBusiness;


    @Autowired
    private ISubdomainDAO subdomainDAO;

    SubdomainBO testSubDomain = CommonTestUtils.createSubDomainBO("test.ec.europa.eu", "ec.europa.eu", ".*", "all", "all");

    @BeforeEach
    @Transactional
    void before() throws TechnicalException {
        subdomainDAO.createSubDomain(testSubDomain);
    }

    @Test
    @Transactional
    void testFindAll() throws TechnicalException {
        List<SubdomainBO> subdomainBOS = subdomainBusiness.findAll();
        assertFalse(subdomainBOS.isEmpty());
        assertEquals("test.isalive.test", subdomainBOS.get(0).getSubdomainName());
        assertEquals("acc.edelivery.tech.ec.europa.eu", subdomainBOS.get(1).getSubdomainName());
        assertEquals("ehealth.acc.edelivery.tech.ec.europa.eu", subdomainBOS.get(2).getSubdomainName());
        assertEquals("ehealth.edelivery.tech.ec.europa.eu", subdomainBOS.get(3).getSubdomainName());
        assertEquals("sea.acc.edelivery.tech.ec.europa.eu", subdomainBOS.get(4).getSubdomainName());
        assertEquals("toledo.acc.edelivery.tech.ec.europa.eu", subdomainBOS.get(5).getSubdomainName());
        assertEquals("delta.acc.edelivery.tech.ec.europa.eu", subdomainBOS.get(6).getSubdomainName());

    }

    @Test
    @Transactional
    void testFindAllSchemesPerDomain() throws TechnicalException {
        List<String> schemes = subdomainBusiness.findAllSchemesPerDomain();
        assertTrue(!schemes.isEmpty());
        assertTrue(schemes.contains("iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu"));
        assertTrue(schemes.contains("ehealth-ncp-ids.acc.edelivery.tech.ec.europa.eu"));
        assertTrue(schemes.contains("domain-ncp-ids.delta.acc.edelivery.tech.ec.europa.eu"));
    }


    @Test
    @Transactional
    void createSubDomainTest() throws TechnicalException {
        // given
        SubdomainBO subdomainBO = CommonTestUtils.createSubDomainBO("domain.ec.europa.eu", "ec.europa.eu", ".*", "all", "all");
        assertNull(subdomainBO.getSubdomainId());
        // when
        subdomainBusiness.createSubDomain(subdomainBO);

        // then
        Optional<SubdomainBO> res = subdomainDAO.getSubDomainByName(subdomainBO.getSubdomainName());
        assertTrue(res.isPresent());
        assertNotNull(res.get().getSubdomainId());
        assertEquals(subdomainBO.getSubdomainName(), res.get().getSubdomainName());
        assertEquals(subdomainBO.getDnsRecordTypes(), res.get().getDnsRecordTypes());
        assertEquals(subdomainBO.getDnsZone(), res.get().getDnsZone());
        assertEquals(subdomainBO.getParticipantIdRegexp(), res.get().getParticipantIdRegexp());
        assertEquals(subdomainBO.getSmpUrlSchemas(), res.get().getSmpUrlSchemas());
        // no exception occurs
    }

    @Test
    @Transactional
    void createSubDomainTestLowerCase() throws TechnicalException {
        // given
        SubdomainBO subdomainBO = CommonTestUtils.createSubDomainBO("domainTest.EC.europa.EU", "EC.europa.EU", ".*", "ALL", "ALL");
        assertNull(subdomainBO.getSubdomainId());
        // when
        subdomainBusiness.createSubDomain(subdomainBO);

        // then
        Optional<SubdomainBO> res = subdomainDAO.getSubDomainByName(subdomainBO.getSubdomainName());
        assertTrue(res.isPresent());
        assertNotNull(res.get().getSubdomainId());
        assertEquals(subdomainBO.getSubdomainName(), res.get().getSubdomainName());
        assertEquals(subdomainBO.getDnsRecordTypes().toLowerCase(), res.get().getDnsRecordTypes());
        assertEquals(subdomainBO.getDnsZone().toLowerCase(), res.get().getDnsZone());
        assertEquals(subdomainBO.getParticipantIdRegexp(), res.get().getParticipantIdRegexp());
        assertEquals(subdomainBO.getSmpUrlSchemas().toLowerCase(), res.get().getSmpUrlSchemas());
        // no exception occurs
    }

    @Test
    @Transactional
    void databaseConstraintSubDomainDuplicateNameTest() {
        // given
        SubdomainBO subdomainBO = CommonTestUtils.createSubDomainBO(testSubDomain.getSubdomainName(), "ec.europa.eu", ".*", "all", "all");

        // when
        assertThrows(BadRequestException.class, () -> subdomainBusiness.createSubDomain(subdomainBO), "[ERR-106] SubDomain already exists!");
    }

    @Test
    @Transactional
    void createSubdomainMissingZoneTest() {
        // given
        SubdomainBO subdomainBO = CommonTestUtils.createSubDomainBO("test", null, ".*", "all", "all");
        // when
        assertThrows(BadRequestException.class, () -> subdomainBusiness.createSubDomain(subdomainBO), "[ERR-106] DNS Zone cannot be 'null' or empty!");
    }

    @Test
    @Transactional
    void createSubdomainInvalidZoneTest() {
        // given
        SubdomainBO subdomainBO = CommonTestUtils.createSubDomainBO("test", "?adinvlalid-.Test+zone.eu", ".*", "all", "all");

        // when
        assertThrows(BadRequestException.class, () -> subdomainBusiness.createSubDomain(subdomainBO), "Invalid DNS zone!");
    }

    @Test
    @Transactional
    void createSubdomainEmptyReqExpTest() {
        // given
        SubdomainBO subdomainBO = CommonTestUtils.createSubDomainBO("test.ec.europa.eu", "ec.europa.eu", "", "all", "all");

        BadRequestException fault = assertThrows(BadRequestException.class, () -> subdomainBusiness.createSubDomain(subdomainBO));
        assertThat(fault.getMessage(),
                CoreMatchers.startsWith("[ERR-106] Regular expression for field [ParticipantRegularExpression] cannot be 'null' or empty!"));
    }

    @Test
    @Transactional
    void createSubdomainInvalidReqExpTest() {
        // given
        SubdomainBO subdomainBO = CommonTestUtils.createSubDomainBO("test.ec.europa.eu", "ec.europa.eu", "((==+**", "all", "all");

        BadRequestException fault = assertThrows(BadRequestException.class, () -> subdomainBusiness.createSubDomain(subdomainBO));
        assertThat(fault.getMessage(),
                CoreMatchers.startsWith("[ERR-106] Invalid regular expression for field [ParticipantRegularExpression]!"));

    }

   /* @Test
    @Transactional
    void createSubdomainNullScheme() throws TechnicalException {
        // given
        SubdomainBO subdomainBO = CommonTestUtils.createSubDomainBO("test.ec.europa.eu", "ec.europa.eu", ".*", "all", null);
        expectedEx.expect(BadRequestException.class);
        expectedEx.expectMessage("[ERR-106] Url scheme cannot be 'null' or empty! Valid values: http, https, all.");

        // when
        subdomainBusiness.createSubDomain(subdomainBO);
    }

    @Test
    @Transactional
    void createSubdomainInvalidScheme() throws TechnicalException {
        // given
        SubdomainBO subdomainBO = CommonTestUtils.createSubDomainBO("test.ec.europa.eu", "ec.europa.eu", ".*", "all", "ftp");
        expectedEx.expect(BadRequestException.class);
        expectedEx.expectMessage("[ERR-106] Invalid url scheme! Valid values: http, https, all.");

        // when
        subdomainBusiness.createSubDomain(subdomainBO);
    }

    @Test
    @Transactional
    void createSubdomainNullNaptrType() throws TechnicalException {
        // given
        SubdomainBO subdomainBO = CommonTestUtils.createSubDomainBO("test.ec.europa.eu", "ec.europa.eu", ".*", null, "all");
        expectedEx.expect(BadRequestException.class);
        expectedEx.expectMessage("[ERR-106] DNS record type cannot be 'null' or empty! Valid values: cname, naptr, all.");

        // when
        subdomainBusiness.createSubDomain(subdomainBO);
    }

    @Test
    @Transactional
    void createSubdomainValidNaptrType() throws TechnicalException {
        // given
        SubdomainBO subdomainBO = CommonTestUtils.createSubDomainBO("test.ec.europa.eu", "ec.europa.eu", ".*", "mdx", "all");
        expectedEx.expect(BadRequestException.class);
        expectedEx.expectMessage("[ERR-106] Invalid DNS record type! Valid values: cname, naptr, all.");

        // when
        subdomainBusiness.createSubDomain(subdomainBO);
    }
*/

    @Test
    @Transactional
    void updateSubDomainTestAll() throws TechnicalException {

        BigInteger maxParticipantCountForDomain = BigInteger.valueOf(1000000);
        BigInteger maxParticipantCountForSMP = BigInteger.valueOf(99999);
        String certificatePolicy = "2.5.29.32.0";
        // when
        subdomainBusiness.updateSubDomain(testSubDomain.getSubdomainName(),
                "test-regular-value", "cname", "https", "CertRegExp", certificatePolicy,
                maxParticipantCountForDomain, maxParticipantCountForSMP);

        // then
        Optional<SubdomainBO> res = subdomainDAO.getSubDomainByName(testSubDomain.getSubdomainName());
        assertNotNull(res.get().getSubdomainId());
        assertEquals("cname", res.get().getDnsRecordTypes());
        assertEquals("test-regular-value", res.get().getParticipantIdRegexp());
        assertEquals("https", res.get().getSmpUrlSchemas());
        assertEquals("CertRegExp", res.get().getSmpCertSubjectRegex());
        assertEquals(certificatePolicy, res.get().getSmpCertPolicyOIDs());
        assertEquals(maxParticipantCountForDomain, res.get().getMaxParticipantCountForDomain());
        assertEquals(maxParticipantCountForSMP, res.get().getMaxParticipantCountForSMP());
    }

    @Transactional
    void updateSubDomainTestLowerCaseAll() throws TechnicalException {

        // when
        subdomainBusiness.updateSubDomain(testSubDomain.getSubdomainName(),
                "test-regular-value", "CNAME", "HTTPS", "", "", null, null);


        // then
        Optional<SubdomainBO> res = subdomainDAO.getSubDomainByName(testSubDomain.getSubdomainName());
        assertNotNull(res.get().getSubdomainId());
        assertEquals("cname", res.get().getDnsRecordTypes());
        assertEquals("test-regular-value", res.get().getParticipantIdRegexp());
        assertEquals("https", res.get().getSmpUrlSchemas());
    }

    @Test
    @Transactional
    void updateSubDomainTestOnlyRegularExp() throws TechnicalException {
        // when
        subdomainBusiness.updateSubDomain(testSubDomain.getSubdomainName(),
                "test-regular-value", null, null, null, null, null, null);

        // then
        Optional<SubdomainBO> res = subdomainDAO.getSubDomainByName(testSubDomain.getSubdomainName());
        assertNotNull(res.get().getSubdomainId());
        assertEquals(testSubDomain.getDnsRecordTypes(), res.get().getDnsRecordTypes());
        assertEquals("test-regular-value", res.get().getParticipantIdRegexp());
        assertNotNull(testSubDomain.getParticipantIdRegexp(), res.get().getParticipantIdRegexp());
        assertEquals(testSubDomain.getSmpUrlSchemas(), res.get().getSmpUrlSchemas());
        assertEquals(testSubDomain.getSmpCertSubjectRegex(), res.get().getSmpCertSubjectRegex());
        assertEquals(testSubDomain.getSmpCertPolicyOIDs(), res.get().getSmpCertPolicyOIDs());
        assertEquals(testSubDomain.getMaxParticipantCountForDomain(), res.get().getMaxParticipantCountForDomain());
        assertEquals(testSubDomain.getMaxParticipantCountForSMP(), res.get().getMaxParticipantCountForSMP());
    }

    @Test
    @Transactional
    void updateSubDomainTestOnlyDNSType() throws TechnicalException {
        // when
        subdomainBusiness.updateSubDomain(testSubDomain.getSubdomainName(),
                null, "CNAME", null, null, null, null, null);

        // then
        Optional<SubdomainBO> res = subdomainDAO.getSubDomainByName(testSubDomain.getSubdomainName());
        assertNotNull(res.get().getSubdomainId());
        assertEquals("cname", res.get().getDnsRecordTypes());
        assertNotEquals(testSubDomain.getDnsRecordTypes(), res.get().getDnsRecordTypes());
        assertEquals(testSubDomain.getParticipantIdRegexp(), res.get().getParticipantIdRegexp());
        assertEquals(testSubDomain.getSmpUrlSchemas(), res.get().getSmpUrlSchemas());
        assertEquals(testSubDomain.getSmpCertSubjectRegex(), res.get().getSmpCertSubjectRegex());
        assertEquals(testSubDomain.getSmpCertPolicyOIDs(), res.get().getSmpCertPolicyOIDs());
        assertEquals(testSubDomain.getMaxParticipantCountForDomain(), res.get().getMaxParticipantCountForDomain());
        assertEquals(testSubDomain.getMaxParticipantCountForSMP(), res.get().getMaxParticipantCountForSMP());
    }

    @Test
    @Transactional
    void updateSubDomainTestOnlyURLScheme() throws TechnicalException {
        // when
        subdomainBusiness.updateSubDomain(testSubDomain.getSubdomainName(),
                null, null, "HTTPS", null, null, null, null);

        // then
        Optional<SubdomainBO> res = subdomainDAO.getSubDomainByName(testSubDomain.getSubdomainName());
        assertNotNull(res.get().getSubdomainId());
        assertEquals(testSubDomain.getDnsRecordTypes(), res.get().getDnsRecordTypes());
        assertEquals(testSubDomain.getParticipantIdRegexp(), res.get().getParticipantIdRegexp());
        assertEquals("https", res.get().getSmpUrlSchemas());
        assertNotEquals(testSubDomain.getSmpUrlSchemas(), res.get().getSmpUrlSchemas());
        assertEquals(testSubDomain.getSmpCertSubjectRegex(), res.get().getSmpCertSubjectRegex());
        assertEquals(testSubDomain.getSmpCertPolicyOIDs(), res.get().getSmpCertPolicyOIDs());
        assertEquals(testSubDomain.getMaxParticipantCountForDomain(), res.get().getMaxParticipantCountForDomain());
        assertEquals(testSubDomain.getMaxParticipantCountForSMP(), res.get().getMaxParticipantCountForSMP());
    }

    @Test
    @Transactional
    void updateSubDomainTestOnlyCertRegExp() throws TechnicalException {
        // when
        subdomainBusiness.updateSubDomain(testSubDomain.getSubdomainName(),
                null, null, null, "NewRegExp", null, null, null);

        // then
        Optional<SubdomainBO> res = subdomainDAO.getSubDomainByName(testSubDomain.getSubdomainName());
        assertNotNull(res.get().getSubdomainId());
        assertEquals(testSubDomain.getDnsRecordTypes(), res.get().getDnsRecordTypes());
        assertEquals(testSubDomain.getParticipantIdRegexp(), res.get().getParticipantIdRegexp());
        assertEquals(testSubDomain.getSmpUrlSchemas(), res.get().getSmpUrlSchemas());
        assertEquals("NewRegExp", res.get().getSmpCertSubjectRegex());
        assertEquals(testSubDomain.getSmpCertPolicyOIDs(), res.get().getSmpCertPolicyOIDs());
        assertEquals(testSubDomain.getMaxParticipantCountForDomain(), res.get().getMaxParticipantCountForDomain());
        assertEquals(testSubDomain.getMaxParticipantCountForSMP(), res.get().getMaxParticipantCountForSMP());
    }

    @Test
    @Transactional
    void updateSubDomainTestOnlyCertPolicyOIDs() throws TechnicalException {
        // when
        subdomainBusiness.updateSubDomain(testSubDomain.getSubdomainName(),
                null, null, null, null, "0.4.0.194112.1.3", null, null);

        // then
        Optional<SubdomainBO> res = subdomainDAO.getSubDomainByName(testSubDomain.getSubdomainName());
        assertNotNull(res.get().getSubdomainId());
        assertEquals(testSubDomain.getDnsRecordTypes(), res.get().getDnsRecordTypes());
        assertEquals(testSubDomain.getParticipantIdRegexp(), res.get().getParticipantIdRegexp());
        assertEquals(testSubDomain.getSmpUrlSchemas(), res.get().getSmpUrlSchemas());
        assertEquals(testSubDomain.getSmpCertSubjectRegex(), res.get().getSmpCertSubjectRegex());
        assertEquals("0.4.0.194112.1.3", res.get().getSmpCertPolicyOIDs());
        assertEquals(testSubDomain.getMaxParticipantCountForDomain(), res.get().getMaxParticipantCountForDomain());
        assertEquals(testSubDomain.getMaxParticipantCountForSMP(), res.get().getMaxParticipantCountForSMP());
    }

    @Test
    @Transactional
    void updateSubDomainTestOnlyMaxParticipantCountForDomain() throws TechnicalException {
        // when
        BigInteger maxParticipantCountForDomain = BigInteger.valueOf(1234567);
        subdomainBusiness.updateSubDomain(testSubDomain.getSubdomainName(),
                null, null, null, null, null, maxParticipantCountForDomain, null);

        // then
        Optional<SubdomainBO> res = subdomainDAO.getSubDomainByName(testSubDomain.getSubdomainName());
        assertNotNull(res.get().getSubdomainId());
        assertEquals(testSubDomain.getDnsRecordTypes(), res.get().getDnsRecordTypes());
        assertEquals(testSubDomain.getParticipantIdRegexp(), res.get().getParticipantIdRegexp());
        assertEquals(testSubDomain.getSmpUrlSchemas(), res.get().getSmpUrlSchemas());
        assertEquals(testSubDomain.getSmpUrlSchemas(), res.get().getSmpUrlSchemas());
        assertEquals(testSubDomain.getSmpCertSubjectRegex(), res.get().getSmpCertSubjectRegex());
        assertEquals(testSubDomain.getSmpCertPolicyOIDs(), res.get().getSmpCertPolicyOIDs());
        assertEquals(maxParticipantCountForDomain, res.get().getMaxParticipantCountForDomain());
        assertEquals(testSubDomain.getMaxParticipantCountForSMP(), res.get().getMaxParticipantCountForSMP());
    }

    @Test
    @Transactional
    void updateSubDomainTestOnlyMaxParticipantCountForSMP() throws TechnicalException {
        // when
        BigInteger maxParticipantCountForSMP = BigInteger.valueOf(1234);
        subdomainBusiness.updateSubDomain(testSubDomain.getSubdomainName(),
                null, null, null, null, null, null, maxParticipantCountForSMP);

        // then
        Optional<SubdomainBO> res = subdomainDAO.getSubDomainByName(testSubDomain.getSubdomainName());
        assertNotNull(res.get().getSubdomainId());
        assertEquals(testSubDomain.getDnsRecordTypes(), res.get().getDnsRecordTypes());
        assertEquals(testSubDomain.getParticipantIdRegexp(), res.get().getParticipantIdRegexp());
        assertEquals(testSubDomain.getSmpUrlSchemas(), res.get().getSmpUrlSchemas());
        assertEquals(testSubDomain.getSmpUrlSchemas(), res.get().getSmpUrlSchemas());
        assertEquals(testSubDomain.getSmpCertSubjectRegex(), res.get().getSmpCertSubjectRegex());
        assertEquals(testSubDomain.getSmpCertPolicyOIDs(), res.get().getSmpCertPolicyOIDs());
        assertEquals(testSubDomain.getMaxParticipantCountForDomain(), res.get().getMaxParticipantCountForDomain());
        assertEquals(maxParticipantCountForSMP, res.get().getMaxParticipantCountForSMP());
    }

    @Test
    @Transactional
    void updateSubDomainNotExists() throws TechnicalException {
        // given
        String randomString = UUID.randomUUID().toString();

        assertThrows(NotFoundException.class, () -> subdomainBusiness.updateSubDomain(randomString,
                "test-regular-value", "CNAME", "HTTPS", "CertSMPRegExp", null, null, null), "[ERR-118] SubDomain does not exist: " + randomString);
    }

    @Test
    @Transactional
    void updateSubDomainNegativeOrNullMaxParticipantsForDomain() {
        // given
        BigInteger maxParticipantCountForDomain = BigInteger.valueOf(0);

        BadRequestException fault = assertThrows(BadRequestException.class, () -> subdomainBusiness.updateSubDomain(testSubDomain.getSubdomainName(),
                null, null, null, null, null, maxParticipantCountForDomain, null));
        assertThat(fault.getMessage(),
                CoreMatchers.startsWith("[ERR-106] Maximum number of participants for the Domain must be 'null' or an integer number greater than 0!"));
    }

    @Test
    @Transactional
    void updateSubDomainNegativeMaxParticipantsForSMP() throws TechnicalException {
        // given
        BigInteger maxParticipantCountForSMP = BigInteger.valueOf(-1);

        BadRequestException fault = assertThrows(BadRequestException.class, () -> subdomainBusiness.updateSubDomain(testSubDomain.getSubdomainName(),
                null, null, null, null, null, null, maxParticipantCountForSMP));
        assertThat(fault.getMessage(),
                CoreMatchers.startsWith("[ERR-106] Maximum number of participants for the SMP must be 'null' or an integer number greater than 0!"));
    }


    @Test
    @Transactional
    void deleteSubDomain() throws TechnicalException {

        Optional<SubdomainBO> resBefore = subdomainDAO.getSubDomainByName(testSubDomain.getSubdomainName());
        assertTrue(resBefore.isPresent());

        // when
        subdomainBusiness.deleteSubDomain(testSubDomain.getSubdomainName());

        // then
        Optional<SubdomainBO> resAfter = subdomainDAO.getSubDomainByName(testSubDomain.getSubdomainName());
        assertFalse(resAfter.isPresent());
    }

    @Test
    @Transactional
    void deleteSubDomainNotExists() throws TechnicalException {
        // given
        String randomString = UUID.randomUUID().toString();
        // then-when
        assertThrows(NotFoundException.class, () -> subdomainBusiness.deleteSubDomain(randomString), "[ERR-118] SubDomain does not exist: " + randomString);
    }

}
