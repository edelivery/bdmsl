/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.ws.soap;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.bo.MigrationRecordBO;
import eu.europa.ec.bdmsl.common.bo.ParticipantBO;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.dao.IMigrationDAO;
import eu.europa.ec.bdmsl.dao.IParticipantDAO;
import eu.europa.ec.bdmsl.security.ICertificateAuthentication;
import eu.europa.ec.bdmsl.security.UnsecureAuthentication;
import org.busdox.servicemetadata.locator._1.MigrationRecordType;
import org.busdox.transport.identifiers._1.ParticipantIdentifierType;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.security.Security;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Adrien FERIAL
 * @since 16/06/2015
 */
class ManageParticipantIdentifierWSMigrateTest extends AbstractJUnit5Test {

    @Autowired
    private IManageParticipantIdentifierWS manageParticipantIdentifierWS;

    @Autowired
    private IMigrationDAO migrationDAO;
    @Autowired
    private IParticipantDAO participantDAO;
    @Autowired
    private ICertificateAuthentication customAuthentication;

    @BeforeAll
    static void beforeClass() throws TechnicalException {
        UnsecureAuthentication authentication = new UnsecureAuthentication();
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    @Test
    void testMigrateButPrepareToMigrateWasNotYetCalledOk() throws Exception {
        MigrationRecordType migrationRecordType = createMigrationRecord();
        migrationRecordType.getParticipantIdentifier().setValue("0009:123456789AlwaysPresent");
        migrationRecordType.setServiceMetadataPublisherID("foundUnsecure");
        assertThrows(NotFoundFault.class, () -> manageParticipantIdentifierWS.migrate(migrationRecordType),
                "Participant was not prepared for migration!");
    }

    @Test
    void testMigrateOk() throws Exception {
        // Ids must be case insensitive so we put them in upper case to test the case insensitivity
        String smpId = "SMPFORMIGRATETEST";
        String participantId = "0009:223456789MIGRATETEST1";

        // verify initial data
        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId(participantId);
        participantBO.setScheme("iso6523-actorid-upis");
        participantBO.setSmpId("FOUNDUNSECURE");
        assertNotNull(participantDAO.findParticipant(participantBO));

        MigrationRecordBO migrationRecordBO = new MigrationRecordBO();
        migrationRecordBO.setOldSmpId(smpId);
        migrationRecordBO.setParticipantId(participantId);
        migrationRecordBO.setScheme("iso6523-actorid-upis");
        MigrationRecordBO found = migrationDAO.findNonMigratedRecord(migrationRecordBO);
        assertFalse(found.isMigrated());

        // Perform the migration
        MigrationRecordType migrationRecordType = createMigrationRecord();
        migrationRecordType.getParticipantIdentifier().setValue(participantId);
        migrationRecordType.setServiceMetadataPublisherID(smpId);
        migrationRecordType.setMigrationKey("@Df1Og2#");
        manageParticipantIdentifierWS.migrate(migrationRecordType);

        // check that the migration is done
        found.setNewSmpId(smpId);
        found = migrationDAO.findMigratedRecord(found);
        assertTrue(found.isMigrated());

        // check that the participant has been updated
        participantBO.setSmpId(smpId);
        assertNotNull(participantDAO.findParticipant(participantBO));
    }

    @Test
    void testMigrateEmpty() throws Exception {
        MigrationRecordType migrationRecordType = new MigrationRecordType();
        assertThrows(BadRequestFault.class, () -> manageParticipantIdentifierWS.migrate(migrationRecordType));
    }

    @Test
    void testMigrateNull() throws Exception {
        assertThrows(BadRequestFault.class, () -> manageParticipantIdentifierWS.migrate(null));
    }

    @Test
    void testMigrateSMPNotExist() throws Exception {
        MigrationRecordType migrationRecordType = createMigrationRecord();
        migrationRecordType.getParticipantIdentifier().setValue("0007:123456789");
        migrationRecordType.setServiceMetadataPublisherID("NotFound");
        assertThrows(NotFoundFault.class, () -> manageParticipantIdentifierWS.migrate(migrationRecordType));
    }

    @Test
    void testMigrateParticipantNotExistAnymore() throws Exception {
        MigrationRecordType migrationRecordType = createMigrationRecord();
        migrationRecordType.getParticipantIdentifier().setValue("0007:notAlreadyExist");
        migrationRecordType.setServiceMetadataPublisherID("foundUnsecure");
        assertThrows(NotFoundFault.class, () -> manageParticipantIdentifierWS.migrate(migrationRecordType));
    }

    @Test
    void testMigrateMigrationCodeTooLong() throws Exception {
        MigrationRecordType migrationRecordType = createMigrationRecord();
        migrationRecordType.getParticipantIdentifier().setValue("0007:123456789");
        migrationRecordType.setServiceMetadataPublisherID("foundUnsecure");
        migrationRecordType.setMigrationKey("TooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLong");
        assertThrows(BadRequestFault.class, () -> manageParticipantIdentifierWS.migrate(migrationRecordType));
    }

    private MigrationRecordType createMigrationRecord() {
        MigrationRecordType migrationRecordType = new MigrationRecordType();
        migrationRecordType.setMigrationKey("_Ae4B3s^");
        ParticipantIdentifierType participantIdentifierType = new ParticipantIdentifierType();
        participantIdentifierType.setValue("0007:123456789");
        participantIdentifierType.setScheme("iso6523-actorid-upis");
        migrationRecordType.setParticipantIdentifier(participantIdentifierType);
        migrationRecordType.setServiceMetadataPublisherID("found");
        return migrationRecordType;
    }

    @Test
    void testMigrateDifferentDomainFail() throws Exception {
        customAuthentication.blueCoatAuthentication("123456789");

        String smpId = "SMP2016"; // invoice domain (not ehealth)
        String newSmpId = "SMP123456789"; // ehealth domain
        String participantId = "urn:ehealth:pl:ncpb-txb";
        String scheme = "ehealth-ncp-ids";

        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId(participantId);
        participantBO.setScheme(scheme);
        participantBO.setSmpId(smpId);
        assertNotNull(participantDAO.findParticipant(participantBO));

        MigrationRecordBO migrationRecordBO = new MigrationRecordBO();
        migrationRecordBO.setOldSmpId(smpId);
        migrationRecordBO.setParticipantId(participantId);
        migrationRecordBO.setScheme(scheme);
        MigrationRecordBO found = migrationDAO.findNonMigratedRecord(migrationRecordBO);
        assertFalse(found.isMigrated());

        // Perform the migration
        MigrationRecordType migrationRecordType = createMigrationRecord();
        migrationRecordType.getParticipantIdentifier().setValue(participantId);
        migrationRecordType.getParticipantIdentifier().setScheme(scheme);
        migrationRecordType.setServiceMetadataPublisherID(newSmpId);
        migrationRecordType.setMigrationKey("@=aFa15B");

        assertThrows(InternalErrorFault.class, () -> manageParticipantIdentifierWS.migrate(migrationRecordType),
                " Participant can be migrated only within the same domain!");
    }

    @Test
    void testMigrateDifferentDomainSchemeNotOk() throws Exception {
        customAuthentication.blueCoatAuthentication("123456789101112");

        String smpId = "SMP2016";
        String newSmpId = "SMP123456789101112";
        String participantId = "urn:ehealth:pl:ncpb-txb";
        String scheme = "ehealth-ncp-ids";

        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId(participantId);
        participantBO.setScheme(scheme);
        participantBO.setSmpId(smpId);
        assertNotNull(participantDAO.findParticipant(participantBO));

        MigrationRecordBO migrationRecordBO = new MigrationRecordBO();
        migrationRecordBO.setOldSmpId(smpId);
        migrationRecordBO.setParticipantId(participantId);
        migrationRecordBO.setScheme(scheme);
        MigrationRecordBO found = migrationDAO.findNonMigratedRecord(migrationRecordBO);
        assertFalse(found.isMigrated());

        MigrationRecordType migrationRecordType = createMigrationRecord();
        migrationRecordType.getParticipantIdentifier().setValue(participantId);
        migrationRecordType.getParticipantIdentifier().setScheme("iso6523-actorid-upis");
        migrationRecordType.setServiceMetadataPublisherID(newSmpId);
        migrationRecordType.setMigrationKey("azsxdc123");
        assertThrows(BadRequestFault.class, () -> manageParticipantIdentifierWS.migrate(migrationRecordType));
    }
}

