/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.entity;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;

import static java.sql.Types.INTEGER;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * This test class validates the data model {@link eu.europa.ec.bdmsl.dao.entity.CertificateDomainEntity)} and its constraints
 *
 * @author Flavio SANTOS
 * @since 22/09/2016
 */
class CertificateDomainConstraintsValidationTest extends AbstractJUnit5Test {

    @Test
    void persistIntoCertificateDomainTableOk() throws SQLException {
        Timestamp date = new Timestamp(Calendar.getInstance().getTimeInMillis());

        assertDoesNotThrow(() -> persistDomainCertificate("CN=unsecure_root,O=delete_in_production,C=only_for_testing_DIGIT_123456", 1, "http://crl.globalsign.net/root.crl", date, date, false, false));
    }

    @Test
    void persistIntoCertificateDomainTableOkWithoutCRL() throws SQLException {
        Timestamp date = new Timestamp(Calendar.getInstance().getTimeInMillis());

        assertDoesNotThrow(() -> persistDomainCertificate("CN=unsecure_root,O=delete_in_production,C=only_for_testing_DIGIT", 2, null, date, date, false, false));
    }

    @Test
    void persistIntoCertificateDomainTablePKNull() {
        Timestamp date = new Timestamp(Calendar.getInstance().getTimeInMillis());

        assertThrows(SQLException.class,
                () -> persistDomainCertificate(null, 1, "http://crl.globalsign.net/root.crl", date, date, false, false));
    }

    @Test
    void persistIntoCertificateDomainTableDomainNull()  {
        Timestamp date = new Timestamp(Calendar.getInstance().getTimeInMillis());

        assertThrows(SQLException.class,
                () -> persistDomainCertificate("CN=unsecure_root,O=delete_in_production,C=only_for_testing_DIGIT_2", null, "http://crl.globalsign.net/root.crl", date, date, false, false));
    }

    private void persistDomainCertificate(String root_certificate_alias, Integer subdomainId, String crl, Timestamp createdOn, Timestamp updatedOn, boolean isRootCA, boolean isAdmin) throws SQLException {
        String sql = "INSERT INTO bdmsl_certificate_domain(certificate,fk_subdomain_id,crl_url,created_on,last_updated_on,is_root_ca, is_admin) values (?,?,?,?,?,?,?)";
        try (Connection connection = dataSource.getConnection();
                PreparedStatement pstmt = connection.prepareStatement(sql);) {
            pstmt.setString(1, root_certificate_alias);
            if (subdomainId == null) {
                pstmt.setNull(2, INTEGER);
            } else {
                pstmt.setLong(2, subdomainId);
            }
            pstmt.setString(3, crl);
            pstmt.setTimestamp(4, createdOn);
            pstmt.setTimestamp(5, updatedOn);
            pstmt.setBoolean(6, isRootCA);
            pstmt.setBoolean(7, isAdmin);
            pstmt.executeUpdate();
            connection.commit();
        }
    }
}
