/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.ws.soap;

import ec.services.wsdl.bdmsl.admin.data._1.DeleteSubDomainRequest;
import ec.services.wsdl.bdmsl.admin.data._1.GetSubDomainRequest;
import ec.services.wsdl.bdmsl.admin.data._1.SubDomainType;
import ec.services.wsdl.bdmsl.admin.data._1.UpdateSubDomainRequest;
import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.enums.SMLRoleEnum;
import eu.europa.ec.bdmsl.common.exception.Messages;
import eu.europa.ec.bdmsl.security.CertificateAuthentication;
import eu.europa.ec.edelivery.security.PreAuthenticatedCertificatePrincipal;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.math.BigInteger;
import java.security.Principal;
import java.util.Collections;

import static eu.europa.ec.bdmsl.util.DomiSMLAssertions.assertThrowsWithContainsMessage;
import static org.junit.jupiter.api.Assertions.*;

class BDMSLAdminServiceWSSubDomainTest extends AbstractJUnit5Test {

    @Autowired
    private IBDMSLAdminServiceWS bdmslAdminServiceWS;


    @BeforeEach
    void before() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        String certHeaderValue = "serial=48:b6:81:ee:8e:0d:cc:08&subject=EMAILADDRESS=receiver@test.be,C=BE, O=DIGIT, OU=FOR TEST ONLY,CN=DIGIT_SMP_TECH_TEAM_3&validfrom=Feb  1 14:20:18 2017 GMT&validto=Jul  9 23:59:00 2019 GMT&issuer=C=BE, O=DIGIT, OU=FOR TEST ONLY, CN=DIGIT_SMP_TECH_TEAM_2";
        Principal principal = blueCoatAuthenticationFilter.buildPrincipalFromHeader(certHeaderValue);
        Authentication authentication = new CertificateAuthentication((PreAuthenticatedCertificatePrincipal) principal, Collections.singletonList(SMLRoleEnum.ROLE_ADMIN.getAuthority()));
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);

    }

    @Test
    void testCreateSubDomainNullRequest() {
        assertThrowsWithContainsMessage(BadRequestFault.class,
                () -> bdmslAdminServiceWS.createSubDomain(null),
                "[ERR-106] The input values must not be null!");
    }

    @Test
    void testDeleteSubDomainNullRequest() {
        assertThrowsWithContainsMessage(BadRequestFault.class,
                () -> bdmslAdminServiceWS.deleteSubDomain(null),
                "[ERR-106] The input values must not be null!");
    }

    @Test
    void testGetSubDomainNullRequest() {

        assertThrowsWithContainsMessage(BadRequestFault.class,
                () -> bdmslAdminServiceWS.getSubDomain(null),
                "[ERR-106] The input values must not be null!");
    }

    @Test
    void testUpdateSubDomainNullRequest() {
        assertThrowsWithContainsMessage(BadRequestFault.class,
                () -> bdmslAdminServiceWS.updateSubDomain(null),
                "[ERR-106] The input values must not be null!");
    }

    @Test
    void testCreateSubDomainEmptyRequest() {
        assertThrowsWithContainsMessage(BadRequestFault.class,
                () -> bdmslAdminServiceWS.createSubDomain(new SubDomainType()),
                "[ERR-106] SubDomain name cannot be 'null' or empty!");
    }


    @Test
    void testDeleteSubDomainEmptyRequest() {
        assertThrowsWithContainsMessage(BadRequestFault.class,
                () -> bdmslAdminServiceWS.deleteSubDomain(new DeleteSubDomainRequest()),
                "[ERR-106] SubDomain name cannot be 'null' or empty!");
    }

    @Test
    void testGetSubDomainEmptyRequest() {
        assertThrowsWithContainsMessage(BadRequestFault.class,
                () -> bdmslAdminServiceWS.getSubDomain(new GetSubDomainRequest()),
                "[ERR-106] SubDomain name cannot be 'null' or empty!");
    }

    @Test
    void testUpdateSubDomainEmptyRequest() {
        assertThrowsWithContainsMessage(BadRequestFault.class,
                () -> bdmslAdminServiceWS.updateSubDomain(new UpdateSubDomainRequest()),
                "[ERR-106] SubDomain name cannot be 'null' or empty!");
    }

    @Test
    void testCreateSubDomainEmptySubDomainName() {
        // given
        SubDomainType request = generateSubdomain();
        request.setSubDomainName("");

        assertThrowsWithContainsMessage(BadRequestFault.class,
                () -> bdmslAdminServiceWS.createSubDomain(request),
                Messages.BAD_REQUEST_PARAMETER_EMPTY_SUBDOMAIN);
    }

    @Test
    void testCreateSubDomainEmptyDnsZone() {
        // given
        SubDomainType request = generateSubdomain();
        request.setDNSZone("");

        assertThrowsWithContainsMessage(BadRequestFault.class,
                () -> bdmslAdminServiceWS.createSubDomain(request),
                Messages.BAD_REQUEST_PARAMETER_EMPTY_DNSZONE);
    }

    @Test
    void testCreateSubDomainEmptySmpUrlScheme() {
        // given
        SubDomainType request = generateSubdomain();
        request.setSmpUrlScheme("");

        assertThrowsWithContainsMessage(BadRequestFault.class,
                () -> bdmslAdminServiceWS.createSubDomain(request),
                Messages.BAD_REQUEST_PARAMETER_EMPTY_URL_SCHEME);
    }

    @Test
    void testCreateSubDomainEmptyNaptrRecord() {
        // given
        SubDomainType request = generateSubdomain();
        request.setDNSRecordType("");

        assertThrowsWithContainsMessage(BadRequestFault.class,
                () -> bdmslAdminServiceWS.createSubDomain(request),
                Messages.BAD_REQUEST_PARAMETER_EMPTY_DNS_RT);
    }

    @Test
    void testCreateSubDomainParticipantEmptyRegExp() {
        // given
        SubDomainType request = generateSubdomain();
        request.setParticipantRegularExpression("");

        assertThrowsWithContainsMessage(BadRequestFault.class,
                () -> bdmslAdminServiceWS.createSubDomain(request),
                "[ERR-106] Regular expression for field [ParticipantRegularExpression] cannot be 'null' or empty!");
    }

    @Test
    void testUpdateSubDomainEmptySubDomainName() {
        // given
        UpdateSubDomainRequest request = new UpdateSubDomainRequest();
        request.setSubDomainName("");

        assertThrowsWithContainsMessage(BadRequestFault.class,
                () -> bdmslAdminServiceWS.updateSubDomain(request),
                Messages.BAD_REQUEST_PARAMETER_EMPTY_SUBDOMAIN);
    }

    @Test
    void testUpdateSubDomainSubDomainNotFound() {
        // given
        UpdateSubDomainRequest request = new UpdateSubDomainRequest();
        request.setSubDomainName("NotExistAtAll");

        assertThrowsWithContainsMessage(NotFoundFault.class,
                () -> bdmslAdminServiceWS.updateSubDomain(request),
                "[ERR-118] SubDomain does not exist: " + request.getSubDomainName().toLowerCase());
    }

    @Test
    void testUpdateSubDomainInvalidDNSRecordType() {
        // given
        UpdateSubDomainRequest request = new UpdateSubDomainRequest();
        request.setSubDomainName("NotExistAtAll");
        request.setDNSRecordType("KarNeki");

        assertThrowsWithContainsMessage(BadRequestFault.class,
                () -> bdmslAdminServiceWS.updateSubDomain(request),
                Messages.BAD_REQUEST_PARAMETER_INVALID_DNS_RT);
    }

    @Test
    void testUpdateSubDomainInvalidParticipantRegularExpression() {
        // given
        UpdateSubDomainRequest request = new UpdateSubDomainRequest();
        request.setSubDomainName("NotExistAtAll");
        request.setParticipantRegularExpression("**-)(");

        assertThrowsWithContainsMessage(BadRequestFault.class,
                () -> bdmslAdminServiceWS.updateSubDomain(request),
                "[ERR-106] Invalid regular expression for field [ParticipantRegularExpression]!");

    }

    @Test
    void testUpdateSubDomainInvalidCertSubjectRegularExpression() {
        // given
        UpdateSubDomainRequest request = new UpdateSubDomainRequest();
        request.setSubDomainName("NotExistAtAll");
        request.setCertSubjectRegularExpression("**-)(");

        assertThrowsWithContainsMessage(BadRequestFault.class,
                () -> bdmslAdminServiceWS.updateSubDomain(request),
                "[ERR-106] Invalid regular expression for field [CertSubjectRegularExpression]!");
    }

    @Test
    void testUpdateSubDomainInvalidSmpUrlScheme() {
        // given
        UpdateSubDomainRequest request = new UpdateSubDomainRequest();
        request.setSubDomainName("NotExistAtAll");
        request.setSmpUrlScheme("KarNeki");

        assertThrowsWithContainsMessage(BadRequestFault.class,
                () -> bdmslAdminServiceWS.updateSubDomain(request),
                Messages.BAD_REQUEST_PARAMETER_INVALID_URL_SCHEME);
    }

    @Test
    void testDeleteSubDomainEmptySubDomainName() {
        // given
        DeleteSubDomainRequest request = new DeleteSubDomainRequest();
        request.setSubDomainName("");

        assertThrowsWithContainsMessage(BadRequestFault.class,
                () -> bdmslAdminServiceWS.deleteSubDomain(request),
                Messages.BAD_REQUEST_PARAMETER_EMPTY_SUBDOMAIN);
    }

    @Test
    void testGetSubDomainEmptySubDomainName() {
        // given
        GetSubDomainRequest request = new GetSubDomainRequest();
        request.setSubDomainName("");

        assertThrowsWithContainsMessage(BadRequestFault.class,
                () -> bdmslAdminServiceWS.getSubDomain(request),
                Messages.BAD_REQUEST_PARAMETER_EMPTY_SUBDOMAIN);
    }

    @Test
    void testCreateSubDomainInvalidSubDomainNameNotEndAsZone() {
        // given
        SubDomainType request = generateSubdomain();
        request.setSubDomainName("notEndAsZone");

        assertThrowsWithContainsMessage(BadRequestFault.class,
                () -> bdmslAdminServiceWS.createSubDomain(request),
                "[ERR-106] Invalid SubDomain name! SubDomain must end with DNS zone: [." + request.getDNSZone() + "].");
    }

    @Test
    void testCreateSubDomainInvalidSubDomainName() {
        // given
        SubDomainType request = generateSubdomain();
        request.setSubDomainName("-*_-*?_-." + request.getDNSZone());

        assertThrowsWithContainsMessage(BadRequestFault.class,
                () -> bdmslAdminServiceWS.createSubDomain(request),
                Messages.BAD_REQUEST_PARAMETER_INVALID_SUBDOMAIN);
    }

    @Test
    void testCreateSubDomainInvalidSubDomainNameToLongLabel() {
        // given
        SubDomainType request = generateSubdomain();
        // domain label must be less than 64 chars
        request.setSubDomainName("tst-123456789012345678901234567890123456789012345678901234567890." + request.getDNSZone());

        assertThrowsWithContainsMessage(BadRequestFault.class,
                () -> bdmslAdminServiceWS.createSubDomain(request),
                Messages.BAD_REQUEST_PARAMETER_INVALID_SUBDOMAIN);
    }

    @Test
    void testCreateSubDomainInvalidSubDomainNameToLongDomain() {
        // given
        SubDomainType request = generateSubdomain();
        // domain  must be less than 254 chars
        request.setSubDomainName("tst-12345678901234567890123456789012345678901234567890." +
                "test-12345678901234567890123456789012345678901234567890." +
                "test-12345678901234567890123456789012345678901234567890." +
                "test-12345678901234567890123456789012345678901234567890." +
                "test-12345678901234567890123456789012345678901234567890." +
                request.getDNSZone());

        assertThrowsWithContainsMessage(BadRequestFault.class,
                () -> bdmslAdminServiceWS.createSubDomain(request),
                Messages.BAD_REQUEST_PARAMETER_INVALID_SUBDOMAIN);
    }

    @Test
    void testCreateSubDomainInvalidDNSRecordType() {
        // given
        SubDomainType request = generateSubdomain();
        request.setDNSRecordType("NotValidRecord");

        assertThrowsWithContainsMessage(BadRequestFault.class,
                () -> bdmslAdminServiceWS.createSubDomain(request),
                Messages.BAD_REQUEST_PARAMETER_INVALID_DNS_RT);
    }

    @Test
    void testCreateSubDomainInvalidSmpUrlScheme() {
        // given
        SubDomainType request = generateSubdomain();
        request.setSmpUrlScheme("NoValidRecord");

        assertThrowsWithContainsMessage(BadRequestFault.class,
                () -> bdmslAdminServiceWS.createSubDomain(request),
                Messages.BAD_REQUEST_PARAMETER_INVALID_URL_SCHEME);
    }

    @Test
    void testCreateSubDomainInvalidParticipantRegularExpression() {
        // given
        SubDomainType request = generateSubdomain();
        request.setParticipantRegularExpression("**.)(");

        assertThrowsWithContainsMessage(BadRequestFault.class,
                () -> bdmslAdminServiceWS.createSubDomain(request),
                "[ERR-106] Invalid regular expression for field [ParticipantRegularExpression]!");

    }

    @Test
    void testCreateSubDomainInvalidCertSubjectRegularExpression() {
        // given
        SubDomainType request = generateSubdomain();
        request.setCertSubjectRegularExpression("**.)(");

        assertThrowsWithContainsMessage(BadRequestFault.class,
                () -> bdmslAdminServiceWS.createSubDomain(request),
                "[ERR-106] Invalid regular expression for field [CertSubjectRegularExpression]!");
    }

    @Test
    void testCreateSubDomainPositiveScenario() throws Exception {
        // given
        SubDomainType request = generateSubdomain();

        SubDomainType response = bdmslAdminServiceWS.createSubDomain(request);

        assertNotNull(response);
        assertEquals(request.getDNSRecordType().toLowerCase(), response.getDNSRecordType());
        assertEquals(request.getDNSZone().toLowerCase(), response.getDNSZone());
        assertEquals(request.getParticipantRegularExpression(), response.getParticipantRegularExpression());
        assertEquals(request.getSmpUrlScheme().toLowerCase(), response.getSmpUrlScheme());
        assertEquals(request.getSubDomainName().toLowerCase(), response.getSubDomainName());
        assertEquals(request.getCertSubjectRegularExpression(), response.getCertSubjectRegularExpression());
        assertEquals(request.getMaxParticipantCountForDomain(), response.getMaxParticipantCountForDomain());
        assertEquals(request.getMaxParticipantCountForSMP(), response.getMaxParticipantCountForSMP());
        assertNull(response.getCertPolicyOIDs()); // default value is null

    }

    @Test
    void testCreateSubDomainPositiveScenarioNotNullCertPolicyId() throws Exception {
        // given
        SubDomainType request = generateSubdomain();
        request.setCertPolicyOIDs("0.4.0.194112.1.3");

        SubDomainType response = bdmslAdminServiceWS.createSubDomain(request);

        assertNotNull(response);
        assertEquals(request.getDNSRecordType().toLowerCase(), response.getDNSRecordType());
        assertEquals(request.getDNSZone().toLowerCase(), response.getDNSZone());
        assertEquals(request.getParticipantRegularExpression(), response.getParticipantRegularExpression());
        assertEquals(request.getSmpUrlScheme().toLowerCase(), response.getSmpUrlScheme());
        assertEquals(request.getSubDomainName().toLowerCase(), response.getSubDomainName());
        assertEquals(request.getCertSubjectRegularExpression(), response.getCertSubjectRegularExpression());
        assertEquals(request.getMaxParticipantCountForDomain(), response.getMaxParticipantCountForDomain());
        assertEquals(request.getMaxParticipantCountForSMP(), response.getMaxParticipantCountForSMP());
        assertEquals("0.4.0.194112.1.3", response.getCertPolicyOIDs()); // default value is null

    }

    @Test
    void testCreateSubDomainPositiveScenarioWithNullCertRegularExpression() throws Exception {
        // given
        SubDomainType request = generateSubdomain();
        request.setCertSubjectRegularExpression(null);

        SubDomainType response = bdmslAdminServiceWS.createSubDomain(request);

        assertNotNull(response);
        assertNull(response.getCertSubjectRegularExpression());
    }

    @Test
    void testCreateSubDomainPositiveScenarioCaseInsensitive() throws Exception {
        // given
        SubDomainType request = generateSubdomain();
        request.setSmpUrlScheme(request.getSmpUrlScheme().toUpperCase());
        request.setDNSRecordType(request.getDNSRecordType().toUpperCase());
        request.setSubDomainName(request.getSubDomainName().toUpperCase());
        request.setDNSZone(request.getDNSZone().toUpperCase());


        SubDomainType response = bdmslAdminServiceWS.createSubDomain(request);

        assertNotNull(response);
        assertEquals(request.getDNSRecordType().toLowerCase(), response.getDNSRecordType());
        assertEquals(request.getDNSZone().toLowerCase(), response.getDNSZone());
        assertEquals(request.getParticipantRegularExpression(), response.getParticipantRegularExpression());
        assertEquals(request.getSmpUrlScheme().toLowerCase(), response.getSmpUrlScheme());
        assertEquals(request.getSubDomainName().toLowerCase(), response.getSubDomainName());

    }

    @Test
    void testCreateSubDomainPositiveScenarioTrim() throws Exception {
        // given
        SubDomainType request = generateSubdomain();
        request.setSmpUrlScheme("  " + request.getSmpUrlScheme() + "  ");
        request.setDNSRecordType("  " + request.getDNSRecordType() + "  ");
        request.setSubDomainName("  " + request.getSubDomainName() + "  ");
        request.setParticipantRegularExpression("  " + request.getParticipantRegularExpression() + "  ");
        request.setDNSZone("  " + request.getDNSZone() + "  ");
        request.setCertSubjectRegularExpression("  " + request.getCertSubjectRegularExpression() + "  ");


        SubDomainType response = bdmslAdminServiceWS.createSubDomain(request);

        assertNotNull(response);
        assertEquals(request.getDNSRecordType().trim(), response.getDNSRecordType());
        assertEquals(request.getDNSZone().trim(), response.getDNSZone());
        assertEquals(request.getParticipantRegularExpression().trim(), response.getParticipantRegularExpression());
        assertEquals(request.getSmpUrlScheme().trim(), response.getSmpUrlScheme());
        assertEquals(request.getSubDomainName().trim(), response.getSubDomainName());
        assertEquals(request.getCertSubjectRegularExpression().trim(), response.getCertSubjectRegularExpression());

    }

    @Test
    void testGetSubDomainPositiveScenario() throws Exception {
        // given
        SubDomainType request = generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(request);

        GetSubDomainRequest getRequest = new GetSubDomainRequest();
        getRequest.setSubDomainName(request.getSubDomainName());
        SubDomainType response = bdmslAdminServiceWS.getSubDomain(getRequest);

        assertNotNull(response);
        assertEquals(request.getDNSRecordType().toLowerCase(), response.getDNSRecordType());
        assertEquals(request.getDNSZone().toLowerCase(), response.getDNSZone());
        assertEquals(request.getParticipantRegularExpression(), response.getParticipantRegularExpression());
        assertEquals(request.getSmpUrlScheme().toLowerCase(), response.getSmpUrlScheme());
        assertEquals(request.getSubDomainName().toLowerCase(), response.getSubDomainName());
        assertEquals(request.getCertSubjectRegularExpression(), response.getCertSubjectRegularExpression());

    }

    @Test
    void testGetSubDomainPositiveScenarioTrim() throws Exception {
        // given
        SubDomainType request = generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(request);

        GetSubDomainRequest getRequest = new GetSubDomainRequest();
        getRequest.setSubDomainName("  " + request.getSubDomainName() + "  ");
        SubDomainType response = bdmslAdminServiceWS.getSubDomain(getRequest);

        assertNotNull(response);
        assertEquals(request.getDNSRecordType().toLowerCase(), response.getDNSRecordType());
        assertEquals(request.getDNSZone().toLowerCase(), response.getDNSZone());
        assertEquals(request.getParticipantRegularExpression(), response.getParticipantRegularExpression());
        assertEquals(request.getSmpUrlScheme().toLowerCase(), response.getSmpUrlScheme());
        assertEquals(request.getSubDomainName().toLowerCase(), response.getSubDomainName());
        assertEquals(request.getCertSubjectRegularExpression(), response.getCertSubjectRegularExpression());

    }

    @Test
    void testGetSubDomainNotExists() {
        // given
        SubDomainType request = generateSubdomain();

        GetSubDomainRequest getRequest = new GetSubDomainRequest();
        getRequest.setSubDomainName("notexistsname" + request.getSubDomainName());

        assertThrowsWithContainsMessage(NotFoundFault.class,
                () -> bdmslAdminServiceWS.getSubDomain(getRequest),
                "[ERR-118] SubDomain does not exist: " + getRequest.getSubDomainName());
    }

    @Test
    void testDeleteSubDomainPositiveScenario() throws Exception {
        // given
        SubDomainType request = generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(request);
        GetSubDomainRequest getRequest = new GetSubDomainRequest();
        getRequest.setSubDomainName(request.getSubDomainName());
        SubDomainType test = bdmslAdminServiceWS.getSubDomain(getRequest);
        assertNotNull(test);

        DeleteSubDomainRequest deleteRequest = new DeleteSubDomainRequest();
        deleteRequest.setSubDomainName(request.getSubDomainName());
        SubDomainType response = bdmslAdminServiceWS.deleteSubDomain(deleteRequest);

        assertNotNull(response);
        assertEquals(request.getDNSRecordType().toLowerCase(), response.getDNSRecordType());
        assertEquals(request.getDNSZone().toLowerCase(), response.getDNSZone());
        assertEquals(request.getParticipantRegularExpression(), response.getParticipantRegularExpression());
        assertEquals(request.getSmpUrlScheme().toLowerCase(), response.getSmpUrlScheme());
        assertEquals(request.getSubDomainName().toLowerCase(), response.getSubDomainName());

        // test if exists in database
        assertThrowsWithContainsMessage(NotFoundFault.class,
                () -> bdmslAdminServiceWS.getSubDomain(getRequest),
                "[ERR-118] SubDomain does not exist: " + getRequest.getSubDomainName());
    }

    @Test
    void testUpdateSubDomainPositiveScenario() throws Exception {
        // given
        SubDomainType request = generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(request);
        GetSubDomainRequest getRequest = new GetSubDomainRequest();
        getRequest.setSubDomainName(request.getSubDomainName());
        SubDomainType test = bdmslAdminServiceWS.getSubDomain(getRequest);
        assertNotNull(test);
        assertFalse(StringUtils.isBlank(test.getCertSubjectRegularExpression()));
        // when
        UpdateSubDomainRequest updateSubDomainRequest = new UpdateSubDomainRequest();
        updateSubDomainRequest.setSubDomainName(request.getSubDomainName());
        updateSubDomainRequest.setCertSubjectRegularExpression("test-.*");
        updateSubDomainRequest.setDNSRecordType("CNAME");
        updateSubDomainRequest.setParticipantRegularExpression("newParticipant.*");
        updateSubDomainRequest.setSmpUrlScheme("HTTPS");

        bdmslAdminServiceWS.updateSubDomain(updateSubDomainRequest);
        // then
        test = bdmslAdminServiceWS.getSubDomain(getRequest);
        assertNotNull(test);
        assertEquals(updateSubDomainRequest.getDNSRecordType().toLowerCase(), test.getDNSRecordType());
        assertEquals(updateSubDomainRequest.getParticipantRegularExpression(), test.getParticipantRegularExpression());
        assertEquals(updateSubDomainRequest.getSmpUrlScheme().toLowerCase(), test.getSmpUrlScheme());
        assertEquals(updateSubDomainRequest.getSubDomainName().toLowerCase(), test.getSubDomainName());
    }

    @Test
    void testUpdateSubDomainSetCertRegularExpressionToEmpty() throws Exception {
        // given
        SubDomainType request = generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(request);
        GetSubDomainRequest getRequest = new GetSubDomainRequest();
        getRequest.setSubDomainName(request.getSubDomainName());
        SubDomainType test = bdmslAdminServiceWS.getSubDomain(getRequest);
        assertNotNull(test);
        assertFalse(StringUtils.isBlank(test.getCertSubjectRegularExpression()));
        // when
        UpdateSubDomainRequest updateSubDomainRequest = new UpdateSubDomainRequest();
        updateSubDomainRequest.setSubDomainName(request.getSubDomainName());
        updateSubDomainRequest.setCertSubjectRegularExpression("");

       assertThrowsWithContainsMessage(BadRequestFault.class,
                () -> bdmslAdminServiceWS.updateSubDomain(updateSubDomainRequest),
                "[ERR-106] Regular expression for field [CertSubjectRegularExpression] cannot be 'null' or empty!");
       }

    @Test
    void testDeleteSubDomainNotExist() throws Exception {
        // given
        SubDomainType request = generateSubdomain();

        DeleteSubDomainRequest deleteRequest = new DeleteSubDomainRequest();
        deleteRequest.setSubDomainName("notexist" + request.getSubDomainName());

        // test if exists in database
        assertThrowsWithContainsMessage(NotFoundFault.class,
                () -> bdmslAdminServiceWS.deleteSubDomain(deleteRequest),
                "[ERR-118] SubDomain does not exist: " + deleteRequest.getSubDomainName());
    }

    @Test
    void testCreateSubDomainAlreadyExist() throws Exception {
        // given
        SubDomainType request = generateSubdomain();
        bdmslAdminServiceWS.createSubDomain(request);

        assertThrowsWithContainsMessage(BadRequestFault.class,
                () -> bdmslAdminServiceWS.createSubDomain(request),
                "[ERR-106] SubDomain already exists!");
    }

    static SubDomainType generateSubdomain() {
        SubDomainType subdomain = new SubDomainType();
        subdomain.setSubDomainName("name.zone.test.eu");
        subdomain.setDNSZone("zone.test.eu");
        subdomain.setDNSRecordType("all");
        subdomain.setParticipantRegularExpression(".*");
        subdomain.setSmpUrlScheme("all");
        subdomain.setCertSubjectRegularExpression(".*");
        subdomain.setMaxParticipantCountForDomain(BigInteger.valueOf(1234567));
        subdomain.setMaxParticipantCountForSMP(BigInteger.valueOf(123456));
        return subdomain;
    }
}
