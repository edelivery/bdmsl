/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.ws.soap;

import ec.services.wsdl.bdmsl.admin.data._1.GenerateReport;
import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.enums.SMLReportEnum;
import eu.europa.ec.bdmsl.common.enums.SMLRoleEnum;
import eu.europa.ec.bdmsl.test.CommonTestUtils;
import eu.europa.ec.bdmsl.security.CertificateAuthentication;
import eu.europa.ec.edelivery.security.PreAuthenticatedCertificatePrincipal;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.security.Principal;
import java.security.cert.X509Certificate;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertThrows;

class BDMSLAdminServiceWSReportTest extends AbstractJUnit5Test {

    @Autowired
    private IBDMSLAdminServiceWS bdmslServiceWS;

    @BeforeEach
    void before() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        String certHeaderValue = "serial=48:b6:81:ee:8e:0d:cc:08&subject=EMAILADDRESS=receiver@test.be,C=BE, O=DIGIT, OU=FOR TEST ONLY,CN=DIGIT_SMP_TECH_TEAM_3&validfrom=Feb  1 14:20:18 2017 GMT&validto=Jul  9 23:59:00 2019 GMT&issuer=C=BE, O=DIGIT, OU=FOR TEST ONLY, CN=DIGIT_SMP_TECH_TEAM_2";
        Principal principal = blueCoatAuthenticationFilter.buildPrincipalFromHeader(certHeaderValue);
        Authentication authentication = new CertificateAuthentication((PreAuthenticatedCertificatePrincipal) principal, Collections.singletonList(SMLRoleEnum.ROLE_ADMIN.getAuthority()));
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Test
    void generateReportNullRequest() {
        assertThrows(BadRequestFault.class,
                () -> bdmslServiceWS.generateReport(null),
                "[ERR-106] The input values must not be null!");
    }

    @Test
    void generateReportEmptyRequest() {
        assertThrows(BadRequestFault.class,
                () -> bdmslServiceWS.generateReport(new GenerateReport()),
                "[ERR-106] Report name cannot be 'null' or empty!");
    }

    @Test
    void generateReportInvalidEmailRequest() {
        GenerateReport request = new GenerateReport();
        request.setReceiverEmailAddress("invalid@@ma@il.cm");
        request.setReportCode(SMLReportEnum.DATABASE_DNS_INCONSISTENCY.name());

        assertThrows(BadRequestFault.class,
                () -> bdmslServiceWS.generateReport(request),
                "[ERR-106] Email address [invalid@@ma@il.cm] is not valid!");
    }

    @Test
    void generateReportInvalidCodeRequest() {
        GenerateReport request = new GenerateReport();
        request.setReceiverEmailAddress("invalid@@ma@il.cm");
        request.setReportCode("Code does not exist");

        assertThrows(BadRequestFault.class,
                () -> bdmslServiceWS.generateReport(request),
                "[ERR-106] Invalid report name!");
    }

    @Test
    void generateReportForNonRootCADomainByX509Certificate() throws Exception {
        //GIVEN
        //Generating a dummy certificate for authentication
        X509Certificate certificate = CommonTestUtils.createCertificate("CN=DIGIT_SMP_TECH_TEAM_2,OU=FOR TEST ONLY,O=DIGIT,ST=BE,C=BE,E=CEF-EDELIVERY-SUPPORT@ec.europa.eu",
                "CN=DIGIT_SMP_TECH_TEAM_2,OU=FOR TEST ONLY,O=DIGIT,ST=BE,C=BE");
        X509Certificate[] certificates = {certificate};


        //Injecting the dummy certificate as an authenticated user
        Authentication authentication = createX509Authentication(certificates);
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        //WHEN-THEN
        //Calling Operation Change Certificate
        GenerateReport request = new GenerateReport();
        request.setReceiverEmailAddress("test@mail.cm");
        request.setReportCode(SMLReportEnum.DATABASE_DNS_INCONSISTENCY.name());


        assertThrows(UnauthorizedFault.class,
                () -> bdmslServiceWS.generateReport(request),
                "Access is denied");
    }

    @Test
    void generateReportForNonRootCADomainByHeaderCertificate() {
        //GIVEN
        //New certificate
        //Injecting the dummy certificate as an authenticated user
        String certHeaderValue = "serial=48:b6:81:ee:8e:0d:cc:08&subject=EMAILADDRESS=receiver@test.be,C=BE, O=DIGIT, OU=FOR TEST ONLY,CN=DIGIT_SMP_TECH_TEAM_3&validfrom=Feb  1 14:20:18 2017 GMT&validto=Jul  9 23:59:00 2019 GMT&issuer=C=BE, O=DIGIT, OU=FOR TEST ONLY, CN=DIGIT_SMP_TECH_TEAM_2";
        Principal principal = blueCoatAuthenticationFilter.buildPrincipalFromHeader(certHeaderValue);
        Authentication bcAuth = new CertificateAuthentication((PreAuthenticatedCertificatePrincipal) principal, Collections.singletonList(SMLRoleEnum.ROLE_SMP.getAuthority()));
        bcAuth.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(bcAuth);

        //WHEN-THEN
        GenerateReport request = new GenerateReport();
        assertThrows(UnauthorizedFault.class,
                () -> bdmslServiceWS.generateReport(request),
                "Access is denied");
    }
}
