/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.ws.soap;

import ec.services.wsdl.bdmsl.data._1.ExistsParticipant;
import ec.services.wsdl.bdmsl.data._1.ExistsParticipantResponse;
import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.security.UnsecureAuthentication;
import org.busdox.transport.identifiers._1.ParticipantIdentifierType;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.security.Security;

import static org.hamcrest.CoreMatchers.startsWithIgnoringCase;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Flavio SANTOS
 */
class BDMSLServiceWSTest extends AbstractJUnit5Test {

    @Autowired
    private IBDMSLServiceWS bdmslServiceWS;

    @BeforeAll
    static void beforeClass() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        UnsecureAuthentication authentication = new UnsecureAuthentication();
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    @Test
    void existsParticipantIdentifierNullRequest() {
        // when then
        BadRequestFault error = assertThrows(BadRequestFault.class,
                () -> bdmslServiceWS.existsParticipantIdentifier(null));

        assertThat(error.getMessage(),
                startsWithIgnoringCase("[ERR-106] The input values must not be null"));

    }

    @Test
    void existsParticipantIdentifierBlankData() {
        // when then
        ExistsParticipant request = new ExistsParticipant();
        BadRequestFault error = assertThrows(BadRequestFault.class,
                () -> bdmslServiceWS.existsParticipantIdentifier(request));

        assertThat(error.getMessage(),
                startsWithIgnoringCase("[ERR-106] The participant identifier must not be null or empty"));
    }

    @Test
    void existsParticipantIdentifierInvalidScheme() {
        // when then
        ExistsParticipant request = new ExistsParticipant();
        request.setParticipantIdentifier(new ParticipantIdentifierType());
        request.getParticipantIdentifier().setValue("0088:123456");
        request.getParticipantIdentifier().setScheme("busdoxactoridupis");
        request.setServiceMetadataPublisherID("foundUnsecure");
        BadRequestFault error = assertThrows(BadRequestFault.class,
                () -> bdmslServiceWS.existsParticipantIdentifier(request));

        assertThat(error.getMessage(),
                startsWithIgnoringCase("[ERR-106] The Scheme Identifier MUST take the form {domain}-{identifierArea}-{identifierType} such as for example 'busdox-actorid-upis'. It may only contain the following characters: [a-z0-9]+-[a-z0-9]+-[a-z0-9]+"));
    }

    @Test
    void existsParticipantIdentifierSMPDoesNotExists() {
        // when then
        ExistsParticipant request = new ExistsParticipant();
        request.setParticipantIdentifier(new ParticipantIdentifierType());
        request.getParticipantIdentifier().setValue("0088:123456");
        request.getParticipantIdentifier().setScheme("busdox-actorid-upis");
        request.setServiceMetadataPublisherID("smp-id");
        NotFoundFault error = assertThrows(NotFoundFault.class,
                () -> bdmslServiceWS.existsParticipantIdentifier(request));

        assertThat(error.getFaultInfo().getFaultMessage(),
                startsWithIgnoringCase("[ERR-100] The Publisher identifier [smp-id] doesn't exist"));
    }

    @Test
    void existsParticipantIdentifierNotExists() throws UnauthorizedFault, InternalErrorFault, BadRequestFault, NotFoundFault {
        // when then
        ExistsParticipant request = new ExistsParticipant();
        request.setParticipantIdentifier(new ParticipantIdentifierType());
        request.getParticipantIdentifier().setValue("0088:123456");
        request.getParticipantIdentifier().setScheme("busdox-actorid-upis");
        request.setServiceMetadataPublisherID("smp-sample");
        ExistsParticipantResponse response = bdmslServiceWS.existsParticipantIdentifier(request);

        assertFalse(response.isExist());
    }

    @Test
    void existsParticipantIdentifierExists() throws UnauthorizedFault, InternalErrorFault, BadRequestFault, NotFoundFault {
        // when then
        ExistsParticipant request = new ExistsParticipant();
        request.setParticipantIdentifier(new ParticipantIdentifierType());
        request.getParticipantIdentifier().setValue("0009:123456789");
        request.getParticipantIdentifier().setScheme("iso6523-actorid-upis");
        request.setServiceMetadataPublisherID("foundUnsecure");
        ExistsParticipantResponse response = bdmslServiceWS.existsParticipantIdentifier(request);

        assertTrue(response.isExist());
    }
}
