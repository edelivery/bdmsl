/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.bo.CertificateDomainBO;
import eu.europa.ec.bdmsl.common.bo.ServiceMetadataPublisherBO;
import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import eu.europa.ec.bdmsl.common.enums.SMLRoleEnum;
import eu.europa.ec.bdmsl.common.exception.BadRequestException;

import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.common.exception.UnauthorizedException;
import eu.europa.ec.bdmsl.test.CommonSMPTestUtils;
import eu.europa.ec.bdmsl.test.CommonTestUtils;
import eu.europa.ec.bdmsl.security.CertificateAuthentication;
import eu.europa.ec.edelivery.security.ClientCertAuthenticationFilter;
import eu.europa.ec.edelivery.security.PreAuthenticatedCertificatePrincipal;
import org.apache.commons.lang3.StringUtils;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import jakarta.transaction.Transactional;
import java.security.Principal;
import java.util.Collections;
import java.util.UUID;

import static eu.europa.ec.bdmsl.test.TestConstants.*;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Adrien FERIAL
 * @since 16/06/2015
 */
class ManageServiceMetadataServiceTest extends AbstractJUnit5Test {
    static ClientCertAuthenticationFilter blueCoatAuthenticationFilter = new ClientCertAuthenticationFilter();

    private static final String UNAUTHORIZED_EXCEPTION_MESSAGE = "[ERR-101] The SMP [SMPTESTFOREDELIVERY22592] was not created with the certificate [CN=SMP_TEST_FOR_EDELIVERY_22591,O=DG-DIGIT,C=BE:00000000000000000000000000022591]";
    @Autowired
    IManageServiceMetadataService testInstance;

    @BeforeEach
    protected void testSetup() throws Exception {
        // add security context
        SubdomainBO subdomainBO = subdomainDAO.getSubDomain("22591.acc.edelivery.tech.ec.europa.eu");
        CertificateDomainBO certificateDomainBO = new CertificateDomainBO();
        certificateDomainBO.setSubdomain(subdomainBO);
        String certHeaderValue = CommonTestUtils.createHeaderCertificateForBlueCoat(
                CERTIFICATE_05_SERIAL, CERTIFICATE_05_ISSUER, CERTIFICATE_05_SUBJECT);
        Principal principal = blueCoatAuthenticationFilter.buildPrincipalFromHeader(certHeaderValue);
        Authentication bcAuth = new CertificateAuthentication(certificateDomainBO, (PreAuthenticatedCertificatePrincipal) principal, Collections.singletonList(SMLRoleEnum.ROLE_SMP.getAuthority()));
        bcAuth.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(bcAuth);
    }

    @Test
    void testReadOK() throws TechnicalException {
        // given
        String id = "SMPTESTFOREDELIVERY22591";
        // when
        ServiceMetadataPublisherBO result = testInstance.read(id);
        // then
        assertNotNull(result);
    }

    @Test
    void testReadNotUnauthorizedException() {
        // given - not created by CERTIFICATE_02_SUBJECT
        String id = "SMPTESTFOREDELIVERY22593";
        // when
        UnauthorizedException exception =
                assertThrows(UnauthorizedException.class, () ->
                        testInstance.read(id));
        // then
        assertEquals(
                "[ERR-101] The SMP [SMPTESTFOREDELIVERY22593] was not created with the certificate [CN=SMP_TEST_FOR_EDELIVERY_22591,O=DG-DIGIT,C=BE:00000000000000000000000000022591]",
                exception.getMessage());
    }

    @Test
    void testReadNull() {

        // when-then
        BadRequestException exception =
                assertThrows(BadRequestException.class, () ->
                        testInstance.read(null));
        // then
        assertEquals(
                "[ERR-106] The SMP ID must not be null",
                exception.getMessage());
    }

    @Test
    void testReadEmpty() {
        // when-then
        BadRequestException exception =
                assertThrows(BadRequestException.class, () ->
                        testInstance.read(""));
        // then
        assertEquals(
                "[ERR-106] The SMP ID must not be null",
                exception.getMessage());
    }

    @Test
    @Transactional
    void testCreateOK() throws TechnicalException {
        // given
        String id = UUID.randomUUID().toString();
        ServiceMetadataPublisherBO smp = CommonSMPTestUtils.createSMP(id, "https://test.eu.local/smp");
        smp.setCertificateId(CERTIFICATE_05_SUBJECT + ":" + StringUtils.leftPad(CERTIFICATE_05_SERIAL, 32, "0"));
        // when
        testInstance.create(smp);
        // then
        ServiceMetadataPublisherBO result = testInstance.read(id);
        assertNotNull(result);
    }

    @Test
    @Transactional
    void testUpdateOK() throws TechnicalException {
        // given
        String id = "SMPTESTFOREDELIVERY22591"; // existing smp in database
        String newLogicalAddress = "https://test.eu.local/smp/" + UUID.randomUUID();
        ServiceMetadataPublisherBO smp = CommonSMPTestUtils.createSMP(id, newLogicalAddress);
        smp.setCertificateId(CERTIFICATE_05_SUBJECT + ":" + StringUtils.leftPad(CERTIFICATE_05_SERIAL, 32, "0"));
        // when
        testInstance.update(smp);
        // then
        ServiceMetadataPublisherBO result = testInstance.read(id);
        assertNotNull(result);
        assertEquals(newLogicalAddress, result.getLogicalAddress());
    }

    @Test
    void testUpdateFailNotCreated() {
        // given
        String id = "SMPTESTFOREDELIVERY22592"; // existing smp in database
        String newLogicalAddress = "https://test.eu.local/smp/" + UUID.randomUUID();
        ServiceMetadataPublisherBO smp = CommonSMPTestUtils.createSMP(id, newLogicalAddress);
        smp.setCertificateId(CERTIFICATE_05_SUBJECT + ":" + StringUtils.leftPad(CERTIFICATE_05_SERIAL, 32, "0"));
        // when
        UnauthorizedException exception = assertThrows(UnauthorizedException.class, () ->
                testInstance.update(smp));

        // then
        assertEquals(UNAUTHORIZED_EXCEPTION_MESSAGE, exception.getMessage());
    }

    @Test
    void testReadFailNotCreated() {
        // given
        String id = "SMPTESTFOREDELIVERY22592"; // existing smp in database
        String newLogicalAddress = "https://test.eu.local/smp/" + UUID.randomUUID();
        ServiceMetadataPublisherBO smp = CommonSMPTestUtils.createSMP(id, newLogicalAddress);
        smp.setCertificateId(CERTIFICATE_05_SUBJECT + ":" + StringUtils.leftPad(CERTIFICATE_05_SERIAL, 32, "0"));
        // when
        UnauthorizedException exception = assertThrows(UnauthorizedException.class, () ->
                testInstance.read(id));

        // then
        assertEquals(UNAUTHORIZED_EXCEPTION_MESSAGE, exception.getMessage());
    }

    @Test
    void testDeletedFailNotCreated() {
        // given
        String id = "SMPTESTFOREDELIVERY22592"; // existing smp in database
        String newLogicalAddress = "https://test.eu.local/smp/" + UUID.randomUUID();
        ServiceMetadataPublisherBO smp = CommonSMPTestUtils.createSMP(id, newLogicalAddress);
        smp.setCertificateId(CERTIFICATE_05_SUBJECT + ":" + StringUtils.leftPad(CERTIFICATE_05_SERIAL, 32, "0"));
        // when
        UnauthorizedException exception = assertThrows(UnauthorizedException.class, () ->
                testInstance.delete(id));

        // then
        assertEquals(UNAUTHORIZED_EXCEPTION_MESSAGE, exception.getMessage());
    }

}
