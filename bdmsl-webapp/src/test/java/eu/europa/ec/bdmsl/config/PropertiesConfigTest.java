/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.config;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;


class PropertiesConfigTest extends AbstractJUnit5Test {

    @Test
    void propertySourcesPlaceholderConfigurer() {
        Environment environment = Mockito.mock(Environment.class);
        Mockito.doReturn("org.hibernate.dialect.H2Dialect")
                .when(environment).getProperty(Mockito.eq(FileProperty.PROPERTY_DB_DIALECT), Mockito.anyString());

        PropertySourcesPlaceholderConfigurer propConf = PropertiesConfig.propertySourcesPlaceholderConfigurer(Mockito.mock(Environment.class));

        assertNotNull(propConf);
    }

    @Test
    void getFilePropertiesDefault() {

        Properties prop = PropertiesConfig.getFileProperties(null);

        assertFalse(prop.isEmpty());
    }

    @Test
    void getFilePropertiesNotExists() {

        Properties prop = PropertiesConfig.getFileProperties("/FileNotInClassPath");

        assertTrue(prop.isEmpty());
    }

    @Test
    void getFilePropertiesCustomFile() {

        Properties prop = PropertiesConfig.getFileProperties("/sml.config.properties");

        assertFalse(prop.isEmpty());
    }

}
