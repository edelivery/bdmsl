/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.business.impl;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.business.IManageServiceMetadataBusiness;
import eu.europa.ec.bdmsl.common.bo.CertificateDomainBO;
import eu.europa.ec.bdmsl.common.bo.ServiceMetadataPublisherBO;
import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import eu.europa.ec.bdmsl.common.enums.SMLRoleEnum;
import eu.europa.ec.bdmsl.common.exception.BadConfigurationException;
import eu.europa.ec.bdmsl.common.exception.BadRequestException;
import eu.europa.ec.bdmsl.dao.ICertificateDomainDAO;
import eu.europa.ec.bdmsl.test.CommonTestUtils;
import eu.europa.ec.bdmsl.security.CertificateAuthentication;
import eu.europa.ec.bdmsl.security.SMLAuthenticationProvider;
import eu.europa.ec.edelivery.security.ClientCertAuthenticationFilter;
import eu.europa.ec.edelivery.security.PreAuthenticatedCertificatePrincipal;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.bean.override.mockito.MockitoSpyBean;
import org.springframework.test.util.ReflectionTestUtils;

import java.security.Principal;
import java.util.Collections;

import static eu.europa.ec.bdmsl.test.CommonSMPTestUtils.createSMPWithLogicalAddress;
import static eu.europa.ec.bdmsl.test.TestConstants.*;
import static org.junit.jupiter.api.Assertions.*;


/**
 * @author Flavio SANTOS
 */
class ManageServiceMetadataBusinessImplTest extends AbstractJUnit5Test {

    @Autowired
    private IManageServiceMetadataBusiness testInstance;

    @Autowired
    private SMLAuthenticationProvider smlAuthenticationProvider;

    @Autowired
    @MockitoSpyBean
    private ICertificateDomainDAO certificateDomainDAO;

    ClientCertAuthenticationFilter blueCoatAuthenticationFilter = new ClientCertAuthenticationFilter();

    @BeforeEach
    void before() throws Exception {

        // create Security authentication context
        String certHeaderValue = CommonTestUtils.createHeaderCertificateForBlueCoat(
                CERTIFICATE_02_SERIAL, CERTIFICATE_02_ISSUER, CERTIFICATE_02_SUBJECT);
        Principal principal = blueCoatAuthenticationFilter.buildPrincipalFromHeader(certHeaderValue);
        Authentication bcAuth = new CertificateAuthentication((PreAuthenticatedCertificatePrincipal) principal, Collections.singletonList(SMLRoleEnum.ROLE_SMP.getAuthority()));
        bcAuth.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(bcAuth);
        ManageServiceMetadataBusinessImpl manageServiceMetadataBusinessImpl = CommonTestUtils.getTargetObject(testInstance);
        ReflectionTestUtils.setField(manageServiceMetadataBusinessImpl, "certificateDomainDAO", certificateDomainDAO);
    }

    @Test
    void testValidateSMPDataLogicalAddressAllProtocolAllowed() throws Exception {
        String[] logicalAddresses = new String[]{"http://demo.test.pt",
                "https://demo.test.pt", "https://demo.test.pt/smp"};
        for (String logicalAddress : logicalAddresses) {
            ServiceMetadataPublisherBO smpBO = createSMPWithLogicalAddress(logicalAddress);
            testInstance.validateSMPData(smpBO);
        }
    }

    @Test
    void testValidateSMPDataLogicalAddressWrongProtocol() {
        String[] logicalAddresses = new String[]{"ftp://demo.test.pt",
                "ldap://demo.test.pt", "wrong://demo.test.pt/smp"};
        for (String testUrl : logicalAddresses) {
            ServiceMetadataPublisherBO smpBO = createSMPWithLogicalAddress(testUrl);
            BadRequestException exception = assertThrows(BadRequestException.class, () -> testInstance.validateSMPData(smpBO));
            assertEquals("[ERR-106] Logical address is invalid [" + testUrl + "].",
                    exception.getMessage());
        }
    }

    @Test
    void testValidateSMPDataLogicalAddressHttpAllowedOnly() throws Exception {
        String[] protocols = new String[]{"http", "HTTP"};
        for (String protocol : protocols) {
            validateSMPDataLogicalAddressHttpAllowedOnly(protocol, "https://demo.test.pt", "[ERR-106] Wrong Protocol in LogicalAddress field. For this domain only HTTP protocol is allowed.");
        }
    }

    @Test
    void testValidateSMPDataLogicalAddressHttpsAllowedOnly() throws Exception {
        String[] protocols = new String[]{"https", "https "};
        for (String protocol : protocols) {
            validateSMPDataLogicalAddressHttpAllowedOnly(protocol, "http://demo.test.pt", "[ERR-106] Wrong Protocol in LogicalAddress field. For this domain only HTTPS protocol is allowed.");
        }
    }

    @Test
    void testValidateSMPDataLogicalAddressHttpsWithDifferentCases() throws Exception {
        String[] protocols = new String[]{"https", "HTTPS", "HttPs", "HttPs "};
        for (String protocol : protocols) {
            validateSMPDataLogicalAddressHttpWithDifferentCases(protocol, "https://demo.test.pt");
        }
    }

    @Test
    void testValidateSMPDataLogicalAddressHttpWithDifferentCases() throws Exception {
        String[] protocols = new String[]{"http", "HTTP", "HttP", "HttP "};
        for (String protocol : protocols) {
            validateSMPDataLogicalAddressHttpWithDifferentCases(protocol, "http://demo.test.pt");
        }
    }

    @Test
    void testValidateSMPDataLogicalAddressInvalidProtocol() {
        // given
        String certHeaderValue = CommonTestUtils.createHeaderCertificateForBlueCoat(
                CERTIFICATE_03_SERIAL, CERTIFICATE_03_ISSUER, CERTIFICATE_03_SUBJECT);
        PreAuthenticatedCertificatePrincipal principal = blueCoatAuthenticationFilter.buildPrincipalFromHeader(certHeaderValue);
        Authentication authentication = new CertificateAuthentication(principal, Collections.singletonList(SMLRoleEnum.ROLE_SMP.getAuthority()));
        Authentication bcAuth = smlAuthenticationProvider.authenticate(authentication);
        SecurityContextHolder.getContext().setAuthentication(bcAuth);
        ServiceMetadataPublisherBO smpBO = createSMPWithLogicalAddress("http://demo.test.pt");
        // when
        BadRequestException exception = assertThrows(BadRequestException.class,
                () -> testInstance.validateSMPData(smpBO));
        // then
        assertEquals("[ERR-106] Wrong Protocol in LogicalAddress field. For this domain only HTTPS protocol is allowed.",
                exception.getMessage());
    }

    @Test
    void testValidateSMPDataLogicalAddressBadConfiguration() {
        // given
        String certHeaderValue = CommonTestUtils.createHeaderCertificateForBlueCoat(
                CERTIFICATE_04_SERIAL, CERTIFICATE_04_ISSUER, CERTIFICATE_04_SUBJECT);
        PreAuthenticatedCertificatePrincipal principal = blueCoatAuthenticationFilter.buildPrincipalFromHeader(certHeaderValue);
        Authentication authentication = new CertificateAuthentication(principal, Collections.singletonList(SMLRoleEnum.ROLE_SMP.getAuthority()));
        Authentication bcAuth = smlAuthenticationProvider.authenticate(authentication);
        SecurityContextHolder.getContext().setAuthentication(bcAuth);
        ServiceMetadataPublisherBO smpBO = createSMPWithLogicalAddress("http://demo.test.pt");
        // when
        BadConfigurationException exception = assertThrows(BadConfigurationException.class,
                () -> testInstance.validateSMPData(smpBO));
        // then
        assertEquals("[ERR-109] Smp logical address protocol restriction is not defined for domain 22595.acc.edelivery.tech.ec.europa.eu",
                exception.getMessage());

    }

    @Test
    void testIsLogicalAddressToBeUpdatedTrue() throws Exception {
        ServiceMetadataPublisherBO serviceMetadataPublisherBO = new ServiceMetadataPublisherBO();
        serviceMetadataPublisherBO.setSmpId("logicalAddressSMP");
        serviceMetadataPublisherBO.setLogicalAddress("http://new.url.eu");
        boolean result = testInstance.isLogicalAddressToBeUpdated(serviceMetadataPublisherBO);

        assertTrue(result);
    }

    @Test
    void testIsLogicalAddressToBeUpdatedFalse() throws Exception {
        ServiceMetadataPublisherBO serviceMetadataPublisherBO = new ServiceMetadataPublisherBO();
        serviceMetadataPublisherBO.setSmpId("logicalAddressSMP");
        serviceMetadataPublisherBO.setLogicalAddress("http://logicalAddress");
        boolean result = testInstance.isLogicalAddressToBeUpdated(serviceMetadataPublisherBO);

        assertFalse(result);
    }

    private void validateSMPDataLogicalAddressHttpAllowedOnly(String protocol, String logicalAddress, String message) throws Exception {
        ServiceMetadataPublisherBO smpBO = createSMPWithLogicalAddress(logicalAddress);
        CertificateDomainBO domainCertBO = Mockito.mock(CertificateDomainBO.class);
        SubdomainBO subdomainBO = Mockito.mock(SubdomainBO.class);

        Mockito.doReturn(domainCertBO).when(certificateDomainDAO).findDomain(Mockito.any());
        Mockito.doReturn(subdomainBO).when(domainCertBO).getSubdomain();
        Mockito.doReturn(protocol).when(subdomainBO).getSmpUrlSchemas();

        BadRequestException exception = assertThrows(BadRequestException.class, () -> testInstance.validateSMPData(smpBO));
        assertEquals(message,
                exception.getMessage());
    }

    void validateSMPDataLogicalAddressHttpWithDifferentCases(String protocol, String logicalAddress) throws Exception {
        ServiceMetadataPublisherBO smpBO = createSMPWithLogicalAddress(logicalAddress);
        CertificateDomainBO domainCertBO = Mockito.mock(CertificateDomainBO.class);
        SubdomainBO subdomainBO = Mockito.mock(SubdomainBO.class);

        Mockito.doReturn(domainCertBO).when(certificateDomainDAO).findDomain(Mockito.any());
        Mockito.doReturn(subdomainBO).when(domainCertBO).getSubdomain();
        Mockito.doReturn(protocol).when(subdomainBO).getSmpUrlSchemas();

        testInstance.validateSMPData(smpBO);
    }
}
