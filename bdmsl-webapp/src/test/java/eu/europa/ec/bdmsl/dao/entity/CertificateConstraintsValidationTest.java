/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.entity;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;

import static eu.europa.ec.bdmsl.util.Constant.NEW_CERT_PUBLIC_KEY;
import static org.junit.jupiter.api.Assertions.*;

/**
 * This test class validates the data model {@link eu.europa.ec.bdmsl.dao.entity.CertificateEntity)} and its constraints
 *
 * @author Flavio SANTOS
 * @since 22/09/2016
 */
class CertificateConstraintsValidationTest extends AbstractJUnit5Test {

    @Test
    void persistIntoCertificateTableOk() {
        String sql = "INSERT INTO bdmsl_certificate(id,certificate_id,valid_from,valid_until,pem_encoding,new_cert_change_date,new_cert_id,created_on,last_updated_on) values (?,?,?,?,?,?,?,?,?)";
        Timestamp date = new Timestamp(Calendar.getInstance().getTimeInMillis());

        assertDoesNotThrow(() -> persist(sql, "1000099999", "CN=SMP_receiverCN,O=DIGIT_TEST,C=BE:0000000000099999", date, date, new ByteArrayInputStream(NEW_CERT_PUBLIC_KEY.getBytes()), date, 1, date, date));
    }

    @Test
    void persistIntoCertificateTablePKNotOk() throws SQLException {
        String sql = "INSERT INTO bdmsl_certificate(id,certificate_id,valid_from,valid_until,pem_encoding,new_cert_change_date,new_cert_id,created_on,last_updated_on) values (?,?,?,?,?,?,?,?,?)";
        Timestamp date = new Timestamp(Calendar.getInstance().getTimeInMillis());

        persist(sql, "99999", "CN=SMP_receiverCN,O=DIGIT_TEST_1,C=BE:0000000000123456", date, date, new ByteArrayInputStream(NEW_CERT_PUBLIC_KEY.getBytes()), date, 1, date, date);
        assertThrows(SQLException.class,
                () -> persist(sql, "99999", "CN=SMP_receiverCN,O=DIGIT_TEST_1,C=BE:0000000000123456", date, date, new ByteArrayInputStream(NEW_CERT_PUBLIC_KEY.getBytes()), date, 1, date, date));
    }

    @Test
    void persistIntoCertificateTableNotUnique() throws SQLException {
        String sql = "INSERT INTO bdmsl_certificate(id,certificate_id,valid_from,valid_until,pem_encoding,new_cert_change_date,new_cert_id,created_on,last_updated_on) values (?,?,?,?,?,?,?,?,?)";
        Timestamp date = new Timestamp(Calendar.getInstance().getTimeInMillis());

        persist(sql, "191919", "CN=SMP_receiverCN,O=DIGIT_TEST_1,C=BE:0000000000123456", date, date, new ByteArrayInputStream(NEW_CERT_PUBLIC_KEY.getBytes()), date, 1, date, date);
        assertThrows(SQLException.class,
                () -> persist(sql, "202020", "CN=SMP_receiverCN,O=DIGIT_TEST_1,C=BE:0000000000123456", date, date, new ByteArrayInputStream(NEW_CERT_PUBLIC_KEY.getBytes()), date, 1, date, date));
    }


    @Test
    void persistIntoCertificateTableUniqueNotOk() {
        String sql = "INSERT INTO bdmsl_certificate(id,certificate_id,valid_from,valid_until,pem_encoding,new_cert_change_date,new_cert_id,created_on,last_updated_on) values (?,?,?,?,?,?,?,?,?)";
        Timestamp date = new Timestamp(Calendar.getInstance().getTimeInMillis());

        assertThrows(SQLException.class,
                () -> persist(sql, "99999999", "CN=SMP_receiverCN,O=DIGIT,C=BE:0000000000000001", date, date, new ByteArrayInputStream(NEW_CERT_PUBLIC_KEY.getBytes()), date, 1, date, date));
    }

    private void persist(String sqlQuery, String id, String certificate, Timestamp validFrom, Timestamp validTo, ByteArrayInputStream pem, Timestamp certificateChangedOn, Integer newCertificateId, Timestamp createdOn, Timestamp updatedOn) throws SQLException {
        persist(dataSource.getConnection(), sqlQuery, id, certificate, validFrom, validTo, pem, certificateChangedOn, newCertificateId, createdOn, updatedOn);
    }

    void persist(Connection connection, String sql, String id, String certificate, Timestamp validFrom, Timestamp validTo, ByteArrayInputStream pem, Timestamp certificateChangedOn, Integer newCertificateId, Timestamp createdOn, Timestamp updatedOn) throws SQLException {
        try (connection;
             PreparedStatement pstmt = connection.prepareStatement(sql);
             InputStreamReader pemReader = new InputStreamReader(pem)
        ) {
            pstmt.setString(1, id);
            pstmt.setString(2, certificate);
            pstmt.setTimestamp(3, validFrom);
            pstmt.setTimestamp(4, validTo);
            pstmt.setClob(5, pemReader);
            pstmt.setTimestamp(6, certificateChangedOn);
            pstmt.setObject(7, newCertificateId);
            pstmt.setTimestamp(8, createdOn);
            pstmt.setTimestamp(9, updatedOn);
            pstmt.executeUpdate();
            connection.commit();
        } catch (IOException e) {
            loggingService.error("An error occurred while reading the PEM file", e);
            fail("Should have not thrown an exception when reading the PEM file", e);
        }
    }
}
