/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.utils;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;


class SMLSchemaGeneratorTest {

    private static final String DIALECT_ORACLE = "org.hibernate.dialect.OracleDialect";
    private static final String DIALECT_MYSQL_INNO5 = "org.hibernate.dialect.MySQL5InnoDBDialect";
    private static final String ENTITY_PACKAGE = "eu.europa.ec.bdmsl.dao.entity";

    private static Object[] dialectTestCases() {
        return new Object[][]{
                {DIALECT_MYSQL_INNO5, "eu.europa.ec.bdmsl.dao.utils.SMLMySQL5InnoDBDialect"},
                {DIALECT_ORACLE, DIALECT_ORACLE},
                {"org.hibernate.dialect.MySQLDialect", "org.hibernate.dialect.MySQLDialect"},
                {null, null},

        };
    }


    SMLSchemaGenerator testInstance = new SMLSchemaGenerator();


    @Test
    void createDDLScript() throws ClassNotFoundException, IOException {
        // given
        String folder = "target";
        String dialect = DIALECT_ORACLE;
        String version = "4.1.0-SNAPSHOT";
        List<String> lstPackages = Collections.singletonList(ENTITY_PACKAGE);
        File f = new File("target/oracle.ddl");
        f.delete(); // delete if exists
        assertFalse(f.exists());
        // when
        testInstance.createDDLScript(folder, dialect, lstPackages, version);
        // then
        assertTrue(f.exists());
        assertTrue(f.length() > 0);
    }

    @Test
    void createFileNameOracleDialect() {
        //given when
        String fileName = testInstance.createFileName(DIALECT_ORACLE);
        // then
        assertEquals("oracle.ddl", fileName);
    }

    @Test
    void createFileNameMySQLDialect() {
        //given when
        String filaName = testInstance.createFileName(DIALECT_MYSQL_INNO5);
        // then
        assertEquals("mysql5innodb.ddl", filaName);
    }

    @ParameterizedTest
    @MethodSource("dialectTestCases")
    void getDialect(String input, String output) {

        //when
        String result = testInstance.getDialect(input);
        // then
        assertEquals(output, result);
    }

    @Test
    void getAllEntityClassesNotFound(){

        ClassNotFoundException result = assertThrows(ClassNotFoundException.class, () -> testInstance.getAllEntityClasses("eu.not.exists"));
        MatcherAssert.assertThat(result.getMessage(), CoreMatchers.containsString("eu.not.exists"));

    }

    @Test
    void getAllEntityClasses() throws ClassNotFoundException, IOException {

        // given when
        List<Class> result = testInstance.getAllEntityClasses(ENTITY_PACKAGE);

        assertEquals(10, result.size());
    }
}
