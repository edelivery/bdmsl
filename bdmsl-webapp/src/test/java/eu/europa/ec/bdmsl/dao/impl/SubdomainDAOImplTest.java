/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.impl;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import eu.europa.ec.bdmsl.common.exception.NotFoundException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.dao.ISubdomainDAO;
import eu.europa.ec.bdmsl.test.CommonTestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import jakarta.persistence.PersistenceException;
import jakarta.transaction.Transactional;
import java.math.BigInteger;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Tiago MIGUEL
 * @since 30/03/2017
 */
class SubdomainDAOImplTest extends AbstractJUnit5Test {


    static final String CERTIFICATE_POLICY_QCP_NATURAL = "0.4.0.194112.1.0";
    static final String CERTIFICATE_POLICY_QCP_LEGAL = "0.4.0.194112.1.1";

    @Autowired
    private ISubdomainDAO subdomainDAO;

    SubdomainBO testSubDomain = CommonTestUtils.createSubDomainBO("test", "test.ec.europa.eu", ".*", "all", "all");

    @BeforeEach
    @Transactional
    void before() throws TechnicalException {
        subdomainDAO.createSubDomain(testSubDomain);
    }

    @Test
    @Transactional
    void testFindAll() throws TechnicalException {
        //given

        // when
        List<SubdomainBO> subdomainBOList = subdomainDAO.findAll();

        // then
        assertNotNull(subdomainBOList);
        assertFalse(subdomainBOList.isEmpty());
        assertEquals("test.isalive.test", subdomainBOList.get(0).getSubdomainName());
        assertEquals("acc.edelivery.tech.ec.europa.eu", subdomainBOList.get(1).getSubdomainName());
        assertEquals("ehealth.acc.edelivery.tech.ec.europa.eu", subdomainBOList.get(2).getSubdomainName());
        assertEquals("ehealth.edelivery.tech.ec.europa.eu", subdomainBOList.get(3).getSubdomainName());
        assertEquals("sea.acc.edelivery.tech.ec.europa.eu", subdomainBOList.get(4).getSubdomainName());
        assertEquals("toledo.acc.edelivery.tech.ec.europa.eu", subdomainBOList.get(5).getSubdomainName());
        assertEquals("delta.acc.edelivery.tech.ec.europa.eu", subdomainBOList.get(6).getSubdomainName());
    }


    @Test
    @Transactional
    void createSubDomainTest() throws TechnicalException {
        // given
        SubdomainBO subdomainBO = CommonTestUtils.createSubDomainBO("domain", "domain.ec.europa.eu", ".*", "all", "all", "SMP_.*");
        assertNull(subdomainBO.getSubdomainId());
        // when
        subdomainDAO.createSubDomain(subdomainBO);

        // then
        Optional<SubdomainBO> res = subdomainDAO.getSubDomainByName(subdomainBO.getSubdomainName());
        assertTrue(res.isPresent());
        assertNotNull(res.get().getSubdomainId());
        assertEquals(subdomainBO.getSubdomainName(), res.get().getSubdomainName());
        assertEquals(subdomainBO.getDnsRecordTypes(), res.get().getDnsRecordTypes());
        assertEquals(subdomainBO.getDnsZone(), res.get().getDnsZone());
        assertEquals(subdomainBO.getParticipantIdRegexp(), res.get().getParticipantIdRegexp());
        assertEquals(subdomainBO.getSmpUrlSchemas(), res.get().getSmpUrlSchemas());
        assertEquals(subdomainBO.getSmpCertSubjectRegex(), res.get().getSmpCertSubjectRegex());
        assertEquals(subdomainBO.getMaxParticipantCountForDomain(), res.get().getMaxParticipantCountForDomain());
        assertEquals(subdomainBO.getMaxParticipantCountForSMP(), res.get().getMaxParticipantCountForSMP());
        // no exception occurs
    }

    @Test
    @Transactional
    void databaseConstraintSubDomainDuplicateNameTest() {
        // given
        SubdomainBO subdomainBO = CommonTestUtils.createSubDomainBO(testSubDomain.getSubdomainName(), "domain.ec.europa.eu", ".*", "all", "all");

        // when
        assertThrows(PersistenceException.class,
                () ->
                        subdomainDAO.createSubDomain(subdomainBO)
        );

    }

    @Test
    @Transactional
    void databaseConstraintNullZoneDuplicateNameTest() {
        // given
        SubdomainBO subdomainBO = CommonTestUtils.createSubDomainBO("domaintest", null, ".*", "all", "all");
        // when

        assertThrows(PersistenceException.class,
                () ->
                        subdomainDAO.createSubDomain(subdomainBO)
        );
    }

    @Test
    @Transactional
    void updateSubDomainTestAll() throws TechnicalException {
        BigInteger maxParticipantCountForDomain = BigInteger.valueOf(1000000);
        BigInteger maxParticipantCountForSMP = BigInteger.valueOf(99999);
        String certificatePolicy = CERTIFICATE_POLICY_QCP_NATURAL + " , " + CERTIFICATE_POLICY_QCP_LEGAL;
        // when
        subdomainDAO.updateSubDomainValues(testSubDomain.getSubdomainName(),
                "test-regular-value", "CNAME", "HTTPS", "^SMP_.*",
                certificatePolicy,
                maxParticipantCountForDomain, maxParticipantCountForSMP);

        // then
        Optional<SubdomainBO> res = subdomainDAO.getSubDomainByName(testSubDomain.getSubdomainName());
        assertNotNull(res.get().getSubdomainId());
        assertEquals("CNAME", res.get().getDnsRecordTypes());
        assertEquals("test-regular-value", res.get().getParticipantIdRegexp());
        assertEquals("HTTPS", res.get().getSmpUrlSchemas());
        assertEquals("^SMP_.*", res.get().getSmpCertSubjectRegex());
        assertEquals(certificatePolicy, res.get().getSmpCertPolicyOIDs());
        assertEquals(2, res.get().getSmpCertPolicyOIDsAsList().size());
        assertTrue(res.get().getSmpCertPolicyOIDsAsList().contains(CERTIFICATE_POLICY_QCP_NATURAL));
        assertTrue(res.get().getSmpCertPolicyOIDsAsList().contains(CERTIFICATE_POLICY_QCP_LEGAL));
        assertEquals(maxParticipantCountForDomain, res.get().getMaxParticipantCountForDomain());
        assertEquals(maxParticipantCountForSMP, res.get().getMaxParticipantCountForSMP());
    }

    @Test
    @Transactional
    void updateSubDomainTestOnlyRegularExp() throws TechnicalException {
        // when
        subdomainDAO.updateSubDomainValues(testSubDomain.getSubdomainName(),
                "test-regular-value", null, null, null, null, null, null);

        // then
        Optional<SubdomainBO> res = subdomainDAO.getSubDomainByName(testSubDomain.getSubdomainName());
        assertNotNull(res.get().getSubdomainId());
        assertEquals(testSubDomain.getDnsRecordTypes(), res.get().getDnsRecordTypes());
        assertEquals("test-regular-value", res.get().getParticipantIdRegexp());
        assertEquals(testSubDomain.getSmpCertSubjectRegex(), res.get().getSmpCertSubjectRegex());
        assertEquals(testSubDomain.getSmpCertPolicyOIDs(), res.get().getSmpCertPolicyOIDs());
        assertEquals(testSubDomain.getSmpUrlSchemas(), res.get().getSmpUrlSchemas());
        assertEquals(testSubDomain.getMaxParticipantCountForDomain(), res.get().getMaxParticipantCountForDomain());
        assertEquals(testSubDomain.getMaxParticipantCountForSMP(), res.get().getMaxParticipantCountForSMP());
    }

    @Test
    @Transactional
    void updateSubDomainTestOnlyDNSType() throws TechnicalException {
        // when
        subdomainDAO.updateSubDomainValues(testSubDomain.getSubdomainName(),
                null, "CNAME", null, null, null, null, null);

        // then
        Optional<SubdomainBO> res = subdomainDAO.getSubDomainByName(testSubDomain.getSubdomainName());
        assertNotNull(res.get().getSubdomainId());
        assertEquals("CNAME", res.get().getDnsRecordTypes());
        assertNotEquals(testSubDomain.getDnsRecordTypes(), res.get().getDnsRecordTypes());
        assertEquals(testSubDomain.getParticipantIdRegexp(), res.get().getParticipantIdRegexp());
        assertEquals(testSubDomain.getSmpCertSubjectRegex(), res.get().getSmpCertSubjectRegex());
        assertEquals(testSubDomain.getSmpCertPolicyOIDs(), res.get().getSmpCertPolicyOIDs());
        assertEquals(testSubDomain.getMaxParticipantCountForDomain(), res.get().getMaxParticipantCountForDomain());
        assertEquals(testSubDomain.getMaxParticipantCountForSMP(), res.get().getMaxParticipantCountForSMP());
    }

    @Test
    @Transactional
    void updateSubDomainTestOnlyURLScheme() throws TechnicalException {
        // when
        subdomainDAO.updateSubDomainValues(testSubDomain.getSubdomainName(),
                null, null, "HTTPS", null, null, null, null);

        // then
        Optional<SubdomainBO> res = subdomainDAO.getSubDomainByName(testSubDomain.getSubdomainName());
        assertNotNull(res.get().getSubdomainId());
        assertEquals(testSubDomain.getDnsRecordTypes(), res.get().getDnsRecordTypes());
        assertEquals(testSubDomain.getParticipantIdRegexp(), res.get().getParticipantIdRegexp());
        assertEquals("HTTPS", res.get().getSmpUrlSchemas());
        assertNotEquals(testSubDomain.getSmpUrlSchemas(), res.get().getSmpUrlSchemas());
        assertEquals(testSubDomain.getSmpCertSubjectRegex(), res.get().getSmpCertSubjectRegex());
        assertEquals(testSubDomain.getSmpCertPolicyOIDs(), res.get().getSmpCertPolicyOIDs());
        assertEquals(testSubDomain.getMaxParticipantCountForDomain(), res.get().getMaxParticipantCountForDomain());
        assertEquals(testSubDomain.getMaxParticipantCountForSMP(), res.get().getMaxParticipantCountForSMP());
    }

    @Test
    @Transactional
    void updateSubDomainTestOnlySMPCertPolicyOids() throws TechnicalException {
        // when
        subdomainDAO.updateSubDomainValues(testSubDomain.getSubdomainName(),
                null, null, null, null, CERTIFICATE_POLICY_QCP_LEGAL, null, null);

        // then
        Optional<SubdomainBO> res = subdomainDAO.getSubDomainByName(testSubDomain.getSubdomainName());
        assertNotNull(res.get().getSubdomainId());
        assertEquals(testSubDomain.getDnsRecordTypes(), res.get().getDnsRecordTypes());
        assertEquals(testSubDomain.getParticipantIdRegexp(), res.get().getParticipantIdRegexp());
        assertEquals(testSubDomain.getSmpUrlSchemas(), res.get().getSmpUrlSchemas());
        assertEquals(testSubDomain.getSmpUrlSchemas(), res.get().getSmpUrlSchemas());
        assertEquals(testSubDomain.getSmpCertSubjectRegex(), res.get().getSmpCertSubjectRegex());
        assertEquals(CERTIFICATE_POLICY_QCP_LEGAL, res.get().getSmpCertPolicyOIDs());
        assertEquals(testSubDomain.getMaxParticipantCountForDomain(), res.get().getMaxParticipantCountForDomain());
        assertEquals(testSubDomain.getMaxParticipantCountForSMP(), res.get().getMaxParticipantCountForSMP());
    }

    @Test
    @Transactional
    void updateSubDomainTestOnlySMPCertRegExp() throws TechnicalException {
        // when
        subdomainDAO.updateSubDomainValues(testSubDomain.getSubdomainName(),
                null, null, null, "SMP_CERT_REG_EXP", null, null, null);

        // then
        Optional<SubdomainBO> res = subdomainDAO.getSubDomainByName(testSubDomain.getSubdomainName());
        assertNotNull(res.get().getSubdomainId());
        assertEquals(testSubDomain.getDnsRecordTypes(), res.get().getDnsRecordTypes());
        assertEquals(testSubDomain.getParticipantIdRegexp(), res.get().getParticipantIdRegexp());
        assertEquals(testSubDomain.getSmpUrlSchemas(), res.get().getSmpUrlSchemas());
        assertEquals(testSubDomain.getSmpUrlSchemas(), res.get().getSmpUrlSchemas());
        assertEquals("SMP_CERT_REG_EXP", res.get().getSmpCertSubjectRegex());
        assertEquals(testSubDomain.getSmpCertPolicyOIDs(), res.get().getSmpCertPolicyOIDs());
        assertEquals(testSubDomain.getMaxParticipantCountForDomain(), res.get().getMaxParticipantCountForDomain());
        assertEquals(testSubDomain.getMaxParticipantCountForSMP(), res.get().getMaxParticipantCountForSMP());
    }

    @Test
    @Transactional
    void updateSubDomainTestOnlyMaxParticipantCountForDomain() throws TechnicalException {
        // when
        BigInteger maxParticipantCountForDomain = BigInteger.valueOf(1234567);
        subdomainDAO.updateSubDomainValues(testSubDomain.getSubdomainName(),
                null, null, null, null, null, maxParticipantCountForDomain, null);

        // then
        Optional<SubdomainBO> res = subdomainDAO.getSubDomainByName(testSubDomain.getSubdomainName());
        assertNotNull(res.get().getSubdomainId());
        assertEquals(testSubDomain.getDnsRecordTypes(), res.get().getDnsRecordTypes());
        assertEquals(testSubDomain.getParticipantIdRegexp(), res.get().getParticipantIdRegexp());
        assertEquals(testSubDomain.getSmpUrlSchemas(), res.get().getSmpUrlSchemas());
        assertEquals(testSubDomain.getSmpUrlSchemas(), res.get().getSmpUrlSchemas());
        assertEquals(testSubDomain.getSmpCertSubjectRegex(), res.get().getSmpCertSubjectRegex());
        assertEquals(testSubDomain.getSmpCertPolicyOIDs(), res.get().getSmpCertPolicyOIDs());
        assertEquals(maxParticipantCountForDomain, res.get().getMaxParticipantCountForDomain());
        assertEquals(testSubDomain.getMaxParticipantCountForSMP(), res.get().getMaxParticipantCountForSMP());
    }

    @Test
    @Transactional
    void updateSubDomainTestOnlyMaxParticipantCountForSMP() throws TechnicalException {
        // when
        BigInteger maxParticipantCountForSMP = BigInteger.valueOf(1234);
        subdomainDAO.updateSubDomainValues(testSubDomain.getSubdomainName(),
                null, null, null, null, null, null, maxParticipantCountForSMP);

        // then
        Optional<SubdomainBO> res = subdomainDAO.getSubDomainByName(testSubDomain.getSubdomainName());
        assertNotNull(res.get().getSubdomainId());
        assertEquals(testSubDomain.getDnsRecordTypes(), res.get().getDnsRecordTypes());
        assertEquals(testSubDomain.getParticipantIdRegexp(), res.get().getParticipantIdRegexp());
        assertEquals(testSubDomain.getSmpUrlSchemas(), res.get().getSmpUrlSchemas());
        assertEquals(testSubDomain.getSmpUrlSchemas(), res.get().getSmpUrlSchemas());
        assertEquals(testSubDomain.getSmpCertSubjectRegex(), res.get().getSmpCertSubjectRegex());
        assertEquals(testSubDomain.getSmpCertPolicyOIDs(), res.get().getSmpCertPolicyOIDs());
        assertEquals(testSubDomain.getMaxParticipantCountForDomain(), res.get().getMaxParticipantCountForDomain());
        assertEquals(maxParticipantCountForSMP, res.get().getMaxParticipantCountForSMP());
    }

    @Test
    @Transactional
    void updateSubDomainNotExists() throws TechnicalException {
        // given
        String randomString = UUID.randomUUID().toString();
        // then-when
        NotFoundException notFoundException = assertThrows(NotFoundException.class,
                () ->
                        subdomainDAO.updateSubDomainValues(randomString,
                                "test-regular-value", "CNAME", "HTTPS", ".*", null, null, null)
        );
        assertEquals("[ERR-118] SubDomain does not exist: " + randomString,notFoundException.getMessage());

    }

    @Test
    @Transactional
    void deleteSubDomain() throws TechnicalException {

        Optional<SubdomainBO> resBefore = subdomainDAO.getSubDomainByName(testSubDomain.getSubdomainName());
        assertTrue(resBefore.isPresent());

        // when
        subdomainDAO.deleteSubDomain(testSubDomain.getSubdomainName());

        // then
        Optional<SubdomainBO> resAfter = subdomainDAO.getSubDomainByName(testSubDomain.getSubdomainName());
        assertFalse(resAfter.isPresent());
    }

    @Test
    @Transactional
    void deleteSubDomainNotExists() throws TechnicalException {
        // given
        String randomString = UUID.randomUUID().toString();

        // then-when
        assertThrows(NotFoundException.class,
                () ->
                        subdomainDAO.deleteSubDomain(randomString),"[ERR-118] SubDomain does not exist: " + randomString
        );
    }

}
