/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */

/**
 * @author Flavio SANTOS
 */
package eu.europa.ec.bdmsl.dao.impl;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.bo.CertificateBO;
import eu.europa.ec.bdmsl.common.bo.WildcardBO;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.dao.IWildcardDAO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

class WildcardDAOImplTest extends AbstractJUnit5Test {

    @Autowired
    private IWildcardDAO wildcardDAO;

    @Test
    @Transactional
    void testChangeWildcardAuthorization() throws TechnicalException {
        //GIVEN
        CertificateBO certificateBO = new CertificateBO();
        certificateBO.setId(new Long("9"));
        WildcardBO wildcardBO = wildcardDAO.findWildcard("iso123456-actorid-upis", certificateBO);

        //WHEN
        wildcardDAO.changeWildcardAuthorization(new Long("5"), new Long("9"));

        //THEN
        assertNull(wildcardBO);
        wildcardBO = wildcardDAO.findWildcard("iso123456-actorid-upis", certificateBO);
        assertNotNull(wildcardBO);
    }

    @Test
    void testFindWildcard() throws TechnicalException {
        //GIVEN
        CertificateBO certificateBO = new CertificateBO();
        certificateBO.setId(new Long("9999999"));

        //WHEN
        WildcardBO wildcardBO = wildcardDAO.findWildcard("iso9999999-id-scheme", certificateBO);

        //GIVEN
        assertNotNull(wildcardBO);
    }
}
