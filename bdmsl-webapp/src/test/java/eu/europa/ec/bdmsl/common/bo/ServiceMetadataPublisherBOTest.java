/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.bo;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.exception.GenericTechnicalException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


/**
 * @author Flavio SANTOS
 */
class ServiceMetadataPublisherBOTest extends AbstractJUnit5Test {

    @Test
    void testSmpBOCcreateDNSRecordOk() throws Exception {
        //GIVEN
        SubdomainBO subdomainBO = new SubdomainBO();
        subdomainBO.setSubdomainId(new Long("1"));
        subdomainBO.setSubdomainName("sample.digit.com");
        ServiceMetadataPublisherBO serviceMetadataPublisherBO = new ServiceMetadataPublisherBO();
        serviceMetadataPublisherBO.setSmpId("SMP_SAMPLE_ID");
        serviceMetadataPublisherBO.setSubdomain(subdomainBO);

        //WHEN-THEN
        assertNull(serviceMetadataPublisherBO.getDnsRecord());
        serviceMetadataPublisherBO.createDNSRecord("publisher");
        assertEquals("SMP_SAMPLE_ID.publisher.sample.digit.com.", serviceMetadataPublisherBO.getDnsRecord());
    }

    @Test
    void testSmpBOCcreateDNSRecordWithNullSubdomain() throws Exception {
        //GIVEN
        ServiceMetadataPublisherBO serviceMetadataPublisherBO = new ServiceMetadataPublisherBO();
        serviceMetadataPublisherBO.setSmpId("SMP_SAMPLE_ID");

        //WHEN-THEN
      assertThrows(GenericTechnicalException.class,
                () -> serviceMetadataPublisherBO.createDNSRecord("publisher"));

    }

    @Test
    void testSmpBOCcreateDNSRecordWithNullPrefix() throws Exception {
        //GIVEN
        ServiceMetadataPublisherBO serviceMetadataPublisherBO = new ServiceMetadataPublisherBO();
        serviceMetadataPublisherBO.setSmpId("SMP_SAMPLE_ID");

        //WHEN-THEN
        assertThrows(GenericTechnicalException.class,
                () -> serviceMetadataPublisherBO.createDNSRecord(null));

    }
}
