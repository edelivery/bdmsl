package eu.europa.ec.bdmsl.service.validation;

import eu.europa.ec.bdmsl.common.enums.SMLPropertyEnum;
import eu.europa.ec.bdmsl.common.exception.BadConfigurationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @since 5.0
 * @author Sebastian-Ion TINCU
 */
public class KeystoreTypeSMLPropertyValidatorTest {

    private KeystoreTypeSMLPropertyValidator validator = new KeystoreTypeSMLPropertyValidator();

    @Test
    public void acceptsSMLPropertyBOOfKeystoreTypeHavingJKSTypeIgnoringCase() {
        Assertions.assertDoesNotThrow(() -> validator.validate(SMLPropertyEnum.KEYSTORE_TYPE, "jKs", null));
    }

    @Test
    public void acceptsSMLPropertyBOOfKeystoreTypeHavingPKCS12TypeIgnoringCase() {
        Assertions.assertDoesNotThrow(() -> validator.validate(SMLPropertyEnum.KEYSTORE_TYPE, "pkcs12", null));
    }

    @Test
    public void rejectsSMLPropertyBOOfKeystoreNotTypeHavingPKCS12OrJKSTypeIgnoringCase() {
        BadConfigurationException validationException = Assertions.assertThrows(BadConfigurationException.class, () -> validator.validate(SMLPropertyEnum.KEYSTORE_TYPE, "jceks", null));

        Assertions.assertEquals("[ERR-109] Property: keystoreType doesn't have the required value (PKCS12, JKS): jceks", validationException.getMessage());
    }

    @Test
    public void acceptsSMLPropertyBOOfTruststoreTypeHavingJKSTypeIgnoringCase() {
        Assertions.assertDoesNotThrow(() -> validator.validate(SMLPropertyEnum.TRUSTSTORE_TYPE, "jKs", null));
    }

    @Test
    public void acceptsSMLPropertyBOOfTruststoreTypeHavingPKCS12TypeIgnoringCase() {
        Assertions.assertDoesNotThrow(() -> validator.validate(SMLPropertyEnum.TRUSTSTORE_TYPE, "pkcs12", null));
    }

    @Test
    public void rejectsSMLPropertyBOOfTruststoreNotTypeHavingPKCS12OrJKSTypeIgnoringCase() {
        BadConfigurationException validationException = Assertions.assertThrows(BadConfigurationException.class, () -> validator.validate(SMLPropertyEnum.TRUSTSTORE_TYPE, "JCEKS", null));

        Assertions.assertEquals("[ERR-109] Property: truststoreType doesn't have the required value (PKCS12, JKS): JCEKS", validationException.getMessage());
    }
}