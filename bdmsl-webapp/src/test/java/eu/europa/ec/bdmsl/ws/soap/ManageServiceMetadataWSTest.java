/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.ws.soap;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.business.IManageServiceMetadataBusiness;
import eu.europa.ec.bdmsl.common.bo.CertificateDomainBO;
import eu.europa.ec.bdmsl.common.bo.ServiceMetadataPublisherBO;
import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import eu.europa.ec.bdmsl.common.enums.SMLRoleEnum;
import eu.europa.ec.bdmsl.common.exception.CertificateAuthenticationException;
import eu.europa.ec.bdmsl.dao.ICertificateDAO;
import eu.europa.ec.bdmsl.test.CommonTestUtils;
import eu.europa.ec.bdmsl.test.DnsMessageSenderServiceMock;
import eu.europa.ec.bdmsl.security.CertificateAuthentication;
import eu.europa.ec.bdmsl.security.ICertificateAuthentication;
import eu.europa.ec.bdmsl.security.UnsecureAuthentication;
import eu.europa.ec.bdmsl.service.IManageServiceMetadataService;
import eu.europa.ec.bdmsl.service.impl.ManageServiceMetadataServiceImpl;
import eu.europa.ec.edelivery.security.ClientCertAuthenticationFilter;
import eu.europa.ec.edelivery.security.PreAuthenticatedCertificatePrincipal;
import org.busdox.servicemetadata.locator._1.PageRequest;
import org.busdox.servicemetadata.locator._1.PublisherEndpointType;
import org.busdox.servicemetadata.locator._1.ServiceMetadataPublisherServiceType;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.xbill.DNS.DClass;
import org.xbill.DNS.NAPTRRecord;
import org.xbill.DNS.Name;

import java.security.Principal;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collections;

import static eu.europa.ec.bdmsl.test.TestConstants.DOMAIN_PEPPOL;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

/**
 * @author Adrien FERIAL
 * @since 16/06/2015
 */
class ManageServiceMetadataWSTest extends AbstractJUnit5Test {

    @Autowired
    private IManageServiceMetadataWS testInstance;


    @Autowired
    private IManageServiceMetadataService manageServiceMetadataService;

    @Autowired
    private IManageServiceMetadataBusiness manageServiceMetadataBusiness;

    @Autowired
    private ICertificateDAO certificateDAO;

    @Autowired
    private IManageParticipantIdentifierWS manageParticipantIdentifierWS;


    @Autowired
    private ICertificateAuthentication customAuthentication;


    ClientCertAuthenticationFilter blueCoatAuthenticationFilter = new ClientCertAuthenticationFilter();

    @BeforeAll
    static void beforeClass() {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    @BeforeEach
    protected void init() throws Exception {

        UnsecureAuthentication authentication = new UnsecureAuthentication();
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        ManageServiceMetadataServiceImpl manageServiceMetadataServiceImpl = CommonTestUtils.getTargetObject(manageServiceMetadataService);
        ReflectionTestUtils.setField(manageServiceMetadataServiceImpl, "dnsClientService", this.dnsClientService);

        ReflectionTestUtils.setField(manageServiceMetadataService, "manageServiceMetadataBusiness", this.manageServiceMetadataBusiness);
        ReflectionTestUtils.setField(manageServiceMetadataBusiness, "configurationBusiness", this.configurationBusiness);


    }

    @Test
    void testRead() throws Exception {
        ServiceMetadataPublisherServiceType input = new ServiceMetadataPublisherServiceType();
        input.setServiceMetadataPublisherID("foundUnsecure");
        ServiceMetadataPublisherServiceType result = testInstance.read(input);
        assertEquals(input.getServiceMetadataPublisherID(), result.getServiceMetadataPublisherID());
        assertNotNull(result.getPublisherEndpoint());
    }

    @Test
    void testReadNull() {
        assertThrows(BadRequestFault.class, () -> testInstance.read(null));
    }

    @Test
    void testReadEmpty() {
        assertThrows(BadRequestFault.class, () -> testInstance.read(new ServiceMetadataPublisherServiceType()));
    }

    @Test
    void testReadNotFound() {
        ServiceMetadataPublisherServiceType smp = new ServiceMetadataPublisherServiceType();
        smp.setServiceMetadataPublisherID("SmpNotFoundException");

        assertThrows(NotFoundFault.class, () -> testInstance.read(smp));

    }

    @Test
    void testCreateNull() {
        assertThrows(BadRequestFault.class, () -> testInstance.create(null));
    }

    @Test
    void testCreateEmpty() {
        assertThrows(BadRequestFault.class, () -> testInstance.create(new ServiceMetadataPublisherServiceType()));
    }

    @Test
    void testCreateBadPhysicalAddress() {
        ServiceMetadataPublisherServiceType smp = new ServiceMetadataPublisherServiceType();
        PublisherEndpointType publisherEndpointType = new PublisherEndpointType();
        publisherEndpointType.setLogicalAddress("http://logical");
        publisherEndpointType.setPhysicalAddress("PhysicalWrong");
        smp.setServiceMetadataPublisherID("smpId");
        smp.setPublisherEndpoint(publisherEndpointType);

        assertThrows(BadRequestFault.class, () -> testInstance.create(smp));
    }

    @Test
    void testCreateAlreadyExist() {
        ServiceMetadataPublisherServiceType smp = createSimpleSMP("found");
        assertThrows(BadRequestFault.class, () -> testInstance.create(smp));
    }

    /**
     * SMP ids are case insensitive
     *
     * @throws Exception
     */
    @Test
    void testCreateAlreadyExistCaseInsensitivity() {
        ServiceMetadataPublisherServiceType smp = createSimpleSMP("FoUnD");
        assertThrows(BadRequestFault.class, () -> testInstance.create(smp));
    }

    private String createSmp(ServiceMetadataPublisherServiceType smpToBeCreated, String smpId) throws Exception {
        smpToBeCreated.setServiceMetadataPublisherID(smpId);

        DnsMessageSenderServiceMock dnsMessageSenderService = (DnsMessageSenderServiceMock) this.dnsMessageSenderService;
        assertTrue(dnsMessageSenderService.getMessages().isEmpty());

        ServiceMetadataPublisherServiceType smp = createSimpleSMP(smpId);
        testInstance.create(smp);

        return dnsMessageSenderService.getMessages();
    }

    @Test
    void testCreateOkBlueCoatCertificate() throws Exception {
        // Blue Coat Authentication
        String serial = "000000000123ABCD";
        String issuer = "CN=rootCN,OU=B4,O=DIGIT,L=Brussels,ST=BE,C=BE";
        String subject = "CN=SMP_TEST_CHANGE_CERTIFICATE_FOUND,O=DG-DIGIT,C=BE";
        String authorizedSubDomain = DOMAIN_PEPPOL;
        SubdomainBO subdomainBO = subdomainDAO.getSubDomain(authorizedSubDomain);
        CertificateDomainBO certificateDomainBO = new CertificateDomainBO();
        certificateDomainBO.setSubdomain(subdomainBO);

        String certHeaderValue = CommonTestUtils.createHeaderCertificateForBlueCoat(serial, issuer, subject);

        Principal principal = blueCoatAuthenticationFilter.buildPrincipalFromHeader(certHeaderValue);
        Authentication bcAuth = new CertificateAuthentication(certificateDomainBO, (PreAuthenticatedCertificatePrincipal) principal, Collections.singletonList(SMLRoleEnum.ROLE_SMP.getAuthority()));
        bcAuth.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(bcAuth);
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());


        // first we ensure that the data don't exist and create SMP
        ServiceMetadataPublisherServiceType smpToBeCreated = new ServiceMetadataPublisherServiceType();
        String messages = createSmp(smpToBeCreated, "notExistBlue");

        assertTrue(messages.contains("notExistBlue.publisher.acc.edelivery.tech.ec.europa.eu.\t0\tANY\tANY"));
        assertTrue(messages.contains("notExistBlue.publisher.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tCNAME\tlogical."));

        // Then we check that the data have been created
        assertNotNull(testInstance.read(smpToBeCreated));
        assertNotNull(certificateDAO.findCertificateByCertificateId("CN=SMP_TEST_CHANGE_CERTIFICATE_FOUND,O=DG-DIGIT,C=BE:0000000000000000000000000123abcd"));
    }

    @Test
    void testCreateOkX509Certificate_setDomainNameIssuer() throws Exception {
        String issuer = "CN=Issuer,O=DG-DIGIT,C=BE";
        String subject = "C=BE, O=Subdomain-Entityaa123, CN=SMP_TEST_1";
        X509Certificate certificate = createX509CertificateKeystoreTrusted(issuer, subject, true);

        X509Certificate[] certificates = {certificate};

        //Injecting the dummy certificate as an authenticated user
        Authentication notAuth = createX509Authentication(certificates);
        Authentication authentication = smlAuthenticationProvider.authenticate(notAuth);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

        // first we ensure that the data don't exist and create SMP
        ServiceMetadataPublisherServiceType smpToBeCreated = new ServiceMetadataPublisherServiceType();
        String messages = createSmp(smpToBeCreated, "notExistX509I");

        assertTrue(messages.contains("notExistX509I.publisher.delta.acc.edelivery.tech.ec.europa.eu.\t0\tANY\tANY"));
        assertTrue(messages.contains("notExistX509I.publisher.delta.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tCNAME\tlogical."));

        // Then we check that the data have been created
        assertNotNull(testInstance.read(smpToBeCreated));
    }

    @Test
    void testCreateOkX509Certificate_setDomainNameSubject() throws Exception {
        String issuer = "CN=SMP_357951852456789,O=DG-DIGIT,C=BE";
        String subject = "C=BE, O=Subdomain-Entityaa, CN=SMP_Subdomain Subject";
        X509Certificate certificate = createX509CertificateKeystoreTrusted(issuer, subject, true);
        X509Certificate[] certificates = {certificate};

        //Injecting the dummy certificate as an authenticated user
        Authentication notAuth = createX509Authentication(certificates);
        Authentication authentication = smlAuthenticationProvider.authenticate(notAuth);

        SecurityContextHolder.getContext().setAuthentication(authentication);
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));


        // first we ensure that the data don't exist and create SMP
        ServiceMetadataPublisherServiceType smpToBeCreated = new ServiceMetadataPublisherServiceType();
        String messages = createSmp(smpToBeCreated, "notExistX509S");

        assertTrue(messages.contains("notExistX509S.publisher.delta.acc.edelivery.tech.ec.europa.eu.\t0\tANY\tANY"));
        assertTrue(messages.contains("notExistX509S.publisher.delta.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tCNAME\tlogical."));

        // Then we check that the data have been created
        assertNotNull(testInstance.read(smpToBeCreated));
    }

    @Test
    void testDeleteNotFound() {
        assertThrows(BadRequestFault.class, () -> testInstance.delete("Not found"));
    }

    @Test
    void testDeleteNull() {
        assertThrows(BadRequestFault.class, () -> testInstance.delete(null));
    }

    @Test
    void testDeleteEmpty() {
        assertThrows(BadRequestFault.class, () -> testInstance.delete(""));
    }

    @Test
    void testDeleteOk() throws Exception {
        ServiceMetadataPublisherServiceType smpToBeDeleted = new ServiceMetadataPublisherServiceType();
        smpToBeDeleted.setServiceMetadataPublisherID("toBeDeleted");
        assertNotNull(testInstance.read(smpToBeDeleted));

        PageRequest PageRequest = new PageRequest();
        PageRequest.setServiceMetadataPublisherID("toBeDeleted");
        assertEquals(1, manageParticipantIdentifierWS.list(PageRequest).getParticipantIdentifiers().size());

        DnsMessageSenderServiceMock dnsMessageSenderService = (DnsMessageSenderServiceMock) this.dnsMessageSenderService;
        assertTrue(dnsMessageSenderService.getMessages().isEmpty());

        // this SMP is linked to one participant. The participant must also be deleted
        testInstance.delete("toBeDeleted");

        String messages = dnsMessageSenderService.getMessages();
        // participant 0009:223456789DeletedTest1 is also deleted
        assertTrue(messages.contains("B-b44c40da17d7831413ea237888fd6cd4.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu.\t0\tANY\tANY"));
        assertTrue(messages.contains("X7GYQZDDDDKHORMJVCTKZAOGHXSW4C6GLYU2UCINCXSQCYISS3UA.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu.\t0\tANY\tANY"));
        assertTrue(messages.contains("toBeDeleted.publisher.acc.edelivery.tech.ec.europa.eu.\t0\tANY\tANY"));
        try {
            testInstance.read(smpToBeDeleted);
            fail("A SmpNotFoundException should have been raised");
        } catch (NotFoundFault fault) {
            // ok, expected
        }
        try {
            manageParticipantIdentifierWS.list(PageRequest);
            fail("A SmpNotFoundException should have been raised");
        } catch (NotFoundFault fault) {
            // ok, expected
        }
    }

    @Test
    void testDeleteUnauthorized() {
        Mockito.doReturn(false).when(configurationBusiness).isUnsecureLoginEnabled();
        assertThrows(UnauthorizedFault.class, () -> testInstance.delete("found"));
    }

    @Test
    void testUpdateUnauthorized() {
        Mockito.doReturn(false).when(configurationBusiness).isUnsecureLoginEnabled();
        String smpId = "found";
        ServiceMetadataPublisherServiceType smpToBeUpdated = createSimpleSMP(smpId);
        smpToBeUpdated.setServiceMetadataPublisherID(smpId);
        assertThrows(UnauthorizedFault.class, () -> testInstance.update(smpToBeUpdated));
    }

    @Test
    void testUpdateUnauthorizedDifferentCase() {
        Mockito.doReturn(false).when(configurationBusiness).isUnsecureLoginEnabled();
        String smpId = "FoUnD";
        ServiceMetadataPublisherServiceType smpToBeUpdated = createSimpleSMP(smpId);
        smpToBeUpdated.setServiceMetadataPublisherID(smpId);
        assertThrows(UnauthorizedFault.class, () -> testInstance.update(smpToBeUpdated));
    }

    @Test
    void testUpdateOnlyDNSRecordsCNAMEOk() throws Exception {
        String smpId = "toBeUpdated"; // is registered to doain with only  CNAME
        ServiceMetadataPublisherServiceType smpToBeUpdated = createSimpleSMP(smpId);
        ServiceMetadataPublisherServiceType found = testInstance.read(smpToBeUpdated);
        assertEquals("10.10.10.10", found.getPublisherEndpoint().getPhysicalAddress());
        assertEquals("http://logicalAddress", found.getPublisherEndpoint().getLogicalAddress());
        String naptrName = "B4UGYBIZS6GY5I5TVZXZWUQGCVT3G7BQ4BYTNNCWANSQQCZNS6JA.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu.";
        NAPTRRecord naptrRecord = new NAPTRRecord(Name.fromString(naptrName), DClass.IN, 60, 100, 10, "U", "Meta:SMP", "!.*!smp.domain.edelivery.com!", Name.fromString("."));
        Mockito.doReturn(Arrays.asList(naptrRecord)).when(super.dnsClientService).lookup(any(String.class), any(Integer.class));
        ManageServiceMetadataServiceImpl manageServiceMetadataServiceImpl = CommonTestUtils.getTargetObject(manageServiceMetadataService);
        ReflectionTestUtils.setField(manageServiceMetadataServiceImpl, "dnsClientService", super.dnsClientService);

        smpToBeUpdated.getPublisherEndpoint().setPhysicalAddress("50.50.50.50");
        smpToBeUpdated.getPublisherEndpoint().setLogicalAddress("http://logicalAddressUpdated                   ");

        DnsMessageSenderServiceMock dnsMessageSenderService = (DnsMessageSenderServiceMock) this.dnsMessageSenderService;
        assertTrue(dnsMessageSenderService.getMessages().isEmpty());

        smpToBeUpdated.setServiceMetadataPublisherID(smpId);
        testInstance.update(smpToBeUpdated);

        String messages = dnsMessageSenderService.getMessages();
        // update message contains only smp update - naptr and CNAME values are not updated
        // because it is only CNAME domain
        assertTrue(messages.contains("toBeUpdated.publisher.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tCNAME\tlogicalAddressUpdated."));
        assertFalse(messages.contains("B4UGYBIZS6GY5I5TVZXZWUQGCVT3G7BQ4BYTNNCWANSQQCZNS6JA."));
        assertFalse(messages.contains("B-"));

        found = testInstance.read(smpToBeUpdated);
        assertEquals("50.50.50.50", found.getPublisherEndpoint().getPhysicalAddress());
        assertEquals("http://logicalAddressUpdated", found.getPublisherEndpoint().getLogicalAddress());
    }


    @Test
    void testUpdateOnlyALLDNSRecordsOk() throws Exception {
        String smpId = "toBeUpdated"; // is registered to doain with only  CNAME
        ServiceMetadataPublisherServiceType smpToBeUpdated = createSimpleSMP(smpId);
        ServiceMetadataPublisherBO smpbo = manageServiceMetadataService.read(smpId);
        ServiceMetadataPublisherServiceType found = testInstance.read(smpToBeUpdated);
        // update subdumain for all DNS records// default is CNAME
        subdomainDAO.updateSubDomainValues(smpbo.getSubdomain().getSubdomainName(),
                ".*", "all", "all", ".*", null, null, null);

        assertEquals("10.10.10.10", found.getPublisherEndpoint().getPhysicalAddress());
        assertEquals("http://logicalAddress", found.getPublisherEndpoint().getLogicalAddress());
        String naptrName = "B4UGYBIZS6GY5I5TVZXZWUQGCVT3G7BQ4BYTNNCWANSQQCZNS6JA.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu.";
        NAPTRRecord naptrRecord = new NAPTRRecord(Name.fromString(naptrName), DClass.IN, 60, 100, 10, "U", "Meta:SMP", "!.*!smp.domain.edelivery.com!", Name.fromString("."));
        Mockito.doReturn(Arrays.asList(naptrRecord)).when(super.dnsClientService).lookup(any(String.class), any(Integer.class));
        ManageServiceMetadataServiceImpl manageServiceMetadataServiceImpl = CommonTestUtils.getTargetObject(manageServiceMetadataService);
        ReflectionTestUtils.setField(manageServiceMetadataServiceImpl, "dnsClientService", super.dnsClientService);

        smpToBeUpdated.getPublisherEndpoint().setPhysicalAddress("50.50.50.50");
        smpToBeUpdated.getPublisherEndpoint().setLogicalAddress("http://logicalAddressUpdated                   ");

        DnsMessageSenderServiceMock dnsMessageSenderService = (DnsMessageSenderServiceMock) this.dnsMessageSenderService;
        assertTrue(dnsMessageSenderService.getMessages().isEmpty());

        smpToBeUpdated.setServiceMetadataPublisherID(smpId);
        testInstance.update(smpToBeUpdated);

        String messages = dnsMessageSenderService.getMessages();
        // update message contains naptr and  smp update -
        // because it has all dns record types
        assertTrue(messages.contains("toBeUpdated.publisher.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tCNAME\tlogicalAddressUpdated."));
        assertTrue(messages.contains("B4UGYBIZS6GY5I5TVZXZWUQGCVT3G7BQ4BYTNNCWANSQQCZNS6JA.iso6523-actorid-upis.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tNAPTR\t100 10 \"U\" \"Meta:SMP\" \"!.*!http://logicalAddressUpdated!\" ."));
        assertFalse(messages.contains("B-"));

        found = testInstance.read(smpToBeUpdated);
        assertEquals("50.50.50.50", found.getPublisherEndpoint().getPhysicalAddress());
        assertEquals("http://logicalAddressUpdated", found.getPublisherEndpoint().getLogicalAddress());
    }

    @Test
    void testUpdateNotFound() {
        String smpId = "Notfound";
        ServiceMetadataPublisherServiceType smpToBeUpdated = createSimpleSMP(smpId);

        assertThrows(NotFoundFault.class, () -> testInstance.update(smpToBeUpdated));
    }

    private ServiceMetadataPublisherServiceType createSimpleSMP(String smpId, String logicalAddress, String physicalAddress) {
        ServiceMetadataPublisherServiceType smpToBeUpdated = new ServiceMetadataPublisherServiceType();
        PublisherEndpointType publisherEndpointType = new PublisherEndpointType();
        publisherEndpointType.setLogicalAddress(logicalAddress);
        publisherEndpointType.setPhysicalAddress(physicalAddress);
        smpToBeUpdated.setPublisherEndpoint(publisherEndpointType);
        smpToBeUpdated.setServiceMetadataPublisherID(smpId);
        return smpToBeUpdated;
    }

    private ServiceMetadataPublisherServiceType createSimpleSMP(String smpId) {
        return createSimpleSMP(smpId, "http://logical        ", "10.10.10.10");
    }

    @Test
    void testUpdateNull() {
        assertThrows(BadRequestFault.class, () -> testInstance.update(null));
    }

    @Test
    void testDeleteWithMigrationPlanned() {
        String smpId = "toBeDeletedWithMigrationPlanned";
        assertThrows(UnauthorizedFault.class, () -> testInstance.delete(smpId));
    }

    @Test
    void testDeleteWithMigrationPlannedDifferentCase() {
        String smpId = "toBeDeletedWithMigrationPLANNED";
        assertThrows(UnauthorizedFault.class, () -> testInstance.delete(smpId));
    }

    @Test
    void testUpdateEmpty() {
        String smpId = "";
        ServiceMetadataPublisherServiceType smpToBeUpdated = new ServiceMetadataPublisherServiceType();
        smpToBeUpdated.setServiceMetadataPublisherID(smpId);
        assertThrows(BadRequestFault.class, () -> testInstance.update(smpToBeUpdated));
    }

    @Test
    void testNotAuthenticatedCertificate() {
        assertThrows(CertificateAuthenticationException.class, () -> customAuthentication.blueCoatAuthentication("asdqwe123"));
    }

    @Test
    void testCreateSMPDifferentDomainOk() throws Exception {

        String serialId = "6caa15ffed56ad049dd2b9bc835d2e46";
        String issuer = "CN=SMP_357951852456,O=DG-DIGIT,C=BE";
        String subject = "C=BE, O=Subdomain-Entityaa123, CN=SMP_TEST_2";
        customAuthentication.blueCoatAuthentication(serialId, issuer, subject, false);

        String smpId = "smpNewDomain";
        ServiceMetadataPublisherServiceType smpToBeCreated = new ServiceMetadataPublisherServiceType();
        smpToBeCreated.setServiceMetadataPublisherID(smpId);

        DnsMessageSenderServiceMock dnsMessageSenderService = (DnsMessageSenderServiceMock) this.dnsMessageSenderService;
        assertTrue(dnsMessageSenderService.getMessages().isEmpty());

        ServiceMetadataPublisherServiceType smp = createSimpleSMP(smpId);
        testInstance.create(smp);

        String messages = dnsMessageSenderService.getMessages();
        assertTrue(messages.contains("smpNewDomain.publisher.delta.acc.edelivery.tech.ec.europa.eu.\t0\tANY\tANY"));
        assertTrue(messages.contains("smpNewDomain.publisher.delta.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tCNAME\tlogical."));

        // Then we check that the data have been created
        assertNotNull(testInstance.read(smpToBeCreated));
        assertNotNull(certificateDAO.findCertificateByCertificateId("CN=SMP_TEST_2,O=Subdomain-Entityaa123,C=BE:6caa15ffed56ad049dd2b9bc835d2e46"));
    }

    @Test
    void testCreateSMPWitchCertificateMatchForIssuerAndSubject() throws Exception {
        String issuer = "C=BE, O=Subdomain-Entity, OU=Subdomain-Entity, ST=Nordrhein Westfalen/postalCode=57250, L=Netphen/street=Untere Industriestr, CN=Subdomain Issuer";
        String subject = "C=BE, O=Subdomain-Entity, CN=Subdomain Subject";

        customAuthentication.blueCoatAuthentication("123456789", issuer, subject, true);

        String smpId = "SMP-Delta";
        ServiceMetadataPublisherServiceType smpToBeCreated = new ServiceMetadataPublisherServiceType();
        smpToBeCreated.setServiceMetadataPublisherID(smpId);

        DnsMessageSenderServiceMock dnsMessageSenderService = (DnsMessageSenderServiceMock) this.dnsMessageSenderService;
        assertTrue(dnsMessageSenderService.getMessages().isEmpty());

        ServiceMetadataPublisherServiceType smp = createSimpleSMP(smpId);
        testInstance.create(smp);

        String messages = dnsMessageSenderService.getMessages();
        assertTrue(messages.contains("SMP-Delta.publisher.delta.acc.edelivery.tech.ec.europa.eu.\t0\tANY\tANY"));
        assertTrue(messages.contains("SMP-Delta.publisher.delta.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tCNAME\tlogical."));

        // Then we check that the data have been created
        assertNotNull(testInstance.read(smpToBeCreated));
        assertNotNull(certificateDAO.findCertificateByCertificateId("CN=SMP_123456789,O=DG-DIGIT,C=BE:00000000000000000000000123456789"));
    }

    @Test
    void testCreateSMPWithPhysicalAddress() throws Exception {
        String serialId = "6caa15ffed56ad049dd2b9bc835d2e46";
        String issuer = "CN=SMP_357951852456,O=DG-DIGIT,C=BE";
        String subject = "CN=SMP_TEST_2,O=Subdomain-Entityaa123,C=BE";
        customAuthentication.blueCoatAuthentication(serialId, issuer, subject, false);

        String smpId = "SMP357951852456";
        ServiceMetadataPublisherServiceType smpToBeCreated = new ServiceMetadataPublisherServiceType();
        smpToBeCreated.setServiceMetadataPublisherID(smpId);

        DnsMessageSenderServiceMock dnsMessageSenderService = (DnsMessageSenderServiceMock) this.dnsMessageSenderService;
        assertTrue(dnsMessageSenderService.getMessages().isEmpty());

        ServiceMetadataPublisherServiceType smp = createSimpleSMP(smpId, "http://10.10.10.10", "10.10.10.10");
        testInstance.create(smp);

        String messages = dnsMessageSenderService.getMessages();
        assertTrue(messages.contains("SMP357951852456.publisher.delta.acc.edelivery.tech.ec.europa.eu.\t0\tANY\tANY"));
        assertTrue(messages.contains("SMP357951852456.publisher.delta.acc.edelivery.tech.ec.europa.eu.\t60\tIN\tA\t10.10.10.10"));

        // Then we check that the data have been created
        assertNotNull(testInstance.read(smpToBeCreated));
        assertNotNull(certificateDAO.findCertificateByCertificateId("CN=SMP_TEST_2,O=Subdomain-Entityaa123,C=BE:6caa15ffed56ad049dd2b9bc835d2e46"));
    }
}
