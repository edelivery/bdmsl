/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.security;

import eu.europa.ec.bdmsl.common.exception.TechnicalException;

import java.util.Date;

/**
 * @author Flavio SANTOS
 * @since 23/11/2016
 */
public interface ICertificateAuthentication {

    void blueCoatAuthentication(String serialId, String issuer, String subject, Boolean isNonRootCA) throws TechnicalException;

    void blueCoatAuthentication(String serialId) throws TechnicalException;

    void blueCoatAuthentication() throws TechnicalException;

    void x509Authentication(String issuer, String subject, Date validFrom, Date validTo) throws Exception;

    void unsecuredAuthentication() throws Exception;

    void adminAuthentication() throws Exception;

    void adminTransientAuthentication() throws Exception;
}
