/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */

/**
 * @author Flavio SANTOS
 */
package eu.europa.ec.bdmsl.security;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.enums.SMLRoleEnum;
import eu.europa.ec.bdmsl.common.exception.CertificateAuthenticationException;
import eu.europa.ec.bdmsl.test.CommonTestUtils;
import eu.europa.ec.edelivery.security.PreAuthenticatedTokenPrincipal;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.util.ReflectionTestUtils;

import jakarta.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.security.Security;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SMLAuthenticationProviderLegacyTest extends AbstractJUnit5Test {

    @BeforeAll
    static void beforeClass() {
        Security.insertProviderAt(new org.bouncycastle.jce.provider.BouncyCastleProvider(), 1);
    }

    @BeforeEach
    protected void init() {
        Mockito.doReturn(true).when(configurationBusiness).isLegacyDomainAuthorizationEnabled();
    }

    @Test
    void testBlueCoatAuthenticateOk() throws Exception {
        //GIVEN
        String issuer = "CN=SMP_123456789,O=DG-DIGIT,C=BE";
        String subject = "CN=SMP_123456789,O=DG-DIGIT,C=BE";
        Authentication authentication = createClientCertPrincipal(issuer, subject);

        //WHEN
        Authentication result = smlAuthenticationProvider.authenticate(authentication);

        //THEN
        assertNotNull(result);
        assertTrue(result.isAuthenticated());
        assertEquals("ROLE_SMP", result.getAuthorities().iterator().next().toString());
    }

    @Test
    void testBlueCoatAuthenticateOkGlobalRegularExpression() throws Exception {
        String issuer = "CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA - G2,OU=FOR TEST ONLY,O=OpenPEPPOL AISBL,C=BE";
        //property must be set: 'authorization.smp.certSubjectRegex', '^.*(CN=SMP_|OU=PEPPOL PRODUCTION SMP).*$'
        String subject = "CN=TEST,OU=PEPPOL PRODUCTION SMP,O=My Company,C=EU";
        Authentication authentication = createClientCertPrincipal(issuer, subject);

        //WHEN
        Authentication result = smlAuthenticationProvider.authenticate(authentication);

        //THEN
        assertNotNull(result);
        assertTrue(result.isAuthenticated());
        assertEquals("ROLE_SMP", result.getAuthorities().iterator().next().toString());
    }

    @Test
    void testBlueCoatAuthenticateOkDomainRegularExpression() throws Exception {
        String issuer = "CN=ISSUER TEST WITH DOMAIN REGEXP,OU=FOR TEST ONLY,O=DIGIT,C=BE";
        //Domain must set:, '^.*(CN=SMP_|OU=Domain SMP).*$'
        String subject = "CN=TEST,OU=Domain SMP,O=My Company,C=EU";
        Authentication authentication = createClientCertPrincipal(issuer, subject);

        //WHEN
        Authentication result = smlAuthenticationProvider.authenticate(authentication);

        //THEN
        assertNotNull(result);
        assertTrue(result.isAuthenticated());
        assertEquals("ROLE_SMP", result.getAuthorities().iterator().next().toString());
    }

    @Test
    void testBlueCoatAuthenticateInvalidDomainRegularExpression() throws Exception {
        String issuer = "CN=ISSUER TEST WITH DOMAIN REGEXP,OU=FOR TEST ONLY,O=DIGIT,C=BE";
        //property must be set: 'authorization.smp.certSubjectRegex', '^.*(CN=SMP_|OU=Domain SMP).*$'
        String subject = "CN=SMP_TEST,OU=Domain bad regexp SMP,O=My Company,C=EU";
        Authentication authentication = createClientCertPrincipal(issuer, subject);

        //WHEN
        assertThrows(AuthenticationServiceException.class, () -> smlAuthenticationProvider.authenticate(authentication));
    }

    @Test
    void testBlueCoatAuthenticateByIssuerOK() throws Exception {
        //GIVEN
        String issuer = "CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA - G2,OU=FOR TEST ONLY,O=OpenPEPPOL AISBL,C=BE";
        String subject = "CN=SMP_TEST,O=My Company,C=EU";
        Authentication authentication = createClientCertPrincipal(issuer, subject);
        //WHEN
        Authentication result = smlAuthenticationProvider.authenticate(authentication);
        //THEN
        assertNotNull(result);
        assertTrue(result.isAuthenticated());
        assertEquals("ROLE_SMP", result.getAuthorities().iterator().next().toString());
    }

    @Test
    void testBlueCoatAuthenticateInvalidDN() throws Exception {
        //GIVEN
        String issuer = "CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA - G2,OU=FOR TEST ONLY,O=OpenPEPPOL AISBL,C=BE";
        String subject = "CN=SMP_TEST,O=My Company";
        Authentication authentication = createClientCertPrincipal(issuer, subject);

        //WHEN
        AuthenticationException exception = assertThrows(AuthenticationException.class,
                () -> smlAuthenticationProvider.authenticate(authentication));

        assertEquals("Certificate Subject DN [CN=SMP_TEST,O=My Company] for required values: [CN,O,C].", exception.getMessage());
    }

    @Test
    void testBlueCoatAuthenticateInvalidDN1() throws Exception {
        //GIVEN
        String issuer = "CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA - G2,OU=FOR TEST ONLY,O=OpenPEPPOL AISBL,C=BE";
        String subject = "CN=SMP_123456789,C=EU";
        Authentication authentication = createClientCertPrincipal(issuer, subject);

        //WHEN
        AuthenticationException exception = assertThrows(AuthenticationException.class,
                () -> smlAuthenticationProvider.authenticate(authentication));

        assertEquals("Certificate Subject DN [CN=SMP_123456789,C=EU] for required values: [CN,O,C].", exception.getMessage());
    }

    @Test
    void testBlueCoatAuthenticateSubjectNotOk() throws Exception {
        //GIVEN
        String issuer = "CN=SMP_123456789temp,O=DG-DIGIT,C=BE";
        String subject = "CN=SMP_123456789temp,O=DG-DIGIT,C=BE";
        Authentication authentication = createClientCertPrincipal(issuer, subject);

        //WHEN
        assertThrows(AuthenticationServiceException.class, () -> smlAuthenticationProvider.authenticate(authentication));
    }

    @Test
    void testX509AuthenticateOk() throws Exception {
        //GIVEN
        String issuer = "CN=SMP_357951852456,O=DG-DIGIT,C=BE";
        String subject = "C=BE, O=Subdomain-Entityaa123, CN=SMP_TEST_1";
        Authentication authentication = createX509AuthenticateKeystoreTrusted(issuer, subject, false);

        //WHEN
        Authentication result = smlAuthenticationProvider.authenticate(authentication);

        //THEN
        assertNotNull(result);
        assertTrue(result.isAuthenticated());
        assertEquals("ROLE_SMP", result.getAuthorities().iterator().next().toString());
    }

    @Test
    void testX509AuthenticateByIssuerOk() throws Exception {
        //GIVEN
        String issuer = "CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA - G2,OU=FOR TEST ONLY,O=OpenPEPPOL AISBL,C=BE";
        String subject = "CN=SMP_TEST,O=My Company,C=EU";
        Authentication authentication = createX509AuthenticateKeystoreTrusted(issuer, subject, true);

        //WHEN
        Authentication result = smlAuthenticationProvider.authenticate(authentication);

        //THEN
        assertNotNull(result);
        assertTrue(result.isAuthenticated());
        assertEquals("ROLE_SMP", result.getAuthorities().iterator().next().toString());
    }

    @Test
    void testX509AuthenticateInvalidDN() throws Exception {
        //GIVEN
        String issuer = "CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA - G2,OU=FOR TEST ONLY,O=OpenPEPPOL AISBL,C=BE";
        String subject = "CN=SMP_TEST,O=My Company";
        Authentication authentication = createX509AuthenticateNotTrusted(issuer, subject);

        //WHEN
        AuthenticationException exception = assertThrows(AuthenticationException.class,
                () -> smlAuthenticationProvider.authenticate(authentication));
        // THEN
        assertEquals("Certificate Subject DN [CN=SMP_TEST,O=My Company] for required values: [CN,O,C].", exception.getMessage());
    }

    @Test
    void testX509AuthenticateInvalidD2() throws Exception {
        //GIVEN
        String issuer = "CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA - G2,OU=FOR TEST ONLY,O=OpenPEPPOL AISBL,C=BE";
        String subject = "CN=SMP_123456789,C=EU";
        Authentication authentication = createX509AuthenticateNotTrusted(issuer, subject);

        //WHEN
        AuthenticationException exception = assertThrows(AuthenticationException.class,
                () -> smlAuthenticationProvider.authenticate(authentication));

        assertEquals("Certificate Subject DN [CN=SMP_123456789,C=EU] for required values: [CN,O,C].", exception.getMessage());
    }

    @Test
    void testGetIssuerBasedAuthorizationRolesNullRegExpWithSMP() throws Exception {
        //GIVEN
        String subject = "CN=SMP_123456789,O=DG-DIGIT,C=BE";
        String regexp = "";
        //WHEN
        List<GrantedAuthority> result = smlAuthenticationProvider.getIssuerBasedAuthorizationRoles(subject, regexp);

        //THEN
        assertEquals(result.size(), 1);
        assertEquals(result.get(0), SMLRoleEnum.ROLE_SMP.getAuthority());
    }

    @Test
    void testGetIssuerBasedAuthorizationRolesFailMissingCN() throws Exception {
        //GIVEN
        String subject = "O=DG-DIGIT,C=BE";
        String regexp = "";
        //WHEN
        assertThrows(CertificateAuthenticationException.class, () ->
                //THEN
                smlAuthenticationProvider.getIssuerBasedAuthorizationRoles(subject, regexp));
    }

    @Test
    void testGetIssuerBasedAuthorizationRolesInvalidSubject() throws Exception {
        //GIVEN
        String subject = "CN=test, O=DG-DIGIT,C=BE";
        String regexp = "NotMatch";
        //WHEN
        List<GrantedAuthority> result = smlAuthenticationProvider.getIssuerBasedAuthorizationRoles(subject, regexp);

        //THEN
        assertTrue(result.isEmpty());
    }

    @Test
    void testGetIssuerBasedAuthorizationRolesInvalidSubject2() throws Exception {
        //GIVEN
        String subject = "CN=SMP_test, O=DG-DIGIT,C=BE";
        String regexp = "NotMatch";
        //WHEN
        List<GrantedAuthority> result = smlAuthenticationProvider.getIssuerBasedAuthorizationRoles(subject, regexp);

        //THEN
        assertTrue(result.isEmpty());
    }

    @Test
    void testGetIssuerBasedAuthorizationRolesInvalidSubject3() throws Exception {
        //GIVEN
        String subject = "CN=SMP_test, O=DG-DIGIT,C=BE";
        String regexp = ".*O=DG-DIGIT.*";
        //WHEN
        List<GrantedAuthority> result = smlAuthenticationProvider.getIssuerBasedAuthorizationRoles(subject, regexp);

        //THEN
        assertEquals(result.size(), 1);
        assertEquals(result.get(0), SMLRoleEnum.ROLE_SMP.getAuthority());
    }

    @Test
    void testMonitorAuthenticateHeaderAdminPwd() throws Exception {
        //GIVEN
        HttpServletRequest mockedRequest = Mockito.mock(HttpServletRequest.class);
        Mockito.when(mockedRequest.getHeader("Admin-Pwd")).thenReturn(CommonTestUtils.ADMIN_PLAINTEXT_PASSWORD);

        Principal principal = eDeliveryTokenAuthenticationFilter.buildDetails(mockedRequest);
        Authentication authentication = new SecurityTokenAuthentication((PreAuthenticatedTokenPrincipal) principal, Collections.emptyList());

        //WHEN
        Authentication result = smlAuthenticationProvider.authenticate(authentication);

        //THEN
        assertNotNull(result);
        assertTrue(result.isAuthenticated());
        assertEquals("ROLE_MONITOR", ((List<SimpleGrantedAuthority>) result.getAuthorities()).get(0).toString());
    }

    @Test
    void testMonitorAuthenticateHeaderMonitorToken() throws Exception {
        //GIVEN
        HttpServletRequest mockedRequest = Mockito.mock(HttpServletRequest.class);
        Mockito.when(mockedRequest.getHeader("Monitor-Token")).thenReturn(CommonTestUtils.ADMIN_PLAINTEXT_PASSWORD);

        Principal principal = eDeliveryTokenAuthenticationFilter.buildDetails(mockedRequest);
        Authentication authentication = new SecurityTokenAuthentication((PreAuthenticatedTokenPrincipal) principal, Collections.emptyList());
        //WHEN
        Authentication result = smlAuthenticationProvider.authenticate(authentication);

        //THEN
        assertNotNull(result);
        assertTrue(result.isAuthenticated());
        assertEquals("ROLE_MONITOR", ((List<SimpleGrantedAuthority>) result.getAuthorities()).get(0).toString());
    }

    @Test
    void testMonitorAuthenticateWithWrongPassword() throws Exception {
        //GIVEN
        HttpServletRequest mockedRequest = Mockito.mock(HttpServletRequest.class);
        Mockito.when(mockedRequest.getHeader("Monitor-Token")).thenReturn("WrongPassword");
        Principal principal = eDeliveryTokenAuthenticationFilter.buildDetails(mockedRequest);
        Authentication authentication = new SecurityTokenAuthentication((PreAuthenticatedTokenPrincipal) principal, Collections.emptyList());
        //WHEN
        Authentication result = smlAuthenticationProvider.authenticate(authentication);

        //THEN
        assertNotNull(result);
        assertFalse(result.isAuthenticated());
    }

    @Test
    void testAdminAuthenticateWithWrongHash() throws Exception {

        //GIVEN
        ReflectionTestUtils.setField(smlAuthenticationProvider, "configurationBusiness", configurationBusiness);
        Mockito.doReturn("MH54HIBHH64AO6RMRENPOCMREYEP64QGYUSYDLFOTYDLWMTFKHFA").when(configurationBusiness).getMonitorToken();

        //GIVEN
        HttpServletRequest mockedRequest = Mockito.mock(HttpServletRequest.class);
        Mockito.when(mockedRequest.getHeader("Monitor-Token")).thenReturn("WrongPassword");
        Principal principal = eDeliveryTokenAuthenticationFilter.buildDetails(mockedRequest);
        Authentication authentication = new SecurityTokenAuthentication((PreAuthenticatedTokenPrincipal) principal, Collections.emptyList());
        //WHEN
        assertThrows(AuthenticationServiceException.class, () -> smlAuthenticationProvider.authenticate(authentication));
    }

    @Test
    void testUnsecureAuthenticate() throws Exception {
        //GIVEN
        UnsecureAuthentication authentication = new UnsecureAuthentication();
        ReflectionTestUtils.setField(smlAuthenticationProvider, "configurationBusiness", configurationBusiness);
        Mockito.doReturn(true).when(configurationBusiness).isUnsecureLoginEnabled();
        //WHEN
        Authentication result = smlAuthenticationProvider.authenticate(authentication);

        //THEN
        assertNotNull(result);
        assertTrue(result.isAuthenticated());
        assertEquals("ROLE_SMP", result.getAuthorities().iterator().next().toString());
    }


    @Test
    void testUnsecureAuthenticateFalse() {
        //GIVEN
        UnsecureAuthentication authentication = new UnsecureAuthentication();
        ReflectionTestUtils.setField(smlAuthenticationProvider, "configurationBusiness", configurationBusiness);
        Mockito.doReturn(false).when(configurationBusiness).isUnsecureLoginEnabled();
        //WHEN
        Authentication result = smlAuthenticationProvider.authenticate(authentication);
        assertNotNull(result);
        assertFalse(result.isAuthenticated());
    }


    @Test
    void testX509AuthenticateNotTrustedOk() throws Exception {
        //GIVEN
        String issuer = "CN=SMP_35795185245678998,O=DG-DIGIT,C=BE";
        String subject = "C=BE, O=Subdomain-Entityaa123156565, CN=SMP_TEST_123,O=DG-DIGIT,C=BE";
        Authentication authentication = createX509AuthenticateNotTrusted(issuer, subject);

        //WHEN
        assertThrows(AuthenticationServiceException.class, () -> smlAuthenticationProvider.authenticate(authentication));
    }
}
