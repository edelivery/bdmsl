/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.entity;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * This test class validates the data model {@link eu.europa.ec.bdmsl.dao.entity.AllowedWildcardEntity)}  and its constraints
 *
 * @author Flavio SANTOS
 * @since 22/09/2016
 */
class AllowedWildSchemeConstraintsValidationTest extends AbstractJUnit5Test {

    @Test
    void persistIntoAllowedSchemesTableOk() throws SQLException {
        String sql = "INSERT INTO bdmsl_allowed_wildcard(scheme, fk_certificate_id, created_on, last_updated_on) values (?,?,?,?)";
        Timestamp createdOn = new Timestamp(Calendar.getInstance().getTimeInMillis());
        Timestamp updatedOn = new Timestamp(Calendar.getInstance().getTimeInMillis());

        assertDoesNotThrow(() -> persist(sql, "iso6523-actorid-upis", 1, createdOn, updatedOn));
    }

    @Test
    void persistIntoAllowedSchemesTableFKNotOk() {
        String sql = "INSERT INTO bdmsl_allowed_wildcard(scheme, fk_certificate_id, created_on, last_updated_on) values (?,?,?,?)";
        Timestamp createdOn = new Timestamp(Calendar.getInstance().getTimeInMillis());
        Timestamp updatedOn = new Timestamp(Calendar.getInstance().getTimeInMillis());

        assertThrows(SQLException.class,
                () -> persist(sql, "*-actorid-upis", Integer.MAX_VALUE, createdOn, updatedOn));
    }

    @Test
    void persistIntoAllowedSchemesTablePKNotOk() throws SQLException {
        String sql = "INSERT INTO bdmsl_allowed_wildcard(scheme, fk_certificate_id, created_on, last_updated_on) values (?,?,?,?)";
        Timestamp createdOn = new Timestamp(Calendar.getInstance().getTimeInMillis());
        Timestamp updatedOn = new Timestamp(Calendar.getInstance().getTimeInMillis());

        persist(sql, "*-actorid-upis", 1, createdOn, updatedOn);
        assertThrows(SQLException.class,
                () -> persist(sql, "*-actorid-upis", 1, createdOn, updatedOn));
    }

    private void persist(String sql, String scheme, int certificateFK, Timestamp createdOn, Timestamp updatedOn) throws SQLException {
        try (Connection connection = dataSource.getConnection();
                PreparedStatement pstmt = connection.prepareStatement(sql)) {
            pstmt.setString(1, scheme);
            pstmt.setInt(2, certificateFK);
            pstmt.setTimestamp(3, createdOn);
            pstmt.setTimestamp(4, updatedOn);
            pstmt.executeUpdate();
            connection.commit();
        }
    }
}
