package eu.europa.ec.bdmsl.service.validation;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.business.impl.ConfigurationBusinessImpl;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @since 5.0
 * @author Sebastian-Ion TINCU
 */
public class ValidatorTest extends AbstractJUnit5Test {

    @Autowired
    private ConfigurationBusinessImpl configurationBusiness;

    @Test
    public void testNonEmptyValidatorComesFirstInListOfValidators() throws Exception {
        List<SMLPropertyValidator> propertyValidators = (List<SMLPropertyValidator>) FieldUtils.readField(configurationBusiness, "propertyValidators", true);

        Assertions.assertTrue(propertyValidators.size() > 1);
        Assertions.assertEquals(NonEmptySMLPropertyValidator.class, propertyValidators.get(0).getClass());
    }
}
