/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.security;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import eu.europa.ec.bdmsl.common.exception.BadConfigurationException;
import eu.europa.ec.bdmsl.common.exception.KeyException;
import eu.europa.ec.bdmsl.common.util.KeyUtil;
import eu.europa.ec.bdmsl.util.CertificateUtils;
import org.apache.wss4j.dom.handler.WSHandlerConstants;
import org.apache.xml.security.signature.XMLSignature;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import javax.xml.crypto.dsig.DigestMethod;
import java.io.File;
import java.security.Key;
import java.security.KeyStore;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;


/**
 * @author Flavio SANTOS
 * @author Sebastian-Ion TINCU
 */
class SignResponseInterceptorTest extends AbstractJUnit5Test {

    static final String KEYSTORE_FILENAME = "keystore.jks";

    static final String KEYSTORE_PASSWORD = "test";

    static final String KEYSTORE_TYPE = "JKS";

    static final String MASTER_KEY_FILENAME = "masterKey.key";


    private SignResponseInterceptor signResponseInterceptor;

    @Mock
    private PublicKey signingKey;

    @BeforeEach
    protected void testSetup() throws Exception {
        signResponseInterceptor = new SignResponseInterceptor(loggingService, configurationBusiness);
    }

    @Test
    void testInitSignResponseFalse() throws Exception {
        Mockito.doReturn("123456").when(configurationBusiness).getKeystorePassword();
        Mockito.doReturn(false).when(configurationBusiness).isSignResponseEnabled();

        Map<String, Object> res = signResponseInterceptor.initializeSignatureProperties();

        assertNull(res);
    }

    @Test
    void testInitSignResponseTrue() throws Exception {
        // GIVEN real values from test folder
        String alias = "receiveralias";
        String keyPath = resourceDirectory.toAbsolutePath() + File.separator + MASTER_KEY_FILENAME;
        String encPasswd = KeyUtil.encrypt(keyPath, KEYSTORE_PASSWORD);
        givenKeystoreDetails(alias, encPasswd, KEYSTORE_TYPE, MASTER_KEY_FILENAME);
        Mockito.doReturn(true).when(configurationBusiness).isSignResponseEnabled();

        // WHEN
        Map<String, Object> res = signResponseInterceptor.initializeSignatureProperties();

        // THEN
        assertTrue(res.containsKey(WSHandlerConstants.ACTION));
        assertTrue(res.containsKey(WSHandlerConstants.SIGNATURE_USER));
        assertTrue(res.containsKey(WSHandlerConstants.SIGNATURE_PARTS));
        assertTrue(res.containsKey(WSHandlerConstants.PW_CALLBACK_REF));
        assertTrue(res.containsKey(WSHandlerConstants.SIG_PROP_REF_ID));
        assertTrue(res.containsKey(WSHandlerConstants.MUST_UNDERSTAND));
        assertTrue(res.containsKey(WSHandlerConstants.SIG_ALGO));
        assertTrue(res.containsKey(WSHandlerConstants.SIG_DIGEST_ALGO));

        String sigPropKey = (String) res.get(WSHandlerConstants.SIG_PROP_REF_ID);
        assertTrue(res.containsKey(sigPropKey));
        // test values
        assertEquals(WSHandlerConstants.SIGNATURE, res.get(WSHandlerConstants.ACTION));
        assertEquals(alias, res.get(WSHandlerConstants.SIGNATURE_USER));
        assertEquals("{Element}{}Body", res.get(WSHandlerConstants.SIGNATURE_PARTS));
        assertEquals(SMLCallbackHandler.class, res.get(WSHandlerConstants.PW_CALLBACK_REF).getClass());
        assertEquals("http://www.w3.org/2001/04/xmldsig-more#rsa-sha256", res.get(WSHandlerConstants.SIG_ALGO));
        assertEquals("http://www.w3.org/2000/09/xmldsig#sha1", res.get(WSHandlerConstants.SIG_DIGEST_ALGO));
    }

    @Test
    void testInitSignReponseTrueButWrongPrivateKeyPassword() {
        Mockito.doReturn("123456").when(configurationBusiness).getKeystorePassword();
        Mockito.doReturn(true).when(configurationBusiness).isSignResponseEnabled();
        try {
            signResponseInterceptor.initializeSignatureProperties();
        } catch (Exception exc) {
            assertEquals("[ERR-105] Either private key or encrypted password might not be correct. Please check both.", exc.getMessage());
        }
    }

    @Test
    void getSignatureAlgorithmForKey() {
        // GIVEN
        String algorithm = "myAlgorithm";

        // WHEN
        String signatureAlgorithmForKey = signResponseInterceptor.getSignatureAlgorithmForKey(signingKey, algorithm);

        // THEN
        assertEquals(
                algorithm, signatureAlgorithmForKey, "Should have returned the provided signing algorithm when not blank");
    }

    @Test
    void getSignatureAlgorithmForKey_ec_real() throws Exception {
        // GIVEN
        X509Certificate certificate = getCertificate("smp_ecdsa_nist-b409");

        String signatureAlgorithmForKey = signResponseInterceptor.getSignatureAlgorithmForKey(certificate.getPublicKey(), null);

        // THEN
        assertEquals(
                XMLSignature.ALGO_ID_SIGNATURE_ECDSA_SHA256, signatureAlgorithmForKey, "Should have returned the 'http://www.w3.org/2001/04/xmldsig-more#ecdsa-sha256' signing algorithm for an EC signing key when the provided signing algorithm is blank");
    }

    @Test
    void getSignatureAlgorithmForKey_ed25519_real() throws Exception {
        // GIVEN
        X509Certificate ed25519 = getCertificate("smp_eddsa_25519");

        String signatureAlgorithmForKey = signResponseInterceptor.getSignatureAlgorithmForKey(ed25519.getPublicKey(), null);

        // THEN
        assertEquals(
                XMLSignature.ALGO_ID_SIGNATURE_EDDSA_ED25519, signatureAlgorithmForKey, "Should have returned the 'http://www.w3.org/2021/04/xmldsig-more#eddsa-ed25519' signing algorithm for an ED25519 signing key when the provided signing algorithm is blank");
    }

    @Test
    void getSignatureAlgorithmForKey_ed448_real() throws Exception {
        // GIVEN
        X509Certificate ed448 = getCertificate("smp_eddsa_448");

        String signatureAlgorithmForKey = signResponseInterceptor.getSignatureAlgorithmForKey(ed448.getPublicKey(), null);

        // THEN
        assertEquals(
                XMLSignature.ALGO_ID_SIGNATURE_EDDSA_ED448, signatureAlgorithmForKey, "Should have returned the 'http://www.w3.org/2021/04/xmldsig-more#eddsa-ed448' signing algorithm for an ED448 signing key when the provided signing algorithm is blank");
    }

    @Test
    void getSignatureAlgorithmForKey_rsa_real() throws Exception {
        // GIVEN
        X509Certificate rsa = getCertificate("receiveralias");

        String signatureAlgorithmForKey = signResponseInterceptor.getSignatureAlgorithmForKey(rsa.getPublicKey(), null);

        // THEN
        assertEquals(
                XMLSignature.ALGO_ID_SIGNATURE_RSA_SHA256, signatureAlgorithmForKey, "Should have returned the 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha256' signing algorithm for a RSA signing key when the provided signing algorithm is blank");
    }

    @Test
    void getSignatureDigestAlgorithmForKey() {
        // GIVEN
        String algorithm = "myAlgorithm";

        // WHEN
        String signatureAlgorithmForKey = signResponseInterceptor.getSignatureDigestAlgorithmForKey(signingKey, algorithm);

        // THEN
        assertEquals(
                algorithm, signatureAlgorithmForKey, "Should have returned the provided digest signing algorithm when not blank");
    }

    @Test
    void getSignatureDigestAlgorithmForKey_default() {
        // GIVEN
        String keyAlgorithm = "unknownAlgorithm";
        Mockito.when(signingKey.getAlgorithm()).thenReturn(keyAlgorithm);

        // WHEN
        String signatureAlgorithmForKey = signResponseInterceptor.getSignatureDigestAlgorithmForKey(signingKey, "  ");

        // THEN
        assertEquals(
                DigestMethod.SHA256, signatureAlgorithmForKey, "Should have returned the default 'http://www.w3.org/2001/04/xmlenc#sha256' digest signing algorithm for an unknown signing key when the provided digest signing algorithm is blank");
    }

    @Test
    void getSignatureDigestAlgorithmForKey_ec_real() throws Exception {
        // GIVEN
        Key ec = getPrivateKey("smp_ecdsa_nist-b409");

        String signatureAlgorithmForKey = signResponseInterceptor.getSignatureDigestAlgorithmForKey(ec, null);

        // THEN
        assertEquals(
                DigestMethod.SHA256, signatureAlgorithmForKey, "Should have returned the default 'http://www.w3.org/2001/04/xmlenc#sha256' digest signing algorithm for an EC signing key when the provided digest signing algorithm is blank");
    }

    @Test
    void getSignatureDigestAlgorithmForKey_ed25519_real() throws Exception {
        // GIVEN
        Key ed25519 = getPrivateKey("smp_eddsa_25519");

        String signatureAlgorithmForKey = signResponseInterceptor.getSignatureDigestAlgorithmForKey(ed25519, null);

        // THEN
        assertEquals(
                DigestMethod.SHA256, signatureAlgorithmForKey, "Should have returned the default 'http://www.w3.org/2001/04/xmlenc#sha256' digest signing algorithm for an ED25519 signing key when the provided digest signing algorithm is blank");
    }

    @Test
    void getSignatureDigestAlgorithmForKey_ed448_real() throws Exception {
        // GIVEN
        Key ed448 = getPrivateKey("smp_eddsa_448");

        String signatureAlgorithmForKey = signResponseInterceptor.getSignatureDigestAlgorithmForKey(ed448, null);

        // THEN
        assertEquals(
                DigestMethod.SHA256, signatureAlgorithmForKey, "Should have returned the default 'http://www.w3.org/2001/04/xmlenc#sha256' digest signing algorithm for an ED448 signing key when the provided digest signing algorithm is blank");
    }

    @Test
    void getSignatureDigestAlgorithmForKey_rsa_real() throws Exception {
        // GIVEN
        Key rsa = getPrivateKey("receiveralias");

        String signatureAlgorithmForKey = signResponseInterceptor.getSignatureDigestAlgorithmForKey(rsa, null);

        // THEN
        assertEquals(
                DigestMethod.SHA256, signatureAlgorithmForKey, "Should have returned the default 'http://www.w3.org/2001/04/xmlenc#sha256' digest signing algorithm for a RSA signing key when the provided digest signing algorithm is blank");
    }

    @Test
    void testDecryptPassword() throws Exception {
        // GIVEN
        givenEncryptionDetails("d2Hun4dq5VPXv6uQp/PFvA==", MASTER_KEY_FILENAME);

        // WHEN
        String password = signResponseInterceptor.retrievePassword();

        // THEN
        assertEquals("EDELIVERY", password);
    }

    @Test
    void testInitInvalidPasswordKey() {
        Mockito.doReturn("invalidKey").when(configurationBusiness).getKeystorePassword();
        assertThrows(KeyException.class,
                () -> signResponseInterceptor.initializeSignatureProperties(),
                "[ERR-105] Either private key or encrypted password might not be correct. Please check both.");
    }

    @Test
    void testInitInvalidKeystoreType() throws Exception {
        String invalidKeystoreType = "INVALID";
        String alias = "receiveralias";
        String keyPath = resourceDirectory.toAbsolutePath() + File.separator + MASTER_KEY_FILENAME;
        String encPasswd = KeyUtil.encrypt(keyPath, KEYSTORE_PASSWORD);
        givenKeystoreDetails(alias, encPasswd, invalidKeystoreType, MASTER_KEY_FILENAME);
        assertThrows(BadConfigurationException.class,
                () -> signResponseInterceptor.initializeSignatureProperties(),
                "[ERR-109] Can not open keystore");
    }

    private void givenEncryptionDetails(String keystoreEncryptedPassword, String encryptionFileName) {
        Mockito.doReturn(keystoreEncryptedPassword).when(configurationBusiness).getKeystorePassword();
        Mockito.doReturn(encryptionFileName).when(configurationBusiness).getEncryptionFilename();
        Mockito.doReturn(targetDirectory.toAbsolutePath().toString()).when(configurationBusiness).getConfigurationFolder();
    }

    private void givenKeystoreDetails(String alias, String keystoreEncryptedPassword, String keystoreType, String encryptionFileName) {
        givenEncryptionDetails(keystoreEncryptedPassword, encryptionFileName);
        Mockito.doReturn(alias).when(configurationBusiness).getSignAlias();
        Mockito.doReturn(keystoreType).when(configurationBusiness).getKeystoreType();
        Mockito.doReturn(KEYSTORE_FILENAME).when(configurationBusiness).getKeystoreFilename();
    }

    private Key getPrivateKey(String alias) throws Exception {
        return CertificateUtils.getSigningKey(KEYSTORE_TYPE, new File(resourceDirectory.toAbsolutePath() + File.separator + KEYSTORE_FILENAME), KEYSTORE_PASSWORD, alias);
    }

    private X509Certificate getCertificate(String alias) throws Exception {
        KeyStore keyStore = CertificateUtils.getKeystore(KEYSTORE_TYPE, new File(resourceDirectory.toAbsolutePath() + File.separator + KEYSTORE_FILENAME), KEYSTORE_PASSWORD);
        return CertificateUtils.getCertificate(keyStore, alias);
    }
}
