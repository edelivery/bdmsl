/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2017 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.util;

import eu.europa.ec.bdmsl.AbstractJUnit5Test;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.web.context.WebApplicationContext;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletContextEvent;
/**
 * @author Flavio SANTOS
 */
class ConfigurationListenerTest extends AbstractJUnit5Test {

    @Autowired
    private ConfigurationListener configurationListener;

    @Test
    void contextInitializedTest() {
        // given
        final ServletContextEvent event = Mockito.mock(ServletContextEvent.class);
        final ServletContext context = Mockito.mock(ServletContext.class);
        final WebApplicationContext webApplicationContext = Mockito.mock(WebApplicationContext.class);
        final AutowireCapableBeanFactory autowireCapableBeanFactory = Mockito.mock(AutowireCapableBeanFactory.class);

        Mockito.when(event.getServletContext()).thenReturn(context);
        Mockito.when(context.getAttribute(ArgumentMatchers.anyString())).thenReturn(webApplicationContext);
        Mockito.when(webApplicationContext.getAutowireCapableBeanFactory()).thenReturn(autowireCapableBeanFactory);

        // when
        ServletContextEvent sce = new ServletContextEvent(context);
        configurationListener.contextInitialized(sce);

        // then
        Mockito.verify(autowireCapableBeanFactory).autowireBean(configurationListener);
    }
}
