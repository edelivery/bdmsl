/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.util;

import eu.europa.ec.bdmsl.common.exception.InvalidArgumentException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.*;

class DataValidatorTest {

    private static String[] loadValidEmails() {
        return new String[]{
                "email@domain.com",
                "email@subdomain.domain.com",
                "firstname.lastname@domain.com",
                "firstname+lastname@domain.com",
                "firstname-lastname@domain.com",
                "DIGIT-CEF-EDELIVERY-SMP-SML@ec.europa.eu",
                "1234567890@domain.com",
                "1234567890@DOMAIN.COM",
                "1234567890@[127.0.0.1]",
                "email@domain-one.com",
                "email@domain-one.local",
                "_______@domain.com",
                "---@example.com",
                "email@domain.name",
                "email@domain.co.jp",
                " abigail@example.com",
                "   abigail@example.com",
                "abigail@example.com ",
                "fred&barny@example.com",
                "あいうえお@domain.com", //non ascii chars are allowed since 2012
        };
    }

    private static String[] loadInvalidEmails() {
        return new String[]{
                " ",
                "plainaddress",
                "#@%^%#$@#$@#.com",
                "@domain.com",
                "Joe Smith <email@domain.com>",
                "email@domain@domain.com",
                ".email@domain.com",
                "email.@domain.com",
                "email..email@domain.com",
                "email@domain.com (Joe Smith)",
                "email@domain",
                "email@domain..com"};
    }

    @ParameterizedTest
    @MethodSource("loadValidEmails")
    void testEmailValid(String validMailAddress) {
        try {
            DataValidator.validateEmail(validMailAddress);
        } catch (InvalidArgumentException e) {
            fail("Email address [" + validMailAddress + "] is valid but was not accepted by the validator.");
        }
    }

    @ParameterizedTest
    @MethodSource("loadInvalidEmails")
    void testEmailInvalid(String invalidMailAddress) {

        TechnicalException result = assertThrows(InvalidArgumentException.class,
                () -> DataValidator.validateEmail(invalidMailAddress));
        assertEquals("[ERR-119] Email address [" + invalidMailAddress + "] is not valid.", result.getMessage());
    }
}
