/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package org.xbill.DNS;

import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class InconsistencyAXFRZoneTransferHandlerTest {

    InconsistencyAXFRZoneTransferHandler testInstance;

    IRDnsRangeData smpRecord = new IRDnsRangeData(1);
    IRDnsRangeData cnameRecord = new IRDnsRangeData(1);
    IRDnsRangeData naptrRecord = new IRDnsRangeData(1);

    private Logger logger;

    @BeforeEach
    void setUp() throws Exception {
        logger = Mockito.mock(Logger.class);
        testInstance = new InconsistencyAXFRZoneTransferHandler("zoneName", ".publisher.", smpRecord, cnameRecord, naptrRecord);
        ReflectionTestUtils.setField(testInstance, "LOG", logger);
    }

    @AfterEach
    void tearDown() {
        smpRecord.clear();
        cnameRecord.clear();
        naptrRecord.clear();
    }

    @Test
    void startAXFR() {
        testInstance.startAXFR();
        Mockito.verify(logger).info("Start transfer DNS zone:zoneName");
    }

    @Test
    void startIXFR() {
        IllegalStateException illegalStateException = assertThrows(IllegalStateException.class, () -> testInstance.startIXFR());
        assertThat(illegalStateException.getMessage(), containsString("InconsistencyAXFRZoneTransferHandler must be used only for AXFR"));
    }

    @Test
    void startIXFRDeletes() {
        IllegalStateException illegalStateException = assertThrows(IllegalStateException.class, () -> testInstance.startIXFRDeletes(null));
        assertThat(illegalStateException.getMessage(), containsString("InconsistencyAXFRZoneTransferHandler must be used only for AXFR"));

    }

    @Test
    void startIXFRAdds() {
        IllegalStateException illegalStateException = assertThrows(IllegalStateException.class, () -> testInstance.startIXFRAdds(null));
        assertThat(illegalStateException.getMessage(), containsString("InconsistencyAXFRZoneTransferHandler must be used only for AXFR"));
    }

    @Test
    void handleNaptrRecord() throws Exception {
        // given
        String[] naptrRecords = new String[]{"naptr-001.test.local.", "naptr-002.test.local.", "naptr-003.test.local."};
        List<Record> lst = getNAPTRRecords(naptrRecords);
        int iSMP = smpRecord.getAddedRowsDNS();
        int iCname = cnameRecord.getAddedRowsDNS();
        int iNaptr = naptrRecord.getAddedRowsDNS();
        //when
        lst.forEach(record -> {
            testInstance.handleRecord(record);
        });
        // then
        assertEquals(iSMP, smpRecord.getAddedRowsDNS());
        assertEquals(iCname, cnameRecord.getAddedRowsDNS());
        assertEquals(iNaptr + naptrRecords.length, naptrRecord.getAddedRowsDNS());

    }

    @Test
    void handleCnameRecord() throws Exception {
        // given
        String[] cnameRecords = new String[]{"b-cname-001.test.local.", "b-cname-002.test.local.", "b-cname-003.test.local."};
        List<Record> lst = getCNameRecords(cnameRecords);
        int iSMP = smpRecord.getAddedRowsDNS();
        int iCname = cnameRecord.getAddedRowsDNS();
        int iNaptr = naptrRecord.getAddedRowsDNS();
        //when
        lst.forEach(record -> {
            testInstance.handleRecord(record);
        });
        // then
        assertEquals(iSMP, smpRecord.getAddedRowsDNS());
        assertEquals(iCname + cnameRecords.length, cnameRecord.getAddedRowsDNS());
        assertEquals(iNaptr, naptrRecord.getAddedRowsDNS());
    }

    @Test
    void handleSMPRecord() throws Exception {
        // given
        String[] cnameRecords = new String[]{"smp-001.publisher.test.local.", "smp-002.publisher.test.local.", "smp-003.publisher.test.local."};
        List<Record> lst = getCNameRecords(cnameRecords);
        int iSMP = smpRecord.getAddedRowsDNS();
        int iCname = cnameRecord.getAddedRowsDNS();
        int iNaptr = naptrRecord.getAddedRowsDNS();
        //when
        lst.forEach(record -> {
            testInstance.handleRecord(record);
        });
        // then
        assertEquals(iSMP + cnameRecords.length, smpRecord.getAddedRowsDNS());
        assertEquals(iCname, cnameRecord.getAddedRowsDNS());
        assertEquals(iNaptr, naptrRecord.getAddedRowsDNS());

    }

    List<Record> getCNameRecords(String... name) throws Exception {
        List<Record> records = Arrays.asList(name).stream().map(dnsName -> {
            try {
                return new CNAMERecord(Name.fromString(dnsName), DClass.IN, 60, Name.fromString("smp.domain.com."));
            } catch (TextParseException e) {
                e.printStackTrace();
            }
            return null;
        }).collect(Collectors.toList());


        return records;
    }

    List<Record> getNAPTRRecords(String... name) throws Exception {
        List<Record> records = Arrays.asList(name).stream().map(dnsName -> {
            try {
                return new NAPTRRecord(Name.fromString(dnsName), DClass.IN, 60, 100, 10, "U", "Meta:SMP", "!^.*$!smp.domain.edelivery.com!", Name.fromString("."));

            } catch (TextParseException e) {
                e.printStackTrace();
            }
            return null;
        }).collect(Collectors.toList());


        return records;
    }

}
