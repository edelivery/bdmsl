/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package org.xbill.DNS;

import eu.europa.ec.bdmsl.service.dns.impl.Bind9PrivateKeyParser;
import eu.europa.ec.bdmsl.service.dns.impl.Sig0KeyTestData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertNotNull;


/**
 * @author Flavio SANTOS
 */

class DomiSmlSIG0ServiceTest extends Sig0KeyTestData {

    @ParameterizedTest
    @MethodSource("keyExamples")
    void testParseKeysType(String keyFileName, String keyAlgName, String keyAlgCode) throws Exception {
        Name sig0zoneName = new Name("test.domain.name.local.");

        try (InputStream publicKeyStream = getKeyAsInputStream(keyFileName + ".key");
             Reader reader = new InputStreamReader(publicKeyStream);
             InputStream privateKeyStream = getKeyAsInputStream(keyFileName + ".private")) {
            // given
            PrivateKey privateKey = Bind9PrivateKeyParser.toPrivateKey(privateKeyStream);
            KEYRecord keyRecord = (KEYRecord) fromString(reader);
            assertNotNull(keyRecord);
            assertNotNull(privateKey);
            // when
            Message message = createTestMessage(sig0zoneName);
            DomiSmlSIG0Service.signMessage(message, keyRecord, privateKey, null, 2);
            message = new Message(message.toWire());

            // then validate the message
            // AD flag is set
            assertThat(message.toString(), containsString("ad: 1"));
            // SIG record is added
            assertThat(message.toString(), containsString("0\tANY\tSIG\tTYPE0 " + keyAlgCode + " 0 0"));
            // verify signature
            SIG0.verifyMessage(message, message.toWire(), keyRecord, null);

        }
    }

    @Test
    void testSignMessage() throws Exception {
        Message message = new Message();
        KeyPair keyPair = createKeyPair();
        KEYRecord keyRecord = new KEYRecord(Name.root, DClass.ANY, 0, 0, 10, DNSSEC.Algorithm.RSAMD5, keyPair.getPublic());
        DomiSmlSIG0Service.signMessage(message, keyRecord, keyPair.getPrivate(), null, 2);
        message = new Message(message.toWire());

        Assertions.assertTrue(message.toString().contains("ad: 1"));
        Assertions.assertTrue(message.toString().contains("0\tANY\tSIG\tTYPE0 1 0 0"));
    }

    private KeyPair createKeyPair() throws Exception {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(1024);

        return keyGen.generateKeyPair();
    }
}
