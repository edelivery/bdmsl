/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package org.xbill.DNS;

import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.Executor;

/**
 * This is a mock class for SimpleResolver
 *
 * @author Joze RIHTARSIC
 * @since 4.1
 */
public class SimpleResolverMock implements Resolver {

    String localhost;
    boolean tcpFlag = false;
    int timeout = 0;
    Message msg = null;

    public SimpleResolverMock(String localhost) {
        this.localhost = localhost;
    }

    @Override
    public void setPort(int port) {
        // do nothing
    }

    @Override
    public void setTCP(boolean flag) {
        tcpFlag = flag;

    }

    @Override
    public void setIgnoreTruncation(boolean flag) {
        // do nothing
    }

    @Override
    public void setEDNS(int level) {
        // do nothing
    }

    @Override
    public void setEDNS(int level, int payloadSize, int flags, List options) {
        // do nothing
    }

    @Override
    public void setTSIGKey(TSIG key) {
        // do nothing
    }

    @Override
    public void setTimeout(Duration duration) {
        timeout = (int) duration.getSeconds();
    }

    @Override
    public void setEDNS(int version, int payloadSize, int flags, EDNSOption... options) {
        Resolver.super.setEDNS(version, payloadSize, flags, options);
    }

    @Override
    public CompletionStage<Message> sendAsync(Message query) {
        return Resolver.super.sendAsync(query);
    }

    @Override
    public CompletionStage<Message> sendAsync(Message query, Executor executor) {
        return Resolver.super.sendAsync(query, executor);
    }

    @Override
    public void setTimeout(int secs, int msecs) {
        this.timeout = secs;
    }

    @Override
    public void setTimeout(int secs) {
        this.timeout = secs;
    }

    @Override
    public Message send(Message query) throws IOException {
        return msg = query;
    }

    @Override
    public Object sendAsync(Message query, ResolverListener listener) {
        return null;
    }

    public String getLocalhost() {
        return localhost;
    }

    public boolean isTcpFlag() {
        return tcpFlag;
    }

    public Duration getTimeout() {
        return Duration.ofSeconds(timeout);
    }

    public Message getMessage() {
        return msg;
    }
}
