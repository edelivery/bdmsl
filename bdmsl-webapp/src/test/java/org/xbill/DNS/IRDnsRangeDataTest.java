/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package org.xbill.DNS;


import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;


class IRDnsRangeDataTest {
    static final String PREFIX = "test-prefix";
    private static int NUM_RANGES = 10;

    static {
        System.setProperty("java.io.tmpdir", "./target/");
    }

    static IRDnsRangeData testInstance = new IRDnsRangeData(NUM_RANGES);



    private String substring;

    @AfterAll
    static void clear() {
        testInstance.deleteFiles();
    }

    @Test
    void testConstructorDefault() throws IOException {

        assertEquals(NUM_RANGES, testInstance.getNumberOfRanges());
        assertEquals(Integer.MIN_VALUE, testInstance.getMinValue());
        assertEquals(Integer.MAX_VALUE, testInstance.getMaxValue());
        // 429496729 (MAX_VALUE -MIN_VALUE )/10
        assertEquals(429496729, testInstance.getIntervalSize());

        File temp = File.createTempFile("test", "dat");
        for (int i = 0; i < NUM_RANGES; i++) {
            File dbFile = testInstance.getDBFile(i);
            File dnsFile = testInstance.getDNSFile(i);
            assertTrue(dnsFile.getName().startsWith(String.format("inc-rep_%03d_DNS_", i)));
            assertTrue(dbFile.getName().startsWith(String.format("inc-rep_%03d_DB_", i)));

            assertEquals(temp.getParentFile(), dnsFile.getParentFile());
            assertEquals(temp.getParentFile(), dbFile.getParentFile());
        }
    }

    @Test
    void testConstructor2() throws IOException {

        // given - when
        IRDnsRangeData localtest = new IRDnsRangeData(-10, 10, 15, "my-test");

        // then
        assertEquals(15, localtest.getNumberOfRanges());
        assertEquals(-10, localtest.getMinValue());
        assertEquals(10, localtest.getMaxValue());
        // 429496729 (20 )/15
        assertEquals(1, localtest.getIntervalSize());

        File temp = File.createTempFile("test", "dat");
        for (int i = 0; i < 15; i++) {
            File dbFile = localtest.getDBFile(i);
            File dnsFile = localtest.getDNSFile(i);

            assertTrue(dnsFile.getName().startsWith(String.format("my-test_%03d_DNS_", i)));
            assertTrue(dbFile.getName().startsWith(String.format("my-test_%03d_DB_", i)));

            assertEquals(temp.getParentFile(), dnsFile.getParentFile());
            assertEquals(temp.getParentFile(), dbFile.getParentFile());
        }
        localtest.deleteFiles();
    }

    @Test
    void testConstructorFault() throws IOException {

        // more ranges that is possible
        assertThrows(IllegalArgumentException.class, () -> new IRDnsRangeData(0, 10, 15, "my-test"));
    }

    @Test
    void getRangeForInt() {

        // given - when
        IRDnsRangeData localtest = new IRDnsRangeData(-10, 10, 8, "my-test");

        // then
//        assertEquals(0, localtest.getRangeForInt(-10));
        assertEquals(0, localtest.getRangeForInt(-10));
        assertEquals(0, localtest.getRangeForInt(-9));
        assertEquals(1, localtest.getRangeForInt(-8));
        assertEquals(1, localtest.getRangeForInt(-7));
        assertEquals(2, localtest.getRangeForInt(-6));
        assertEquals(2, localtest.getRangeForInt(-5));
        assertEquals(3, localtest.getRangeForInt(-4));
        assertEquals(3, localtest.getRangeForInt(-3));
        assertEquals(4, localtest.getRangeForInt(-2));
        assertEquals(4, localtest.getRangeForInt(-1));
        assertEquals(5, localtest.getRangeForInt(-0));
        assertEquals(5, localtest.getRangeForInt(1));
        assertEquals(6, localtest.getRangeForInt(2));
        assertEquals(6, localtest.getRangeForInt(3));
        assertEquals(7, localtest.getRangeForInt(4));
        assertEquals(7, localtest.getRangeForInt(5));
        assertEquals(7, localtest.getRangeForInt(6));
        // the remaining   (max-min)%numOfRanges is added to last class
        assertEquals(7, localtest.getRangeForInt(7));
        assertEquals(7, localtest.getRangeForInt(8));
        assertEquals(7, localtest.getRangeForInt(9));
        assertEquals(7, localtest.getRangeForInt(10));


        localtest.deleteFiles();
    }

    @Test
    void closeFiles() throws IOException {
        // given
        IRDnsRangeData localtest = new IRDnsRangeData(-10, 10, 15, "my-test");
        for (int i = 0; i < 15; i++) {
            File dbFile = localtest.getDBFile(i);
            File dnsFile = localtest.getDNSFile(i);
            assertTrue(dnsFile.exists());
            assertTrue(dbFile.exists());
        }
        // when
        localtest.deleteFiles();
        // then
        for (int i = 0; i < 15; i++) {
            File dbFile = localtest.getDBFile(i);
            File dnsFile = localtest.getDNSFile(i);

            assertFalse(dnsFile.exists());
            assertFalse(dbFile.exists());
        }

    }

    @Test
    void testCompare() {
        // given
        Set<String> missingInDNS = new HashSet<>();
        Set<String> missingInDB = new HashSet<>();
        IRDnsRangeData localtest = new IRDnsRangeData(-10, 10, 15, "my-test");
        localtest = Mockito.spy(localtest);
        Mockito.doNothing().when(localtest).compareFiles(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any());
        // when
        localtest.compare(missingInDNS, missingInDB);
        // then
        Mockito.verify(localtest, Mockito.times(localtest.getNumberOfRanges())).compareFiles(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any());

        localtest.deleteFiles();
    }

    @Test
    void testCompareFilesAllOk() throws IOException {
        // given
        Set<String> missingInDNS = new HashSet<>();
        Set<String> missingInDB = new HashSet<>();
        File fDns = File.createTempFile("dns_", ".dat");
        File fDb = File.createTempFile("db_", ".dat");
        writeLinesToFile(fDns, "test1|test", "test2|test", "test3|test");
        writeLinesToFile(fDb, "test1|test#1", "test2|test#2", "test3|test#3");

        IRDnsRangeData localtest = new IRDnsRangeData(-10, 10, 15, "my-test");

        // when
        localtest.compareFiles(fDns, fDb, missingInDNS, missingInDB);
        // then
        assertEquals(0, missingInDNS.size());
        assertEquals(0, missingInDB.size());

        localtest.deleteFiles();
        fDns.delete();
        fDb.delete();
    }

    @Test
    void testCompareFilesAllMissingInDb() throws IOException {
        // given
        Set<String> missingInDNS = new HashSet<>();
        Set<String> missingInDB = new HashSet<>();
        File fDns = File.createTempFile("dns_", ".dat");
        File fDb = File.createTempFile("db_", ".dat");
        writeLinesToFile(fDns, "test1|test", "test2|test", "test3|test");
        writeLinesToFile(fDb, "test1|test#1", "test2|test#2");

        IRDnsRangeData localtest = new IRDnsRangeData(-10, 10, 15, "my-test");
        // when
        localtest.compareFiles(fDns, fDb, missingInDNS, missingInDB);
        // then
        assertEquals(0, missingInDNS.size());
        assertEquals(1, missingInDB.size());
        assertEquals("test3|test", missingInDB.toArray()[0]);

        localtest.deleteFiles();
        fDns.delete();
        fDb.delete();
    }

    @Test
    void testCompareFilesAllMissingInDns() throws IOException {
        // given
        Set<String> missingInDNS = new HashSet<>();
        Set<String> missingInDB = new HashSet<>();
        File fDns = File.createTempFile("dns_", ".dat");
        File fDb = File.createTempFile("db_", ".dat");
        writeLinesToFile(fDns, "test1|test", "test2|test");
        writeLinesToFile(fDb, "test1|test#1", "test2|test#2", "test3|test#3");

        IRDnsRangeData localtest = new IRDnsRangeData(-10, 10, 15, "my-test");
        // when
        localtest.compareFiles(fDns, fDb, missingInDNS, missingInDB);
        // then
        assertEquals(1, missingInDNS.size());
        assertEquals(0, missingInDB.size());
        assertEquals("test3|test#3", missingInDNS.toArray()[0]);

        localtest.deleteFiles();
        fDns.delete();
        fDb.delete();
    }

    @Test
    void deleteFiles() throws IOException {
        // given
        IRDnsRangeData localtest = new IRDnsRangeData(-10, 10, 15, "my-test");
        for (int i = 0; i < 15; i++) {
            File dbFile = localtest.getDBFile(i);
            File dnsFile = localtest.getDNSFile(i);

            assertTrue(dnsFile.exists());
            assertTrue(dbFile.exists());
        }
        // when
        localtest.deleteFiles();
        // then
        for (int i = 0; i < 15; i++) {
            File dbFile = localtest.getDBFile(i);
            File dnsFile = localtest.getDNSFile(i);

            assertFalse(dnsFile.exists());
            assertFalse(dbFile.exists());
        }

    }

    @Test
    void addDNSEntry() {
        // given
        int iValDB = testInstance.getAddedRowsDB();
        int iValDNS = testInstance.getAddedRowsDNS();

        // when
        testInstance.addDBEntry("test", "identifier");

        // then
        assertEquals(iValDB + 1, testInstance.getAddedRowsDB());
        assertEquals(iValDNS, testInstance.getAddedRowsDNS());

    }

    @Test
    void addDBEntry() {
        // given
        int iValDB = testInstance.getAddedRowsDB();
        int iValDNS = testInstance.getAddedRowsDNS();

        // when
        testInstance.addDNSEntry("test");

        // then
        assertEquals(iValDB, testInstance.getAddedRowsDB());
        assertEquals(iValDNS + 1, testInstance.getAddedRowsDNS());
    }


    void writeLinesToFile(File f, String... lines) throws IOException {
        try (FileWriter fw = new FileWriter(f)) {
            for (String line : lines) {
                fw.write(line);
                fw.write("\n");
            }

        }
    }


}
