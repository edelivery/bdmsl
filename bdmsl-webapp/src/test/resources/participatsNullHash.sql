INSERT INTO bdmsl_participant_identifier(id, participant_id, scheme, fk_smp_id, cname_hash, naptr_hash,created_on, last_updated_on) VALUES
(120, '1002:12345678', 'domain-ncp-ids', 14, '','DHI5GMVYRYNY4YWCJEIQ3Q6NM57MIZILJK7WNF5V3GEGWPFVIIHA',  TIMESTAMP '2019-01-24 19:09:06.427', TIMESTAMP '2019-01-24 19:09:06.427'),
(121, '1009:98765432019', 'iso6523-actorid-upis', 3, '374b348ec5f5c9c7d57641c25b21c46d','',  TIMESTAMP '2019-01-24 19:09:06.551', TIMESTAMP '2019-01-24 19:09:06.551'),
(122, '1009:98765432018', 'iso6523-actorid-upis', 3,'','',  TIMESTAMP '2019-01-24 19:09:06.551', TIMESTAMP '2019-01-24 19:09:06.551'),
(123, '1009:9876543212017', 'iso6523-actorid-upis', 3,'','',  TIMESTAMP '2019-01-24 19:09:06.551', TIMESTAMP '2019-01-24 19:09:06.551'),
(124, '1009:987testDifDomain', 'iso6523-actorid-upis', 3,'','',  TIMESTAMP '2019-01-24 19:09:06.551', TIMESTAMP '2019-01-24 19:09:06.551'),
(125, '1009:987testDifDomain', 'iso6523-actorid-upis', 14,'','',  TIMESTAMP '2019-01-24 19:09:06.551', TIMESTAMP '2019-01-24 19:09:06.551'),
(126, 'no:scheme::1009:987emptyscheme','', 14,'','',  TIMESTAMP '2019-01-24 19:09:06.551', TIMESTAMP '2019-01-24 19:09:06.551'),
(127, 'no:scheme::1009:987nullscheme',NULL, 14,'','',  TIMESTAMP '2019-01-24 19:09:06.551', TIMESTAMP '2019-01-24 19:09:06.551'),
(128, '1blue-gw','urn:oasis:names:tc:ebcore:partyid-type:unregistered', 14,'','',  TIMESTAMP '2019-01-24 19:09:06.551', TIMESTAMP '2019-01-24 19:09:06.551'),
(129, '1blue-gw','urn:oasis:names:tc:ebcore:partyid-type:unregistered:cef-scheme', 14,'','',  TIMESTAMP '2019-01-24 19:09:06.551', TIMESTAMP '2019-01-24 19:09:06.551'),
(130, '1health:001','urn:oasis:names:tc:ebcore:partyid-type:unregistered:cef-scheme', 14,'','',  TIMESTAMP '2019-01-24 19:09:06.551', TIMESTAMP '2019-01-24 19:09:06.551');