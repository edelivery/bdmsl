SET REFERENTIAL_INTEGRITY FALSE;
TRUNCATE TABLE BDMSL_MIGRATE_AUD;
TRUNCATE TABLE BDMSL_PARTICIPANT_IDENT_AUD;
TRUNCATE TABLE BDMSL_ALLOWED_WILDCARD_AUD;
TRUNCATE TABLE BDMSL_SMP_AUD;
TRUNCATE TABLE BDMSL_CERTIFICATE_AUD;
TRUNCATE TABLE BDMSL_CERTIFICATE_DOMAIN_AUD;
TRUNCATE TABLE BDMSL_SUBDOMAIN_AUD;
TRUNCATE TABLE BDMSL_CONFIGURATION_AUD;
TRUNCATE TABLE BDMSL_DNS_RECORD_AUD;

TRUNCATE TABLE BDMSL_MIGRATE;
TRUNCATE TABLE BDMSL_PARTICIPANT_IDENTIFIER;
TRUNCATE TABLE BDMSL_ALLOWED_WILDCARD;
TRUNCATE TABLE BDMSL_SMP;
TRUNCATE TABLE BDMSL_CERTIFICATE;
TRUNCATE TABLE BDMSL_CERTIFICATE_DOMAIN;
TRUNCATE TABLE BDMSL_SUBDOMAIN;
TRUNCATE TABLE BDMSL_CONFIGURATION;
TRUNCATE TABLE BDMSL_DNS_RECORD;
SET REFERENTIAL_INTEGRITY TRUE;



INSERT INTO BDMSL_CERTIFICATE(ID, CERTIFICATE_ID, VALID_FROM, VALID_UNTIL, PEM_ENCODING, NEW_CERT_CHANGE_DATE, NEW_CERT_ID, CREATED_ON, LAST_UPDATED_ON) VALUES
(1, 'unsecure-http-client', NOW(), NOW() + INTERVAL '1' YEAR, NULL, NULL, NULL, NOW(), NOW()),
(2, 'CN=test:123', NOW(), NOW() + INTERVAL '1' YEAR, NULL, NULL, NULL, NOW(), NOW()),
(3, 'CN=SMP_TEST_CHANGE_CERTIFICATE,O=DG-DIGIT,C=BE:0000000000000000000000000123abcd', NOW(), NOW() + INTERVAL '1' YEAR, NULL, NULL, NULL, NOW(), NOW()),
(4, 'CN=SMP_TEST_CHANGE_CERTIFICATE_SCHEDULER,O=DG-DIGIT,C=BE:0000000000000000000000000456efgh',NOW(), NOW() + INTERVAL '1' YEAR, NULL, DATE '2019-01-24', 2, NOW(), NOW()),
(5, 'CN=SMP_TEST_CHANGE_CERTIFICATE1,O=DG-DIGIT,C=BE:0000000000000000110000000123abcd', NOW(), NOW() + INTERVAL '1' YEAR, NULL, NULL, NULL, NOW(), NOW()),
(6, 'CN=SMP_TEST_CHANGE_CERTIFICATE2,O=DG-DIGIT,C=BE:00000000000000002200000006666bcd', NOW(), NOW() + INTERVAL '1' YEAR, NULL, NULL, NULL, NOW(), NOW()),
(7, 'CN=SMP_TEST_CHANGE_CERTIFICATE_NEW,O=DG-DIGIT,C=BE:0000000000000000220000000666xbcd', NOW(), NOW() + INTERVAL '1' YEAR, NULL, NULL, NULL, NOW(), NOW()),
(8, 'CN=test:Delete', NOW(), NOW() + INTERVAL '1' YEAR, NULL, NULL, NULL, NOW(), NOW()),
(9, 'CN=test:Update', NOW(), NOW() + INTERVAL '1' YEAR, NULL, NULL, NULL, NOW(), NOW()),
(10, 'CN=test_expired_cert,O=DG-DIGIT,C=BE:0000000000000001', NOW() - INTERVAL '1' YEAR, NOW() - INTERVAL '1' DAY, NULL, NULL, NULL, NOW(), NOW()),
(963, 'CN=SMP_CHANGE_CERT_CHECK_DOMAIN-1,O=DG-DIGIT,C=BE:000000000000000000000000000000001', NOW(), NOW() + INTERVAL '1' YEAR, NULL, NULL, NULL, NOW(), NOW()),
(1054, 'CN=test-discovery-2017,O=DIGIT,C=PT:123456789123456789', NOW(), NOW() + INTERVAL '1' YEAR, NULL, NULL, NULL, NOW(), NOW()),
(1055, 'CN=SMP_DISCOVERY_1,O=DG-DIGIT,C=BE:2016112131415', NOW(), NOW() + INTERVAL '1' YEAR, NULL, NULL, NULL, NOW(), NOW()),
(1056, 'CN=SMP_DISCOVERY_3,O=DG-DIGIT,C=BE:2016112131415', NOW(), NOW() + INTERVAL '1' YEAR, NULL, NULL, NULL, NOW(), NOW()),
(1057, 'CN=SMP_DISCOVERY_4,O=DG-DIGIT,C=BE:2016112131415', NOW(), NOW() + INTERVAL '1' YEAR, NULL, NULL, NULL, NOW(), NOW()),
(22591, 'CN=SMP_TEST_FOR_EDELIVERY_22591,O=DG-DIGIT,C=BE:00000000000000000000000000022591', NOW(), NOW() + INTERVAL '1' YEAR, NULL, NULL, NULL, NOW(), NOW()),
(22592, 'CN=SMP_TEST_FOR_EDELIVERY_22592,O=DG-DIGIT,C=BE:00000000000000000000000000022592', NOW(), NOW() + INTERVAL '1' YEAR, NULL, NULL, NULL, NOW(), NOW()),
(22593, 'CN=SMP_TEST_FOR_EDELIVERY_22593,O=DG-DIGIT,C=BE:00000000000000000000000000022593', NOW(), NOW() + INTERVAL '1' YEAR, NULL, NULL, NULL, NOW(), NOW());
INSERT INTO BDMSL_CERTIFICATE(ID, CERTIFICATE_ID, VALID_FROM, VALID_UNTIL, PEM_ENCODING, NEW_CERT_CHANGE_DATE, NEW_CERT_ID, CREATED_ON, LAST_UPDATED_ON) VALUES
(22594, 'CN=SMP_TEST_FOR_EDELIVERY_22594,O=DG-DIGIT,C=BE:00000000000000000000000000022594', NOW(), NOW() + INTERVAL '1' YEAR, NULL, NULL, NULL, NOW(), NOW()),
(22595, 'CN=SMP_TEST_FOR_EDELIVERY_22595,O=DG-DIGIT,C=BE:00000000000000000000000000022595', NOW(), NOW() + INTERVAL '1' YEAR, NULL, NULL, NULL, NOW(), NOW()),
(123456, 'CN=SMP_123456789101112,O=DG-DIGIT,C=BE:00000000000000000123456789101112', NOW(), NOW() + INTERVAL '1' YEAR, NULL, NULL, NULL, NOW(), NOW()),
(963753, 'CN=SMP_12345678910111277_SUBJECT,O=DG-DIGIT,C=BE:00000000000000012345678910111277', NOW(), NOW() + INTERVAL '1' YEAR, NULL, NULL, NULL, NOW(), NOW()),
(987654, 'CN=senderCN,O=DIGIT,C=BE:0000000000000000000000005b1a52b0', NOW(),NOW() + INTERVAL '1' YEAR, NULL, NULL, NULL, NOW(), NOW()),
(987655, 'CN=SMP_ROOTED,O=DIGIT,C=BE:0000000000000000000000005b1a52b0', NOW(),NOW() + INTERVAL '1' YEAR, NULL, NULL, NULL, NOW(), NOW()),
(1234567, 'CN=SMP_TEST_CHANGE_CERTIFICATE_6,O=DG-DIGIT,C=BE:00000000000000000000000001234567', NOW(),NOW() + INTERVAL '1' YEAR, NULL, NULL, NULL, NOW(), NOW()),
(9999999, 'CN=SMP_9999999_EDELIVERY,O=DG-DIGIT,C=BE:9999999', NOW(), NOW() + INTERVAL '1' YEAR, NULL, NULL, NULL, NOW(), NOW()),
(123456789, 'CN=SMP_123456789,O=DG-DIGIT,C=BE:00000000000000000000000123456789', NOW(), NOW() + INTERVAL '1' YEAR, NULL, NULL, NULL, NOW(), NOW()),
(1000000001, 'CN=SMP_receiverCN,O=DIGIT,C=BE:0000000000000001', TIMESTAMP '2015-06-01 08:37:53.0',NOW() + INTERVAL '1' YEAR, NULL, NULL, NULL, TIMESTAMP '2019-01-24 19:09:05.814', TIMESTAMP '2019-01-24 19:09:05.814'),
(1000000032, 'CN=EHEALTH_SMP_1000000032,O=DG-DIGIT,C=BE:6caa15ffed56ad049dd2b9bc835d2e46', NOW(),NOW() + INTERVAL '1' YEAR, NULL, NULL, NULL, NOW(), NOW()),
(1000000040, 'CN=createSmpOk,O=Subdomain-Entity-EU,C=BE:000000000000asdadf44878wqeadfs54', NOW(), NOW() + INTERVAL '1' YEAR, NULL, NULL, NULL, NOW(), NOW()),
(1000000002, 'CN=SMP-ChangeNonPKICert,O=DIGIT,C=BE:00000000000000000000000000000004', NOW(), NOW() + INTERVAL '1' YEAR, NULL, NULL, NULL, NOW(), NOW());


INSERT INTO BDMSL_ALLOWED_WILDCARD(SCHEME, FK_CERTIFICATE_ID, CREATED_ON, LAST_UPDATED_ON) VALUES
('iso6523-actorid-upis', 4, NOW(), NOW()),
('iso123456-actorid-upis', 5, NOW(), NOW()),
('iso9999999-id-scheme', 9999999, NOW(), NOW());


INSERT INTO BDMSL_SUBDOMAIN(SUBDOMAIN_ID, SUBDOMAIN_NAME,DNS_ZONE, DESCRIPTION, PARTICIPANT_ID_REGEXP, DNS_RECORD_TYPES, SMP_URL_SCHEMAS, CREATED_ON, LAST_UPDATED_ON, SMP_IA_CERT_REGEXP) VALUES
(0, 'test.isalive.test', 'test.isalive.test','is alive domain','^.*$','all','all', NOW(), NOW(), null),
(1, 'acc.edelivery.tech.ec.europa.eu', 'acc.edelivery.tech.ec.europa.eu','Domain for OpenPeppol Acceptance', '^((((0002|0007|0009|0037|0060|0088|0096|0097|0106|0135|0142|9901|9902|9904|9905|9906|9907|9908|9909|9910|9912|9913|9914|9915|9916|9917|9918|9919|9920|9921|9922|9923|9924|9925|9926|9927|9928|9929|9930|9931|9932|9933|9934|9935|9936|9937|9938|9939|9940|9941|9942|9943|9944|9945|9946|9947|9948|9949|9950|9951|9952|9953|9954|9955|9956|9957|0184):).*)|(\*))$','cname','all', NOW(), NOW(),null),
(2, 'ehealth.acc.edelivery.tech.ec.europa.eu','acc.edelivery.tech.ec.europa.eu','Domain for eHealth Acceptance','^.*$','naptr','all',NOW(), NOW(), null),
(3, 'ehealth.edelivery.tech.ec.europa.eu','edelivery.tech.ec.europa.eu','Domain for eHealth production','^.*$','all','all',NOW(), NOW(),null),
(4, 'sea.acc.edelivery.tech.ec.europa.eu','sea.acc.edelivery.tech.ec.europa.eu','Domain for sea Acceptance','^.*$','all','all',NOW(), NOW(),null),
(5, 'toledo.acc.edelivery.tech.ec.europa.eu','acc.edelivery.tech.ec.europa.eu','Domain for toledo Acceptance','^.*$','all','all',NOW(), NOW(),null),
(6, 'delta.acc.edelivery.tech.ec.europa.eu','acc.edelivery.tech.ec.europa.eu','Domain for delta Acceptance','^.*$','all','all', NOW(), NOW(),null),
(7, 'test1.isalive.test', 'test1.isalive.test','is alive domain','^.*$','cname','all', NOW(), NOW(), null),
(8, 'test2.isalive.test', 'test2.isalive.test','is alive domain','^.*$','naptr','all', NOW(), NOW(), null),
(9, 'test-domain.acc.edelivery.tech.ec.europa.eu','acc.edelivery.tech.ec.europa.eu','','^.*$','all','https',NOW(), NOW(), '^.*((CN=SECOND_DOMAIN)|(OU=Domain SMP)).*$'),
(22591, '22591.acc.edelivery.tech.ec.europa.eu','acc.edelivery.tech.ec.europa.eu','','^.*$','all','all',NOW(), NOW(), null),
(22592, '22592.acc.edelivery.tech.ec.europa.eu','acc.edelivery.tech.ec.europa.eu','','^.*$','cname','all',NOW(), NOW(), null),
(22593, '22593.acc.edelivery.tech.ec.europa.eu','acc.edelivery.tech.ec.europa.eu','','^.*$','naptr','all',NOW(), NOW(), null),
(22594, '22594.acc.edelivery.tech.ec.europa.eu','acc.edelivery.tech.ec.europa.eu','','^.*$','wrong parameter','all',NOW(), NOW(), null),
(22595, '22595.acc.edelivery.tech.ec.europa.eu','acc.edelivery.tech.ec.europa.eu','','^.*$','','',NOW(), NOW(), null),
(99999, 'logicaladdress.acc.edelivery.tech.ec.europa.eu','acc.edelivery.tech.ec.europa.eu','','^.*$','all','https',NOW(), NOW(), null);


INSERT INTO BDMSL_CONFIGURATION(PROPERTY, VALUE, DESCRIPTION, CREATED_ON, LAST_UPDATED_ON) VALUES
('unsecureLoginAllowed', 'true', '''true'' if the use of HTTPS is not required. If the value is set to ''true'', then the user ''unsecure-http-client'' is automatically created. Possible values: true/false',NOW(),NOW()),
('configurationDir', './target/', 'The path to the folder containing all the configuration files (keystore and sig0 key)',NOW(),NOW()),
('httpProxyHost', '158.169.9.13', 'The http proxy host',NOW(),NOW()),
('httpProxyPort', '8012', 'The http proxy port',NOW(),NOW()),
('httpsProxyHost', '158.169.9.13', 'The https proxy host',NOW(),NOW()),
('httpsProxyPort', '8012', 'The https proxy port',NOW(),NOW()),
('useProxy', 'true', '''true'' if a proxy is required to connect to the internet. Possible values: true/false',NOW(),NOW()),
('httpProxyUser', 'user', 'The proxy user',NOW(),NOW()),
('dnsClient.enabled', 'true', '''true'' if registration of DNS records is required. Must be ''true'' in production. Possible values: true/false',NOW(),NOW()),
('dnsClient.show.entries', 'true', 'Possible values: true/false',NOW(),NOW()),
('dnsClient.server', 'ddnsext.tech.ec.europa.eu', 'The DNS server',NOW(),NOW()),
('dnsClient.publisherPrefix', 'publisher', 'This is the prefix for the publishers (SMP). This is to be concatenated with the associated DNS domain in the table ''bdmsl_certificate_domain''',NOW(),NOW()),
('dnsClient.SIG0Enabled', 'false', '''true'' if the SIG0 signing is enabled. Required fr DNSSEC. Possible values: true/false',NOW(),NOW()),
('dnsClient.SIG0PublicKeyName', 'sig0.acc.edelivery.tech.ec.europa.eu.', 'The public key name of the SIG0 key',NOW(),NOW()),
('dnsClient.SIG0KeyFileName', 'SIG0.private', 'The actual SIG0 key file. Should be just the filename if the file is in the classpath or in the ''configurationDir''',NOW(),NOW()),
('signResponse', 'true', '''true'' if the responses must be signed. Possible values: true/false',NOW(),NOW()),
('signResponseAlgorithm','','The signature algorithm to use when signing responses. Examples: ''http://www.w3.org/2001/04/xmldsig-more#rsa-sha256'', ''http://www.w3.org/2021/04/xmldsig-more#eddsa-ed25519'', ...', NOW(), NOW()),
('signResponseDigestAlgorithm','http://www.w3.org/2000/09/xmldsig#sha1','The signature digest algorithm to use when signing responses. Examples: ''http://www.w3.org/2001/04/xmlenc#sha256'', ''http://www.w3.org/2001/04/xmlenc#sha512''', NOW(), NOW()),
('keystoreFileName', 'keystore.jks', 'The keystore file. Should be just the filename if the file is in the classpath or in the ''configurationDir''',NOW(),NOW()),
('keystoreType','JKS','The keystore type. Possible values: JKS/PKCS12.', NOW(), NOW()),
('keystoreAlias', 'senderalias', 'The alias in the keystore.',NOW(),NOW()),
('keystorePassword', 'bF3jlZ7O385k8CgSTStjBw==', 'Base64 encrypted password for Keystore.',NOW(),NOW()),
('truststoreFileName', 'bdmsl-truststore.p12', 'The truststore file. Should be just the filename if the file is in the classpath or in the ''configurationDir''',NOW(),NOW()),
('truststoreType','PKCS12','The keystore type. Possible values: JKS/PKCS12.', NOW(), NOW()),
('truststorePassword', 'bF3jlZ7O385k8CgSTStjBw==', 'Base64 encrypted password for truststore.',NOW(),NOW()),
('paginationListRequest', '100', 'Number of participants per page for the ''list'' operation of ''ManageParticipantIdentifier'' service. This property is used for pagination purposes.',NOW(),NOW()),
('certificateChangeCronExpression', '0 0 2 ? * *', 'Cron expression for the changeCertificate job. Example: 0 0 2 ? * * (everyday at 2:00 am)',NOW(),NOW()),
('dataInconsistencyAnalyzer.cronJobExpression', '0 0 3 ? * *', 'Cron expression for dataInconsistencyChecker job. Example: 0 0 3 ? * * (everyday at 3:00 am)', NOW(), NOW()),
('dataInconsistencyAnalyzer.recipientEmail', 'email@domain.com', 'Email address to receive Data Inconsistency Checker results', NOW(), NOW()),
('dataInconsistencyAnalyzer.senderEmail', 'automated-notifications@nomail.ec.europa.eu', 'Sender email address for reporting Data Inconsistency Analyzer.', NOW(), NOW()),
('authentication.bluecoat.enabled', 'false', 'Enables reverse proxy authentication.', NOW(), NOW()),
('authorization.domain.legacy.enabled', 'true', 'Enables legacy authentication withou truststore.', NOW(), NOW()),
('adminPassword', '$2a$10$tbHhDiojqNy33xOqeAPz3./r0dz4dR8ZPPAOfH7wWHfcC2I91V/zG', 'BCrypt Hashed password to access admin services', NOW(), NOW()),
('encriptionPrivateKey', 'encryptedPasswordKey.key', 'Name of the 256 bit AES secret key to encrypt or decrypt passwords.', NOW(), NOW()),
('httpProxyPassword', 'bF3jlZ7O385k8CgSTStjBw==', 'Base64 encrypted password for Proxy.',NOW(),NOW()),
('authorization.smp.certSubjectRegex', '^.*(CN=EHEALTH_SMP|CN=SMP_|OU=PEPPOL PRODUCTION SMP).*$', 'User with ''ROOT-CA'' is granted SMP_ROLE only if its certificate''s Subject matches configured regexp', NOW(), NOW()),
('mail.smtp.port', '25', 'smpt port',NOW(), NOW()),
('mail.smtp.host', 'localhost', '',NOW(), NOW()),
('sml.property.refresh.cronJobExpression', '*/5 * * * * *', '',NOW(), NOW());

INSERT INTO BDMSL_CERTIFICATE_DOMAIN(TRUSTSTORE_ALIAS, CERTIFICATE, CRL_URL, CREATED_ON, LAST_UPDATED_ON, IS_ROOT_CA, FK_SUBDOMAIN_ID, is_admin) VALUES
(null, STRINGDECODE('CN=slash/backslash\\\\quote\\\"colon:_rfc2253special_ampersand&comma\\,equals\\=plus\\+lessthan\\<greaterthan\\>hash\\#semicolon\\;end, O=DE\u1e9e\u00dfAaPLzo\u0142cNO\u00c6\u00e6\u00d8\u00f8Aa, C=PL'), NULL, NOW(), NOW(), 0, 22594, 0),
(null, STRINGDECODE('CN=issuer-slash/backslash\\\\quote\\\"colon:_rfc2253special_ampersand&comma\\,equals\\=plus\\+lessthan\\<greaterthan\\>hash\\#semicolon\\;end, O=DE\u1e9e\u00dfAaPLzo\u0142cNO\u00c6\u00e6\u00d8\u00f8Aa, C=PL'), NULL, NOW(), NOW(), 1, 22594, 0),
(null, 'CN=unsecure_root,O=delete_in_production,C=only_for_testing', 'http://crl.globalsign.net/root.crl',NOW(),NOW(), 1, 1, 0),
(null, 'CN=rootCN,O=DIGIT,C=BE', NULL, NOW(), NOW(), 1, 1, 0),
('test intermediate issuer 01 (test root ca v01)','CN=Test intermediate Issuer 01,OU=eDelivery,O=DIGIT,C=BE', NULL, NOW(), NOW(), 1, 1, 0),
('test intermediate issuer 01 (test root ca v01)','CN=Test intermediate Issuer 02,OU=eDelivery,O=DIGIT,C=BE', NULL, NOW(), NOW(), 1, 9, 0),
(null, 'CN=GlobalSign Root CA,OU=Root CA,O=GlobalSign nv-sa,C=BE', NULL, NOW(), NOW(), 1, 1, 0),
(null, 'CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA,OU=FOR TEST PURPOSES ONLY,O=NATIONAL IT AND TELECOM AGENCY,C=DK', 'test.crl', NOW(), NOW(), 1, 1, 0),
(null, 'CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA,O=NATIONAL AGENCY,C=BE', NULL, NOW(), NOW(), 1, 1, 0),
(null, 'CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA - G2,OU=FOR TEST ONLY,O=OpenPEPPOL AISBL,C=BE', NULL, NOW(), NOW(), 1, 1, 0),
(null, 'CN=PEPPOL SERVICE METADATA PUBLISHER CA,O=NATIONAL IT AND TELECOM AGENCY,C=DK', NULL, NOW(), NOW(), 1, 1, 0),
(null, 'CN=ISSUER TEST WITH DOMAIN REGEXP,OU=FOR TEST ONLY,O=DIGIT,C=BE', NULL, NOW(), NOW(), 1, 9, 0),
(null, 'CN=SMP_123456789,O=DG-DIGIT,C=BE', NULL, NOW(), NOW(), 0, 2, 0),
(null, 'CN=SMP_123456789101112,O=DG-DIGIT,C=BE', NULL, NOW(), NOW(), 0, 2, 0),
(null, 'CN=SMP_XPISSUER,O=DG-DIGIT,C=BE', NULL, NOW(), NOW(), 1, 1, 0),
(null, 'CN=SMP_12345678910111277,O=DG-DIGIT,C=BE', NULL, NOW(), NOW(), 1, 3, 0),
(null, 'CN=SMP_0000011113333377777,O=DG-DIGIT,C=BE', NULL, NOW(), NOW(), 0, 3, 0),
(null, 'CN=SMP_357951852456,O=DG-DIGIT,C=BE', NULL, NOW(), NOW(), 0, 3, 0),
(null, 'CN=EHEALTH_SMP_77777777,O=DG-DIGIT,C=BE', NULL, NOW(), NOW(), 0, 3, 0),
(null, 'CN=COMMON_TRUSTED_CERTIFICATE_77777777,O=DG-DIGIT,C=BE', NULL, NOW(), NOW(), 0, 3, 0),
(null, 'C=BE, O=European Commission, OU=CEF_eDelivery.europa.eu, OU=eHealth, CN=DUMMY_SMP_TEST, emailAddress=CEF-EDELIVERY-SUPPORT@ec.europa.eu', NULL, NOW(), NOW(), 0, 3, 0),
(null, 'C=PT, O=T-Systems GmbH, OU=T-Systems Center, ST=Nordrhein Westfalen,postalCode=57250, L=Netphen,street=Untere Industriestr, CN=Business CA 4', NULL, NOW(), NOW(), 1, 4, 0),
(null, 'C=BE, O=Subdomain-Entity, OU=Subdomain-Entity, ST=Nordrhein Westfalen, L=Netphen, CN=Subdomain Issuer', NULL, NOW(), NOW(), 1, 5, 0),
(null, 'C=BE, O=Subdomain-Entity,OU=DoubleSubdomain, CN=Subdomain SubjectDouble', NULL, NOW(), NOW(), 0, 6, 0),
(null, 'C=BE, O=Subdomain-Entity, CN=Subdomain SubjectDouble', NULL, NOW(), NOW(), 0, 6, 0),
(null, 'CN=Connectivity Test Component CA,OU=Connecting Europe Facility,O=Connectivity Test,ST=Belgium,C=BE', NULL, NOW(), NOW(), 1, 1, 0),
(null, 'C=BE, O=Subdomain-Entity, CN=Subdomain Subject', NULL, NOW(), NOW(), 0, 6, 0),
(null, 'C=BE, O=Subdomain-Entityaa, CN=SMP_Subdomain Subject', NULL, NOW(), NOW(), 0, 6, 0),
(null, 'C=BE, O=Subdomain-Entityaa123, CN=SMP_TEST_1', NULL, NOW(), NOW(), 0, 6, 0),
(null, 'C=BE, O=Subdomain-Entityaa123, CN=SMP_TEST_2', NULL, NOW(), NOW(), 0, 6, 0),
(null, 'C=BE,CN=Subdomain issuer,O=Subdomain-Entityaaa', NULL, NOW(), NOW(), 1, 6, 0),
(null, 'C=BE, O=DG-DIGIT, CN=COMMON_CERTIFICATE_TEST', NULL, NOW(), NOW(), 0, 1, 0),
(null, 'CN=SMP_TEST_CREATESMP,O=DG-DIGIT,C=BE', NULL, NOW(), NOW(), 0, 1, 0),
(null, 'CN=SMP_TEST_CHANGE_CERTIFICATE2,O=DG-DIGIT,C=BE', NULL, NOW(), NOW(), 0, 1, 0),
(null, 'CN=EHEALTH_SMP_1000000032,O=DG-DIGIT,C=BE', NULL, NOW(), NOW(), 0, 1, 0),
(null, 'CN=SMP_TEST_CHANGE_CERTIFICATE,O=DG-DIGIT,C=BE', NULL, NOW(), NOW(), 0, 1,0),
(null, 'C=DK, CN=TEST_LOGICAL_ADDRESS,O=DIGIT', NULL, NOW(), NOW(), 1, 99999, 0),
(null, 'CN=TEST_LOGICAL_ADDRESS_12345,O=DIGIT,C=DK', NULL, NOW(), NOW(), 1, 99999, 0),
(null, 'C=DK,CN=TEST_LOGICAL_ADDRESS_123456789,O=DIGIT', NULL, NOW(), NOW(), 1, 99999, 0),
(null, 'CN=test-discovery-2017,O=DIGIT,C=PT', NULL, NOW(), NOW(), 0, 99999, 0),
(null, 'CN=SMP_DISCOVERY_1,O=DG-DIGIT,C=BE', NULL, NOW(), NOW(), 0, 3, 0),
(null, 'CN=SMP_DISCOVERY_3,O=DG-DIGIT,C=BE', NULL, NOW(), NOW(), 0, 6, 0),
(null, 'CN=SMP_DISCOVERY_ISSUER_2,O=DG-DIGIT,C=BE', NULL, NOW(), NOW(), 1, 6, 0),
(null, 'CN=SMP_DISCOVERY_ISSUER_4,O=DG-DIGIT,C=BE', NULL, NOW(), NOW(), 1, 6, 0),
(null, 'CN=SMP_TEST_FOR_EDELIVERY_22591,O=DG-DIGIT,C=BE', NULL, NOW(), NOW(), 0, 22591, 0),
(null, 'CN=SMP_TEST_FOR_EDELIVERY_22592,O=DG-DIGIT,C=BE', NULL, NOW(), NOW(), 0, 22592, 0),
(null, 'CN=SMP_TEST_FOR_EDELIVERY_22593,O=DG-DIGIT,C=BE', NULL, NOW(), NOW(), 0, 22593, 0),
(null, 'CN=SMP_TEST_FOR_EDELIVERY_22594,O=DG-DIGIT,C=BE', NULL, NOW(), NOW(), 0, 22594, 0),
(null, 'CN=SMP_TEST_FOR_EDELIVERY_22595,O=DG-DIGIT,C=BE', NULL, NOW(), NOW(), 0, 22595, 0),
(null, 'C=BE,CN=Subdomain issuer,O=Subdomain-Entityaaa-Special-Chars', NULL, NOW(), NOW(), 1, 22594, 0),
(null, 'C=BE,CN=Changing Certificates 1,O=DIGIT-B28', NULL, NOW(), NOW(), 0, 22594, 0),
(null, 'C=BE,CN=Changing Certificates 2,O=DIGIT-B28', NULL, NOW(), NOW(), 0, 22594, 0),
(null, 'CN=EHEALTH_SMP_PREPARE_CHANGE, O=European Commission, C=BE', NULL, NOW(), NOW(), 0, 2, 0),
(null, 'CN=Chain1 RootCA,O=DIGIT-B28, C=BE', NULL, NOW(), NOW(), 1, 2, 0),
(null, 'CN=Chain1 IntermediateCA,O=DIGIT-B28, C=BE', NULL, NOW(), NOW(), 1, 2, 0),
(null, 'CN=Chain2 IntermediateCA,O=DIGIT-B28, C=BE', NULL, NOW(), NOW(), 1, 2, 0),
(null, 'CN=Chain3 RootCA,O=DIGIT-B28, C=BE', NULL, NOW(), NOW(), 1, 2,0),
(null, 'CN=SMLAdmin,O=DIGIT, C=BE', NULL, NOW(), NOW(), 0, 2,1),
(null, 'CN=DIGIT_SMP_TECH_TEAM_3,O=DIGIT,OU=FOR TEST ONLY,C=BE', NULL, NOW(), NOW(), 0, 2,1),
(null, 'CN=SMP-ChangeNonPKICert,O=DIGIT,C=BE', NULL, NOW(), NOW(), 0, 3,0);

INSERT INTO BDMSL_CERTIFICATE_DOMAIN(CERTIFICATE, CRL_URL, CREATED_ON, LAST_UPDATED_ON, IS_ROOT_CA, FK_SUBDOMAIN_ID, is_admin) VALUES
('CN=SerialAuthentication,O=DIGIT,C=BE', NULL, NOW(), NOW(), 0, 3,0 ),
('CN=Trusted Issuer CA,O=DIGIT,C=BE', NULL, NOW(), NOW(), 1, 3,0 );


INSERT INTO BDMSL_SMP(ID, SMP_ID, FK_CERTIFICATE_ID, ENDPOINT_PHYSICAL_ADDRESS, ENDPOINT_LOGICAL_ADDRESS, CREATED_ON, LAST_UPDATED_ON, FK_SUBDOMAIN_ID) VALUES
(1, 'found', 2, '10.10.10.10', 'http://logicalAddress', NOW(), NOW(), 1),
(2, 'toBeDeleted', 1, '10.10.10.10', 'http://logicalAddress', NOW(), NOW(), 1),
(3, 'toBeUpdated', 1, '10.10.10.10', 'http://logicalAddress', NOW(), NOW(), 1),
(4, 'foundUnsecure', 1, '10.10.10.10', 'http://logicalAddress.be', NOW(), NOW(), 1),
(5, 'smpForListTest', 1, '10.10.10.10', 'http://logicalAddress', NOW(), NOW(), 1),
(6, 'smpForMigrateTest', 1, '10.10.10.10', 'http://logicalAddress', NOW(), NOW(), 1),
(7, 'smpForChangeCertificateTest', 4, '10.10.10.10', 'http://logicalAddress', NOW(), NOW(), 1),
(8, 'toBeDeletedWithMigrationPlanned', 1, '10.10.10.10', 'http://logicalAddress', NOW(), NOW(), 1),
(9, 'TESTsmpChangeCertificate', 5, '10.10.10.10', 'http://logicalAddress', NOW(), NOW(), 1),
(10, 'smpSameCertificate1', 6, '10.10.10.10', 'http://logicalAddress', NOW(), NOW(), 1),
(11, 'SMP123456789', 123456789, '10.10.10.10', 'http://logicalAddress', NOW(), NOW(), 2),
(12, 'SMP2016', 123456, '10.10.10.10', 'http://logicalAddress', NOW(), NOW(), 1),
(13, 'SMP123456789101112', 123456, '10.10.10.10', 'https://logicalAddress.com.eu', NOW(), NOW(), 2),
(14, 'SMP2017', 1, '10.10.10.10', 'http://logicalAddress', NOW(), NOW(), 6),
(15, 'SMP-CHANGE-CERT-BY-ADMIN', 3, '10.10.10.10', 'http://logicalAddress', NOW(), NOW(), 1),
(16, 'SMP-CHANGE-CERT-BY-ADMIN-2', 987655, '10.10.10.10', 'http://logicalAddress', NOW(), NOW(), 1),
(17, 'SMP-CHANGE-CERT-BY-ADMIN-3', 1000000002, '10.10.10.10', 'http://logicalAddress', NOW(), NOW(), 3),
(18, 'isAliveSmpId', 1, '10.10.10.10', 'https://isAliveSmpId.edelivery.be', NOW(), NOW(), 1),
(19, 'smp-sample', 1, '10.10.10.10', 'https://domain.sample.edelivery.be.', NOW(), NOW(), 1),
(20, 'SMP12345678910111277', 963753, '10.10.10.10', 'https://logicalAddress.com.eu', NOW(), NOW(), 1),
(21, 'SMPTESTFOREDELIVERY22591', 22591, '10.10.10.10', 'http://logicalAddress', NOW(), NOW(), 22591),
(22, 'SMPTESTFOREDELIVERY22592', 22592, '10.10.10.10', 'http://logicalAddress', NOW(), NOW(), 22592),
(23, 'SMPTESTFOREDELIVERY22593', 22593, '10.10.10.10', 'http://logicalAddress', NOW(), NOW(), 22593),
(24, 'SMPTESTFOREDELIVERY22594', 22594, '10.10.10.10', 'http://logicalAddress', NOW(), NOW(), 22594),
(25, 'SMPTESTFOREDELIVERY22595', 22595, '10.10.10.10', 'http://logicalAddress', NOW(), NOW(), 22595),
(26, 'logicalAddressSMP', 1, '10.10.10.10', 'http://logicalAddress', NOW(), NOW(), 1),
(27, 'SMP-CHANGE-CERT-CHECK-DOMAIN', 963, '10.10.10.10', 'http://logicalAddress', NOW(), NOW(), 3),
(28, 'SMP-TO-BE-FOUND', 1, '10.10.10.10', 'http://logicalAddress', NOW(), NOW(), 3),
(29, 'SMP-TO-BE-DELETED', 1, '10.10.10.10', 'http://logicalAddress', NOW(), NOW(), 3),
(30, 'SMP-TO-BE-UPDATED', 1, '10.10.10.10', 'http://logicalAddress', NOW(), NOW(), 3),
(31, 'SMP-EXPIRED-CERT-01', 10, '10.10.10.10', 'http://logicalAddress', NOW(), NOW(), 3),
(32, 'SMP-EXPIRED-CERT-02', 10, '10.10.10.10', 'http://logicalAddress', NOW(), NOW(), 3),
(33, 'SMP-CHANGE-CERT-WRONG-DOMAIN', 1057, '10.10.10.10', 'http://logicalAddress', NOW(), NOW(), 2);

INSERT INTO BDMSL_SMP(ID, SMP_ID, FK_CERTIFICATE_ID, ENDPOINT_PHYSICAL_ADDRESS, ENDPOINT_LOGICAL_ADDRESS, CREATED_ON, LAST_UPDATED_ON, FK_SUBDOMAIN_ID, SMP_DISABLED) VALUES
(34, 'SMP-DISABLED', 1, '10.10.10.10', 'http://logicalAddress', NOW(), NOW(), 2, true);



INSERT INTO BDMSL_MIGRATE(SCHEME, PARTICIPANT_ID, MIGRATION_KEY, NEW_SMP_ID, OLD_SMP_ID, MIGRATED, CREATED_ON, LAST_UPDATED_ON) VALUES
('iso6523-actorid-upis', '0009:223456789MigrateTest', '@Df1Og2#', NULL, 'foundUnsecure', FALSE, NOW(), NOW()),
('iso6523-actorid-upis', '0009:223456789MigrateTest1', '@Df1Og2#', NULL, 'foundUnsecure', FALSE, NOW(), NOW()),
('iso6523-actorid-upis', '0009:toBeDeletedWithMigrationPlanned', '@Df1Og2#', NULL, 'toBeDeletedWithMigrationPlanned', FALSE, NOW(), NOW()),
('ehealth-ncp-ids', 'urn:ehealth:pl:ncpb-txb', '@=aFa15B', NULL, 'SMP2016', FALSE, NOW(), NOW()),
('ehealth-ncp-ids', 'urn:ehealth:lu:ncpb-txb', 'azsxdc123', NULL, 'SMP2016', FALSE, NOW(), NOW()),
('iso6523-actorid-upis', '0037:01571593999', '66fac918-0f4f-4fc5', 'newSmpId', 'SMP2016', TRUE, NOW(), NOW()),
('iso6523-actorid-upis', '0009:tobeMigrated123456', '@Df1Og2#9875', 'SMP2016', 'toBeDeletedWithMigrationPlanned', FALSE, NOW(), NOW());

-- insert enabled participatns
INSERT INTO bdmsl_participant_identifier(id, participant_id, scheme, fk_smp_id, cname_hash, naptr_hash,created_on, last_updated_on) VALUES
(1, '0009:123456789', 'iso6523-actorid-upis', 4, 'c83d76ce1d71b56cb8704d73408f8d3b','E3MEGUPGRSOFOE3WE5AOL3XTNHEIESCGUEGWGRUPP3ZQVY6DZNXQ', NOW(), NOW()),
(2, '0009:223456789', 'iso6523-actorid-upis', 4,'6fae389af70dd8f6a9feed0b85da665d','2IE4VXBWIUAMVMR7NN5B4YFY3DKRM4FPLEJF25C3KADYCXRZB6VQ', NOW(), NOW()),
(3, '0009:123456789AlwaysPresent', 'iso6523-actorid-upis', 4,'9d4f746090d7cba619b5c248aeff9f3a','BMBEQFLAXB3ANKCUYUOSBNYW5PNS6AYE6K7KDF6JLUZJP2VCYDBQ',  NOW(), NOW()),
(4, '0009:123456789AlwaysPresent2', 'iso6523-actorid-upis', 4,'768d5cd4cc378966ff9f6ce8e3cdd5de','AV6S4RAYB66VMGRVRJAOHV4STOZRNZLYU52DK76QLWK5XZJPS63A',  NOW(), NOW()),
(5, '0009:caseInsensitivityCheck', 'iso6523-actorid-upis', 4,'07e5f29872dd2e495f7a2d99b1f1e9b3','G3TBLEDJQLEP4KFKRQUPCA76H27ORD4LY37VGQX2PNEJTMXCNWNQ',  NOW(), NOW()),
(6, '0007:123456789', 'iso6523-actorid-upis', 4,'49d31990d68b7c0135de52b34189a41f','WDBETX7YYPTY2QMCNYTCHEF43EAKUSOZBQ4IOPJWRVWEHDXAEO2Q',  NOW(), NOW()),
(7, '0060:123456789', 'iso6523-actorid-upis', 4, 'f6f992ed2a1d9e78dbda10b20648c09d','4UVBL5NLEHZK7GNZPK25KNNAFKQTKBG2RDUT5V6KSIUNTHLFHGIQ',  NOW(), NOW()),
(8, '0009:123456789DeleteList', 'iso6523-actorid-upis', 4,'c1aecc94189b353d0b48251a56c7748f','HA5ELVJJZC2LGIU2U57GFILOPSNNJ5OVO6Y4M4JMOAPYDZBTHGBA',  NOW(), NOW()),
(9, '0009:223456789DeleteList', 'iso6523-actorid-upis', 4,'4813b1ab90eb5a50ce3e9a6e8b8c9e31','6KIZXQEXYEQWALJIF5FT7BP4E46WU4EZOUHMLAMQYE3VXJ3DJUQQ',  NOW(), NOW()),
(10, '0009:123456789DeleteList2', 'iso6523-actorid-upis', 5,'8a0fc42404836d445e67f902ce8ec062','CDIF5P6ASMCZ55LD65DRIJRQBH5LHJS6DCPEW7MMGCVGFJIVWZ6Q',  NOW(), NOW()),
(11, '0009:223456789DeleteList2', 'iso6523-actorid-upis', 5,'7c922c272302f8cc8d66c74c9a3d5c11','J7QI2YVWGLAPTCK4ATEFGML62DSB24JMSL6NXKD4IYDAX7DTDU5A',  NOW(), NOW()),
(12, '0009:223456789MigrateTest', 'iso6523-actorid-upis', 4,'00e6d94f0e9c39f6e8dc9862af6ca300','6HBNJQE2P7KOQE2NSPHRCS6WLOLJQSKD63Q3HMLYTHHHWY3Z4ETA',  NOW(), NOW()),
(13, '0009:223456789MigrateTest1', 'iso6523-actorid-upis', 4,'1801f4b9ff962cab50e4bc54b97f2e14','Z4B23OSTDCYBZ3DSFA2GAOHX6ZPOOI7UMAM7NSYBJYJ4OCGCON6A',  NOW(), NOW()),
(14, '0009:223456789DeletedTest1', 'iso6523-actorid-upis', 2,'0d8a3142047e1f07f0b56ff4ab3c5c8a','ZG7JNV2K4ZRIWBS6SLQRRNBVNES4FDZ5CA4XMI45VC5EB32G3RHA',  NOW(), NOW()),
(15, '0009:toBeDeletedWithMigrationPlanned', 'iso6523-actorid-upis', 8, 'bcd861f1224b4f50e064c57b174731b5','2O7KCSOMPRQZLMY7J3622QKCMBAOPOCDPV4BHPX3EYJC7CAB2HJA',  NOW(), NOW()),
(16, 'urn:ehealth:us:ncpb-ed', 'ehealth-ncp-ids', 13,'31dbffee1e26fb067178f78c06e93a9a','YN4JC343OI56CSDFTCUBQD5EWGF3FE7ECKSEOTC3WBYJGEAFPA5A',  NOW(), NOW()),
(17, 'urn:ehealth:pl:ncpb-txb', 'ehealth-ncp-ids', 12,'58ceaaf13829faa52c8e3fb24a661717','J7YO5Z5YFBACZKEADNUUYKQOJNJCQRSL7LPEM3OJCPLBQJ4KZ2CQ',  NOW(), NOW()),
(18, 'urn:ehealth:sw:ncpb-ics', 'ehealth-ncp-ids', 12,'17aec24652a69c0d8ff79d815f2afb40','MWZSALJEYP65KBZTLZMIJLVQIYEQEOYZHV4UNPMGYOQBYPLJJ2TQ',  NOW(), NOW()),
(19, 'urn:ehealth:lu:ncpb-txb', 'ehealth-ncp-ids', 13,'0650a69c4bf0b730aacf9590602c7038','DLL2VOAVHADANCUTNJSKULCTKPPWE5NAWKRFATT2W7MM3VUUV7AA',  NOW(), NOW()),
(20, '0002:12345678', 'domain-ncp-ids', 14, 'fc9df932c93099377b50ad5e05be966e','DHI5GMVYRYNY4YWCJEIQ3Q6NM57MIZILJK7WNF5V3GEGWPFVIIHA',  NOW(), NOW()),
(21, '0009:98765432019', 'iso6523-actorid-upis', 3, '374b348ec5f5c9c7d57641c25b21c46d','B4UGYBIZS6GY5I5TVZXZWUQGCVT3G7BQ4BYTNNCWANSQQCZNS6JA',  NOW(), NOW()),
(22, '0009:98765432018', 'iso6523-actorid-upis', 3,'efeeb41133d517a9d25a0e9e99d259f7','XHPBWT7TYYTVWKLYGUJGOPAM6IJ7ANOM3THUGLZZOTPZ74AFR6EA',  NOW(), NOW()),
(23, '0009:9876543212017', 'iso6523-actorid-upis', 3,'a0f6ae7abfcdb56112394172d5ec4a5b','AFEIEROAFXXWWTGTRN3APKMKKARGBIXZLNJOHEHYFS4B35ORKYHA',  NOW(), NOW()),
(24, '0009:987testDifDomain', 'iso6523-actorid-upis', 3,'831cf636ef34c2e889352c05c4269667','5IOPBWOUQS56RAEYHP3Q7QM5ZPAVVCJWPTUU56ULVJJQHUHJ3VWQ',  NOW(), NOW()),
(25, '0009:987testDifDomain', 'iso6523-actorid-upis', 14,'831cf636ef34c2e889352c05c4269667','5IOPBWOUQS56RAEYHP3Q7QM5ZPAVVCJWPTUU56ULVJJQHUHJ3VWQ',  NOW(), NOW()),
(26, 'no:scheme::0009:987emptyscheme','', 14,'13730cbc816d277a248f39fa22890ded','KJY4LFUHVWNRIZZWUAAYMATCJCQQ57ZAUM7GXZMNDJKTC367LVAA',  NOW(), NOW()),
(27, 'no:scheme::0009:987nullscheme',NULL, 14,'ea67be67451e81d1113fba54f5540477','N7BXKM7GSMFXZJ2VA3Q6B5DLWRMT4PYNEIUYVD6CCZ46J7VIG52A',  NOW(), NOW()),
(28, 'blue-gw','urn:oasis:names:tc:ebcore:partyid-type:unregistered', 14,'1ea67be67451e81d1113fba54f5540477','1N7BXKM7GSMFXZJ2VA3Q6B5DLWRMT4PYNEIUYVD6CCZ46J7VIG52A',  NOW(), NOW()),
(29, 'blue-gw','urn:oasis:names:tc:ebcore:partyid-type:unregistered:cef-scheme', 14,'2ea67be67451e81d1113fba54f5540477','2N7BXKM7GSMFXZJ2VA3Q6B5DLWRMT4PYNEIUYVD6CCZ46J7VIG52A',  NOW(), NOW()),
(30, 'health:001','urn:oasis:names:tc:ebcore:partyid-type:unregistered:cef-scheme', 14,'3ea67be67451e81d1113fba54f5540477','3N7BXKM7GSMFXZJ2VA3Q6B5DLWRMT4PYNEIUYVD6CCZ46J7VIG52A',  NOW(), NOW()),
(31, 'blue-gw','urn:oasis:names:tc:ebcore:partyid-type:unregistered', 31,'1ea67be67451e81d1113fba54f5540477','1N7BXKM7GSMFXZJ2VA3Q6B5DLWRMT4PYNEIUYVD6CCZ46J7VIG52A',  NOW(), NOW()),
(32, '0007:0000000001', 'iso6523-actorid-upis', 11,'f5fdb3f6dab699437c8da5f5652e4410','F7BS7CDQIHQUC2IOQSVF6NZB63CDC6MVOPD7SVDARXOCMIYPEYBQ',  NOW(), NOW());
-- insert disabled participatns
INSERT INTO bdmsl_participant_identifier(id, participant_id, scheme, fk_smp_id, cname_hash, naptr_hash,created_on, last_updated_on, disabled) VALUES
(33, '0007:disabled:0001', 'iso6523-actorid-upis', 34,'686008ff93b48bcf46725003ec23f977','ROR3GUHQAWFJHG6HVIBTXLA4GYHITBXU27DZWHPHDAAS5JER3FJA',  NOW(), NOW(), 1),
(34, '0007:disabled:0002', 'iso6523-actorid-upis', 34,'2b866e4e447d46fdfe7b629fcda57aed','HK77FHVITVLKNHBWE6UZOHQFDFQQNKZ5RJNPXJ3G4GWAZHHA6CRA',  NOW(), NOW(), 1);
