Keys are generated with  (Bind 9.11.3 and Bind 9.18.12 for ED2259 and ED448) using the following commands :

```
dnssec-keygen -a DSA -b 1024 -T KEY -n HOST domisml.edelivery.eu.local
dnssec-keygen -a RSASHA256 -b 4096 -T KEY -n HOST domisml.edelivery.eu.local
dnssec-keygen -a RSASHA512 -b 4096 -T KEY -n HOST domisml.edelivery.eu.local
dnssec-keygen -a ECDSAP256SHA256 -T KEY -n HOST domisml.edelivery.eu.local
dnssec-keygen -a ECDSAP384SHA384 -T KEY -n HOST domisml.edelivery.eu.local
dnssec-keygen -a ED25519 -T KEY -n HOST domisml.edelivery.eu.local
dnssec-keygen -a ED448 -T KEY -n HOST domisml.edelivery.eu.local
```
