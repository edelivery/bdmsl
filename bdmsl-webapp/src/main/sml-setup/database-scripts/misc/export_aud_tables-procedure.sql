
CREATE OR REPLACE PROCEDURE exportAuditRecords (
    offset_for_days INTEGER, delete_after_export BOOLEAN:=false, max_export_record_count INTEGER := 50000)
IS
            -- declare day offset

    day_offset         INTEGER := offset_for_days;
    -- maksimum number of records.
    max_record_count   INTEGER := max_export_record_count;
    -- delete exported records
    delete_records     BOOLEAN := delete_after_export;
    -- min and max revision ids
    rev_min            INTEGER;
    rev_max            INTEGER;
     -- calculated max date
    rev_max_date       DATE;
    -- revision table description
    rev_desc_tab       dbms_sql.desc_tab;
     -- current audit table description
    tbl_desc_tab       dbms_sql.desc_tab;

    -- revision table cursor
    rev_cur            NUMBER;
    rev_res            INTEGER;
    rev_column_count            INTEGER;

    curr_rev_id        VARCHAR2(64);
    tbl_cur            NUMBER;
    tbl_res            NUMBER;
    tbl_cnt            INTEGER;

    -- this is current value var-char  buffer
    value_buffer_varchar          VARCHAR2(32767);
    col_type_nm        VARCHAR2(300);

    -- exporting is done in smaller chunks, The chunk size is defined by this variable
    max_chunk_size   NUMBER(5) := 1000;

    -- audit tables
    TYPE table_varchar_type IS
    TABLE OF VARCHAR2(30);
    -- list of tables for audit export
    audtablelist       table_varchar_type :=table_varchar_type('BDMSL_DNS_RECORD_AUD','BDMSL_CERTIFICATE_DOMAIN_AUD','BDMSL_SUBDOMAIN_AUD','BDMSL_ALLOWED_WILDCARD_AUD'
   ,'BDMSL_CONFIGURATION_AUD','BDMSL_MIGRATE_AUD','BDMSL_CERTIFICATE_AUD','BDMSL_PARTICIPANT_IDENT_AUD','BDMSL_SMP_AUD');

    sql_stmt_delete    VARCHAR2(200);
BEGIN

    dbms_output.enable(buffer_size => NULL);
    dbms_output.put_line('Execute export with parameters: Offset days: ['
                               || day_offset
                               || '], delete records after export: ['
                               ||  CASE delete_records WHEN TRUE THEN 'TRUE' WHEN FALSE THEN 'FALSE' ELSE 'NULL' END
                               || '], maximum count of exported records: ['
                               || max_export_record_count
                               || ']!');

            -- calculate date to export delete records
    rev_max_date := trunc(SYSDATE) - day_offset;
    SELECT
        MIN(id),
        MAX(id)
    INTO
        rev_min,
        rev_max
    FROM
        bdmsl_rev_info
    WHERE
        revision_date < rev_max_date;


    IF ( rev_max IS NULL AND rev_min IS NULL ) THEN
        dbms_output.put_line('There are no records to export for max revision date: ['
                               || rev_max_date
                               || '] (to offset day: sysdate - ['
                               || day_offset
                               || '])!');
        return;
    ELSIF ( ( rev_max - rev_min ) > max_record_count ) THEN
        rev_max := rev_min + max_record_count;
        dbms_output.put_line('Export first  ['
                               || max_record_count
                               || '] records  from revision: ['
                               || rev_min
                               || '] to revision: ['
                               || rev_max
                               || '] for max revision date: ['
                               || rev_max_date
                               || ']!');

    ELSE
        dbms_output.put_line('Export from revision: ['
                               || rev_min
                               || '] to revision: ['
                               || rev_max
                               || '] for max revision date: ['
                               || rev_max_date
                               || ']!');
    END IF;

            -- audit tables
    dbms_output.put_line('The following characters are escaped in all values: " as \", new line - CHR(10) as \n, carriage return  - CHR(13)  as \r');

            -- export in chunks:
            -- each time export is done rev_min is increase by the max_chunk_size
    WHILE rev_min <= rev_max LOOP
                        -- open cursor for revision table
        rev_cur := dbms_sql.open_cursor;
        dbms_sql.parse(rev_cur,'select ID, REVISION_DATE, TIMESTAMP, USERNAME from BDMSL_REV_INFO WHERE ID >= :REV_FROM and ID < :REV_TO and ID <= :REV_TO_MAX ORDER BY id ASC'
       ,dbms_sql.native);
        -- bind variables
        dbms_sql.bind_variable(rev_cur,':REV_FROM',rev_min);
        dbms_sql.bind_variable(rev_cur,':REV_TO', (rev_min + max_chunk_size) );
        dbms_sql.bind_variable(rev_cur,':REV_TO_MAX', rev_max );

                        -- sets the column column count and description for the cursor
        dbms_sql.describe_columns(rev_cur,rev_column_count,rev_desc_tab);

                        -- This procedure defines a column to be selected from the given cursor.
                        -- type value_buffer_varchar defines the extracted type
        FOR i IN 1..rev_column_count LOOP
            dbms_sql.define_column(rev_cur,i,value_buffer_varchar,32767);
        END LOOP;

                        -- fetch the rows
        rev_res := dbms_sql.execute(rev_cur);
        WHILE dbms_sql.fetch_rows(rev_cur) > 0 LOOP
            -- [JSON] start to write json to output.
                                    dbms_output.put('{');
            -- iterate over columns and write column as json: "name": "value"
                                    FOR i IN 1..rev_column_count LOOP

                                                -- [JSON] put revision values
                dbms_output.put('"'
                                  || rev_desc_tab(i).col_name
                                  || '": ');
                                                -- fetch value
                dbms_sql.column_value(rev_cur,i,value_buffer_varchar);
                                                -- [JSON] set value to json format
                dbms_output.put('"'
                                  || replace(replace(replace(value_buffer_varchar,'"','\"'),chr(13),'\\r'),chr(10),'\\n')
                                  || '"');

                                                --  From the upper select the first column is revision id
                                                -- remember the value to fetch values from connected tables
                IF ( i = 1 ) THEN
                    curr_rev_id := value_buffer_varchar;
                END IF;

                                                -- [JSON] add comma separator -
                dbms_output.put(', ');
            END LOOP;

                                    -- export all tables for revision
            FOR tbl IN 1..audtablelist.count LOOP
                tbl_cur := dbms_sql.open_cursor;
                dbms_sql.parse(tbl_cur,'select * from '
                                         || audtablelist(tbl)
                                         || ' WHERE rev = '
                                         || curr_rev_id,dbms_sql.native);

                dbms_sql.describe_columns(tbl_cur,tbl_cnt,tbl_desc_tab);
                FOR ti IN 1..tbl_cnt LOOP
                    dbms_sql.define_column(tbl_cur,ti,value_buffer_varchar,32767);
                END LOOP;
                -- [JSON] write audit table object
                dbms_output.put('"'
                                  || audtablelist(tbl)
                                  || '": {');
                tbl_res := dbms_sql.execute(tbl_cur);
                IF ( dbms_sql.fetch_rows(tbl_cur) > 0 ) THEN
                    FOR tbl_it IN 1..tbl_cnt LOOP
                                                                        -- put revision values
                        dbms_output.put('"'
                                          || tbl_desc_tab(tbl_it).col_name
                                          || '": ');
                        dbms_sql.column_value(tbl_cur,tbl_it,value_buffer_varchar);
                        dbms_output.put('"'
                                          || replace(replace(replace(value_buffer_varchar,'"','\"'),chr(13),'\\r'),chr(10),'\\n')
                                          || '"');

                        IF ( tbl_it < tbl_cnt ) THEN
                            dbms_output.put(', ');
                        END IF;
                    END LOOP;
                END IF;
                dbms_sql.close_cursor(tbl_cur);
                                                -- [JSON] close table object
                dbms_output.put('}');
                IF ( tbl < audtablelist.count ) THEN
                    dbms_output.put(', ');
                END IF;

            END LOOP;

            dbms_output.put('}');
            dbms_output.new_line;
        END LOOP;

        IF delete_records THEN
            FOR tbl IN 1..audtablelist.count LOOP
                sql_stmt_delete := 'DELETE  FROM '
                                   || audtablelist(tbl)
                                   || ' WHERE rev >= '
                                   || rev_min
                                   || 'AND rev <'
                                   || ( rev_min + max_chunk_size )
                                   || 'AND rev <='
                                   || rev_max ; -- rev max is upper limit

                EXECUTE IMMEDIATE sql_stmt_delete;
                COMMIT;
            END LOOP;
            -- delete also main revision table

            sql_stmt_delete := 'DELETE  FROM BDMSL_REV_INFO WHERE id >= '
                               || rev_min
                               || 'AND id <'
                               || ( rev_min + max_chunk_size )
                               || 'AND id <='
                               || rev_max; -- rev max is upper limit

            EXECUTE IMMEDIATE sql_stmt_delete;
            COMMIT;
        END IF;
        -- close cursor and get ready for next bulk

        dbms_sql.close_cursor(rev_cur);
        rev_min := rev_min + max_chunk_size;
    END LOOP;

END;
/
