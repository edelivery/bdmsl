-- 13/10/2020
-- Script exports and deletes exported audit tables for eDelivery BDMSL 4.0.2+.  Audit records are exported by lines in json format.
-- The following characters are escaped:
--  *  '"' as '\"',
--  *  new line - CHR(10) as  '\n',
--  *  carriage return  - CHR(13)  as '\r',
-- Characters in values must be be replaced back for use.
--
--
-- register procedure exportAuditRecords to oracle database
--
-- 1. export - dry-run - only export without deletion
--
--      exec exportAuditRecords(numDaysOffset);
--
-- ex.: export records older than 150 days
--
--      exec exportAuditRecords(150);
--
--
-- 2. export and delete exported records
--
--      exec exportAuditRecords(numDaysOffset, true);
--
-- IMPORTANT NOTE: Be careful also not to overwrite results on next export.
--                    change file SPOOL  "C:\data\bdmsl-export.json on each export!
-- Recommendation : first run script as dry run - just to export data with DELETE_RECORDS=false and  save the file
-- then export with  DELETE_RECORDS=false
--
-- Tools:
-- select to check records per day.
--
--      select trunc(revision_date), trunc(sysdate) - trunc(revision_date),  count(id) from bdmsl_rev_info group by trunc(revision_date) order by  trunc(revision_date) asc
--


SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WRAPPED
-- Turn off all unwanted statement confirmation messages
SET FEEDBACK OFF
--  Set PAGESIZE to zero to suppress all headings, page breaks, titles, the initial blank line, and other formatting information
SET PAGESIZE 0
--  width of the data displayed, Must be higher then expected json entry byte count
SET LINESIZE 20000
-- remove leading spaces..
SET TRIMSPOOL ON


-- SET output file - file is just an example - update it to
SPOOL  "C:\export\data\bdmsl-export_20201030.json"

-- when setting delete/record parameters !use small caps true/false!
exec exportAuditRecords(150, false);

SPOOL OFF