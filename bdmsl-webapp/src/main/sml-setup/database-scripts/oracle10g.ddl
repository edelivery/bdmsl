-- This file was generated by hibernate for BDMSL version 5.0-SNAPSHOT.
create sequence bdmsl_certificate_domain_seq start with 1 increment by  1;
create sequence bdmsl_certificate_seq start with 1 increment by  1;
create sequence bdmsl_dns_rec_seq start with 1 increment by  1;
create sequence bdmsl_participant_ident_seq start with 1 increment by  1;
create sequence bdmsl_rev_info_seq start with 1 increment by  1;
create sequence bdmsl_smp_seq start with 1 increment by  1;
create sequence bdmsl_subdomain_seq start with 1 increment by  1;

    create table bdmsl_allowed_wildcard (
       scheme varchar2(255 char) not null,
        created_on timestamp,
        last_updated_on timestamp,
        fk_certificate_id number(19,0) not null,
        primary key (fk_certificate_id, scheme)
    );

    comment on table bdmsl_allowed_wildcard is
        'This table identifies the SMP with their certificates and map them to schemes for which they can create wildcard records.';

    create table bdmsl_allowed_wildcard_aud (
       fk_certificate_id number(19,0) not null,
        scheme varchar2(255 char) not null,
        REV number(19,0) not null,
        REVTYPE number(3,0),
        created_on timestamp,
        last_updated_on timestamp,
        primary key (fk_certificate_id, scheme, REV)
    );

    create table bdmsl_certificate (
       id number(19,0) not null,
        created_on timestamp,
        last_updated_on timestamp,
        certificate_id varchar2(255 char) not null,
        new_cert_change_date timestamp,
        pem_encoding clob,
        valid_from timestamp not null,
        valid_until timestamp not null,
        new_cert_id number(19,0),
        primary key (id)
    );

    comment on column bdmsl_certificate.certificate_id is
        'The certificate_id is a key composed of the subject and the serial number of the certificate.';

    comment on column bdmsl_certificate.new_cert_change_date is
        'The date of the change for the new certificate. Hour of change is defined in Configuration!';

    comment on column bdmsl_certificate.pem_encoding is
        'PEM encoding for the certificate';

    comment on column bdmsl_certificate.valid_from is
        'Start validity date of the certificate';

    comment on column bdmsl_certificate.valid_until is
        'Expiry date of the certificate';

    create table bdmsl_certificate_aud (
       id number(19,0) not null,
        REV number(19,0) not null,
        REVTYPE number(3,0),
        created_on timestamp,
        last_updated_on timestamp,
        certificate_id varchar2(255 char),
        new_cert_change_date timestamp,
        pem_encoding clob,
        valid_from timestamp,
        valid_until timestamp,
        new_cert_id number(19,0),
        primary key (id, REV)
    );

    create table bdmsl_certificate_domain (
       id number(19,0) not null,
        created_on timestamp,
        last_updated_on timestamp,
        certificate varchar2(255 char) not null,
        crl_url varchar2(1000 char),
        is_admin number(1,0) not null,
        is_root_ca number(1,0) not null,
        pem_encoding clob,
        truststore_alias varchar2(512 char),
        valid_from timestamp,
        valid_until timestamp,
        fk_subdomain_id number(19,0) not null,
        primary key (id)
    );

    comment on table bdmsl_certificate_domain is
        'Table contains domain authorization certificate data for Issuer based authorization and Leaf certificate base authorization.';

    comment on column bdmsl_certificate_domain.certificate is
        'Legacy Certificate id.';

    comment on column bdmsl_certificate_domain.crl_url is
        'URL to the certificate revocation list (CRL)';

    comment on column bdmsl_certificate_domain.is_admin is
        'Can certificate(s) call also the admin services. True only for nonroot certificates';

    comment on column bdmsl_certificate_domain.is_root_ca is
        'Is certificate Root certificate.';

    comment on column bdmsl_certificate_domain.pem_encoding is
        'PEM encoding for the certificate';

    comment on column bdmsl_certificate_domain.truststore_alias is
        'TrustStore alias';

    comment on column bdmsl_certificate_domain.valid_from is
        'Start validity date of the certificate';

    comment on column bdmsl_certificate_domain.valid_until is
        'Expiry date of the certificate';

    create table bdmsl_certificate_domain_aud (
       id number(19,0) not null,
        REV number(19,0) not null,
        REVTYPE number(3,0),
        created_on timestamp,
        last_updated_on timestamp,
        certificate varchar2(255 char),
        crl_url varchar2(1000 char),
        is_admin number(1,0),
        is_root_ca number(1,0),
        pem_encoding clob,
        truststore_alias varchar2(512 char),
        valid_from timestamp,
        valid_until timestamp,
        fk_subdomain_id number(19,0),
        primary key (id, REV)
    );

    create table bdmsl_configuration (
       property varchar2(512 char) not null,
        created_on timestamp,
        last_updated_on timestamp,
        description varchar2(4000 char),
        value varchar2(4000 char),
        primary key (property)
    );

    comment on table bdmsl_configuration is
        'SML configuration properties';

    comment on column bdmsl_configuration.property is
        'Property key/name';

    comment on column bdmsl_configuration.description is
        'Description of the property';

    comment on column bdmsl_configuration.value is
        'Value of the property';

    create table bdmsl_configuration_aud (
       property varchar2(512 char) not null,
        REV number(19,0) not null,
        REVTYPE number(3,0),
        created_on timestamp,
        last_updated_on timestamp,
        description varchar2(4000 char),
        value varchar2(4000 char),
        primary key (property, REV)
    );

    create table bdmsl_dns_record (
       id number(19,0) not null,
        created_on timestamp,
        last_updated_on timestamp,
        dns_zone varchar2(255 char) not null,
        name varchar2(255 char) not null,
        service varchar2(32 char),
        type varchar2(32 char) not null,
        value varchar2(512 char),
        primary key (id)
    );

    comment on table bdmsl_dns_record is
        'Contains custom DNS records';

    comment on column bdmsl_dns_record.id is
        'Surrogate key for DNSRecord Entity';

    comment on column bdmsl_dns_record.dns_zone is
        'Domain (dns zone) of dns server.';

    comment on column bdmsl_dns_record.name is
        'Dns name.';

    comment on column bdmsl_dns_record.service is
        'Service - part of naptr record. If not given (for naptr) default value is: Meta:SMP.';

    comment on column bdmsl_dns_record.type is
        'Record type: A, CNAME, NAPTR.';

    comment on column bdmsl_dns_record.value is
        'Dns Value.  For A type it must be IP address, for CNAME it must valid Domain, for NAPTR it must be regular expresion.';

    create table bdmsl_dns_record_aud (
       id number(19,0) not null,
        REV number(19,0) not null,
        REVTYPE number(3,0),
        created_on timestamp,
        last_updated_on timestamp,
        dns_zone varchar2(255 char),
        name varchar2(255 char),
        service varchar2(32 char),
        type varchar2(32 char),
        value varchar2(512 char),
        primary key (id, REV)
    );

    create table bdmsl_migrate (
       migration_key varchar2(50 char) not null,
        participant_id varchar2(255 char) not null,
        scheme varchar2(255 char) not null,
        created_on timestamp,
        last_updated_on timestamp,
        migrated number(1,0) not null,
        new_smp_id varchar2(64 char),
        old_smp_id varchar2(64 char) not null,
        primary key (migration_key, participant_id, scheme)
    );

    comment on column bdmsl_migrate.migrated is
        'True if the migration is done';

    comment on column bdmsl_migrate.new_smp_id is
        'The id of the SMP after the migration';

    comment on column bdmsl_migrate.old_smp_id is
        'The id of the old SMP (before the migration)';

    create table bdmsl_migrate_aud (
       migration_key varchar2(50 char) not null,
        participant_id varchar2(255 char) not null,
        scheme varchar2(255 char) not null,
        REV number(19,0) not null,
        REVTYPE number(3,0),
        created_on timestamp,
        last_updated_on timestamp,
        migrated number(1,0),
        new_smp_id varchar2(64 char),
        old_smp_id varchar2(64 char),
        primary key (migration_key, participant_id, scheme, REV)
    );

    create table bdmsl_participant_ident_aud (
       id number(19,0) not null,
        REV number(19,0) not null,
        REVTYPE number(3,0),
        created_on timestamp,
        last_updated_on timestamp,
        cname_hash varchar2(255 char),
        disabled number(1,0),
        naptr_hash varchar2(255 char),
        participant_id varchar2(255 char),
        scheme varchar2(255 char),
        fk_smp_id number(19,0),
        primary key (id, REV)
    );

    create table bdmsl_participant_identifier (
       id number(19,0) not null,
        created_on timestamp,
        last_updated_on timestamp,
        cname_hash varchar2(255 char),
        disabled number(1,0) default 0 not null,
        naptr_hash varchar2(255 char),
        participant_id varchar2(255 char) not null,
        scheme varchar2(255 char),
        fk_smp_id number(19,0),
        primary key (id)
    );

    comment on table bdmsl_participant_identifier is
        'Participant identifiers registered on SML';

    comment on column bdmsl_participant_identifier.cname_hash is
        'MD5 hash value of participant identifier for cname value';

    comment on column bdmsl_participant_identifier.disabled is
        'Is the participant disabled. If disabled, participant does not have record in dns.';

    comment on column bdmsl_participant_identifier.naptr_hash is
        'Base32 Sha256 hash value of participant identifier for naptr';

    comment on column bdmsl_participant_identifier.participant_id is
        'The participant identifier';

    comment on column bdmsl_participant_identifier.scheme is
        'The scheme of the participant identifier';

    create table bdmsl_rev_info (
       id number(19,0) not null,
        REVISION_DATE timestamp,
        timestamp number(19,0) not null,
        USERNAME varchar2(255 char),
        primary key (id)
    );

    create table bdmsl_smp (
       id number(19,0) not null,
        created_on timestamp,
        last_updated_on timestamp,
        smp_disabled number(1,0) default 0 not null,
        endpoint_logical_address varchar2(255 char),
        endpoint_physical_address varchar2(255 char),
        smp_id varchar2(64 char) not null,
        fk_certificate_id number(19,0) not null,
        fk_subdomain_id number(19,0) not null,
        primary key (id)
    );

    comment on column bdmsl_smp.id is
        'Surrogate key for SMP entity';

    comment on column bdmsl_smp.smp_disabled is
        'Is the SMP disabled. If disabled, no new participant can be registered on the SMP.';

    comment on column bdmsl_smp.endpoint_logical_address is
        'URL of SMP server. Value is used for generating NAPTR and domain from URL for CNAME records.';

    comment on column bdmsl_smp.endpoint_physical_address is
        'The physical address of the endpoint server.';

    comment on column bdmsl_smp.smp_id is
        'SMP identifier used by SMP instance to manage data';

    create table bdmsl_smp_aud (
       id number(19,0) not null,
        REV number(19,0) not null,
        REVTYPE number(3,0),
        created_on timestamp,
        last_updated_on timestamp,
        smp_disabled number(1,0),
        endpoint_logical_address varchar2(255 char),
        endpoint_physical_address varchar2(255 char),
        smp_id varchar2(64 char),
        fk_certificate_id number(19,0),
        fk_subdomain_id number(19,0),
        primary key (id, REV)
    );

    create table bdmsl_subdomain (
       subdomain_id number(19,0) not null,
        created_on timestamp,
        last_updated_on timestamp,
        description varchar2(1024 char),
        dns_record_types varchar2(128 char) not null,
        dns_zone varchar2(512 char) not null,
        domain_max_participant_count number(19,0),
        smp_max_participant_count number(19,0),
        participant_id_regexp varchar2(1024 char) not null,
        smp_ia_cert_policy_oids varchar2(1024 char),
        smp_ia_cert_regexp varchar2(255 char),
        smp_url_schemas varchar2(255 char) not null,
        subdomain_name varchar2(255 char) not null,
        primary key (subdomain_id)
    );

    comment on table bdmsl_subdomain is
        'SML handle multiple business domains. This table contains domain specific data';

    comment on column bdmsl_subdomain.description is
        'Domain (short) description.';

    comment on column bdmsl_subdomain.dns_record_types is
        'Type of DNS Record when registering/updating participant, all means that both DNS record types are accepted as possible values: [cname, naptr, all].';

    comment on column bdmsl_subdomain.dns_zone is
        'Domain (dns zone) for subdomain.';

    comment on column bdmsl_subdomain.domain_max_participant_count is
        'Maximum number of participant allowed to be registered on the domain';

    comment on column bdmsl_subdomain.smp_max_participant_count is
        'Maximum number of participant allowed to be registered on the SMP';

    comment on column bdmsl_subdomain.participant_id_regexp is
        'Regular expression for the participant identifier validation.';

    comment on column bdmsl_subdomain.smp_ia_cert_policy_oids is
        'User with issuer-authorized SMP certificate is granted SMP_ROLE only if one of the certificate policy extension matches the list. Value is a list of certificate policy OIDs separated by ,.';

    comment on column bdmsl_subdomain.smp_ia_cert_regexp is
        'User with issuer-authorized SMP certificate is granted SMP_ROLE only if its certificate Subject DN matches configured regexp.';

    comment on column bdmsl_subdomain.smp_url_schemas is
        'Protocol that MUST be used for LogicalAddress when registering new SMP, all means both protocols are accepted possible values: [ http, https, all].';

    comment on column bdmsl_subdomain.subdomain_name is
        'subdomain name - part of domain';

    create table bdmsl_subdomain_aud (
       subdomain_id number(19,0) not null,
        REV number(19,0) not null,
        REVTYPE number(3,0),
        created_on timestamp,
        last_updated_on timestamp,
        description varchar2(1024 char),
        dns_record_types varchar2(128 char),
        dns_zone varchar2(512 char),
        domain_max_participant_count number(19,0),
        smp_max_participant_count number(19,0),
        participant_id_regexp varchar2(1024 char),
        smp_ia_cert_policy_oids varchar2(1024 char),
        smp_ia_cert_regexp varchar2(255 char),
        smp_url_schemas varchar2(255 char),
        subdomain_name varchar2(255 char),
        primary key (subdomain_id, REV)
    );

    alter table bdmsl_certificate 
       add constraint UK_o3sq5i83tx8amcur4g4oet95w unique (certificate_id);

    alter table bdmsl_dns_record 
       add constraint sml_dns_record_idx unique (type, name, value);
create index SML_PARTC_IDENT_ID_IDX on bdmsl_participant_identifier (participant_id);
create index SML_PARTC_IDENT_SCH_IDX on bdmsl_participant_identifier (scheme);

    alter table bdmsl_participant_identifier 
       add constraint SML_PARTC_IDENT_NKEY_IDX unique (participant_id, scheme, fk_smp_id);

    alter table bdmsl_smp 
       add constraint UK_f7ofiybgngnf5h9dfe4eurtcj unique (smp_id);

    alter table bdmsl_subdomain 
       add constraint UK_jkvfepsihj6crc4cd7cnhrljc unique (subdomain_name);

    alter table bdmsl_allowed_wildcard 
       add constraint FK8adg88l4w3pno7yg7h8x5s7ix 
       foreign key (fk_certificate_id) 
       references bdmsl_certificate;

    alter table bdmsl_allowed_wildcard_aud 
       add constraint FKsnoxb79lxxpgfd2m3mjmsw8mr 
       foreign key (REV) 
       references bdmsl_rev_info;

    alter table bdmsl_certificate 
       add constraint FKinjtivxspwkk8evqch3wx961x 
       foreign key (new_cert_id) 
       references bdmsl_certificate;

    alter table bdmsl_certificate_aud 
       add constraint FKhn0pxsby9h6nph5uwe45f8bqf 
       foreign key (REV) 
       references bdmsl_rev_info;

    alter table bdmsl_certificate_domain 
       add constraint FKmqsugy77t3egjaifyvv4bqowm 
       foreign key (fk_subdomain_id) 
       references bdmsl_subdomain;

    alter table bdmsl_certificate_domain_aud 
       add constraint FKe1jqwwrmqxh1qg3j4j838hw6f 
       foreign key (REV) 
       references bdmsl_rev_info;

    alter table bdmsl_configuration_aud 
       add constraint FK86tlud8xce1x5oeg200okcv3v 
       foreign key (REV) 
       references bdmsl_rev_info;

    alter table bdmsl_dns_record_aud 
       add constraint FKciofgw2b2jvv0tqsg2q7uyh4m 
       foreign key (REV) 
       references bdmsl_rev_info;

    alter table bdmsl_migrate_aud 
       add constraint FKqicrt1pfwntojycm43bfo9xof 
       foreign key (REV) 
       references bdmsl_rev_info;

    alter table bdmsl_participant_ident_aud 
       add constraint FKn1o91k0h87bxq4eo3kl8sgob5 
       foreign key (REV) 
       references bdmsl_rev_info;

    alter table bdmsl_participant_identifier 
       add constraint FK8ftyd77rtcanves77tp0jswrx 
       foreign key (fk_smp_id) 
       references bdmsl_smp;

    alter table bdmsl_smp 
       add constraint FKn5xnvpr86joyfsyam39y01o4c 
       foreign key (fk_certificate_id) 
       references bdmsl_certificate;

    alter table bdmsl_smp 
       add constraint FKpcty91ma3jiqrr6w0iix723u4 
       foreign key (fk_subdomain_id) 
       references bdmsl_subdomain;

    alter table bdmsl_smp_aud 
       add constraint FKi5af66kb7j5jv86auhbg4hfl9 
       foreign key (REV) 
       references bdmsl_rev_info;

    alter table bdmsl_subdomain_aud 
       add constraint FKqxihyf6otws3f0tbflgomvic4 
       foreign key (REV) 
       references bdmsl_rev_info;
