ALTER TABLE bdmsl_configuration MODIFY COLUMN  value varchar(4000)  CHARACTER SET utf8 COLLATE utf8_bin comment 'Value of the property';
ALTER TABLE bdmsl_participant_identifier ADD disabled bit default 0 not null comment 'Is the participant disabled. If disabled, participant does not have record in dns.';
ALTER TABLE bdmsl_participant_ident_aud ADD disabled bit;
ALTER TABLE bdmsl_smp ADD smp_disabled bit default 0 not null comment 'Is the SMP disabled. If disabled, no new participant can be registered on the SMP.';
ALTER TABLE bdmsl_smp_aud ADD smp_disabled bit;
ALTER TABLE bdmsl_smp DROP CONSTRAINT UK_g54wnbn1l9n88fhcfwpijqoqf;
ALTER TABLE bdmsl_smp ADD CONSTRAINT UK_f7ofiybgngnf5h9dfe4eurtcj unique (smp_id);
