
-- UPDATE BDMSL_CERTIFICATE_DOMAIN
ALTER TABLE bdmsl_certificate_domain DROP PRIMARY KEY;
ALTER TABLE bdmsl_certificate_domain ADD id bigint;
ALTER TABLE bdmsl_certificate_domain ADD truststore_alias varchar(512) CHARACTER SET utf8 COLLATE utf8_bin comment 'TrustStore alias';

SET @incrCertDomId = 0;
UPDATE bdmsl_certificate_domain SET id =  @incrCertDomId:=@incrCertDomId+1 WHERE id is null;
commit;

ALTER TABLE bdmsl_certificate_domain ADD PRIMARY KEY (id);
ALTER TABLE bdmsl_certificate_domain MODIFY COLUMN id bigint NOT NULL auto_increment;

ALTER TABLE bdmsl_certificate_domain ADD CONSTRAINT UK_9l12yk7pnp8yo3rijgsgdg01y UNIQUE (certificate);
ALTER TABLE bdmsl_certificate_domain ADD CONSTRAINT UK_i88g5xcymgn1pd3lk8vt5o24p UNIQUE (truststore_alias);

-- UPDATE BDMSL_CERTIFICATE_DOMAIN_AUD
ALTER TABLE bdmsl_certificate_domain_aud DROP PRIMARY KEY;
ALTER TABLE bdmsl_certificate_domain_aud ADD id bigint;
ALTER TABLE bdmsl_certificate_domain_aud ADD truststore_alias varchar(512)  CHARACTER SET utf8 COLLATE utf8_bin comment 'TrustStore alias';
ALTER TABLE bdmsl_certificate_domain_aud ADD PRIMARY KEY (id, REV);

-- UPDATE BDMSL_SMP
ALTER TABLE bdmsl_smp MODIFY COLUMN fk_certificate_id bigint not null;
ALTER TABLE bdmsl_smp MODIFY COLUMN fk_subdomain_id bigint not null;

-- UPDATE BDMSL_SUBDOMAIN
ALTER TABLE bdmsl_subdomain ADD smp_ia_cert_regexp varchar(255) CHARACTER SET utf8 COLLATE utf8_bin comment 'User with issuer-authorized smp certificate is granted SMP_ROLE only if its certificates Subject matches configured regexp';

ALTER TABLE bdmsl_subdomain_aud ADD smp_ia_cert_regexp varchar(255)  CHARACTER SET utf8 COLLATE utf8_bin;
commit;