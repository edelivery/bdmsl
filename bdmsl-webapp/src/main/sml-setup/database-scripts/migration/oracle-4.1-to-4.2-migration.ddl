-- Migration script from BDMSL 4.1 to BDMSL 4.2

ALTER TABLE bdmsl_subdomain
ADD (
    domain_max_participant_count number(19,0),
    smp_max_participant_count number(19,0),
    smp_ia_cert_policy_oids varchar2(1024 char)
);


ALTER TABLE bdmsl_subdomain_aud
ADD (
    domain_max_participant_count number(19,0),
    smp_max_participant_count number(19,0),
    smp_ia_cert_policy_oids varchar2(1024 char)
);


comment on column bdmsl_subdomain.domain_max_participant_count is
    'Maximum number of participant allowed to be registered on the domain';

comment on column bdmsl_subdomain.smp_max_participant_count is
    'Maximum number of participant allowed to be registered on the SMP';

comment on column bdmsl_subdomain.participant_id_regexp is
    'Regular expression for the participant identifier validation.';

comment on column bdmsl_subdomain.smp_ia_cert_policy_oids is
    'User with issuer-authorized SMP certificate is granted SMP_ROLE only if one of the certificate policy extension matches the list. Value is a list of certificate policy OIDs separated by comma.';


