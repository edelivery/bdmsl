ALTER TABLE bdmsl_configuration MODIFY value NULL;
ALTER TABLE bdmsl_participant_identifier ADD disabled number(1,0) default 0 not null;
ALTER TABLE bdmsl_participant_ident_aud ADD disabled number(1,0);
ALTER TABLE bdmsl_smp ADD smp_disabled number(1,0) default 0 not null;
ALTER TABLE bdmsl_smp_aud ADD smp_disabled number(1,0);
ALTER TABLE bdmsl_smp DROP CONSTRAINT UK_g54wnbn1l9n88fhcfwpijqoqf;
ALTER TABLE bdmsl_smp ADD CONSTRAINT UK_f7ofiybgngnf5h9dfe4eurtcj unique (smp_id);

comment on column bdmsl_participant_identifier.disabled is 'Is the participant disabled. If disabled, participant does not have record in dns.';
comment on column bdmsl_smp.smp_disabled is 'Is the SMP disabled. If disabled, no new participant can be registered on the SMP.';
