-- Migration script from BDMSL 4.1 to BDMSL 4.2


-- UPDATE bdmsl_subdomain
ALTER TABLE bdmsl_subdomain ADD domain_max_participant_count bigint comment 'Maximum number of participant allowed to be registered on the domain';
ALTER TABLE bdmsl_subdomain ADD smp_max_participant_count bigint comment 'Maximum number of participant allowed to be registered on the SMP';
ALTER TABLE bdmsl_subdomain ADD smp_ia_cert_policy_oids varchar(1024) CHARACTER SET utf8 COLLATE utf8_bin comment 'User with issuer-authorized SMP certificate is granted SMP_ROLE only if one of the certificate policy extension matches the list. Value is a list of certificate policy OIDs separated by comma.';


-- UPDATE bdmsl_subdomain
ALTER TABLE bdmsl_subdomain_aud ADD domain_max_participant_count bigint;
ALTER TABLE bdmsl_subdomain_aud ADD smp_max_participant_count bigint;
ALTER TABLE bdmsl_subdomain_aud ADD smp_ia_cert_policy_oids varchar(1024) CHARACTER SET utf8 COLLATE utf8_bin;

