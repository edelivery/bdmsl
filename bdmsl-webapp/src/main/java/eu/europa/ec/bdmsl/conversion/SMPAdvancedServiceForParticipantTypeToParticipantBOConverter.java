/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2017 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.conversion;

import ec.services.wsdl.bdmsl.data._1.SMPAdvancedServiceForParticipantService;
import eu.europa.ec.bdmsl.common.bo.ParticipantBO;
import org.busdox.servicemetadata.locator._1.ServiceMetadataPublisherServiceForParticipantType;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import static org.apache.commons.lang3.StringUtils.trim;

/**
 * This converter class transforms SMPAdvancedServiceForParticipantType objects into ParticipantBO objects.
 * Note: WS Type to BO is convert manually and strings are trimmed!
 *
 * @author Thomas Dussart
 * @since 24/05/2024
 */
@Component
public class SMPAdvancedServiceForParticipantTypeToParticipantBOConverter extends AutoRegisteringConverter<SMPAdvancedServiceForParticipantService, ParticipantBO> {

    public SMPAdvancedServiceForParticipantTypeToParticipantBOConverter(ConversionService conversionService) {
        super(conversionService);
    }

    @Override
    public ParticipantBO convert(SMPAdvancedServiceForParticipantService source) {
        if (source == null) {
            return null;
        }

        ParticipantBO target = new ParticipantBO();
        if (source.getCreateParticipantIdentifier() != null) {
            ServiceMetadataPublisherServiceForParticipantType smpForPartc = source.getCreateParticipantIdentifier();
            target.setSmpId(trim(smpForPartc.getServiceMetadataPublisherID()));

            if (source.getCreateParticipantIdentifier().getParticipantIdentifier() != null) {
                target.setParticipantId(trim(source.getCreateParticipantIdentifier().getParticipantIdentifier().getValue()));
                target.setScheme(trim(source.getCreateParticipantIdentifier().getParticipantIdentifier().getScheme()));
            }
        }
        target.setType(trim(source.getServiceName()));

        return target;
    }
}
