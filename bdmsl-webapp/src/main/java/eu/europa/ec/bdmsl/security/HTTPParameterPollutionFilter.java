package eu.europa.ec.bdmsl.security;
/*
 * (C) Copyright 2024 - European Commission | CEF eDelivery
 * <p>
 * Licensed under the EUPL, Version 1.2 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * EUPL.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12, https://joinup.ec.europa.eu/sites/default/files/custom-page/attachment/eupl_v1.2_en.pdf
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;

/**
 * Remove having twice a parameter with the same name to avoir HTTP parameter pollution.
 *
 * @author Thomas Dussart
 * @since 2024
 */
@Component
public class HTTPParameterPollutionFilter implements Filter {

    private static final Logger LOG = LoggerFactory.getLogger(HTTPParameterPollutionFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        LOG.debug("Initializing HTTPParameterPollutionFilter");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        LOG.debug("Starting HTTPParameterPollutionFilter for request:[{}]", httpRequest.getRequestURI());
        try {
            checkForParameterPollution(httpRequest);
            chain.doFilter(request, response);
        } catch (ServletException e) {
            LOG.error("Security issue detected in request parameters", e);
            throw e;
        }
        LOG.debug("HTTPParameterPollutionFilter processing completed for request: [{}]", httpRequest.getRequestURI());
    }

    @Override
    public void destroy() {
        LOG.debug("Destroying HTTPParameterPollutionFilter");
    }

    private void checkForParameterPollution(HttpServletRequest request) throws ServletException {
        Map<String, String[]> parameterMap = request.getParameterMap();
        for (Map.Entry<String, String[]> entry : parameterMap.entrySet()) {
            String paramName = entry.getKey();
            String[] paramValues = entry.getValue();
            LOG.debug("Checking parameter: {} with values: {}", paramName, paramValues);

            if (paramValues.length > 1) {
                LOG.error("HTTP parameter pollution detected for parameter: [{}] with values: [{}]", paramName, paramValues);
                throw new ServletException("HTTP parameter pollution detected for parameter: " + paramName);
            }
        }
    }
}
