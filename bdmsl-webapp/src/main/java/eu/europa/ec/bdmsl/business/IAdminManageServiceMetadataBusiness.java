/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.business;

import eu.europa.ec.bdmsl.common.bo.ServiceMetadataPublisherBO;

import eu.europa.ec.bdmsl.common.enums.AdminSMPManageActionEnum;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;

/**
 * Administrator methods for managing the SMP instances.
 */
public interface IAdminManageServiceMetadataBusiness {


    /**
     * Method validates if SMP-ID is valid and if identifier exists in the database. If smpId is owned by the certificateId
     * and if it belongs to given domainZone.
     *
     * @param smpId smp identifier to be validated
     * @param certificateOwnerId  for validation of the certificate identifier to be associated to the smpId
     * @param domainZone for validation that SMP belongs to of the domain zone
     * @throws TechnicalException if technical error occurs
     */
    ServiceMetadataPublisherBO validateSMPIdentifier(String smpId,  String certificateOwnerId, String domainZone) throws
             TechnicalException;

    /**
     * Method validates action data for the SMP instance.
     * @param action action to be performed
     * @param smpBO SMP instance
     * @param smpAddress smp address
     * @param smpLogicalAddress smp logical address / url
     * @throws TechnicalException if technical error occurs
     */
    void validateTaskData(AdminSMPManageActionEnum action, ServiceMetadataPublisherBO  smpBO, String smpAddress, String smpLogicalAddress) throws
            TechnicalException;
}
