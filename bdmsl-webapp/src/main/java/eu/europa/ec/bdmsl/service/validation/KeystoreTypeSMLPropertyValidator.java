/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.validation;

import eu.europa.ec.bdmsl.common.enums.SMLPropertyEnum;
import eu.europa.ec.bdmsl.common.exception.BadConfigurationException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.io.File;

/**
 * A validator that handles {@code SMLPropertyEnum.KEYSTORE_TYPE} and {@code SMLPropertyEnum.TRUSTSTORE_TYPE} values, throwing {@code SMLPropertyValidationException} exceptions
 * @since 5.0
 * @author Sebastian-Ion TINCU
 */
@Component
@Order(1)
public class KeystoreTypeSMLPropertyValidator implements SMLPropertyValidator {

    @Override
    public void validate(SMLPropertyEnum property, String value, File configDirectory) throws BadConfigurationException {
        switch (property) {
            case KEYSTORE_TYPE:
                // fallthrough
            case TRUSTSTORE_TYPE:
                String finalValue = StringUtils.trim(value);
                if (!StringUtils.equalsAnyIgnoreCase(finalValue, "PKCS12", "JKS")) {
                    throw new BadConfigurationException("Property: " + property.getProperty() + " doesn't have the required value (PKCS12, JKS): " + value);
                }
                break;
            default:
                // do nothing
                break;

        }
    }

    @Override
    public boolean supports(SMLPropertyEnum property) {
        return SMLPropertyEnum.KEYSTORE_TYPE.equals(property)
                || SMLPropertyEnum.TRUSTSTORE_TYPE.equals(property);
    }
}
