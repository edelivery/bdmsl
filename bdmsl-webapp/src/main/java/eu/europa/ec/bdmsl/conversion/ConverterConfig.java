/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2017 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.conversion;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Primary;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.converter.ConverterRegistry;
import org.springframework.core.convert.support.DefaultConversionService;

/**
 * This converter configuration class.
 * <p/>
 *
 * @author Thomas Dussart
 * @since 24/05/2024
 */
@Configuration
@EnableAspectJAutoProxy
public class ConverterConfig {


    @Bean
    @Primary
    public DefaultConversionService conversionService() {
        DefaultConversionService conversionService = new DefaultConversionService();
        registerConverters(conversionService);
        return conversionService;
    }

    private void registerConverters(ConverterRegistry registry) {
        registry.addConverter(subdomainEntityToSubdomainBOConverter());
        registry.addConverter(subdomainBOToSubdomainEntityConverter());
        registry.addConverter(configurationEntityToConfigurationBOConverter());
        registry.addConverter(dnsRecordEntityToDNSRecordBOConverter());
    }

    @Bean
    public Converter subdomainEntityToSubdomainBOConverter() {
        return new SubdomainEntityToSubdomainBOConverter();
    }

    @Bean
    public Converter subdomainBOToSubdomainEntityConverter() {
        return new SubdomainBOToSubdomainEntityConverter();
    }

    @Bean
    public Converter configurationEntityToConfigurationBOConverter() {
        return new ConfigurationEntityToConfigurationBOConverter();
    }

    @Bean
    public Converter dnsRecordEntityToDNSRecordBOConverter() {
        return new DNSRecordEntityToDNSRecordBOConverter();
    }

    @Bean
    public Converter smpEntityToServiceMetadataPublisherBOConverter() {
        return new SmpEntityToServiceMetadataPublisherBOConverter(conversionService());
    }

    @Bean
    public Converter serviceMetadataPublisherBOToSmpEntityConverter() {
        return new ServiceMetadataPublisherBOToSmpEntityConverter(conversionService());
    }

    @Bean
    public Converter certificateDomainEntityToCertificateDomainBOConverter() {
        return new CertificateDomainEntityToCertificateDomainBOConverter(conversionService());
    }

    @Bean
    public Converter certificateBOToCertificateEntityConverter() {
        return new CertificateBOToCertificateEntityConverter(conversionService());
    }

    @Bean
    public Converter certificateEntityToCertificateBOConverter() {
        return new CertificateEntityToCertificateBOConverter(conversionService());
    }

    @Bean
    public Converter participantIdentifierEntityToParticipantBOConverter() {
        return new ParticipantIdentifierEntityToParticipantBOConverter(conversionService());
    }

    @Bean
    public Converter participantBOToParticipantIdentifierEntityConverter() {
        return new ParticipantBOToParticipantIdentifierEntityConverter(conversionService());
    }

    @Bean
    public Converter migrateEntityToMigrationRecordBOConverter() {
        return new MigrateEntityToMigrationRecordBOConverter(conversionService());
    }

    @Bean
    public Converter allowedWildcardEntityToWildcardBOConverter() {
        return new AllowedWildcardEntityToWildcardBOConverter(conversionService());
    }

    @Bean
    public Converter serviceMetadataPublisherBOToServiceMetadataPublisherServiceTypeConverter() {
        return new ServiceMetadataPublisherBOToServiceMetadataPublisherServiceTypeConverter(conversionService());
    }

    @Bean
    public Converter pageRequestTypeToPageRequestBOConverter() {
        return new PageRequestTypeToPageRequestBOConverter(conversionService());
    }

    @Bean
    public Converter serviceMetadataPublisherServiceTypeToServiceMetadataPublisherBOConverter() {
        return new ServiceMetadataPublisherServiceTypeToServiceMetadataPublisherBOConverter(conversionService());
    }

    @Bean
    public Converter serviceMetadataPublisherServiceForParticipantTypeToParticipantBOConverter() {
        return new ServiceMetadataPublisherServiceForParticipantTypeToParticipantBOConverter(conversionService());
    }

    @Bean
    public Converter participantIdentifierTypeToParticipantBOConverter() {
        return new ParticipantIdentifierTypeToParticipantBOConverter(conversionService());
    }

    @Bean
    public Converter smpAdvancedServiceForParticipantTypeToParticipantBOConverter() {
        return new SMPAdvancedServiceForParticipantTypeToParticipantBOConverter(conversionService());
    }

    @Bean
    public Converter participantIdentifierPageTypeToParticipantListBOConverter() {
        return new ParticipantIdentifierPageTypeToParticipantListBOConverter(conversionService());
    }

    @Bean
    public Converter participantsTypeToParticipantBOConverter() {
        return new ParticipantsTypeToParticipantBOConverter(conversionService());
    }

    @Bean
    public Converter participantListBOToParticipantIdentifierPageTypeConverter() {
        return new ParticipantListBOToParticipantIdentifierPageTypeConverter(conversionService());
    }

    @Bean
    public Converter participantBOToParticipantIdentifierTypeConverter() {
        return new ParticipantBOToParticipantIdentifierTypeConverter(conversionService());
    }

    @Bean
    public Converter participantListBOToListParticipantsTypeConverter() {
        return new ParticipantListBOToListParticipantsTypeConverter(conversionService());
    }

    @Bean
    public Converter migrationRecordBOToMigrationRecordTypeConverter() {
        return new MigrationRecordBOToMigrationRecordTypeConverter(conversionService());
    }

    @Bean
    public Converter migrationRecordTypeToMigrationRecordBOConverter() {
        return new MigrationRecordTypeToMigrationRecordBOConverter(conversionService());
    }

    @Bean
    public Converter certificateDomainBOToDomainCertificateTypeConverter() {
        return new CertificateDomainBOToDomainCertificateTypeConverter(conversionService());
    }

    @Bean
    public Converter domainCertificateTypeToCertificateDomainBOConverter() {
        return new DomainCertificateTypeToCertificateDomainBOConverter(conversionService());
    }

    @Bean
    public Converter prepareChangeCertificateBOToPrepareChangeCertificateConverter() {
        return new PrepareChangeCertificateBOToPrepareChangeCertificateTypeConverter(conversionService());
    }

    @Bean
    public Converter ChangeCertificateToChangeCertificateBOConverter() {
        return new ChangeCertificateTypeToChangeCertificateBOConverter(conversionService());
    }

    @Bean
    public Converter subDomainTypeToSubdomainBOConverter() {
        return new SubDomainTypeToSubdomainBOConverter(conversionService());
    }

    @Bean
    public Converter subdomainBOToSubDomainTypeConverter() {
        return new SubdomainBOToSubDomainTypeConverter(conversionService());
    }

    @Bean
    public Converter dnsRecordBOToDNSRecordTypeConverter() {
        return new DNSRecordBOToDNSRecordTypeConverter(conversionService());
    }

    @Bean
    public Converter dnsRecordTypeToDNSRecordBOConverter() {
        return new DNSRecordTypeToDNSRecordBOConverter(conversionService());
    }

    @Bean
    public Converter configurationBOToPropertyTypeConverter() {
        return new ConfigurationBOToPropertyTypeConverter(conversionService());
    }

    @Bean
    public Converter propertyTypeToConfigurationBOConverter() {
        return new PropertyTypeToConfigurationBOConverter(conversionService());
    }

    @Bean
    public Converter truststoreCertificateBOToTruststoreCertificateTypeConverter() {
        return new TruststoreCertificateBOToTruststoreCertificateTypeConverter(conversionService());
    }

    @Bean
    public Converter truststoreCertificateTypeToTruststoreCertificateBOConverter() {
        return new TruststoreCertificateTypeToTruststoreCertificateBOConverter(conversionService());
    }

}
