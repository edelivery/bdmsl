/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.enums;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.Optional;
import java.util.regex.Pattern;

import static eu.europa.ec.bdmsl.common.enums.SMLPropertyTypeEnum.*;
import static eu.europa.ec.bdmsl.common.util.Constant.LOCAL_HOST;

public enum SMLPropertyEnum {

    CONFIGURATION_DIR("configurationDir", "./", true, "The absolute path to the folder containing all the configuration files (keystore and sig0 key)", false, PATH),
    ENCRYPTION_FILENAME("encriptionPrivateKey", "encriptionPrivateKey.private", true, "Name of the 256 bit AES secret key to encrypt or decrypt passwords.", false, FILENAME),
    MONITOR_TOKEN("adminPassword", "$2a$10$9RzbkquhBYRkHUoKMTNZhOPJmevTbUKWf549MEiCWUd.1LdblMhBi", false, "BCrypt Hashed password to monitor is-alive service", false, STRING),
    AUTH_BLUE_COAT_ENABLED("authentication.bluecoat.enabled", Boolean.FALSE.toString(), true, "Enable/Disable Client-Cert header authentication. Possible values: true/false. ", false, BOOLEAN),
    AUTH_SSLCLIENTCERT_ENABLED("authentication.sslclientcert.enabled", Boolean.FALSE.toString(), true, "Enable/Disable SSLClientCert header authentication. Possible values: true/false. ", false, BOOLEAN),

    AUTH_SMP_CERT_REGEXP("authorization.smp.certSubjectRegex", "^.*(CN=SMP_|OU=PEPPOL TEST SMP).*$", true, "User with ROOT-CA is granted SMP_ROLE only if its certificates Subject matches configured regexp", false, REGEXP),
    AUTH_LEGACY_ENABLED("authorization.domain.legacy.enabled", "true", true, "Allow domain legacy authorization, based on certificate string.", false, BOOLEAN),
    AUTH_UNSEC_LOGIN("unsecureLoginAllowed", Boolean.FALSE.toString(), true, "true if the use of HTTPS is not required. If the values is set to true, then the user unsecure-http-client is automatically created. Possible values: true/false", false, BOOLEAN),

    CERT_CHANGE_CRON("certificateChangeCronExpression", "0 0 2 ? * *", true, "Cron expression for the changeCertificate job. Example: 0 0 2 ? * * (everyday at 2:00 am)", false, CRON_EXPRESSION),

    CERT_VERIFICATION_GRACEFUL("cert.revocation.validation.graceful", "true", true, "Graceful validation of certificate revocation. If URL retrieving does not succeed, do not throw error!", false, BOOLEAN),
    CRL_ALLOWED_PROTOCOLS("cert.revocation.validation.crl.protocols", "http://,https://", true, "Allowed crl/OCSP protocols for fetching the CRL list or submitting OCSP requests.", false, STRING),
    CERT_VERIFICATION_STRATEGY("cert.revocation.validation.strategy", "CRL_ONLY", true, "Certificate validation strategy. Default: OCSP_CRL (OCSP first, CRL second if OCSP fails). Possible values: OCSP_CRL, CRL_OCSP, OCSP_ONLY,CRL_ONLY,NO_VALIDATION.", false, STRING),

    DIA_CRON("dataInconsistencyAnalyzer.cronJobExpression", "0 0 3 ? * *", true, "Cron expression for dataInconsistencyChecker job. Example: 0 0 3 ? * * (everyday at 3:00 am)", false, CRON_EXPRESSION),
    DIA_MAIL_TO("dataInconsistencyAnalyzer.recipientEmail", "email@domain.com", true, "Email address to receive Data Inconsistency Checker results", false, EMAIL),
    DIA_MAIL_FROM("dataInconsistencyAnalyzer.senderEmail", "automated-notifications@nsome-mail.eu", true, "Sender email address for reporting Data Inconsistency Analyzer.", false, EMAIL),
    DIA_GENERATE_SERVER("dataInconsistencyAnalyzer.serverInstance", LOCAL_HOST, true, "If sml.cluster.enabled is set to true then then instance (hostname) to generate report. ", false, STRING),

    MAIL_SERVER_HOST("mail.smtp.host", "mail.server.com", true, "Email server - configuration for submitting the emails.", false, STRING),
    MAIL_SERVER_PORT("mail.smtp.port", "25", true, "Smtp mail port - configu1ration for submitting the emails.", false, INTEGER),
    MAIL_SERVER_PROTOCOL("mail.smtp.protocol", "smtp", true, "smtp mail protocol- configuration for submitting the emails.", false, STRING),
    MAIL_SERVER_USERNAME("mail.smtp.username", "", false, "smtp mail protocol- username for submitting the emails.", false, STRING),
    MAIL_SERVER_PASSWORD("mail.smtp.password", "", false, "smtp mail protocol - encrypted password for submitting the emails.", true, STRING),
    MAIL_SERVER_PROPERTIES("mail.smtp.properties", "", false, "smtp mail ;-separated properties: ex: mail.smtp.auth:true;mail.smtp.starttls.enable:true;mail.smtp.quitwait:false.", false, STRING),

    DNS_SIG0_ENABLED("dnsClient.SIG0Enabled", Boolean.FALSE.toString(), true, "true if the SIG0 signing is enabled. Possible values: true/false", false, BOOLEAN),
    DNS_SIG0_KEY_FILENAME("dnsClient.SIG0KeyFileName", "SIG0.private", false, "The actual SIG0 key file. Should be just the filename if the file is in the classpath or in the configurationDir", false, FILENAME),
    DNS_SIG0_PKEY_NAME("dnsClient.SIG0PublicKeyName", "sig0.acc.edelivery.tech.ec.europa.eu.", false, "The public key name of the SIG0 key", false, STRING),
    DNS_TSIG_ENABLED("dnsClient.TSIGEnabled", Boolean.FALSE.toString(), true, "true if the TSIG signing is enabled.  Possible values: true/false", false, BOOLEAN),
    DNS_TSIG_KEY_NAME("dnsClient.TSIGKeyName", "acc.edelivery.tech.ec.europa.eu", false, "The name of the shared key e.g. acc.edelivery.tech.ec.europa.eu", false, STRING),
    DNS_TSIG_KEY_VALUE("dnsClient.TSIGKeyValue", "", false, "Base64 encrypted shared secret for TSIG.", true, STRING),
    DNS_TSIG_HASH_ALGORITHM("dnsClient.TSIGAlgorithm", "hmac-sha256", false, "The RFC8945 algorithm name of the shared key. The supported values are:hmac-md5,hmac-sha1,hmac-sha224,hmac-sha256,hmac-sha384,hmac-sha512.", false, STRING),

    DNS_ENABLED("dnsClient.enabled", Boolean.FALSE.toString(), true, "true if registration of DNS records is required. Must be true in production. Possible values: true/false", false, BOOLEAN),
    DNS_PUBLISHER_PREFIX("dnsClient.publisherPrefix", "publisher", false, "This is the prefix for the publishers (SMP). This is to be concatenated with the associated DNS domain in the table bdmsl_certificate_domain", false, STRING),
    DNS_SERVER("dnsClient.server", "dns.server.host.local", false, "The DNS server", false, STRING),
    DNS_SHOW_ENTRIES("dnsClient.show.entries", "true", false, "if true then service ListDNS transfer and show the DNS entries. (Not recommended for large zones)  Possible values: true/false", false, BOOLEAN),
    DNS_TCP_TIMEOUT("dnsClient.tcp.timeout", "60", false, "DNS TCP timeout in seconds. If the value is not given then tcp timeout is set to default value 60s!", false, INTEGER),

    PROXY_ENABLED("useProxy", Boolean.FALSE.toString(), true, "true if a proxy is required to connect to the internet. Possible values: true/false", false, BOOLEAN),
    HTTP_PROXY_HOST("httpProxyHost", LOCAL_HOST, false, "The http proxy host", false, STRING),
    HTTP_NO_PROXY_HOSTS("httpNoProxyHosts", "localhost|127.0.0.1", true, "list of nor proxy hosts. Ex.: localhost|127.0.0.1", false, STRING),
    HTTP_PROXY_PASSWORD("httpProxyPassword", " ", false, "Base64 encrypted password for Proxy.", true, STRING),
    HTTP_PROXY_PORT("httpProxyPort", "8012", false, "The http proxy port", false, INTEGER),
    HTTP_PROXY_USER("httpProxyUser", "user", false, "The proxy user", false, STRING),

    SIGNATURE_ALIAS("keystoreAlias", "senderalias", true, "The alias in the keystore for signing responses.", false, STRING),
    KEYSTORE_FILENAME("keystoreFileName", "keystore.jks", true, "The keystore file. Should be just the filename if the file is in the classpath or in the configurationDir", false, FILENAME),
    KEYSTORE_TYPE("keystoreType", "JKS", true, "The keystore type. Possible values: JKS/PKCS12.", false, STRING),
    KEYSTORE_PASSWORD("keystorePassword", "", false, "Base64 encrypted password for Keystore.", true, STRING),
    KEYSTORE_PASSWORD_DECRYPTED("keystorePassword.decrypted", "", false, "Only for backup purposes. This password was automatically created. Store password somewhere save and delete this entry!", false, STRING),
    TRUSTSTORE_FILENAME("truststoreFileName", "truststore.p12", true, "The PKCS12 truststore filename. The file name must be in the configurationDir!", false, FILENAME),
    TRUSTSTORE_TYPE("truststoreType", "PKCS12", true, "The truststore type. Possible values: JKS/PKCS12.", false, STRING),
    TRUSTSTORE_PASSWORD("truststorePassword", "", false, "Base64 encrypted password for Truststore.", true, STRING),
    TRUSTSTORE_PASSWORD_DECRYPTED("truststorePassword.decrypted", "", false, "Only for backup purposes. This password was automatically created. Store password somewhere save and delete this entry!", false, STRING),

    PARTY_IDENTIFIER_URN_PATTERN("partyIdentifier.splitPattern", "",
            false, "Regular expression with groups <scheme> and <identifier> for splitting the URN identifiers to scheme and identifier part as example: '^(?i)\\s*?(?<scheme>urn:ehealth:partyid-type)::?(?<identifier>.+)?\\s*$'!", false, REGEXP),
    PARTY_IDENTIFIER_SCHEME_MANDATORY("partyIdentifier.scheme.mandatory", Boolean.FALSE.toString(), false, "Scheme for participant identifier is mandatory",
           false, BOOLEAN),

    PARTY_IDENTIFIER_CASESENSITIVE_SCHEMES("partyIdentifier.scheme.caseSensitive", "sensitive-participant-sc1|sensitive-participant-sc2",
            false, "Specifies | separated scheme list of participant identifiers that must be considered CASE-SENSITIVE.",
            false, LIST_STRING),

    SML_PROPERTY_REFRESH_CRON("sml.property.refresh.cronJobExpression", "0 53 */1 * * *", true, "Property refresh cron expression (def 7 minutes to each hour)!", false, CRON_EXPRESSION),
    PAGE_SIZE("paginationListRequest", "100", true, "Number of participants per page for the list operation of ManageParticipantIdentifier service. This property is used for pagination purposes.", false, INTEGER),
    SIGN_RESPONSE("signResponse", Boolean.FALSE.toString(), true, "true if the responses must be signed. Possible values: true/false", false, BOOLEAN),
    SIGN_RESPONSE_ALGORITHM("signResponseAlgorithm", "", false, "The signature algorithm to use when signing responses. Examples: 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha256', 'http://www.w3.org/2021/04/xmldsig-more#eddsa-ed25519', ...", false, STRING),
    SIGN_RESPONSE_DIGEST_ALGORITHM("signResponseDigestAlgorithm", "http://www.w3.org/2001/04/xmlenc#sha256", false, "The signature digest algorithm to use when signing responses. Examples: 'http://www.w3.org/2001/04/xmlenc#sha256', 'http://www.w3.org/2001/04/xmlenc#sha512'", false, STRING),
    SMP_CHANGE_MAX_PARTC_SIZE("smp.update.max.part.size", "10000", true, "Max number of participants on SMP which are automatically updated/deleted by SMP.", false, INTEGER),
    BDMSL_CLUSTER_ENABLED("sml.cluster.enabled", "true", false, "Define if application is set in cluster. In not cluster environment, properties are updated on setProperty.", false, BOOLEAN),

    REPORT_EXPIRED_CERT_CRON("report.expiredSMPCertificates.cron", "0 22 6 ? * *", true, "Cron expression for expired SMP certificates job. Example: 0 22 6 ? * * (everyday at 3:00 am)", false, CRON_EXPRESSION),
    REPORT_EXPIRED_CERT_MAIL_TO("report.expiredSMPCertificates.recipientEmail", "email@domain.com", true, "Email address to receive expired SMP certificates report", false, EMAIL),
    REPORT_EXPIRED_CERT_MAIL_FROM("report.expiredSMPCertificates.senderEmail", "automated-notifications@nsome-mail.eu", true, "Sender email address for expired SMP certificates report.", false, EMAIL),
    REPORT_EXPIRED_CERT_GENERATE_SERVER("report.expiredSMPCertificates.serverInstance", LOCAL_HOST, true, "If sml.cluster.enabled is set to true then then instance (hostname) to generate report. ", false, STRING),

    /**
     * The HTTP Strict-Transport-Security response header (often abbreviated as HSTS) informs browsers
     * that the site should only be accessed using HTTPS, and that any future attempts to access it using HTTP
     * should automatically be converted to HTTPS.
     */
    HSTS_ENABLED("hsts.enabled", Boolean.FALSE.toString(), false, "Enable/Disable HTTP Strict Transport Security (HSTS) header. Possible values: true/false.", false, BOOLEAN),
    HSTS_MAX_AGE("hsts.max.age", "31536000", false, "HTTP Strict Transport Security (HSTS) header max age parameter. Possible values: Timeout in seconds.", false, INTEGER  ),
    HSTS_INCLUDE_SUBDOMAIN("hsts.include.subdomain", Boolean.TRUE.toString(), false, "If this optional parameter is specified, this rule applies to all of the site's subdomains as well.", false, BOOLEAN  ),
    HSTS_PRELOAD("hsts.preload", Boolean.TRUE.toString(), false, "When using preload, the max-age directive must be at least 31536000 (1 year), and the includeSubDomains directive must be present.", false, BOOLEAN);

    private final String property;
    private final String defValue;
    private final String desc;

    private final boolean isEncrypted;
    private final boolean isMandatory;
    private final SMLPropertyTypeEnum propertyType;

    private final Pattern valuePattern;
    private final String errorValueMessage;

    SMLPropertyEnum(String property, String defValue, boolean isMandatory, String desc, boolean isEncrypted, SMLPropertyTypeEnum propertyType) {
        this(property, defValue, isMandatory, desc, isEncrypted, propertyType, propertyType.getDefValidationRegExp(), propertyType.getMessage(property));
    }

    SMLPropertyEnum(String property, String defValue, boolean isMandatory,
                    String desc, boolean isEncrypted, SMLPropertyTypeEnum propertyType,
                    String valuePattern, String errorValueMessage) {
        this.property = property;
        this.defValue = defValue;
        this.desc = desc;
        this.isEncrypted = isEncrypted;
        this.propertyType = propertyType;
        this.isMandatory = isMandatory;
        this.valuePattern = Pattern.compile(valuePattern);
        this.errorValueMessage = errorValueMessage;
    }

    public String getProperty() {
        return property;
    }

    public String getDefValue() {
        return defValue;
    }

    public String getDesc() {
        return desc;
    }

    public boolean isEncrypted() {
        return isEncrypted;
    }

    public boolean isMandatory() {
        return isMandatory;
    }

    public SMLPropertyTypeEnum getPropertyType() {
        return propertyType;
    }

    public Pattern getValuePattern() {
        return valuePattern;
    }

    public String getErrorValueMessage() {
        return this.errorValueMessage;
    }

    public static Optional<SMLPropertyEnum> getByProperty(String key) {
        String keyTrim = StringUtils.trimToNull(key);
        if (keyTrim == null) {
            return Optional.empty();
        }
        return Arrays.stream(values()).filter(val -> val.getProperty().equalsIgnoreCase(keyTrim)).findAny();
    }
}


