/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.bo;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/*
 * @author Joze RIHTARSIC
 * @since 4.1
 */
public class TruststoreCertificateBO extends AbstractBusinessObject {
    String alias;
    protected byte[] certificatePublicKey;

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public byte[] getCertificatePublicKey() {
        return certificatePublicKey;
    }

    public void setCertificatePublicKey(byte[] certificatePublicKey) {
        this.certificatePublicKey = certificatePublicKey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        TruststoreCertificateBO that = (TruststoreCertificateBO) o;

        return new EqualsBuilder()
                .append(alias, that.alias)
                .append(certificatePublicKey, that.certificatePublicKey)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(alias)
                .append(certificatePublicKey)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "TruststoreCertificateBO{" +
                "alias='" + alias + '\'' +
                '}';
    }
}
