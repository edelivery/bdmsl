/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.security;

import eu.europa.ec.bdmsl.business.IConfigurationBusiness;
import eu.europa.ec.edelivery.security.cert.ProxyDataCallback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Purpose of the class is dynamically provide the proxy data to {@link HttpCrlConnectionService}.
 *
 * @author Joze RIHTARSIC
 * @since 4.1
 */
@Component
public class HttpCrlProxyDataService implements ProxyDataCallback {

    IConfigurationBusiness configurationBusiness;

    @Autowired
    public HttpCrlProxyDataService(IConfigurationBusiness configurationBusiness) {
        this.configurationBusiness = configurationBusiness;
    }

    @Override
    public String getUsername() {
        return configurationBusiness.getHttpProxyUsername();
    }

    @Override
    public String getSecurityToken() {
        return configurationBusiness.getHttpProxyPassword();
    }

    @Override
    public String getHttpProxyHost() {
        return configurationBusiness.getHttpProxyHost();
    }

    @Override
    public Integer getHttpProxyPort() {
        return configurationBusiness.getHttpProxyPort();
    }

    @Override
    public String getHttpNoProxyHosts() {
        return configurationBusiness.getHttpNoProxyHosts();
    }

    @Override
    public boolean isProxyEnabled() {
        return configurationBusiness.isProxyEnabled();
    }

    @Override
    public List<String> getAllowedURLProtocols() {
        return configurationBusiness.getCertRevocationValidationAllowedUrlProtocols();
    }
}
