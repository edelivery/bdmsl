/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.dns.impl;

import eu.europa.ec.bdmsl.business.ICertificateDomainBusiness;
import eu.europa.ec.bdmsl.business.IConfigurationBusiness;
import eu.europa.ec.bdmsl.business.ISubdomainBusiness;
import eu.europa.ec.bdmsl.common.bo.*;
import eu.europa.ec.bdmsl.common.enums.DNSRecordEnum;
import eu.europa.ec.bdmsl.common.enums.DNSSubSomainRecordTypeEnum;
import eu.europa.ec.bdmsl.common.exception.*;
import eu.europa.ec.bdmsl.common.logging.ILoggingService;
import eu.europa.ec.bdmsl.common.util.LogEvents;
import eu.europa.ec.bdmsl.common.util.StringUtil;
import eu.europa.ec.bdmsl.security.CertificateAuthentication;
import eu.europa.ec.bdmsl.security.CertificateDetails;
import eu.europa.ec.bdmsl.security.UnsecureAuthentication;
import eu.europa.ec.bdmsl.service.dns.IDnsClientService;
import eu.europa.ec.bdmsl.service.dns.IDnsMessageSenderService;
import eu.europa.ec.bdmsl.service.dns.ISIG0KeyProviderService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.xbill.DNS.*;
import org.xbill.DNS.Record;

import java.util.*;

import static eu.europa.ec.bdmsl.common.util.Constant.FAILED_TO_BUILD_DNS_NAME_FROM;

/**
 * Implementation of DNS client service for creating, updating and deleting DNS records.
 * The implementation uses DNS Java library to interact with DNS server creating the
 * nsupdate requests <a href='https://datatracker.ietf.org/doc/html/rfc2136'>RFC 2136</a>
 *
 * @author Adrien FERIAL
 * @author Joze RIHTARSIC
 * @since 1.0
 */
@Service
public class DnsClientServiceImpl implements IDnsClientService {
    private static final Logger LOG = LoggerFactory.getLogger(DnsClientServiceImpl.class);

    private static final int DNS_PARTICIPANT_MESSAGE_SIZE = 100;
    private static final int MAX_RETRY = 5;
    public static final String FOR_IS_ALIVE = " FOR IS ALIVE";

    @Autowired
    private ILoggingService loggingService;

    @Autowired
    private ICertificateDomainBusiness certificateDomainBusiness;

    @Autowired
    private ISubdomainBusiness subdomainBusiness;

    @Autowired
    private IDnsMessageSenderService dnsMessageSenderService;

    @Autowired
    private ISIG0KeyProviderService sig0KeyProviderService;

    @Autowired
    private IConfigurationBusiness configurationBusiness;

    @Autowired
    private DnsRecordManager dnsRecordManager;



    @Override
    public Map<String, Collection<Record>> getAllParticipantRecords(String participant, String scheme) throws TechnicalException {
        Map<String, Collection<Record>> recordMap = new HashMap<>();
        if (configurationBusiness.isDNSEnabled()) {
            String trimParticipant = StringUtils.trim(participant);
            String trimScheme = StringUtils.trim(scheme);

            List<SubdomainBO> subdomainBOList = subdomainBusiness.findAll();
            for (SubdomainBO subdomainBO : subdomainBOList) {
                List<Record> records = new ArrayList<>();
                Name cnameDomain = dnsRecordManager.generateParticipantCNAME(trimParticipant, trimScheme, subdomainBO.getSubdomainName());
                Name naptrDomain = dnsRecordManager.generateParticipantNAPTR(trimParticipant, trimScheme, subdomainBO.getSubdomainName());
                List<Record> cnameResult = lookupAny(cnameDomain.toString());
                List<Record> naptrResult = lookupAny(naptrDomain.toString());
                if (!cnameResult.isEmpty()) {
                    records.addAll(cnameResult);
                }
                if (!naptrResult.isEmpty()) {
                    records.addAll(naptrResult);
                }
                recordMap.put(subdomainBO.getSubdomainName(), records);
            }
        }
        return recordMap;
    }

    @Override
    public List<Record> getAllRecords(String dnsZone) throws TechnicalException {
        // do zone transfer to get complete list..
        String dnsZoneName = StringUtil.addFinalDot(dnsZone);

        final ZoneTransferIn xfr;
        List<Record> records;
        try {
            xfr = ZoneTransferIn.newAXFR(Name.fromString(dnsZoneName),
                    configurationBusiness.getDNSServer(),
                    null);

            xfr.run();
            records = xfr.getAXFR();
        } catch (Exception exc) {
            loggingService.error(exc.getMessage(), exc);
            throw new DNSClientException("Impossible to retrieve all records for zone " + dnsZone, exc);
        }

        return records;
    }

    @Override
    public int getDNSDataForInconsistencyReport(String publisherName, String dnsZone, IRDnsRangeData smpData, IRDnsRangeData cnameData, IRDnsRangeData naptrData) throws TechnicalException {

        // do zone transfer to get complete list..
        String dnsZoneName = StringUtil.addFinalDot(dnsZone);
        int iRecords;
        final ZoneTransferIn xfr;
        try {
            InconsistencyAXFRZoneTransferHandler transferHandler
                    = new InconsistencyAXFRZoneTransferHandler(dnsZone, publisherName, smpData, cnameData, naptrData);

            xfr = ZoneTransferIn.newAXFR(Name.fromString(dnsZoneName), configurationBusiness.getDNSServer(), null);
            xfr.run(transferHandler);
            iRecords = transferHandler.getAllRecordCount();

        } catch (Exception exc) {
            loggingService.error(exc.getMessage(), exc);
            throw new DNSClientException("Impossible to retrieve all records for zone " + dnsZone, exc);
        }

        return iRecords;
    }

    @Override
    public int getAllDNSDataForInconsistencyReport(String publisherName, IRDnsRangeData smpData, IRDnsRangeData cnameData, IRDnsRangeData naptrData) throws TechnicalException {
        // get all zones
        List<String> dnsZones = getDistDomainZones();
        // get records!
        int iRecords = 0;
        for (String domain : dnsZones) {
            iRecords += getDNSDataForInconsistencyReport(publisherName, domain, smpData, cnameData, naptrData);
        }

        return iRecords;
    }

    public List<String> getDistDomainZones() throws TechnicalException {
        List<SubdomainBO> subdomainBOList = subdomainBusiness.findAll();
        List<String> dnsZones = new ArrayList<>();
        subdomainBOList.forEach(sc -> {
            if (!dnsZones.contains(sc.getDnsZone())) {
                dnsZones.add(sc.getDnsZone());
            }
        });
        return dnsZones;
    }

    @Override
    public void createDNSRecordsForSMP(ServiceMetadataPublisherBO smpBO) throws TechnicalException {
        LOG.debug("Creating DNS records for SMP [{}]", smpBO);
        DnsZone dnsZoneName = getDnsZoneNameForSubDomain(smpBO.getSubdomain());
        final Update dnsUpdate = getUpdateObject(dnsZoneName.getDomain());

        Record smpRecord = dnsRecordManager.createSMP(smpBO.getSmpId(), dnsZoneName.getSubdomain(), smpBO.getLogicalAddress(), configurationBusiness.getDNSPublisherPrefix());


        LOG.debug("Creating in the DNS for SMP [{}]", smpRecord.getName());
        // Delete old host(if exists!) and the new one this adds "ANY ANY" record to nsupdate message -
        dnsUpdate.delete(smpRecord.getName());
        dnsUpdate.add(smpRecord);

        sendAndValidateMessage(dnsUpdate);

        if (smpRecord instanceof CNAMERecord) {
            loggingService.businessLog(LogEvents.BUS_CNAME_RECORD_FOR_SMP_CREATED, smpBO.getSmpId(), smpRecord.toString());
        }
        if (smpRecord instanceof ARecord) {
            loggingService.businessLog(LogEvents.BUS_A_RECORD_FOR_SMP_CREATED, smpBO.getSmpId(), smpRecord.toString());
        }
    }

    @Override
    public void createDNSRecordsForParticipant(ParticipantBO participantBO, ServiceMetadataPublisherBO smpBO) throws TechnicalException {
        this.createDNSRecordsForParticipants(Collections.singletonList(participantBO), smpBO);
    }

    @Override
    public void updateSMPDNSRecord(ServiceMetadataPublisherBO smpBO) throws
            TechnicalException {
        this.createDNSRecordsForSMP(smpBO);
    }

    @Override
    public void synchronizeDNSRecordsForSMP(ServiceMetadataPublisherBO smpBO, List<ParticipantBO> participants) throws
            TechnicalException {
        // get all records from participant
        SubdomainBO subdomainBO = smpBO.getSubdomain();
        SupportedDnsRecordType domainRecordsTypes = SupportedDnsRecordType.getByCode(subdomainBO.getDnsRecordTypes());
        DnsZone dnsZoneName = getDnsZoneNameForSubDomain(subdomainBO);
        final Update dnsUpdate = getUpdateObject(dnsZoneName.getDomain());
        for (ParticipantBO participantBO : participants) {
            buildSynchronizeDNSRecordsForParticipant(participantBO, dnsUpdate, smpBO, domainRecordsTypes, dnsZoneName);
        }
        sendAndValidateMessage(dnsUpdate);
    }


    protected void buildSynchronizeDNSRecordsForParticipant(ParticipantBO participantBO,
                                                    Update dnsUpdate,
                                                    ServiceMetadataPublisherBO smpBO,
                                                    SupportedDnsRecordType domainRecordsTypes,
                                                    DnsZone dnsZoneName) throws TechnicalException {


        // get all records for participant
        Name cnameRecord =  dnsRecordManager.generateParticipantCNAME(participantBO.getParticipantId(),  participantBO.getScheme(), dnsZoneName.getSubdomain());
        Name naptrRecord =  dnsRecordManager.generateParticipantNAPTR(participantBO.getParticipantId(),  participantBO.getScheme(),  dnsZoneName.getSubdomain());

        boolean cnameExist = recordExists(cnameRecord.toString(), Type.CNAME);
        boolean naptrExist = recordExists(naptrRecord.toString(), Type.NAPTR);

        if (!cnameExist && domainRecordsTypes.isCNameEnabled()) {
            Record cnameAdd = dnsRecordManager.createParticipantCNAME(participantBO.getParticipantId(), participantBO.getScheme(),
                    dnsZoneName.getSubdomain(),
                    participantBO.getSmpId(),
                    configurationBusiness.getDNSPublisherPrefix());
            dnsUpdate.add(cnameAdd);
        }

        if (cnameExist && !domainRecordsTypes.isCNameEnabled()) {
            dnsUpdate.delete(cnameRecord);
        }

        if (!naptrExist && domainRecordsTypes.isNaptrEnabled()) {
            Record naptrAdd = dnsRecordManager.createParticipantNAPTR(participantBO.getParticipantId(),
                    participantBO.getScheme(), dnsZoneName.getSubdomain(),
                    participantBO.getType(),
                    smpBO.getLogicalAddress());
            dnsUpdate.add(naptrAdd);
        }

        if (naptrExist && !domainRecordsTypes.isNaptrEnabled()) {
            dnsUpdate.delete(naptrRecord);
        }
    }



    @Override
    public void updateDNSRecordsForSMP(ServiceMetadataPublisherBO smpBO, List<ParticipantBO> participants, boolean isLogicalAddressToBeUpdated) throws
            TechnicalException {
        this.createDNSRecordsForSMP(smpBO);

        if (isLogicalAddressToBeUpdated) {
            this.createDNSRecordsForParticipants(participants, smpBO, SupportedDnsRecordType.NAPTR); // UPDATE NAPTR ONLY - LOGICAL ADDRESS
        }
    }

    @Override
    public void updateDNSRecordsForParticipant(ParticipantBO participantBO, ServiceMetadataPublisherBO smpBO) throws TechnicalException {
        this.createDNSRecordsForParticipant(participantBO, smpBO);
    }

    @Override
    public void deleteDNSRecordsForParticipants(List<ParticipantBO> participantBOList, ServiceMetadataPublisherBO smpBO) throws TechnicalException {
        deleteDNSRecordsForParticipants(participantBOList, smpBO, false);
    }

    private void deleteDNSRecordsForParticipants(List<ParticipantBO> participantBOList, ServiceMetadataPublisherBO smpBO, boolean isAlive) throws TechnicalException {
        DnsZone dnsZoneName = isAlive ? getDnsZoneNameIsAlive() : getDnsZoneNameForSubDomain(smpBO.getSubdomain());
        final Update dnsUpdate = getUpdateObject(dnsZoneName.getDomain());

        for (ParticipantBO participantBO : participantBOList) {
            deleteParticipantFromDNS(dnsUpdate, dnsZoneName, participantBO);
        }
        sendAndValidateMessage(dnsUpdate);
    }

    private void deleteParticipantFromDNS(final Update dnsUpdate, DnsZone dnsZoneName, ParticipantBO participantBO, SupportedDnsRecordType... dnsRecordTypes) throws TechnicalException {
        if (dnsRecordTypes == null || dnsRecordTypes.length == 0) {
            dnsRecordTypes = SupportedDnsRecordType.values();
        }

        for (SupportedDnsRecordType dnsRecordType : dnsRecordTypes) {
            switch (dnsRecordType) {
                case CNAME:
                    final Name participantCnameHost = dnsRecordManager.generateParticipantCNAME(participantBO.getParticipantId(), participantBO.getScheme(), dnsZoneName.getSubdomain());
                    dnsUpdate.delete(participantCnameHost);
                    break;
                case NAPTR:
                    final Name participantNaptrHost = dnsRecordManager.generateParticipantNAPTR(participantBO.getParticipantId(), participantBO.getScheme(), dnsZoneName.getSubdomain());
                    dnsUpdate.delete(participantNaptrHost);
                    break;
                case ALL:
                    LOG.debug("Invalid/Unsupported record type: [{}]", SupportedDnsRecordType.ALL);
                    break;
            }
        }
    }

    @Override
    public ParticipantDnsRecord createEntriesIsAliveDNS(ParticipantBO participantBO, String smpLogicalAddress) throws TechnicalException {
        try {
            DnsZone dnsZoneName = getDnsZoneNameIsAlive();
            final Update dnsUpdate = getUpdateObject(dnsZoneName.getDomain());
            SupportedDnsRecordType supportedDnsRecordType = getDnsRecordTypes(dnsZoneName.getSubdomain());

            if (supportedDnsRecordType == null) {
                String subdomain = dnsZoneName.getSubdomain();
                throw new DNSClientException(String.format("The property dnsClient.recordTypes.%s was not found in the database for the domain %s.", subdomain, subdomain));
            }

            SupportedDnsRecordType[] dnsTypesToBeCreated = supportedDnsRecordType == SupportedDnsRecordType.ALL ? new SupportedDnsRecordType[]{SupportedDnsRecordType.CNAME, SupportedDnsRecordType.NAPTR} : new SupportedDnsRecordType[]{supportedDnsRecordType};
            ParticipantDnsRecord created = createDNSRecordsForParticipant(dnsZoneName, dnsUpdate, participantBO, smpLogicalAddress, dnsTypesToBeCreated);
            sendAndValidateMessage(dnsUpdate);
            return created;
        } catch (TechnicalException tex) {
            loggingService.error(tex.getMessage(), tex);
            throw tex;
        } catch (Exception exc) {
            loggingService.error(exc.getMessage(), exc);
            throw new DNSClientException("Impossible to write on DNS", exc);
        }
    }

    @Override
    public void createDNSRecordsForParticipants(List<ParticipantBO> participantBOList, ServiceMetadataPublisherBO smpBO, SupportedDnsRecordType... dnsTypesToBeCreated) throws TechnicalException {

        DnsZone dnsZoneName = getDnsZoneNameForSubDomain(smpBO.getSubdomain());

        Update dnsUpdate = getUpdateObject(dnsZoneName.getDomain());
        SupportedDnsRecordType supportedDnsRecordType = getDnsRecordTypes(dnsZoneName.getSubdomain());

        if (supportedDnsRecordType == null) {
            String subdomain = dnsZoneName.getSubdomain();
            throw new DNSClientException(String.format("The property dnsClient.recordTypes.%s was not found in the database for the domain %s.", subdomain, subdomain));
        }

        List<ParticipantBO> participantToBeCreated = new ArrayList<>();
        for (ParticipantBO participantBO : participantBOList) {
            participantToBeCreated.add(participantBO);

            try {

                if (dnsTypesToBeCreated == null || dnsTypesToBeCreated.length == 0) {
                    dnsTypesToBeCreated = supportedDnsRecordType == SupportedDnsRecordType.ALL ? new SupportedDnsRecordType[]{SupportedDnsRecordType.NAPTR, SupportedDnsRecordType.CNAME} : new SupportedDnsRecordType[]{supportedDnsRecordType};
                }
                createDNSRecordsForParticipant(dnsZoneName, dnsUpdate, participantBO, smpBO.getLogicalAddress(), dnsTypesToBeCreated);

                if (participantToBeCreated.size() == DNS_PARTICIPANT_MESSAGE_SIZE) {
                    participantToBeCreated.clear();
                    sendAndValidateMessage(dnsUpdate);
                    dnsUpdate = getUpdateObject(dnsZoneName.getDomain());
                }

            } catch (TechnicalException exc) {
                loggingService.error(exc.getMessage(), exc);
                throw exc;
            } catch (Exception exc) {
                loggingService.error(exc.getMessage(), exc);
                throw new DNSClientException(exc.getMessage());
            }
        }
        if (!participantToBeCreated.isEmpty()) {
            sendAndValidateMessage(dnsUpdate);
        }
    }

    private ParticipantDnsRecord createDNSRecordsForParticipant(DnsZone dnsZoneName, Update dnsUpdate,
                                                                ParticipantBO participantBO,
                                                                String smpLogicalAddress,
                                                                SupportedDnsRecordType... dnsRecordTypes) throws TechnicalException {
        Record cnameRecord = null;
        Record naptrRecord = null;

        for (SupportedDnsRecordType supportedDnsRecordType : dnsRecordTypes) {
            switch (supportedDnsRecordType) {
                case CNAME:
                    cnameRecord = dnsRecordManager.createParticipantCNAME(participantBO.getParticipantId(), participantBO.getScheme(), dnsZoneName.getSubdomain(), participantBO.getSmpId(), configurationBusiness.getDNSPublisherPrefix());
                    createParticipantRecordIntoDNS(cnameRecord, dnsUpdate, dnsZoneName, participantBO, LogEvents.BUS_CNAME_RECORD_FOR_PARTICIPANT_CREATED, supportedDnsRecordType);
                    break;
                case NAPTR:
                    naptrRecord = dnsRecordManager.createParticipantNAPTR(participantBO.getParticipantId(), participantBO.getScheme(), dnsZoneName.getSubdomain(), participantBO.getType(), smpLogicalAddress);
                    createParticipantRecordIntoDNS(naptrRecord, dnsUpdate, dnsZoneName, participantBO, LogEvents.BUS_NAPTR_RECORD_FOR_PARTICIPANT_CREATED, supportedDnsRecordType);
                    break;
                case ALL:
                    LOG.debug("Invalid/Unsupported record type: [{}]", SupportedDnsRecordType.ALL);
                    break;
            }
        }
        return new ParticipantDnsRecord(cnameRecord, naptrRecord);
    }

    private void createParticipantRecordIntoDNS(Record record, Update
            dnsUpdate, DnsZone dnsZoneName, ParticipantBO participantBO, String logEvent, SupportedDnsRecordType... dnsRecordTypes) throws
            TechnicalException {

        // Delete old host - if exists!
        deleteParticipantFromDNS(dnsUpdate, dnsZoneName, participantBO, dnsRecordTypes);

        LOG.debug("Creating Record in the DNS: [{}]", record.getName());
        dnsUpdate.add(record);
        loggingService.businessLog(logEvent, participantBO.getParticipantId(), record.toString());
    }

    @Override
    public void deleteDNSRecordsForParticipant(ParticipantBO participantBO, ServiceMetadataPublisherBO smpBO) throws TechnicalException {
        this.deleteDNSRecordsForParticipants(Collections.singletonList(participantBO), smpBO, false);
    }

    @Override
    public void deleteDNSRecordsForParticipantIsAlive(ParticipantBO participantBO) throws
            TechnicalException {
        this.deleteDNSRecordsForParticipants(Collections.singletonList(participantBO), null, true);
    }

    @Override
    public void deleteDNSRecordsForSMP(ServiceMetadataPublisherBO smpBO) throws TechnicalException {
        DnsZone dnsZoneName = getDnsZoneNameForSubDomain(smpBO.getSubdomain());
        final Update dnsUpdate = getUpdateObject(dnsZoneName.getDomain());
        final Name publisherHost = dnsRecordManager.generateSmpCNAME(smpBO.getSmpId(), dnsZoneName.getSubdomain(), configurationBusiness.getDNSPublisherPrefix());
        dnsUpdate.delete(publisherHost);
        sendAndValidateMessage(dnsUpdate);
    }

    private Update getUpdateObject(String dnsZoneName) throws DNSClientException {
        final Update dnsUpdate;
        try {
            dnsUpdate = new Update(Name.fromString(dnsZoneName));
        } catch (TextParseException exc) {
            throw new DNSClientException(FAILED_TO_BUILD_DNS_NAME_FROM + dnsZoneName + "'", exc);
        }
        return dnsUpdate;
    }

    private String addDot(String domain) {
        // we need the full qualified dns-name
        if (!domain.endsWith(".")) {
            domain += '.';
        }
        return domain;
    }

    @Override
    public DnsZone getDnsZoneNameIsAlive() throws TechnicalException {
        // user first
        List<SubdomainBO> lstSB = subdomainBusiness.findAll();
        if (lstSB.isEmpty()) {
            throw new GenericTechnicalException("Impossible to get domain to be used");
        }

        String dnsDomain = lstSB.get(0).getDnsZone();
        return new DnsZone(addDot(dnsDomain), addDot(dnsDomain));
    }

    @Override
    public DnsZone getDnsZoneName() throws TechnicalException {

        CertificateDomainBO certificateDomainBO = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof CertificateAuthentication) {
            certificateDomainBO = ((CertificateAuthentication) authentication).getGrantedSubDomainAnchor();
        } else if (authentication instanceof UnsecureAuthentication) {
            certificateDomainBO = certificateDomainBusiness.findDomain(((CertificateDetails) authentication.getDetails()));
        }
        return getDnsZoneName(certificateDomainBO);
    }

    @Override
    public DnsZone getDnsZoneName(CertificateDomainBO certificateDomainBO) throws TechnicalException {
        if (certificateDomainBO == null) {
            throw new DNSClientException("Certificate domain can not be null");
        }
        if (certificateDomainBO.getSubdomain() == null) {
            throw new DNSClientException("DNS client subdomain must not be null");
        }
        return getDnsZoneNameForSubDomain(certificateDomainBO.getSubdomain());
    }

    public DnsZone getDnsZoneNameForSubDomain(SubdomainBO subdomainBO) throws TechnicalException {
        // retrieve the DNS zone name
        String dnsSubDomain = subdomainBO.getSubdomainName();
        if (dnsSubDomain == null) {
            throw new DNSClientException("DNS client subdomain name must not be null");
        }

        String dnsDomain = subdomainBO.getDnsZone();
        if (StringUtils.isBlank(dnsDomain)) {
            throw new DNSClientException(String.format("DNS zone for subdomain [%s] must be configured in the database.", dnsSubDomain));
        }
        return new DnsZone(StringUtil.addFinalDot(dnsDomain), StringUtil.addFinalDot(dnsSubDomain));
    }

    @Override
    public void verifyDNSAccess() throws TechnicalException {
        synchronized (DnsClientServiceImpl.class) {
            boolean dnsClientEnabled = configurationBusiness.isDNSEnabled();
            if (dnsClientEnabled) {
                loggingService.debug("Start verifying dns access");

                // create a Participant
                String participantId = Long.toString(System.currentTimeMillis());
                String scheme = "iso6523-actorid-upis";
                String smpId = "IS_ALIVE_BDMSL_SELF_CHECK_SMP_" + participantId;
                String smpLogicalAddress = "http://isAlive.edelivery.com";

                ParticipantBO isAliveParticipant = new ParticipantBO(participantId, smpId, scheme);

                // add participant on DNS
                loggingService.debug("Creating participant: " + isAliveParticipant + FOR_IS_ALIVE);
                ParticipantDnsRecord dnsRecordsForParticipantIsAlive = createEntriesIsAliveDNS(isAliveParticipant, smpLogicalAddress);

                try {
                    DnsClientServiceImpl.class.wait(5000); // NOSONAR
                } catch (Exception e) {
                    loggingService.error("Rolling back dns operation - deleting participant " + isAliveParticipant + FOR_IS_ALIVE, e);
                    deleteEntriesIsAliveDNS(isAliveParticipant);
                    throw new DNSClientException("Delay exception while waiting for DNS propagation", e);
                }

                try {
                    loggingService.debug("Looking up participant " + isAliveParticipant + FOR_IS_ALIVE);
                    // lookup participant on DNS
                    lookupEntriesIsAliveDNS(dnsRecordsForParticipantIsAlive);
                } catch (TechnicalException exc1) {
                    try {
                        loggingService.error("Trying to look up participant " + isAliveParticipant + " for is alive for the second time", exc1);
                        lookupEntriesIsAliveDNS(dnsRecordsForParticipantIsAlive);
                    } catch (TechnicalException exc2) {
                        loggingService.error("Rolling back dns operation - deleting participant " + isAliveParticipant + FOR_IS_ALIVE, exc2);
                        deleteEntriesIsAliveDNS(isAliveParticipant);
                        throw exc2;
                    }
                }

                // delete participant from DNS
                loggingService.debug("Deleting dns participant " + isAliveParticipant + FOR_IS_ALIVE);
                deleteEntriesIsAliveDNS(isAliveParticipant);
            }
        }
    }

    private void lookupEntriesIsAliveDNS(ParticipantDnsRecord dnsRecordsForParticipantIsAlive) throws
            TechnicalException {
        lookup(dnsRecordsForParticipantIsAlive.getCnameRecord().getName().toString(), Type.CNAME);
        lookup(dnsRecordsForParticipantIsAlive.getNaptrRecord().getName().toString(), Type.NAPTR);
    }

    private void deleteEntriesIsAliveDNS(ParticipantBO isAliveParticipant) throws DNSClientException {
        try {
            deleteDNSRecordsForParticipantIsAlive(isAliveParticipant);
        } catch (TechnicalException e) {
            throw new DNSClientException("Impossible to delete from DNS", e);
        }
    }

    /**
     * Send a message to the DNS server and validate it. If it fails and an exception is caught, a
     * retry mechanism has been put in place. After X failures, an exception is thrown.
     *
     * @param dnsUpdate the update message to send to the DNS server
     */
    protected void sendAndValidateMessage(Update dnsUpdate) throws TechnicalException {
        sendAndValidateMessage(dnsUpdate, 0, true, null, null);
    }

    private void sendAndValidateMessage(Update dnsUpdate, int retry, boolean sign, Exception
            lastException, Message lastResponse) throws TechnicalException {
        if (retry > 0) {
            LOG.info("Retrying for the [{}] time.", retry);
        }
        if (retry < MAX_RETRY) {
            Message response = null;
            try {
                response = sendMessageToDnsServer(dnsUpdate, sign);
                validateDNSResponse(response);
            } catch (Exception exc) {
                LOG.warn("An error occurred while sending message to the DNS: [{}]. Retrying...", ExceptionUtils.getRootCauseMessage(exc));
                // retry message is already signed no need to sign it again
                sendAndValidateMessage(dnsUpdate, retry + 1, false, exc, response);
            }
        } else {
            LOG.info("Error response message from the DNS: [{}].", lastResponse);
            throw new DNSClientException("ERROR: There was an error. Impossible to update the DNS server after [" + retry + "] retries.", lastException);
        }
    }

    /**
     * Common method for validating Responses from DNS
     *
     * @param response the response from the DNS  to validate
     */
    private void validateDNSResponse(final Message response) throws TechnicalException {
        final String retCode = Rcode.string(response.getRcode());

        if (response.getRcode() != Rcode.NOERROR) {
            LOG.info("DNS response code: [{}]", retCode);
            // Error - not handling special cases yet
            throw new DNSClientException("Error performing DNS request: " + retCode);
        }
    }

    private Message sendMessageToDnsServer(Message updateMessage, boolean sign) throws Exception {
        boolean sig0Enabled = configurationBusiness.isDNSSig0Enabled();
        LOG.debug("Starting signature of the DNS call");
        long init = System.currentTimeMillis();
        if (sig0Enabled && sign) {
            // To avoid any problem with the validity start date for the time of signature,
            // we start the validity a few minutes back
            int validityMinutesBack = 2;
            DomiSmlSIG0Service.signMessage(updateMessage, sig0KeyProviderService.getSIG0Record(),
                    sig0KeyProviderService.getPrivateSIG0Key(), null, validityMinutesBack);
            LOG.debug("DNS signature call executed in [{}] ms", (System.currentTimeMillis() - init));
        }



        init = System.currentTimeMillis();
        Message message = new Message(updateMessage.toWire());

        LOG.info("Sending update to DNS [{}]", message);
        Message resp = dnsMessageSenderService.sendMessage(message);
        LOG.debug("DNS signature call executed in [{}] ms", (System.currentTimeMillis() - init));
        return resp;
    }

    @Override
    public boolean isParticipantAlreadyCreated(ParticipantBO participantBO, DNSSubSomainRecordTypeEnum recordType) {
        int participantType = Type.value(recordType.name());
        if (!Arrays.asList(Type.CNAME, Type.NAPTR).contains(participantType)) {
            loggingService.warn("Participant '" + participantBO.getParticipantId() + "' having scheme '" + participantBO.getScheme()
                    + "' is being created with an unsupported DNS record type: '" + Type.string(participantType) + "'. Possible values: CNAME and NAPTR");
            return false;
        }
        try {
            String md5ParticipantIdHash = dnsRecordManager.generateDnsQuery(
                    participantBO.getParticipantId(), participantBO.getScheme(),
                    getDnsZoneName().getSubdomain(), participantType)
                .toString();
            if (md5ParticipantIdHash.endsWith(".")) {
                md5ParticipantIdHash = md5ParticipantIdHash.substring(0, md5ParticipantIdHash.length() - 1);
            }

            List<Record> records = lookup(md5ParticipantIdHash, participantType);
            return !records.isEmpty();
        } catch (DNSClientException exc) {
            loggingService.debug(String.format("VALIDATION | The Participant %s for Scheme %s does not exist in the DNS", participantBO.getParticipantId(), participantBO.getScheme()));
            return false;
        } catch (Exception exc) {
            loggingService.error(exc.getMessage(), exc);
            return false;
        }
    }

    @Override
    public List<Record> lookup(String domainName, int type) throws TechnicalException {
        try {
            String trimDomain = StringUtils.trim(domainName);
            Lookup lookup = new Lookup(trimDomain, type);
            lookup.setCache(null);
            if (!StringUtils.isEmpty(configurationBusiness.getDNSServer())) {
                lookup.setResolver(new SimpleResolver(configurationBusiness.getDNSServer()));
            }
            Record[] records = lookup.run();
            if (lookup.getResult() != Lookup.SUCCESSFUL) {
                throw new DNSClientException(String.format("Impossible to lookup '%s' on DNS, Response: %s, DnsType: %s", domainName, lookup.getErrorString(), type));
            }
            return Arrays.asList(records);
        } catch (DNSClientException dnsexc) {
            throw dnsexc;
        } catch (Exception e) {
            throw new DNSClientException(e.getMessage(), e);
        }
    }

    protected boolean recordExists(String domainName, int type) throws TechnicalException {
        List<Record> records = lookup(domainName, type);
        return !records.isEmpty();
    }

    @Override
    public List<Record> lookupAny(String domainName) throws TechnicalException {
        return lookup(domainName, Type.ANY);
    }

    @Override
    public void createCustomDNSRecord(DNSRecordBO dnsRecordBO, DNSRecordEnum dnsRecord) throws TechnicalException {

        Name dnsName;
        try {
            dnsName = Name.fromString(StringUtil.addFinalDot(dnsRecordBO.getName()));
        } catch (TextParseException e) {
            throw new BadRequestException("Bad dns name: " + dnsRecordBO.getName());
        }

        Name dnsZone;
        try {
            dnsZone = Name.fromString(StringUtil.addFinalDot(dnsRecordBO.getDNSZone()));
        } catch (TextParseException e) {
            throw new BadRequestException("Bad dns zone name: " + dnsRecordBO.getDNSZone());
        }

        Record record;
        switch (dnsRecord) {
            case A:
                record = dnsRecordManager.createCustomARecord(dnsName, dnsRecordBO.getValue());
                break;
            case NAPTR:
                record = dnsRecordManager.createCustomNaptrRecord(dnsName, dnsRecordBO.getValue(), dnsRecordBO.getService());
                break;
            case CNAME:
                record = dnsRecordManager.createCustomCNameRecord(dnsName, dnsRecordBO.getValue());
                break;
            default:
                throw new BadRequestException("Not supported DNS type: " + dnsRecordBO.getType());
        }

        Update updateCustomRecord = new Update(dnsZone);
        updateCustomRecord.add(record);
        sendAndValidateMessage(updateCustomRecord);

        loggingService.businessLog(LogEvents.BUS_CUSTOM_DNS_CREATED, dnsRecordBO.toString());

    }

    @Override
    public void deleteCustomDNSRecord(Name name, Name dnsZone) throws TechnicalException {

        final Update dnsDelete = new Update(dnsZone);

        dnsDelete.delete(name);
        loggingService.debug("Creating  request to delete  " + name);

        sendAndValidateMessage(dnsDelete);
        loggingService.businessLog(LogEvents.BUS_CUSTOM_DNS_DELETED, name.toString(), dnsZone.toString());

    }

    private SupportedDnsRecordType getDnsRecordTypes(String dnsSubDomain) {

        if (dnsSubDomain.endsWith(".")) {
            dnsSubDomain = dnsSubDomain.substring(0, dnsSubDomain.length() - 1);
        }
        Optional<SubdomainBO> optSubdomainBO;
        try {
            optSubdomainBO = subdomainBusiness.getSubDomainByName(dnsSubDomain);
            if (optSubdomainBO.isEmpty()) {
                throw new ConfigurationException(String.format("It was not possible to find subdomain %s by name.", dnsSubDomain));
            }
        } catch (TechnicalException exc) {
            throw new ConfigurationException(String.format("Subdomain %s is not defined in database", dnsSubDomain));
        }
        String recordTypes = optSubdomainBO.get().getDnsRecordTypes();
        if (StringUtils.isBlank(recordTypes)) {
            throw new ConfigurationException(String.format("Record types for subdomain %s by name must not be empty.", dnsSubDomain));
        }
        try {
            return SupportedDnsRecordType.valueOf(recordTypes.trim().toUpperCase());
        } catch (IllegalArgumentException exc) {
            throw new ConfigurationException(String.format("Subdomain [%s] has illegal dns type: [%s].", dnsSubDomain, recordTypes));
        }
    }

    public static class ParticipantDnsRecord {
        private final Record cnameRecord;
        private final Record naptrRecord;

        public ParticipantDnsRecord(Record cnameRecord, Record naptrRecord) {
            this.cnameRecord = cnameRecord;
            this.naptrRecord = naptrRecord;
        }

        public Record getCnameRecord() {
            return cnameRecord;
        }

        public Record getNaptrRecord() {
            return naptrRecord;
        }
    }
}
