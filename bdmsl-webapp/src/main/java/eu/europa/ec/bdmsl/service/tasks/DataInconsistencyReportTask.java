/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.tasks;


import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.service.IDataQualityInconsistencyAnalyzerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class DataInconsistencyReportTask implements Runnable {

    protected static final Logger LOG = LoggerFactory.getLogger(DataInconsistencyReportTask.class);

    @Autowired
    protected IDataQualityInconsistencyAnalyzerService dataQualityInconsistencyAnalyzerService;


    String receiverAddress;

    public String getReceiverAddress() {
        return receiverAddress;
    }

    public void setReceiverAddress(String receiverAddress) {
        this.receiverAddress = receiverAddress;
    }


    @Override
    public void run() {
        try {
            LOG.info("Start generating data inconsistency report for receiver: [{}]", receiverAddress);
            updateHashValuesForParticipants();
            LOG.info("Ended update hash values: [{}]", receiverAddress);
            dataQualityInconsistencyAnalyzerService.generateDataInconsistencyReport(receiverAddress);
            LOG.info("Ended generating data inconsistency report for receiver: [{}]", receiverAddress);
        } catch (TechnicalException e) {
            LOG.error("Data inconsistency error generation for receiver '" + receiverAddress
                    + "' failed with error: " + e.getMessage(), e);
        }
    }


    public void updateHashValuesForParticipants() throws TechnicalException {
        int val = dataQualityInconsistencyAnalyzerService.updateNextBatchHashValuesForParticipants();
        int iSum = 0;
        while (val > 0) {
            // check if there are more participants to update
            val = dataQualityInconsistencyAnalyzerService.updateNextBatchHashValuesForParticipants();
            iSum += val;
            LOG.info("Updated in a loop: [{}] participants hash values!", iSum);
        }
        LOG.info("Finished update: [{}] participants hash values!", iSum);
    }
}
