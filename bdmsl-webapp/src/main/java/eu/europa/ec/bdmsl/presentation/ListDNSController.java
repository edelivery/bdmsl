/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.presentation;

import eu.europa.ec.bdmsl.business.IConfigurationBusiness;
import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import eu.europa.ec.bdmsl.common.exception.ConfigurationException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.common.logging.ILoggingService;
import eu.europa.ec.bdmsl.service.IBDMSLService;
import eu.europa.ec.bdmsl.service.dns.IDnsClientService;
import eu.europa.ec.bdmsl.service.dns.impl.DnsZone;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.xbill.DNS.ARecord;
import org.xbill.DNS.CNAMERecord;
import org.xbill.DNS.NAPTRRecord;
import org.xbill.DNS.Record;

import jakarta.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Adrien FERIAL
 * @since 16/07/2015
 */
@Controller
public class ListDNSController {

    public static final String ERROR = "error";

    @Autowired
    private IDnsClientService dnsClientService;

    @Autowired
    private IBDMSLService bdmslService;

    @Autowired
    private ILoggingService loggingService;

    @Autowired
    IConfigurationBusiness configurationBusiness;


    public static final String VIEW_LIST_DNS = "listDNS";
    public static final String VIEW_SEARCH = "search";
    public static final String VIEW_SEARCH_RESULT = "searchResult";
    public static final String VIEW_SEARCH_ERROR = "searchError";


    @RequestMapping(path="/listDNS", method = RequestMethod.GET)
    public String listDNS(Model model) {
        loggingService.debug("Calling listDNS...");
        if (configurationBusiness.isDNSEnabled() && configurationBusiness.isShowDNSEntriesEnabled()) {
            model.addAttribute("dnsServer", configurationBusiness.getDNSServer());
            try {
                Set<String> domainSet = listAllDomains();
                Map<String, Collection<Record>> recordMap = getDNSRecordsByDomain(domainSet, model);
                model.addAttribute("recordMap", recordMap);
            } catch (TechnicalException exc) {
                loggingService.error("Error during the ListDNS call", exc);
            }
        }
        model.addAttribute("dnsEnabled", configurationBusiness.isDNSEnabled());
        model.addAttribute("isShowDNSEntriesEnabled", configurationBusiness.isShowDNSEntriesEnabled());
        loggingService.debug("listDNS Done!");
        return VIEW_LIST_DNS;
    }

    @RequestMapping(path="/search", method = RequestMethod.GET)
    public String search(Model model) {
        return VIEW_SEARCH;
    }

    @RequestMapping(path="/searchResult", method = RequestMethod.POST)
    public String searchResult(@Valid @ModelAttribute("searchParams") SMLSearchParams searchParams,
                               BindingResult result, ModelMap model) {


        String searchType = searchParams.getSearchType();
        model.addAttribute("searchType", searchType);
        model.addAttribute("dnsEnabled", configurationBusiness.isDNSEnabled());
        if (!configurationBusiness.isDNSEnabled()) {
            model.addAttribute(ERROR, "DNS is not enabled!");
            return VIEW_SEARCH_ERROR;
        }

        loggingService.info("searchResult: " + searchParams);
        if (StringUtils.equalsIgnoreCase(searchType, "domain")) {
            model.addAttribute("domainName", searchParams.getDomainName());
            if (StringUtils.isBlank(searchParams.getDomainName())) {
                model.addAttribute(ERROR, "DNS domain must not be null!");
                return VIEW_SEARCH_ERROR;
            }
            try {
                Collection<Record> lst = dnsClientService.lookupAny(searchParams.getDomainName());
                model.addAttribute("recordList", lst);
            } catch (TechnicalException e) {
                String message = "Error occurred while resolving domain: '" + searchParams.getDomainName() + "'";
                model.addAttribute(ERROR, message);
                loggingService.error(message, e);
                return VIEW_SEARCH_ERROR;
            }
        } else {

            model.addAttribute("identifier", searchParams.getIdentifier());
            model.addAttribute("scheme", searchParams.getScheme());
            try {
                Map<String, Collection<Record>> recordMap = dnsClientService.getAllParticipantRecords(searchParams.getIdentifier(), searchParams.getScheme());
                loggingService.info(" got records " + recordMap);
                model.addAttribute("recordMap", recordMap);
            } catch (TechnicalException exc) {
                String message = "Error occurred while resolving participant: '" + searchParams.getIdentifier() + "' with scheme: '" + searchParams.getScheme() + "'!";
                model.addAttribute(ERROR, message);
                loggingService.error(message, exc);
                return VIEW_SEARCH_ERROR;
            }

        }
        return VIEW_SEARCH_RESULT;
    }


    private Set<String> listAllDomains() throws TechnicalException {
        List<SubdomainBO> certificateDomainBOList = bdmslService.findAllSubdomains();
        Set<String> domainSet = certificateDomainBOList.stream().map(domain -> {
            try {
                DnsZone dnsZone = dnsClientService.getDnsZoneNameForSubDomain(domain);
                return dnsZone.getDomain();
            } catch (TechnicalException e) {
                throw new ConfigurationException(String.format("Impossible to retrieve domain for subdomain %s", domain));
            }
        }).collect(Collectors.toSet());
        return domainSet;
    }

    private Map<String, Collection<Record>> getDNSRecordsByDomain(Set<String> domainSet, Model model) throws TechnicalException {
        int numberOfRecords = 0;
        Map<String, Collection<Record>> recordMap = new HashMap<>();
        for (String domain : domainSet) {
            List<Record> dnsRecords = dnsClientService.getAllRecords(domain);
            List<Record> records = dnsRecords.stream()
                    .filter(record -> record instanceof ARecord || record instanceof CNAMERecord || record instanceof NAPTRRecord)
                    .collect(Collectors.toList());

            numberOfRecords += records.size();
            recordMap.put(domain, records);
        }
        model.addAttribute("numberOfRecords", numberOfRecords);

        return recordMap;
    }
}
