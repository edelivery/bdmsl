/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.impl;


import eu.europa.ec.bdmsl.common.bo.DNSRecordBO;
import eu.europa.ec.bdmsl.dao.AbstractDAOImpl;
import eu.europa.ec.bdmsl.dao.IDNSRecordDAO;
import eu.europa.ec.bdmsl.dao.entity.DNSRecordEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Repository;

import jakarta.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static eu.europa.ec.bdmsl.dao.QueryNames.*;

@Repository
public class DNSRecordDAOImpl extends AbstractDAOImpl implements IDNSRecordDAO {

    @Autowired
    private ConversionService conversionService;

    @Override
    public Optional<DNSRecordBO> getDNSRecord(String type, String name, String dnsZone, String value) {
        Optional<DNSRecordEntity> dnsRecordEntityOpt = getDNSRecordEntity(type, name, dnsZone, value);
        return dnsRecordEntityOpt.map(dnsRecordEntity -> conversionService.convert(dnsRecordEntity, DNSRecordBO.class));
    }

    @Override
    public Optional<DNSRecordBO> deleteDNSRecord(String type, String name, String dnsZone, String value) {
        Optional<DNSRecordEntity> dnsRecordEntityOptional = getDNSRecordEntity(type, name, dnsZone, value);
        Optional<DNSRecordBO> dnsRecordBO = dnsRecordEntityOptional.map(dnsRecordEntity -> {
            getEntityManager().remove(dnsRecordEntity);
            loggingService.info("Remove dns record: " + dnsRecordEntity);
            return conversionService.convert(dnsRecordEntity, DNSRecordBO.class);
        });
        if (!dnsRecordBO.isPresent()) {
            loggingService.info(
                    String.format("DNS record not deleted. Record with name: '%s', type: '%s', value: '%s' does not exist in database!", name, type, value));
        }
        return dnsRecordBO;
    }

    @Override
    public List<DNSRecordBO> deleteDNSRecords(String name, String dnsZone) {
        TypedQuery<DNSRecordEntity> query = getEntityManager().createNamedQuery(DNS_RECORD_ENTITY_GET_BY_NAME_AND_ZONE, DNSRecordEntity.class);
        query.setParameter(NAME, name);
        query.setParameter(DNS_ZONE, dnsZone);
        List<DNSRecordEntity> result = query.getResultList();
        List<DNSRecordBO> deletedRecords = new ArrayList<>();
        result.forEach(dnsRecordEntity -> {
            getEntityManager().remove(dnsRecordEntity);
            loggingService.info("Remove dns entry: " + dnsRecordEntity);
            deletedRecords.add(conversionService.convert(dnsRecordEntity, DNSRecordBO.class));
        });
        return deletedRecords;
    }

    @Override
    public void addDNSRecord(DNSRecordBO dnsRecordBO) {
        DNSRecordEntity dnsRecordEntity = new DNSRecordEntity();
        dnsRecordEntity.setType(dnsRecordBO.getType().toUpperCase());// domain to lower
        dnsRecordEntity.setDNSZone(dnsRecordBO.getDNSZone().toLowerCase());// domain to lower
        dnsRecordEntity.setName(dnsRecordBO.getName().toLowerCase()); // type to upper
        dnsRecordEntity.setValue(dnsRecordBO.getValue());
        dnsRecordEntity.setService(dnsRecordBO.getService());
        super.persist(dnsRecordEntity);

    }

    private Optional<DNSRecordEntity> getDNSRecordEntity(String type, String name, String dnsZone, String value) {
        TypedQuery<DNSRecordEntity> query = getEntityManager().createNamedQuery(DNS_RECORD_ENTITY_GET_BY_NAME_AND_ZONE_AND_TYPE_AND_VALUE, DNSRecordEntity.class);
        query.setParameter(NAME, name.toLowerCase()); // domain to lower
        query.setParameter(DNS_ZONE, dnsZone.toLowerCase()); // domain to lower
        query.setParameter(DNS_TYPE, type.toUpperCase()); // type to upper
        query.setParameter(DNS_VALUE, value);
        List<DNSRecordEntity> result = query.getResultList();
        if (result.isEmpty()) {
            return Optional.empty();
        }
        if (result.size() > 1) {
            loggingService.warn(
                    String.format("Inconsistent data in database: multiple instances of DNS record with name: '%s', type: '%s', value: '%s' in database!", name, type, value));
        }
        // return first
        return Optional.of(result.get(0));
    }
}
