/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.bo;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Adrien FERIAL
 * @since 12/06/2015
 */
public class ParticipantListBO extends AbstractBusinessObject {

    private List<ParticipantBO> participantBOList;

    private String nextPage;

    public List<ParticipantBO> getParticipantBOList() {
        return participantBOList;
    }

    public void setParticipantBOList(List<ParticipantBO> participantBOList) {
        this.participantBOList = participantBOList;
    }

    public String getNextPage() {
        return nextPage;
    }

    public void setNextPage(String nextPage) {
        this.nextPage = nextPage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ParticipantListBO)) return false;

        ParticipantListBO that = (ParticipantListBO) o;

        if (participantBOList != null ? !participantBOList.equals(that.participantBOList) : that.participantBOList != null)
            return false;
        return !(nextPage != null ? !nextPage.equals(that.nextPage) : that.nextPage != null);

    }

    @Override
    public int hashCode() {
        int result = participantBOList != null ? participantBOList.hashCode() : 0;
        result = 31 * result + (nextPage != null ? nextPage.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ParticipantListBO{" +
                "participantBOList=" + (participantBOList != null ? participantBOList.stream()
                .map(p -> "("+ p.getScheme() + ", "  + p.getParticipantId() + ")")
                .collect(Collectors.joining(", ")) : null) +
                ", nextPage=" + nextPage +
                '}';
    }
}
