/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.business.impl;


import eu.europa.ec.bdmsl.business.IAdminManageServiceMetadataBusiness;
import eu.europa.ec.bdmsl.business.IManageServiceMetadataBusiness;
import eu.europa.ec.bdmsl.common.bo.ServiceMetadataPublisherBO;
import eu.europa.ec.bdmsl.common.enums.AdminSMPManageActionEnum;
import eu.europa.ec.bdmsl.common.exception.BadRequestException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import static eu.europa.ec.bdmsl.common.exception.ErrorMessagesType.*;

/**
 * Business class implementation of the {@link IAdminManageServiceMetadataBusiness} interface.
 * for DomiSML administrators to manage custom ServiceMetadataPublisherBO.
 * <p>
 *
 * @author Joze RIHTARSIC
 * @since 4.3
 */
@Component
public class AdminManageServiceMetadataBusinessImpl implements IAdminManageServiceMetadataBusiness {

    private static final Logger LOG = LoggerFactory.getLogger(AdminManageServiceMetadataBusinessImpl.class);

    private final IManageServiceMetadataBusiness manageServiceMetadataBusiness;


    public AdminManageServiceMetadataBusinessImpl(
            IManageServiceMetadataBusiness manageServiceMetadataBusiness) {
        this.manageServiceMetadataBusiness = manageServiceMetadataBusiness;
    }

    @Override
    public ServiceMetadataPublisherBO validateSMPIdentifier(String serviceMetadataPublisherId, String certificateOwnerId, String domainZone) throws TechnicalException {
        LOG.debug("validateSMPIdentifier: serviceMetadataPublisherId: [{}], certificateOwnerId:[{}], domainZone:[{}]", serviceMetadataPublisherId, certificateOwnerId, domainZone);
        ServiceMetadataPublisherBO metadataPublisherBO = manageServiceMetadataBusiness.read(serviceMetadataPublisherId);
        if (metadataPublisherBO == null) {
            throw new BadRequestException(PUBLISHER_ID_NOT_EXISTING, serviceMetadataPublisherId);
        }
        if (!StringUtils.equalsIgnoreCase(metadataPublisherBO.getCertificateId(), certificateOwnerId)) {
            throw new BadRequestException(PUBLISHER_ID_NOT_BOUND, serviceMetadataPublisherId, certificateOwnerId);
        }

        if (!StringUtils.equalsIgnoreCase(metadataPublisherBO.getSubdomain().getSubdomainName(), domainZone)) {
            throw new BadRequestException(PUBLISHER_ID_NOT_REGISTERED, serviceMetadataPublisherId, domainZone);
        }
        return metadataPublisherBO;
    }

    @Override
    public void validateTaskData(AdminSMPManageActionEnum action, ServiceMetadataPublisherBO smpBO, String logicalAddress, String physicalAddress) throws TechnicalException {

        if (action == null) {
            throw new BadRequestException("Action is mandatory!");
        }

        if (smpBO == null) {
            throw new BadRequestException("SMP instance is mandatory!");
        }

        if (action == AdminSMPManageActionEnum.DELETE && !smpBO.isDisabled()) {
            throw new BadRequestException(PUBLISHER_ID_NOT_DISABLED, smpBO.getSmpId());
        }

        if (action == AdminSMPManageActionEnum.ENABLE && !smpBO.isDisabled()) {
            throw new BadRequestException(PUBLISHER_ID_ALREADY_ENABLED, smpBO.getSmpId());
        }

        if (action == AdminSMPManageActionEnum.DISABLE && smpBO.isDisabled()) {
            throw new BadRequestException(PUBLISHER_ID_ALREADY_DISABLED, smpBO.getSmpId());
        }

        if (action == AdminSMPManageActionEnum.UPDATE){
            if (StringUtils.isBlank(logicalAddress) && StringUtils.isBlank(physicalAddress)) {
                throw new BadRequestException("Action [UPDATE] must have at least one of the parameters: [logicalAddress, physicalAddress]!");
            }

            ServiceMetadataPublisherBO updatedSmp = new ServiceMetadataPublisherBO();
            updatedSmp.setSmpId(smpBO.getSmpId());
            updatedSmp.setPhysicalAddress(StringUtils.defaultIfBlank(physicalAddress, smpBO.getPhysicalAddress()));
            updatedSmp.setLogicalAddress(StringUtils.defaultIfBlank(logicalAddress,  smpBO.getLogicalAddress()));
            manageServiceMetadataBusiness.validateSMPData(updatedSmp);
        }
    }
}
