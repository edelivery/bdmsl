/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2017 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.conversion;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;

/**
 * Interceptor that trims all non-null string values of the converted object.
 * Applies only to classes extending AutoRegisteringConverter.
 *
 *  * @author Thomas Dussart
 *  * @since 24/05/2024
 */
@Aspect
@Component
public class AutoRegisteringConverterInterceptor {

    @Around("execution(* org.springframework.core.convert.converter.Converter+.convert(..)) && within(eu.europa.ec.bdmsl.conversion..*)")
    public Object interceptConvert(ProceedingJoinPoint joinPoint) throws Throwable {
        Object result = joinPoint.proceed();

        if (result != null) {
            Class<?> resultClass = result.getClass();
            Field[] fields = resultClass.getDeclaredFields();

            for (Field field : fields) {
                if (field.getType().equals(String.class)) {
                    field.setAccessible(true);
                    String value = (String) field.get(result);
                    if (value != null) {
                        field.set(result, value.trim());
                    }
                }
            }
        }
        return result;
    }
}
