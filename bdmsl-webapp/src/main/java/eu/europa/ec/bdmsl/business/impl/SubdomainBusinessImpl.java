/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.business.impl;

import eu.europa.ec.bdmsl.business.ISubdomainBusiness;
import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import eu.europa.ec.bdmsl.common.business.AbstractBusinessImpl;
import eu.europa.ec.bdmsl.common.enums.DNSSubSomainRecordTypeEnum;
import eu.europa.ec.bdmsl.common.enums.UrlSchemeSubDomainTypeEnum;
import eu.europa.ec.bdmsl.common.exception.BadRequestException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.dao.ISubdomainDAO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import static eu.europa.ec.bdmsl.common.exception.Messages.*;
import static java.lang.String.format;

/**
 * @author Tiago MIGUEL
 * @since 13/03/2017
 */
@Component
public class SubdomainBusinessImpl extends AbstractBusinessImpl implements ISubdomainBusiness {

    @Autowired
    private ISubdomainDAO subdomainDAO;

    private static final Pattern DOMAIN_PATTERN = Pattern.compile("^(?=.{1,255}$)(?!-)[A-Za-z0-9\\-]{1,63}(\\.[A-Za-z0-9\\-]{1,63})*\\.?(?<!-)$");
    private static final List<String> DNS_TYPE = DNSSubSomainRecordTypeEnum.getCodeList();
    private static final List<String> URL_SCHEME = UrlSchemeSubDomainTypeEnum.getCodeList();

    @Override
    public List<SubdomainBO> findAll() throws TechnicalException {
        return subdomainDAO.findAll();
    }

    @Override
    public Optional<SubdomainBO> getSubDomainByName(String domainName) throws TechnicalException {
        return subdomainDAO.getSubDomainByName(domainName);
    }

    @Override
    public List<String> findAllSchemesPerDomain() throws TechnicalException {
        return subdomainDAO.findAllSchemesPerDomain();
    }

    @Override
    public SubdomainBO createSubDomain(SubdomainBO subdomainBO) throws TechnicalException {
        if (StringUtils.isBlank(subdomainBO.getSubdomainName())) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_EMPTY_SUBDOMAIN);
        }
        subdomainBO.setSubdomainName(subdomainBO.getSubdomainName().toLowerCase().trim());

        if (StringUtils.isBlank(subdomainBO.getDnsZone())) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_EMPTY_DNSZONE);
        } else {
            subdomainBO.setDnsZone(subdomainBO.getDnsZone().toLowerCase().trim());
            validateDomainZone(subdomainBO.getDnsZone());
        }

        // validate SubDomainName
        validateSubDomainName(subdomainBO.getSubdomainName(), subdomainBO.getDnsZone());

        validateNullableRegularExpresion(subdomainBO.getParticipantIdRegexp(), "ParticipantRegularExpression", false);
        validateNullableRegularExpresion(subdomainBO.getSmpCertSubjectRegex(), "CertSubjectRegularExpression", true);

        if (StringUtils.isBlank(subdomainBO.getSmpUrlSchemas())) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_EMPTY_URL_SCHEME);
        } else {
            subdomainBO.setSmpUrlSchemas(subdomainBO.getSmpUrlSchemas().toLowerCase().trim());
            validateUrlScheme(subdomainBO.getSmpUrlSchemas());
        }
        if (StringUtils.isBlank(subdomainBO.getDnsRecordTypes())) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_EMPTY_DNS_RT);
        } else {
            subdomainBO.setDnsRecordTypes(subdomainBO.getDnsRecordTypes().toLowerCase().trim());
            validateDnsType(subdomainBO.getDnsRecordTypes());
        }
        if (subdomainDAO.getSubDomainByName(subdomainBO.getSubdomainName()).isPresent()) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_EXIST_SUBDOMAIN);
        }

        if (subdomainBO.getMaxParticipantCountForSMP() != null && subdomainBO.getMaxParticipantCountForSMP().intValue() < 1) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_INVALID_MAX_PARTICIPANT_COUNT_SMP);
        }

        if (subdomainBO.getMaxParticipantCountForDomain() != null && subdomainBO.getMaxParticipantCountForDomain().intValue() < 1) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_INVALID_MAX_PARTICIPANT_COUNT_DOMAIN);
        }

        subdomainDAO.createSubDomain(subdomainBO);
        return subdomainDAO.getSubDomain(subdomainBO.getSubdomainName());
    }

    @Override
    public SubdomainBO updateSubDomain(String domainName, String regExp, String dnsRecordType, String urlSchemas,
                                       String certSubjectRegEx,
                                       String certPolicyOIDs,
                                       BigInteger maxParticipantCountForDomain,
                                       BigInteger maxParticipantCountForSMP) throws TechnicalException {

        if (StringUtils.isBlank(domainName)) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_EMPTY_SUBDOMAIN);
        }

        validateNullableRegularExpresion(regExp, "ParticipantRegularExpression", true);
        validateNullableRegularExpresion(certSubjectRegEx, "CertSubjectRegularExpression", true);

        if (maxParticipantCountForSMP != null && maxParticipantCountForSMP.intValue() < 1) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_INVALID_MAX_PARTICIPANT_COUNT_SMP);
        }

        if (maxParticipantCountForDomain != null && maxParticipantCountForDomain.intValue() < 1) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_INVALID_MAX_PARTICIPANT_COUNT_DOMAIN);
        }

        String dnsRT = null;
        String urlSC = null;
        if (dnsRecordType != null) {
            dnsRT = dnsRecordType.trim().toLowerCase();
            validateDnsType(dnsRT);
        }
        if (urlSchemas != null) {
            urlSC = urlSchemas.trim().toLowerCase();
            validateUrlScheme(urlSC);
        }

        subdomainDAO.updateSubDomainValues(domainName.toLowerCase(),
                regExp,
                dnsRT,
                urlSC,
                certSubjectRegEx,
                certPolicyOIDs,
                maxParticipantCountForDomain,
                maxParticipantCountForSMP);

        return subdomainDAO.getSubDomain(domainName.toLowerCase());
    }

    @Override
    public SubdomainBO deleteSubDomain(String domainName) throws TechnicalException {
        if (StringUtils.isBlank(domainName)) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_EMPTY_SUBDOMAIN);
        }
        //  all subdomains are stored in lower case
        return subdomainDAO.deleteSubDomain(domainName.toLowerCase());
    }

    @Override
    public SubdomainBO getSubDomain(String domainName) throws TechnicalException {
        if (StringUtils.isBlank(domainName)) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_EMPTY_SUBDOMAIN);
        }
        //  all subdomains are stored in lower case
        return subdomainDAO.getSubDomain(domainName.toLowerCase());
    }

    private void validateNullableRegularExpresion(String regExp, String fieldName, boolean allowNull) throws TechnicalException {

        if (regExp == null && allowNull) {
            loggingService.debug("Regular expression for optional field [" + fieldName + "] is null! Skip regular expression validation.");
            return;
        }
        if (StringUtils.isBlank(regExp)) {
            throw new BadRequestException(format(BAD_REQUEST_PARAMETER_EMPTY_REG_EXP, fieldName));
        }
        try {
            Pattern.compile(regExp);
        } catch (PatternSyntaxException e) {
            throw new BadRequestException(format(BAD_REQUEST_PARAMETER_INVALID_REG_EXP, fieldName));

        }
    }

    private void validateDomainZone(String domainZone) throws TechnicalException {
        Matcher matcher = DOMAIN_PATTERN.matcher(domainZone);
        if (!matcher.matches()) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_INVALID_DNSZONE);
        }
    }

    private void validateSubDomainName(String subDomainName, String domainZone) throws TechnicalException {
        Matcher matcher = DOMAIN_PATTERN.matcher(subDomainName);
        if (!matcher.matches()) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_INVALID_SUBDOMAIN);
        }


        if (!subDomainName.endsWith("." + domainZone) && !subDomainName.equalsIgnoreCase(domainZone)) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_INVALID_SUBDOMAIN + " SubDomain must end with DNS zone: [." + domainZone + "].");
        }
    }

    private void validateDnsType(String dnsType) throws TechnicalException {

        if (StringUtils.isBlank(dnsType)) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_EMPTY_DNS_RT);
        }

        if (!DNS_TYPE.contains(dnsType)) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_INVALID_DNS_RT);
        }
    }

    private void validateUrlScheme(String dnsType) throws TechnicalException {
        if (StringUtils.isBlank(dnsType)) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_EMPTY_URL_SCHEME);
        }

        if (!URL_SCHEME.contains(dnsType)) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_INVALID_URL_SCHEME);
        }
    }
}
