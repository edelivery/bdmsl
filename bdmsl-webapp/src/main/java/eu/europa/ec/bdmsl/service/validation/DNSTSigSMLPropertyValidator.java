/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.validation;

import eu.europa.ec.bdmsl.common.enums.SMLPropertyEnum;
import eu.europa.ec.bdmsl.common.exception.BadConfigurationException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.io.File;

import static eu.europa.ec.bdmsl.common.enums.SMLPropertyEnum.DNS_TSIG_KEY_VALUE;
import static org.apache.commons.codec.binary.Base64.isBase64;
import static org.apache.commons.lang3.StringUtils.removeEnd;
import static org.apache.commons.lang3.StringUtils.trim;

/**
 * DNSTSigSMLPropertyValidator is a class that validates the properties of the DNS TSig configuration.
 *
 * @author Joze RIHTARSIC
 * @since 5.0
 */
@Component
@Order(3)
public class DNSTSigSMLPropertyValidator implements SMLPropertyValidator {
    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(DNSTSigSMLPropertyValidator.class);
    private static final String[] TSIG_HASH_ALGORITHMS = {"hmac-md5", "hmac-sha1", "hmac-sha224", "hmac-sha256", "hmac-sha384", "hmac-sha512"};

    @Override
    public void validate(SMLPropertyEnum property, String value, File configDirectory) throws BadConfigurationException {
        switch (property) {
            case DNS_TSIG_HASH_ALGORITHM:
                validateHashAlgorithm(value);
                break;
            case DNS_TSIG_KEY_VALUE:
                validateKey(value);
                break;
            default:
                // do nothing
                break;
        }
    }

    protected void validateKey(String value) throws BadConfigurationException {
        // remove the trailing dot if exists
        if (StringUtils.isBlank(value)) {
            LOG.debug("Property: [{}] is empty", DNS_TSIG_KEY_VALUE.getProperty());
            return;
        }

        if (!isBase64(value)) {
            throw new BadConfigurationException("Property: " + DNS_TSIG_KEY_VALUE.getProperty() + " is not a valid base64 encoded string: [" + value + "]");
        }
    }


    protected void validateHashAlgorithm(String value) throws BadConfigurationException {
        // remove the trailing dot if exists
        String finalValue = removeEnd(trim(value), ".");
        if (!StringUtils.equalsAnyIgnoreCase(finalValue, TSIG_HASH_ALGORITHMS)) {
            throw new BadConfigurationException("Property: " + SMLPropertyEnum.DNS_TSIG_HASH_ALGORITHM.getProperty()
                    + " doesn't have the required value ["
                    + String.join(", ", TSIG_HASH_ALGORITHMS) + "]: [" + value + "]");
        }
    }


    @Override
    public boolean supports(SMLPropertyEnum property) {
        switch (property) {
            case DNS_TSIG_KEY_NAME:
            case DNS_TSIG_KEY_VALUE:
            case DNS_TSIG_HASH_ALGORITHM:
                return true;
            default:
                return false;
        }
    }
}
