/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.business.impl;

import eu.europa.ec.bdmsl.business.IConfigurationBusiness;
import eu.europa.ec.bdmsl.business.IDataQualityInconsistencyAnalyzerBusiness;
import eu.europa.ec.bdmsl.common.bo.ServiceMetadataPublisherBO;
import eu.europa.ec.bdmsl.common.business.AbstractBusinessImpl;
import eu.europa.ec.bdmsl.common.exception.InconsistencyReportException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.common.reports.DataInconsistencyReport;
import eu.europa.ec.bdmsl.dao.IParticipantDAO;
import eu.europa.ec.bdmsl.dao.ISmpDAO;
import eu.europa.ec.bdmsl.service.dns.DataInconsistencyAnalyzer;
import eu.europa.ec.bdmsl.service.dns.IDnsClientService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.xbill.DNS.IRDnsRangeData;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Tiago MIGUEL
 * @author Flavio SANTOS
 * @since 22/12/2016
 */
@Component
public class DataQualityInconsistencyAnalyzerBusinessImpl extends AbstractBusinessImpl implements IDataQualityInconsistencyAnalyzerBusiness {

    /**
     * Set "max" line count in a file. To many lines in a batch would consume more memory, but to many files would
     * increase inconsistency report generation time.
     */
    public static final int MAX_LINE_COUNT_IN_CHUNK = 50000;

    private final static String ROOT_CAUSE = ". Root cause: ";

    private static final String ERROR_OCCURRED_WHILE_ANALYSING_DATA_FOR_INCONSISTENCIES = "Error occurred while analysing data for inconsistencies: ";

    @Autowired
    @Qualifier("SmpDAOImpl")
    private ISmpDAO smpDAO;

    @Autowired
    private IParticipantDAO participantDAO;

    @Autowired
    private IDnsClientService dnsClientService;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private IConfigurationBusiness configurationBusiness;


    private DataInconsistencyAnalyzer dataInconsistencyAnalyzer = new DataInconsistencyAnalyzer();

    @Override
    public DataInconsistencyReport checkDataInconsistencies() throws TechnicalException {
        setLog("\n## STARTING INCONSISTENCY DATA CHECKING TOOL ##");

        dataInconsistencyAnalyzer.clear();
        // get publisher domain part do distinct between smp record and cname
        String publisherName = "." + configurationBusiness.getDNSPublisherPrefix().toLowerCase() + ".";
        setLog("Get participant count from db.");


        long l = participantDAO.getParticipantCount();
        dataInconsistencyAnalyzer.setDbTotalOfParticipants((int) l);
        setLog("END participant count from db.");
        int numberOfRanges = (int) l / MAX_LINE_COUNT_IN_CHUNK;

        numberOfRanges = numberOfRanges == 0 ? 1 : numberOfRanges;
        // SMP count is not expected to be more than MAX_LINE_COUNT_IN_CHUNK.
        IRDnsRangeData smpData = new IRDnsRangeData(1);
        IRDnsRangeData cnameData = new IRDnsRangeData(numberOfRanges);
        IRDnsRangeData naptrData = new IRDnsRangeData(numberOfRanges);
        Date startReport = Calendar.getInstance().getTime();
        setLog("Start retrieving DNS records");
        try {
            int iCount = dnsClientService.getAllDNSDataForInconsistencyReport(publisherName, smpData, cnameData, naptrData);
            setLog("Got " + iCount + " DNS records (Smp: " + smpData.getAddedRowsDNS() + ", CNAME: "
                    + cnameData.getAddedRowsDNS() + ", NAPTR: " + naptrData.getAddedRowsDNS() +
                    ", Other:" + (iCount - smpData.getAddedRowsDNS() - cnameData.getAddedRowsDNS() - naptrData.getAddedRowsDNS()) + ")");

            dataInconsistencyAnalyzer.setDnsTotalOfSMPs(smpData.getAddedRowsDNS());
            dataInconsistencyAnalyzer.setDnsTotalOfParticipantsCNAME(cnameData.getAddedRowsDNS());
            dataInconsistencyAnalyzer.setDnsTotalOfParticipantsNAPTR(naptrData.getAddedRowsDNS());
        } catch (RuntimeException ex) {

            loggingService.error("InconsistencyReportException occurred:" + ex.getMessage()
                    + ROOT_CAUSE + ExceptionUtils.getRootCauseMessage(ex), null);
            setLog("Runtime exception occurred while retrieving data from DNS :" + ex.getMessage());
            DataInconsistencyAnalyzer.DataInconsistencyEntry dataInconsistencyData = new DataInconsistencyAnalyzer.DataInconsistencyEntry();
            dataInconsistencyData.setMissingType(DataInconsistencyAnalyzer.DataInconsistencyEntry.IssueType.ERROR);
            dataInconsistencyData.setErrorMessage(ERROR_OCCURRED_WHILE_ANALYSING_DATA_FOR_INCONSISTENCIES + ex.getMessage());
            dataInconsistencyAnalyzer.addInconsistencyNoLimitation(dataInconsistencyData);
        }

        try {
            setLog("Started to analise CNAME records.");
            analyzeCNameRecords(publisherName, cnameData, startReport);
            setLog("Started to analise naptr records.");
            analyzeNaptrRecords(naptrData, startReport);
            setLog("Started to analise SMP records.");
            analyzeSMPRecords(smpData);
        } catch (InconsistencyReportException ex) {
            loggingService.error("InconsistencyReportException occurred:" + ex.getMessage()
                    + ROOT_CAUSE + ExceptionUtils.getRootCauseMessage(ex), null);
            DataInconsistencyAnalyzer.DataInconsistencyEntry dataInconsistencyData = new DataInconsistencyAnalyzer.DataInconsistencyEntry();
            dataInconsistencyData.setMissingType(DataInconsistencyAnalyzer.DataInconsistencyEntry.IssueType.ERROR);
            dataInconsistencyData.setErrorMessage(ERROR_OCCURRED_WHILE_ANALYSING_DATA_FOR_INCONSISTENCIES + ex.getMessage());
            dataInconsistencyAnalyzer.addInconsistencyNoLimitation(dataInconsistencyData);
        } catch (RuntimeException ex) {
            loggingService.error("Runtime exception occurred:" + ex.getMessage()
                    + ROOT_CAUSE + ExceptionUtils.getRootCauseMessage(ex), null);
            DataInconsistencyAnalyzer.DataInconsistencyEntry dataInconsistencyData = new DataInconsistencyAnalyzer.DataInconsistencyEntry();
            dataInconsistencyData.setMissingType(DataInconsistencyAnalyzer.DataInconsistencyEntry.IssueType.ERROR);
            dataInconsistencyData.setErrorMessage(ERROR_OCCURRED_WHILE_ANALYSING_DATA_FOR_INCONSISTENCIES + ex.getMessage());
            dataInconsistencyAnalyzer.addInconsistencyNoLimitation(dataInconsistencyData);

        }
        // make sure files are deleted
        naptrData.deleteFiles();
        cnameData.deleteFiles();
        smpData.deleteFiles();


        DataInconsistencyReport dataInconsistencyReport = applicationContext.getBean(DataInconsistencyReport.class);
        setLog("Started creating the report.");
        dataInconsistencyReport.createReport(dataInconsistencyAnalyzer);
        setLog("Ended creating the report.\n");
        setLog("## ENDING INCONSISTENCY DATA CHECKING TOOL ## \n");
        return dataInconsistencyReport;
    }


    public void analyzeCNameRecords(String publisherDomain, IRDnsRangeData cnameData, Date createDateTo) {
        // get data from database
        participantDAO.retrieveCNameDBDataForInconsistencyReport(publisherDomain, cnameData, createDateTo);
        dataInconsistencyAnalyzer.setDbTotalOfParticipantsCNAME(cnameData.getAddedRowsDB());
        dataInconsistencyAnalyzer.setDbTotalOfDisabledParticipantsCNAME(cnameData.getDisabledDBEntityCount());
        // flush data to make sure everything is written to file before analise the files
        cnameData.flush();
        // compare records
        dataInconsistencyAnalyzer.compareCNameRecords(cnameData);
        // clean the files
        cnameData.deleteFiles();
    }

    public void analyzeSMPRecords(IRDnsRangeData smpData) throws TechnicalException {
        // get data from database
        List<ServiceMetadataPublisherBO> smps = smpDAO.listSMPs();
        int iInactiveSMPs = 0;
        for (ServiceMetadataPublisherBO smp : smps) {
            if (smp.isDisabled()) {
                iInactiveSMPs++;
                continue;
            }
            String host;
            // smp record points to host of logical address!
            try {
                host = new URL(smp.getLogicalAddress()).getHost();
            } catch (MalformedURLException e) {
                throw new InconsistencyReportException("Logical address " + smp.getLogicalAddress() + " of SMP: " + smp.getSmpId() + " is malformed: " + e.getMessage(), e);
            }
            // domain name and hosts(cname or A target) are case-insensitive Put it to lowercase for comparing
            // same is done when receiving dns records.
            String record = StringUtils.lowerCase(smp.getDnsRecord()) + "|" + StringUtils.lowerCase(host);
            // add db entry
            smpData.addDBEntry(record, smp.getSmpId());

        }
        dataInconsistencyAnalyzer.setDbTotalOfSMPs(smpData.getAddedRowsDB());
        dataInconsistencyAnalyzer.setDbTotalInactiveSMPs(iInactiveSMPs);
        // flush data to make sure everything is written to file before analise the files
        smpData.flush();
        // compare records
        dataInconsistencyAnalyzer.compareSMPRecords(smpData);
        // clean the files
        smpData.deleteFiles();
    }

    public void analyzeNaptrRecords(IRDnsRangeData naptrData, Date createDateTo) {
        // get data from database
        participantDAO.retrieveNaptrDBDataForInconsistencyReport(naptrData, createDateTo);
        dataInconsistencyAnalyzer.setDbTotalOfParticipantsNAPTR(naptrData.getAddedRowsDB());
        dataInconsistencyAnalyzer.setDbTotalOfDisabledParticipantsNAPTR(naptrData.getDisabledDBEntityCount());
        // flush data to make sure everything is written to file before analise the files
        naptrData.flush();
        // compare records
        dataInconsistencyAnalyzer.compareNaptrRecords(naptrData);
        // clean the files
        naptrData.deleteFiles();
    }

    @Override
    public int updateNextBatchHashValuesForParticipants() throws TechnicalException {
        return participantDAO.updateNextBatchHashValuesForParticipants();
    }


    private void setLog(String event) {
        loggingService.info(DataInconsistencyReport.LOG_PREFIX + event);
    }
}

