/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2017 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.conversion;

import eu.europa.ec.bdmsl.common.bo.WildcardBO;
import eu.europa.ec.bdmsl.dao.entity.AllowedWildcardEntity;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

/**
 * This converter class transforms AllowedWildcardEntity objects into WildcardBO objects.
 * <p/>
 *
 * @author Thomas Dussart
 * @since 24/05/2024
 */
@Component
public class AllowedWildcardEntityToWildcardBOConverter extends AutoRegisteringConverter<AllowedWildcardEntity, WildcardBO> {

    public AllowedWildcardEntityToWildcardBOConverter(ConversionService conversionService) {
        super(conversionService);
    }

    @Override
    public WildcardBO convert(AllowedWildcardEntity source) {
        if (source == null) {
            return null;
        }

        WildcardBO target = new WildcardBO();
        BeanUtils.copyProperties(source, target);

        // Custom mapping for certificateId
        target.setCertificateId(source.getCertificate().getCertificateId());

        return target;
    }
}
