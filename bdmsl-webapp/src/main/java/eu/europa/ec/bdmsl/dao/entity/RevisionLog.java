/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.entity;


import eu.europa.ec.bdmsl.dao.impl.SMLRevisionListener;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.envers.RevisionEntity;
import org.hibernate.envers.RevisionNumber;
import org.hibernate.envers.RevisionTimestamp;

import jakarta.persistence.*;
import java.time.LocalDateTime;

/**
 * Implementation of hibernate envers Revision log entity.
 *
 * @author Joze RIHTARSIC
 * @author Thomas DUSSART
 * @since 4.0
 */
@Entity
@Table(name = "bdmsl_rev_info")
@RevisionEntity(SMLRevisionListener.class)
public class RevisionLog {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "bdmsl_rev_info_seq")
    // strategy native uses sequence in oracle and auto_increment on mysql
    // from hibernate 5.4 name is also the name of sequence, before it was hibernate
    @GenericGenerator(name = "bdmsl_rev_info_seq", strategy = "native")
    @RevisionNumber
    private long id;

    @RevisionTimestamp
    private long timestamp;
    /**
     * User involve in this modification
     */
    @Column(name = "USERNAME")
    private String userName;
    /**
     * Date of the modification.
     */
    @Column(name = "REVISION_DATE")
    private LocalDateTime revisionDate;


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public LocalDateTime getRevisionDate() {
        return revisionDate;
    }

    public void setRevisionDate(LocalDateTime revisionDate) {
        this.revisionDate = revisionDate;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public long getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (!(o instanceof RevisionLog)) {
            return false;
        } else {
            RevisionLog that = (RevisionLog) o;
            return this.id == that.id && this.timestamp == that.timestamp;
        }
    }

    public int hashCode() {
        int result = (int) this.id;
        result = 31 * result + (int) (this.timestamp ^ this.timestamp >>> 32);
        return result;
    }


}
