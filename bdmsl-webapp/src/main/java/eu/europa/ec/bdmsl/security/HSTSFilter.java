/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2017 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.security;

import eu.europa.ec.bdmsl.business.IConfigurationBusiness;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * In this class we define a servlet filter that allows the configuration of HSTS headers.
 * <p/>
 *
 * @author Thomas Dussart
 * @since 14/06/2024
 */

@Component
public class HSTSFilter implements Filter {

    private static final Logger LOG = LoggerFactory.getLogger(HSTSFilter.class);
    public static final String MAX_AGE_KEY = "max-age=";
    public static final String INCLUDE_SUB_DOMAINS = "includeSubDomains";
    public static final String PRELOAD = "preload";

    @Autowired
    private IConfigurationBusiness configurationBusiness;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        LOG.debug("Initializing HSTSFilter");
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
                filterConfig.getServletContext());
        LOG.debug("HSTSFilter initialization completed");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        LOG.debug("HSTSFilter executing doFilter method");
        if (!configurationBusiness.isHSTSEnabled()) {
            LOG.debug("HSTS is not enabled");
            chain.doFilter(request, response);
            return;
        }

        LOG.debug("HSTS is enabled");
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        String stsValue = MAX_AGE_KEY + configurationBusiness.getHSTSMaxAge();
        LOG.debug("HSTS max-age set to: {}", configurationBusiness.getHSTSMaxAge());

        if (configurationBusiness.isHSTSConfiguredWithIncludeSubDomain()) {
            stsValue = String.join(";", stsValue, INCLUDE_SUB_DOMAINS);
            LOG.debug("HSTS includeSubDomains is configured");
        }

        if (configurationBusiness.isHSTSConfiguredWithPreload()) {
            stsValue = String.join("; ", stsValue, PRELOAD);
            LOG.debug("HSTS preload is configured");
        }

        httpServletResponse.setHeader("Strict-Transport-Security", stsValue);
        LOG.debug("HSTS header set to: {}", stsValue);
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        LOG.debug("Destroying HSTSFilter");
        // Cleanup logic if needed
    }
}
