/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.enums;

import static eu.europa.ec.bdmsl.common.util.Constant.DEF_PROPERTY_VALUE_VALIDATION_REGEXP;

public enum SMLPropertyTypeEnum {

    STRING(DEF_PROPERTY_VALUE_VALIDATION_REGEXP, "Property value [%s] must be less than 2000 characters!"),
    LIST_STRING(DEF_PROPERTY_VALUE_VALIDATION_REGEXP,"Property [%s] is not valid LIST_STRING type!"),
    INTEGER("\\d{0,12}", "Property [%s] is not valid Integer!"),
    BOOLEAN("true|false", "Property [%s] is not valid Boolean type!"),
    REGEXP(DEF_PROPERTY_VALUE_VALIDATION_REGEXP, "Property [%s] is not valid Regular Expression type!"),
    CRON_EXPRESSION(DEF_PROPERTY_VALUE_VALIDATION_REGEXP, "Property [%s] is not valid Cron Expression type!"),
    EMAIL(DEF_PROPERTY_VALUE_VALIDATION_REGEXP, "Property [%s] is not valid Email address type!"),
    FILENAME(DEF_PROPERTY_VALUE_VALIDATION_REGEXP, "Property [%s] is not valid Filename type or it does not exist!"),
    PATH(DEF_PROPERTY_VALUE_VALIDATION_REGEXP, "Property [%s] is not valid Path type or it does not exist!"),
    URL(DEF_PROPERTY_VALUE_VALIDATION_REGEXP, "Property [%s] is not valid URL!");


    protected final String defValidationRegExp;
    protected final String errorTemplate;

    SMLPropertyTypeEnum(String defValidationRegExp, String errorTemplate) {
        this.defValidationRegExp = defValidationRegExp;
        this.errorTemplate = errorTemplate;
    }

    public String getMessage(String property) {
        return String.format(errorTemplate, property);
    }

    public String getDefValidationRegExp() {
        return defValidationRegExp;
    }
}
