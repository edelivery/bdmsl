/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.ws.soap.impl;

import ec.services.wsdl.bdmsl.data._1.ExistsParticipant;
import ec.services.wsdl.bdmsl.data._1.ExistsParticipantResponse;
import ec.services.wsdl.bdmsl.data._1.PrepareChangeCertificate;
import ec.services.wsdl.bdmsl.data._1.SMPAdvancedServiceForParticipantService;
import eu.europa.ec.bdmsl.business.IManageServiceMetadataBusiness;
import eu.europa.ec.bdmsl.common.bo.CertificateBO;
import eu.europa.ec.bdmsl.common.bo.ParticipantBO;
import eu.europa.ec.bdmsl.common.bo.PrepareChangeCertificateBO;
import eu.europa.ec.bdmsl.common.bo.ServiceMetadataPublisherBO;
import eu.europa.ec.bdmsl.common.exception.BadRequestException;
import eu.europa.ec.bdmsl.common.exception.CertificateNotFoundException;
import eu.europa.ec.bdmsl.common.exception.Severity;
import eu.europa.ec.bdmsl.common.util.LogEvents;
import eu.europa.ec.bdmsl.service.IManageCertificateService;
import eu.europa.ec.bdmsl.service.IManageParticipantIdentifierService;
import eu.europa.ec.bdmsl.util.JaxbUtils;
import eu.europa.ec.bdmsl.ws.soap.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.ConversionService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import jakarta.jws.WebService;
import static eu.europa.ec.bdmsl.common.exception.Messages.BAD_REQUEST_PARAMETERS_EMPTY;

@Service
@WebService(endpointInterface = "eu.europa.ec.bdmsl.ws.soap.IBDMSLServiceWS")
public class BDMSLServiceWSImpl extends AbstractWSImpl implements IBDMSLServiceWS {

    @Autowired
    @Qualifier(value = "manageParticipantIdentifierServiceImpl")
    private IManageParticipantIdentifierService manageParticipantIdentifierService;

    @Autowired
    private IManageServiceMetadataBusiness manageServiceMetadataBusiness;

    @Autowired
    private IManageCertificateService manageCertificateService;

    @Autowired
    private ConversionService conversionService;

    @Override
    public void createParticipantIdentifier(
            SMPAdvancedServiceForParticipantService participantType
    ) throws InternalErrorFault, NotFoundFault, UnauthorizedFault, BadRequestFault {
        loggingService.info("Calling BDMSLServiceWSImpl.createParticipantIdentifier with participantType=" + JaxbUtils.serializeJaxbEntity(participantType));
        try {
            if (participantType == null) {
                throw new BadRequestException(BAD_REQUEST_PARAMETERS_EMPTY);
            } else if (participantType.getServiceName() == null || participantType.getServiceName().trim().isEmpty()) {
                throw new BadRequestException("The serviceName must not be null or empty ");
            }
            ParticipantBO participantBO = conversionService.convert(participantType, ParticipantBO.class);
            manageParticipantIdentifierService.create(participantBO);
            loggingService.businessLog(LogEvents.BUS_PARTICIPANT_CREATED, participantBO.getParticipantId());
        } catch (Exception exc) {
            // convert the exception to the associated SOAP fault
            loggingService.businessLog(Severity.ERROR, LogEvents.BUS_PARTICIPANT_CREATION_FAILED, JaxbUtils.serializeJaxbEntity(participantType));
            loggingService.error(exc.getMessage(), exc);
            handleException(exc);
        }
    }

    @Override
    public void prepareChangeCertificate(
            PrepareChangeCertificate prepareChangeCertificate
    ) throws InternalErrorFault, NotFoundFault, UnauthorizedFault, BadRequestFault {
        loggingService.info("Calling BDMSLServiceWSImpl.prepareChangeCertificate with prepareChangeCertificate=" + JaxbUtils.serializeJaxbEntity(prepareChangeCertificate));
        try {
            if (prepareChangeCertificate == null) {
                throw new BadRequestException(BAD_REQUEST_PARAMETERS_EMPTY);
            }

            String certificateName = SecurityContextHolder.getContext().getAuthentication().getName();
            CertificateBO certificateId = manageCertificateService.findCertificate(certificateName);
            if (certificateId == null) {
                throw new CertificateNotFoundException("The certificate " + certificateName + " couldn't be found");
            }

            PrepareChangeCertificateBO prepareChangeCertificateBO = conversionService.convert(prepareChangeCertificate, PrepareChangeCertificateBO.class);
            manageCertificateService.prepareChangeCertificate(prepareChangeCertificateBO);
            loggingService.businessLog(LogEvents.BUS_CERTIFICATE_CHANGED, prepareChangeCertificateBO.getCurrentCertificate().getCertificateId());
        } catch (Exception exc) {
            // convert the exception to the associated SOAP fault
            loggingService.businessLog(Severity.ERROR, LogEvents.BUS_CERTIFICATE_CHANGE_FAILED, SecurityContextHolder.getContext().getAuthentication().getName());
            loggingService.error(exc.getMessage(), exc);
            handleException(exc);
        }
    }

    @Override
    public ExistsParticipantResponse existsParticipantIdentifier(ExistsParticipant participantType) throws NotFoundFault, BadRequestFault, UnauthorizedFault, InternalErrorFault {
        loggingService.info("Calling BDMSLServiceWSImpl.existsParticipantIdentifier with participantType=" + JaxbUtils.serializeJaxbEntity(participantType));
        ExistsParticipantResponse response = new ExistsParticipantResponse();
        try {
            if (participantType == null) {
                throw new BadRequestException(BAD_REQUEST_PARAMETERS_EMPTY);
            } else if (participantType.getParticipantIdentifier() == null
                    || StringUtils.isBlank(participantType.getParticipantIdentifier().getValue())) {
                throw new BadRequestException("The participant identifier must not be null or empty");
            } else if (StringUtils.isBlank(participantType.getServiceMetadataPublisherID())) {
                throw new BadRequestException("The service metadata must not be null or empty");
            }

            ParticipantBO participantBO = conversionService.convert(participantType, ParticipantBO.class);
            manageServiceMetadataBusiness.validateSMPId(participantBO.getSmpId());
            ServiceMetadataPublisherBO existingSmpBO = manageServiceMetadataBusiness.verifySMPExist(participantBO.getSmpId());
            manageServiceMetadataBusiness.validateAuthorization(existingSmpBO);

            ParticipantBO result = manageParticipantIdentifierService.getParticipant(participantBO);
            loggingService.businessLog(LogEvents.BUS_PARTICIPANT_FOUND, participantBO.getParticipantId(), Boolean.toString(result != null));
            response.setParticipantIdentifier(participantType.getParticipantIdentifier());
            response.setServiceMetadataPublisherID(participantType.getServiceMetadataPublisherID());
            response.setExist(result != null);
        } catch (Exception exc) {
            // convert the exception to the associated SOAP fault
            loggingService.businessLog(Severity.ERROR, LogEvents.BUS_PARTICIPANT_FOUND_FAILED, JaxbUtils.serializeJaxbEntity(participantType));
            loggingService.error(exc.getMessage(), exc);
            handleException(exc);
        }
        return response;
    }
}
