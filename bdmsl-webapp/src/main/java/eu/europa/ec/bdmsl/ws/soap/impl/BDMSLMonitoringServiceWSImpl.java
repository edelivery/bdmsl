/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.ws.soap.impl;

import ec.services.wsdl.bdmsl.monitoring.data._1.IsAlive;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.ws.soap.*;
import jakarta.jws.WebService;
import eu.europa.ec.bdmsl.service.IBDMSLMonitoringService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;

/**
 * @author Sebastian-Ion TINCU
 * @since 5.0
 */
@Service
@WebService(endpointInterface = "eu.europa.ec.bdmsl.ws.soap.IBDMSLMonitoringServiceWS")
public class BDMSLMonitoringServiceWSImpl extends AbstractWSImpl implements IBDMSLMonitoringServiceWS {

    @Autowired
    private IBDMSLMonitoringService bdmslMonitoringService;

    @Override
    public void isAlive(IsAlive messagePart) throws InternalErrorFault {
        loggingService.info("Calling BDMSLMonitoringServiceWSImpl.isAlive");
        try {
            bdmslMonitoringService.isAlive();
        } catch (TechnicalException e) {
            loggingService.error(e.getMessage(), e);
            try {
                handleException(e);
            } catch (NotFoundFault notFoundFault) {
                // never happens
                throw new InternalErrorFault(RequestContextHolder.currentRequestAttributes().getSessionId(), notFoundFault.getFaultInfo(), notFoundFault);
            } catch (UnauthorizedFault unauthorizedFault) {
                // never happens
                throw new InternalErrorFault(RequestContextHolder.currentRequestAttributes().getSessionId(), unauthorizedFault.getFaultInfo(), unauthorizedFault);
            } catch (BadRequestFault badRequestFault) {
                // never happens
                throw new InternalErrorFault(RequestContextHolder.currentRequestAttributes().getSessionId(), badRequestFault.getFaultInfo(), badRequestFault);
            }
        }
    }
}
