/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.dns.impl;

import eu.europa.ec.bdmsl.business.IConfigurationBusiness;
import eu.europa.ec.bdmsl.common.exception.DNSClientException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.service.dns.IDnsMessageSenderService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xbill.DNS.*;

import java.io.IOException;
import java.net.UnknownHostException;

/**
 * @author Adrien FERIAL
 * @since 24/06/2015
 */
@Service
public class DnsMessageSenderServiceImpl implements IDnsMessageSenderService {
    private static final Logger LOG = LoggerFactory.getLogger(DnsMessageSenderServiceImpl.class);
    IConfigurationBusiness configurationBusiness;

    @Autowired
    public DnsMessageSenderServiceImpl(IConfigurationBusiness configurationBusiness) {
        this.configurationBusiness = configurationBusiness;
    }

    @Override
    public Message sendMessage(Message message) throws TechnicalException {

        String dnsServer = configurationBusiness.getDNSServer();
        Resolver res = createSimpleResolverForDNSHost(dnsServer);
        if (configurationBusiness.isDNSTSigEnabled()) {
            LOG.debug("TSIG is enabled for the DNS call");
            TSIG tsig = createTSIGForDNSHost();
            message.setTSIG(tsig);
            res.setTSIGKey(tsig);
        }

        int tcpTimeout = configurationBusiness.getDNSTCPTimeout();
        res.setTimeout(tcpTimeout);
        res.setTCP(true);
        try {
            return res.send(message);
        } catch (IOException exc) {
            throw new DNSClientException("Couldn't send the message to the DNS server: " + dnsServer, exc);
        }
    }

    protected TSIG createTSIGForDNSHost() throws DNSClientException {
        String tsigKeyName = StringUtils.appendIfMissing(configurationBusiness.getDNSTSigName(), ".");
        String algorithm = configurationBusiness.getDNSTSigAlgorithm();
        String dnsServer = configurationBusiness.getDNSServer();
        LOG.info("Create TSIG signature for  the DNS call for name: [{}], algorithm: [{}] and DNS server: [{}]",
                tsigKeyName, algorithm, dnsServer);

        Name algorithmName = TSIG.algorithmToName(algorithm);
        Name keyName = null;
        try {
            LOG.info("Create TSIG signature for  the DNS call for name: [{}]", tsigKeyName);
            keyName = Name.fromString(tsigKeyName);
        } catch (TextParseException e) {
            LOG.info("Create TSIG signature for  the DNS call");
            throw new DNSClientException("Couldn't send the message to the DNS server: " + dnsServer, e);
        }
        return new TSIG(algorithmName,
                keyName,
                configurationBusiness.getDNSTSigKeyValue());
    }

    protected Resolver createSimpleResolverForDNSHost(String dnsServer) throws DNSClientException {
        try {
            SimpleResolver resolver = new SimpleResolver(dnsServer);
            // disable EDNS to avoid problems breaking the signed update requests
            resolver.setEDNS(-1);
            return resolver;
        } catch (UnknownHostException exc) {
            throw new DNSClientException("Invalid DNS server : '" + dnsServer + "'", exc);
        }
    }
}
