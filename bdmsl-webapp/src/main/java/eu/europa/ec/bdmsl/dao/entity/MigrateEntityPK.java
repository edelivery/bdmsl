/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.entity;

import eu.europa.ec.bdmsl.dao.utils.ColumnDescription;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Adrien FERIAL
 * @since 15/06/2015
 */
@Embeddable
public class MigrateEntityPK implements Serializable {

    @Column(name = "scheme", length = CommonColumnsLengths.MAX_PARTICIPANT_IDENTIFIER_SCHEME_LENGTH, nullable = true)
    @ColumnDescription(comment = "The scheme of the participant identifier to be migrated")
    private String scheme;

    @Column(name = "participant_id", length = CommonColumnsLengths.MAX_PARTICIPANT_IDENTIFIER_VALUE_LENGTH, nullable = false)
    @ColumnDescription(comment = "The participant identifier to be migrated")
    private String participantId;

    @Column(name = "migration_key", length = CommonColumnsLengths.MAX_MIGRATION_KEY, nullable = false)
    @ColumnDescription(comment = "The migration key. It's a code that must be passed out-of-band to the SMP which is taking over the publishing of the metadata for the participant identifier.")
    private String migrationKey;

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getParticipantId() {
        return participantId;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }

    public String getMigrationKey() {
        return migrationKey;
    }

    public void setMigrationKey(String migrationKey) {
        this.migrationKey = migrationKey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MigrateEntityPK)) return false;
        MigrateEntityPK that = (MigrateEntityPK) o;
        return Objects.equals(scheme, that.scheme) &&
                Objects.equals(participantId, that.participantId) &&
                Objects.equals(migrationKey, that.migrationKey);
    }

    @Override
    public int hashCode() {
        return Objects.hash(scheme, participantId, migrationKey);
    }
}
