/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2017 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.conversion;

import eu.europa.ec.bdmsl.common.bo.ParticipantBO;
import eu.europa.ec.bdmsl.common.bo.ParticipantListBO;
import org.apache.commons.lang3.StringUtils;
import org.busdox.servicemetadata.locator._1.ParticipantIdentifierPageType;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * This converter class transforms ParticipantIdentifierPageType objects into ParticipantListBO objects.
 * <p/>
 * Note: WS Type to BO is converted manually and strings are trimmed!
 *
 * @author Thomas Dussart
 * @since 24/05/2024
 */
@Component
public class ParticipantIdentifierPageTypeToParticipantListBOConverter extends AutoRegisteringConverter<ParticipantIdentifierPageType, ParticipantListBO> {

    public ParticipantIdentifierPageTypeToParticipantListBOConverter(ConversionService conversionService) {
        super(conversionService);
    }

    @Override
    public ParticipantListBO convert(ParticipantIdentifierPageType source) {
        if (source == null) {
            return null;
        }

        ParticipantListBO target = new ParticipantListBO();
        String smpId = StringUtils.trim(source.getServiceMetadataPublisherID());

        // Convert the list of ParticipantIdentifierType to ParticipantBO
        List<ParticipantBO> participantBOList = source.getParticipantIdentifiers().stream()
                .map(participantIdentifierType -> {
                    ParticipantBO participantBO = getConversionService().convert(participantIdentifierType, ParticipantBO.class);
                    if (participantBO != null) {
                        participantBO.setSmpId(smpId);
                    }
                    return participantBO;
                })
                .collect(Collectors.toList());

        target.setParticipantBOList(participantBOList);
        target.setNextPage(source.getNextPageIdentifier());

        return target;
    }
}
