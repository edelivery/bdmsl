/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.enums;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public enum UrlSchemeSubDomainTypeEnum {

    HTTPS("https"),
    HTTP("http"),
    ALL("all"),
    ;

    String code;

    private UrlSchemeSubDomainTypeEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public static Optional<UrlSchemeSubDomainTypeEnum> getByCode(String val) {
        if (StringUtils.isBlank(val)) {
            return Optional.empty();
        }
        String rec = val.trim().toLowerCase();
        return Arrays.asList(values()).stream().filter(enm -> enm.getCode().equalsIgnoreCase(rec)).findAny();
    }

    public static List<String> getCodeList() {
        UrlSchemeSubDomainTypeEnum[] val = values();
        List<String> allCodes = new ArrayList<>();
        Arrays.asList(val).forEach(enm -> allCodes.add(enm.getCode()));
        return allCodes;
    }
}
