/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.business;

import eu.europa.ec.bdmsl.common.bo.*;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.common.identifier.ParticipantIdentifier;

import java.util.List;
import java.util.Optional;

/**
 * @author Adrien FERIAL
 * @since 12/06/2015
 */
public interface IManageParticipantIdentifierBusiness {

    ParticipantIdentifier normalizeAndValidateParticipant(ParticipantBO participantBO) throws TechnicalException;

    void createParticipant(ParticipantBO participantBO) throws TechnicalException;

    void createParticipantList(String publisherId, List<ParticipantBO> participantBO) throws TechnicalException;

    ParticipantBO findParticipant(ParticipantBO participantBO) throws TechnicalException;

    Optional<ParticipantBO> getParticipantForDomain(ParticipantBO participantBO, SubdomainBO subdomainBO) throws TechnicalException;

    Optional<ParticipantBO> getParticipantForDifferentSMPOnDomain(ParticipantBO participantBO, ServiceMetadataPublisherBO existingSmpBO) throws TechnicalException;

    void validatePageRequest(PageRequestBO pageRequestBO) throws TechnicalException;

    ParticipantListBO list(PageRequestBO pageRequestBO) throws TechnicalException;

    long getParticipantCountForSMP(ServiceMetadataPublisherBO smpBO);

    long getParticipantCountForSMPAndStatus(ServiceMetadataPublisherBO smpBO, boolean disabled);

    long getParticipantCountForSMPDomain(ServiceMetadataPublisherBO smpBO) throws TechnicalException;

    void validateMigrationRecord(MigrationRecordBO prepareToMigrateBO) throws TechnicalException;

    void prepareToMigrate(MigrationRecordBO prepareToMigrateBO) throws TechnicalException;

    void performMigration(MigrationRecordBO migrateBO) throws TechnicalException;

    MigrationRecordBO findNonMigratedRecord(MigrationRecordBO migrateBO) throws TechnicalException;

    ParticipantListBO list() throws TechnicalException;

    List<ParticipantBO> getAllParticipantsForSMP(ServiceMetadataPublisherBO smpBO);

    List<ParticipantBO> getParticipantsForSMP(ServiceMetadataPublisherBO smpBO, int page, int pageSize);

    List<ParticipantBO> findParticipants(String smpId, List<ParticipantBO> participantBOList) throws TechnicalException;

    void deleteParticipantList(String smpId, List<ParticipantBO> participantBOList) throws TechnicalException;
    void validateParticipantBOList(ParticipantListBO participantListBO) throws TechnicalException;
    void validateParticipantBOList(List<ParticipantBO> participantListBO) throws TechnicalException;
    void validateFreeQuotaForSMP(int iCount, ServiceMetadataPublisherBO existingSmpBO) throws TechnicalException;

    void checkNoMigrationPlanned(String smpId, List<ParticipantBO> participantBOList) throws TechnicalException;

    // batch tasks
    List<ParticipantBO> updateNextParticipantsBatchStatusForSMP(ServiceMetadataPublisherBO metadataPublisherBO, boolean disabled) throws TechnicalException;

    // batch tasks
    List<ParticipantBO> deleteNextParticipantsBatchForSMP(ServiceMetadataPublisherBO metadataPublisherBO) throws TechnicalException;

    // batch tasks
    List<ParticipantBO> updateNaptrRecordsForNextParticipantsBatchSMP(ServiceMetadataPublisherBO newSMP, ServiceMetadataPublisherBO oldSMP) throws TechnicalException;

}
