/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.business;

import eu.europa.ec.bdmsl.common.bo.ParticipantBO;
import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import eu.europa.ec.bdmsl.common.identifier.ParticipantIdentifier;
import eu.europa.ec.bdmsl.dao.entity.ParticipantIdentifierEntity;
import eu.europa.ec.dynamicdiscovery.enums.DNSLookupHashType;

/**
 * Interface for identifier handling such as normalization, validation,  DNS formatting etc.
 * @author Joze RIHTARSIC
 * @since 4.3
 **/
public interface IIdentifierFormatterBusiness {

    ParticipantIdentifier normalizeForDomain(ParticipantBO participantBO, SubdomainBO subdomainBO);
    String dnsLookupFormat(String scheme, String identifier, DNSLookupHashType dnsLookupHashType);
    void updateHashValuesForParticipant(ParticipantIdentifierEntity participantIdentifierEntity);
    boolean isSchemeNotCaseSensitive(String scheme);
}
