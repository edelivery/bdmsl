/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.entity;

import eu.europa.ec.bdmsl.dao.entity.reports.ExpiredSMPEntity;
import eu.europa.ec.bdmsl.dao.utils.ColumnDescription;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NaturalId;
import org.hibernate.envers.Audited;

import jakarta.persistence.*;
import java.math.BigInteger;
import java.time.OffsetDateTime;
import java.util.Objects;

import static eu.europa.ec.bdmsl.dao.QueryNames.*;

/**
 * @author Adrien FERIAL
 * @since 15/06/2015
 */
@Entity
@Audited
@Table(name = "bdmsl_smp")
@NamedQueries({
        @NamedQuery(name = SMP_ENTITY_GET_ALL,
                query = "SELECT smp FROM SmpEntity smp"),
        @NamedQuery(name = SMP_ENTITY_GET_BY_SMP_ID,
                query = "SELECT smp from SmpEntity smp where upper(smp.smpId) = upper(:smpId)"),
        @NamedQuery(name = SMP_ENTITY_GET_BY_CERTIFICATE_ID,
                query = "SELECT smp from SmpEntity smp where certificate.id = :certificateId"),

})
@NamedNativeQueries({
        @NamedNativeQuery(name = "ExpiredSMPEntity.getAllSMPsWithExpiredCerts",
                query = "SELECT " +
                        "    dt.fk_smp_id AS fk_smp_id, " +
                        "    dt.smp_id AS smp_id, " +
                        "    dt.created_on AS created_on, " +
                        "    max(prt.created_on) AS last_added_participant_on, " +
                        "    dt.certificate_id AS certificate_id, " +
                        "    dt.cert_valid_from AS cert_valid_from, " +
                        "    dt.cert_valid_until AS cert_valid_until, " +
                        "    COUNT(prt.id) AS participant_count, " +
                        "    dom.subdomain_name AS subdomain_name " +
                        "FROM " +
                        "    (SELECT " +
                        "            smp.id AS fk_smp_id, " +
                        "            smp.fk_subdomain_id AS fk_subdomain_id, " +
                        "            smp.smp_id AS smp_id, " +
                        "            smp.created_on AS created_on, " +
                        "            smp.last_updated_on AS last_updated_on, " +
                        "            crt.certificate_id AS certificate_id, " +
                        "            crt.valid_from AS cert_valid_from, " +
                        "            crt.valid_until AS cert_valid_until " +
                        "        FROM " +
                        "            bdmsl_smp smp INNER JOIN bdmsl_certificate crt ON smp.fk_certificate_id = crt.id " +
                        "        WHERE  crt.valid_from > :currentDate OR crt.valid_until < :currentDate " +
                        "    ) dt INNER JOIN bdmsl_subdomain dom ON dt.fk_subdomain_id = dom.subdomain_id " +
                        "    LEFT JOIN bdmsl_participant_identifier prt ON dt.fk_smp_id = prt.fk_smp_id  " +
                        "GROUP BY " +
                        "    dt.fk_smp_id, dt.created_on,dt.last_updated_on, dt.smp_id, dt.certificate_id, dt.cert_valid_from, dt.cert_valid_until, dom.subdomain_name " +
                        "ORDER BY dt.smp_id, dt.certificate_id",
                resultSetMapping = "ExpiredSMPEntity"),
})
@SqlResultSetMappings({
        @SqlResultSetMapping(name = "ExpiredSMPEntity",
                classes = {
                        @ConstructorResult(targetClass = ExpiredSMPEntity.class,
                                columns = {
                                        @ColumnResult(name = "smp_id", type = String.class),
                                        @ColumnResult(name = "created_on", type = OffsetDateTime.class),
                                        @ColumnResult(name = "last_added_participant_on", type = OffsetDateTime.class),
                                        @ColumnResult(name = "certificate_id", type = String.class),
                                        @ColumnResult(name = "cert_valid_from", type = OffsetDateTime.class),
                                        @ColumnResult(name = "cert_valid_until", type = OffsetDateTime.class),
                                        @ColumnResult(name = "subdomain_name", type = String.class),
                                        @ColumnResult(name = "participant_count", type = BigInteger.class)
                                })
                })
})
public class SmpEntity extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "bdmsl_smp_seq")
    @GenericGenerator(name = "bdmsl_smp_seq", strategy = "native")
    @Column(name = "id")
    @ColumnDescription(comment = "Surrogate key for SMP entity")
    Long id;

    @NaturalId(mutable = true)
    @Column(name = "smp_id", length = CommonColumnsLengths.MAX_SML_SMP_ID_LENGTH, nullable = false, unique = true)
    @ColumnDescription(comment = "SMP identifier used by SMP instance to manage data")
    private String smpId;

    @JoinColumn(name = "fk_certificate_id", nullable = false)
    @ColumnDescription(comment = "The The certificate used by this SMP")
    @ManyToOne
    private CertificateEntity certificate;

    @Column(name = "endpoint_physical_address")
    @ColumnDescription(comment = "The physical address of the endpoint server.")
    private String endpointPhysicalAddress;

    @Column(name = "endpoint_logical_address")
    @ColumnDescription(comment = "URL of SMP server. Value is used for generating NAPTR and domain from URL for CNAME records.")
    private String endpointLogicalAddress;

    @Column(name = "smp_disabled", nullable = false)
    @ColumnDefault("0")
    @ColumnDescription(comment = "Is the SMP disabled. If disabled, no new participant can be registered on the SMP.")
    private boolean disabled = false;

    @JoinColumn(name = "fk_subdomain_id", nullable = false)
    @ManyToOne
    private SubdomainEntity subdomain;

    @Override
    public Object getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSmpId() {
        return smpId;
    }

    public void setSmpId(String smpId) {
        this.smpId = smpId;
    }

    public CertificateEntity getCertificate() {
        return certificate;
    }

    public void setCertificate(CertificateEntity certificate) {
        this.certificate = certificate;
    }

    public String getEndpointPhysicalAddress() {
        return endpointPhysicalAddress;
    }

    public void setEndpointPhysicalAddress(String endpointPhysicalAddress) {
        this.endpointPhysicalAddress = endpointPhysicalAddress;
    }

    public String getEndpointLogicalAddress() {
        return endpointLogicalAddress;
    }

    public void setEndpointLogicalAddress(String endpointLogicalAddress) {
        this.endpointLogicalAddress = endpointLogicalAddress;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean active) {
        this.disabled = active;
    }

    public SubdomainEntity getSubdomain() {
        return subdomain;
    }

    public void setSubdomain(SubdomainEntity subdomain) {
        this.subdomain = subdomain;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SmpEntity)) return false;
        SmpEntity smpEntity = (SmpEntity) o;
        return Objects.equals(id, smpEntity.id) &&
                Objects.equals(smpId, smpEntity.smpId) &&
                Objects.equals(certificate, smpEntity.certificate) &&
                Objects.equals(endpointPhysicalAddress, smpEntity.endpointPhysicalAddress) &&
                Objects.equals(endpointLogicalAddress, smpEntity.endpointLogicalAddress) &&
                Objects.equals(subdomain, smpEntity.subdomain);
    }

    @Override
    public int hashCode() {
        return Objects.hash(smpId);
    }

    @Override
    public String toString() {
        return "SmpEntity{" +
                "id=" + id +
                ", smpId='" + smpId + '\'' +
                ", certificate=" + (certificate != null ? certificate.getCertificateId() : "null") +
                ", endpointPhysicalAddress='" + endpointPhysicalAddress + '\'' +
                ", endpointLogicalAddress='" + endpointLogicalAddress + '\'' +
                ", subdomain=" + (subdomain != null ? subdomain.getSubdomainName() : "null") +
                ", disabled=" + disabled+
                '}';
    }
}
