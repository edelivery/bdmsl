/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.util;

import eu.europa.ec.bdmsl.common.enums.SMLPropertyEnum;
import eu.europa.ec.bdmsl.common.enums.SMLPropertyTypeEnum;
import eu.europa.ec.bdmsl.common.exception.BadConfigurationException;
import eu.europa.ec.bdmsl.common.exception.InvalidArgumentException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.support.CronExpression;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import static org.apache.commons.lang3.StringUtils.trim;

public class PropertyUtils {
    static final Logger LOG = LoggerFactory.getLogger(PropertyUtils.class);

    private static final String REG_EXP_VALUE_SEPARATOR = "\\|";

    public static final String ERROR = "]. Error:";

    public static Object parsePropertyType(SMLPropertyTypeEnum type, String value, File rootFolder) throws BadConfigurationException {
        if (StringUtils.isBlank(value)) {
            return null;
        }

        if (StringUtils.length(value) > 2000) {
            throw new BadConfigurationException("Invalid property value! Error: Value to long. Max. allowed size 2000 characters!");
        }

        switch (type) {
            case BOOLEAN:
                if (StringUtils.equalsAnyIgnoreCase(trim(value), "true", "false")) {
                    return Boolean.valueOf(value.trim());
                }
                throw new BadConfigurationException("Invalid boolean value: ["
                        + value + "]. Error: Only {true, false} are allowed!");
            case REGEXP:
                try {
                    return Pattern.compile(value);
                } catch (PatternSyntaxException ex) {
                    throw new BadConfigurationException("Invalid regular expression: ["
                            + value + ERROR + ExceptionUtils.getRootCauseMessage(ex), ex);
                }
            case INTEGER:
                try {
                    return Integer.parseInt(value);
                } catch (NumberFormatException ex) {
                    throw new BadConfigurationException("Invalid integer: ["
                            + value + ERROR + ExceptionUtils.getRootCauseMessage(ex), ex);
                }
            case LIST_STRING: {
                return Arrays.asList(value.split(REG_EXP_VALUE_SEPARATOR));
            }
            case PATH: {
                File file = new File(rootFolder, value);
                if (!file.exists() && !file.mkdirs()) {
                    throw new BadConfigurationException("Folder: ["
                            + value + "] does not exist, and can not be created!");
                }
                if (!file.isDirectory()) {
                    throw new BadConfigurationException("Path: [" + value + "] is not folder!");
                }
                return new File(value);
            }
            // nothing to validate
            case FILENAME:
                File file = new File(rootFolder, value);
                if (!file.exists()) {
                    LOG.warn("File: [{}] does not exist. Full path: [{}].", value, file.getAbsolutePath());
                }
                return file;
            case EMAIL:
                String trimVal = value.trim();
                try {
                    DataValidator.validateEmail(trimVal);
                } catch (InvalidArgumentException e) {
                    throw new BadConfigurationException(e.getMessageWithoutErrorCode());
                }
                return trimVal;
            case URL:
                try {
                    return new URL(value.trim());
                } catch (MalformedURLException ex) {
                    throw new BadConfigurationException("Invalid URL address:  ["
                            + value + ERROR + ExceptionUtils.getRootCauseMessage(ex), ex);
                }
            case STRING:
                return value;
            case CRON_EXPRESSION:
                try {
                    return CronExpression.parse(value);
                } catch (IllegalArgumentException ex) {
                    throw new BadConfigurationException("cron expression:  ["
                            + value + ERROR + ExceptionUtils.getRootCauseMessage(ex), ex);
                }
        }
        return null;
    }

    public static Object parseProperty(SMLPropertyEnum prop, String value, File rootFolder) throws BadConfigurationException {
        if (StringUtils.isBlank(value)) {
            // empty/ null value is invalid
            if (prop.isMandatory()) {
                throw new BadConfigurationException("Empty mandatory property: " + prop.getProperty());
            }
            return null;
        }
        if (!prop.getValuePattern().matcher(value).find()) {
            LOG.debug("Value [{}] for property [{}] does not match [{}]", value, prop.getProperty(), prop.getValuePattern().pattern());
            throw new BadConfigurationException(prop.getErrorValueMessage());
        }

        SMLPropertyTypeEnum type = prop.getPropertyType();
        return parsePropertyType(type, value, rootFolder);
    }
}
