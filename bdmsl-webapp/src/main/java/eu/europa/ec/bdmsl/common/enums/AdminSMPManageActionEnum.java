/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.enums;

import java.util.Optional;
import java.util.stream.Stream;

import static org.apache.commons.lang3.StringUtils.*;

/**
 * Enumerated action for DomiSML admin to manage custom SMP
 * <ul>
 *     <li>DELETE - delete custom SMP with all registered participants from Database. The SMP must be in DISABLED state.</li>
 *     <li>ENABLE - enable custom SMP and register all records to DNS</li>
 *     <li>DISABLE - disable custom SMP and unregister all records from DNS</li>
 *     <li>UPDATE - update smp logical and physical address</li>
 * </ul>
 *
 * @author Joze RIHTARSIC
 * @since 4.3
 */
public enum AdminSMPManageActionEnum {
    DELETE,
    ENABLE,
    DISABLE,
    UPDATE,
    SYNC_DNS,
    ;

    public static Optional<AdminSMPManageActionEnum> getByName(String val) {
        if (isBlank(val)) {
            return Optional.empty();
        }
        String rec = trim(upperCase(val));
        return Stream.of(values()).filter(enm -> enm.name().equals(rec)).findAny();
    }
}
