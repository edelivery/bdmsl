/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao;

import eu.europa.ec.bdmsl.common.bo.CertificateBO;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.dao.entity.CertificateEntity;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;

/**
 * @author Adrien FERIAL
 * @since 12/06/2015
 */
public interface ICertificateDAO {
    CertificateBO findCertificateByCertificateId(String certificateId) throws TechnicalException;

    /**
     * Method returns Certificate entity. If not certificate is found, then NotFoundException it thrown
     *
     * @param certificateId
     * @return CertificateEntity
     * @throws TechnicalException
     */
    Optional<CertificateEntity> findCertificateEntityByCertificateId(String certificateId) throws TechnicalException;

    CertificateBO findCertificateById(Long certificateId) throws TechnicalException;

    Long createCertificate(CertificateBO certificateBO) throws TechnicalException;

    void updateCertificate(CertificateBO certificateBO) throws TechnicalException;

    List<CertificateBO> findCertificatesToChange(Calendar calendar) throws TechnicalException;

    void delete(CertificateBO certificateBO) throws TechnicalException;

}
