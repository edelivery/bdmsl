/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.security;

import eu.europa.ec.bdmsl.common.bo.CertificateDomainBO;
import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import eu.europa.ec.edelivery.security.PreAuthenticatedCertificatePrincipal;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CertificateAuthentication implements Authentication {

    private PreAuthenticatedCertificatePrincipal principal;
    private CertificateDetails details;
    private List<GrantedAuthority> listAuthorities = new ArrayList<>();
    private boolean isAuthenticated;
    private boolean isRootPKICertificate;
    private static final int SERIAL_PADDING_SIZE = 32;

    private CertificateDomainBO grantedSubDomainAnchor;

    public CertificateAuthentication(PreAuthenticatedCertificatePrincipal principal, List<GrantedAuthority> listAuthorities) {
        this(null, principal, listAuthorities);
    }

    public CertificateAuthentication(CertificateDomainBO grantedSubDomainAnchor, PreAuthenticatedCertificatePrincipal principal, List<GrantedAuthority> listAuthorities) {
        this.principal = principal;
        this.grantedSubDomainAnchor = grantedSubDomainAnchor;
        this.details = new CertificateDetails();
        this.details.setCertificateId(principal.getName(SERIAL_PADDING_SIZE));
        this.details.setSerial(principal.getCertSerial());
        this.details.setIssuer(principal.getIssuerDN());
        this.details.setRootCertificateDN(principal.getIssuerDN());
        this.details.setSubject(principal.getSubjectShortDN());
        this.details.setValidFrom(DateUtils.toCalendar(principal.getNotBefore()));
        this.details.setValidTo(DateUtils.toCalendar(principal.getNotAfter()));
        this.listAuthorities.addAll(listAuthorities);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return listAuthorities;
    }

    @Override
    public Object getCredentials() {
        return this.principal != null ? this.principal.getCredentials() : null;
    }

    @Override
    public Object getDetails() {
        return this.details;
    }

    @Override
    public Object getPrincipal() {
        return this.principal;
    }

    @Override
    public boolean isAuthenticated() {
        return isAuthenticated;
    }

    @Override
    public void setAuthenticated(boolean b) {
        isAuthenticated = b;
    }

    @Override
    public String getName() {
        return principal.getName(SERIAL_PADDING_SIZE);
    }

    @Override
    public String toString() {
        return getName();
    }

    public boolean isRootPKICertificate() {
        return isRootPKICertificate;
    }

    public void setRootPKICertificate(boolean rootPKICertificate) {
        isRootPKICertificate = rootPKICertificate;
    }

    public SubdomainBO getGrantedSubDomain() {
        return grantedSubDomainAnchor != null ? grantedSubDomainAnchor.getSubdomain() : null;
    }

    public CertificateDomainBO getGrantedSubDomainAnchor() {
        return grantedSubDomainAnchor;
    }
}
