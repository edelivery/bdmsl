/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.impl;

import eu.europa.ec.bdmsl.common.bo.ConfigurationBO;
import eu.europa.ec.bdmsl.common.enums.SMLPropertyEnum;
import eu.europa.ec.bdmsl.common.exception.BadConfigurationException;
import eu.europa.ec.bdmsl.common.exception.ConfigurationException;
import eu.europa.ec.bdmsl.common.exception.KeyException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.common.util.KeyUtil;
import eu.europa.ec.bdmsl.config.DatabaseProperties;
import eu.europa.ec.bdmsl.config.properties.PropertyUpdateListener;
import eu.europa.ec.bdmsl.dao.AbstractDAOImpl;
import eu.europa.ec.bdmsl.dao.IConfigurationDAO;
import eu.europa.ec.bdmsl.dao.entity.ConfigurationEntity;
import eu.europa.ec.bdmsl.util.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.ContextStoppedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.convert.ConversionService;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import jakarta.persistence.TypedQuery;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.time.OffsetDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static eu.europa.ec.bdmsl.common.enums.SMLPropertyEnum.*;
import static eu.europa.ec.bdmsl.dao.QueryNames.*;

/**
 * @author Flavio SANTOS
 * @since 08/05/2017
 */
@Repository(value = "configurationDAOImpl")
public class ConfigurationDAOImpl extends AbstractDAOImpl implements IConfigurationDAO {

    private static final Logger LOG = LoggerFactory.getLogger(ConfigurationDAOImpl.class);

    @Autowired
    private ConversionService conversionService;

    boolean refreshProcessOngoing = false;
    Properties applicationProperties = new Properties();
    Map<String, Object> cachedPropertyValues = new HashMap();
    Calendar lastUpdate = null;
    OffsetDateTime initiateDate = null;
    ApplicationContext applicationContext;
    JavaMailSender mailSender;

    public ConfigurationDAOImpl(ApplicationContext applicationContext, JavaMailSender mailSender) {
        this.applicationContext = applicationContext;
        this.mailSender = mailSender;
    }

    private static String getProperty(Properties properties, SMLPropertyEnum key) {
        return StringUtils.trimToNull(properties.getProperty(key.getProperty(), key.getDefValue()));
    }

    private static void checkIfExists(Properties properties, SMLPropertyEnum val) throws BadConfigurationException {
        if (StringUtils.isEmpty(getProperty(properties, val))) {
            setBadConfigurationException(val.getProperty());
        }

    }

    private static void checkIsInteger(Properties properties, SMLPropertyEnum key) throws BadConfigurationException {
        String value = properties.getProperty(key.getProperty());
        if (StringUtils.isBlank(value)) {
            throw new BadConfigurationException(String.format("Missing integer property %s.", key.getProperty()));
        }

        try {
            Integer.parseInt(value);
        } catch (NumberFormatException ex) {
            throw new BadConfigurationException(String.format("Invalid integer property %s, value: '%s'.", key.getProperty(), value));
        }
    }

    private static File checkFileExist(String filename, Properties props) throws BadConfigurationException {
        String configurationDir = getProperty(props, CONFIGURATION_DIR);
        File file = new File(configurationDir, filename);
        if (!file.exists()) {
            throw new BadConfigurationException(String.format("The directory [%s] or file [%s] doesn't exist.", configurationDir, filename));
        }
        // test if file
        if (!file.isFile()) {
            throw new BadConfigurationException(filename + " must be a file!");
        }
        return file;

    }

    private static void setBadConfigurationException(String propertyName)
            throws BadConfigurationException {
        throw new BadConfigurationException(
                String.format("You must set the '%s' property",
                        propertyName));
    }

    private static void setBadConfigurationException(String configurationName, String propertyName)
            throws BadConfigurationException {
        throw new BadConfigurationException(
                String.format("If you enable %s configuration, then you must set the '%s' property", configurationName,
                        propertyName));
    }

    /**
     * Application event when an {@code ApplicationContext} gets initialized or start
     */
    @org.springframework.context.event.EventListener({ContextRefreshedEvent.class})
    public void contextRefreshedEvent() {
        LOG.debug("Application context is initialized: triggered  refresh  to update all property listeners");
        setInitializedTime(OffsetDateTime.now());
        reloadPropertiesFromDatabase();
    }

    /**
     * Application event when an application stops {@code ApplicationContext}
     */
    @EventListener({ContextStoppedEvent.class})
    public void contextStopEvent() {
        LOG.debug("Application context is stopped!");
        setInitializedTime(null);
    }

    protected void setInitializedTime(OffsetDateTime dateTime) {
        initiateDate = dateTime;
    }

    public OffsetDateTime getInitiateDate() {
        return initiateDate;
    }

    public boolean isApplicationInitialized() {
        return initiateDate != null;
    }

    @Override
    public ConfigurationBO findConfigurationProperty(String propertyName) throws TechnicalException {
        TypedQuery<ConfigurationEntity> query = getEntityManager().createNamedQuery(CONFIGURATION_ENTITY_GET_BY_PROPERTY_NAME, ConfigurationEntity.class);
        query.setParameter(PROPERTY_NAME, propertyName);
        ConfigurationEntity result = query.getSingleResult();
        return conversionService.convert(result, ConfigurationBO.class);
    }

    private Optional<ConfigurationEntity> getConfigurationEntityFromDatabase(SMLPropertyEnum key) throws TechnicalException {
        TypedQuery<ConfigurationEntity> query = getEntityManager().createNamedQuery(CONFIGURATION_ENTITY_GET_BY_PROPERTY_NAME, ConfigurationEntity.class);
        query.setParameter(PROPERTY_NAME, key.getProperty());
        List<ConfigurationEntity> result = query.getResultList();
        if (result.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(result.get(0));
    }

    @Override
    public ConfigurationBO setPropertyToDatabase(SMLPropertyEnum key, String value, String description) throws TechnicalException {
        Optional<ConfigurationEntity> result = getConfigurationEntityFromDatabase(key);
        ConfigurationEntity configurationEntity;
        if (!result.isPresent()) {
            configurationEntity = new ConfigurationEntity();
            configurationEntity.setProperty(key.getProperty());
            configurationEntity.setValue(value);
            configurationEntity.setDescription(StringUtils.isBlank(description) ? key.getDesc() : description);
            configurationEntity.setCreationDate(Calendar.getInstance());
            configurationEntity.setLastUpdateDate(Calendar.getInstance());
            getEntityManager().persist(configurationEntity);

        } else {
            configurationEntity = result.get();
            configurationEntity.setValue(value);
            // set default  for null value
            if (description != null) {
                configurationEntity.setDescription(description);
            }
            configurationEntity.setLastUpdateDate(Calendar.getInstance());
            configurationEntity = getEntityManager().merge(configurationEntity);
        }
        return conversionService.convert(configurationEntity, ConfigurationBO.class);

    }

    @Override
    public Optional<ConfigurationBO> getPropertyFromDatabase(SMLPropertyEnum key) throws TechnicalException {
        Optional<ConfigurationEntity> result = getConfigurationEntityFromDatabase(key);
        if (!result.isPresent()) {
            return Optional.empty();
        }
        return Optional.ofNullable(conversionService.convert(result.get(), ConfigurationBO.class));
    }

    @Override
    public Optional<ConfigurationBO> deletePropertyFromDatabase(SMLPropertyEnum key) throws TechnicalException {
        Optional<ConfigurationEntity> result = getConfigurationEntityFromDatabase(key);
        if (!result.isPresent()) {
            return Optional.empty();
        }
        getEntityManager().remove(result.get());
        return Optional.ofNullable(conversionService.convert(result.get(), ConfigurationBO.class));
    }

    @Override
    public List<ConfigurationBO> findConfigurationProperties(String propertyName) throws TechnicalException {
        TypedQuery<ConfigurationEntity> query = getEntityManager().createNamedQuery(CONFIGURATION_ENTITY_FIND_BY_PROPERTY_NAME, ConfigurationEntity.class);
        query.setParameter(PROPERTY_NAME, propertyName + "%");

        List<ConfigurationEntity> results = query.getResultList();
        return results.stream()
                .map(entity -> conversionService.convert(entity, ConfigurationBO.class))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public String getProperty(SMLPropertyEnum key) {
        if (lastUpdate == null) {
            refreshProperties();

        }
        return applicationProperties.getProperty(key.getProperty(), key.getDefValue());
    }

    @Override
    @Transactional
    public <T extends Object> T getPropertyValue(SMLPropertyEnum key) {
        if (lastUpdate == null) {
            refreshProperties();

        }
        return (T) cachedPropertyValues.get(key.getProperty());
    }

    @Override
    public Calendar getLastUpdate() {
        TypedQuery<Calendar> query = getEntityManager().createNamedQuery(CONFIGURATION_ENTITY_MAX_UPDATE_DATE, Calendar.class);
        return query.getSingleResult();
    }

    @Override
    @Transactional
    public void refreshProperties() {
        // get update
        Calendar lastUpdateFromDB = getLastUpdate();
        if (lastUpdate == null || lastUpdateFromDB == null || lastUpdateFromDB.after(lastUpdate)) {
            reloadPropertiesFromDatabase();
        } else {
            loggingService.info("Skip property update because max(LastUpdate): " + DateFormat.getDateTimeInstance().format(lastUpdateFromDB.getTime()) + " of properties in database is not changed! " +
                    "Currently loaded properties have max date:" + DateFormat.getDateTimeInstance().format(lastUpdate.getTime()) + ".");
        }
    }

    @Override
    @Transactional
    public void forceRefreshProperties() {
        // get update
        reloadPropertiesFromDatabase();
    }

    public void reloadPropertiesFromDatabase() {
        if (!refreshProcessOngoing) {
            refreshProcessOngoing = true;
            try {
                DatabaseProperties newProperties = new DatabaseProperties(getEntityManager());
                Map<String, Object> resultProperties;
                try {
                    resultProperties = validateConfiguration(newProperties);
                } catch (Throwable ex) {
                    loggingService.error("Throwable error occurred while refreshing configuration. Configuration was not changed!  Error: " + ex.getMessage(), null);
                    return;
                }

                synchronized (applicationProperties) {
                    applicationProperties.clear();
                    applicationProperties.putAll(newProperties);
                    cachedPropertyValues.putAll(resultProperties);
                    // setup smtp configuration
                    setupSMTPConfiguration(newProperties);
                    updatePropertyListeners();
                    // setup last update
                    lastUpdate = newProperties.getLastUpdate();
                }
            } finally {
                refreshProcessOngoing = false;
            }
        } else {
            loggingService.warn("Refreshing of database properties is already in process!");
        }
    }

    private void updatePropertyListeners() {
        // wait to get all property listener beans to avoid cyclic initialization
        // some beans are using ConfigurationService also are in PropertyUpdateListener
        // for listener to update properties
        if (!isApplicationInitialized()) {
            LOG.debug("Application is not started. The PropertyUpdateEvent is not triggered");
            return;
        }
        LOG.debug("Update all property listeners");
        Map<String, PropertyUpdateListener> updateListenerList = getPropertyUpdateListener();
        if (updateListenerList != null) {
            for (Map.Entry<String, PropertyUpdateListener> entry : updateListenerList.entrySet()) {
                String key = entry.getKey();
                PropertyUpdateListener value = entry.getValue();
                updateListener(key, value);
            }
        }
    }

    /**
     * To avoid circular dependencies (some update PropertyUpdateListener can use objects with ConfigurationService )
     */
    public Map<String, PropertyUpdateListener> getPropertyUpdateListener() {
        return applicationContext.getBeansOfType(PropertyUpdateListener.class);
    }

    protected void updateListener(String name, PropertyUpdateListener listener) {
        LOG.debug("updateListener [{}]", name);
        Map<SMLPropertyEnum, Object> mapProp = new HashMap<>();
        for (SMLPropertyEnum prop : listener.handledProperties()) {
            LOG.debug("Put property [{}]", prop.getProperty());
            if (this.cachedPropertyValues == null) {
                LOG.error("cachedPropertyValues is FOR SOME REASON NULL ");
            }
            if (this.cachedPropertyValues != null && this.cachedPropertyValues.containsKey(prop.getProperty())) {
                LOG.debug("Put property [{}] value [{}]", prop.getProperty(),
                        this.cachedPropertyValues.get(prop.getProperty()));
                mapProp.put(prop, this.cachedPropertyValues.get(prop.getProperty()));
            } else {
                LOG.debug("Property [{}] does not exist in cached map!", prop.getProperty());
            }
        }
        listener.updateProperties(mapProp);
    }

    protected void setupSMTPConfiguration(Properties newProperties) {
        if (!(mailSender instanceof JavaMailSenderImpl)) {
            loggingService.warn("Unknown mail sender bean: [" + mailSender + "]!. The mail sender was not configured!");
            return;
        }
        JavaMailSenderImpl javaMailSender = (JavaMailSenderImpl) mailSender;


        String host = getProperty(newProperties, MAIL_SERVER_HOST);
        Integer port = null;
        try {
            port = Integer.parseInt(getProperty(newProperties, MAIL_SERVER_PORT));
        } catch (NumberFormatException e) {
            loggingService.error("Error occurred while parsing mailserver port! Error: " + ExceptionUtils.getRootCauseMessage(e), e);
            return;
        }
        String protocol = getProperty(newProperties, MAIL_SERVER_PROTOCOL);
        String username = getProperty(newProperties, MAIL_SERVER_USERNAME);
        String password = null;
        try {
            if (!StringUtils.isBlank(username)) {
                password = retrieveMailPassword(newProperties);
            }
        } catch (KeyException e) {
            loggingService.error("Error occurred while retrieving mailserver password! Error: " + ExceptionUtils.getRootCauseMessage(e), e);
            return;
        }
        String properties = getProperty(newProperties, MAIL_SERVER_PROPERTIES);

        javaMailSender.setHost(host);
        javaMailSender.setPort(port);
        if (!StringUtils.isBlank(protocol)) {
            javaMailSender.setProtocol(protocol);
        }

        if (!StringUtils.isBlank(username)) {
            javaMailSender.setUsername(username);
            javaMailSender.setPassword(password);
        }
        javaMailSender.setJavaMailProperties(getMailPropertiesFromString(properties));
    }

    protected Properties getMailPropertiesFromString(String strProperties) {
        Properties properties = new Properties();
        if (strProperties != null) {
            Arrays.asList(strProperties.split(";")).forEach(prop -> {
                String[] val = prop.split(":");
                if (val.length == 1) {
                    properties.setProperty(val[0], "");
                } else if (val.length == 2) {
                    properties.setProperty(val[0], val[1]);
                } else {
                    loggingService.warn("Invalid mail property: " + prop + ". Property was ignored!");
                }
            });
        }
        return properties;
    }

    protected String retrieveMailPassword(Properties newProperties) throws KeyException {
        String password = getProperty(newProperties, MAIL_SERVER_PASSWORD);

        if (StringUtils.isBlank(password)) {
            return null;
        }
        String configFolder = getProperty(newProperties, CONFIGURATION_DIR);
        String encFileName = getProperty(newProperties, ENCRYPTION_FILENAME);
        try {
            Path location = Paths.get(configFolder, encFileName);
            return KeyUtil.decryptToString(location.toAbsolutePath().toString(), password);
        } catch (KeyException kexc) {
            throw kexc;
        } catch (Exception exc) {
            throw new KeyException(exc.getMessage(), exc);
        }
    }

    public Map<String, Object> validateConfiguration(Properties props) {
        HashMap<String, Object> propertyValues = new HashMap();
        String configurationDir = getProperty(props, CONFIGURATION_DIR);
        String encryptionKeyFilename = getProperty(props, ENCRYPTION_FILENAME);

        File configFolder = new File(configurationDir);
        File encryptionKeyFile = new File(configurationDir, encryptionKeyFilename);
        // put the first two values
        propertyValues.put(CONFIGURATION_DIR.getProperty(), configFolder);
        propertyValues.put(ENCRYPTION_FILENAME.getProperty(), encryptionKeyFile);


        try {


            if (Boolean.parseBoolean(getProperty(props, SIGN_RESPONSE))) {
                if (StringUtils.isEmpty(getProperty(props, SIGNATURE_ALIAS))) {
                    setBadConfigurationException(RESPONSE_SIGNING, SIGNATURE_ALIAS.getProperty());
                }
                if (StringUtils.isEmpty(getProperty(props, KEYSTORE_FILENAME))) {
                    setBadConfigurationException(RESPONSE_SIGNING, KEYSTORE_FILENAME.getProperty());
                }

                if (StringUtils.isEmpty(getProperty(props, KEYSTORE_PASSWORD))) {
                    setBadConfigurationException(RESPONSE_SIGNING, KEYSTORE_PASSWORD.getProperty());
                }

                if (StringUtils.isEmpty(getProperty(props, ENCRYPTION_FILENAME))) {
                    setBadConfigurationException("PrivateKey", ENCRYPTION_FILENAME.getProperty());
                }

                checkFileExist(getProperty(props, KEYSTORE_FILENAME), props);
                checkFileExist(getProperty(props, ENCRYPTION_FILENAME), props);
            }

            if (Boolean.parseBoolean(getProperty(props, DNS_ENABLED))) {
                if (StringUtils.isEmpty(getProperty(props, DNS_PUBLISHER_PREFIX))) {
                    setBadConfigurationException("the dns client", "dnsClient.publisherPrefix");
                }
                if (StringUtils.isEmpty(getProperty(props, DNS_SERVER))) {
                    setBadConfigurationException("the dns client", "dnsClient.server");
                }
            }

            if (Boolean.parseBoolean(getProperty(props, DNS_SIG0_ENABLED))) {
                if (StringUtils.isEmpty(getProperty(props, DNS_SIG0_KEY_FILENAME))) {
                    setBadConfigurationException("DNSSEC", "dnsClient.SIG0KeyFileName");
                }
                if (StringUtils.isEmpty(getProperty(props, DNS_SIG0_PKEY_NAME))) {
                    setBadConfigurationException("DNSSEC", "dnsClient.SIG0PublicKeyName");
                }
                checkFileExist(getProperty(props, DNS_SIG0_KEY_FILENAME), props);
            }

            if (Boolean.parseBoolean(getProperty(props, DNS_TSIG_ENABLED))) {
                if (StringUtils.isEmpty(getProperty(props, DNS_TSIG_KEY_NAME))) {
                    setBadConfigurationException(DNS_TSIG_ENABLED.getProperty(), DNS_TSIG_KEY_NAME.getProperty());
                }
                if (StringUtils.isEmpty(getProperty(props, DNS_TSIG_KEY_VALUE))) {
                    setBadConfigurationException(DNS_TSIG_ENABLED.getProperty(), DNS_TSIG_KEY_VALUE.getProperty());
                }

                String tsigProperty = getProperty(props, DNS_TSIG_HASH_ALGORITHM);
                if (StringUtils.isEmpty(tsigProperty)) {
                    setBadConfigurationException(DNS_TSIG_ENABLED.getProperty(), DNS_TSIG_HASH_ALGORITHM.getProperty());
                }
                if (!StringUtils.equalsAnyIgnoreCase(tsigProperty, "hmac-md5","hmac-sha1","hmac-sha224","hmac-sha256","hmac-sha384","hmac-sha512")){
                    setBadConfigurationException(DNS_TSIG_ENABLED.getProperty(), DNS_TSIG_HASH_ALGORITHM.getProperty());
                }



            }

            if (Boolean.parseBoolean(getProperty(props, PROXY_ENABLED))) {

                if (StringUtils.isEmpty(getProperty(props, HTTP_PROXY_USER))) {
                    setBadConfigurationException("proxy", "httpProxyUser");
                }

                if (StringUtils.isEmpty(getProperty(props, HTTP_PROXY_PASSWORD))) {
                    setBadConfigurationException("proxy", "httpProxyPassword");
                }

                if (StringUtils.isEmpty(getProperty(props, ENCRYPTION_FILENAME))) {
                    setBadConfigurationException("PrivateKey", "encryptionPrivateKey");
                }
                checkIsInteger(props, HTTP_PROXY_PORT);
                // always check this
                checkFileExist(getProperty(props, ENCRYPTION_FILENAME), props);
            }

            checkIsInteger(props, MAIL_SERVER_PORT);
            checkIsInteger(props, PAGE_SIZE);

            if (StringUtils.isEmpty(getProperty(props, MAIL_SERVER_HOST))) {
                setBadConfigurationException(MAIL_SERVER_HOST.getDesc(), MAIL_SERVER_HOST.getProperty());
            }

            checkIfExists(props, SML_PROPERTY_REFRESH_CRON);
            checkIfExists(props, CERT_CHANGE_CRON);
            checkIfExists(props, DIA_CRON);


        } catch (Exception exc) {
            throw new ConfigurationException(exc.getMessage(), exc);
        }
        // parse properties
        for (SMLPropertyEnum prop : SMLPropertyEnum.values()) {
            if (prop.equals(CONFIGURATION_DIR) || prop.equals(ENCRYPTION_FILENAME)) {
                // already checked and added to property values.
                continue;
            }
            String value = props.getProperty(prop.getProperty(), prop.getDefValue());
            Object parsedProperty = null;
            if (!prop.isEncrypted()) {
                try {
                    parsedProperty = PropertyUtils.parseProperty(prop, value, configFolder);
                } catch (BadConfigurationException exc) {
                    throw new ConfigurationException(exc.getMessage(), exc);
                }
            }
            propertyValues.put(prop.getProperty(), parsedProperty);
        }
        return propertyValues;
    }
}
