/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.security;

import com.google.common.base.Strings;
import eu.europa.ec.bdmsl.business.ICertificateDomainBusiness;
import eu.europa.ec.bdmsl.business.IConfigurationBusiness;
import eu.europa.ec.bdmsl.common.bo.CertificateDomainBO;
import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import eu.europa.ec.bdmsl.common.enums.SMLRoleEnum;
import eu.europa.ec.bdmsl.common.exception.*;
import eu.europa.ec.bdmsl.common.logging.ILoggingService;
import eu.europa.ec.bdmsl.common.util.LogEvents;
import eu.europa.ec.bdmsl.service.ITruststoreService;
import eu.europa.ec.bdmsl.util.CertificateUtils;
import eu.europa.ec.edelivery.security.PreAuthenticatedAnonymousPrincipal;
import eu.europa.ec.edelivery.security.PreAuthenticatedCertificatePrincipal;
import eu.europa.ec.edelivery.security.PreAuthenticatedTokenPrincipal;
import eu.europa.ec.edelivery.security.cert.CertificateValidator;
import eu.europa.ec.edelivery.security.cert.IRevocationValidator;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;

import javax.naming.InvalidNameException;
import javax.naming.ldap.LdapName;
import java.math.BigInteger;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.Principal;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.CertificateRevokedException;
import java.security.cert.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.Locale.US;


@Component
public class SMLAuthenticationProvider implements AuthenticationProvider {

    public static final String ERROR = "Error: ";

    public static final String REASON = ". Reason: ";

    private final DateFormat dateFormat = new SimpleDateFormat("MMM d hh:mm:ss yyyy zzz", US);

    private static final int SERIAL_PADDING_SIZE = 32;

    @Autowired
    private ILoggingService loggingService;

    @Autowired
    private ICertificateDomainBusiness certificateDomainBusiness;

    @Autowired
    IConfigurationBusiness configurationBusiness;

    @Autowired
    IRevocationValidator crlVerifierService;

    @Autowired
    ITruststoreService truststoreService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        return authenticatePrincipal((Principal) authentication.getPrincipal());
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return CertificateAuthentication.class.equals(clazz) || UnsecureAuthentication.class.equals(clazz) || SecurityTokenAuthentication.class.equals(clazz);
    }

    public Authentication authenticatePrincipal(Principal principal) throws AuthenticationException {
        loggingService.debug("Authentication type: " + (principal != null ? principal.getClass().toString() : null));
        Authentication authentication;
        try {

            if (principal instanceof PreAuthenticatedCertificatePrincipal) {
                authentication = authenticateCertificate((PreAuthenticatedCertificatePrincipal) principal);
            } else if (principal instanceof PreAuthenticatedTokenPrincipal) {
                authentication = authenticateSecurityToken((PreAuthenticatedTokenPrincipal) principal);

            } else if (principal instanceof PreAuthenticatedAnonymousPrincipal) {
                authentication = new UnsecureAuthentication();
                authentication.setAuthenticated(configurationBusiness.isUnsecureLoginEnabled());
            } else {
                // unknown principal type
                authentication = new UnsecureAuthentication();
                authentication.setAuthenticated(false);
            }
        } catch (TechnicalException exception) {
            throw new AuthenticationServiceException(exception.getMessageWithoutErrorCode(), exception);
        } catch (Exception exception) {
            throw new AuthenticationServiceException("Couldn't authenticate the principal " + principal, exception);
        }

        return authentication;
    }

    public Authentication authenticateSecurityToken(PreAuthenticatedTokenPrincipal principal) throws TechnicalException {
        loggingService.debug("Authenticating using the Admin Authenticator's Basic authentication");
        boolean res = false;
        if (!Strings.isNullOrEmpty(configurationBusiness.getMonitorToken())) {
            res = BCrypt.checkpw(principal.getName(), configurationBusiness.getMonitorToken());
        }
        List<GrantedAuthority> lstAuth = new ArrayList<>();
        lstAuth.add(SMLRoleEnum.ROLE_MONITOR.getAuthority());

        SecurityTokenAuthentication result = new SecurityTokenAuthentication(principal, lstAuth);
        result.setAuthenticated(res);
        return result;
    }

    /**
     * Method authenticate and authorize certificate to the domain. The use of  PreAuthenticatedCertificatePrincipal
     * already synchronize the "Reverse proxy"  TLS authentication with the Client-Cert header and the X509Certificate.
     *
     * @param principal - PreAuthenticatedCertificatePrincipal data
     * @return authentication
     * @throws TechnicalException
     */
    public Authentication authenticateCertificate(PreAuthenticatedCertificatePrincipal principal) throws TechnicalException {

        String subjectDn = principal.getSubjectCommonDN();
        CertificateUtils.validateSubjectDNRequiredValues(subjectDn);

        KeyStore truststore = truststoreService.getTruststore();
        if (truststore != null && principal.getCertificate() != null) {
            return authenticateCertificateWithTruststore(principal, truststore);
        }
        if (configurationBusiness.isLegacyDomainAuthorizationEnabled()) {
            return authenticateCertificateLegacy(principal);
        }

        String error = String.format("Certificate [Issuer: %s, Subject: %s] is not authorized!", principal.getIssuerDN(), principal.getSubjectShortDN());
        loggingService.securityLog(LogEvents.SEC_AUTHORIZED_ACCESS, error + "Can not validate certificate! Configure truststore and use X509Certificate authentication or enable legacy authentication!");
        throw new CertificateNotFoundException(error);
    }

    /**
     * @param principal
     * @return
     * @throws TechnicalException
     */
    public Authentication authenticateCertificateWithTruststore(PreAuthenticatedCertificatePrincipal principal, KeyStore truststore) throws TechnicalException {

        // check certificate validity
        X509Certificate clientCert = principal.getCertificate();
        try {
            clientCert.checkValidity();
        } catch (CertificateExpiredException | CertificateNotYetValidException e) {
            String error = String.format("CertificateException: Certificate with [Issuer: %s, Subject: %s] is not valid! Error: %s",
                    principal.getIssuerDN(), principal.getSubjectShortDN(), ExceptionUtils.getRootCauseMessage(e));
            loggingService.error(error, e);
            loggingService.securityLog(error);
            throw new CertificateAuthenticationException(error);
        }

        // for legacy purposes regular expression is verified later in method
        //createCertificateAuthenticationForDomain(...)
        // when legacy authorization will be deprecated set regular expression validation here
        CertificateValidator certificateValidator = new CertificateValidator(crlVerifierService, truststore, null);

        // check certificate revocation
        CertificateDomainBO authorizedDomain = getDomainAuthorizationCertificateForClientCertificate(clientCert, principal, certificateValidator);
        SubdomainBO subdomainBO = null;
        if (authorizedDomain == null) {
            // legacy message if user is not authenticated
            String error = String.format("Certificate [Issuer: %s, Subject: %s] does not match or was not found.", principal.getIssuerDN(), principal.getSubjectShortDN());
            loggingService.securityLog(LogEvents.SEC_AUTHORIZED_ACCESS, error);
            throw new CertificateNotFoundException(error);
        }

        subdomainBO = authorizedDomain.getSubdomain();
        if (StringUtils.isNotBlank(subdomainBO.getSmpCertPolicyOIDs())) {
            loggingService.debug("Set Certificate policy OIDs: [" + subdomainBO.getSmpCertPolicyOIDs() + "]");
            certificateValidator.setAllowedCertificatePolicyOIDs(subdomainBO.getSmpCertPolicyOIDsAsList());
        }

        // set regular expression
        String regExp = StringUtils.isBlank(subdomainBO.getSmpCertSubjectRegex()) ?
                configurationBusiness.getSMPCertRegularExpression() : subdomainBO.getSmpCertSubjectRegex();
        if (StringUtils.isNotBlank(regExp)) {
            loggingService.debug("Set Certificate subject regular expression: [" + regExp + "]");
            certificateValidator.setSubjectRegularExpressionPattern(Pattern.compile(regExp));
        }

        try {
            certificateValidator.validateCertificate(clientCert);
        } catch (CertificateException e) {
            // set legacy exception message!
            String error = String.format("Certificate Certificate Issuer: %s, Subject: %s does not match or was not found.", principal.getIssuerDN(), principal.getSubjectShortDN());
            loggingService.error(error + ERROR + ExceptionUtils.getRootCauseMessage(e), e);
            loggingService.securityLog(error);
            throw new CertificateAuthenticationException(error + REASON + ExceptionUtils.getRootCauseMessage(e));
        }
        return createCertificateAuthenticationForDomain(authorizedDomain, principal);
    }

    public CertificateDomainBO getDomainAuthorizationCertificateForClientCertificate(X509Certificate clientCert,
                                                                                     PreAuthenticatedCertificatePrincipal principal,
                                                                                     CertificateValidator certificateValidator)
            throws TechnicalException {

        // First try with certificate based domain authorization
        CertificateDomainBO authorizedDomain = getDomainForCertificateBasedDomainAuthorization(clientCert, certificateValidator);
        if (authorizedDomain == null) {
            // Second try with issuer based domain authorization
            authorizedDomain = getDomainForIssuerBasedDomainAuthorization(clientCert, certificateValidator);
        }

        // Third try: if legacy authentication is enabled
        if (authorizedDomain == null && configurationBusiness.isLegacyDomainAuthorizationEnabled()) {
            loggingService.info("Did not find domain from truststore. Try with legacy authorization");
            authorizedDomain = getAuthorizedDomain(principal).orElse(null);
        }
        return authorizedDomain;
    }

    public CertificateDomainBO getDomainForCertificateBasedDomainAuthorization(X509Certificate clientCert,
                                                                               CertificateValidator certificateValidator) throws CertificateAuthenticationException {

        String alias;
        try {
            alias = certificateValidator.getCertificateAlias(clientCert);
            if (StringUtils.isBlank(alias)) {
                return null;
            }
        } catch (CertificateException e) {
            String error = String.format("Error occurred while authorizing the Certificate with Issuer: [%s], Subject: [%s]!.",
                    clientCert.getIssuerX500Principal().getName(),
                    clientCert.getSubjectX500Principal().getName());

            loggingService.error(error + ERROR + ExceptionUtils.getRootCauseMessage(e), e);
            loggingService.securityLog(error);
            throw new CertificateAuthenticationException(error + REASON + ExceptionUtils.getRootCauseMessage(e));
        }

        List<CertificateDomainBO> authorizedDomain = certificateDomainBusiness.getDomainForCertificateAlias(alias, false);
        if (authorizedDomain.size() > 1) {
            String msg = "Certificate based domain authorization certificate [" + alias + "] is registered to more that one domain! ";
            loggingService.securityLog(msg);
            throw new CertificateAuthenticationException(msg);
        }
        return authorizedDomain.isEmpty() ? null : authorizedDomain.get(0);
    }

    public CertificateDomainBO getDomainForIssuerBasedDomainAuthorization(X509Certificate clientCert,
                                                                          CertificateValidator certificateValidator) throws CertificateAuthenticationException {

        String alias;
        try {
            alias = certificateValidator.getDirectParentAliasFromTruststore(clientCert);
            if (StringUtils.isBlank(alias)) {
                return null;
            }
        } catch (CertificateException | KeyStoreException e) {
            String error = String.format("Error occurred while authorizing the Certificate with Issuer: [%s], Subject: [%s]!.",
                    clientCert.getIssuerX500Principal().getName(),
                    clientCert.getSubjectX500Principal().getName());

            loggingService.error(error + ERROR + ExceptionUtils.getRootCauseMessage(e), e);
            loggingService.securityLog(error);
            throw new CertificateAuthenticationException(error + REASON + ExceptionUtils.getRootCauseMessage(e));
        }

        List<CertificateDomainBO> authorizedDomainsCandiates = certificateDomainBusiness.getDomainForCertificateAlias(alias, true);
        List<CertificateDomainBO> authorizedDomains = authorizedDomainsCandiates.stream().filter(domain -> isDomainAuthorizedCertificate(clientCert, domain)).collect(Collectors.toList());

        if (authorizedDomains.size() > 1) {
            String msg = "Issuer based domain authorization certificate [" + clientCert + "] is authorized on multiple domains! ";
            loggingService.securityLog(msg);
            throw new CertificateAuthenticationException(msg);
        }

        return authorizedDomains.isEmpty() ? null : authorizedDomains.get(0);
    }

    private boolean isDomainAuthorizedCertificate(X509Certificate clientCert, CertificateDomainBO certificateDomainBO) {

        // set validator policy OIDs
        SubdomainBO subdomainBO = certificateDomainBO.getSubdomain();

        // set regular expression
        String regExp = StringUtils.isBlank(subdomainBO.getSmpCertSubjectRegex()) ?
                configurationBusiness.getSMPCertRegularExpression() : subdomainBO.getSmpCertSubjectRegex();
        Pattern regExpCollection = StringUtils.isNotBlank(regExp) ? Pattern.compile(regExp) : null;
        if (regExpCollection == null) {
            return true;
        }

        String subject = clientCert.getSubjectX500Principal().getName();
        Matcher matcher = regExpCollection.matcher(subject);
        return matcher.find();
    }


    public Authentication authenticateCertificateLegacy(PreAuthenticatedCertificatePrincipal principal) throws TechnicalException {

        validateValidityPeriod(principal);
        // get authorized domain for the principal
        Optional<CertificateDomainBO> optCertificateDomainBO = getAuthorizedDomain(principal);
        if (!optCertificateDomainBO.isPresent()) {
            // certificate is not authorized to any domain!
            String error = String.format("Certificate Issuer: %s, Subject: %s", principal.getIssuerDN(), principal.getSubjectShortDN());
            loggingService.securityLog(LogEvents.SEC_UNKNOWN_CERTIFICATE, error);
            throw new CertificateNotFoundException(String.format("Certificate %s does not match or was not found.", error));
        }
        CertificateDomainBO certificateDomainBO = optCertificateDomainBO.get();

        // validate if certificate is not revoked!
        loggingService.debug("Got domain:" + certificateDomainBO.getCertificate());
        if (!Strings.isNullOrEmpty(certificateDomainBO.getCrl())) {
            //CRL URL is defined in database: check the list.
            loggingService.debug(String.format("Validate  CRL %s for serial %s : ", certificateDomainBO.getCrl(), principal.getCertSerial()));
            verifyCertificateCRLs(principal.getName(SERIAL_PADDING_SIZE), principal.getCertSerial(), Collections.singletonList(certificateDomainBO.getCrl()));
        } else {
            // no CRL defined - just warn to update the configuration
            loggingService.warn("Not CRL defined for CertificateDomain: " + certificateDomainBO.getCertificate());
        }
        return createCertificateAuthenticationForDomain(certificateDomainBO, principal);
    }

    public void verifyCertificateCRLs(String certificateId, String serial, List<String> crlDistPoints) throws TechnicalException {
        try {
            BigInteger biSerial = serialNumberToBigInteger(serial);
            crlVerifierService.isSerialNumberRevoked(biSerial, null, crlDistPoints);
            loggingService.debug("Certificate validated: [" + certificateId + "]");
        } catch (CertificateParsingException e) {
            loggingService.error("Failed to validate certificate [" + certificateId + "] for revocation. Parse error with message [" + ExceptionUtils.getRootCauseMessage(e) + "] ", e);
            throw new GenericTechnicalException("Error occurred while extracting CRL distribution point URLs", e);
        } catch (CertificateRevokedException e) {
            loggingService.securityLog("Certificate [" + certificateId + "] is revoked: [" + ExceptionUtils.getRootCauseMessage(e) + "] ");
            throw new eu.europa.ec.bdmsl.common.exception.CertificateRevokedException("The certificate with serial number: [" + serial + "] is revoked!", e);
        } catch (CertificateException e) {
            loggingService.error("Failed to validate certificate [" + certificateId + "] for revocation. Error occurred with message [" + ExceptionUtils.getRootCauseMessage(e) + "] ", e);
            throw new GenericTechnicalException("Error occurred while validating the certificate revocation list!", e);
        }
    }

    public Authentication createCertificateAuthenticationForDomain(CertificateDomainBO certificateDomainBO, PreAuthenticatedCertificatePrincipal principal)
            throws CertificateAuthenticationException, CertificateNotFoundException {

        List<GrantedAuthority> lstAuth = new ArrayList<>();
        if (certificateDomainBO.isRootCA()) {
            // for issuer based authorization subject must match regular expression. If
            // domain regular expression is null/empty/blank then use global regular expression
            String globalSmpCertSubjectRegex = configurationBusiness.getSMPCertRegularExpression();
            String domainSmpCertSubjectRegex = certificateDomainBO.getSubdomain().getSmpCertSubjectRegex();

            lstAuth = getIssuerBasedAuthorizationRoles(principal.getSubjectCommonDN(),
                    StringUtils.isBlank(domainSmpCertSubjectRegex) ? globalSmpCertSubjectRegex : domainSmpCertSubjectRegex);

        } else {
            // for Certificate based authorization
            // the certificates subject are matched to domain - and they are already trusted with SMP role.
            lstAuth.add(SMLRoleEnum.ROLE_SMP.getAuthority());
            // check also certificate has admin role (only certificate based authorization certs can have admin roles)
            if (certificateDomainBO.isAdmin()) {
                lstAuth.add(SMLRoleEnum.ROLE_ADMIN.getAuthority());
            }
        }

        // throw error if certificate is not authenticated
        if (lstAuth.isEmpty()) {
            String error = String.format("Certificate Issuer: %s, Subject: %s", principal.getIssuerDN(), principal.getSubjectShortDN());
            loggingService.securityLog(LogEvents.SEC_UNKNOWN_CERTIFICATE, error);
            throw new CertificateNotFoundException(String.format("Certificate %s does not match or was not found.", error));
        }

        CertificateAuthentication result = new CertificateAuthentication(certificateDomainBO, principal, lstAuth);
        result.setAuthenticated(true);
        result.setRootPKICertificate(certificateDomainBO.isRootCA());
        ((CertificateDetails) result.getDetails()).setRootCertificateDN(certificateDomainBO.getCertificate());
        // create authentication:
        return result;
    }

    public void validateValidityPeriod(PreAuthenticatedCertificatePrincipal principal) throws CertificateAuthenticationException {
        // get current time - we trust to server
        Date currentDate = Calendar.getInstance().getTime();
        // validate  dates
        if (principal.getNotBefore().after(currentDate)) {
            throw new CertificateAuthenticationException("Invalid certificate: NotBefore: " + dateFormat.format(principal.getNotBefore()));
        } else if (principal.getNotAfter().before(currentDate)) {
            throw new CertificateAuthenticationException("Invalid certificate:  NotAfter: " + dateFormat.format(principal.getNotAfter()));
        }
    }


    public Optional<CertificateDomainBO> getAuthorizedDomain(PreAuthenticatedCertificatePrincipal principal) throws TechnicalException {
        // first search for non-root ca than for root...
        loggingService.debug("Try to get certificate based authorized domain for:" + principal);
        Optional<CertificateDomainBO> certificateDomainBO = getAuthorizedDomainForNonRootCertificate(principal);

        if (!certificateDomainBO.isPresent()) {
            loggingService.debug("Try to get issuer based authorized domain for:" + principal);
            certificateDomainBO = getAuthorizedDomainForRootCertificate(principal);
        }
        loggingService.debug("Try to get domain by issuer:" + principal.getIssuerDN());

        return certificateDomainBO;
    }

    /**
     * Method returns domain for principal which by certificate domain authorization. That meant that
     * certificate entry must be in database!
     *
     * @param principal - certificate entity data
     * @return The authorized domain else Optional.empty
     * @throws TechnicalException
     */
    public Optional<CertificateDomainBO> getAuthorizedDomainForNonRootCertificate(PreAuthenticatedCertificatePrincipal principal) throws TechnicalException {
        return certificateDomainBusiness.findDomainForPrincipal(principal, false);
    }

    /**
     * @param principal
     * @return
     * @throws TechnicalException
     */
    public Optional<CertificateDomainBO> getAuthorizedDomainForRootCertificate(PreAuthenticatedCertificatePrincipal principal) throws TechnicalException {
        return certificateDomainBusiness.findDomainForPrincipal(principal, true);
    }

    public List<GrantedAuthority> getIssuerBasedAuthorizationRoles(String subject,
                                                                   String smpCertSubjectRegex)
            throws CertificateAuthenticationException {

        List<GrantedAuthority> roles = new ArrayList<>();
        if (StringUtils.isEmpty(subject)) {
            loggingService.error("Could not authenticate certificate for null subject!", null);
            return roles;
        }
        // legacy code - if regular expression is not set
        // then subject CN value must start with SMP_
        if (StringUtils.isBlank(smpCertSubjectRegex)) {
            LdapName ldapName;
            try {
                ldapName = new LdapName(subject);
            } catch (InvalidNameException exc) {
                throw new CertificateAuthenticationException("Impossible to parse certificate subject. " + subject, exc);
            }

            Optional<String> oCnValue = ldapName.getRdns().stream()
                    .filter(rdn -> "CN".equalsIgnoreCase(rdn.getType()))
                    .findFirst().map(rdn -> (String) rdn.getValue());


            if (!oCnValue.isPresent()) {
                throw new CertificateAuthenticationException("Invalid certificate subject: Missing CN. Subject value:" + subject);
            }
            if (oCnValue.get().startsWith("SMP_")) {
                loggingService.debug("Subject [" + subject + "] authenticated by legacy rule of SMP_");
                roles.add(SMLRoleEnum.ROLE_SMP.getAuthority());
            }
        } else if (subject.matches(smpCertSubjectRegex)) {
            loggingService.debug("Subject [" + subject + "] authenticated by regular expression: [" + smpCertSubjectRegex + "]");
            roles.add(SMLRoleEnum.ROLE_SMP.getAuthority());
        }
        return roles;
    }

    protected BigInteger serialNumberToBigInteger(String serialNumber) {
        assert serialNumber != null;
        String cleanSerial = serialNumber.trim().replaceAll("\\s", "");
        return new BigInteger(cleanSerial, 16);
    }
}
