/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.config;

public class FileProperty {
    // the property file is set in the root fo the resources
    public static final String PROPERTY_FILE = "sml.config.properties";
    // legacy configuration file
    public static final String PROPERTY_FILE_BACKUP = "config.properties";

    public static final String PROPERTY_LOG_FOLDER = "sml.log.folder";
    public static final String PROPERTY_LOG_PROPERTIES = "log.configuration.file";
    public static final String PROPERTY_DB_DRIVER = "sml.jdbc.driver";
    public static final String PROPERTY_DB_USER = "sml.jdbc.user";
    public static final String PROPERTY_DB_TOKEN = "sml.jdbc.password";
    public static final String PROPERTY_DB_URL = "sml.jdbc.url";
    public static final String PROPERTY_DB_JNDI = "sml.datasource.jndi";
    public static final String PROPERTY_DB_DIALECT = "sml.hibernate.dialect";

    public static final String PROPERTY_INIT_FOLDER = "sml.init.folder";
    public static final String DISPLAY_SERVICE_PAGE = "sml.display.service.page";
}


