/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.impl;

import eu.europa.ec.bdmsl.common.exception.BadConfigurationException;
import eu.europa.ec.bdmsl.common.exception.GenericTechnicalException;
import eu.europa.ec.bdmsl.common.exception.InvalidArgumentException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.service.IMailSenderService;
import eu.europa.ec.bdmsl.util.ConstraintsUtil;
import eu.europa.ec.bdmsl.util.DataValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

/**
 * @author Tiago MIGUEL
 * @author Flavio SANTOS
 * @since 01/02/2017
 */
@Service("mailService")
public class MailSenderServiceImpl implements IMailSenderService {

    @Autowired
    private MailSender mailSender;

    public void sendMessage(String subject, String content, String senderEmail, String recipientEmail) throws TechnicalException {
        emailValidator(senderEmail);
        emailValidator(recipientEmail);
        emailDataValidator(subject, "subject");
        emailDataValidator(content, "content");
        send(subject, content, senderEmail, recipientEmail);
    }

    private void send(String subject, String content, String senderEmail, String recipientEmail) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(senderEmail);
        message.setTo(recipientEmail);
        message.setSubject(subject);
        message.setText(content);
        mailSender.send(message);
    }

    private void emailDataValidator(String data, String complementaryMessage) throws GenericTechnicalException {
        ConstraintsUtil.emailDataValidator(data, complementaryMessage);
    }

    private void emailValidator(String emailAddress) throws BadConfigurationException {
        try {
            DataValidator.validateEmail(emailAddress);
        } catch (InvalidArgumentException e) {
            throw new BadConfigurationException(e.getMessageWithoutErrorCode());
        }
    }
}
