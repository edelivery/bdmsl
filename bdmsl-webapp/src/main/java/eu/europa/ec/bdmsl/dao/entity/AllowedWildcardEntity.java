/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.entity;


import org.hibernate.envers.Audited;

import jakarta.persistence.*;
import java.util.Objects;

import static eu.europa.ec.bdmsl.dao.QueryNames.ALLOWED_WILDCARD_ENTITY_GET_ALL_BY_CERTIFICATE_ID;

/**
 * @author Adrien FERIAL
 * @since 15/06/2015
 */
@Entity
@Audited
@Table(name = "bdmsl_allowed_wildcard")
@org.hibernate.annotations.Table(appliesTo = "bdmsl_allowed_wildcard", comment = "This table identifies the SMP with their certificates and map them to schemes for which they can create wildcard records.")
@NamedQueries({
        @NamedQuery(name = ALLOWED_WILDCARD_ENTITY_GET_ALL_BY_CERTIFICATE_ID, query = "select awe from AllowedWildcardEntity awe where awe.id.certificate.id=:certificateId"),
})
public class AllowedWildcardEntity extends AbstractEntity {

    @EmbeddedId
    private AllowedWildcardEntityPK id;

    public AllowedWildcardEntity() {
    }

    public AllowedWildcardEntity(CertificateEntity ce, String scheme) {
        setScheme(scheme);
        setCertificate(ce);
    }


    public AllowedWildcardEntityPK getId() {
        return id;
    }

    public String getScheme() {
        return id != null ? id.getScheme() : null;
    }

    public void setScheme(String scheme) {
        if (this.id == null) {
            this.id = new AllowedWildcardEntityPK();
        }
        this.id.setScheme(scheme);
    }

    public CertificateEntity getCertificate() {
        return id != null ? id.getCertificate() : null;
    }

    public void setCertificate(CertificateEntity certificate) {
        if (this.id == null) {
            this.id = new AllowedWildcardEntityPK();
        }
        this.id.setCertificate(certificate);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AllowedWildcardEntity)) return false;
        AllowedWildcardEntity that = (AllowedWildcardEntity) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
