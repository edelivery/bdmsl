/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.reports;

import eu.europa.ec.bdmsl.common.logging.ILoggingService;
import eu.europa.ec.bdmsl.service.dns.DataInconsistencyAnalyzer;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Flavio SANTOS
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class DataInconsistencyReport {

    public static final String DATABASE = "Database";

    public static final String DNS = "DNS";

    @Autowired
    protected ILoggingService loggingService;

    public final static String LOG_PREFIX = "Inconsistency Report >> ";

    private List<String> scopes;
    private Map<String, List<String>> reportContent;
    private final String SERVER_SCOPE = "SERVER";
    private final String OVERVIEW_SCOPE = "OVERVIEW";
    private final String SMP_SCOPE = "SMP";
    private final String PARTICIPANT_SCOPE = "PARTICIPANT";
    private final String ERROR_SCOPE = "ERROR";

    public DataInconsistencyReport() {
        scopes = new ArrayList<>();
        reportContent = new HashMap<>();
    }

    public void createReport(DataInconsistencyAnalyzer dataInconsistencyRetriever) {
        setLog("Started creating report overview");
        reportOverview(dataInconsistencyRetriever);
        setLog("Ended creating report overview");

        setLog("Started creating report for smps");
        reportSmps(dataInconsistencyRetriever.getSmpsDataInconsistencies());
        setLog("Ended creating report for smps");

        setLog("Started creating report for participants - CNAME");
        reportParticipants(dataInconsistencyRetriever.getParticipantCNAMEDataInconsistencies());
        setLog("Ended creating report for participants - CNAME");

        setLog("Started creating report for participants - NAPTR");
        reportParticipants(dataInconsistencyRetriever.getParticipantNAPTRDataInconsistencies());
        setLog("Ended creating report for participants - NAPTR");

        setLog("Started creating report for participants - ERROR");
        reportErrors(dataInconsistencyRetriever.geDataInconsistencyErrors());
        setLog("Ended creating report for participants - ERROR");

    }

    public void reportOverview(DataInconsistencyAnalyzer dataInconsistencyRetriever) {
        addText(SERVER_SCOPE, "## DATA INCONSISTENCY TOOL ## \n");
        addTextForServerInfo();
        addText(OVERVIEW_SCOPE, "# Overview # \n");
        addText(OVERVIEW_SCOPE, messageForOverview(dataInconsistencyRetriever.getDnsTotalOfSMPs(), "SMP", DNS));
        addText(OVERVIEW_SCOPE, messageForOverview(dataInconsistencyRetriever.getDbTotalOfSMPs(), "SMP", DATABASE));
        addText(OVERVIEW_SCOPE, messageForOverview(dataInconsistencyRetriever.getDbTotalInactiveSMPs(), "Inactive SMP", DATABASE));
        addText(OVERVIEW_SCOPE, messageForOverview(dataInconsistencyRetriever.getDbTotalOfParticipants(), "Participants", DATABASE));
        addText(OVERVIEW_SCOPE, messageForOverview(dataInconsistencyRetriever.getDbTotalOfDisabledParticipantsCNAME(), "Disabled (CNAME) Participant", DATABASE));
        addText(OVERVIEW_SCOPE, messageForOverview(dataInconsistencyRetriever.getDbTotalOfDisabledParticipantsNAPTR(), "Disabled (NAPTR) Participant", DATABASE));
        addText(OVERVIEW_SCOPE, messageForOverview(dataInconsistencyRetriever.getDnsTotalOfParticipantsCNAME(), "CNAME for Participants", "DNS"));
        addText(OVERVIEW_SCOPE, messageForOverview(dataInconsistencyRetriever.getDnsTotalOfParticipantsNAPTR(), "NAPTR for Participants", "DNS"));
        addText(OVERVIEW_SCOPE, messageForOverview(getTotalOfInconsistencies(dataInconsistencyRetriever), null, null));

        if (dataInconsistencyRetriever.getDataInconsistencyEntries().size() > 0) {
            addText(OVERVIEW_SCOPE, "\n\n# Detail #");
        }
    }

    private int getTotalOfInconsistencies(DataInconsistencyAnalyzer dataInconsistencyRetriever) {
        if (!dataInconsistencyRetriever.getDataInconsistencyEntries().isEmpty()) {
            int issues = dataInconsistencyRetriever.getDataInconsistencyEntries().size();
            return issues;
        }
        return 0;
    }

    private void addTextForServerInfo() {
        String reportCreatedAt = "Report created at " + LocalDateTime.now() + "\n";
        addText(SERVER_SCOPE, reportCreatedAt);

        String serverLocalHost = null;
        try {
            serverLocalHost = "Server Local Host: " + InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            loggingService.error(e.getMessage(), e);
        }
        addText(SERVER_SCOPE, "# Server Info # \n");
        if (!StringUtils.isEmpty(serverLocalHost)) {
            addText(SERVER_SCOPE, serverLocalHost);
        }
        String serverName = System.getenv("SERVER_NAME");
        if (!StringUtils.isEmpty(serverName)) {
            addText(SERVER_SCOPE, "Server Name: " + serverName);
        }
    }

    public void reportSmps(List<DataInconsistencyAnalyzer.DataInconsistencyEntry> dataInconsistencyEntries) {
        for (DataInconsistencyAnalyzer.DataInconsistencyEntry dataInconsistencyEntry : dataInconsistencyEntries) {
            switch (dataInconsistencyEntry.getMissingSourceType()) {
                case DNS:
                    addText(SMP_SCOPE, String.format("The %s with ID %s hash %s is with target %s in the Database but it is not in the DNS.", dataInconsistencyEntry.getDataType().toString(), dataInconsistencyEntry.getId(), dataInconsistencyEntry.getDomainName(), dataInconsistencyEntry.getTarget()));
                    break;
                case DATABASE:
                    addText(SMP_SCOPE, String.format("The %s with hash %s is in the DNS with target %s  but it is not in the Database or is Inactive.", dataInconsistencyEntry.getDataType().toString(), dataInconsistencyEntry.getDomainName(), dataInconsistencyEntry.getTarget()));
                    break;
            }
        }
    }

    public void reportParticipants(List<DataInconsistencyAnalyzer.DataInconsistencyEntry> dataInconsistencyEntries) {
        for (DataInconsistencyAnalyzer.DataInconsistencyEntry dataInconsistencyEntry : dataInconsistencyEntries) {
            switch (dataInconsistencyEntry.getMissingSourceType()) {
                case DNS:
                    reportDnsSourceIssues(dataInconsistencyEntry);
                    break;
                case DATABASE:
                    addText(PARTICIPANT_SCOPE, String.format("The %s with hash %s and target %s is in the DNS but it is not in the Database or is Inactive.", dataInconsistencyEntry.getDataType().toString(), dataInconsistencyEntry.getDomainName(), dataInconsistencyEntry.getTarget()));
                    break;
            }
        }
    }

    public void reportErrors(List<DataInconsistencyAnalyzer.DataInconsistencyEntry> dataInconsistencyEntries) {
        for (DataInconsistencyAnalyzer.DataInconsistencyEntry dataInconsistencyEntry : dataInconsistencyEntries) {
            addText(ERROR_SCOPE, dataInconsistencyEntry.getErrorMessage());

        }
    }

    private void reportDnsSourceIssues(DataInconsistencyAnalyzer.DataInconsistencyEntry dataInconsistencyEntry) {
        addText(PARTICIPANT_SCOPE, String.format("The %s with ID %s hash %s and target %s is in the Database but it is not in the DNS.",
                dataInconsistencyEntry.getDataType().toString(), dataInconsistencyEntry.getId(), dataInconsistencyEntry.getDomainName(), dataInconsistencyEntry.getTarget()));

    }

    public String getSubjectVerb(int option, int total) {
        if (option == 1) {
            return total == 1 ? "is" : "are";
        }
        return total == 1 ? "" : "s";
    }

    public String messageForOverview(int total, String type, String source) {
        if (type == null && source == null) {
            if (total > 0) {
                return String.format("There %s [ %s ] difference%s between the database and the DNS. Please take appropriate actions to solve the problem.", getSubjectVerb(1, total), total, getSubjectVerb(2, total));
            } else {
                return "\nThere are no inconsistencies between the database and the DNS!";
            }
        }
        return String.format("There %s %s %s(s) in the %s", getSubjectVerb(1, total), total, type, source);
    }

    public void addScope(String newScope) {
        scopes.add(newScope);
        reportContent.put(newScope, new ArrayList<>());
    }

    public void addText(String scope, String text) {
        if (!scopes.contains(scope)) {
            addScope(scope);
        }
        reportContent.get(scope).add(text);
    }

    public String getText(String scope) {
        List<String> listText = reportContent.get(scope);
        StringBuilder result = new StringBuilder();
        for (String text : listText) {
            result.append(text).append("\n");
        }
        return result.toString();
    }

    public String getDetailedReport() {
        StringBuilder result = new StringBuilder();
        for (String scope : scopes) {
            result.append(getText(scope)).append("\n");
        }
        return result.toString().trim();
    }

    public List<String> getScopes() {
        return scopes;
    }

    private void setLog(String event) {
        loggingService.info(LOG_PREFIX + event);
    }
}
