/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.config;

import eu.europa.ec.bdmsl.dao.entity.ConfigurationEntity;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;

import static eu.europa.ec.bdmsl.dao.QueryNames.CONFIGURATION_ENTITY_GET_ALL;

public class DatabaseProperties extends Properties {
    private static final Log LOG = LogFactory.getLog(DatabaseProperties.class);
    private static final long serialVersionUID = 1L;


    private Calendar lastUpdate = null;

    public DatabaseProperties(EntityManager em) {
        TypedQuery<ConfigurationEntity> tq = em.createNamedQuery(CONFIGURATION_ENTITY_GET_ALL, ConfigurationEntity.class);

        List<ConfigurationEntity> lst = tq.getResultList();
        for (ConfigurationEntity dc : lst) {
            if (dc.getValue() != null) {
                LOG.info("Database property: '[" + dc.getProperty() + "]' value: '[" +
                        (dc.getProperty().toLowerCase().contains("password") ? "******" : dc.getValue()) + "]'");
                setProperty(dc.getProperty(), dc.getValue());
            }
            lastUpdate = lastUpdate == null || lastUpdate.before(dc.getLastUpdateDate()) ? dc.getLastUpdateDate() : lastUpdate;
        }
    }

    public Calendar getLastUpdate() {
        return lastUpdate;
    }

}
