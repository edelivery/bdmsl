/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.business.impl;

import eu.europa.ec.bdmsl.business.IIdentifierFormatterBusiness;
import eu.europa.ec.bdmsl.common.bo.ParticipantBO;
import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import eu.europa.ec.bdmsl.common.enums.SMLPropertyEnum;
import eu.europa.ec.bdmsl.common.identifier.ParticipantIdentifier;
import eu.europa.ec.bdmsl.common.identifier.ParticipantIdentifierFormatter;
import eu.europa.ec.bdmsl.config.properties.PropertyUpdateListener;
import eu.europa.ec.bdmsl.dao.entity.ParticipantIdentifierEntity;
import eu.europa.ec.dynamicdiscovery.enums.DNSLookupHashType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import static eu.europa.ec.bdmsl.common.enums.SMLPropertyEnum.*;

/**
 * @author Joze RIHTARSIC
 * @since 4.3
 */
@Component
public class IdentifierFormatterBusinessImpl implements IIdentifierFormatterBusiness, PropertyUpdateListener {
    private static final Logger LOG = LoggerFactory.getLogger(IdentifierFormatterBusinessImpl.class);


    ParticipantIdentifierFormatter participantIdentifierFormatter = new ParticipantIdentifierFormatter();


    @Override
    public void updateProperties(Map<SMLPropertyEnum, Object> properties) {
        LOG.info("Update IdentifierFormatter properties: [{}]", properties);

        if (properties.containsKey(PARTY_IDENTIFIER_SCHEME_MANDATORY)) {
            setSchemeMandatory((Boolean) properties.get(PARTY_IDENTIFIER_SCHEME_MANDATORY));
        }
        if (properties.containsKey(PARTY_IDENTIFIER_URN_PATTERN)) {
            setCustomURNSplitRegularExpression((Pattern) properties.get(PARTY_IDENTIFIER_URN_PATTERN));
        }
        if (properties.containsKey(PARTY_IDENTIFIER_CASESENSITIVE_SCHEMES)) {
            setCaseSensitiveSchemes((List<String>) properties.get(PARTY_IDENTIFIER_CASESENSITIVE_SCHEMES));
        }
    }

    public void setSchemeMandatory(boolean schemeMandatory) {
        participantIdentifierFormatter.setSchemeMandatory(schemeMandatory);
    }

    public void setCustomURNSplitRegularExpression(Pattern customURNSplitRegularExpression) {
        participantIdentifierFormatter.setCustomURNSplitRegularExpression(customURNSplitRegularExpression);
    }

    public void setCaseSensitiveSchemes(List<String> caseSensitiveSchemes) {
        participantIdentifierFormatter.setCaseSensitiveSchemas(caseSensitiveSchemes);
    }

    @Override
    public boolean isSchemeNotCaseSensitive(String scheme) {
        return participantIdentifierFormatter.isCaseInsensitiveSchema(scheme);
    }

    @Override
    public List<SMLPropertyEnum> handledProperties() {
        return Arrays.asList(PARTY_IDENTIFIER_URN_PATTERN,
                PARTY_IDENTIFIER_SCHEME_MANDATORY,
                PARTY_IDENTIFIER_CASESENSITIVE_SCHEMES);
    }

    /**
     * Normalize participant identifier for domain using the domain custom split regular expression and value validation patterns
     *
     * @param participantBO participant identifier to normalize
     * @param subdomainBO  subdomain of the participant
     * @return normalized participant identifier
     */
    @Override
    public ParticipantIdentifier normalizeForDomain(ParticipantBO participantBO, SubdomainBO subdomainBO) {

        String participantIdRegex = subdomainBO.getParticipantIdRegexp();
        participantIdentifierFormatter.setValueValidationPattern(participantIdRegex);
        return participantIdentifierFormatter.normalize(participantBO.getScheme(), participantBO.getParticipantId());
    }


    @Override
    public String dnsLookupFormat(String scheme, String identifier, DNSLookupHashType dnsLookupHashType) {
        // back compatible to lower case
        participantIdentifierFormatter.setValueValidationPattern(null);
        return participantIdentifierFormatter.dnsLookupFormat(scheme, identifier, dnsLookupHashType);
    }

    /**
     * Update hash values for participant identifier entity using DNSLookupHashType.MD5_HEX (no prefix) for CNAME
     * and DNSLookupHashType.SHA256_BASE32 for naptr value.
     *
     * @param participantIdentifierEntity participant identifier entity
     */
    @Override
    public void updateHashValuesForParticipant(ParticipantIdentifierEntity participantIdentifierEntity) {
        participantIdentifierFormatter.setValueValidationPattern(null);

        String cnameHash = participantIdentifierFormatter.dnsLookupHash(participantIdentifierEntity.getScheme(),
                participantIdentifierEntity.getParticipantId(), DNSLookupHashType.MD5_HEX);
        String naptrHash = participantIdentifierFormatter.dnsLookupHash(participantIdentifierEntity.getScheme(),
                participantIdentifierEntity.getParticipantId(), DNSLookupHashType.SHA256_BASE32);

        participantIdentifierEntity.setCnameHash(cnameHash);
        participantIdentifierEntity.setNaptrHash(naptrHash);
    }
}
