/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.business;

import eu.europa.ec.bdmsl.common.bo.CertificateDomainBO;

import eu.europa.ec.bdmsl.common.exception.TechnicalException;

import java.security.cert.X509Certificate;

/**
 * @author Adrien FERIAL
 * @since 14/07/2015
 */
public interface IX509CertificateBusiness {

    X509Certificate getCertificate(final X509Certificate[] requestCerts) throws TechnicalException;

    String getTrustedRootCertificateDN(X509Certificate[] certificates) throws TechnicalException;

    String validateClientX509Certificate(final X509Certificate[] certificates, boolean isCertificateAlreadyTrusted) throws TechnicalException;

    CertificateDomainBO validateRootCA(final X509Certificate cert) throws TechnicalException,  java.security.cert.CertificateNotYetValidException, java.security.cert.CertificateExpiredException;

    CertificateDomainBO validateNonRootCA(final X509Certificate cert) throws TechnicalException,  java.security.cert.CertificateNotYetValidException, java.security.cert.CertificateExpiredException;

    void validateCertificateCRLs(final X509Certificate cert) throws java.security.cert.CertificateNotYetValidException, java.security.cert.CertificateExpiredException, TechnicalException;
}
