/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.impl;

import eu.europa.ec.bdmsl.business.IConfigurationBusiness;
import eu.europa.ec.bdmsl.common.bo.ServiceMetadataPublisherBO;
import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import eu.europa.ec.bdmsl.common.exception.NotFoundException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.dao.AbstractDAOImpl;
import eu.europa.ec.bdmsl.dao.ICertificateDAO;
import eu.europa.ec.bdmsl.dao.ISmpDAO;
import eu.europa.ec.bdmsl.dao.entity.CertificateEntity;
import eu.europa.ec.bdmsl.dao.entity.CommonColumnsLengths;
import eu.europa.ec.bdmsl.dao.entity.SmpEntity;
import eu.europa.ec.bdmsl.dao.entity.SubdomainEntity;
import eu.europa.ec.bdmsl.security.CertificateDetails;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import jakarta.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import static eu.europa.ec.bdmsl.common.exception.ErrorMessagesType.CANNOT_UPDATE_SMP_BECAUSE_NOT_EXISTING;
import static eu.europa.ec.bdmsl.dao.QueryNames.*;

/**
 * @author Adrien FERIAL
 * @since 15/06/2015
 */
@Repository
@Qualifier("SmpDAOImpl")
public class SmpDAOImpl extends AbstractDAOImpl implements ISmpDAO {

    @Autowired
    IConfigurationBusiness configurationBusiness;

    @Autowired
    ICertificateDAO certificateDAO;

    @Autowired
    private ConversionService conversionService;

    @Override
    public ServiceMetadataPublisherBO findSMP(String smpId) throws TechnicalException {
        ServiceMetadataPublisherBO resultBO = null;
        TypedQuery<SmpEntity> query = getEntityManager().createNamedQuery(SMP_ENTITY_GET_BY_SMP_ID, SmpEntity.class);
        query.setParameter(SMP_ID, smpId);
        List<SmpEntity> resultSmpEntities = query.getResultList();
        if (!resultSmpEntities.isEmpty()) {
            resultBO = conversionService.convert(resultSmpEntities.get(0), ServiceMetadataPublisherBO.class);
        } else {
            loggingService.warn("No SMP found for smpId: [" + smpId + "]!");
        }
        if (resultSmpEntities.size() > 1) {
            loggingService.warn("Inconsistent database entries! Found [" + resultSmpEntities.size() + "] SMPs found for smpId: [" + smpId + "]!");
        }

        return resultBO;
    }

    @Override
    public ServiceMetadataPublisherBO createSMP(ServiceMetadataPublisherBO smpBO, CertificateDetails certificateDetails, SubdomainBO domain) throws TechnicalException {

        Optional<CertificateEntity> certificateEntity = certificateDAO.findCertificateEntityByCertificateId(certificateDetails.getCertificateId());
        if (!certificateEntity.isPresent()) {
            throw new NotFoundException("Certificate with id [" + certificateDetails.getCertificateId() + "] does not exist!");
        }
        SubdomainEntity subdomainEntity = conversionService.convert(domain, SubdomainEntity.class);

        SmpEntity smpEntity = conversionService.convert(smpBO, SmpEntity.class);
        smpEntity.setCertificate(certificateEntity.get());
        smpEntity.setSubdomain(subdomainEntity);
        super.persist(smpEntity);
        return conversionService.convert(smpEntity, ServiceMetadataPublisherBO.class);
    }

    @Override
    public ServiceMetadataPublisherBO deleteSMP(ServiceMetadataPublisherBO smpBO) throws TechnicalException {
        // do not user delete query else envers will not catch the change
        TypedQuery<SmpEntity> query = getEntityManager().createNamedQuery(SMP_ENTITY_GET_BY_SMP_ID, SmpEntity.class);
        query.setParameter(SMP_ID, smpBO.getSmpId());
        List<SmpEntity> resultSmpEntities = query.getResultList();
        if (resultSmpEntities.isEmpty()) {
            loggingService.warn(CANNOT_UPDATE_SMP_BECAUSE_NOT_EXISTING.getMessage(smpBO.getSmpId()));
            return null;
        } else {
            resultSmpEntities.forEach(smpEntity -> {
                loggingService.info("Remove smp: " + smpEntity);
                getEntityManager().remove(smpEntity);
            });
            return conversionService.convert(resultSmpEntities.get(0), ServiceMetadataPublisherBO.class);
        }
    }


    @Override
    public ServiceMetadataPublisherBO updateSMP(ServiceMetadataPublisherBO smpBO) throws TechnicalException {
        // do not user update query else envers will not catch the change
        TypedQuery<SmpEntity> query = getEntityManager().createNamedQuery(SMP_ENTITY_GET_BY_SMP_ID, SmpEntity.class);
        query.setParameter(SMP_ID, smpBO.getSmpId());
        List<SmpEntity> resultSmpEntities = query.getResultList();
        if (resultSmpEntities.isEmpty()) {
            loggingService.warn(CANNOT_UPDATE_SMP_BECAUSE_NOT_EXISTING.getMessage(smpBO.getSmpId()));
            return null;
        } else {
            for (SmpEntity smpEntity : resultSmpEntities) {
                smpEntity.setEndpointLogicalAddress(smpBO.getLogicalAddress());
                smpEntity.setEndpointPhysicalAddress(smpBO.getPhysicalAddress());
                smpEntity.setLastUpdateDate(Calendar.getInstance());
                getEntityManager().merge(smpEntity);
            }
            return conversionService.convert(resultSmpEntities.get(0), ServiceMetadataPublisherBO.class);
        }
    }

    @Override
    public ServiceMetadataPublisherBO updateSMPIdentifier(ServiceMetadataPublisherBO smpBO, String newSmpIdentifier){
        loggingService.info("Update SMP identifier from [" + smpBO.getSmpId() + "] to [" + newSmpIdentifier + "]");
        TypedQuery<SmpEntity> query = getEntityManager().createNamedQuery(SMP_ENTITY_GET_BY_SMP_ID, SmpEntity.class);
        query.setParameter(SMP_ID, smpBO.getSmpId());
        SmpEntity smpEntity = query.getSingleResult();
        smpEntity.setSmpId(newSmpIdentifier);
        getEntityManager().merge(smpEntity);
        return conversionService.convert(smpEntity, ServiceMetadataPublisherBO.class);
    }

    @Override
    @Transactional
    public void disableSMP(ServiceMetadataPublisherBO smpBO, boolean disabled) throws TechnicalException {
        // do not user update query else envers will not catch the change
        TypedQuery<SmpEntity> query = getEntityManager().createNamedQuery(SMP_ENTITY_GET_BY_SMP_ID, SmpEntity.class);
        query.setParameter(SMP_ID, smpBO.getSmpId());
        List<SmpEntity> resultSmpEntities = query.getResultList();
        if (resultSmpEntities.isEmpty()) {
            loggingService.warn(CANNOT_UPDATE_SMP_BECAUSE_NOT_EXISTING.getMessage(smpBO.getSmpId()));
        } else {
            for (SmpEntity smpEntity : resultSmpEntities) {
                smpEntity.setDisabled(disabled);
            }
        }
    }

    @Override
    public void changeCertificateForSMP(Long oldId, Long newCertificateId) throws TechnicalException {
        // do not user update query else envers will not catch the change
        TypedQuery<SmpEntity> query = getEntityManager().createNamedQuery(SMP_ENTITY_GET_BY_CERTIFICATE_ID, SmpEntity.class);
        query.setParameter("certificateId", oldId);
        List<SmpEntity> resultSmpEntities = query.getResultList();

        TypedQuery<CertificateEntity> certQuery = getEntityManager().createNamedQuery(CERTIFICATE_ENTITY_GET_BY_ID, CertificateEntity.class);
        certQuery.setParameter("id", newCertificateId);
        CertificateEntity certificateEntity = certQuery.getSingleResult();

        for (SmpEntity smpEntity : resultSmpEntities) {
            smpEntity.setCertificate(certificateEntity);
            smpEntity.setLastUpdateDate(Calendar.getInstance());
            getEntityManager().merge(smpEntity);
        }
    }

    @Override
    public List<ServiceMetadataPublisherBO> listSMPs() throws TechnicalException {
        TypedQuery<SmpEntity> query = getEntityManager().createNamedQuery(SMP_ENTITY_GET_ALL, SmpEntity.class);
        List<SmpEntity> result = query.getResultList();

        List<ServiceMetadataPublisherBO> boList = new ArrayList<>();
        String publisherPrefix = configurationBusiness.getDNSPublisherPrefix();
        for (SmpEntity smpEntity : result) {
            ServiceMetadataPublisherBO smpBO = conversionService.convert(smpEntity, ServiceMetadataPublisherBO.class);
            smpBO.createDNSRecord(publisherPrefix);
            boList.add(smpBO);
        }
        return boList;
    }

    @Override
    public ServiceMetadataPublisherBO cloneSMP(ServiceMetadataPublisherBO smpBO) throws TechnicalException {
        TypedQuery<SmpEntity> query = getEntityManager().createNamedQuery(SMP_ENTITY_GET_BY_SMP_ID, SmpEntity.class);
        query.setParameter(SMP_ID, smpBO.getSmpId());
        SmpEntity entity = query.getSingleResult();
        String smpId = StringUtils.right(entity.getSmpId()+"-upd-"+entity.getId(), CommonColumnsLengths.MAX_SML_SMP_ID_LENGTH);

        SmpEntity clone = new SmpEntity();
        clone.setSmpId(smpId);
        clone.setCertificate(entity.getCertificate());
        clone.setEndpointLogicalAddress(smpBO.getLogicalAddress());
        clone.setEndpointPhysicalAddress(smpBO.getPhysicalAddress());
        clone.setSubdomain(entity.getSubdomain());
        clone.setDisabled(entity.isDisabled());
        super.persist(clone);

        return conversionService.convert(clone, ServiceMetadataPublisherBO.class);
    }
}

