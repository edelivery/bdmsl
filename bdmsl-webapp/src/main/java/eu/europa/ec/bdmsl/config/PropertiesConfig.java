/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.config;


import eu.europa.ec.bdmsl.common.enums.SMLPropertyEnum;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;

import java.io.IOException;
import java.io.InputStream;
import java.security.Security;
import java.util.Optional;
import java.util.Properties;

import static eu.europa.ec.bdmsl.config.FileProperty.*;
import static eu.europa.ec.bdmsl.config.PropertyUtils.*;


/**
 * Class read properties from configuration file if exists. Then it use datasource (default by JNDI
 * if not defined in property file jdbc/smpDatasource) to read application properties. Because this class is
 * invoked before datasource is initialized by default - it create it's own database connection.
 * Also it uses hibernate to handle dates  for Configuration table.
 */
@Configuration
@PropertySource(value = "file:./application.properties", ignoreResourceNotFound = true)
public class PropertiesConfig {


    private static final Log LOG = LogFactory.getLog(PropertiesConfig.class);

    public static final String PROPERTY_JSP_SERVLET = "sml.jsp.servlet.class";

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer(Environment environment) {
        PropertySourcesPlaceholderConfigurer propertiesConfig = new PropertySourcesPlaceholderConfigurer();
        PropertyUtils propertyUtils = new PropertyUtils(environment);
        Properties fileProp = getFileProperties(null);
        propertyUtils.updateLog4jConfiguration(fileProp.getProperty(PROPERTY_LOG_FOLDER, null), fileProp.getProperty(PROPERTY_LOG_PROPERTIES, null));

        Optional<Properties> oProp = propertyUtils.getDatabaseProperties(fileProp);

        Properties prop;
        if (oProp.isPresent()) {
            prop = oProp.get();
        } else {
            // properties are not initialized from database
            LOG.error("Database properties are not initialized - set default properties!");
            prop = new Properties();
            for (SMLPropertyEnum property : SMLPropertyEnum.values()) {
                LOG.error("Set default property: '[" + property.getProperty() + "]' value: '[" +
                        (property.getProperty().toLowerCase().contains("password") ? "******" : property.getDefValue()) + "]'");
                prop.setProperty(property.getProperty(), property.getDefValue());
            }
        }

        // copy hibernate dialect from file properties
        // for security reasons do not copy others..
        prop.setProperty(PROPERTY_DB_JNDI, fileProp.getProperty(PROPERTY_DB_JNDI, "java:comp/env/jdbc/eDeliverySML"));
        prop.setProperty(PROPERTY_DB_DIALECT, fileProp.getProperty(PROPERTY_DB_DIALECT, "org.hibernate.dialect.MySQLDialect"));
        prop.setProperty(PROPERTY_VALIDATE_XSD_SCHEME_SERVICEMETADATA, fileProp.getProperty(PROPERTY_VALIDATE_XSD_SCHEME_SERVICEMETADATA, "true"));
        prop.setProperty(PROPERTY_VALIDATE_XSD_SCHEME_PARTICIPANTS, fileProp.getProperty(PROPERTY_VALIDATE_XSD_SCHEME_PARTICIPANTS, "true"));
        prop.setProperty(PROPERTY_VALIDATE_XSD_SCHEME_BDMSLSERVICE, fileProp.getProperty(PROPERTY_VALIDATE_XSD_SCHEME_BDMSLSERVICE, "true"));
        prop.setProperty(PROPERTY_VALIDATE_XSD_SCHEME_BDMSLADMINSERVICE, fileProp.getProperty(PROPERTY_VALIDATE_XSD_SCHEME_BDMSLADMINSERVICE, "true"));
        prop.setProperty(PROPERTY_VALIDATE_XSD_SCHEME_BDMSLMONITORINGSERVICE, fileProp.getProperty(PROPERTY_VALIDATE_XSD_SCHEME_BDMSLMONITORINGSERVICE, "true"));

        propertiesConfig.setProperties(prop);
        propertiesConfig.setLocalOverride(true);

        return propertiesConfig;
    }

    public static Properties getFileProperties(String propertyFilePath) {

        String filePath = StringUtils.isBlank(propertyFilePath) ? "/" + PROPERTY_FILE : propertyFilePath;

        LOG.info("Start read file properties from classpath:" + filePath);
        Properties connectionProp = new Properties();

        try (InputStream is = PropertiesConfig.class.getResourceAsStream(filePath)) {
            if (is != null) {
                connectionProp.load(is);
            } else {
                LOG.error("Property file [" + filePath + "] not in classpath! Check configuration how to set property file in classpath!", null);
            }
        } catch (IOException e) {
            LOG.error("Error occurred while reading property file [" + filePath + "]!", e);
        }

        return connectionProp;
    }
}
