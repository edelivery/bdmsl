/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.cron;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static eu.europa.ec.bdmsl.common.enums.SMLPropertyEnum.*;


/**
 * Class initialize the cron trigger beans
 *
 * @author Joze RIHTARSIC
 * @since 4.2
 */
@Configuration
public class CronTriggerConfig {

    public static final String TRIGGER_BEAN_PROPERTY_REFRESH = "BDMSLCronTriggerPropertyRefreshBean";
    public static final String TRIGGER_BEAN_CHANGE_CERTIFICATE = "BDMSLChangeCertificatesBean";
    public static final String TRIGGER_BEAN_INCONSISTENCY_REPORT = "BDMSLInconsistencyReportBean";
    public static final String TRIGGER_BEAN_SMP_WITH_EXPIRED_CERT_REPORT = "BDMSLSmpExpiredCertReportBean";


    @Bean(TRIGGER_BEAN_PROPERTY_REFRESH)
    public DynamicCronTrigger getPropertyRefreshCronTrigger() {
        return new DynamicCronTrigger(SML_PROPERTY_REFRESH_CRON.getDefValue(), SML_PROPERTY_REFRESH_CRON);
    }

    @Bean(TRIGGER_BEAN_CHANGE_CERTIFICATE)
    public DynamicCronTrigger getChangeCertificateCronTrigger() {
        return new DynamicCronTrigger(CERT_CHANGE_CRON.getDefValue(), CERT_CHANGE_CRON);
    }

    @Bean(TRIGGER_BEAN_INCONSISTENCY_REPORT)
    public DynamicCronTrigger getInconsistencyReportTrigger() {
        return new DynamicCronTrigger(DIA_CRON.getDefValue(), DIA_CRON);
    }

    @Bean(TRIGGER_BEAN_SMP_WITH_EXPIRED_CERT_REPORT)
    public DynamicCronTrigger getSMPExpiredCertificateReportCronTrigger() {
        return new DynamicCronTrigger(REPORT_EXPIRED_CERT_CRON.getDefValue(), REPORT_EXPIRED_CERT_CRON);
    }
}
