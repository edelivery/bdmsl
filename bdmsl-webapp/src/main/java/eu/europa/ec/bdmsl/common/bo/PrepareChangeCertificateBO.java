/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.bo;

import java.util.Calendar;

/**
 * @author Adrien FERIAL
 * @since 14/07/2015
 */
public class PrepareChangeCertificateBO extends AbstractBusinessObject {
    private CertificateBO currentCertificate;
    private Calendar migrationDate;
    private String publicKey;

    public CertificateBO getCurrentCertificate() {
        return currentCertificate;
    }

    public void setCurrentCertificate(CertificateBO currentCertificate) {
        this.currentCertificate = currentCertificate;
    }

    public Calendar getMigrationDate() {
        return migrationDate;
    }

    public void setMigrationDate(Calendar migrationDate) {
        this.migrationDate = migrationDate;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PrepareChangeCertificateBO)) return false;

        PrepareChangeCertificateBO that = (PrepareChangeCertificateBO) o;

        if (currentCertificate != null ? !currentCertificate.equals(that.currentCertificate) : that.currentCertificate != null)
            return false;
        if (migrationDate != null ? !migrationDate.equals(that.migrationDate) : that.migrationDate != null)
            return false;
        return !(publicKey != null ? !publicKey.equals(that.publicKey) : that.publicKey != null);

    }

    @Override
    public int hashCode() {
        int result = currentCertificate != null ? currentCertificate.hashCode() : 0;
        result = 31 * result + (migrationDate != null ? migrationDate.hashCode() : 0);
        result = 31 * result + (publicKey != null ? publicKey.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PrepareChangeCertificateBO{" +
                "currentCertificate=" + currentCertificate +
                ", migrationDate=" + migrationDate +
                ", publicKey='" + publicKey + '\'' +
                '}';
    }
}
