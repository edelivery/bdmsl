/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.security;

import eu.europa.ec.bdmsl.common.enums.SMLRoleEnum;
import eu.europa.ec.edelivery.security.PreAuthenticatedAnonymousPrincipal;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Calendar;
import java.util.Collection;

/**
 * @author Adrien FERIAL
 * @since 17/06/2015
 */
public class UnsecureAuthentication implements Authentication {

    private boolean authenticated;
    public static final String UNSECURE_HTTP_SERIAL = "unsecure-serial";
    public static final String UNSECURE_HTTP_CLIENT = "unsecure-http-client";
    public static final String UNSECURE_HTTP_ROOT_CA = "CN=unsecure_root,O=delete_in_production,C=only_for_testing";
    public static final String UNSECURE_HTTP_SUBJECT = "CN=unsecure_subject,O=delete_in_production,C=only_for_testing";

    private static final PreAuthenticatedAnonymousPrincipal principal = new PreAuthenticatedAnonymousPrincipal();

    private CertificateDetails certificate;

    public UnsecureAuthentication() {
        certificate = new CertificateDetails();
        Calendar validFrom = Calendar.getInstance();
        validFrom.set(1970, 1, 1);

        Calendar validTo = Calendar.getInstance();
        validTo.set(validTo.get(Calendar.YEAR) + 50, 1, 1);
        certificate.setValidFrom(validFrom);
        certificate.setValidTo(validTo);
        certificate.setIssuer(UNSECURE_HTTP_ROOT_CA);
        certificate.setSerial(UNSECURE_HTTP_SERIAL);
        certificate.setCertificateId(UNSECURE_HTTP_CLIENT);
        certificate.setSubject(UNSECURE_HTTP_SUBJECT);
        certificate.setRootCertificateDN(UNSECURE_HTTP_ROOT_CA);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return SMLRoleEnum.getAllAuthorities();
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getDetails() {
        return certificate;
    }

    @Override
    public Object getPrincipal() {
        return principal;
    }

    @Override
    public boolean isAuthenticated() {
        return authenticated;
    }

    @Override
    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }

    @Override
    public String getName() {
        return UNSECURE_HTTP_CLIENT;
    }

}
