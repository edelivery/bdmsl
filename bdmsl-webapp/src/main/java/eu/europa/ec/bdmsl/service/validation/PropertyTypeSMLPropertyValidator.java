/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.validation;

import eu.europa.ec.bdmsl.common.enums.SMLPropertyEnum;
import eu.europa.ec.bdmsl.common.enums.SMLPropertyTypeEnum;
import eu.europa.ec.bdmsl.common.exception.BadConfigurationException;
import eu.europa.ec.bdmsl.util.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.io.File;

/**
 * A validator that handles ALL {@code SMLPropertyEnum} values to validate value
 * against the property type, throwing {@code SMLPropertyValidationException} exceptions
 *
 * @since 5.0
 * @author Sebastian-Ion TINCU
 */
@Component
@Order(2)
public class PropertyTypeSMLPropertyValidator implements SMLPropertyValidator {
    private static final Logger LOG = LoggerFactory.getLogger(PropertyTypeSMLPropertyValidator.class);

    public static final String ERROR = "]. Error:";

    @Override
    public void validate(SMLPropertyEnum property, String value, File configDirectory) throws BadConfigurationException{
        if (StringUtils.isBlank(value) && property.isMandatory()) {
            throw new BadConfigurationException("empty/null value is invalid");
        }
        SMLPropertyTypeEnum type = property.getPropertyType();
        validatePropertyType(type, value, configDirectory);
    }

    @Override
    public boolean supports(SMLPropertyEnum property) {
        return true;
    }

    protected void validatePropertyType(SMLPropertyTypeEnum type, String value, File confFolder) throws BadConfigurationException {
        if (StringUtils.isBlank(value)) {
            return;
        }
        try {
            PropertyUtils.parsePropertyType(type, value, confFolder);
        } catch (BadConfigurationException ex) {
            LOG.debug("Invalid property value [{}] for type [{}]: {}", value, type, ExceptionUtils.getRootCauseMessage(ex));
            throw ex;
        }
    }
}
