/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.enums;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public enum SMLRoleEnum {
    ROLE_SMP("ROLE_SMP"),
    ROLE_MONITOR("ROLE_MONITOR"),
    ROLE_ADMIN("ROLE_ADMIN"),
    ;

    private static final List<GrantedAuthority> ALL_AUTHORITIES = Arrays.asList(SMLRoleEnum.values())
            .stream().map(role -> role.getAuthority())
            .collect(Collectors.toList());

    private static final List<GrantedAuthority> NO_AUTHORITIES = Collections.emptyList();

    String code;
    GrantedAuthority authority;

    SMLRoleEnum(String code) {
        this.code = code;
        this.authority = new SimpleGrantedAuthority(code);
    }

    public String getCode() {
        return code;
    }

    public GrantedAuthority getAuthority() {
        return authority;
    }

    public static List<GrantedAuthority> getAllAuthorities() {
        return ALL_AUTHORITIES;
    }

    public static List<GrantedAuthority> getNoAuthorities() {
        return NO_AUTHORITIES;
    }
}
