/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.business.impl;

import eu.europa.ec.bdmsl.business.ICertificateDomainBusiness;
import eu.europa.ec.bdmsl.business.IConfigurationBusiness;
import eu.europa.ec.bdmsl.business.IX509CertificateBusiness;
import eu.europa.ec.bdmsl.common.bo.CertificateDomainBO;
import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import eu.europa.ec.bdmsl.common.business.AbstractBusinessImpl;
import eu.europa.ec.bdmsl.common.exception.*;
import eu.europa.ec.bdmsl.common.util.Constant;
import eu.europa.ec.bdmsl.common.util.LogEvents;
import eu.europa.ec.bdmsl.util.CertificateUtils;
import eu.europa.ec.edelivery.security.cert.IRevocationValidator;
import eu.europa.ec.edelivery.text.DistinguishedNamesCodingUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.security.auth.x500.X500Principal;
import java.security.cert.CertificateException;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * X509 Certificate Business Implementation class
 *
 * @author Adrien FERIAL
 * @since 15/06/2015
 */
@Component
public class X509CertificateBusinessImpl extends AbstractBusinessImpl implements IX509CertificateBusiness {

    @Autowired
    private ICertificateDomainBusiness certificateDomainBusiness;

    @Autowired
    private IConfigurationBusiness configurationBusiness;

    @Autowired
    private IRevocationValidator verifierService;

    @Override
    public String validateClientX509Certificate(final X509Certificate[] certificates, boolean certificateAlreadyTrusted) throws TechnicalException {
        // We look into the database to search if the issuer belongs to the list of authorized root certificate aliases
        String trustedRootDN = getTrustedRootCertificateDN(certificates);

        if (StringUtils.isBlank(trustedRootDN)) {
            if (certificateAlreadyTrusted) {
                validateCertificateCRLs(certificates);
                return trustedRootDN;
            }

            StringJoiner certificateSubjects = new StringJoiner(", ");
            Arrays.stream(certificates).forEachOrdered(x509Certificate ->
                    certificateSubjects.add(CertificateUtils.normalizeForX509(x509Certificate.getSubjectX500Principal().toString())));
            loggingService.securityLog(LogEvents.SEC_UNKNOWN_CERTIFICATE, certificateSubjects.toString());
            throw new CertificateNotFoundException("Unknown or not trusted certificate " + certificateSubjects);
        } else {
            X509Certificate childCert = CertificateUtils.getNotCACertificate(certificates);
            validateChildCertificate(childCert);
        }

        return trustedRootDN;
    }

    private void validateCertificateCRLs(X509Certificate[] certificates) throws TechnicalException {
        for (X509Certificate certificate : certificates) {
            try {
                validateCertificateCRLs(certificate);
            } catch (java.security.cert.CertificateNotYetValidException
                     | java.security.cert.CertificateExpiredException exc) {
                handleException(exc, certificate);
            }
        }
    }

    private void validateChildCertificate(X509Certificate childCert) throws TechnicalException {
        CertificateDomainBO certificateDomainBO = validateNonRootCALoggingErrors(childCert);

        // validate if not a root cert check if is cert with root
        if (certificateDomainBO == null) {
            certificateDomainBO = validateRootCALoggingErrors(childCert);
        }

        // if still not found domain
        if (certificateDomainBO == null) {
            loggingService.securityLog(LogEvents.SEC_UNKNOWN_CERTIFICATE,
                    String.format("Certificate Issuer: %s, Subject: %s",
                            CertificateUtils.normalizeForX509(childCert.getIssuerX500Principal().toString()),
                            CertificateUtils.normalizeForX509(childCert.getSubjectX500Principal().toString())));
            throw new CertificateNotFoundException("Certificate does not match criteria or was not found.");
        }
    }

    @Override
    public CertificateDomainBO validateRootCA(final X509Certificate cert) throws TechnicalException,  java.security.cert.CertificateNotYetValidException, java.security.cert.CertificateExpiredException {
        // non root PKI has priority
        String subjectDN = CertificateUtils.normalizeForX509(cert.getSubjectDN().toString());
        final CertificateDomainBO certNonRootPKIDomainBO = certificateDomainBusiness.findDomain(subjectDN);
        if (certNonRootPKIDomainBO != null && !certNonRootPKIDomainBO.isRootCA()) {
            // Even thought certificate has registered root CA - the certificate itself is registered as non-root ca!
            return null;
        }
        // try with root CA
        String issuerDN = CertificateUtils.normalizeForX509(cert.getIssuerDN().toString());
        final CertificateDomainBO certDomainBO = certificateDomainBusiness.findDomain(issuerDN);
        if (certDomainBO != null && certDomainBO.isRootCA()) {
            validateCertificateCRLs(cert);
            return certDomainBO;
        }
        return null;
    }

    private CertificateDomainBO validateRootCALoggingErrors(final X509Certificate cert) throws TechnicalException {
        CertificateDomainBO certDomainBO = null;
        try {
            certDomainBO = validateRootCA(cert);
        } catch (final Exception exc) {
            handleException(exc, cert);
        }
        return certDomainBO;

    }

    @Override
    public CertificateDomainBO validateNonRootCA(final X509Certificate cert) throws TechnicalException,  java.security.cert.CertificateNotYetValidException, java.security.cert.CertificateExpiredException {
        //if not null, CertificateDomain.certificate is equal Certificate.subject, only CertificateDomain.isRootCA flag needs to be checked.
        final CertificateDomainBO certDomainBO = certificateDomainBusiness.findDomain(CertificateUtils.normalizeForX509(cert.getSubjectDN().toString()));
        if (certDomainBO != null && !certDomainBO.isRootCA()) {
            //Certificate is nonRootCA, CertificateDomain and Certificate have the same content as Subject
            validateCertificateCRLs(cert);
            return certDomainBO;
        }
        return null;
    }

    public CertificateDomainBO validateNonRootCALoggingErrors(final X509Certificate cert) throws TechnicalException {
        CertificateDomainBO certDomainBO = null;

        try {
            certDomainBO = validateNonRootCA(cert);
        } catch (final Exception exc) {
            handleException(exc, cert);
        }

        return certDomainBO;
    }

    @Override
    public void validateCertificateCRLs(final X509Certificate cert) throws java.security.cert.CertificateNotYetValidException, java.security.cert.CertificateExpiredException, TechnicalException {
        // crl from the certificate
        if (cert == null) {
            loggingService.warn("Can not validate null certificate! Validation skipped!");
            return;
        }
        String certName = cert.getSubjectX500Principal().getName();
        try {
            verifierService.isCertificateRevoked(cert, null);
        } catch (CertificateParsingException e) {
            loggingService.error("Failed to validate certificate [" + certName + "] for revocation. Parse error with message [" + ExceptionUtils.getRootCauseMessage(e) + "] ", e);
            throw new GenericTechnicalException("Error occurred while extracting CRL distribution point URLs", e);
        } catch (java.security.cert.CertificateRevokedException e) {
            loggingService.securityLog("Certificate [" + certName + "] is revoked: [" + ExceptionUtils.getRootCauseMessage(e) + "] ");
            throw new eu.europa.ec.bdmsl.common.exception.CertificateRevokedException("The certificate is revoked!", e);
        } catch (CertificateException e) {
            loggingService.error("Failed to validate certificate [" + certName + "] for revocation. Error occurred with message [" + ExceptionUtils.getRootCauseMessage(e) + "] ", e);
            throw new GenericTechnicalException("Error occurred while validating the certificate revocation list with error " + ExceptionUtils.getRootCauseMessage(e) + "!", e);
        }
        cert.checkValidity();
    }

    @Override
    public String getTrustedRootCertificateDN(X509Certificate[] certificates) throws TechnicalException {
        for (X509Certificate cert : certificates) {
            String subject = cert.getSubjectX500Principal().getName(X500Principal.RFC2253);
            String issuer = cert.getIssuerX500Principal().getName(X500Principal.RFC2253);
            CertificateDomainBO domainBO = getDomain(subject, issuer);
            if (domainBO != null) {
                return domainBO.normalizedFullDN();
            }
        }
        return null;
    }

    /**
     * It returns the domain for a specific certificate. If the domain has 2 certificates, Non Root CA and Root CA
     * then Non Root CA is preferred.
     *
     * @return the domain of the specified certificate
     */
    protected CertificateDomainBO getDomain(String subject, String issuer) throws TechnicalException {
        List<CertificateDomainBO> certificateDomainEntities = certificateDomainBusiness.findAll();
        try {
            return getNonRootCertificateDomain(certificateDomainEntities, subject)
                    .orElseGet(() -> getRootCertificateDomain(certificateDomainEntities, issuer, subject)
                            .orElse(null));
        } catch (IllegalArgumentException e) {
            throw new GenericTechnicalException(e.getMessage(), e);
        }
    }

    private Optional<CertificateDomainBO> getNonRootCertificateDomain(List<CertificateDomainBO> certificateDomainEntities, String subject) {
        String orderedFullSubject = DistinguishedNamesCodingUtil.normalizeDN(subject, DistinguishedNamesCodingUtil.getCommonAttributesDN());
        String orderedShortSubject = DistinguishedNamesCodingUtil.normalizeDN(subject, DistinguishedNamesCodingUtil.getMinimalAttributesDN());

        List<CertificateDomainBO> nonRootCaCertificates = certificateDomainEntities.stream()
                .filter(Predicate.not(CertificateDomainBO::isRootCA))
                .filter(certificateDomainBO ->
                        certificateDomainBO.normalizedFullDN().equals(orderedFullSubject) ||
                                certificateDomainBO.normalizedShortDN().equals(orderedShortSubject))
                .collect(Collectors.toList());

        if (nonRootCaCertificates.size() > 1) {
            throw new IllegalArgumentException(String.format("Certificate Domain has more than one Non Root CA Certificate or the certificate is invalid [%s].", nonRootCaCertificates));
        }
        if (nonRootCaCertificates.isEmpty()) {
            loggingService.debug("Could not find any Non Root CA Certificates");
            return Optional.empty();
        }
        return Optional.of(nonRootCaCertificates.iterator().next());
    }

    private Optional<CertificateDomainBO> getRootCertificateDomain(List<CertificateDomainBO> certificateDomainEntities, String issuer, String subject) {
        String orderedFullIssuer = DistinguishedNamesCodingUtil.normalizeDN(issuer, DistinguishedNamesCodingUtil.getCommonAttributesDN());
        String orderedShortIssuer = DistinguishedNamesCodingUtil.normalizeDN(issuer, DistinguishedNamesCodingUtil.getMinimalAttributesDN());
        String orderedFullSubject = DistinguishedNamesCodingUtil.normalizeDN(subject, DistinguishedNamesCodingUtil.getCommonAttributesDN());

        List<CertificateDomainBO> rootCaCertificates = certificateDomainEntities.stream()
                .filter(CertificateDomainBO::isRootCA)
                .filter(certificateDomainBO -> certificateDomainBO.normalizedFullDN().equals(orderedFullIssuer)
                        || certificateDomainBO.normalizedShortDN().equals(orderedShortIssuer))
                .filter(certificateDomainBO -> isDomainAuthorizedCertificate(orderedFullSubject, certificateDomainBO))
                .collect(Collectors.toList());
        if (rootCaCertificates.size() > 1) {
            throw new IllegalArgumentException(String.format("Certificate Domain has more than one Root CA Certificate or the certificate is invalid [%s].", rootCaCertificates));
        }
        if (rootCaCertificates.isEmpty()) {
            loggingService.debug("Could not find any Root CA Certificates");
            return Optional.empty();
        }
        return Optional.of(rootCaCertificates.iterator().next());
    }

    private boolean isDomainAuthorizedCertificate(String certificateSubject, CertificateDomainBO certificateDomainBO) {
        loggingService.info("Validating certificate subject [" + certificateSubject + "] against domain [" + certificateDomainBO + "]");

        // set validator policy OIDs
        SubdomainBO subdomainBO = certificateDomainBO.getSubdomain();
        // set regular expression
        String regExp = StringUtils.isBlank(subdomainBO.getSmpCertSubjectRegex()) ?
                configurationBusiness.getSMPCertRegularExpression() : subdomainBO.getSmpCertSubjectRegex();
        loggingService.info("Validating certificate subject [" + certificateSubject + "] against domain regexp [" + regExp + "]");
        Pattern regExpCollection = StringUtils.isNotBlank(regExp) ? Pattern.compile(regExp) : null;
        if (regExpCollection == null) {
            return true;
        }

        Matcher matcher = regExpCollection.matcher(certificateSubject);
        return matcher.find();
    }

    public void handleException(Exception exc, final X509Certificate cert) throws TechnicalException {
        Date today = Calendar.getInstance().getTime();
        DateFormat df = new SimpleDateFormat("MMM d hh:mm:ss yyyy zzz", Constant.LOCALE);

        if (exc instanceof CertificateAuthenticationException) {
            loggingService.error(exc.getMessage(), exc);
            loggingService.securityLog(LogEvents.SEC_UNAUTHORIZED_ACCESS, cert.getSubjectDN().toString(), cert.getIssuerDN().toString());
            throw (CertificateAuthenticationException) exc;
        } else if (exc instanceof java.security.cert.CertificateExpiredException) {
            loggingService.error(exc.getMessage(), exc);
            loggingService.securityLog(LogEvents.SEC_CERTIFICATE_EXPIRED, df.format(today), df.format(cert.getNotBefore().getTime()), df.format(cert.getNotAfter().getTime()));
            throw new eu.europa.ec.bdmsl.common.exception.CertificateExpiredException("Certificate expired.", exc);
        } else if (exc instanceof java.security.cert.CertificateNotYetValidException) {
            loggingService.error(exc.getMessage(), exc);
            loggingService.securityLog(LogEvents.SEC_CERTIFICATE_NOT_YET_VALID, df.format(today), df.format(cert.getNotBefore().getTime()), df.format(cert.getNotAfter().getTime()));
            throw new eu.europa.ec.bdmsl.common.exception.CertificateNotYetValidException("Certificate not valid yet.", exc);
        } else if (exc instanceof CertificateRevokedException) {
            loggingService.error(exc.getMessage(), exc);
            loggingService.securityLog(LogEvents.SEC_REVOKED_CERTIFICATE, cert.getSubjectX500Principal().toString());
            throw (CertificateRevokedException) exc;
        } else if (exc instanceof GenericTechnicalException) {
            loggingService.error(exc.getMessage(), exc);
            loggingService.securityLog(LogEvents.SEC_UNKNOWN_CERTIFICATE, String.format("Certificate Issuer: %s, Subject: %s", cert.getIssuerDN(), cert.getSubjectDN()));
            throw (GenericTechnicalException) exc;
        } else {
            loggingService.error(exc.getMessage(), exc);
            loggingService.securityLog(LogEvents.SEC_UNKNOWN_CERTIFICATE, String.format("Internal error occured while validating certificate Issuer: %s, Subject: %s", cert.getIssuerDN(), cert.getSubjectDN()));
            throw new GenericTechnicalException("Internal error", exc);
        }
    }

    @Override
    public X509Certificate getCertificate(final X509Certificate[] requestCerts) throws TechnicalException {
        return CertificateUtils.getNotCACertificate(requestCerts);
    }
}
