/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.business;

import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

/**
 * @author Tiago MIGUEL
 * @since 13/03/2017
 */
public interface ISubdomainBusiness {

    List<SubdomainBO> findAll() throws TechnicalException;

    Optional<SubdomainBO> getSubDomainByName(String domainName) throws TechnicalException;

    List<String> findAllSchemesPerDomain() throws TechnicalException;

    SubdomainBO createSubDomain(SubdomainBO subdomainBO) throws TechnicalException;

    SubdomainBO updateSubDomain(String domainName, String regExp, String dnsRecordType, String urlSchemas,
                                String certSubjectRegEx,
                                String certPolicyOIDs,
                                BigInteger maxParticipantCountForDomain,
                                BigInteger maxParticipantCountForSMP) throws TechnicalException;

    SubdomainBO deleteSubDomain(String domainName) throws TechnicalException;

    SubdomainBO getSubDomain(String domainName) throws TechnicalException;
}
