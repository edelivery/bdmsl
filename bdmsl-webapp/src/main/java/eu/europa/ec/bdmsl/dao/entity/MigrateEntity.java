/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.entity;

import eu.europa.ec.bdmsl.dao.utils.ColumnDescription;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.envers.Audited;

import jakarta.persistence.*;
import java.util.Objects;

import static eu.europa.ec.bdmsl.dao.QueryNames.*;

/**
 * @author Adrien FERIAL
 * @since 15/06/2015
 */
@Entity
@Audited
@Table(name = "bdmsl_migrate")
@NamedQueries({
        @NamedQuery(name = MIGRATE_ENTITY_GET_BY_PARTICIPANT_AND_MIGRATION_STATUS,
                query = "SELECT m FROM MigrateEntity m where m.id.scheme = :scheme and upper(m.id.participantId) = upper(:participantId) and m.migrated = :migrated"),
        @NamedQuery(name = MIGRATE_ENTITY_GET_BY_OLD_SMP_AND_PARTICIPANT_AND_MIGRATION_STATUS,
                query = "SELECT m FROM MigrateEntity m where upper(m.oldSmpId) = upper(:smpId) and upper(m.id.participantId) in :participantIds and m.migrated = :migrated"),
        @NamedQuery(name = MIGRATE_ENTITY_GET_RECORD,
                query = "SELECT m FROM MigrateEntity m" +
                        " where m.id.scheme = :scheme and upper(m.id.participantId) = upper(:participantId) and m.id.migrationKey = :migrationKey" +
                        " and m.migrated = :migrated and upper(m.oldSmpId) =upper(:oldSmpId) and upper(m.newSmpId) = upper(:newSmpId)"),
        @NamedQuery(name = MIGRATE_ENTITY_GET_BY_PARTICIPANT_AND_MIGRATION_KEY,
                query = "SELECT m FROM MigrateEntity m where m.id.scheme = :scheme and upper(m.id.participantId) = upper(:participantId) " +
                        "and m.id.migrationKey = :migrationKey"),
        @NamedQuery(name = MIGRATE_ENTITY_GET_BY_OLD_SMP_IDENTIFIER_AND_MIGRATION_STATUS,
                query = "SELECT m FROM MigrateEntity m where upper(m.oldSmpId) = upper(:smpId) and m.migrated = :migrated"),
        @NamedQuery(name = MIGRATE_ENTITY_GET_BY_OLD_SMP_IDENTIFIER_AND_MIGRATION_KEY,
                query = "SELECT m FROM MigrateEntity m where m.id.scheme = :scheme and upper(m.id.participantId) = upper(:participantId) and upper(m.oldSmpId) = upper(:oldSmpId) and m.id.migrationKey = :migrationKey"),
})
public class MigrateEntity extends AbstractEntity {

    public static final String NULL_SCHEME = "NULL-SCHEME";

    @EmbeddedId
    MigrateEntityPK id;

    @Column(name = "old_smp_id", length = CommonColumnsLengths.MAX_SML_SMP_ID_LENGTH, nullable = false)
    @ColumnDescription(comment = "The id of the old SMP (before the migration)")
    private String oldSmpId;

    @Column(name = "new_smp_id", length = CommonColumnsLengths.MAX_SML_SMP_ID_LENGTH, nullable = true)
    @ColumnDescription(comment = "The id of the SMP after the migration")
    private String newSmpId;

    @Column(name = "migrated", nullable = false)
    @ColumnDescription(comment = "True if the migration is done")
    private boolean migrated;

    @Override
    public Object getId() {
        return id;
    }


    public String getScheme() {
        return id == null || StringUtils.equalsIgnoreCase(NULL_SCHEME, id.getScheme()) ?
                null : id.getScheme();
    }

    public void setScheme(String scheme) {
        if (this.id == null) {
            this.id = new MigrateEntityPK();
        }
        this.id.setScheme(StringUtils.isBlank(scheme) ? NULL_SCHEME : scheme);
    }

    public String getParticipantId() {
        return id != null ? id.getParticipantId() : null;
    }

    public void setParticipantId(String participantId) {
        if (this.id == null) {
            this.id = new MigrateEntityPK();
        }
        this.id.setParticipantId(participantId);
    }

    public String getMigrationKey() {
        return id != null ? id.getMigrationKey() : null;
    }

    public void setMigrationKey(String migrationKey) {
        if (this.id == null) {
            this.id = new MigrateEntityPK();
        }
        this.id.setMigrationKey(migrationKey);
    }

    public String getOldSmpId() {
        return oldSmpId;
    }

    public void setOldSmpId(String oldSmpId) {
        this.oldSmpId = oldSmpId;
    }

    public boolean isMigrated() {
        return migrated;
    }

    public void setMigrated(boolean migrated) {
        this.migrated = migrated;
    }

    public String getNewSmpId() {
        return newSmpId;
    }

    public void setNewSmpId(String newSmpId) {
        this.newSmpId = newSmpId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MigrateEntity)) return false;
        MigrateEntity that = (MigrateEntity) o;
        return migrated == that.migrated &&
                Objects.equals(id, that.id) &&
                Objects.equals(oldSmpId, that.oldSmpId) &&
                Objects.equals(newSmpId, that.newSmpId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
