/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.impl;

import eu.europa.ec.bdmsl.business.IConfigurationBusiness;
import eu.europa.ec.bdmsl.business.IManageCertificateBusiness;
import eu.europa.ec.bdmsl.business.IManageParticipantIdentifierBusiness;
import eu.europa.ec.bdmsl.business.IManageServiceMetadataBusiness;
import eu.europa.ec.bdmsl.common.bo.*;
import eu.europa.ec.bdmsl.common.exception.*;
import eu.europa.ec.bdmsl.common.service.AbstractServiceImpl;
import eu.europa.ec.bdmsl.service.IManageParticipantIdentifierService;
import eu.europa.ec.bdmsl.service.dns.IDnsClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static eu.europa.ec.bdmsl.common.exception.ErrorMessagesType.NOT_AUTHORIZED_FOR_WILDCARD_SCHEME;
import static eu.europa.ec.bdmsl.common.exception.ErrorMessagesType.SMP_WAS_NOT_CREATED_WITH_THE_CERTIFICATE;

/**
 * @author Adrien FERIAL
 * @since 12/06/2015
 */
@Service
@Transactional(readOnly = true)
public class ManageParticipantIdentifierServiceImpl extends AbstractServiceImpl implements IManageParticipantIdentifierService {
    private static final Logger LOG = LoggerFactory.getLogger(ManageParticipantIdentifierServiceImpl.class);
    public static final String THE_SMP = "The SMP ";

    public static final String THE_PARTICIPANT_IDENTIFIER = "The participant identifier '";

    @Autowired
    private IManageParticipantIdentifierBusiness manageParticipantIdentifierBusiness;

    @Autowired
    private IManageServiceMetadataBusiness manageServiceMetadataBusiness;

    @Autowired
    private IManageCertificateBusiness manageCertificateBusiness;

    @Autowired
    private IDnsClientService dnsClientService;

    @Autowired
    private IConfigurationBusiness configurationBusiness;

    @Override
    @PreAuthorize("hasAnyRole('ROLE_SMP')")
    @Transactional
    public ParticipantListBO list(PageRequestBO pageRequestBO) throws TechnicalException {
        // Validate the input data
        manageParticipantIdentifierBusiness.validatePageRequest(pageRequestBO);

        // Check that the smp exists.
        ServiceMetadataPublisherBO smpBO = manageServiceMetadataBusiness.verifySMPExist(pageRequestBO.getSmpId());
        if (smpBO == null) {
            throw new SmpNotFoundException(THE_SMP + pageRequestBO.getSmpId() + " couldn't be found");
        }

        // check that the user owns the SMP
        String currentCertificate = SecurityContextHolder.getContext().getAuthentication().getName();
        if (manageCertificateBusiness.equalsCertificate(smpBO.getCertificateId(), currentCertificate)) {
            return manageParticipantIdentifierBusiness.list(pageRequestBO);
        } else {
            throw new UnauthorizedException(SMP_WAS_NOT_CREATED_WITH_THE_CERTIFICATE.getMessage(pageRequestBO.getSmpId(), currentCertificate));
        }
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_SMP')")
    @Transactional(rollbackFor = Exception.class)
    public void create(ParticipantBO participantBO) throws TechnicalException {
        LOG.info("Create participant: [{}], with scheme: [{}]", participantBO.getParticipantId(),  participantBO.getScheme());
        createList(Collections.singletonList(participantBO));
    }

    /**
     * Validate ParticipantListBO data and the free quota for the SMP and domain is not exceeded.
     *
     * @param participantListBO ParticipantListBO object to validate and check the free quota for the SMP and domain
     * @throws TechnicalException if the ParticipantListBO data is not valid or the free quota for the SMP and domain is exceeded
     */
    @Override
    @PreAuthorize("hasAnyRole('ROLE_SMP')")
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void createList(ParticipantListBO participantListBO) throws TechnicalException {
        LOG.info("Create participant list: [{}]", participantListBO);
        manageParticipantIdentifierBusiness.validateParticipantBOList(participantListBO);
        List<ParticipantBO> participantBOs = participantListBO.getParticipantBOList();
        createList(participantBOs);
    }

    /**
     * Create a list of participants for the same SMP from the first object.
     * The method checks if the user is authorized to create participants for the SMP.
     * It also checks if the user is authorized to create a wildcard record.
     * @param participantBOs List of ParticipantBO objects to create
     * @throws TechnicalException if the user is not authorized to create participants for the SMP or if the user is not authorized to create a wildcard record
     */
    private void createList(List<ParticipantBO> participantBOs) throws TechnicalException {
        // All the participants are linked to the same SMP, check if user is
        // authorized to create participants for the SMP
        String publisherId = participantBOs.get(0).getSmpId();
        ServiceMetadataPublisherBO existingSmpBO = manageServiceMetadataBusiness.verifySMPExist(publisherId);

        manageServiceMetadataBusiness.validateAuthorization(existingSmpBO);
        // check if wildcard record is allowed for the user
        for (ParticipantBO participantBO : participantBOs) {
            authorizeCreateWildcardIdentifier(participantBO, existingSmpBO);
        }
        manageParticipantIdentifierBusiness.createParticipantList(publisherId, participantBOs);
    }

    /**
     * Method checks if data is in database - if not  - deletes records from  DNS
     *
     * @param participantListBO
     * @throws TechnicalException
     */
    public void deleteDNSForMissingParticipants(ParticipantListBO participantListBO) {
        boolean isDNSEnabled = configurationBusiness.isDNSEnabled();

        if (isDNSEnabled) {
            for (ParticipantBO participantBO : participantListBO.getParticipantBOList()) {

                try {
                    // Then make sure that the smp exists.
                    ServiceMetadataPublisherBO smpBO = manageServiceMetadataBusiness.verifySMPExist(participantBO.getSmpId());

                    Optional<ParticipantBO> existingParticipantBO = manageParticipantIdentifierBusiness.getParticipantForDomain(participantBO,
                            smpBO.getSubdomain());
                    if (!existingParticipantBO.isPresent()) {
                        dnsClientService.deleteDNSRecordsForParticipant(participantBO, smpBO);
                    }
                } catch (TechnicalException exc) {
                    loggingService.error("Error occurred while deleting DNS record for missing participant: "
                            + participantBO.getScheme() + " ::" + participantBO.getParticipantId(), exc);
                }
            }
        }

    }

    /**
     * Validate if the participant identifier is a wildcard and if the user is authorized to create a wildcard record.
     * @param participantBO ParticipantBO object to validate
     * @param existingSmpBO ServiceMetadataPublisherBO object to validate
     * @throws TechnicalException if the participant identifier is a wildcard and the user is not authorized to create a wildcard record
     */
    private void authorizeCreateWildcardIdentifier(ParticipantBO participantBO, ServiceMetadataPublisherBO existingSmpBO ) throws TechnicalException {
        // Is it a wildcard record allowed for the publisher and certificate
        String currentCertificate = existingSmpBO.getCertificateId();
        if ("*".equals(participantBO.getParticipantId())) {
            loggingService.debug("The participant Identifier is a wildcard");
            // check that the user is authorized to create a wildcard association
            WildcardBO wildcardBO = manageCertificateBusiness.findWildCard(participantBO.getScheme(), currentCertificate);

            if (wildcardBO == null) {
                throw new UnauthorizedException(
                        NOT_AUTHORIZED_FOR_WILDCARD_SCHEME.getMessage(currentCertificate, participantBO.getScheme()));
            } else {
                loggingService.debug("The certificate " + currentCertificate + " is allowed to create a wildcard record for the scheme " + participantBO.getScheme());
            }
        }

    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_SMP')")
    public ParticipantBO getParticipant(ParticipantBO participantBO) throws
            TechnicalException {

        manageParticipantIdentifierBusiness.normalizeAndValidateParticipant(participantBO);
        manageServiceMetadataBusiness.validateSMPId(participantBO.getSmpId());

        // Then make sure that the smp exists.
        ServiceMetadataPublisherBO existingSmpBO = manageServiceMetadataBusiness.verifySMPExist(participantBO.getSmpId());
        // validate smp subdomain and authenticated cert subdomain
        manageServiceMetadataBusiness.validateSMPSubdomain(existingSmpBO);
        return manageParticipantIdentifierBusiness.findParticipant(participantBO);
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_SMP')")
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void delete(ParticipantBO participantBO) throws TechnicalException {

        this.deleteParticipantsForSMP(participantBO.getSmpId(), Collections.singletonList(participantBO));
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_SMP')")
    @Transactional(rollbackFor = Exception.class)
    public void delete(String smpId, List<ParticipantBO> participantBOList) throws TechnicalException {
        this.deleteParticipantsForSMP(smpId, participantBOList);
    }


    @Override
    @PreAuthorize("hasAnyRole('ROLE_SMP')")
    @Transactional(rollbackFor = Exception.class)
    public void deleteList(ParticipantListBO participantListBO) throws TechnicalException {
        manageParticipantIdentifierBusiness.validateParticipantBOList(participantListBO);
        // assuming that all the participants are linked to the same SMP
        this.deleteParticipantsForSMP(participantListBO.getParticipantBOList().get(0).getSmpId(), participantListBO.getParticipantBOList());
    }

    private void deleteParticipantsForSMP(String publisherId, List<ParticipantBO> participantBOList) throws TechnicalException {
        manageServiceMetadataBusiness.validateSMPId(publisherId);
        ServiceMetadataPublisherBO existingSmpBO = manageServiceMetadataBusiness.verifySMPExist(publisherId);
        manageServiceMetadataBusiness.validateAuthorization(existingSmpBO);

        manageParticipantIdentifierBusiness.deleteParticipantList(publisherId, participantBOList);
    }


    @Override
    @PreAuthorize("hasAnyRole('ROLE_SMP')")
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void prepareToMigrate(MigrationRecordBO prepareToMigrateBO) throws TechnicalException {
        // Validate the input data
        manageParticipantIdentifierBusiness.validateMigrationRecord(prepareToMigrateBO);
        manageServiceMetadataBusiness.validateSMPId(prepareToMigrateBO.getOldSmpId());

        // verify the SMP exists
        ServiceMetadataPublisherBO existingSmpBO = manageServiceMetadataBusiness.verifySMPExist(prepareToMigrateBO.getOldSmpId());

        // Check if the participant exists
        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setScheme(prepareToMigrateBO.getScheme());
        participantBO.setSmpId(prepareToMigrateBO.getOldSmpId());
        participantBO.setParticipantId(prepareToMigrateBO.getParticipantId());
        // get participant for subdomain
        Optional<ParticipantBO> optExistingParticipantBO = manageParticipantIdentifierBusiness.getParticipantForDomain(participantBO, existingSmpBO.getSubdomain());
        if (!optExistingParticipantBO.isPresent()) {
            throw new ParticipantNotFoundException(THE_PARTICIPANT_IDENTIFIER + prepareToMigrateBO.getParticipantId()
                    + "' doesn't exist for the scheme " + prepareToMigrateBO.getScheme() + " for the smp " + prepareToMigrateBO.getOldSmpId());
        }
        ParticipantBO existingParticipantBO = optExistingParticipantBO.get();

        // check that the participant is linked to the SMP
        if (!existingParticipantBO.getSmpId().equalsIgnoreCase(existingSmpBO.getSmpId())) {
            throw new UnauthorizedException("The participant " + existingParticipantBO.getParticipantId() + " is not linked to the SMP " + existingSmpBO.getSmpId());
        }

        // check that the user owns the SMP
        String currentCertificate = SecurityContextHolder.getContext().getAuthentication().getName();
        if (manageCertificateBusiness.equalsCertificate(existingSmpBO.getCertificateId(), currentCertificate)) {
            manageParticipantIdentifierBusiness.prepareToMigrate(prepareToMigrateBO);
        } else {
            throw new UnauthorizedException(SMP_WAS_NOT_CREATED_WITH_THE_CERTIFICATE.getMessage(prepareToMigrateBO.getOldSmpId(), currentCertificate));
        }
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_SMP')")
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void migrate(MigrationRecordBO migrateBO) throws TechnicalException {
        // Validate the input data
        manageParticipantIdentifierBusiness.validateMigrationRecord(migrateBO);
        manageServiceMetadataBusiness.validateSMPId(migrateBO.getNewSmpId());

        // verify the SMP exists
        ServiceMetadataPublisherBO existingSmpBO = manageServiceMetadataBusiness.verifySMPExist(migrateBO.getNewSmpId());
        manageServiceMetadataBusiness.validateAuthorization(existingSmpBO);

        boolean isDNSEnabled = configurationBusiness.isDNSEnabled();

        //find the migration record created by the prepareToMigrate service
        MigrationRecordBO existingMigrationRecord = manageParticipantIdentifierBusiness.findNonMigratedRecord(migrateBO);
        if (existingMigrationRecord == null) {
            throw new MigrationNotFoundException("No migration record found. Please call the prepareToMigrate service prior to the Migrate service.");
        }

        // Check if the participant still exists
        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setParticipantId(migrateBO.getParticipantId());
        participantBO.setScheme(migrateBO.getScheme());
        participantBO.setSmpId(existingMigrationRecord.getOldSmpId());
        ParticipantBO existingParticipantBO = manageParticipantIdentifierBusiness.findParticipant(participantBO);
        if (existingParticipantBO == null) {
            throw new ParticipantNotFoundException(THE_PARTICIPANT_IDENTIFIER + migrateBO.getParticipantId() + "' doesn't exist for the scheme " + migrateBO.getScheme() + " for the smp " + migrateBO.getNewSmpId());
        }

        // check that the user owns the SMP
        String currentCertificate = SecurityContextHolder.getContext().getAuthentication().getName();
        if (manageCertificateBusiness.equalsCertificate(existingSmpBO.getCertificateId(), currentCertificate)) {
            if (!existingMigrationRecord.getMigrationCode().equals(migrateBO.getMigrationCode())) {
                throw new UnauthorizedException(String.format("The migration key [%s] for certificate [%s] doesn't match the expected one", migrateBO.getMigrationCode(), currentCertificate));
            }

            // Actually perform the migration
            migrateBO.setOldSmpId(existingMigrationRecord.getOldSmpId());
            manageParticipantIdentifierBusiness.performMigration(migrateBO);

            if (isDNSEnabled) {
                // If DNS update fails, then an exception is thrown and the database import is rolled back
                participantBO.setSmpId(migrateBO.getNewSmpId());
                dnsClientService.updateDNSRecordsForParticipant(participantBO, existingSmpBO);
            }
        } else {
            throw new UnauthorizedException(SMP_WAS_NOT_CREATED_WITH_THE_CERTIFICATE.getMessage(migrateBO.getNewSmpId(), currentCertificate));
        }
    }

    @Override
    public IManageParticipantIdentifierBusiness getManageParticipantIdentifierBusiness() {
        return manageParticipantIdentifierBusiness;
    }
}

