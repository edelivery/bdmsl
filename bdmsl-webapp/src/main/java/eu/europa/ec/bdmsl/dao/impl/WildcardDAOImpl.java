/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.impl;

import eu.europa.ec.bdmsl.common.bo.CertificateBO;
import eu.europa.ec.bdmsl.common.bo.WildcardBO;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.dao.AbstractDAOImpl;
import eu.europa.ec.bdmsl.dao.IWildcardDAO;
import eu.europa.ec.bdmsl.dao.entity.AllowedWildcardEntity;
import eu.europa.ec.bdmsl.dao.entity.AllowedWildcardEntityPK;
import eu.europa.ec.bdmsl.dao.entity.CertificateEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Repository;

import jakarta.persistence.TypedQuery;
import java.util.List;
import java.util.Objects;

import static eu.europa.ec.bdmsl.dao.QueryNames.ALLOWED_WILDCARD_ENTITY_GET_ALL_BY_CERTIFICATE_ID;

/**
 * @author Adrien FERIAL
 * @since 15/06/2015
 */
@Repository
public class WildcardDAOImpl extends AbstractDAOImpl implements IWildcardDAO {
    
    @Autowired
    private ConversionService conversionService;
    
    @Override
    public void changeWildcardAuthorization(Long oldId, Long newCertificateId) throws TechnicalException {
        if (Objects.equals(oldId, newCertificateId)) {
            return;
        }

        // envers can't intercept bulk-update changes therefore we modify the entities via objects.
        List<AllowedWildcardEntity> lstWCToChagne = getWildcardByCertificateId(oldId);
        CertificateEntity newCertificateEntity = getEntityManager().find(CertificateEntity.class, newCertificateId);

        for (AllowedWildcardEntity allowedWildcardEntity : lstWCToChagne) {
            // because certificate is part of key - old record is removed and "new entity" is added
            getEntityManager().remove(allowedWildcardEntity);
            // add new entry
            AllowedWildcardEntity newWilcard = new AllowedWildcardEntity(newCertificateEntity, allowedWildcardEntity.getScheme());
            getEntityManager().persist(newWilcard);
        }
    }

    @Override
    public WildcardBO findWildcard(String scheme, CertificateBO certificateBO) throws TechnicalException {
        WildcardBO resultBO = null;
        AllowedWildcardEntityPK pk = new AllowedWildcardEntityPK();
        pk.setCertificate(getEntityManager().find(CertificateEntity.class, certificateBO.getId()));
        pk.setScheme(scheme);
        AllowedWildcardEntity resultWildcardEntity = getEntityManager().find(AllowedWildcardEntity.class, pk);
        if (resultWildcardEntity != null) {
            resultBO = conversionService.convert(resultWildcardEntity, WildcardBO.class);
        } else {
            loggingService.debug("No wildcard authorization found for scheme " + scheme + " and certificate " + certificateBO.getCertificateId());
        }

        return resultBO;
    }

    /**
     * Returns all Wilcards for certificate id
     *
     * @param certificateId
     * @return
     * @throws TechnicalException
     */
    protected List<AllowedWildcardEntity> getWildcardByCertificateId(Long certificateId) throws TechnicalException {

        TypedQuery<AllowedWildcardEntity> query = getEntityManager().createNamedQuery(ALLOWED_WILDCARD_ENTITY_GET_ALL_BY_CERTIFICATE_ID, AllowedWildcardEntity.class);
        query.setParameter("certificateId", certificateId);
        return query.getResultList();
    }
}
