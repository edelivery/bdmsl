/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.business.impl;

import eu.europa.ec.bdmsl.business.IManageDNSRecordBusiness;
import eu.europa.ec.bdmsl.common.bo.DNSRecordBO;
import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import eu.europa.ec.bdmsl.common.business.AbstractBusinessImpl;
import eu.europa.ec.bdmsl.common.enums.DNSRecordEnum;
import eu.europa.ec.bdmsl.common.exception.BadRequestException;
import eu.europa.ec.bdmsl.common.exception.InvalidArgumentException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.common.util.StringUtil;
import eu.europa.ec.bdmsl.dao.IDNSRecordDAO;
import eu.europa.ec.bdmsl.dao.ISubdomainDAO;
import eu.europa.ec.bdmsl.service.dns.IDnsClientService;
import eu.europa.ec.bdmsl.util.DataValidator;
import org.apache.commons.lang3.StringUtils;
import org.apache.hc.core5.net.InetAddressUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xbill.DNS.Name;
import org.xbill.DNS.TextParseException;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import static eu.europa.ec.bdmsl.common.exception.ErrorMessagesType.*;
import static eu.europa.ec.bdmsl.common.exception.Messages.*;


@Component
public class ManageDNSRecordBusinessImpl extends AbstractBusinessImpl implements IManageDNSRecordBusiness {

    @Autowired
    private IDNSRecordDAO dnsRecordDAO;

    @Autowired
    IDnsClientService dnsClientService;

    @Autowired
    private ISubdomainDAO subdomainDAO;

    @Override
    public List<DNSRecordBO> deleteDNSRecords(String dnsRecordName, String dnsRecordZone) throws TechnicalException {

        if (StringUtils.isBlank(dnsRecordName)) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_EMPTY_DNS_NAME);
        }
        if (StringUtils.isBlank(dnsRecordZone)) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_EMPTY_DNSZONE);
        }

        String name = dnsRecordName.trim().toLowerCase();
        Name dnsName;
        try {
            dnsName = Name.fromString(StringUtil.addFinalDot(name));
        } catch (TextParseException ex) {
            loggingService.error(INVALID_DNS_NAME.getMessage(name), ex);
            throw new BadRequestException(BAD_REQUEST_PARAMETER_EMPTY_DNS_NAME);
        }

        String dnsZone = dnsRecordZone.trim().toLowerCase();
        Name dnsZoneName;
        try {
            dnsZoneName = Name.fromString(StringUtil.addFinalDot(dnsZone));
        } catch (TextParseException ex) {
            loggingService.error(INVALID_DNS_ZONE_NAME.getMessage(dnsZone), ex);
            throw new BadRequestException(BAD_REQUEST_PARAMETER_INVALID_DNSZONE);
        }


        // remove from database
        List<DNSRecordBO> res = dnsRecordDAO.deleteDNSRecords(name, dnsZone);

        // remove from DNS
        dnsClientService.deleteCustomDNSRecord(dnsName, dnsZoneName);


        return res;
    }

    @Override
    public DNSRecordBO addDNSRecord(DNSRecordBO dnsRecordBO) throws TechnicalException {

        if (StringUtils.isBlank(dnsRecordBO.getType())) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_EMPTY_DNS_ENTRY_RT);
        }

        Optional<DNSRecordEnum> recTypeOpt = DNSRecordEnum.getByCode(dnsRecordBO.getType());
        if (!recTypeOpt.isPresent()) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_INVALID_DNS_ENTRY_RT);
        }

        if (Objects.equals(recTypeOpt.get(), DNSRecordEnum.NAPTR)
                && StringUtils.isBlank(dnsRecordBO.getService())) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_EMPTY_DNS_SERVICE);
        }


        // validate data
        DNSRecordBO normalizedRecord = validateDNSRecord(dnsRecordBO, recTypeOpt.get());
        // check if already exists in database DNS
        Optional<DNSRecordBO> recordBO = dnsRecordDAO.getDNSRecord(normalizedRecord.getType(), normalizedRecord.getName(),
                normalizedRecord.getDNSZone(), normalizedRecord.getValue());
        if (recordBO.isPresent()) {
            throw new BadRequestException("DNS record already exists:" + recordBO.get());
        }


        // add to database
        dnsRecordDAO.addDNSRecord(normalizedRecord);
        // add to dns
        dnsClientService.createCustomDNSRecord(normalizedRecord, recTypeOpt.get());

        return normalizedRecord;
    }


    protected DNSRecordBO validateDNSRecord(DNSRecordBO dnsRecordBO, DNSRecordEnum dnsRecordType) throws TechnicalException {
        if (StringUtils.isBlank(dnsRecordBO.getName())) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_EMPTY_DNS_NAME);
        }
        if (StringUtils.isBlank(dnsRecordBO.getType())) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_EMPTY_DNS_RT);
        }
        if (StringUtils.isBlank(dnsRecordBO.getValue())) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_EMPTY_DNS_VALUE);
        }

        if (StringUtils.isBlank(dnsRecordBO.getDNSZone())) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_EMPTY_DNSZONE);
        }

        String name = dnsRecordBO.getName().trim().toLowerCase();
        try {
            Name dnsName = Name.fromString(StringUtil.addFinalDot(name));
        } catch (TextParseException ex) {
            loggingService.error(INVALID_DNS_NAME.getMessage(name), ex);
            throw new BadRequestException(BAD_REQUEST_PARAMETER_INVALID_DNS_NAME);
        }

        String dnsZone = dnsRecordBO.getDNSZone().trim().toLowerCase();
        try {
            Name dnsZoneName = Name.fromString(StringUtil.addFinalDot(dnsZone));
        } catch (TextParseException ex) {
            loggingService.error(INVALID_DNS_ZONE_NAME.getMessage(dnsZone), ex);
            throw new BadRequestException(BAD_REQUEST_PARAMETER_INVALID_DNSZONE);
        }
        // test if valid dns zone
        isValidDNSZone(dnsZone);

        if (!name.endsWith(dnsZone)) {
            String message = "Invalid DNS Name: [" + name + "]! Name must end with DNS Zone: [" + dnsZone + "]";
            loggingService.error(message, null);
            throw new BadRequestException(BAD_REQUEST_PARAMETER_INVALID_DNS_NAME + " Name must end with DNS Zone: [" + dnsZone + "]");
        }

        switch (dnsRecordType) {
            case A:
                return validateADNSRecord(dnsRecordBO, name, dnsZone);
            case NAPTR:
                return validateNaptrDNSRecord(dnsRecordBO, name, dnsZone);
            case CNAME:
                return validateCNameDNSRecord(dnsRecordBO, name, dnsZone);
            default:
                throw new BadRequestException("Not supported DNS type: " + dnsRecordBO.getType());
        }
    }

    protected void isValidDNSZone(String dnsZone) throws TechnicalException {
        List<SubdomainBO> subdomainBOList = subdomainDAO.findAll();

        Optional<SubdomainBO> optHasZone = subdomainBOList.stream().filter(subdomainBO -> StringUtils.endsWithIgnoreCase(subdomainBO.getDnsZone(), dnsZone)).findAny();
        if (!optHasZone.isPresent()) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_INVALID_DNSZONE + " It must be one of the dns zones registered for subdomains ex.: "
                    + (subdomainBOList.isEmpty() ? "" : subdomainBOList.get(0).getDnsZone()));
        }

    }

    protected DNSRecordBO validateADNSRecord(DNSRecordBO dnsRecordBO, String name, String dnsZone) throws BadRequestException {

        String valueIPAddress = dnsRecordBO.getValue().trim().toLowerCase();
        if (!InetAddressUtils.isIPv4Address(valueIPAddress)) {
            throw new BadRequestException(NOT_VALID_IP_ADDRESS,valueIPAddress);
        }
        return createDnsRecordBO(DNSRecordEnum.A.getCode(), name, dnsZone, valueIPAddress, null);
    }

    protected DNSRecordBO validateCNameDNSRecord(DNSRecordBO dnsRecordBO, String name, String dnsZone) throws BadRequestException {
        String domainValue = dnsRecordBO.getValue().trim().toLowerCase();
        try {
            DataValidator.validateDnsDomainName(domainValue);
        } catch (InvalidArgumentException e) {
            throw new BadRequestException(NOT_VALID_DOMAIN_ADDRESS,domainValue);
        }
        return createDnsRecordBO(DNSRecordEnum.CNAME.getCode(), name, dnsZone, domainValue, null);
    }

    protected DNSRecordBO validateNaptrDNSRecord(DNSRecordBO dnsRecordBO, String name, String dnsZone) throws BadRequestException {
        String regExpValue = dnsRecordBO.getValue().trim();
        String srv = dnsRecordBO.getService() != null ? dnsRecordBO.getService().trim() : null;

        try {
            Pattern.compile(regExpValue);
        } catch (PatternSyntaxException e) {
            throw new BadRequestException("Value: [" + regExpValue + "] is invalid regular expression!");

        }
        return createDnsRecordBO(DNSRecordEnum.NAPTR.getCode(), name, dnsZone, regExpValue, srv);
    }

    protected static DNSRecordBO createDnsRecordBO(String type, String name, String dnsZone, String value, String service) {
        DNSRecordBO dnsRecordBO = new DNSRecordBO();
        dnsRecordBO.setName(name);
        dnsRecordBO.setDNSZone(dnsZone);
        dnsRecordBO.setType(type);
        dnsRecordBO.setValue(value);
        dnsRecordBO.setService(service);
        return dnsRecordBO;

    }
}
