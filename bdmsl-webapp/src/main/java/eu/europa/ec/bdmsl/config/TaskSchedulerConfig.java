/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.config;


import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.cron.CronTriggerConfig;
import eu.europa.ec.bdmsl.cron.DynamicCronTrigger;
import eu.europa.ec.bdmsl.dao.IConfigurationDAO;
import eu.europa.ec.bdmsl.service.IDataQualityInconsistencyAnalyzerService;
import eu.europa.ec.bdmsl.service.IManageCertificateService;
import eu.europa.ec.bdmsl.service.IReportService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.config.CronTask;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import java.util.List;
import java.util.concurrent.Executor;

/**
 * Change XML task schedule configuration with java bean configuration. The purpose of the class is to link triggers
 * with the task execution objects. Also make it dynamically update the cron when the sml cron property change.
 *
 * @author Joze RIHTARSIC
 * @since 4.2
 */

@Configuration
@EnableScheduling
public class TaskSchedulerConfig implements SchedulingConfigurer {
    private static final Log LOG = LogFactory.getLog(TaskSchedulerConfig.class);

    final IConfigurationDAO configurationDao;
    final IDataQualityInconsistencyAnalyzerService dataQualityInconsistencyAnalyzerService;
    final IManageCertificateService manageCertificateService;
    final IReportService reportService;
    final DynamicCronTrigger refreshPropertiesTrigger;
    final DynamicCronTrigger changeCertificateCronTrigger;
    final DynamicCronTrigger inconsistencyReportTrigger;
    final DynamicCronTrigger smpExpiredCertReportTrigger;

    ScheduledTaskRegistrar taskRegistrar;

    @Autowired
    public TaskSchedulerConfig(
            IConfigurationDAO configurationDao,
            IDataQualityInconsistencyAnalyzerService dataQualityInconsistencyAnalyzerService,
            IManageCertificateService manageCertificateService,
            IReportService reportService,
            @Qualifier(CronTriggerConfig.TRIGGER_BEAN_PROPERTY_REFRESH) DynamicCronTrigger refreshPropertiesTrigger,
            @Qualifier(CronTriggerConfig.TRIGGER_BEAN_CHANGE_CERTIFICATE) DynamicCronTrigger changeCertificateCronTrigger,
            @Qualifier(CronTriggerConfig.TRIGGER_BEAN_INCONSISTENCY_REPORT) DynamicCronTrigger inconsistencyReportTrigger,
            @Qualifier(CronTriggerConfig.TRIGGER_BEAN_SMP_WITH_EXPIRED_CERT_REPORT) DynamicCronTrigger smpExpiredCertReportTrigger
    ) {
        this.configurationDao = configurationDao;
        this.dataQualityInconsistencyAnalyzerService = dataQualityInconsistencyAnalyzerService;
        this.reportService = reportService;
        this.manageCertificateService = manageCertificateService;

        this.refreshPropertiesTrigger = refreshPropertiesTrigger;
        this.changeCertificateCronTrigger = changeCertificateCronTrigger;
        this.inconsistencyReportTrigger = inconsistencyReportTrigger;
        this.smpExpiredCertReportTrigger = smpExpiredCertReportTrigger;
    }

    @Bean
    public Executor taskExecutor() {
        //return Executors.newSingleThreadScheduledExecutor();
        ThreadPoolTaskScheduler taskExecutor = new ThreadPoolTaskScheduler();
        taskExecutor.setPoolSize(1);
        taskExecutor.setAwaitTerminationSeconds(300);
        return taskExecutor;


    }

    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        this.taskRegistrar = taskRegistrar;
        LOG.info("Configure cron tasks");
        this.taskRegistrar.setScheduler(taskExecutor());
        LOG.debug("Configure cron task for property refresh");
        this.taskRegistrar.addTriggerTask(
                configurationDao::refreshProperties,
                refreshPropertiesTrigger
        );

        LOG.debug("Configure cron task for updating the certificates");
        this.taskRegistrar.addTriggerTask(
                () -> {
                    try {
                        manageCertificateService.changeCertificates();
                    } catch (TechnicalException e) {
                        LOG.error("Error occurred when triggering the data certificate change", e);
                    }
                },
                changeCertificateCronTrigger
        );

        LOG.debug("Configure cron task for inconsistency verification");
        this.taskRegistrar.addTriggerTask(
                () -> {
                    try {
                        dataQualityInconsistencyAnalyzerService.checkDataInconsistencies();
                    } catch (TechnicalException e) {
                        LOG.error("Error occurred when triggering the data inconsistency report", e);
                    }
                },
                inconsistencyReportTrigger
        );

        LOG.debug("Configure cron task for generating the SMP expired certificate report");
        this.taskRegistrar.addTriggerTask(
                reportService::generateSMPsWithExpiredCertificatesReport,
                smpExpiredCertReportTrigger
        );
    }

    public void updateCronTasks() { //call it when you want to change chron
        synchronized (TaskSchedulerConfig.class) {
            List<CronTask> crons = this.taskRegistrar.getCronTaskList();
            taskRegistrar.destroy(); //important, cleanups current scheduled tasks
            taskRegistrar.afterPropertiesSet(); //rebuild
        }
    }
}
