/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.business.impl;

import com.google.common.base.Strings;
import eu.europa.ec.bdmsl.business.ICertificateDomainBusiness;
import eu.europa.ec.bdmsl.business.IManageCertificateBusiness;
import eu.europa.ec.bdmsl.business.IX509CertificateBusiness;
import eu.europa.ec.bdmsl.common.bo.*;
import eu.europa.ec.bdmsl.common.business.AbstractBusinessImpl;
import eu.europa.ec.bdmsl.common.exception.*;
import eu.europa.ec.bdmsl.dao.ICertificateDAO;
import eu.europa.ec.bdmsl.dao.ISmpDAO;
import eu.europa.ec.bdmsl.dao.IWildcardDAO;
import eu.europa.ec.bdmsl.dao.entity.CertificateEntity;
import eu.europa.ec.bdmsl.security.CertificateDetails;
import eu.europa.ec.bdmsl.util.CertificateUtils;
import eu.europa.ec.edelivery.security.utils.X509CertificateUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.naming.InvalidNameException;
import javax.naming.ldap.LdapName;
import javax.naming.ldap.Rdn;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;

/**
 * @author Adrien FERIAL
 * @since 15/06/2015
 */
@Component
public class ManageCertificateBusinessImpl extends AbstractBusinessImpl implements IManageCertificateBusiness {

    ICertificateDAO certificateDAO;

    ICertificateDomainBusiness certificateDomainBusiness;

    IX509CertificateBusiness x509CertificateBusiness;

    ISmpDAO smpDAO;

    IWildcardDAO wildcardDAO;

    @Autowired
    public ManageCertificateBusinessImpl(ICertificateDAO certificateDAO, ICertificateDomainBusiness certificateDomainBusiness,
                                         IX509CertificateBusiness x509CertificateBusiness, ISmpDAO smpDAO, IWildcardDAO wildcardDAO) {
        this.certificateDAO = certificateDAO;
        this.certificateDomainBusiness = certificateDomainBusiness;
        this.x509CertificateBusiness = x509CertificateBusiness;
        this.smpDAO = smpDAO;
        this.wildcardDAO = wildcardDAO;
    }

    @Override
    public void domainValidationForNewCertificate(X509Certificate newCertificate) throws  TechnicalException {
        String domainForNewCertificateStr = x509CertificateBusiness.validateClientX509Certificate(new X509Certificate[]{newCertificate}, false);
        CertificateDomainBO domainForNewCertificate = certificateDomainBusiness.findDomain(domainForNewCertificateStr);
        CertificateDomainBO domainForCurrentCertificate = null;

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getDetails() != null && authentication.getDetails() instanceof CertificateDetails) {
            domainForCurrentCertificate = certificateDomainBusiness.findDomain(((CertificateDetails) authentication.getDetails()).getSubject());
            if (domainForCurrentCertificate == null) {
                domainForCurrentCertificate = certificateDomainBusiness.findDomain(((CertificateDetails) authentication.getDetails()).getIssuer());
            }
        }

        if (domainForCurrentCertificate == null || domainForNewCertificate == null) {
            throw new GenericTechnicalException("Impossible to find out the domain of either the current certificate or new certificate.");
        }

        if (!domainForCurrentCertificate.getSubdomain().getSubdomainName().equals(domainForNewCertificate.getSubdomain().getSubdomainName())) {
            throw new BadRequestException(String.format("The new certificate '%s' with domain '%d' does not belong to the same subdomain '%d' as the current certificate %s.",
                    domainForNewCertificate.getCertificate(), domainForNewCertificate.getSubdomain().getSubdomainId(), domainForCurrentCertificate.getSubdomain().getSubdomainId(),
                    domainForCurrentCertificate.getCertificate()));
        }
    }

    @Override
    public void validateChangeCertificate(ChangeCertificateBO changeCertificateBO) throws  TechnicalException {
        validateChangeCertificate(changeCertificateBO.getPublicKey());
    }

    @Override
    public void validateChangeCertificate(PrepareChangeCertificateBO prepareChangeCertificateBO) throws  TechnicalException {
        validateChangeCertificate(prepareChangeCertificateBO.getPublicKey());
    }


    private void validateChangeCertificate(byte[] publicKey) throws  TechnicalException {
        if (publicKey == null || publicKey.length == 0) {
            throw new BadRequestException("The public key can't be null or empty");
        }
    }

    private void validateChangeCertificate(String publicKey) throws  TechnicalException {
        if (Strings.isNullOrEmpty(publicKey)) {
            throw new BadRequestException("The public key can't be null or empty");
        }
    }

    @Override
    public void checkMigrationDate(X509Certificate certificate, PrepareChangeCertificateBO prepareChangeCertificateBO) throws  TechnicalException {
        if (prepareChangeCertificateBO.getMigrationDate() != null && new Date().after(prepareChangeCertificateBO.getMigrationDate().getTime())) {
            throw new BadRequestException("The migration date can't be in the past");
        }
        if (!prepareChangeCertificateBO.getMigrationDate().getTime().after(certificate.getNotBefore()) || !prepareChangeCertificateBO.getMigrationDate().getTime().before(certificate.getNotAfter())) {
            throw new BadRequestException("The migration date must be within " + certificate.getNotBefore() + " and " + certificate.getNotAfter());
        }
    }

    @Override
    public X509Certificate getX509Certificate(String publicKey) throws BadRequestException {
        try {
            return X509CertificateUtils.getX509Certificate(publicKey);
        } catch (CertificateException exc) {
            throw new BadRequestException(String.format("The certificate is not valid - [%s]", ExceptionUtils.getRootCause(exc)), exc);
        }
    }

    @Override
    public X509Certificate getX509Certificate(byte[] publicKey) throws BadRequestException {
        try {
            return X509CertificateUtils.getX509Certificate(publicKey);
        } catch (CertificateException exc) {
            throw new BadRequestException(String.format("The certificate is not valid - [%s]", ExceptionUtils.getRootCause(exc)), exc);
        }
    }

    @Override
    public CertificateBO extractCertificate(String publicKey) throws  TechnicalException {

        X509Certificate newCert = getX509Certificate(publicKey);
        CertificateBO certificateBO = CertificateUtils.extractCertificate(newCert);
        return certificateBO;
    }

    @Override
    public CertificateBO extractCertificate(byte[] publicKey) throws  TechnicalException {

        X509Certificate newCert = getX509Certificate(publicKey);
        CertificateBO certificateBO = CertificateUtils.extractCertificate(newCert);
        return certificateBO;
    }

    @Override
    public Long createNewCertificate(CertificateBO certificateBO) throws  TechnicalException {
        return certificateDAO.createCertificate(certificateBO);
    }

    @Override
    public void updateCertificate(CertificateBO certificateBO) throws  TechnicalException {
        certificateDAO.updateCertificate(certificateBO);
    }

    @Override
    public CertificateBO findCertificate(String certificateId) throws  TechnicalException {
        return certificateDAO.findCertificateByCertificateId(certificateId);
    }

    @Override
    public CertificateBO findCertificateById(Long id) throws  TechnicalException {
        return certificateDAO.findCertificateById(id);
    }

    @Override
    public int changeCertificates() throws  TechnicalException {
        int count = 0;
        // List all certificates with a migration date in the past or at the present day
        List<CertificateBO> certificateBOList = certificateDAO.findCertificatesToChange(Calendar.getInstance());
        if (certificateBOList != null && !certificateBOList.isEmpty()) {
            for (CertificateBO certificateBO : certificateBOList) {
                loggingService.info("Migrating from certificate " + certificateBO.getId() + " to " + certificateBO.getNewCertificateId());
                // change the certificate for all the SMP
                loggingService.info("Changing certificate for SMP from " + certificateBO.getId() + " to " + certificateBO.getNewCertificateId());
                smpDAO.changeCertificateForSMP(certificateBO.getId(), certificateBO.getNewCertificateId());

                // change the wildcard authorization
                loggingService.info("Changing certificate for the wildcard authorization from " + certificateBO.getId() + " to " + certificateBO.getNewCertificateId());
                wildcardDAO.changeWildcardAuthorization(certificateBO.getId(), certificateBO.getNewCertificateId());

                // delete the old certificate
                loggingService.info(String.format("Removing old certificate id %s and certificate id %s . ", certificateBO.getId(), certificateBO.getCertificateId()));
                certificateDAO.delete(certificateBO);

                count++;

                try {
                    CertificateBO newCertificate = findCertificateById(certificateBO.getNewCertificateId());
                    if (newCertificate.getPemEncoding() != null) {
                        X509Certificate certificate = getX509Certificate(newCertificate.getPemEncoding());
                        certificateDomainBusiness.changeCertificateDomain(certificate, certificateBO, newCertificate, false);
                    }
                } catch (TechnicalException e) {
                    throw e;
                } catch (Exception e) {
                    throw new GenericTechnicalException(e.getMessage(), e);
                }
            }
        } else {
            loggingService.info("No certificates must be changed");
        }
        return count;
    }

    @Override
    public void changeCertificate(CertificateBO newCertificateBO, CertificateBO oldCertificateBO) throws  TechnicalException {

        loggingService.info("Changing certificate for SMP from " + oldCertificateBO.getCertificateId() + " to " + newCertificateBO.getCertificateId());
        smpDAO.changeCertificateForSMP(oldCertificateBO.getId(), newCertificateBO.getId());

        loggingService.info("Changing certificate for the wildcard authorization from " + oldCertificateBO.getCertificateId() + " to " + newCertificateBO.getCertificateId());
        wildcardDAO.changeWildcardAuthorization(oldCertificateBO.getId(), newCertificateBO.getId());

        loggingService.info(String.format("Removing old certificate id %s and certificate id %s . ", oldCertificateBO.getId(), oldCertificateBO.getCertificateId()));
        try {
            certificateDAO.delete(oldCertificateBO);
        } catch (Exception e) {
            throw new BadRequestException("Certificate '" + oldCertificateBO.getCertificateId() + "' Could not be deleted.", e);
        }

    }

    @Override
    public WildcardBO findWildCard(String scheme, String currentCertificate) throws  TechnicalException {
        CertificateBO certificateBO = certificateDAO.findCertificateByCertificateId(currentCertificate);
        return wildcardDAO.findWildcard(scheme, certificateBO);
    }

    @Override
    public boolean equalsCertificate(String certificateId1, String certificateId2) throws TechnicalException {
        String subjectName1;
        String subjectName2;
        try {
            subjectName1 = orderCertificate(certificateId1);
        } catch (final Exception exc) {
            // Can only happen with the http-unsecure-client certificate
            subjectName1 = certificateId1;
        }
        try {
            subjectName2 = orderCertificate(certificateId2);
        } catch (final Exception exc) {
            // Can only happen with the http-unsecure-client certificate
            subjectName2 = certificateId2;
        }
        return subjectName1.equals(subjectName2);
    }

    private String orderCertificate(String certificateId) throws InvalidNameException {
        final LdapName ldapName1 = new LdapName(certificateId);
        final Map<String, Rdn> parts = new HashMap<>();
        for (final Rdn rdn : ldapName1.getRdns()) {
            parts.put(rdn.getType(), rdn);
        }
        return parts.get("CN").toString() + "," + parts.get("O").toString() + "," + parts.get("C").toString();
    }

    @Override
    public void validateCurrentCertificate(CertificateBO currentCertificate) throws TechnicalException {
        Calendar calendar = new GregorianCalendar();

        if (!calendar.after(currentCertificate.getValidFrom())) {
            throw new CertificateNotYetValidException("Client certificate is not yet valid.");
        }

        if (!calendar.before(currentCertificate.getValidTo())) {
            throw new CertificateExpiredException("Client certificate has expired.");
        }
    }

    @Override
    public X509Certificate getX509CertificateForCertificateId(String certificateId) throws TechnicalException {
        Optional<CertificateEntity> certificateEntity = certificateDAO.findCertificateEntityByCertificateId(certificateId);
        if (!certificateEntity.isPresent() || StringUtils.isBlank(certificateEntity.get().getPemEncoding())) {
            return null;
        }
        return getX509Certificate(certificateEntity.get().getPemEncoding());
    }
}
