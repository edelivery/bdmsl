/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.business.impl;

import com.google.common.base.Strings;
import eu.europa.ec.bdmsl.business.IConfigurationBusiness;
import eu.europa.ec.bdmsl.business.IManageCertificateBusiness;
import eu.europa.ec.bdmsl.business.IManageServiceMetadataBusiness;
import eu.europa.ec.bdmsl.common.bo.CertificateBO;
import eu.europa.ec.bdmsl.common.bo.CertificateDomainBO;
import eu.europa.ec.bdmsl.common.bo.MigrationRecordBO;
import eu.europa.ec.bdmsl.common.bo.ServiceMetadataPublisherBO;
import eu.europa.ec.bdmsl.common.business.AbstractBusinessImpl;
import eu.europa.ec.bdmsl.common.exception.*;
import eu.europa.ec.bdmsl.dao.ICertificateDAO;
import eu.europa.ec.bdmsl.dao.ICertificateDomainDAO;
import eu.europa.ec.bdmsl.dao.IMigrationDAO;
import eu.europa.ec.bdmsl.dao.ISmpDAO;
import eu.europa.ec.bdmsl.security.CertificateAuthentication;
import eu.europa.ec.bdmsl.security.CertificateDetails;
import eu.europa.ec.bdmsl.security.UnsecureAuthentication;
import eu.europa.ec.bdmsl.service.dns.impl.SupportedDnsRecordType;
import eu.europa.ec.bdmsl.util.ConstraintsUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import jakarta.transaction.Transactional;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import static eu.europa.ec.bdmsl.common.exception.ErrorMessagesType.SMP_WAS_NOT_CREATED_WITH_THE_CERTIFICATE;
import static eu.europa.ec.bdmsl.common.exception.Messages.BAD_REQUEST_PARAMETER_INVALID_PUBLISHER_ID;

/**
 * @author Adrien FERIAL
 * @since 15/06/2015
 */
@Component
public class ManageServiceMetadataBusinessImpl extends AbstractBusinessImpl implements IManageServiceMetadataBusiness {

    private static final String DOMAIN_IDENTIFIER = "((\\p{Alnum})([-]|(\\p{Alnum}))*(\\p{Alnum}))|(\\p{Alnum})";
    private static final String DOMAIN_NAME_RULE = "(" + DOMAIN_IDENTIFIER + ")((\\.)(" + DOMAIN_IDENTIFIER + "))*";
    private static final String IP_V4_RULE = "(\\p{Digit})+(\\.)(\\p{Digit})+(\\.)(\\p{Digit})+(\\.)(\\p{Digit})+";

    @Autowired
    private ISmpDAO smpDAO;

    @Autowired
    private IConfigurationBusiness configurationBusiness;

    @Autowired
    private ICertificateDAO certificateDAO;

    @Autowired
    private IMigrationDAO migrationDAO;

    @Autowired
    private ICertificateDomainDAO certificateDomainDAO;

    @Autowired
    private IManageCertificateBusiness manageCertificateBusiness;

    @Override
    public ServiceMetadataPublisherBO read(String id) throws TechnicalException {
        return smpDAO.findSMP(id);
    }

    @Override
    public boolean isLogicalAddressToBeUpdated(ServiceMetadataPublisherBO toBeUpdatedSMP) throws TechnicalException {
        ServiceMetadataPublisherBO persistedSMP = smpDAO.findSMP(toBeUpdatedSMP.getSmpId());
        // url  - the context is case sensitive!
        return !StringUtils.equals(persistedSMP.getLogicalAddress(), toBeUpdatedSMP.getLogicalAddress());
    }

    @Override
    public boolean hasNaptrRecords(ServiceMetadataPublisherBO toBeUpdatedSMP) throws TechnicalException {
        ServiceMetadataPublisherBO persistedSMP = smpDAO.findSMP(toBeUpdatedSMP.getSmpId());
        SupportedDnsRecordType recordType = SupportedDnsRecordType.valueOf(persistedSMP.getSubdomain().getDnsRecordTypes().trim().toUpperCase());

        return recordType == SupportedDnsRecordType.ALL || recordType == SupportedDnsRecordType.NAPTR;
    }

    @Override
    public void validateSMPSubdomain(ServiceMetadataPublisherBO smpBO) throws TechnicalException {

        CertificateDomainBO certificateDomainBO = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CertificateDetails certificateDetails = (CertificateDetails) authentication.getDetails();
        if (authentication instanceof CertificateAuthentication) {
            certificateDomainBO = ((CertificateAuthentication) authentication).getGrantedSubDomainAnchor();
        } else if (authentication instanceof UnsecureAuthentication) {
            certificateDomainBO = certificateDomainDAO.findDomain(certificateDetails);
        }

        if (certificateDomainBO == null || certificateDomainBO.getSubdomain() == null) {
            throw new BadConfigurationException(String.format("Certificate Domain not found for certificate %s.", certificateDetails.getCertificateId()));
        }

        ServiceMetadataPublisherBO smpdb = smpDAO.findSMP(smpBO.getSmpId());
        if (smpdb == null || smpdb.getSubdomain() == null) {
            throw new BadConfigurationException(String.format("SMP Domain not found for smp %s.", smpBO.getSmpId()));
        }

        if (!Objects.equals(smpdb.getSubdomain().getSubdomainId(), certificateDomainBO.getSubdomain().getSubdomainId())) {
            throw new BadConfigurationException(String.format("SMP: '%s' domain '%d' does not match authorized certificate '%s' domain: '%d' .",
                    smpdb.getSmpId(),
                    smpdb.getSubdomain().getSubdomainId(),
                    certificateDetails.getCertificateId(), certificateDomainBO.getSubdomain().getSubdomainId()));
        }
    }

    @Override
    public void validateSMPData(ServiceMetadataPublisherBO smpBO) throws TechnicalException {

        this.validateSMPId(smpBO.getSmpId());

        if (Strings.isNullOrEmpty(smpBO.getLogicalAddress())) {
            throw new BadRequestException("Logical address must not be null or empty.");
        }

        if (!smpBO.getLogicalAddress().matches(ConstraintsUtil.LOGICAL_ADDRESS_PROTOCOL_REGEX)) {
            throw new BadRequestException(String.format("Logical address is invalid [%s].", smpBO.getLogicalAddress()));
        }

        if (Strings.isNullOrEmpty(smpBO.getPhysicalAddress())) {
            throw new BadRequestException("Physical address must not be null or empty.");
        }

        String protocol;
        try {
            protocol = new URL(smpBO.getLogicalAddress()).getProtocol();
        } catch (final MalformedURLException ex) {
            throw new BadRequestException("Logical address is malformed: " + ex.getMessage(), ex);
        }

        if (!smpBO.getPhysicalAddress().matches(IP_V4_RULE)) {
            throw new BadRequestException("Physical address is malformed: " + smpBO.getPhysicalAddress());
        }

        protocolValidation(protocol);
    }

    private void protocolValidation(String protocol) throws TechnicalException {
        String smpLogicalAddressProtocolRestriction = findConfigurationProperty();
        if (!smpLogicalAddressProtocolRestriction.equalsIgnoreCase("all")) {
            if (!protocol.equalsIgnoreCase(smpLogicalAddressProtocolRestriction.trim())) {
                throw new BadRequestException(String.format("Wrong Protocol in LogicalAddress field. For this domain only %s protocol is allowed.", smpLogicalAddressProtocolRestriction.trim().toUpperCase(Locale.US)));
            }
        }
    }

    protected String findConfigurationProperty() throws TechnicalException {
        CertificateDetails certificateDetails = ((CertificateDetails) SecurityContextHolder.getContext().getAuthentication().getDetails());
        CertificateDomainBO certificateDomainBO = certificateDomainDAO.findDomain(certificateDetails);

        if (certificateDomainBO == null || certificateDomainBO.getSubdomain() == null) {
            throw new BadRequestException(String.format("Certificate Domain not found for certificate %s.", certificateDetails.getCertificateId()));
        }
        String schemas = certificateDomainBO.getSubdomain().getSmpUrlSchemas();

        if (StringUtils.isBlank(schemas)) {
            throw new BadConfigurationException("Smp logical address protocol restriction is not defined for domain " + certificateDomainBO.getSubdomain().getSubdomainName());
        }
        return schemas;
    }

    @Override
    public void verifySMPNotExist(String smpId) throws TechnicalException {
        ServiceMetadataPublisherBO smp = smpDAO.findSMP(smpId);
        if (smp != null) {
            throw new BadRequestException("The SMP '" + smpId + "' already exists.");
        }
    }

    @Override
    public ServiceMetadataPublisherBO createSMP(ServiceMetadataPublisherBO smpBO) throws TechnicalException {
        CertificateAuthentication certificateAuthentication = (CertificateAuthentication) SecurityContextHolder.getContext().getAuthentication();
        return smpDAO.createSMP(smpBO, (CertificateDetails) certificateAuthentication.getDetails(), certificateAuthentication.getGrantedSubDomain());
    }

    @Override
    public void createCurrentCertificate() throws TechnicalException {
        CertificateDetails details = (CertificateDetails) SecurityContextHolder.getContext().getAuthentication().getDetails();
        CertificateBO certificateBO = new CertificateBO();
        certificateBO.setValidFrom(details.getValidFrom());
        certificateBO.setValidTo(details.getValidTo());
        certificateBO.setCertificateId(SecurityContextHolder.getContext().getAuthentication().getName());
        if (!Strings.isNullOrEmpty(details.getPemEncoding())) {
            certificateBO.setPemEncoding(details.getPemEncoding());
        }
        certificateDAO.createCertificate(certificateBO);
    }

    @Override
    public CertificateBO findCertificate(String name) throws TechnicalException {
        return certificateDAO.findCertificateByCertificateId(name);
    }

    @Override
    public ServiceMetadataPublisherBO verifySMPExist(String smpId) throws TechnicalException {
        ServiceMetadataPublisherBO smp = smpDAO.findSMP(smpId);
        if (smp == null) {
            throw new SmpNotFoundException(BAD_REQUEST_PARAMETER_INVALID_PUBLISHER_ID, smpId);
        }
        return smp;
    }

    @Override
    @Transactional
    public ServiceMetadataPublisherBO updateSMP(ServiceMetadataPublisherBO smpBO) throws TechnicalException {
        return smpDAO.updateSMP(smpBO);
    }


    @Override
    @Transactional
    public ServiceMetadataPublisherBO updateSMPIdentifier(ServiceMetadataPublisherBO smpBO, String newSMPIdentifier) {
        return smpDAO.updateSMPIdentifier(smpBO, newSMPIdentifier);
    }

    @Override
    @Transactional
    public ServiceMetadataPublisherBO deleteSMP(ServiceMetadataPublisherBO smpBO) throws TechnicalException {
        return smpDAO.deleteSMP(smpBO);
    }

    @Override
    public void disableSMP(ServiceMetadataPublisherBO smpBO, boolean disable) throws TechnicalException {
        smpDAO.disableSMP(smpBO, disable);
    }

    @Override
    public void validateSMPId(String smpId) throws TechnicalException {
        if (Strings.isNullOrEmpty(smpId)) {
            throw new BadRequestException("Publisher ID cannot be 'null' or empty");
        }
        if (!smpId.matches(DOMAIN_NAME_RULE)) {
            throw new BadRequestException("Publisher ID is malformed: " + smpId);
        }

        if (smpId.length() > 253) {
            throw new BadRequestException("Publisher ID total length > 253 : " + smpId + " : " + smpId.length());
        }

        final String[] parts = smpId.split("\\.");
        for (final String part : parts) {
            if (part.length() > 63) {
                throw new BadRequestException("Publisher ID part length > 63 : " + smpId);
            }
        }

        loggingService.debug("SMP id '" + smpId + "' is valid");
    }

    @Override
    public void checkNoMigrationPlanned(String smpId) throws TechnicalException {
        List<MigrationRecordBO> migrationRecordBOs = migrationDAO.findMigrationsRecordsForSMP(smpId);
        if (migrationRecordBOs != null && !migrationRecordBOs.isEmpty()) {
            throw new MigrationPlannedException("A migration is planned for the SMP " + smpId + ". Please contact your system administrator.");
        }
    }

    /**
     * Purpose of the method os to validated if authenticated smp's certificate matches the given
     * certificate id. If Authentication does not contain Certificate (ClientCert header) or if certificate is not
     * registered in database it validates only the certificate ids. Else Certificate is also validated!
     *
     * @param smpBO smpBO to be validated if authenticated smp's certificate matches the given certificate id
     * @return true/false if authenticated smp's certificate matches the given certificate id
     */
    public boolean isAuthorizedOnSMP(ServiceMetadataPublisherBO smpBO) throws TechnicalException {

        String certificateId = smpBO.getCertificateId();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            throw new UnauthorizedException("Missing authentication!");
        }

        if (authentication instanceof UnsecureAuthentication) {
            return configurationBusiness.isUnsecureLoginEnabled();
        }
        if (!(authentication instanceof CertificateAuthentication)) {
            throw new InternalError("Unknown authentication type: [" + authentication.getClass() + "]!");
        }

        CertificateAuthentication certificateAuthentication = (CertificateAuthentication) authentication;
        // validates if certificate id equals authorization id
        if (!manageCertificateBusiness.equalsCertificate(certificateId, certificateAuthentication.getName())) {
            return false;
        }
        // get certificate from authentication
        Object credentials = certificateAuthentication.getCredentials();
        X509Certificate authenticatedCertificate = null;
        if (credentials instanceof X509Certificate) {
            authenticatedCertificate = (X509Certificate) credentials;
        }
        //
        if (authenticatedCertificate == null) {
            loggingService.warn("Missing X509Certificate in authentication!");
            return true;
        }

        X509Certificate certificate = manageCertificateBusiness.getX509CertificateForCertificateId(certificateId);
        if (certificate == null) {
            loggingService.warn("Missing X509Certificate in database!");
            return true;
        }
        return certificate.equals(authenticatedCertificate);
    }

    public void validateAuthorization(ServiceMetadataPublisherBO smpBO) throws TechnicalException {
        // validate authorization
        if (!isAuthorizedOnSMP(smpBO)) {
            String currentCertificate = (SecurityContextHolder.getContext() == null || SecurityContextHolder.getContext().getAuthentication() == null ?
                    "Not authenticated" : SecurityContextHolder.getContext().getAuthentication().getName());
            throw new UnauthorizedException(SMP_WAS_NOT_CREATED_WITH_THE_CERTIFICATE.getMessage(smpBO.getSmpId(),
                    currentCertificate));
        }
    }

    @Override
    @Transactional
    public ServiceMetadataPublisherBO prepareToBatchUpdateSMP(ServiceMetadataPublisherBO smpBO) throws TechnicalException {
        return smpDAO.cloneSMP(smpBO);
    }
}
