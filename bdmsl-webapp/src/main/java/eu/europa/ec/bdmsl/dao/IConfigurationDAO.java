/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao;

import eu.europa.ec.bdmsl.common.bo.ConfigurationBO;
import eu.europa.ec.bdmsl.common.enums.SMLPropertyEnum;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;

/**
 * @author Flavio SANTOS
 * @since 08/05/2017
 */
public interface IConfigurationDAO {
    /**
     * This method finds a configuration property
     *
     * @param propertyName
     * @return
     * @throws TechnicalException
     */
    ConfigurationBO findConfigurationProperty(String propertyName) throws TechnicalException;

    /**
     * This method finds a list configuration properties that contains a specific word
     *
     * @param parameter
     * @return
     * @throws TechnicalException
     */
    List<ConfigurationBO> findConfigurationProperties(String parameter) throws TechnicalException;


    String getProperty(SMLPropertyEnum key);


    <T extends Object> T getPropertyValue(SMLPropertyEnum key);

    Calendar getLastUpdate();

    void refreshProperties();

    void forceRefreshProperties();

    ConfigurationBO setPropertyToDatabase(SMLPropertyEnum key, String value, String description) throws TechnicalException;

    Optional<ConfigurationBO> getPropertyFromDatabase(SMLPropertyEnum key) throws TechnicalException;

    Optional<ConfigurationBO> deletePropertyFromDatabase(SMLPropertyEnum key) throws TechnicalException;

    void contextRefreshedEvent();

    void contextStopEvent();
}
