/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2017 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao;

/**
 * In this class we define the query and parameter names that can be used in the application.
 * <p/>
 *
 * @author Thomas Dussart
 * @since 13/05/2024
 */

public class QueryNames {
    public static final String MIGRATE_ENTITY_GET_BY_PARTICIPANT_AND_MIGRATION_STATUS = "MigrateEntity.getByParticipantAndMigrationStatus";
    public static final String MIGRATE_ENTITY_GET_RECORD = "MigrateEntity.getRecord";
    public static final String MIGRATE_ENTITY_GET_BY_PARTICIPANT_AND_MIGRATION_KEY = "MigrateEntity.getByParticipantAndMigrationKey";
    public static final String MIGRATE_ENTITY_GET_BY_OLD_SMP_IDENTIFIER_AND_MIGRATION_KEY = "MigrateEntity.getByOldSMPIdentifierAndMigrationKey";
    public static final String MIGRATE_ENTITY_GET_BY_OLD_SMP_AND_PARTICIPANT_AND_MIGRATION_STATUS = "MigrateEntity.getByOldSMPAndParticipantAndMigrationStatus";
    public static final String MIGRATE_ENTITY_GET_BY_OLD_SMP_IDENTIFIER_AND_MIGRATION_STATUS = "MigrateEntity.getByOldSMPIdentifierAndMigrationStatus";
    public static final String PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_IDENTIFIER = "ParticipantIdentifierEntity.getByIdentifier";
    public static final String PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_CASE_SENSITIVE_IDENTIFIER = "ParticipantIdentifierEntity.getByCaseSensitiveIdentifier";
    public static final String PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_IDENTIFIER_AND_SMP = "ParticipantIdentifierEntity.getByIdentifierAndSMP";
    public static final String PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_CASE_SENSITIVE_IDENTIFIER_AND_SMP = "ParticipantIdentifierEntity.getByCaseSensitiveIdentifierAndSMP";
    public static final String PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_SMP_IDENTIFIER = "ParticipantIdentifierEntity.getBySMPIdentifier";
    public static final String SMP_ENTITY_GET_BY_SMP_ID = "SmpEntity.getBySMPId";
    public static final String PARTICIPANT_IDENTIFIER_ENTITY_GET_ALL = "ParticipantIdentifierEntity.getAll";
    public static final String PARTICIPANT_IDENTIFIER_ENTITY_FIND_BY_IDENTIFIER_LIST_AND_SMP = "ParticipantIdentifierEntity.findByIdentifierListAndSMP";
    public static final String PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_IDENTIFIER_AND_DOMAIN = "ParticipantIdentifierEntity.getByIdentifierAndDomain";
    public static final String PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_CASE_SENSITIVE_IDENTIFIER_AND_DOMAIN = "ParticipantIdentifierEntity.getByCaseSensitiveIdentifierAndDomain";
    public static final String PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_IDENTIFIER_AND_DOMAIN_AND_NOT_FOR_SMP = "ParticipantIdentifierEntity.getByIdentifierAndDomainAndNotForSMP";
    public static final String PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_NULL_HASH = "ParticipantIdentifierEntity.getByNullHash";
    public static final String PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_SMP_IDENTIFIER_AND_DISABLED = "ParticipantIdentifierEntity.getBySMPIdentifierAndDisabled";
    public static final String INCONSISTENCY_DB_CNAME_ENTITY_GET_ALL_DNS_CNAME_RECORDS = "InconsistencyDbCNameEntity.getAllDNSCNameRecords";
    public static final String INCONSISTENCY_DB_NAPTR_ENTITY_GET_ALL_DNS_NAPTR_RECORDS = "InconsistencyDbNaptrEntity.getAllDNSNaptrRecords";
    public static final String PARTICIPANT_IDENTIFIER_ENTITY_GET_PARTICIPANT_COUNT = "ParticipantIdentifierEntity.getParticipantCount";
    public static final String PARTICIPANT_IDENTIFIER_ENTITY_GET_PARTICIPANT_COUNT_FOR_SMP = "ParticipantIdentifierEntity.getParticipantCountForSMP";
    public static final String PARTICIPANT_IDENTIFIER_ENTITY_GET_PARTICIPANT_COUNT_FOR_SMP_AND_STATUS = "ParticipantIdentifierEntity.getParticipantCountForSMPAndStatus";
    public static final String PARTICIPANT_IDENTIFIER_ENTITY_GET_PARTICIPANT_COUNT_FOR_SUB_DOMAIN = "ParticipantIdentifierEntity.getParticipantCountForSubDomain";
    public static final String PARTICIPANT_IDENTIFIER_ENTITY_GET_PARTICIPANT_COUNT_DOMAIN_ID = "ParticipantIdentifierEntity.getParticipantCountDomainId";
    public static final String PARTICIPANT_IDENTIFIER_ENTITY_GET_CASE_SENSITIVE_PARTICIPANT_COUNT_DOMAIN_ID = "ParticipantIdentifierEntity.getCaseSensitiveParticipantCountDomainId";
    public static final String CONFIGURATION_ENTITY_GET_BY_PROPERTY_NAME = "ConfigurationEntity.getByPropertyName";
    public static final String CONFIGURATION_ENTITY_FIND_BY_PROPERTY_NAME = "ConfigurationEntity.findByPropertyName";
    public static final String CONFIGURATION_ENTITY_MAX_UPDATE_DATE = "ConfigurationEntity.maxUpdateDate";
    public static final String CONFIGURATION_ENTITY_GET_ALL = "ConfigurationEntity.getAll";
    public static final String SMP_ENTITY_GET_ALL = "SmpEntity.getAll";
    public static final String SMP_ENTITY_GET_BY_CERTIFICATE_ID = "SmpEntity.getByCertificateId";
    public static final String CERTIFICATE_ENTITY_GET_BY_ID = "CertificateEntity.getById";
    public static final String CERTIFICATE_ENTITY_GET_BY_CERTIFICATE_ID = "CertificateEntity.getByCertificateId";
    public static final String CERTIFICATE_ENTITY_LIST_CERTIFICATES_CHANGE_TO = "CertificateEntity.listCertificatesChangeTo";

    public static final String ALLOWED_WILDCARD_ENTITY_GET_ALL_BY_CERTIFICATE_ID = "AllowedWildcardEntity.getAllByCertificateId";
    public static final String CERTIFICATE_DOMAIN_ENTITY_GET_ALL = "CertificateDomainEntity.getAll";
    public static final String CERTIFICATE_DOMAIN_ENTITY_GET_BY_CERTIFICATE_ID = "CertificateDomainEntity.getByCertificateId";
    public static final String CERTIFICATE_DOMAIN_ENTITY_GET_BY_CERTIFICATE_ID_AND_SUB_DOMAIN_ID = "CertificateDomainEntity.getByCertificateIdAndSubDomainId";
    public static final String CERTIFICATE_DOMAIN_ENTITY_GET_BY_CERTIFICATE_ALIAS_AND_IS_ROOT = "CertificateDomainEntity.getByCertificateAliasAndIsRoot";
    public static final String CERTIFICATE_DOMAIN_ENTITY_LIST_BY_CERTIFICATE_ID = "CertificateDomainEntity.listByCertificateId";
    public static final String CERTIFICATE_DOMAIN_ENTITY_LIST_BY_CERTIFICATE_ID_AND_SUB_DOMAIN = "CertificateDomainEntity.listByCertificateIdAndSubDomain";
    public static final String CERTIFICATE_DOMAIN_ENTITY_LIST_BY_SUB_DOMAIN = "CertificateDomainEntity.listBySubDomain";

    public static final String DNS_RECORD_ENTITY_GET_ALL = "DNSRecordEntity.getAll";
    public static final String DNS_RECORD_ENTITY_GET_BY_NAME_AND_ZONE = "DNSRecordEntity.getByNameAndZone";
    public static final String DNS_RECORD_ENTITY_GET_BY_NAME_AND_ZONE_AND_TYPE_AND_VALUE = "DNSRecordEntity.getByNameAndZoneAndTypeAndValue";

    public static final String SUBDOMAIN_ENTITY_GET_ALL = "SubdomainEntity.getAll";
    public static final String SUBDOMAIN_ENTITY_GET_BY_ID = "SubdomainEntity.getById";
    public static final String SUBDOMAIN_ENTITY_GET_BY_NAME = "SubdomainEntity.getByName";
    public static final String SUBDOMAIN_ENTITY_GET_ALL_SCHEMES_PER_DOMAIN = "SubdomainEntity.getAllSchemesPerDomain";
    public static final String SUBDOMAIN_ENTITY_GET_BY_CERTIFICATE_ID = "SubdomainEntity.getByCertificateId";

    public static final String SCHEME = "scheme";
    public static final String PARTICIPANT_ID = "participantId";
    public static final String MIGRATED = "migrated";
    public static final String MIGRATION_KEY = "migrationKey";
    public static final String OLD_SMP_ID = "oldSmpId";
    public static final String NEW_SMP_ID = "newSmpId";
    public static final String SMP_ID = "smpId";
    public static final String PARTICIPANT_IDS = "participantIds";
    public static final String DISABLED = "disabled";
    public static final String DNS_ALL_TYPE = "dnsAllType";
    public static final String DNS_NAPTR_TYPE = "dnsNaptrType";
    public static final String CREATE_DATE = "createDate";
    public static final String DNS_CNAME_TYPE = "dnsCnameType";
    public static final String PARTICIPANT_ID_LIST = "participantIdList";
    public static final String SUBDOMAIN_ID = "subdomainId";
    public static final String CERTIFICATE_ID = "certificateId";
    public static final String PROPERTY_NAME = "propertyName";
    public static final String RESPONSE_SIGNING = "response signing";

    public static final String NAME = "name";
    public static final String DNS_ZONE = "dnsZone";
    public static final String DNS_TYPE = "dnsType";
    public static final String DNS_VALUE = "dnsValue";
}
