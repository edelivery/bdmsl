/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.dns;

import eu.europa.ec.bdmsl.common.bo.*;
import eu.europa.ec.bdmsl.common.enums.DNSRecordEnum;

import eu.europa.ec.bdmsl.common.enums.DNSSubSomainRecordTypeEnum;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.service.dns.impl.DnsClientServiceImpl;
import eu.europa.ec.bdmsl.service.dns.impl.DnsZone;
import eu.europa.ec.bdmsl.service.dns.impl.SupportedDnsRecordType;
import org.xbill.DNS.IRDnsRangeData;
import org.xbill.DNS.Name;
import org.xbill.DNS.Record;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Adrien FERIAL
 * @since 12/06/2015
 */
public interface IDnsClientService {


    void createDNSRecordsForSMP(ServiceMetadataPublisherBO smpBO) throws TechnicalException;

    List<Record> getAllRecords(String dnsZone) throws TechnicalException;

    int getDNSDataForInconsistencyReport(String publisherName, String dnsZone, IRDnsRangeData smpData, IRDnsRangeData cnameData, IRDnsRangeData naptrData) throws TechnicalException;

    int getAllDNSDataForInconsistencyReport(String publisherName, IRDnsRangeData smpData, IRDnsRangeData cnameData, IRDnsRangeData naptrData) throws TechnicalException;


    Map<String, Collection<Record>> getAllParticipantRecords(String participant, String scheme) throws TechnicalException;

    List<Record> lookup(String domainName, int type) throws TechnicalException;

    List<Record> lookupAny(String domainName) throws TechnicalException;


    /**
     * Delete the DNS record of a SMP
     *
     * @param smpBO the SMP
     * @throws TechnicalException a technical exception
     */
    void deleteDNSRecordsForSMP(ServiceMetadataPublisherBO smpBO) throws TechnicalException;

    void updateSMPDNSRecord(ServiceMetadataPublisherBO smpBO) throws TechnicalException;

    void synchronizeDNSRecordsForSMP(ServiceMetadataPublisherBO smpBO, List<ParticipantBO> participants) throws TechnicalException;

    void updateDNSRecordsForSMP(ServiceMetadataPublisherBO smpBO, List<ParticipantBO> participants, boolean isLogicalAddressToBeUpdated) throws TechnicalException;

    void createDNSRecordsForParticipant(ParticipantBO participantBO, ServiceMetadataPublisherBO smpBO) throws TechnicalException;

    void deleteDNSRecordsForParticipant(ParticipantBO participantBO, ServiceMetadataPublisherBO smpBO) throws TechnicalException;

    void updateDNSRecordsForParticipant(ParticipantBO participantBO, ServiceMetadataPublisherBO smpBO) throws TechnicalException;

    void deleteDNSRecordsForParticipants(List<ParticipantBO> participantBOList , ServiceMetadataPublisherBO smpBO) throws TechnicalException;

    void createDNSRecordsForParticipants(List<ParticipantBO> participantBOList, ServiceMetadataPublisherBO smpBO, SupportedDnsRecordType... dnsTypesToBeCreated) throws TechnicalException;

    DnsZone getDnsZoneName(CertificateDomainBO certificateDomainBO) throws TechnicalException;

    DnsZone getDnsZoneNameForSubDomain(SubdomainBO subdomainBO) throws TechnicalException;

    DnsZone getDnsZoneName() throws TechnicalException;

    boolean isParticipantAlreadyCreated(ParticipantBO participantBO, DNSSubSomainRecordTypeEnum recordType);

    /**
     * Verifies if it's possible to access to DNS for writing, lookup and deleting records
     *
     * @throws TechnicalException a technical exception
     */
    void verifyDNSAccess() throws TechnicalException;

    /**
     * Gets DNS Zone used for IsAlive task
     *
     * @return DNS Zone
     * @throws TechnicalException a technical exception, if not possible to get DNS Zone
     */
    DnsZone getDnsZoneNameIsAlive() throws TechnicalException;

    /**
     * Creates CNAME and NAPTR entries on DNS for participantBO
     *
     * @param participantBO Participant to be created
     * @return Pair with CNAME and NAPTR created on DNS
     * @throws TechnicalException a technical exception, if not created
     */
    DnsClientServiceImpl.ParticipantDnsRecord createEntriesIsAliveDNS(ParticipantBO participantBO, String smpLogicalAddress) throws TechnicalException;

    /**
     * Deletes participantBO entries from DNS
     *
     * @param participantBO Participant to be deleted
     * @throws TechnicalException a technical exception, if not deleted
     */
    void deleteDNSRecordsForParticipantIsAlive(ParticipantBO participantBO) throws TechnicalException;


    void createCustomDNSRecord(DNSRecordBO dnsRecordBO, DNSRecordEnum dnsRecord) throws TechnicalException;

    void deleteCustomDNSRecord(Name name, Name dnsZone) throws TechnicalException;

}
