/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2017 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.conversion;

import ec.services.wsdl.bdmsl.admin.data._1.SubDomainType;
import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import static org.apache.commons.lang3.StringUtils.trim;

/**
 * This converter class transforms SubDomainType objects into SubdomainBO objects.
 * <p/>
 * Note: WS Type to BO is converted manually and strings are trimmed!
 *
 * @author Thomas Dussart
 * @since 24/05/2024
 */
@Component
public class SubDomainTypeToSubdomainBOConverter extends AutoRegisteringConverter<SubDomainType, SubdomainBO> {

    public SubDomainTypeToSubdomainBOConverter(ConversionService conversionService) {
        super(conversionService);
    }

    @Override
    public SubdomainBO convert(SubDomainType source) {
        if (source == null) {
            return null;
        }

        SubdomainBO target = new SubdomainBO();

        target.setSubdomainName(trim(source.getSubDomainName()));
        target.setDescription(trim(source.getSubDomainDescription()));
        target.setDnsZone(trim(source.getDNSZone()));
        target.setParticipantIdRegexp(trim(source.getParticipantRegularExpression()));
        target.setDnsRecordTypes(trim(source.getDNSRecordType()));
        target.setSmpUrlSchemas(trim(source.getSmpUrlScheme()));
        target.setSmpCertSubjectRegex(trim(source.getCertSubjectRegularExpression()));
        target.setSmpCertPolicyOIDs(trim(source.getCertPolicyOIDs()));
        target.setMaxParticipantCountForDomain(source.getMaxParticipantCountForDomain());
        target.setMaxParticipantCountForSMP(source.getMaxParticipantCountForSMP());

        return target;
    }
}
