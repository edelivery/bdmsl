/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.bo;

import eu.europa.ec.bdmsl.common.exception.GenericTechnicalException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * @author Adrien FERIAL
 * @since 12/06/2015
 */
public class ServiceMetadataPublisherBO extends AbstractBusinessObject {

    private String smpId;
    private String certificateId;
    private String physicalAddress;
    private String logicalAddress;
    private SubdomainBO subdomain;
    private String dnsRecord;
    private boolean disabled;

    public ServiceMetadataPublisherBO() {
        this(null, null, null, null, null, null, false);
    }

    public ServiceMetadataPublisherBO(String smpId,
                                      String certificateId,
                                      String physicalAddress,
                                      String logicalAddress,
                                      SubdomainBO subdomain,
                                      String dnsRecord,
                                      boolean disabled) {
        this.smpId = smpId;
        this.certificateId = certificateId;
        this.physicalAddress = physicalAddress;
        this.logicalAddress = logicalAddress;
        this.subdomain = subdomain;
        this.dnsRecord = dnsRecord;
        this.disabled = disabled;
    }

    public String getSmpId() {
        return smpId;
    }

    public void setSmpId(String smpId) {
        this.smpId = smpId;
    }

    public String getCertificateId() {
        return certificateId;
    }

    public void setCertificateId(String certificateId) {
        this.certificateId = certificateId;
    }

    public String getPhysicalAddress() {
        return physicalAddress;
    }

    public void setPhysicalAddress(String physicalAddress) {
        this.physicalAddress = physicalAddress;
    }

    public String getLogicalAddress() {
        return logicalAddress;
    }

    public void setLogicalAddress(String logicalAddress) {
        if (!StringUtils.isEmpty(logicalAddress)) {
            this.logicalAddress = logicalAddress.trim();
        }
    }

    public SubdomainBO getSubdomain() {
        return subdomain;
    }

    public void setSubdomain(SubdomainBO subdomain) {
        this.subdomain = subdomain;
    }

    public String getDnsRecord() {
        return this.dnsRecord;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public void createDNSRecord(String publisherPrefix)
            throws GenericTechnicalException {
        if ((getSubdomain() == null) || (StringUtils.isEmpty(getSubdomain().getSubdomainName()))) {
            throw new GenericTechnicalException("Subdomain is null for SMP " + getSmpId());
        }
        if (StringUtils.isEmpty(publisherPrefix)) {
            throw new GenericTechnicalException("Publisher Prefix is null.");
        }
        this.dnsRecord = (getSmpId() + "." + publisherPrefix + "." + getSubdomain().getSubdomainName() + ".");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ServiceMetadataPublisherBO that = (ServiceMetadataPublisherBO) o;

        return new EqualsBuilder()
                .append(smpId, that.getSmpId())
                .append(certificateId, that.getCertificateId())
                .append(physicalAddress, that.getPhysicalAddress())
                .append(logicalAddress, that.getLogicalAddress())
                .append(subdomain, that.getSubdomain())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(smpId)
                .append(certificateId)
                .append(physicalAddress)
                .append(logicalAddress)
                .append(subdomain)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("smpId", smpId)
                .append("certificateId", certificateId)
                .append("physicalAddress", physicalAddress)
                .append("logicalAddress", logicalAddress)
                .append("subdomain", subdomain)
                .toString();
    }
}
