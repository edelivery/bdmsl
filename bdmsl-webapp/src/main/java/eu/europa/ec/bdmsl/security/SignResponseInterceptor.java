/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.security;

import eu.europa.ec.bdmsl.business.IConfigurationBusiness;
import eu.europa.ec.bdmsl.common.exception.BadConfigurationException;
import eu.europa.ec.bdmsl.common.exception.KeyException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.common.logging.ILoggingService;
import eu.europa.ec.bdmsl.common.util.KeyUtil;
import eu.europa.ec.bdmsl.common.util.KeyUtils;
import eu.europa.ec.bdmsl.util.CertificateUtils;
import eu.europa.ec.bdmsl.util.ExceptionUtils;
import eu.europa.ec.bdmsl.ws.soap.InternalErrorFault;
import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wss4j.common.ConfigurationConstants;
import org.apache.wss4j.common.crypto.Merlin;
import org.apache.wss4j.common.ext.WSPasswordCallback;
import org.apache.xml.security.signature.XMLSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.xml.crypto.dsig.DigestMethod;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Key;
import java.security.KeyStore;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

/**
 * @author Adrien FERIAL
 * @author Sebastian-Ion TINCU
 * @since 10/07/2015
 */
@Component(value = "signResponseInterceptor")
public class SignResponseInterceptor extends AbstractSoapInterceptor {

    public static final String SEC_MERLIN_KEYSTORE_ALIAS = Merlin.PREFIX + Merlin.KEYSTORE_ALIAS;
    public static final String SEC_MERLIN_KEYSTORE_FILE = Merlin.PREFIX + Merlin.KEYSTORE_FILE;
    public static final String SEC_MERLIN_KEYSTORE_PASS = Merlin.PREFIX + Merlin.KEYSTORE_PASSWORD;
    public static final String SEC_MERLIN_KEYSTORE_TYPE = Merlin.PREFIX + Merlin.KEYSTORE_TYPE;
    public static final String SEC_PROVIDER_MERLIN = "org.apache.wss4j.common.crypto.Merlin";
    public static final String SEC_PROVIDER = "org.apache.wss4j.crypto.provider";

    private IConfigurationBusiness configurationBusiness;
    private ILoggingService loggingService;

    public SignResponseInterceptor(ILoggingService loggingService, IConfigurationBusiness configurationBusiness) {
        super("pre-protocol");
        this.loggingService = loggingService;
        this.configurationBusiness = configurationBusiness;
    }

    public Map<String, Object> initializeSignatureProperties() throws TechnicalException {
        if (!configurationBusiness.isSignResponseEnabled()) {
            return null;
        }
        try {
            Path keystorePath = Paths.get(configurationBusiness.getConfigurationFolder(), configurationBusiness.getKeystoreFilename());
            String sigPropName = "SigProp-" + UUID.randomUUID();
            String keystoreType = configurationBusiness.getKeystoreType();
            String keyAlias = configurationBusiness.getSignAlias();
            String sigAlgo = configurationBusiness.getSignResponseAlgorithm();
            String sigDigestAlgo = configurationBusiness.getSignResponseDigestAlgorithm();
            final String dataHolder = retrievePassword();

            KeyStore keystore = CertificateUtils.getKeystore(keystoreType, keystorePath.toFile(), dataHolder);
            X509Certificate signingCertificate = CertificateUtils.getCertificate(keystore, keyAlias);
            Key signingKey = CertificateUtils.getSigningKey(keystore, dataHolder, keyAlias);
            loggingService.debug("Alias key [" + keyAlias + "] exists in [" + keystorePath.toFile().getAbsolutePath() + "] and it is accessible!");

            Map<String, Object> props = new HashMap<>();
            props.put(ConfigurationConstants.ACTION, ConfigurationConstants.SIGNATURE);
            props.put(ConfigurationConstants.SIGNATURE_USER, keyAlias);
            props.put(ConfigurationConstants.SIGNATURE_PARTS, "{Element}{}Body");
            props.put(ConfigurationConstants.SIG_ALGO, getSignatureAlgorithmForKey(signingCertificate.getPublicKey(), sigAlgo));
            props.put(ConfigurationConstants.SIG_DIGEST_ALGO, getSignatureDigestAlgorithmForKey(signingKey, sigDigestAlgo));

            props.put(ConfigurationConstants.PW_CALLBACK_REF, new SMLCallbackHandler(dataHolder));
            props.put(ConfigurationConstants.SIG_PROP_REF_ID, sigPropName);
            props.put(ConfigurationConstants.MUST_UNDERSTAND, "false");

            Properties sigProps = new Properties();
            sigProps.setProperty(SEC_PROVIDER, SEC_PROVIDER_MERLIN);
            sigProps.setProperty(SEC_MERLIN_KEYSTORE_TYPE, keystoreType);
            sigProps.setProperty(SEC_MERLIN_KEYSTORE_PASS, dataHolder);
            sigProps.setProperty(SEC_MERLIN_KEYSTORE_ALIAS, keyAlias);
            sigProps.setProperty(SEC_MERLIN_KEYSTORE_FILE, keystorePath.toFile().getAbsolutePath());

            props.put(sigPropName, sigProps);

            props.forEach((key, value) -> {
                // do not print properties - do it after...
                // only sigProps are expected
                if (!(value instanceof Properties)) {
                    loggingService.debug("\t[" + key + "] --> [" + (key.contains("passw") ? "*******" : value) + "]");
                }
            });

            sigProps.forEach((key, value) -> {
                loggingService.debug("\t\t[" + key + "] --> [" + (((String) key).contains("passw") ? "*******" : value) + "]");
            });

            return props;
        } catch (TechnicalException texc) {
            throw texc;
        } catch (Exception exc) {
            throw new BadConfigurationException(exc.getMessage(), exc);
        }

    }

    protected String getSignatureAlgorithmForKey(PublicKey key, String algorithm) {
        if (StringUtils.isNotBlank(algorithm)) {
            return algorithm;
        }

        String keyAlgorithm = KeyUtils.getAlgorithmIdFromPublicKey(key);


        switch (keyAlgorithm) {
            case "1.3.101.112": // fall through
            case "ed25519":
                return XMLSignature.ALGO_ID_SIGNATURE_EDDSA_ED25519;
            case "1.3.101.113": // fall through
            case "ed448":
                return XMLSignature.ALGO_ID_SIGNATURE_EDDSA_ED448;
            case "ec":
            case "1.2.840.10045.2.1": // fall through
                return XMLSignature.ALGO_ID_SIGNATURE_ECDSA_SHA256;
            default:
                return XMLSignature.ALGO_ID_SIGNATURE_RSA_SHA256;
        }

    }

    protected String getSignatureDigestAlgorithmForKey(Key key, String digestAlgorithm) {
        if (StringUtils.isNotBlank(digestAlgorithm)) {
            return digestAlgorithm;
        }

        return DigestMethod.SHA256;
    }

    @Override
    public void handleMessage(SoapMessage mc) throws Fault {
        if (configurationBusiness.isSignResponseEnabled()) {
            try {
                Map<String, Object> properties = this.initializeSignatureProperties();
                WSS4JOutInterceptor wss4JOutInterceptor = new WSS4JOutInterceptor(properties);
                wss4JOutInterceptor.handleMessage(mc);

            } catch (Fault | TechnicalException uexc) {
                String sessionId = RequestContextHolder.currentRequestAttributes().getSessionId();
                loggingService.error(uexc.getMessage() + " [" + sessionId + "].", uexc);
                InternalErrorFault internalError = ExceptionUtils.buildInternalErrorFault(uexc.getMessage());
                throw new Fault(internalError);
            }
        }
    }

    protected String retrievePassword() throws KeyException {
        try {
            Path location = Paths.get(configurationBusiness.getConfigurationFolder(), configurationBusiness.getEncryptionFilename());
            return KeyUtil.decryptToString(location.toAbsolutePath().toString(), configurationBusiness.getKeystorePassword());
        } catch (KeyException kexc) {
            throw kexc;
        } catch (Exception exc) {
            throw new KeyException(exc.getMessage(), exc);
        }
    }
}

class SMLCallbackHandler implements CallbackHandler {
    private static final Logger LOG = LogManager.getLogger(SMLCallbackHandler.class);

    String plainTextPassword;

    public SMLCallbackHandler(String plainTextPassword) {
        this.plainTextPassword = plainTextPassword;
    }

    @Override
    public void handle(Callback[] callbacks) {
        for (Callback cb : callbacks) {
            if (cb instanceof WSPasswordCallback) {
                WSPasswordCallback pc = (WSPasswordCallback) cb;

                LOG.debug("Request for password: [" + pc.getIdentifier() + "], usage: [" + pc.getUsage() + "]!");
                pc.setPassword(plainTextPassword);
            }
        }
    }
}
