package eu.europa.ec.bdmsl.security;

/*
 * (C) Copyright 2024 - European Commission | CEF eDelivery
 * <p>
 * Licensed under the EUPL, Version 1.2 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * EUPL.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12, https://joinup.ec.europa.eu/sites/default/files/custom-page/attachment/eupl_v1.2_en.pdf
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import eu.europa.ec.bdmsl.common.exception.SMLRuntimeException;
import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.helpers.IOUtils;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.apache.cxf.transport.servlet.ServletController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.servlet.ServletConfig;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Custom CXF servlet for BDMSL that overrides the init parameters to control the visibility of the service list page.
 * This servlet reads configuration properties to determine whether to hide the service list page.
 * <p/>
 * Logs are provided to trace the changes in the configuration.
 *
 * @author Thomas Dussart
 * @since 2024
 */
public class BdsmlCxfServlet extends CXFServlet {

    private static final Logger LOG = LoggerFactory.getLogger(BdsmlCxfServlet.class);

    @Override
    protected ServletController createServletController(ServletConfig servletConfig) {
        LOG.info("Overriding createServletController method to override init params.");
        return super.createServletController(new ServletConfigDelegate(servletConfig));
    }

    protected InputStream getResourceAsStream(String path) {
        InputStream resource = super.getResourceAsStream(path);
        if (!StringUtils.equalsIgnoreCase(path, "/index.html")) {
            LOG.debug("Serving non-static file from path [{}]", path);
            return resource;
        }
        String contextPath = getServletContext().getContextPath();
        LOG.debug("Serving static file using context path [{}]", contextPath);
        String staticFileContent;
        try {
            staticFileContent = IOUtils.readStringFromStream(resource);
            staticFileContent = staticFileContent.replace("/edelivery-sml", contextPath);
            return new ByteArrayInputStream(staticFileContent.getBytes());
        } catch (IOException e) {
            throw new SMLRuntimeException("Cannot read the string content of the /index.html file", e);
        }
    }

}
