/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.business.impl;

import eu.europa.ec.bdmsl.business.IConfigurationBusiness;
import eu.europa.ec.bdmsl.business.IIdentifierFormatterBusiness;
import eu.europa.ec.bdmsl.business.IManageParticipantIdentifierBusiness;
import eu.europa.ec.bdmsl.business.IManageServiceMetadataBusiness;
import eu.europa.ec.bdmsl.common.bo.*;
import eu.europa.ec.bdmsl.common.business.AbstractBusinessImpl;
import eu.europa.ec.bdmsl.common.enums.DNSSubSomainRecordTypeEnum;
import eu.europa.ec.bdmsl.common.exception.*;
import eu.europa.ec.bdmsl.common.identifier.ParticipantIdentifier;
import eu.europa.ec.bdmsl.dao.ICertificateDomainDAO;
import eu.europa.ec.bdmsl.dao.IMigrationDAO;
import eu.europa.ec.bdmsl.dao.IParticipantDAO;
import eu.europa.ec.bdmsl.security.CertificateDetails;
import eu.europa.ec.bdmsl.service.dns.IDnsClientService;
import eu.europa.ec.bdmsl.service.dns.impl.SupportedDnsRecordType;
import eu.europa.ec.bdmsl.util.ConstraintsUtil;
import eu.europa.ec.dynamicdiscovery.exception.MalformedIdentifierException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static eu.europa.ec.bdmsl.common.enums.DNSSubSomainRecordTypeEnum.CNAME;
import static eu.europa.ec.bdmsl.common.enums.DNSSubSomainRecordTypeEnum.NAPTR;


/**
 * @author Joze RIHTARSIC
 * @since 4.3
 */
@Component
public class ManageParticipantIdentifierBusinessImpl extends AbstractBusinessImpl implements IManageParticipantIdentifierBusiness {

    /* we delete by batch of 300 participants to avoid an exception in the DNS because the maximum size of a DNS packet is 65k.
     * If the number of participants is big, then the size of 65k is exceeded and the update is refused */
    public final static int MAX_PARTICIPANTS_COUNT_IN_BATCH = 300;

    private static final Logger LOG = LoggerFactory.getLogger(IdentifierFormatterBusinessImpl.class);
    private final IMigrationDAO migrationDAO;
    private final IParticipantDAO participantDAO;
    private final ICertificateDomainDAO certificateDomainDAO;
    private final IConfigurationBusiness configurationBusiness;
    private final IManageServiceMetadataBusiness manageServiceMetadataBusiness;
    private final IIdentifierFormatterBusiness participantIdentifierFormatter;
    private final IDnsClientService dnsClientService;

    public ManageParticipantIdentifierBusinessImpl(IConfigurationBusiness configurationBusiness,
                                                   IMigrationDAO migrationDAO,
                                                   IParticipantDAO participantDAO,
                                                   ICertificateDomainDAO certificateDomainDAO,
                                                   IManageServiceMetadataBusiness manageServiceMetadataBusiness,
                                                   IIdentifierFormatterBusiness participantIdentifierFormatter,
                                                   IDnsClientService dnsClientService) {
        this.configurationBusiness = configurationBusiness;
        this.migrationDAO = migrationDAO;
        this.participantDAO = participantDAO;
        this.certificateDomainDAO = certificateDomainDAO;
        this.manageServiceMetadataBusiness = manageServiceMetadataBusiness;
        this.participantIdentifierFormatter = participantIdentifierFormatter;
        this.dnsClientService = dnsClientService;
    }

    @Override
    public long getParticipantCountForSMP(ServiceMetadataPublisherBO smpBO) {
        return participantDAO.getParticipantCountForSMP(smpBO.getSmpId());
    }

    @Override
    public long getParticipantCountForSMPAndStatus(ServiceMetadataPublisherBO smpBO, boolean disabled) {
        return participantDAO.getParticipantCountForSMPAndStatus(smpBO.getSmpId(), disabled);
    }

    @Override
    public long getParticipantCountForSMPDomain(ServiceMetadataPublisherBO smpBO) {
        return participantDAO.getParticipantCountForSubDomain(smpBO.getSubdomain().getSubdomainId());
    }

    @Override
    public ParticipantIdentifier normalizeAndValidateParticipant(ParticipantBO participantBO) throws TechnicalException {
        LOG.debug("normalizeAndValidateParticipant: [{}]", participantBO);
        CertificateDomainBO certificateDomainBO = certificateDomainDAO.findDomain(((CertificateDetails) SecurityContextHolder.getContext().getAuthentication().getDetails()));
        ParticipantIdentifier identifier;
        try {
            identifier = participantIdentifierFormatter.normalizeForDomain(participantBO, certificateDomainBO.getSubdomain());
        } catch (MalformedIdentifierException e) {
            throw new BadRequestException(e.getMessage());
        }

        LOG.info("normalizeAndValidateParticipant: [{}] - Normalized participant [{}]", participantBO, identifier);
        String partyId = identifier.getIdentifier();
        String scheme = identifier.getScheme();

        identifierValidation(partyId);
        // update normalized value
        participantBO.setParticipantId(partyId);
        participantBO.setScheme(scheme);

        loggingService.debug("Participant [" + participantBO + "] is valid!");
        return identifier;

    }

    /**
     * Validate the participant identifier value is not null, not containing non ascii character and according to the
     * regular expression defined in the domain configuration if any.
     *
     * @param identifierValue the participant identifier value to validate
     * @throws TechnicalException if the participant identifier value is invalid.
     */
    protected void identifierValidation(String identifierValue) throws TechnicalException {

        if (!identifierValue.matches(ConstraintsUtil.PARTICIPANT_IDENTIFIER_ASCII_REGEX)) {
            throw new BadRequestException("Participant Identifier Value should only contain ASCII characters");
        }
    }

    @Override
    public void createParticipant(ParticipantBO participantBO) throws TechnicalException {
        participantDAO.createParticipant(participantBO);
    }

    /**
     * Method to validate the data and create/registers a list of participants to the database and DNS
     * @param publisherId the publisher id of the participants
     * @param participantBOs the list of participants to create
     * @throws TechnicalException if the data is invalid or an error occurs
     */

    @Override
    public void createParticipantList(String publisherId, List<ParticipantBO> participantBOs) throws TechnicalException {
        boolean isDNSEnabled = configurationBusiness.isDNSEnabled();
       validateParticipantBOList(participantBOs);

        manageServiceMetadataBusiness.validateSMPId(publisherId);
        ServiceMetadataPublisherBO existingSmpBO = manageServiceMetadataBusiness.verifySMPExist(publisherId);
        //
        // validate free quota for the SMP
        validateFreeQuotaForSMP(participantBOs.size(), existingSmpBO);

        // perform checks on every participant
        for (ParticipantBO participantBO : participantBOs) {
            // normalize and validate the input data
            normalizeAndValidateParticipant(participantBO);
            if (!StringUtils.equalsIgnoreCase(publisherId, participantBO.getSmpId())) {
                throw new BadRequestException("All participants in the list must have the same Publisher Id!");
            }
            validateParticipantExists(participantBO, existingSmpBO, isDNSEnabled);
        }

        for (ParticipantBO participantBO : participantBOs) {
            // The participants list is valid, we can now perform the insertions
            createParticipant(participantBO);
        }

        if (isDNSEnabled) {
            // If DNS create fails, then an exception is thrown and the database import is rolled back
            dnsClientService.createDNSRecordsForParticipants(participantBOs, existingSmpBO);
            // because create DNS record can take a while (up to a couple of minutes) in large domain other SMP can insert participant
            // at almost the same time. See the ticket: EDELIVERY-9270
            // before the commit double check that other participant does not exist yet in the database.
            for (ParticipantBO participantBO: participantBOs) {
                validateIfParticipantExistsInDatabase(participantBO, existingSmpBO);
            }
        }
    }

    @Override
    public void deleteParticipantList(String publisherId, List<ParticipantBO> participantBOs) throws TechnicalException {
        boolean isDNSEnabled = configurationBusiness.isDNSEnabled();
        validateParticipantBOList(participantBOs);

        manageServiceMetadataBusiness.validateSMPId(publisherId);
        ServiceMetadataPublisherBO existingSmpBO = manageServiceMetadataBusiness.verifySMPExist(publisherId);

        // perform checks on every participant
        for (ParticipantBO participantBO : participantBOs) {
            // normalize and validate the input data
            normalizeAndValidateParticipant(participantBO);
            if (!StringUtils.equalsIgnoreCase(publisherId, participantBO.getSmpId())) {
                throw new BadRequestException("All participants in the list must have the same Publisher Id!");
            }
        }

        // Check if the participants already exist
        List<ParticipantBO> existingParticipantBOList = findParticipants(existingSmpBO.getSmpId(), participantBOs);
        if (existingParticipantBOList == null || existingParticipantBOList.size() != participantBOs.size()) {
            throw new ParticipantNotFoundException("At least one of the participants doesn't exist in the list " + participantBOs);
        }

        for (ParticipantBO existingParticipantBO : existingParticipantBOList) {
            // Verify that the SMP owns the participant
            if (!existingParticipantBO.getSmpId().equalsIgnoreCase(publisherId)) {
                throw new UnauthorizedException("The SMP '" + publisherId + "' doesn't own the participant '" + existingParticipantBO.getParticipantId() + "'. This participant is linked to the SMP '" + existingParticipantBO.getSmpId() + "'");
            }
        }

        // Check that no migration is planned for any participant
        checkNoMigrationPlanned(publisherId, participantBOs);

        // delete participants from database
        Map<String, List<String>> mapParticipants = toMap(participantBOs);
        participantDAO.deleteParticipants(publisherId, mapParticipants);

        if (isDNSEnabled) {
            // If DNS deletion fails, then an exception is thrown and the database import is rolled back
            dnsClientService.deleteDNSRecordsForParticipants(participantBOs, existingSmpBO);
        }
    }

    /**
     * Validate if the participant exists in the database. If it exists, then recreate the DNS record with the committed participant data!
     * Because create DNS record can take a while (up to a couple of minutes) in large domain other SMP can insert participant
     * at almost the same time. See the ticket: EDELIVERY-9270
     * before the commit double check that other participant does not exist yet in the database.
     *
     * @param participantBO the participant to validate
     * @param existingSmpBO the SMP to validate
     * @throws TechnicalException if the participant already exists in the database
     */
    private void validateIfParticipantExistsInDatabase(ParticipantBO participantBO, ServiceMetadataPublisherBO existingSmpBO) throws TechnicalException {
        // because create DNS record can take a while (up to a couple of minutes) in large domain other SMP can insert participant
        // at almost the same time. See the ticket: EDELIVERY-9270
        // before the commit double check that other participant does not exist yet in the database.
        Optional<ParticipantBO> existingParticipantBOOption = getParticipantForDifferentSMPOnDomain(participantBO, existingSmpBO);
        if (existingParticipantBOOption.isPresent()) {
            // recreate the DNS record with the committed participant data!
            ParticipantBO existingParticipantBO = existingParticipantBOOption.get();
            loggingService.warn("Participant was 'just' created by the SMP :  " + existingParticipantBO.getParticipantId() + "! Recreate the DNS record and rollback the request!");
            dnsClientService.createDNSRecordsForParticipant(existingParticipantBO, existingSmpBO);
            throw new BadRequestException("The participant value: "+ participantBO.getParticipantId() + "' with scheme: '" + participantBO.getScheme() + "' already exist on subdomain: '" + existingSmpBO.getSubdomain().getSubdomainName() + "'");
        }
    }

    @Override
    public ParticipantBO findParticipant(ParticipantBO participantBO) throws TechnicalException {
        return participantDAO.findParticipant(participantBO);
    }

    @Override
    public Optional<ParticipantBO> getParticipantForDomain(ParticipantBO participantBO, SubdomainBO subdomainBO) throws TechnicalException {
        return participantDAO.getParticipantForDomain(participantBO, subdomainBO);
    }

    @Override
    public Optional<ParticipantBO> getParticipantForDifferentSMPOnDomain(ParticipantBO participantBO, ServiceMetadataPublisherBO existingSmpBO) throws TechnicalException {
        return participantDAO.getParticipantForDifferentSMPOnDomain(participantBO, existingSmpBO);
    }

    @Override
    public void validatePageRequest(PageRequestBO pageRequestBO) throws TechnicalException {
        manageServiceMetadataBusiness.validateSMPId(pageRequestBO.getSmpId());
        if (!StringUtils.isEmpty(pageRequestBO.getPage())) {
            try {
                int value = Integer.parseInt(pageRequestBO.getPage());
                if (value < 1) {
                    throw new BadRequestException("The NextPageIdentifier must be a positive integer");
                }
            } catch (final NumberFormatException exc) {
                throw new BadRequestException("The NextPageIdentifier must be a positive integer", exc);
            }
        }
        loggingService.debug("Page request " + pageRequestBO + " is valid");
    }

    @Override
    public ParticipantListBO list(PageRequestBO pageRequestBO) throws TechnicalException {
        int participantPerPageCount = ConstraintsUtil.DEFAULT_PARTICIPANT_PER_PAGE;
        participantPerPageCount = configurationBusiness.getListPageSize();
        ParticipantListBO participantListBO = null;
        final List<ParticipantBO> participantResultList = participantDAO.listParticipant(pageRequestBO, participantPerPageCount);
        if (participantResultList != null && !participantResultList.isEmpty()) {
            participantListBO = new ParticipantListBO();
            participantListBO.setParticipantBOList(participantResultList);

            // Is it the last page?
            int currentPage;
            if (!StringUtils.isEmpty(pageRequestBO.getPage())) {
                currentPage = Integer.parseInt(pageRequestBO.getPage());
            } else {
                currentPage = 1;
            }
            pageRequestBO.setPage(String.valueOf(currentPage + 1));
            List<ParticipantBO> nextParticipantResultList = participantDAO.listParticipant(pageRequestBO, participantPerPageCount + 1);
            if (nextParticipantResultList != null && !nextParticipantResultList.isEmpty()) {
                participantListBO.setNextPage(String.valueOf(currentPage + 1));
            } else {
                participantListBO.setNextPage(null);
            }
        }
        return participantListBO;
    }

    @Override
    public void validateMigrationRecord(MigrationRecordBO prepareToMigrateBO) throws TechnicalException {
        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setSmpId(prepareToMigrateBO.getOldSmpId());
        participantBO.setScheme(prepareToMigrateBO.getScheme());
        participantBO.setParticipantId(prepareToMigrateBO.getParticipantId());
        this.normalizeAndValidateParticipant(participantBO);
        prepareToMigrateBO.setParticipantId(participantBO.getParticipantId());
        prepareToMigrateBO.setScheme(participantBO.getScheme());

        if (!StringUtils.isEmpty(prepareToMigrateBO.getNewSmpId())) {
            manageServiceMetadataBusiness.validateSMPId(prepareToMigrateBO.getNewSmpId());
            participantBO.setSmpId(prepareToMigrateBO.getNewSmpId());
        }

        if (StringUtils.isEmpty(prepareToMigrateBO.getMigrationCode())) {
            throw new BadRequestException("The migration code can not be null or empty");
        }

        if (!prepareToMigrateBO.getMigrationCode().matches(ConstraintsUtil.MIGRATION_KEY_REGEX)) {
            throw new BadRequestException("The migration code [ " + prepareToMigrateBO.getMigrationCode() + " ] must contain a minimum of (8 characters,2 Upper Case letters,2 Lower Case letters,2 Special Characters among @#$%()[]{}*^-!~|+= and 2 numbers), " +
                    "a maximum of 24 characters and No White Spaces.");
        }
        loggingService.debug("Migration record " + prepareToMigrateBO + " is valid");
    }

    @Override
    public void prepareToMigrate(MigrationRecordBO prepareToMigrateBO) throws TechnicalException {
        // find if a migration record already exists
        MigrationRecordBO found = migrationDAO.findNonMigratedRecord(prepareToMigrateBO);

        // a migration record already exists, then update it
        if (found != null) {
            migrationDAO.updateMigrationRecord(prepareToMigrateBO);
        } else {
            found = migrationDAO.findMigrationRecordByCompositePrimaryKey(prepareToMigrateBO);
            if (found != null) {
                throw new BadRequestException(String.format("The migration key %s was already used to migrate participant (%s,%s).", prepareToMigrateBO.getMigrationCode(), prepareToMigrateBO.getScheme(), prepareToMigrateBO.getParticipantId()));
            }
            // create a new migration record
            migrationDAO.createMigrationRecord(prepareToMigrateBO);
        }
    }

    @Override
    public void performMigration(MigrationRecordBO migrateBO) throws TechnicalException {
        ParticipantBO participantBO = new ParticipantBO();
        participantBO.setSmpId(migrateBO.getNewSmpId());
        participantBO.setScheme(migrateBO.getScheme());
        participantBO.setParticipantId(migrateBO.getParticipantId());
        participantDAO.updateParticipant(participantBO, migrateBO.getOldSmpId());
        migrateBO.setMigrated(true);
        migrationDAO.performMigration(migrateBO);
    }

    @Override
    public MigrationRecordBO findNonMigratedRecord(MigrationRecordBO migrateBO) throws TechnicalException {
        return migrationDAO.findNonMigratedRecord(migrateBO);
    }

    @Override
    public ParticipantListBO list() throws TechnicalException {
        ParticipantListBO participantListBO = null;
        final List<ParticipantBO> participantResultList = participantDAO.getAllParticipants();
        if (participantResultList != null && !participantResultList.isEmpty()) {
            participantListBO = new ParticipantListBO();
            participantListBO.setParticipantBOList(participantResultList);
        }
        return participantListBO;
    }

    @Override
    public List<ParticipantBO> getAllParticipantsForSMP(ServiceMetadataPublisherBO smpBO) {
        return participantDAO.getAllParticipantsForSMP(smpBO);
    }

    @Override
    public List<ParticipantBO> getParticipantsForSMP(ServiceMetadataPublisherBO smpBO, int page, int pageSize) {
        return participantDAO.getParticipantsForSMP(smpBO, page, pageSize);
    }

    @Override
    public List<ParticipantBO> findParticipants(String smpId, List<ParticipantBO> participantBOList) throws TechnicalException {
        Map<String, List<String>> mapParticipants = toMap(participantBOList);
        return participantDAO.findParticipants(smpId, mapParticipants);
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<ParticipantBO> updateNextParticipantsBatchStatusForSMP(ServiceMetadataPublisherBO metadataPublisherBO, boolean disabled) throws TechnicalException {

        List<ParticipantBO> participantBOList = participantDAO.setDisabledNextSMPParticipantsBatch(metadataPublisherBO,
                MAX_PARTICIPANTS_COUNT_IN_BATCH, disabled);

        if (configurationBusiness.isDNSEnabled() && !participantBOList.isEmpty()) {
            // If DNS update fails, then an exception is thrown and database is rolled back for batch
            if (disabled) {
                dnsClientService.deleteDNSRecordsForParticipants(participantBOList, metadataPublisherBO);
            } else {
                dnsClientService.createDNSRecordsForParticipants(participantBOList, metadataPublisherBO);
            }
        }
        return participantBOList;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<ParticipantBO> deleteNextParticipantsBatchForSMP(ServiceMetadataPublisherBO metadataPublisherBO) throws TechnicalException {
        return participantDAO.deleteNextSMPParticipantsBatch(metadataPublisherBO, MAX_PARTICIPANTS_COUNT_IN_BATCH);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<ParticipantBO> updateNaptrRecordsForNextParticipantsBatchSMP(ServiceMetadataPublisherBO smpToUpdate, ServiceMetadataPublisherBO newSMP) throws TechnicalException {
        List<ParticipantBO> participantBOList = participantDAO.updateSMPForNextParticipantsBatchSMP(smpToUpdate, newSMP,
                MAX_PARTICIPANTS_COUNT_IN_BATCH);
        if (configurationBusiness.isDNSEnabled() && !participantBOList.isEmpty()) {
            // UPDATE NAPTR ONLY - LOGICAL ADDRESS
            dnsClientService.createDNSRecordsForParticipants(participantBOList, newSMP, SupportedDnsRecordType.NAPTR);

        }
        return participantBOList;
    }

    @Override
    public void validateParticipantBOList(ParticipantListBO participantListBO) throws TechnicalException {

        validateParticipantBOList(participantListBO.getParticipantBOList());

        if (participantListBO.getNextPage() != null) {
            throw new BadRequestException("The NextPageIdentifier must be null or empty");
        }
    }

    @Override
    public void validateParticipantBOList(List<ParticipantBO> participantListBO) throws TechnicalException {
        if (participantListBO == null || participantListBO.isEmpty()) {
            throw new BadRequestException(Messages.BAD_REQUEST_PARAMETER_EMPTY,  "List of participants");
        }

        if (new HashSet<>(participantListBO).size() != participantListBO.size()) {
            // all participant identifiers must be different
            throw new BadRequestException(Messages.BAD_REQUEST_PARAMETER_INVALID_PARTICIPANTS_EQUALS);
        }

        if (participantListBO.size() > ConstraintsUtil.MAX_PARTICIPANT_LIST) {
            throw new BadRequestException(Messages.BAD_REQUEST_PARAMETER_INVALID_MAX_PARTICIPANT_COUNT,  ConstraintsUtil.MAX_PARTICIPANT_LIST + "");
        }
    }


    private void validateParticipantExists(ParticipantBO participantBO, ServiceMetadataPublisherBO existingSmpBO, boolean isDNSEnabled) throws TechnicalException {

         // Check if the participant already exists in database!
        // this method validates as case-insensitive identifiers and is stricter than database constraint validation..
        Optional<ParticipantBO> existingParticipantBO = getParticipantForDomain(participantBO, existingSmpBO.getSubdomain());
        if (existingParticipantBO.isPresent()) {
            throw new BadRequestException(Messages.BAD_REQUEST_PARAMETER_PARTICIPANT_EXISTS_SUBDOMAIN, participantBO.getParticipantId(), participantBO.getScheme(), existingSmpBO.getSubdomain().getSubdomainName());
        }

        // Check if the participant record already exists!
        // this method is much faster than checking large database
        if (isDNSEnabled) {
            DNSSubSomainRecordTypeEnum recordType = (StringUtils.equalsIgnoreCase(existingSmpBO.getSubdomain().getDnsRecordTypes(), NAPTR.getCode())) ? NAPTR : CNAME;
            if (dnsClientService.isParticipantAlreadyCreated(participantBO, recordType)) {
                // just warn that DNS entry already exits
                loggingService.warn(String.format(Messages.BAD_REQUEST_PARAMETER_PARTICIPANT_EXISTS_DNS,
                        participantBO.getParticipantId(), participantBO.getScheme(), existingSmpBO.getSubdomain().getDnsZone()));
            }
        }
    }

    @Override
    public void validateFreeQuotaForSMP(int iCount, ServiceMetadataPublisherBO existingSmpBO) throws TechnicalException {
        SubdomainBO subdomainBO = existingSmpBO.getSubdomain();
        if (subdomainBO == null) {
            LOG.warn("Skipping participant quota validation because no subdomain entity was found for SMP: [{}]", existingSmpBO.getSmpId());
            return;
        }

        long maxDomainParticipantCount = subdomainBO.getMaxParticipantCountForDomain() == null ?
                -1 :subdomainBO.getMaxParticipantCountForDomain().longValue();
        long maxSMPParticipantCount = subdomainBO.getMaxParticipantCountForSMP() == null ?
                -1 : subdomainBO.getMaxParticipantCountForSMP().longValue();

        long participantCountForSMPDomain = getParticipantCountForSMPDomain(existingSmpBO);

        if (maxDomainParticipantCount > -1  && participantCountForSMPDomain + iCount > maxDomainParticipantCount) {
            throw new SubDomainOutOfParticipantQuotaException("SubDomain: [" + subdomainBO.getSubdomainName()
                    + "] exceeded max. number [" + maxDomainParticipantCount
                    + "] of allowed participants for the subdomain. Expected new count ["
                    + (participantCountForSMPDomain + iCount) + "]!");
        }

        long participantCountForSMP = getParticipantCountForSMP(existingSmpBO);
        if (maxSMPParticipantCount > -1  && participantCountForSMP + iCount > maxSMPParticipantCount) {
            throw new SMPOutOfParticipantQuotaException("SMP: [" + existingSmpBO.getSmpId()
                    + "] exceeded max. number [" + maxSMPParticipantCount
                    + "] of allowed participants for the SMP. Expected new count ["
                    + (participantCountForSMP + iCount) + "]!");
        }
    }



    @Override
    public void checkNoMigrationPlanned(String smpId, List<ParticipantBO> participantBOList) throws TechnicalException {
        List<MigrationRecordBO> migrationRecordBOs = migrationDAO.findMigrationsRecordsForParticipants(smpId, participantBOList);
        if (migrationRecordBOs != null && !migrationRecordBOs.isEmpty()) {
            StringBuilder participants = new StringBuilder();
            for (int i = 0; i < migrationRecordBOs.size(); i++) {
                participants.append(migrationRecordBOs.get(i).getParticipantId());
                if (i != migrationRecordBOs.size() - 1) {
                    participants.append(", ");
                }
            }
            throw new MigrationPlannedException("A migration is planned for the participants [" + participants + "] of the SMP " + smpId + ". Please contact your system administrator.");
        }
    }

    /**
     * Convert a list of participant to a Map with the key being the scheme
     *
     * @param participantBOList the list to be converted
     * @return a Map with the key being the scheme
     */
    private Map<String, List<String>> toMap(List<ParticipantBO> participantBOList) {
        // transform from List to Map
        Map<String, List<String>> mapParticipants = new HashMap<>();
        if (participantBOList != null) {
            for (final ParticipantBO participantBO : participantBOList) {
                if (mapParticipants.get(participantBO.getScheme()) == null) {
                    List<String> partListForScheme = new ArrayList<>();
                    mapParticipants.put(participantBO.getScheme(), partListForScheme);
                }
                mapParticipants.get(participantBO.getScheme()).add(participantBO.getParticipantId());
            }
        }
        return mapParticipants;
    }
}
