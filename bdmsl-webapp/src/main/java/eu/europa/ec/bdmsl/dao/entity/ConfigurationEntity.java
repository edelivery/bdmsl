/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.entity;

import eu.europa.ec.bdmsl.dao.utils.ColumnDescription;
import org.hibernate.envers.Audited;

import jakarta.persistence.*;
import java.util.Objects;

import static eu.europa.ec.bdmsl.dao.QueryNames.*;

/**
 * @author Adrien FERIAL
 * @since 15/06/2015
 */
@Entity
@Audited
@Table(name = "bdmsl_configuration")
@org.hibernate.annotations.Table(appliesTo = "bdmsl_configuration", comment = "SML configuration properties")
@NamedQueries({
        @NamedQuery(name = CONFIGURATION_ENTITY_GET_ALL,
                query = "SELECT config from ConfigurationEntity config"),
        @NamedQuery(name = CONFIGURATION_ENTITY_GET_BY_PROPERTY_NAME,
                query = "SELECT config from ConfigurationEntity config where config.property = :propertyName"),
        @NamedQuery(name = CONFIGURATION_ENTITY_FIND_BY_PROPERTY_NAME,
                query = "SELECT config from ConfigurationEntity config where LOWER(config.property) like LOWER(:propertyName)"),
        @NamedQuery(name = CONFIGURATION_ENTITY_MAX_UPDATE_DATE,
                query = "SELECT max(config.lastUpdateDate) from ConfigurationEntity config"),
})
public class ConfigurationEntity extends AbstractEntity {
    @Id
    @Column(name = "property", length = CommonColumnsLengths.MAX_TEXT_LENGTH_512, nullable = false, unique = true)
    @ColumnDescription(comment = "Property key/name")
    private String property;

    @Basic
    @Column(name = "value", length = CommonColumnsLengths.MAX_FREE_TEXT_LENGTH)
    @ColumnDescription(comment = "Value of the property")
    private String value;

    @Column(name = "description", length = CommonColumnsLengths.MAX_FREE_TEXT_LENGTH)
    @ColumnDescription(comment = "Description of the property")
    private String description;

    public String getProperty() {
        return property;
    }

    @Override
    public Object getId() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ConfigurationEntity)) return false;
        ConfigurationEntity that = (ConfigurationEntity) o;
        return Objects.equals(property, that.property) &&
                Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(property);
    }
}
