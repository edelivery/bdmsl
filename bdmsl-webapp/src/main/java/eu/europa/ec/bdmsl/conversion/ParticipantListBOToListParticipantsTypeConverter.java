/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2017 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.conversion;

import ec.services.wsdl.bdmsl.data._1.ExistsParticipant;
import ec.services.wsdl.bdmsl.data._1.ListParticipantsType;
import eu.europa.ec.bdmsl.common.bo.ParticipantListBO;
import org.busdox.transport.identifiers._1.ParticipantIdentifierType;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * This converter class transforms ParticipantListBO objects into ListParticipantsType objects.
 * <p/>
 *
 * @author Thomas Dussart
 * @since 24/05/2024
 */
@Component
public class ParticipantListBOToListParticipantsTypeConverter extends AutoRegisteringConverter<ParticipantListBO, ListParticipantsType> {

    public ParticipantListBOToListParticipantsTypeConverter(ConversionService conversionService) {
        super(conversionService);
    }

    @Override
    public ListParticipantsType convert(ParticipantListBO source) {
        if (source == null) {
            return null;
        }

        ListParticipantsType target = new ListParticipantsType();
        BeanUtils.copyProperties(source, target);

        // Convert the list of ParticipantBO to ParticipantsType
        List<ExistsParticipant> participantsTypeList = source.getParticipantBOList().stream()
                .map(participantBO -> {
                    ExistsParticipant participantsType = new ExistsParticipant();
                    participantsType.setParticipantIdentifier(getConversionService().convert(participantBO, ParticipantIdentifierType.class));
                    participantsType.setServiceMetadataPublisherID(participantBO.getSmpId());
                    return participantsType;
                })
                .collect(Collectors.toList());

        target.getParticipants().addAll(participantsTypeList);

        return target;
    }
}
