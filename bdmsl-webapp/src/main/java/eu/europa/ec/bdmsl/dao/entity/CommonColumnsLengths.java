/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.entity;

/**
 * @author Joze RIHTARSIC
 * @since 22/12/2018
 */
public class CommonColumnsLengths {
    public static final int MAX_ALLOWED_WILDCARD = 255;
    public static final int MAX_CERTIFICATE_ID = 255;
    public static final int MAX_CRL_URL = 1000;
    public static final int MAX_PARTICIPANT_IDENTIFIER_SCHEME_LENGTH = 255;
    public static final int MAX_PARTICIPANT_IDENTIFIER_VALUE_LENGTH = 255;
    public static final int MAX_SML_SMP_ID_LENGTH = 64;
    // Migration key by PEPPOL spec should be only 24 chars. But current database length is 50!
    public static final int MAX_MIGRATION_KEY = 50;

    public static final int MAX_TEXT_LENGTH_512 = 512;
    public static final int MAX_FREE_TEXT_LENGTH = 4000;
    public static final int MAX_MEDIUM_TEXT_LENGTH = 1024;
    public static final int MAX_SML_SUBDOMAIN_LENGTH = 255;


    public static final int MAX_DOCUMENT_TYPE_IDENTIFIER_VALUE_LENGTH = 500;
    public static final int MAX_DOCUMENT_TYPE_IDENTIFIER_SCHEME_LENGTH = 500;
    public static final int MAX_USERNAME_LENGTH = 256;
    public static final int MAX_PASSWORD_LENGTH = 256;


    public static final int MAX_USER_ROLE_LENGTH = 256;

    public static final int MAX_TEXT_LENGTH_128 = 128;
    public static final int MAX_DNS_TYPE_LENGTH = 32;
    public static final int MAX_DNS_NAPTR_SERVICE = 32;


}
