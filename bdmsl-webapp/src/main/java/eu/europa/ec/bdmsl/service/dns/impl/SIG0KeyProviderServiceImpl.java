/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.dns.impl;

import com.google.common.base.Strings;
import eu.europa.ec.bdmsl.business.IConfigurationBusiness;
import eu.europa.ec.bdmsl.common.exception.SIG0Exception;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.common.logging.ILoggingService;
import eu.europa.ec.bdmsl.service.dns.ISIG0KeyProviderService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.xbill.DNS.*;
import org.xbill.DNS.Record;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;


/**
 * Class is ISIG0KeyProviderService that provides the SIG0 key for the DNS server and retrieves configured SIG0 key for
 * message signing.
 *
 * @author feriaad
 * @since 12/06/2015.
 */
@Service
public class SIG0KeyProviderServiceImpl implements ISIG0KeyProviderService {

    private static final Logger LOG = LoggerFactory.getLogger(SIG0KeyProviderServiceImpl.class);


    protected ILoggingService loggingService;
    protected IConfigurationBusiness configurationBusiness;


    public SIG0KeyProviderServiceImpl(ILoggingService loggingService, IConfigurationBusiness configurationBusiness) {
        this.loggingService = loggingService;
        this.configurationBusiness = configurationBusiness;
    }


    // cached private keys and SIG0 records
    private String privateKeyFileName;
    private PrivateKey privateKey = null;
    private KEYRecord sig0Record = null;

    /**
     * This method is used to read a KEYRecord for SIG0 from a file.
     *
     * @return KEYRecord
     * @throws TechnicalException
     */
    @Override
    public PrivateKey getPrivateSIG0Key() throws TechnicalException {

        String errorMessage = "Can not get the SIG0 private key";
        String getDNSSig0KeyFilename = configurationBusiness.getDNSSig0KeyFilename();
        if (privateKey != null && StringUtils.equals(privateKeyFileName, getDNSSig0KeyFilename)) {
            LOG.debug("Returning cached SIG0 private key!");
            return privateKey;
        }

        try (InputStream stream = getStream(getDNSSig0KeyFilename)) {
            if (stream == null) {
                throw new SIG0Exception(errorMessage);
            }
            LOG.debug("Parse private key!");
            privateKey = Bind9PrivateKeyParser.toPrivateKey(stream);
            privateKeyFileName  = getDNSSig0KeyFilename;
        } catch (IOException exc) {
            throw new SIG0Exception(errorMessage, exc);
        }
        return privateKey;
    }

    /**
     * This method is used to read a KEYRecord for SIG0 from a file. Method tries to open the stream to a file,
     * if is not successful then it tries to open the stream to a resource file.
     *
     * @return InputStream BIND9 private key file
     * @throws InvalidKeySpecException
     */
    protected InputStream getStream(String dnsClientSIG0KeyFileName) {
        String configurationDir = configurationBusiness.getConfigurationFolder();
        Path sig0Keypath = Paths.get(configurationDir, dnsClientSIG0KeyFileName);
        LOG.debug("Open the DNS key stream to the file: [{}]", sig0Keypath.toAbsolutePath());
        try {
            return new FileInputStream(sig0Keypath.toFile());
        } catch (IOException exc) {
            InputStream stream = this.getClass().getResourceAsStream(dnsClientSIG0KeyFileName);
            if (stream == null) {
                stream = ClassLoader.getSystemClassLoader().getResourceAsStream(dnsClientSIG0KeyFileName);
                if (stream == null) {
                    stream = Thread.currentThread().getContextClassLoader().getResourceAsStream(dnsClientSIG0KeyFileName);
                }
            }
            return stream;
        }
    }

    /**
     * This method is used to retrieve a KEYRecord for SIG0 from the DNS server.
     *
     * @return KEYRecord    SIG0 key record from the DNS server
     * @throws TechnicalException if the SIG0 key can not be retrieved
     */
    @Override
    public KEYRecord getSIG0Record() throws TechnicalException {
        String sig0PublicKeyName = configurationBusiness.getDNSSig0KeyName();
        LOG.info("Get the SIG0 record for the key name: [{}] and cached record [{}].", sig0PublicKeyName, sig0Record);
        if (StringUtils.isBlank(sig0PublicKeyName)) {
            LOG.warn("SIG0 record is blank. Can not retrieve KEYRecord for blank name");
            return null;
        }

        if (sig0Record != null && StringUtils.equalsIgnoreCase(sig0Record.getName().toString(), sig0PublicKeyName)) {
            LOG.info("Returning cached SIG0 record [{}]!", sig0Record);
            return sig0Record;
        }

        LOG.info("Fetch SIG0 record for domain name [{}]!", sig0PublicKeyName);
        String dnsServer = configurationBusiness.getDNSServer();
        if (!Strings.isNullOrEmpty(sig0PublicKeyName)) {
            Lookup aLookup;
            try {
                aLookup = new Lookup(sig0PublicKeyName, Type.KEY);
            } catch (TextParseException e) {
                throw new SIG0Exception("Can not get the SIG0 record from the SIG0 key " + sig0PublicKeyName, e);
            }
            SimpleResolver res;
            try {
                res = new SimpleResolver(dnsServer);
            } catch (UnknownHostException e) {
                throw new SIG0Exception("Can not instantiate the resolver for dns server " + dnsServer, e);
            }
            res.setTCP(true);
            aLookup.setResolver(res);
            aLookup.setCache(null);
            Record[] aRecords = aLookup.run();
            for (Record rec : aRecords) {
                if (rec.getType() == Type.KEY) {
                    sig0Record = (KEYRecord) rec;
                }
            }
        }
        LOG.info("GOT SIG0 record for domain name [{}]!", sig0PublicKeyName);
        return sig0Record;
    }
}
