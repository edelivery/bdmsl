/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.business;

import eu.europa.ec.bdmsl.common.bo.CertificateBO;
import eu.europa.ec.bdmsl.common.bo.ServiceMetadataPublisherBO;

import eu.europa.ec.bdmsl.common.exception.TechnicalException;

/**
 * @author Adrien FERIAL
 * @since 12/06/2015
 */
public interface IManageServiceMetadataBusiness {
    ServiceMetadataPublisherBO read(String id) throws
             TechnicalException;

    boolean isLogicalAddressToBeUpdated(ServiceMetadataPublisherBO smpBO) throws
             TechnicalException;

    boolean hasNaptrRecords(ServiceMetadataPublisherBO smpBO) throws
             TechnicalException;


    /**
     * Method validates if SMP has valid SMP-ID, logical and physical address data.
     * If data are incorrect it throws an exception.
     *
     * @param smpBO ServiceMetadataPublisherBO object to be validated
     * @throws TechnicalException if data are incorrect
     */
    void validateSMPData(ServiceMetadataPublisherBO smpBO) throws
             TechnicalException;

    /**
     * Method validates SMP subdomain and authorized certificate subdomain. if they do not match it return configuration error.
     * Validation was added doe to several database misconfiguration where once certificate was used in multiple domains..
     *
     * @param smpBO ServiceMetadataPublisherBO object to be validated against certificate subdomain
     * @throws TechnicalException if data are incorrect
     */
    void validateSMPSubdomain(ServiceMetadataPublisherBO smpBO)
            throws  TechnicalException;

    /**
     * Method validates if SMP ID (case-insensitive) already exits. If it exists then it throws an exception!
     *
     * @param smpId SMP-ID to be validated if it exists
     * @throws  TechnicalException if SMP-ID  exists
     */
    void verifySMPNotExist(String smpId) throws
             TechnicalException;

    void validateAuthorization(ServiceMetadataPublisherBO smpBO) throws TechnicalException;

    ServiceMetadataPublisherBO createSMP(ServiceMetadataPublisherBO smpBO) throws
             TechnicalException;

    void createCurrentCertificate() throws  TechnicalException;

    CertificateBO findCertificate(String name) throws
             TechnicalException;

    ServiceMetadataPublisherBO verifySMPExist(String smpId) throws
             TechnicalException;

    ServiceMetadataPublisherBO deleteSMP(ServiceMetadataPublisherBO smpBO) throws
             TechnicalException;

    void disableSMP(ServiceMetadataPublisherBO smpBO, boolean disable) throws
            TechnicalException;

    /**
     * Method validates if SMP-ID is valid dns domain part! It is not empty, and is smaller then 253 chars, parts divided by '.'
     * are smaller then 64 char,
     *
     * @param smpId SMP-ID to be validated
     * @throws TechnicalException
     */
    void validateSMPId(String smpId) throws
             TechnicalException;

    ServiceMetadataPublisherBO updateSMP(ServiceMetadataPublisherBO smpBO) throws
             TechnicalException;

    ServiceMetadataPublisherBO updateSMPIdentifier(ServiceMetadataPublisherBO smpBO, String newIdentifier);

    void checkNoMigrationPlanned(String smpId) throws  TechnicalException;

    /**
     * Purpose of the method is to validated if authenticated SMP's is authorized on the smpBO
     * Authorization matches SMP's certificate and the authenticated certificate. If Authentication does not contain
     * X509Certificate (ClientCert header) or if certificate is not
     * registered in database it validates only the certificate ids!
     *
     * @param smpBO
     * @return true/false
     */
    boolean isAuthorizedOnSMP(ServiceMetadataPublisherBO smpBO) throws TechnicalException;

    /**
     * Method creates a DB clone of the smpBO and prepares it for update/migrate participants to new SMP instance
     * @param smpBO
     * @return
     * @throws TechnicalException
     */
    ServiceMetadataPublisherBO prepareToBatchUpdateSMP(ServiceMetadataPublisherBO smpBO) throws
             TechnicalException;

}
