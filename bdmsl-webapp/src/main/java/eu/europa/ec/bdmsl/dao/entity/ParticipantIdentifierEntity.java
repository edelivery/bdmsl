/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.entity;

import eu.europa.ec.bdmsl.dao.entity.reports.InconsistencyDbCNameEntity;
import eu.europa.ec.bdmsl.dao.entity.reports.InconsistencyDbNaptrEntity;
import eu.europa.ec.bdmsl.dao.utils.ColumnDescription;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import jakarta.persistence.*;
import java.util.Objects;

import static eu.europa.ec.bdmsl.dao.QueryNames.*;

/**
 * @author Adrien FERIAL
 * @since 15/06/2015
 */
@Entity
@Audited
@AuditTable("bdmsl_participant_ident_aud") // change name else is to long for oracle
@Table(name = "bdmsl_participant_identifier",
        indexes = {@Index(name = "SML_PARTC_IDENT_NKEY_IDX", columnList = "participant_id, scheme, fk_smp_id", unique = true),
                @Index(name = "SML_PARTC_IDENT_ID_IDX", columnList = "participant_id", unique = false),
                @Index(name = "SML_PARTC_IDENT_SCH_IDX", columnList = "scheme", unique = false)
        })
@NamedQueries({
        @NamedQuery(name = PARTICIPANT_IDENTIFIER_ENTITY_GET_ALL,
                query = "SELECT part from ParticipantIdentifierEntity part"),
        @NamedQuery(name = PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_IDENTIFIER,
                query = "SELECT part from ParticipantIdentifierEntity part where upper(part.participantId) = upper(:participantId) " +
                        " and (:scheme IS NULL AND part.scheme IS NULL OR part.scheme = :scheme)"),
        @NamedQuery(name = PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_CASE_SENSITIVE_IDENTIFIER,
                query = "SELECT part from ParticipantIdentifierEntity part where part.participantId = :participantId " +
                        " and (:scheme IS NULL AND part.scheme IS NULL OR part.scheme = :scheme)"),
        @NamedQuery(name = PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_SMP_IDENTIFIER,
                query = "SELECT part from ParticipantIdentifierEntity part where upper(part.smp.smpId) = upper(:smpId) order by part.id asc"),
        @NamedQuery(name = PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_IDENTIFIER_AND_SMP,
                query = "SELECT part from ParticipantIdentifierEntity part where upper(part.participantId) = upper(:participantId)" +
                        " and (:scheme IS NULL and part.scheme IS NULL OR  part.scheme = :scheme) and upper(part.smp.smpId) = upper(:smpId)"),
        @NamedQuery(name = PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_CASE_SENSITIVE_IDENTIFIER_AND_SMP,
                query = "SELECT part from ParticipantIdentifierEntity part where part.participantId = :participantId" +
                        " and (:scheme IS NULL and part.scheme IS NULL OR  part.scheme = :scheme) and upper(part.smp.smpId) = upper(:smpId)"),
        @NamedQuery(name = PARTICIPANT_IDENTIFIER_ENTITY_FIND_BY_IDENTIFIER_LIST_AND_SMP,
                query = "SELECT part from ParticipantIdentifierEntity part where (:scheme IS NULL and part.scheme IS NULL OR  part.scheme = :scheme) " +
                        "and upper(part.participantId) in :participantIdList and upper(part.smp.smpId) = upper(:smpId)"),
        @NamedQuery(name = PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_CASE_SENSITIVE_IDENTIFIER_AND_DOMAIN,
                query = "SELECT part from ParticipantIdentifierEntity part where part.participantId = :participantId" +
                        " and  (:scheme IS NULL AND part.scheme IS NULL OR part.scheme = :scheme) and part.smp.subdomain.subdomainId = :subdomainId"),
        @NamedQuery(name = PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_IDENTIFIER_AND_DOMAIN,
                query = "SELECT part from ParticipantIdentifierEntity part where upper(part.participantId) = upper(:participantId)" +
                        " and  (:scheme IS NULL AND part.scheme IS NULL OR part.scheme = :scheme) and part.smp.subdomain.subdomainId = :subdomainId"),
        @NamedQuery(name = PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_IDENTIFIER_AND_DOMAIN_AND_NOT_FOR_SMP,
                query = "SELECT part from ParticipantIdentifierEntity part where upper(part.participantId) = upper(:participantId)" +
                        " and  (:scheme IS NULL AND part.scheme IS NULL OR part.scheme = :scheme) and part.smp.subdomain.subdomainId = :subdomainId and upper(part.smp.smpId) != upper(:smpId)"),

        @NamedQuery(name = PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_NULL_HASH,
                query = "SELECT part from ParticipantIdentifierEntity part where part.cnameHash is null OR part.naptrHash is null" +
                        " OR part.cnameHash ='' OR part.naptrHash =''"),
        @NamedQuery(name = PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_SMP_IDENTIFIER_AND_DISABLED,
                query = "SELECT part from ParticipantIdentifierEntity part where upper(part.smp.smpId) = upper(:smpId) and part.disabled = :disabled order by part.id asc"),

})
@NamedNativeQueries({
        @NamedNativeQuery(name = INCONSISTENCY_DB_CNAME_ENTITY_GET_ALL_DNS_CNAME_RECORDS,
                query = "SELECT prt.scheme AS scheme, prt.participant_id AS participant_id, subd.subdomain_name AS subdomain_name, smp.smp_id as smp_id," +
                        "    LOWER(concat(concat('B-',prt.cname_hash),concat(concat('.', ( " +
                        "        CASE " +
                        "            WHEN prt.scheme IS NULL THEN '' " +
                        "            WHEN prt.scheme = '' THEN '' " +
                        "            WHEN prt.scheme LIKE 'urn:oasis:names:tc:ebcore:partyid-type:%' THEN '' " +
                        "            ELSE concat(prt.scheme,'.') " +
                        "        END " +
                        "    ) ),concat(subd.subdomain_name,'.') ) )) AS dns_record_name," +
                        "    prt.disabled as disabled " +
                        "FROM   bdmsl_participant_identifier prt  INNER JOIN bdmsl_smp smp ON prt.fk_smp_id = smp.id INNER JOIN bdmsl_subdomain subd ON smp.fk_subdomain_id = subd.subdomain_id " +
                        "   WHERE subd.dns_record_types in (:dnsAllType,:dnsCnameType) " +
                        "   AND prt.created_on < :createDate" +
                        " order by prt.id asc",
                resultSetMapping = "InconsistencyDbCNameEntity"),

        @NamedNativeQuery(name = INCONSISTENCY_DB_NAPTR_ENTITY_GET_ALL_DNS_NAPTR_RECORDS,
                query = "SELECT prt.scheme AS scheme, prt.participant_id AS participant_id, subd.subdomain_name AS subdomain_name, smp.endpoint_logical_address AS logical_address," +
                        "   LOWER(CONCAT(  CONCAT(prt.naptr_hash, '.') ,CONCAT( CONCAT( (CASE " +
                        "            WHEN prt.scheme IS NULL THEN '' " +
                        "            WHEN prt.scheme = '' THEN '' " +
                        "            WHEN prt.scheme like 'urn:oasis:names:tc:ebcore:partyid-type:%' THEN '' " +
                        "            ELSE CONCAT(prt.scheme, '.') " +
                        "            END ) " +
                        "    , subd.subdomain_name ) " +
                        "    , '.'))) AS dns_record_name," +
                        "   prt.disabled as disabled " +
                        " FROM bdmsl_participant_identifier prt INNER JOIN bdmsl_smp smp ON prt.fk_smp_id = smp.id INNER JOIN bdmsl_subdomain subd ON smp.fk_subdomain_id = subd.subdomain_id " +
                        "             WHERE subd.dns_record_types IN (:dnsAllType,:dnsNaptrType)" +
                        "   AND prt.created_on < :createDate" +
                        " order by prt.id asc",
                resultSetMapping = "InconsistencyDbNaptrEntity"),

        @NamedNativeQuery(name = PARTICIPANT_IDENTIFIER_ENTITY_GET_PARTICIPANT_COUNT,
                query = "SELECT count(prt.participant_id) AS participant_count" +
                        " FROM bdmsl_participant_identifier prt"),

        @NamedNativeQuery(name = PARTICIPANT_IDENTIFIER_ENTITY_GET_PARTICIPANT_COUNT_FOR_SMP,
                query = "SELECT count(prt.participant_id) AS participant_count" +
                        " FROM bdmsl_participant_identifier prt INNER JOIN bdmsl_smp smp ON prt.fk_smp_id = smp.id " +
                        " where smp.smp_id=:smpId"),

        @NamedNativeQuery(name = PARTICIPANT_IDENTIFIER_ENTITY_GET_PARTICIPANT_COUNT_FOR_SMP_AND_STATUS,
                query = "SELECT count(prt.participant_id) AS participant_count" +
                        " FROM bdmsl_participant_identifier prt INNER JOIN bdmsl_smp smp ON prt.fk_smp_id = smp.id " +
                        " where smp.smp_id=:smpId and prt.disabled=:disabled"),

        @NamedNativeQuery(name = PARTICIPANT_IDENTIFIER_ENTITY_GET_PARTICIPANT_COUNT_FOR_SUB_DOMAIN,
                query = "SELECT count(prt.participant_id) AS participant_count" +
                        " FROM bdmsl_participant_identifier prt INNER JOIN bdmsl_smp smp ON prt.fk_smp_id = smp.id " +
                        " where smp.fk_subdomain_id=:subdomainId"),

        @NamedNativeQuery(name = PARTICIPANT_IDENTIFIER_ENTITY_GET_CASE_SENSITIVE_PARTICIPANT_COUNT_DOMAIN_ID,
                query = "SELECT count(prt.participant_id) AS participant_count" +
                        " FROM bdmsl_participant_identifier prt INNER JOIN bdmsl_smp smp ON prt.fk_smp_id = smp.id " +
                        " WHERE smp.fk_subdomain_id=:subdomainId " +
                        "   AND prt.participant_id = :participantId " +
                        "   AND (:scheme IS NULL AND prt.scheme IS NULL OR prt.scheme = :scheme)"),
        @NamedNativeQuery(name = PARTICIPANT_IDENTIFIER_ENTITY_GET_PARTICIPANT_COUNT_DOMAIN_ID,
                query = "SELECT count(prt.participant_id) AS participant_count" +
                        " FROM bdmsl_participant_identifier prt INNER JOIN bdmsl_smp smp ON prt.fk_smp_id = smp.id " +
                        " WHERE smp.fk_subdomain_id=:subdomainId " +
                        "   AND upper(prt.participant_id) = upper(:participantId) " +
                        "   AND (:scheme IS NULL AND prt.scheme IS NULL OR prt.scheme = :scheme)")
})
@SqlResultSetMappings({
        @SqlResultSetMapping(name = "InconsistencyDbNaptrEntity",
                classes = {
                        @ConstructorResult(targetClass = InconsistencyDbNaptrEntity.class,
                                columns = {
                                        @ColumnResult(name = "scheme"),
                                        @ColumnResult(name = "participant_id"),
                                        @ColumnResult(name = "dns_record_name"),
                                        @ColumnResult(name = "subdomain_name"),
                                        @ColumnResult(name = "logical_address"),
                                        @ColumnResult(name = "disabled")})

                }),
        @SqlResultSetMapping(name = "InconsistencyDbCNameEntity",
                classes = {
                        @ConstructorResult(targetClass = InconsistencyDbCNameEntity.class,
                                columns = {
                                        @ColumnResult(name = "scheme"),
                                        @ColumnResult(name = "participant_id"),
                                        @ColumnResult(name = "dns_record_name"),
                                        @ColumnResult(name = "subdomain_name"),
                                        @ColumnResult(name = "smp_id"),
                                        @ColumnResult(name = "disabled")})
                })
})
@org.hibernate.annotations.Table(appliesTo = "bdmsl_participant_identifier",
        comment = "Participant identifiers registered on SML")
public class ParticipantIdentifierEntity extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "bdmsl_participant_ident_seq")
    // strategy native uses sequence in oracle and auto_increment on mysql
    // from hibernate 5.4 name is also the name of sequence, before it was hibernate
    @GenericGenerator(name = "bdmsl_participant_ident_seq", strategy = "native")
    @Column(name = "id")
    private Long id;


    @Column(name = "participant_id", length = CommonColumnsLengths.MAX_PARTICIPANT_IDENTIFIER_VALUE_LENGTH, nullable = false)
    @ColumnDescription(comment = "The participant identifier")
    private String participantId;


    @Column(name = "scheme", length = CommonColumnsLengths.MAX_PARTICIPANT_IDENTIFIER_SCHEME_LENGTH)
    @ColumnDescription(comment = "The scheme of the participant identifier")
    private String scheme;

    @Column(name = "naptr_hash")
    @ColumnDescription(comment = "Base32 Sha256 hash value of participant identifier for naptr")
    private String naptrHash;

    @Column(name = "cname_hash")
    @ColumnDescription(comment = "MD5 hash value of participant identifier for cname value")
    private String cnameHash;

    @Column(name = "disabled", nullable = false)
    @ColumnDefault("0")
    @ColumnDescription(comment = "Is the participant disabled. If disabled, participant does not have record in dns.")
    private boolean disabled = false;

    @ManyToOne
    @JoinColumn(name = "fk_smp_id")
    private SmpEntity smp;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getParticipantId() {
        return participantId;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public SmpEntity getSmp() {
        return smp;
    }

    public void setSmp(SmpEntity smp) {
        this.smp = smp;
    }

    public String getNaptrHash() {
        return naptrHash;
    }

    public void setNaptrHash(String naptrHash) {
        this.naptrHash = naptrHash;
    }

    public String getCnameHash() {
        return cnameHash;
    }

    public void setCnameHash(String cnameHash) {
        this.cnameHash = cnameHash;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ParticipantIdentifierEntity)) return false;
        ParticipantIdentifierEntity that = (ParticipantIdentifierEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(participantId, that.participantId) &&
                Objects.equals(scheme, that.scheme) &&
                Objects.equals(smp, that.smp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(participantId, scheme, smp);
    }

    @Override
    public String toString() {
        return "ParticipantIdentifierEntity{" +
                "id=" + id +
                ", participantId='" + participantId + '\'' +
                ", scheme='" + scheme + '\'' +
                ", smp=" + (smp != null ? smp.getSmpId() : "null") +
                '}';
    }
}
