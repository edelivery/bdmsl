/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.reports;

import eu.europa.ec.bdmsl.dao.entity.reports.ExpiredSMPEntity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.util.Strings;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.StringJoiner;

import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME;

/**
 * Expired smp report object. The input of the report object is the list ExpiredSMPEntity. The report can be serialized to string or
 * to {@code java.io.Writer} object. The report has 3 parts:
 * - info: basic info of the report as creation date, server name etc...
 * - Overview: the basic information of the report
 * - Details: the list of all SMPs with expired certificates.
 *
 * @author Joze RIHTARSIC
 * @since 4.2
 */
public class SMPExpiredCertificatesReport {
    private static final Logger LOG = LogManager.getLogger(SMPExpiredCertificatesReport.class);

    final List<ExpiredSMPEntity> expiredSMPEntities;
    final String serverLocalHost;
    final String serverName;

    public SMPExpiredCertificatesReport(List<ExpiredSMPEntity> expiredSMPEntities, String serverLocalHost, String serverName) {
        this.expiredSMPEntities = expiredSMPEntities;
        this.serverLocalHost = serverLocalHost;
        this.serverName = serverName;
    }

    public void serialize(Writer writer) throws IOException {

        writer.append("## BDMSL REPORT: SMP list with an expired certificates! ##\n");
        writeServerInfo(writer);
        writer.append("\n# Overview # \n\n");
        if (expiredSMPEntities == null || expiredSMPEntities.isEmpty()) {
            writer.append("No registered SMP with an expired certificate!");
            return;
        }

        writer.append("There are ")
                .append(expiredSMPEntities.size() + "")
                .append(" SMP(s) with expired certificate\n");

        writer.append("\n# Detail # \n\n");
        serializeHeaderData(writer);
        expiredSMPEntities.forEach(expiredSMPEntity ->
                serializeData(expiredSMPEntity, writer)
        );
    }

    protected void serializeData(ExpiredSMPEntity expiredSMPEntity, Writer writer) {
        try {
            writer.append(expiredSMPEntity.getSmpId()).append("\t");
            writer.append(dateTimeToString(expiredSMPEntity.getSmpCreatedOn())).append("\t");
            writer.append(dateTimeToString(expiredSMPEntity.getLastParticipantAddedOn())).append("\t");
            writer.append(expiredSMPEntity.getCertificateId()).append("\t");
            writer.append(dateTimeToString(expiredSMPEntity.getCertificateValidFrom())).append("\t");
            writer.append(dateTimeToString(expiredSMPEntity.getCertificateValidTo())).append("\t");
            writer.append(expiredSMPEntity.getSubdomainName()).append("\t");
            writer.append(expiredSMPEntity.getRegisteredParticipantsCount() + "").append("\n");
        } catch (IOException e) {
            LOG.error("Error occurred while writing the entity for the SMP Expired certificates report!", e);
        }
    }

    protected void serializeHeaderData(Writer writer) throws IOException {
        writer.append("SMP ID\t");
        writer.append("Created on\t");
        writer.append("Last part. created on\t");
        writer.append("Certificate id\t");
        writer.append("Cert. valid from\t");
        writer.append("Cert. valid to\t");
        writer.append("Subdomain\t");
        writer.append("Partc. count\n");
    }

    protected String dateTimeToString(OffsetDateTime dateTime) {
        if (dateTime == null) {
            return "/";
        }
        return ISO_OFFSET_DATE_TIME.format(dateTime);
    }

    private void writeServerInfo(Writer writer) throws IOException {
        writer.append(" Report created at ");
        writer.append(LocalDateTime.now().toString());
        writer.append("\n\n");
        writer.append("# Server Info # \n");
        writer.append("Server Local Host: ");
        writer.append(serverLocalHost).append("\n");
        if (Strings.isNotBlank(serverName)) {
            writer.append("Server Name: ");
            writer.append(serverName).append("\n");
        }
    }

    public String getDetailedReport() {
        StringWriter result = new StringWriter();
        try {
            serialize(result);
        } catch (IOException e) {
            LOG.error("Error occurred while generating the SMP Expired certificates report!", e);
        }
        return result.toString();
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", SMPExpiredCertificatesReport.class.getSimpleName() + "[", "]")
                .add("expiredSMPEntities=" + expiredSMPEntities)
                .toString();
    }
}
