/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.impl;


import eu.europa.ec.bdmsl.business.IConfigurationBusiness;
import eu.europa.ec.bdmsl.common.exception.BadConfigurationException;
import eu.europa.ec.bdmsl.common.exception.GenericTechnicalException;
import eu.europa.ec.bdmsl.common.exception.KeyException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.common.logging.ILoggingService;
import eu.europa.ec.bdmsl.service.ITruststoreService;
import eu.europa.ec.edelivery.security.utils.KeystoreUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Implementation of truststore service
 *
 * @author Joze RIHTARSIC
 * @since 4.1
 */
@Service
public class TruststoreServiceImpl implements ITruststoreService {

    public static final String ERROR_OCCURRED_WHILE_ADDING_CERTIFICATE = "Error occurred while adding certificate!";

    private IConfigurationBusiness configurationBusiness;
    ILoggingService loggingService;

    KeyStore truststore = null;
    String truststoreFilePath = null;
    long lastModifiedTruststore = 0;

    @Autowired
    public TruststoreServiceImpl(IConfigurationBusiness configurationBusiness, ILoggingService loggingService) {
        this.configurationBusiness = configurationBusiness;
        this.loggingService = loggingService;
    }

    @Override
    public KeyStore getTruststore() {
        File truststoreFile = configurationBusiness.getTruststoreFile();
        if (truststoreFile != null &&
                (this.truststore == null
                        || !StringUtils.equalsIgnoreCase(this.truststoreFilePath, truststoreFile.getAbsolutePath())
                        || this.lastModifiedTruststore < truststoreFile.lastModified())) {
            this.truststore = loadTruststore(truststoreFile);
            this.truststoreFilePath = truststoreFile.getAbsolutePath();
            this.lastModifiedTruststore = truststoreFile.lastModified();
        }
        return this.truststore;
    }


    public KeyStore validateTruststoreConfiguration() throws BadConfigurationException {
        getTruststore();
        if (this.truststore == null) {
            throw new BadConfigurationException("Truststore is not configured!");
        }
        return truststore;
    }


    /**
     * Method tries to load truststore. If it does not succeed in return null value
     *
     * @param truststoreFile - truststore file
     * @return truststore if it can open - else it returns null.
     */
    private KeyStore loadTruststore(File truststoreFile) {

        String encToken = configurationBusiness.getTruststorePassword();

        if (truststoreFile == null) {
            loggingService.warn("Can not load null truststore file! Validate the truststore configuration!", null);
            return null;
        }
        try {
            return KeystoreUtils.loadKeystore(truststoreFile,
                    configurationBusiness.decryptString(encToken), configurationBusiness.getTruststoreType());
        } catch (KeyException | IOException | KeyStoreException | CertificateException | NoSuchAlgorithmException e) {
            loggingService.error("Can not decrypt truststore password. Check the BDMSL configuration!", e);
        }
        return null;
    }

    /**
     * Method tries to store truststore. If it does not succeed in return TechnicalException
     *
     * @param truststore - truststore file
     * @throws  TechnicalException - if it can not store the truststore
     */
    private void storeTruststore(KeyStore truststore) throws TechnicalException {

        if (truststoreFilePath == null) {
            throw new GenericTechnicalException("Illegal state: Can not save truststore because it was not initialized!");
        }

        String encToken = configurationBusiness.getTruststorePassword();
        try (FileOutputStream fos = new FileOutputStream(truststoreFilePath)) {
            truststore.store(fos, configurationBusiness.decryptString(encToken).toCharArray());
            lastModifiedTruststore = (new File(truststoreFilePath)).lastModified();
        } catch (IOException e) {
            throw new GenericTechnicalException("Can not save truststore to file [" + truststoreFilePath + "].", e);
        } catch (CertificateException | NoSuchAlgorithmException | KeyStoreException e) {
            throw new GenericTechnicalException("Can not save truststore to file [" + truststoreFilePath + "] with error: [" + ExceptionUtils.getRootCauseMessage(e) + "].", e);
        }
    }

    @Override
    public String addCertificate(X509Certificate certificate, String alias) throws TechnicalException {
        // validate if truststore is configured
        validateTruststoreConfiguration();

        String aliasPrivate;
        try {
            aliasPrivate = KeystoreUtils.addCertificate(this.truststore, certificate, alias);
            storeTruststore(this.truststore);
        } catch (KeyStoreException e) {
            throw new BadConfigurationException(ERROR_OCCURRED_WHILE_ADDING_CERTIFICATE, e);
        }
        return aliasPrivate;
    }

    @Override
    public String getAliasForCertificate(X509Certificate certificate) throws TechnicalException {
        // validate if truststore is configured
        validateTruststoreConfiguration();
        try {
            // test of certificate is already in store
            return KeystoreUtils.getAliasForCertificate(this.truststore, certificate);
        } catch (KeyStoreException e) {
            throw new BadConfigurationException(ERROR_OCCURRED_WHILE_ADDING_CERTIFICATE, e);
        }
    }

    @Override
    public X509Certificate deleteCertificate(String alias) throws TechnicalException {

        // validate if truststore is configured
        validateTruststoreConfiguration();
        X509Certificate certificate;
        try {
            certificate = KeystoreUtils.deleteCertificate(this.truststore, alias);
            storeTruststore(this.truststore);
        } catch (KeyStoreException e) {
            throw new BadConfigurationException(ERROR_OCCURRED_WHILE_ADDING_CERTIFICATE, e);
        }
        return certificate;
    }

    @Override
    public X509Certificate getCertificate(String alias) throws TechnicalException {
        // validate if truststore is configured
        validateTruststoreConfiguration();
        X509Certificate certificate;
        try {
            certificate = KeystoreUtils.getCertificate(this.truststore, alias);
        } catch (KeyStoreException e) {
            throw new BadConfigurationException(ERROR_OCCURRED_WHILE_ADDING_CERTIFICATE, e);
        }
        return certificate;
    }

    @Override
    public List<String> getAllAliases(String match) throws TechnicalException {
        // validate if truststore is configured
        validateTruststoreConfiguration();

        List<String> list;
        try {
            String privateMatch = StringUtils.trimToNull(match);
            list = KeystoreUtils.getAllAliases(this.truststore);
            if (privateMatch != null) {
                list = list.stream().filter(val -> val.contains(match)).collect(Collectors.toList());
            }
        } catch (KeyStoreException e) {
            throw new BadConfigurationException(ERROR_OCCURRED_WHILE_ADDING_CERTIFICATE, e);
        }
        return list;
    }
}
