/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.dns;

import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import org.xbill.DNS.KEYRecord;

import java.security.PrivateKey;

/**
 * Interface ISIG0KeyProviderService that provides the SIG0 key for the DNS server and retrieves
 * configured SIG0 key for DNS message signing.
 *
 * @author Adrien FERIAL
 * @since 12/06/2015.
 */
public interface ISIG0KeyProviderService {
    /**
     * Method returns the private key for signing the SIG0 nsupdate messages .
     *
     * @return PrivateKey the private key for signing the SIG0 nsupdate messages
     * @throws TechnicalException if the private key cannot be retrieved
     */
    PrivateKey getPrivateSIG0Key() throws TechnicalException;

    /**
     * Method returns the SIG0 KEYRecord for the DNS server.
     *
     * @return KEYRecord the SIG0 KEYRecord for the DNS server
     * @throws TechnicalException if the SIG0 KEYRecord cannot be retrieved
     */
    KEYRecord getSIG0Record() throws TechnicalException;
}
