/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.config;

import eu.europa.ec.bdmsl.common.exception.BadConfigurationException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import jakarta.persistence.EntityManagerFactory;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.naming.NamingException;
import javax.sql.DataSource;
import java.util.Properties;


/**
 * Database bean configuration
 *
 * @author Joze RIHTARSIC
 * @since 4.2
 */
@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = {"eu.europa.ec.bdmsl.dao.entity"})
public class DatabaseConfig {
    private static final Log LOG = LogFactory.getLog(DatabaseConfig.class);


    @Value("${" + FileProperty.PROPERTY_DB_DRIVER + ":}")
    protected String driver;

    @Value("${" + FileProperty.PROPERTY_DB_USER + ":}")
    protected String username;

    @Value("${" + FileProperty.PROPERTY_DB_TOKEN + ":}")
    protected String password;

    @Value("${" + FileProperty.PROPERTY_DB_URL + ":}")
    protected String url;
    // set default jdbc
    @Value("${" + FileProperty.PROPERTY_DB_JNDI + ":jdbc/smlDatasource}")
    protected String jndiDatasourceName;

    @Value("${" + FileProperty.PROPERTY_DB_DIALECT + ":}")
    protected String hibernateDialect;

    @Bean(name = "dataSource")
    public DataSource getDataSource() throws TechnicalException {

        DataSource dataSource;
        if (!StringUtils.isBlank(url)) {
            LOG.info("create datasource with URL: " + url);
            DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
            driverManagerDataSource.setDriverClassName(driver);
            driverManagerDataSource.setUrl(url);
            driverManagerDataSource.setUsername(username);
            driverManagerDataSource.setPassword(password);
            dataSource = driverManagerDataSource;
        } else {
            LOG.info("Retrieve datasource with JNDI: " + jndiDatasourceName);
            JndiObjectFactoryBean jndiDataSource = new JndiObjectFactoryBean();
            jndiDataSource.setJndiName(jndiDatasourceName);
            try {
                jndiDataSource.afterPropertiesSet();
            } catch (IllegalArgumentException | NamingException e) {
                // rethrow
                throw new BadConfigurationException("Error occurred when creating the datasource: " + jndiDatasourceName, e);
            }
            dataSource = (DataSource) jndiDataSource.getObject();
        }
        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean smpEntityManagerFactory(DataSource dataSource, JpaVendorAdapter jpaVendorAdapter) {
        Properties prop = new Properties();
        prop.setProperty("org.hibernate.envers.store_data_at_delete", "true");
        prop.setProperty("org.hibernate.envers.audit_table_suffix", "_aud");
        LocalContainerEntityManagerFactoryBean lef = new LocalContainerEntityManagerFactoryBean();
        lef.setEntityManagerFactoryInterface(jakarta.persistence.EntityManagerFactory.class);
        lef.setDataSource(dataSource);
        lef.setJpaVendorAdapter(jpaVendorAdapter);
        lef.setPackagesToScan("eu.europa.ec.bdmsl.dao.entity");
        lef.setJpaProperties(prop);
        return lef;
    }

    @Bean
    public PlatformTransactionManager smpTransactionManager(EntityManagerFactory emf) {
        return new JpaTransactionManager(emf);
    }


    @Bean
    public JpaVendorAdapter jpaVendorAdapter() {
        HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
        if (!StringUtils.isBlank(hibernateDialect)) {
            hibernateJpaVendorAdapter.setDatabasePlatform(hibernateDialect);
        }

        return hibernateJpaVendorAdapter;
    }
}
