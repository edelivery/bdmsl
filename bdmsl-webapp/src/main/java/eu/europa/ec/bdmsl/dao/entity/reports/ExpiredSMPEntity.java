/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.entity.reports;

import java.math.BigInteger;
import java.time.OffsetDateTime;

/**
 * Expired smp data for the SMP list with an expired certificate report.
 *
 * @author Joze RIHTARSIC
 * @since 4.2
 */
public class ExpiredSMPEntity {

    private String certificateId;
    private OffsetDateTime certificateValidFrom;
    private OffsetDateTime certificateValidTo;

    private String smpId;
    private OffsetDateTime smpCreatedOn;
    private OffsetDateTime lastParticipantAddedOn;

    private String subdomainName;
    BigInteger registeredParticipantsCount;

    public ExpiredSMPEntity() {
    }

    public ExpiredSMPEntity(String smpId,
                            OffsetDateTime smpCreatedOn,
                            OffsetDateTime lastParticipantAddedOn,
                            String certificateId,
                            OffsetDateTime certificateValidFrom,
                            OffsetDateTime certificateValidTo,
                            String subdomainName,
                            BigInteger registeredParticipantsCount) {
        this.certificateId = certificateId;
        this.certificateValidFrom = certificateValidFrom;
        this.certificateValidTo = certificateValidTo;
        this.smpId = smpId;
        this.smpCreatedOn = smpCreatedOn;
        this.lastParticipantAddedOn = lastParticipantAddedOn;
        this.subdomainName = subdomainName;
        this.registeredParticipantsCount = registeredParticipantsCount;
    }

    public String getCertificateId() {
        return certificateId;
    }

    public void setCertificateId(String certificateId) {
        this.certificateId = certificateId;
    }

    public OffsetDateTime getCertificateValidFrom() {
        return certificateValidFrom;
    }

    public void setCertificateValidFrom(OffsetDateTime certificateValidFrom) {
        this.certificateValidFrom = certificateValidFrom;
    }

    public OffsetDateTime getCertificateValidTo() {
        return certificateValidTo;
    }

    public void setCertificateValidTo(OffsetDateTime certificateValidTo) {
        this.certificateValidTo = certificateValidTo;
    }

    public String getSmpId() {
        return smpId;
    }

    public void setSmpId(String smpId) {
        this.smpId = smpId;
    }

    public OffsetDateTime getSmpCreatedOn() {
        return smpCreatedOn;
    }

    public void setSmpCreatedOn(OffsetDateTime smpCreatedOn) {
        this.smpCreatedOn = smpCreatedOn;
    }

    public OffsetDateTime getLastParticipantAddedOn() {
        return lastParticipantAddedOn;
    }

    public void setLastParticipantAddedOn(OffsetDateTime lastParticipantAddedOn) {
        this.lastParticipantAddedOn = lastParticipantAddedOn;
    }

    public String getSubdomainName() {
        return subdomainName;
    }

    public void setSubdomainName(String subdomainName) {
        this.subdomainName = subdomainName;
    }

    public BigInteger getRegisteredParticipantsCount() {
        return registeredParticipantsCount;
    }

    public void setRegisteredParticipantsCount(BigInteger registeredParticipantsCount) {
        this.registeredParticipantsCount = registeredParticipantsCount;
    }


}
