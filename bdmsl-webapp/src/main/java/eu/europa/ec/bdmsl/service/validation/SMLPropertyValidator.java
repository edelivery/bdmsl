/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.validation;

import eu.europa.ec.bdmsl.common.enums.SMLPropertyEnum;
import eu.europa.ec.bdmsl.common.exception.BadConfigurationException;

import java.io.File;

/**
 * A validator that handles {@code SMLPropertyEnum} values, throwing {@code SMLPropertyValidationException} exceptions
 * when these values are invalid.
 *
 * @since 5.0
 * @author Sebastian-Ion TINCU
 */
public interface SMLPropertyValidator {

    void validate(SMLPropertyEnum property, String value, File configDirectory) throws BadConfigurationException;

    /**
     * Checks if the validator supports the valiation for the  given property.
     *
     * @param property the property to check
     * @return {@code true} if the validator supports the property, {@code false} otherwise
     */
    boolean supports(SMLPropertyEnum property);

}
