/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.impl;

import eu.europa.ec.bdmsl.business.IConfigurationBusiness;
import eu.europa.ec.bdmsl.common.exception.CertificateAuthenticationException;
import eu.europa.ec.bdmsl.common.logging.ILoggingService;
import eu.europa.ec.bdmsl.common.service.AbstractServiceImpl;
import eu.europa.ec.edelivery.security.cert.IRevocationValidator;
import eu.europa.ec.edelivery.security.cert.URLFetcher;
import eu.europa.ec.edelivery.security.cert.impl.CertificateRVStrategyEnum;
import eu.europa.ec.edelivery.security.cert.impl.CertificateRevocationValidatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;

/**
 * Service validates Certificate revocation status against CRLs and/or OCSP.
 *
 * @author Adrien FERIAL
 * @author Joze RIHTARSIC
 *
 * @since 19/06/2015
 */
@Service
public class RevocationValidatorServiceImpl extends AbstractServiceImpl implements IRevocationValidator {

    private final ILoggingService loggerService;
    private final IConfigurationBusiness configurationBusiness;
    // CRLVerifierService  instance
    private final CertificateRevocationValidatorService strategyCrvService;
    // OCSPVerifierService  instance

    @Autowired
    public RevocationValidatorServiceImpl(URLFetcher httpConnection, IConfigurationBusiness configurationBusiness, ILoggingService loggerSecurity) {
        this.configurationBusiness = configurationBusiness;
        this.loggerService = loggerSecurity;
        this.strategyCrvService = new CertificateRevocationValidatorService(httpConnection);
    }

    /**
     * Extracts the CRL distribution points from the certificate (if available)
     * and checks the certificate revocation status against the CRLs coming from
     * the distribution points. Supports HTTP, HTTPS, FTP, File based URLs.
     *
     * @param cert the certificate to be checked for revocation
     * @throws CertificateAuthenticationException if the certificate is revoked
     */
    @Override
    public boolean isCertificateRevoked(X509Certificate cert, X509Certificate issuer) throws CertificateException {
        // update verification property
        updateVerificationProperties();
        String certName = cert == null ? "Null" : cert.getSubjectX500Principal().getName();
        loggerService.debug("Validate certificate: [" + certName + "]");
        return strategyCrvService.isCertificateRevoked(cert, issuer);
    }


    /**
     * Set http implementation for fetching the revocation data.
     *
     * @param connection
     */
    @Override
    public void setHttpConnection(URLFetcher connection) {
        strategyCrvService.setHttpConnection(connection);
    }


    @Override
    public boolean isCertificateRevoked(X509Certificate x509Certificate, X509Certificate issuer, List<String> list) throws CertificateException {
        updateVerificationProperties();
        return strategyCrvService.isCertificateRevoked(x509Certificate, issuer, list);
    }

    @Override
    public boolean isSerialNumberRevoked(BigInteger bigInteger, X509Certificate issuer, String revocationUrl) throws CertificateException {
        updateVerificationProperties();
        return strategyCrvService.isSerialNumberRevoked(bigInteger, issuer, revocationUrl);
    }

    @Override
    public boolean isSerialNumberRevoked(BigInteger bigInteger, X509Certificate issuer, List<String> revocationUrlList) throws CertificateException {
        updateVerificationProperties();
        return strategyCrvService.isSerialNumberRevoked(bigInteger, issuer, revocationUrlList);
    }

    public void updateVerificationProperties(){
        CertificateRVStrategyEnum validationStrategy = configurationBusiness.getCertRevocationValidationStrategy();
        boolean isRevocationValidationEnabled = configurationBusiness.isCertRevocationValidationGraceful();
        List<String>  allowedSchemas = configurationBusiness.getCertRevocationValidationAllowedUrlProtocols();
        strategyCrvService.setValidationParameters(validationStrategy, isRevocationValidationEnabled, allowedSchemas);

    }

    @Override
    public URLFetcher getHttpConnection() {
        return strategyCrvService.getHttpConnection();
    }

}
