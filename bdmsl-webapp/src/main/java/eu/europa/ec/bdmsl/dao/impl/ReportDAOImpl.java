/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.impl;

import eu.europa.ec.bdmsl.dao.AbstractDAOImpl;
import eu.europa.ec.bdmsl.dao.IReportDAO;
import eu.europa.ec.bdmsl.dao.entity.reports.ExpiredSMPEntity;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;
import java.util.List;

/**
 * Report database component implementation
 *
 * @author Joze RIHTARSIC
 * @since 4.2
 */
@Component
public class ReportDAOImpl extends AbstractDAOImpl implements IReportDAO {

    @Override
    public List<ExpiredSMPEntity> getListOfSMPsWithExpiredCertificates() {
        List<ExpiredSMPEntity> resultEntityList = getEntityManager()
                .createNamedQuery("ExpiredSMPEntity.getAllSMPsWithExpiredCerts",
                        ExpiredSMPEntity.class)
                .setParameter("currentDate", OffsetDateTime.now())
                .getResultList();
        return resultEntityList;
    }
}
