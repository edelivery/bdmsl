/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.exception;

import eu.europa.ec.bdmsl.common.enums.SMLReportEnum;

public class Messages {
    public static final String BAD_REQUEST_NULL = "The input values must not be null!";

    public static final String BAD_REQUEST_PARAMETER_EMPTY_EMAIL = "Recipient email address cannot be 'null' or empty!";
    public static final String BAD_REQUEST_PARAMETER_EMPTY_REPORT = "Report name cannot be 'null' or empty! Allowed values: [" + SMLReportEnum.namesAsString() + "]";
    public static final String BAD_REQUEST_PARAMETER_INVALID_REPORT = "Invalid report name! Allowed values: [" + SMLReportEnum.namesAsString() + "]";
    public static final String BAD_REQUEST_PARAMETER_EMPTY_CERTIFICATE = "Certificate id cannot be 'null' or empty!";
    public static final String BAD_REQUEST_PARAMETER_EMPTY_SUBDOMAIN = "SubDomain name cannot be 'null' or empty!";
    public static final String BAD_REQUEST_PARAMETER_EMPTY_PROPERTY = "Property cannot be 'null' or empty!";
    public static final String BAD_REQUEST_PARAMETER_EMPTY_REG_EXP = "Regular expression for field [%s] cannot be 'null' or empty!";
    public static final String BAD_REQUEST_PARAMETER_EMPTY_URL_SCHEME = "Url scheme cannot be 'null' or empty! Valid values: http, https, all.";
    public static final String BAD_REQUEST_PARAMETER_EMPTY_DNS_NAME = "DNS name cannot be 'null' or empty!";
    public static final String BAD_REQUEST_PARAMETER_EMPTY_DNS_VALUE = "DNS value cannot be 'null' or empty!";
    public static final String BAD_REQUEST_PARAMETER_EMPTY_DNS_RT = "DNS record type cannot be 'null' or empty! Valid values: cname, naptr, all.";
    public static final String BAD_REQUEST_PARAMETER_EMPTY_DNS_ENTRY_RT = "DNS entry type cannot be 'null' or empty! Supported values: A, CNAME, NAPTR.";
    public static final String BAD_REQUEST_PARAMETER_EMPTY_DNSZONE = "DNS Zone cannot be 'null' or empty!";
    public static final String BAD_REQUEST_PARAMETER_EMPTY_PUBLIC_KEY = "Certificate Public Key cannot be 'null' or empty!";
    public static final String BAD_REQUEST_PARAMETER_EMPTY_DNS_SERVICE = "DNS Service for NAPTR record cannot be 'null' or empty!";
    public static final String BAD_REQUEST_PARAMETER_EMPTY = "Parameter [%s] cannot be 'null' or empty!";
    public static final String BAD_REQUEST_PARAMETERS_EMPTY = "The input values must not be null";


    public static final String BAD_REQUEST_PARAMETER_INVALID_EMAIL = "Email address [%s] is not valid!";
    public static final String BAD_REQUEST_PARAMETER_INVALID_PROPERTY = "Invalid property!";
    public static final String BAD_REQUEST_PARAMETER_INVALID_CERT_ID = "Invalid domain certificate id! Certificate id must be valid LDAP DN.";
    public static final String BAD_REQUEST_PARAMETER_INVALID_URL_SCHEME = "Invalid url scheme! Valid values: http, https, all.";
    public static final String BAD_REQUEST_PARAMETER_INVALID_DNS_RT = "Invalid DNS record type! Valid values: cname, naptr, all.";
    public static final String BAD_REQUEST_PARAMETER_INVALID_DNS_ENTRY_RT = "Invalid DNS entry type! Supported values: A, CNAME, NAPTR.l.";
    public static final String BAD_REQUEST_PARAMETER_INVALID_REG_EXP = "Invalid regular expression for field [%s]!";
    public static final String BAD_REQUEST_PARAMETER_INVALID_DNSZONE = "Invalid DNS zone!";
    public static final String BAD_REQUEST_PARAMETER_INVALID_SUBDOMAIN = "Invalid SubDomain name!";
    public static final String BAD_REQUEST_PARAMETER_INVALID_DNS_NAME = "Invalid Domain name!";
    public static final String BAD_REQUEST_PARAMETER_INVALID_MAX_PARTICIPANT_COUNT_SMP = "Maximum number of participants for the SMP must be 'null' or an integer number greater than 0!";
    public static final String BAD_REQUEST_PARAMETER_INVALID_MAX_PARTICIPANT_COUNT_DOMAIN = "Maximum number of participants for the Domain must be 'null' or an integer number greater than 0!";
    public static final String BAD_REQUEST_PARAMETER_INVALID_CERT_ID_CURRENTLY_IN_USE = "Invalid domain certificate id! Certificate is currently being used so cannot be deleted!";
    public static final String BAD_REQUEST_PARAMETER_INVALID_ENUM = "Unknown [%s]: [%s].Valid values are: [%s]!";
    public static final String BAD_REQUEST_PARAMETER_INVALID_PARTICIPANTS_EQUALS = "All the participant identifiers must be different!";
    public static final String BAD_REQUEST_PARAMETER_INVALID_MAX_PARTICIPANT_COUNT = "A maximum of [%s] participants is allowed!";

    public static final String BAD_REQUEST_PARAMETER_NOT_EXISTS_SUBDOMAIN = "SubDomain does not exist: ";
    public static final String BAD_REQUEST_PARAMETER_PARTICIPANT_EXISTS_SUBDOMAIN = "The participant identifier [%s] with scheme: [%s] already exist on subdomain: [%s]";
    public static final String BAD_REQUEST_PARAMETER_PARTICIPANT_EXISTS_DNS = "The participant identifier [%s] with scheme: [%s] already exist in DNS: [%s]";

    public static final String BAD_REQUEST_PARAMETER_EXIST_SUBDOMAIN = "SubDomain already exists!";
    public static final String BAD_REQUEST_PARAMETER_INCONSISTEN_DOMAIN_CERT = "Only non root domain certificate can be flagged as admin!";
    public static final String BAD_REQUEST_DNS_SERVER_NOT_ENABLED = "DNS server is not enabled!";

    public static final String BAD_REQUEST_PARAMETER_INVALID_PUBLISHER_ID = "The Publisher identifier [%s] doesn't exist";
}
