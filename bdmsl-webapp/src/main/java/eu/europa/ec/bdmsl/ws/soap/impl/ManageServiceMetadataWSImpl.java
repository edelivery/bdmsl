/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.ws.soap.impl;

import eu.europa.ec.bdmsl.common.bo.ServiceMetadataPublisherBO;
import eu.europa.ec.bdmsl.common.exception.BadRequestException;
import eu.europa.ec.bdmsl.common.exception.Severity;
import eu.europa.ec.bdmsl.common.util.LogEvents;
import eu.europa.ec.bdmsl.service.IManageServiceMetadataService;
import eu.europa.ec.bdmsl.util.JaxbUtils;
import eu.europa.ec.bdmsl.ws.soap.*;
import org.busdox.servicemetadata.locator._1.ServiceMetadataPublisherServiceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebResult;
import jakarta.jws.WebService;
import jakarta.jws.soap.SOAPBinding;

import static eu.europa.ec.bdmsl.common.exception.Messages.BAD_REQUEST_PARAMETERS_EMPTY;

/**
 * @author Adrien FERIAL
 * @since 12/06/2015
 */
@Service
@WebService(targetNamespace = "http://busdox.org/serviceMetadata/ManageServiceMetadataService/1.0/", name = "ManageServiceMetadataServiceSoap", endpointInterface = "eu.europa.ec.bdmsl.ws.soap.IManageServiceMetadataWS")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
public class ManageServiceMetadataWSImpl extends AbstractWSImpl implements IManageServiceMetadataWS {

    @Autowired
    private IManageServiceMetadataService manageServiceMetadataService;

    @Autowired
    private ConversionService conversionService;

    @Override
    @WebResult(name = "ServiceMetadataPublisherService", targetNamespace = "http://busdox.org/serviceMetadata/locator/1.0/", partName = "messagePart")
    @WebMethod(operationName = "Read", action = "http://busdox.org/serviceMetadata/ManageServiceMetadataService/1.0/:readIn")
    public ServiceMetadataPublisherServiceType read(
            @WebParam(partName = "smp", name = "ReadServiceMetadataPublisherService", targetNamespace = "http://busdox.org/serviceMetadata/locator/1.0/")
            ServiceMetadataPublisherServiceType smp
    ) throws InternalErrorFault, BadRequestFault, UnauthorizedFault, NotFoundFault {

        ServiceMetadataPublisherServiceType result = null;
        try {
            loggingService.info("Calling ManageServiceMetadataWSImpl.read with smp=" + JaxbUtils.serializeJaxbEntity(smp));
            if (smp == null) {
                throw new BadRequestException(BAD_REQUEST_PARAMETERS_EMPTY);
            }
            String serviceMetadataPublisherID = smp.getServiceMetadataPublisherID();
            ServiceMetadataPublisherBO resultBO = manageServiceMetadataService.read(serviceMetadataPublisherID);
            result = conversionService.convert(resultBO, ServiceMetadataPublisherServiceType.class);
            loggingService.businessLog(LogEvents.BUS_SMP_READ, resultBO.getSmpId());
        } catch (Exception exc) {
            loggingService.businessLog(Severity.ERROR, LogEvents.BUS_SMP_READ_FAILED, JaxbUtils.serializeJaxbEntity(smp));
            loggingService.error(exc.getMessage(), exc);
            // convert the exception to the associated SOAP fault
            handleException(exc);
        }
        return result;
    }

    @Override
    @WebMethod(operationName = "Create", action = "http://busdox.org/serviceMetadata/ManageServiceMetadataService/1.0/:createIn")
    public void create(
            @WebParam(partName = "smp", name = "CreateServiceMetadataPublisherService", targetNamespace = "http://busdox.org/serviceMetadata/locator/1.0/")
            ServiceMetadataPublisherServiceType smp
    ) throws InternalErrorFault, BadRequestFault, UnauthorizedFault {
        loggingService.info("Calling ManageServiceMetadataWSImpl.create with smp=" + JaxbUtils.serializeJaxbEntity(smp));
        try {
            if (smp == null) {
                throw new BadRequestException(BAD_REQUEST_PARAMETERS_EMPTY);
            }
            ServiceMetadataPublisherBO smpBo = conversionService.convert(smp, ServiceMetadataPublisherBO.class);
            manageServiceMetadataService.create(smpBo);
            loggingService.businessLog(LogEvents.BUS_SMP_CREATED, smpBo.getSmpId());
        } catch (Exception exc) {
            // convert the exception to the associated SOAP fault
            try {
                loggingService.businessLog(Severity.ERROR, LogEvents.BUS_SMP_CREATION_FAILED, JaxbUtils.serializeJaxbEntity(smp));
                loggingService.error(exc.getMessage(), exc);
                handleException(exc);
            } catch (NotFoundFault notFoundFault) {
                // never happens
                throw new InternalErrorFault(RequestContextHolder.currentRequestAttributes().getSessionId(), notFoundFault.getFaultInfo(), notFoundFault);
            }
        }
    }

    @Override
    @WebMethod(operationName = "Delete", action = "http://busdox.org/serviceMetadata/ManageServiceMetadataService/1.0/:deleteIn")
    public void delete(
            @WebParam(partName = "smp", name = "ServiceMetadataPublisherID", targetNamespace = "http://busdox.org/serviceMetadata/locator/1.0/")
            String smp
    ) throws InternalErrorFault, BadRequestFault, UnauthorizedFault, NotFoundFault {
        loggingService.info("Calling ManageServiceMetadataWSImpl.delete with smp=" + smp);
        try {
            manageServiceMetadataService.delete(smp);
            loggingService.businessLog(LogEvents.BUS_SMP_DELETED, smp);
        } catch (Exception exc) {
            // convert the exception to the associated SOAP fault
            loggingService.businessLog(Severity.ERROR, LogEvents.BUS_SMP_DELETION_FAILED, JaxbUtils.serializeJaxbEntity(smp));
            loggingService.error(exc.getMessage(), exc);
            handleException(exc);
        }
    }

    @Override
    @WebMethod(operationName = "Update", action = "http://busdox.org/serviceMetadata/ManageServiceMetadataService/1.0/:updateIn")
    public void update(
            @WebParam(partName = "smp", name = "UpdateServiceMetadataPublisherService", targetNamespace = "http://busdox.org/serviceMetadata/locator/1.0/")
            ServiceMetadataPublisherServiceType smp
    ) throws InternalErrorFault, BadRequestFault, UnauthorizedFault, NotFoundFault {

        try {
            loggingService.info("Calling ManageServiceMetadataWSImpl.update with smp=" + JaxbUtils.serializeJaxbEntity(smp));
            if (smp == null) {
                throw new BadRequestException(BAD_REQUEST_PARAMETERS_EMPTY);
            }
            ServiceMetadataPublisherBO smpBO = conversionService.convert(smp, ServiceMetadataPublisherBO.class);
            manageServiceMetadataService.update(smpBO);
            loggingService.businessLog(LogEvents.BUS_SMP_UPDATED, smpBO.getSmpId());
        } catch (Exception exc) {
            // convert the exception to the associated SOAP fault
            loggingService.businessLog(Severity.ERROR, LogEvents.BUS_SMP_UPDATE_FAILED, JaxbUtils.serializeJaxbEntity(smp));
            loggingService.error(exc.getMessage(), exc);
            handleException(exc);
        }
    }

}

