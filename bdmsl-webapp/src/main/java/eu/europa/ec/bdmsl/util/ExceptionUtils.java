/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.util;

import eu.europa.ec.bdmsl.ws.soap.BadRequestFault;
import eu.europa.ec.bdmsl.ws.soap.InternalErrorFault;
import eu.europa.ec.bdmsl.ws.soap.UnauthorizedFault;
import org.busdox.servicemetadata.locator._1.FaultType;
import org.busdox.servicemetadata.locator._1.ObjectFactory;
import org.springframework.web.context.request.RequestContextHolder;

/**
 * @author Flavio SANTOS
 * @since 16/02/2017
 */
public class ExceptionUtils {

    public static UnauthorizedFault buildUnauthorizedFault(String message) {
        return new UnauthorizedFault(message + " [" + getSessionId() + "]", createFaultType(message));
    }

    public static BadRequestFault buildBadRequestFault(String message) {
        return new BadRequestFault(message + " [" + getSessionId() + "]", createFaultType(message));
    }

    public static InternalErrorFault buildInternalErrorFault(String message) {
        return new InternalErrorFault(message + " [" + getSessionId() + "]", createFaultType(message));
    }

    private static FaultType createFaultType(String message) {
        final ObjectFactory objectFactory = new ObjectFactory();
        final FaultType faultInfo = objectFactory.createFaultType();
        faultInfo.setFaultMessage(message);

        return faultInfo;
    }

    private static String getSessionId() {
        return RequestContextHolder.currentRequestAttributes().getSessionId();
    }
}
