/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.ws.soap.impl;


import eu.europa.ec.bdmsl.common.exception.*;
import eu.europa.ec.bdmsl.common.logging.ILoggingService;
import eu.europa.ec.bdmsl.ws.soap.BadRequestFault;
import eu.europa.ec.bdmsl.ws.soap.InternalErrorFault;
import eu.europa.ec.bdmsl.ws.soap.NotFoundFault;
import eu.europa.ec.bdmsl.ws.soap.UnauthorizedFault;
import org.busdox.servicemetadata.locator._1.FaultType;
import org.busdox.servicemetadata.locator._1.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.context.request.RequestContextHolder;


/**
 * @author Adrien FERIAL
 * @since 12/06/2015
 */
public abstract class AbstractWSImpl {

    @Autowired
    protected ILoggingService loggingService;

    protected void handleExceptionExcludingNotFoundFault(final Exception e) throws
            UnauthorizedFault,
            BadRequestFault,
            InternalErrorFault {
        try {
            handleException(e);
        } catch (NotFoundFault notFoundFault) {
            throw new InternalErrorFault(RequestContextHolder.currentRequestAttributes().getSessionId(), notFoundFault.getFaultInfo(), notFoundFault);
        }
    }


    protected void handleException(final Exception e) throws NotFoundFault,
            UnauthorizedFault,
            BadRequestFault,
            InternalErrorFault {


        final ObjectFactory objectFactory = new ObjectFactory();
        String sessionId = RequestContextHolder.currentRequestAttributes().getSessionId();
        if (e instanceof BadRequestException) {
            final FaultType faultInfo = objectFactory.createFaultType();
            faultInfo.setFaultMessage(e.getMessage());
            throw new BadRequestFault(e.getMessage() + " [" + sessionId + "]", faultInfo, e);
        } else if (e instanceof NotFoundException) {
            final FaultType faultInfo = objectFactory.createFaultType();
            faultInfo.setFaultMessage(e.getMessage());
            throw new NotFoundFault(e.getMessage() + " [" + sessionId + "]", faultInfo, e);
        } else if (e instanceof SmpNotFoundException) {
            final FaultType faultInfo = objectFactory.createFaultType();
            faultInfo.setFaultMessage(e.getMessage());
            throw new NotFoundFault(e.getMessage() + " [" + sessionId + "]", faultInfo, e);
        } else if (e instanceof ParticipantNotFoundException) {
            final FaultType faultInfo = objectFactory.createFaultType();
            faultInfo.setFaultMessage(e.getMessage());
            throw new NotFoundFault(e.getMessage() + " [" + sessionId + "]", faultInfo, e);
        } else if (e instanceof MigrationNotFoundException) {
            final FaultType faultInfo = objectFactory.createFaultType();
            faultInfo.setFaultMessage(e.getMessage());
            throw new NotFoundFault(e.getMessage() + " [" + sessionId + "]", faultInfo, e);
        } else if (e instanceof UnauthorizedException) {
            final FaultType faultInfo = objectFactory.createFaultType();
            faultInfo.setFaultMessage(e.getMessage());
            throw new UnauthorizedFault(e.getMessage() + " [" + sessionId + "]", faultInfo, e);
        } else if (e instanceof CertificateAuthenticationException) {
            final FaultType faultInfo = objectFactory.createFaultType();
            faultInfo.setFaultMessage(e.getMessage());
            throw new UnauthorizedFault(e.getMessage() + " [" + sessionId + "]", faultInfo, e);
        } else if (e instanceof MigrationPlannedException) {
            final FaultType faultInfo = objectFactory.createFaultType();
            faultInfo.setFaultMessage(e.getMessage());
            throw new UnauthorizedFault(e.getMessage() + " [" + sessionId + "]", faultInfo, e);
        } else if (e instanceof AccessDeniedException) {
            final FaultType faultInfo = objectFactory.createFaultType();
            faultInfo.setFaultMessage(e.getMessage());
            throw new UnauthorizedFault(e.getMessage() + " [" + sessionId + "]", faultInfo, e);
        } else if (e instanceof CertificateNotFoundException) {
            final FaultType faultInfo = objectFactory.createFaultType();
            faultInfo.setFaultMessage(e.getMessage());
            throw new UnauthorizedFault(e.getMessage() + " [" + sessionId + "]", faultInfo, e);
        } else if (e instanceof CertificateNotYetValidException) {
            final FaultType faultInfo = objectFactory.createFaultType();
            faultInfo.setFaultMessage(e.getMessage());
            throw new BadRequestFault(e.getMessage() + " [" + sessionId + "]", faultInfo, e);
        } else if (e instanceof CertificateExpiredException) {
            final FaultType faultInfo = objectFactory.createFaultType();
            faultInfo.setFaultMessage(e.getMessage());
            throw new BadRequestFault(e.getMessage() + " [" + sessionId + "]", faultInfo, e);
        } else if (e instanceof ConfigurationException || e instanceof TechnicalException) {
            final FaultType faultInfo = objectFactory.createFaultType();
            faultInfo.setFaultMessage(e.getMessage());
            throw new InternalErrorFault(e.getMessage() + " [" + sessionId + "]", faultInfo, e);
        } else {
            // All others as internal errors
            final FaultType faultInfo = objectFactory.createFaultType();
            faultInfo.setFaultMessage("Internal error");
            throw new InternalErrorFault("Internal error" + " [" + sessionId + "]", faultInfo, e);
        }
    }
}
