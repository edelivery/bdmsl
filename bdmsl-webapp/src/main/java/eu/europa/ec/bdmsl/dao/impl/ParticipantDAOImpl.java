/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.impl;

import com.google.common.base.Strings;
import eu.europa.ec.bdmsl.business.IIdentifierFormatterBusiness;
import eu.europa.ec.bdmsl.common.bo.PageRequestBO;
import eu.europa.ec.bdmsl.common.bo.ParticipantBO;
import eu.europa.ec.bdmsl.common.bo.ServiceMetadataPublisherBO;
import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import eu.europa.ec.bdmsl.common.enums.DNSSubSomainRecordTypeEnum;
import eu.europa.ec.bdmsl.common.exception.BadRequestException;
import eu.europa.ec.bdmsl.common.exception.GenericTechnicalException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.dao.AbstractDAOImpl;
import eu.europa.ec.bdmsl.dao.IParticipantDAO;
import eu.europa.ec.bdmsl.dao.entity.ParticipantIdentifierEntity;
import eu.europa.ec.bdmsl.dao.entity.SmpEntity;
import eu.europa.ec.bdmsl.dao.entity.reports.InconsistencyDbCNameEntity;
import eu.europa.ec.bdmsl.dao.entity.reports.InconsistencyDbNaptrEntity;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.hibernate.*;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.xbill.DNS.IRDnsRangeData;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceException;
import jakarta.persistence.TypedQuery;
import java.util.*;
import java.util.stream.Collectors;

import static eu.europa.ec.bdmsl.dao.QueryNames.*;


/**
 * Participant identifier DAO service implementation!
 *
 * @author Adrien FERIAL
 * @since 3.0
 */
@Repository
public class ParticipantDAOImpl extends AbstractDAOImpl implements IParticipantDAO {
    // internal session JDBC batch size
    private static final int JDBC_BATCH_SIZE = 20;

    public static final String NO_ENTRY_FOUND_FOR_PARTICIPANT = "No entry found for participant ";

    protected final IIdentifierFormatterBusiness identifierFormatterBusiness;

    @Autowired
    private ConversionService conversionService;

    public ParticipantDAOImpl(IIdentifierFormatterBusiness identifierFormatterBusiness) {
        this.identifierFormatterBusiness = identifierFormatterBusiness;
    }

    @Override
    public void createParticipant(ParticipantBO participantBO) throws TechnicalException {
        long l = Calendar.getInstance().getTimeInMillis();
        loggingService.debug((Calendar.getInstance().getTimeInMillis() - l) + " createParticipant:  " + participantBO.getParticipantId());
        // validation
        Optional<SmpEntity> smp = getSMPByName(participantBO.getSmpId());
        if (!smp.isPresent()) {
            throw new GenericTechnicalException(String.format("Error occurred while creating participant id [%s] and scheme [%s]. Missing smp [%s]",
                    participantBO.getParticipantId(), participantBO.getScheme(), participantBO.getSmpId()));
        }

        // map the business object to a JPA entity
        ParticipantIdentifierEntity participantIdentifierEntity = conversionService.convert(participantBO, ParticipantIdentifierEntity.class);
        // calculate hashes.
        updateHashValuesForParticipant(participantIdentifierEntity);
        // now associate the SMP entity
        participantIdentifierEntity.setSmp(smp.get());
        try {
            validateParticipantBeforePersist(participantIdentifierEntity);
            super.persist(participantIdentifierEntity);
        } catch (PersistenceException exc) {
            loggingService.error("Error occurred while adding participant " + participantIdentifierEntity, exc);
            String message = ExceptionUtils.getRootCauseMessage(exc);
            if (StringUtils.containsIgnoreCase(message, "Unique index or primary key violation")) {
                throw new BadRequestException("The participant identifier '" + participantBO.getParticipantId() + "' with scheme: '" + participantBO.getScheme() + "' already exist on subdomain: '" + smp.get().getSubdomain().getSubdomainName() + "'");
            } else {
                throw new GenericTechnicalException(String.format("Error occurred while creating participant id [%s::%s]. Cause: [%s]",
                        participantBO.getScheme(), participantBO.getParticipantId(), ExceptionUtils.getRootCauseMessage(exc)));
            }

        }
        loggingService.debug((Calendar.getInstance().getTimeInMillis() - l) + " persisted:  " + participantBO.getParticipantId());
    }

    @Override
    public ParticipantBO findParticipant(ParticipantBO participantBO) throws TechnicalException {
        ParticipantBO resultBO = null;

        // get participant for domain
        Optional<SmpEntity> smp = getSMPByName(participantBO.getSmpId());
        if (smp.isPresent()) {
            Optional<ParticipantIdentifierEntity> optPartEntity = getParticipantForDomain(
                    participantBO.getParticipantId(),
                    participantBO.getScheme(),
                    smp.get().getSubdomain().getSubdomainId());
            resultBO = optPartEntity.map(participantIdentifierEntity
                            -> conversionService.convert(participantIdentifierEntity, ParticipantBO.class))
                    .orElse(null);
        } else {

            String namedQuery = identifierFormatterBusiness.isSchemeNotCaseSensitive(participantBO.getScheme())
                    ? PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_IDENTIFIER : PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_CASE_SENSITIVE_IDENTIFIER;
            // find participant only by participant id and scheme, because  the smp and domain validation should be done later
            TypedQuery<ParticipantIdentifierEntity> query = getEntityManager().createNamedQuery(namedQuery,
                            ParticipantIdentifierEntity.class)
                    .setParameter(PARTICIPANT_ID, participantBO.getParticipantId())
                    .setParameter(SCHEME, participantBO.getScheme()
                    );

            List<ParticipantIdentifierEntity> results = query.getResultList();
            ParticipantIdentifierEntity resultEntity;


            if (!results.isEmpty()) {
                resultEntity = results.get(0);
                resultBO = conversionService.convert(resultEntity, ParticipantBO.class);
            } else {
                loggingService.info(NO_ENTRY_FOUND_FOR_PARTICIPANT + participantBO);
            }

        }
        return resultBO;
    }

    @Override
    public Optional<ParticipantBO> getParticipantForDomain(ParticipantBO participantBO, SubdomainBO subdomainBO) throws TechnicalException {
        Optional<ParticipantIdentifierEntity> result = getParticipantForDomain(participantBO.getParticipantId(), participantBO.getScheme(), subdomainBO.getSubdomainId());

        if (result.isPresent()) {
            return Optional.of(conversionService.convert(result.get(), ParticipantBO.class));
        } else {
            loggingService.info(NO_ENTRY_FOUND_FOR_PARTICIPANT + participantBO);
            return Optional.empty();
        }
    }

    @Override
    public Optional<ParticipantBO> getParticipantForDifferentSMPOnDomain(ParticipantBO participantBO, ServiceMetadataPublisherBO smpBO) throws TechnicalException {
        Optional<ParticipantIdentifierEntity> result = getParticipantForDifferentSMPOnDomain(participantBO.getParticipantId(), participantBO.getScheme(), smpBO.getSmpId(), smpBO.getSubdomain().getSubdomainId());

        if (result.isPresent()) {
            return Optional.of(conversionService.convert(result.get(), ParticipantBO.class));
        } else {
            loggingService.info(NO_ENTRY_FOUND_FOR_PARTICIPANT + participantBO);
            return Optional.empty();
        }
    }

    /**
     * Method removes participant(s) from the database. Participant must match by the participant identfifer and SMP id!
     *
     * @param participantBO participant to be removed from the database
     * @throws TechnicalException if an error occurs
     */
    @Override
    public void deleteParticipant(ParticipantBO participantBO) throws TechnicalException {
        String namedQuery = identifierFormatterBusiness.isSchemeNotCaseSensitive(participantBO.getScheme())
                ? PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_IDENTIFIER_AND_SMP : PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_CASE_SENSITIVE_IDENTIFIER_AND_SMP;

        TypedQuery<ParticipantIdentifierEntity> query = getEntityManager().createNamedQuery(namedQuery,
                        ParticipantIdentifierEntity.class)
                .setParameter(PARTICIPANT_ID, participantBO.getParticipantId())
                .setParameter(SCHEME, participantBO.getScheme())
                .setParameter(SMP_ID, participantBO.getSmpId());
        List<ParticipantIdentifierEntity> results = query.getResultList();
        if (results.isEmpty()) {
            loggingService.info("Can not remove participant because it does not exist! Participant: " + participantBO);
        } else {
            results.forEach(participantIdentifierEntity -> {
                loggingService.info("Remove participant: " + participantIdentifierEntity);
                getEntityManager().remove(participantIdentifierEntity);
            });
        }
    }

    public List<ParticipantBO> listParticipant(PageRequestBO pageRequestBO, int participantPerPageCount) throws TechnicalException {

        // The NextPageIdentifier starts with 1
        int page = 0;
        if (!Strings.isNullOrEmpty(pageRequestBO.getPage())) {
            page = Integer.parseInt(pageRequestBO.getPage()) - 1;
        }

        TypedQuery<ParticipantIdentifierEntity> query = getEntityManager().createNamedQuery(PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_SMP_IDENTIFIER, ParticipantIdentifierEntity.class)
                .setParameter(SMP_ID, pageRequestBO.getSmpId())
                .setFirstResult(page * participantPerPageCount)
                .setMaxResults(participantPerPageCount);

        List<ParticipantIdentifierEntity> resultList = query.getResultList();
        return resultList.stream().map(participantIdentifierEntity ->
                conversionService.convert(participantIdentifierEntity, ParticipantBO.class)).collect(Collectors.toList());

    }

    @Override
    public void updateParticipant(ParticipantBO participantBO, final String oldSmpId) throws TechnicalException {
        String namedQuery = identifierFormatterBusiness.isSchemeNotCaseSensitive(participantBO.getScheme())
                ? PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_IDENTIFIER_AND_SMP : PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_CASE_SENSITIVE_IDENTIFIER_AND_SMP;

        TypedQuery<ParticipantIdentifierEntity> query = getEntityManager().createNamedQuery(namedQuery,
                        ParticipantIdentifierEntity.class)
                .setParameter(PARTICIPANT_ID, participantBO.getParticipantId())
                .setParameter(SCHEME, participantBO.getScheme())
                .setParameter(SMP_ID, oldSmpId);
        ParticipantIdentifierEntity participantIdentifierEntity = query.getSingleResult();

        // now associate the SMP entity
        TypedQuery<SmpEntity> querySMP = getEntityManager().createNamedQuery(SMP_ENTITY_GET_BY_SMP_ID, SmpEntity.class);
        querySMP.setParameter(SMP_ID, participantBO.getSmpId());
        SmpEntity newSmpEntity = querySMP.getSingleResult();

        // migration can be done only to the same domain from one SMP to another SMP.
        if (participantIdentifierEntity.getSmp().getSubdomain().getSubdomainId() != newSmpEntity.getSubdomain().getSubdomainId()) {
            throw new GenericTechnicalException(String.format("Error occurred while migration the participant id [%s::%s] from smp [%s] to new smp [%s]. Participant can be migrated only " +
                            "within the same domain!",
                    participantIdentifierEntity.getScheme(), participantIdentifierEntity.getParticipantId(),
                    oldSmpId, newSmpEntity.getSmpId()));
        }

        participantIdentifierEntity.setSmp(newSmpEntity);

        super.merge(participantIdentifierEntity);
    }

    @Override
    public List<ParticipantBO> getAllParticipants(){
        List<ParticipantIdentifierEntity> resultList = getEntityManager().createNamedQuery(PARTICIPANT_IDENTIFIER_ENTITY_GET_ALL, ParticipantIdentifierEntity.class)
                .getResultList();
        return resultList.stream().map(participantIdentifierEntity ->
                conversionService.convert(participantIdentifierEntity, ParticipantBO.class)).collect(Collectors.toList());
    }


    @Override
    public List<ParticipantBO> getAllParticipantsForSMP(ServiceMetadataPublisherBO smpBO) {
      return getParticipantsForSMP(smpBO, -1, -1);
    }

    @Override
    public List<ParticipantBO> getParticipantsForSMP(ServiceMetadataPublisherBO smpBO, int iPage, int iPageSize) {
        TypedQuery<ParticipantIdentifierEntity> query = getEntityManager().createNamedQuery(PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_SMP_IDENTIFIER, ParticipantIdentifierEntity.class)
                .setParameter(SMP_ID, smpBO.getSmpId());
        if (iPageSize> -1 && iPage >-1 ) {
            query.setFirstResult(iPage * iPageSize);
        }
        if (iPageSize > 0) {
            query.setMaxResults(iPageSize);
        }
        List<ParticipantIdentifierEntity> resultList = query.getResultList();

        return resultList.stream().map(participantIdentifierEntity ->
                conversionService.convert(participantIdentifierEntity, ParticipantBO.class)).collect(Collectors.toList());
    }

    @Override
    public List<ParticipantBO> findParticipants(String smpId, Map<String, List<String>> mapParticipants) throws TechnicalException {
        List<ParticipantBO> resultBOList = new ArrayList<>();
        for (String scheme : mapParticipants.keySet()) {
            Collection<String> participantIds = toUpperCase(mapParticipants, scheme);

            List<ParticipantIdentifierEntity> resultEntityList = getEntityManager().createNamedQuery(PARTICIPANT_IDENTIFIER_ENTITY_FIND_BY_IDENTIFIER_LIST_AND_SMP,
                            ParticipantIdentifierEntity.class)
                    .setParameter(SCHEME, scheme)
                    .setParameter(PARTICIPANT_ID_LIST, participantIds)
                    .setParameter(SMP_ID, smpId)
                    .getResultList();
            resultBOList.addAll(resultEntityList.stream().map(participantIdentifierEntity ->
                    conversionService.convert(participantIdentifierEntity, ParticipantBO.class)).collect(Collectors.toList()));

        }
        return resultBOList;
    }

    @Override
    public void deleteParticipants(String smpId, Map<String, List<String>> mapParticipants) throws TechnicalException {
        for (String scheme : mapParticipants.keySet()) {
            Collection<String> participantIds = toUpperCase(mapParticipants, scheme);
            // do user delete sql query else envers will not catch the change
            List<ParticipantIdentifierEntity> resultEntityList = getEntityManager().createNamedQuery(PARTICIPANT_IDENTIFIER_ENTITY_FIND_BY_IDENTIFIER_LIST_AND_SMP,
                            ParticipantIdentifierEntity.class)
                    .setParameter(SCHEME, scheme)
                    .setParameter(PARTICIPANT_ID_LIST, participantIds)
                    .setParameter(SMP_ID, smpId)
                    .getResultList();

            for (ParticipantIdentifierEntity pie : resultEntityList) {
                loggingService.info("Remove participant: " + pie);
                getEntityManager().remove(pie);
            }
        }
    }

    private Collection<String> toUpperCase(Map<String, List<String>> mapParticipants, String scheme) {
        List<String> participants = mapParticipants.get(scheme);
        return participants.stream().map(partcId -> StringUtils.upperCase(partcId)).collect(Collectors.toList());
    }

    private Optional<ParticipantIdentifierEntity> getParticipantForDomain(String participantId, String participantScheme, long domainId) throws TechnicalException {
        String namedQuery = identifierFormatterBusiness.isSchemeNotCaseSensitive(participantScheme)
                ? PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_IDENTIFIER_AND_DOMAIN : PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_CASE_SENSITIVE_IDENTIFIER_AND_DOMAIN;

        TypedQuery<ParticipantIdentifierEntity> query = getEntityManager().createNamedQuery(namedQuery,
                        ParticipantIdentifierEntity.class)
                .setParameter(PARTICIPANT_ID, participantId)
                .setParameter(SCHEME, participantScheme)
                .setParameter(SUBDOMAIN_ID, domainId);
        List<ParticipantIdentifierEntity> results = query.getResultList();
        if (results.size() > 1) {
            throw new GenericTechnicalException(String.format("More than one participant with participant id [%s], scheme [%s] is registered on domain with id [%d] ",
                    participantId, participantScheme, domainId));
        }

        return results.isEmpty() ? Optional.empty() : Optional.of(results.get(0));
    }

    private Optional<ParticipantIdentifierEntity> getParticipantForDifferentSMPOnDomain(String participantId, String participantScheme, String smpId, long domainId) throws TechnicalException {
        TypedQuery<ParticipantIdentifierEntity> query = getEntityManager().createNamedQuery(PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_IDENTIFIER_AND_DOMAIN_AND_NOT_FOR_SMP,
                        ParticipantIdentifierEntity.class)
                .setParameter(PARTICIPANT_ID, participantId)
                .setParameter(SCHEME, participantScheme)
                .setParameter(SMP_ID, smpId)
                .setParameter(SUBDOMAIN_ID, domainId);
        List<ParticipantIdentifierEntity> results = query.getResultList();
        if (results.size() > 1) {
            throw new GenericTechnicalException(String.format("More than one participant with participant id [%s], scheme [%s] is registered on domain with id [%d] ",
                    participantId, participantScheme, domainId));
        }

        return results.isEmpty() ? Optional.empty() : Optional.of(results.get(0));
    }


    private Optional<SmpEntity> getSMPByName(String smpName) {
        TypedQuery<SmpEntity> query = getEntityManager().createNamedQuery(SMP_ENTITY_GET_BY_SMP_ID, SmpEntity.class);
        query.setParameter(SMP_ID, smpName);
        List<SmpEntity> smpEntities = query.getResultList();
        if (smpEntities.isEmpty()) {
            return Optional.empty();
        } else {
            if (smpEntities.size() > 1) {
                loggingService.error("Inconsistent data in database! Found multiple (" + smpEntities.size() + ") value of smp: [" + smpName + "]!", null);
            }
            return Optional.of(smpEntities.get(0));
        }
    }


    @Override
    @Transactional
    public int updateNextBatchHashValuesForParticipants() throws TechnicalException {
        int batchSize = 100;

        EntityManager entityManager = getEntityManager();
        entityManager.clear(); // clear cache
        entityManager.unwrap(Session.class)
                .setJdbcBatchSize(JDBC_BATCH_SIZE);

        TypedQuery<ParticipantIdentifierEntity> query = entityManager.createNamedQuery(PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_NULL_HASH, ParticipantIdentifierEntity.class);
        query.setMaxResults(batchSize);
        List<ParticipantIdentifierEntity> listResult = query.getResultList();

        for (ParticipantIdentifierEntity participantIdentifierEntity : listResult) {
            // update hash values
            updateHashValuesForParticipant(participantIdentifierEntity);
            // persist new values
            entityManager.merge(participantIdentifierEntity);
        }
        return listResult.size();
    }

    @Override
    public List<ParticipantBO> setDisabledNextSMPParticipantsBatch(ServiceMetadataPublisherBO smpBO, int batchSize, boolean disabled) throws TechnicalException {

        EntityManager entityManager = getEntityManager();
        entityManager.clear(); // clear cache
        entityManager.unwrap(Session.class)
                .setJdbcBatchSize(JDBC_BATCH_SIZE);

        TypedQuery<ParticipantIdentifierEntity> query = entityManager.createNamedQuery(PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_SMP_IDENTIFIER_AND_DISABLED,
                ParticipantIdentifierEntity.class);
        query.setParameter(SMP_ID, smpBO.getSmpId());
        query.setParameter(DISABLED, !disabled);
        query.setMaxResults(batchSize);
        List<ParticipantIdentifierEntity> listResult = query.getResultList();

        for (ParticipantIdentifierEntity entity : listResult) {
            entity.setDisabled(disabled);
        }

        return listResult.stream().map(entity -> conversionService.convert(entity, ParticipantBO.class)).collect(Collectors.toList());
    }

    @Override
    public List<ParticipantBO> deleteNextSMPParticipantsBatch(ServiceMetadataPublisherBO smpBO, int batchSize) throws TechnicalException {

        EntityManager entityManager = getEntityManager();
        entityManager.clear(); // clear cache
        entityManager.unwrap(Session.class)
                .setJdbcBatchSize(JDBC_BATCH_SIZE);

        TypedQuery<ParticipantIdentifierEntity> query = entityManager.createNamedQuery(PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_SMP_IDENTIFIER,
                ParticipantIdentifierEntity.class);
        query.setParameter(SMP_ID, smpBO.getSmpId());
        query.setMaxResults(batchSize);
        List<ParticipantIdentifierEntity> listResult = query.getResultList();

        for (ParticipantIdentifierEntity entity : listResult) {
            getEntityManager().remove(entity);
        }


        return listResult.stream().map(entity -> conversionService.convert(entity, ParticipantBO.class)).collect(Collectors.toList());
    }

    /**
     * Method is intended to be used for NAPTR records update for SMP with large number of participants. The purpose of the method
     * is to move participants in batches from one oldSMP  instance to new SMP with updated logical address. For the returned batch naptr records should be updated with new logical address.
     *
     * @param smpToUpdate smp where the participants must be updated with new smp.
     * @param newSMPBO    new smp where the participants are migrated.
     * @param batchSize   batch size for the migration.
     * @return list of migrated participants.
     * @throws TechnicalException if an error occurs
     */
    @Override
    public List<ParticipantBO> updateSMPForNextParticipantsBatchSMP(ServiceMetadataPublisherBO smpToUpdate, ServiceMetadataPublisherBO newSMPBO, int batchSize) throws TechnicalException {
        EntityManager entityManager = getEntityManager();
        // get SMP entity
        TypedQuery<SmpEntity> querySMP = entityManager.createNamedQuery(SMP_ENTITY_GET_BY_SMP_ID,
                SmpEntity.class);
        querySMP.setParameter(SMP_ID, newSMPBO.getSmpId());

        SmpEntity newSmpEntity = querySMP.getSingleResult();

        entityManager.clear(); // clear cache
        entityManager.unwrap(Session.class)
                .setJdbcBatchSize(JDBC_BATCH_SIZE);

        TypedQuery<ParticipantIdentifierEntity> query = entityManager.createNamedQuery(PARTICIPANT_IDENTIFIER_ENTITY_GET_BY_SMP_IDENTIFIER,
                ParticipantIdentifierEntity.class);
        query.setParameter(SMP_ID, smpToUpdate.getSmpId());
        query.setMaxResults(batchSize);
        List<ParticipantIdentifierEntity> listResult = query.getResultList();

        for (ParticipantIdentifierEntity entity : listResult) {
            entity.setSmp(newSmpEntity);
            getEntityManager().persist(entity);
        }

        return listResult.stream().map(entity -> conversionService.convert(entity, ParticipantBO.class)).collect(Collectors.toList());
    }

    protected void updateHashValuesForParticipant(ParticipantIdentifierEntity participantIdentifierEntity) {
        identifierFormatterBusiness.updateHashValuesForParticipant(participantIdentifierEntity);
    }

    @Transactional(readOnly = true)
    public void retrieveCNameDBDataForInconsistencyReport(String publisherName, IRDnsRangeData irDnsRangeData, Date createDateTo) {

        try (StatelessSession session = ((Session) getEntityManager().getDelegate()).getSessionFactory().openStatelessSession()) {
            Query<InconsistencyDbCNameEntity> query = session.createNamedQuery(INCONSISTENCY_DB_CNAME_ENTITY_GET_ALL_DNS_CNAME_RECORDS,
                    InconsistencyDbCNameEntity.class);
            query.setParameter(DNS_ALL_TYPE, DNSSubSomainRecordTypeEnum.ALL.getCode());
            query.setParameter(DNS_CNAME_TYPE, DNSSubSomainRecordTypeEnum.CNAME.getCode());
            query.setParameter(CREATE_DATE, createDateTo);

            org.hibernate.query.Query q = query.unwrap(org.hibernate.query.Query.class);
            q.setReadOnly(true);
            try (ScrollableResults results = q.scroll(ScrollMode.FORWARD_ONLY)) {
                int index = 0;
                while (results.next()) {
                    InconsistencyDbCNameEntity addr = (InconsistencyDbCNameEntity) results.get();
                    if (addr.isDisabled()) {
                        // ignore disabled values
                        irDnsRangeData.incrementDisabledDBEntityCount();
                        continue;
                    }

                    String subDomain = addr.getSubdomain().toLowerCase();
                    String scheme = StringUtils.isEmpty(addr.getScheme()) ? "" : addr.getScheme();
                    String target = addr.getSmpId().toLowerCase().concat(publisherName).concat(subDomain);
                    irDnsRangeData.addParticipantDBEntry(addr.getDnsRecordName().concat("|").concat(target),
                            scheme.concat("::").concat(addr.getParticipantId()));

                    index++;
                    if (index % 100000 == 0) {
                        loggingService.debug("Got cname records:" + index + " Free memory: " + ((double) Runtime.getRuntime().freeMemory() / 1024));
                    }
                }
                loggingService.info("Got " + index + " cname records from db. Free memory: " + ((double) Runtime.getRuntime().freeMemory() / 1024));
            }
            session.close();
        }
    }

    @Transactional(readOnly = true)
    public void retrieveNaptrDBDataForInconsistencyReport(IRDnsRangeData irDnsRangeData, Date createDateTo) {
        try (StatelessSession session = ((Session) getEntityManager().getDelegate()).getSessionFactory().openStatelessSession()) {
            TypedQuery<InconsistencyDbNaptrEntity> query =
                    getEntityManager().createNamedQuery(INCONSISTENCY_DB_NAPTR_ENTITY_GET_ALL_DNS_NAPTR_RECORDS,
                            InconsistencyDbNaptrEntity.class);
            query.setParameter(DNS_ALL_TYPE, DNSSubSomainRecordTypeEnum.ALL.getCode());
            query.setParameter(DNS_NAPTR_TYPE, DNSSubSomainRecordTypeEnum.NAPTR.getCode());
            query.setParameter(CREATE_DATE, createDateTo);

            org.hibernate.query.Query q = query.unwrap(org.hibernate.query.Query.class);
            q.setFetchSize(1000);
            q.setReadOnly(true);
            try (ScrollableResults results = q.scroll(ScrollMode.FORWARD_ONLY)) {
                int index = 0;
                System.out.println("session.isOpen() = " + session.isOpen());

                while (results.next()) {

                    InconsistencyDbNaptrEntity addr = (InconsistencyDbNaptrEntity) results.get();
                    if (addr.isDisabled()) {
                        // ignore disabled values
                        irDnsRangeData.incrementDisabledDBEntityCount();
                        continue;
                    }
                    // scheme can be null
                    String scheme = StringUtils.isEmpty(addr.getScheme()) ? "" : addr.getScheme();
                    irDnsRangeData.addParticipantDBEntry(addr.getDnsRecordName().concat("|").concat(addr.getLogicalAddress()),
                            scheme.concat("::").concat(addr.getParticipantId()));
                    index++;
                    if (index % 100000 == 0) {
                        loggingService.debug("Processed naptr records: " + index + " Free memory: " + ((double) Runtime.getRuntime().freeMemory() / 1024));
                    }
                }
                loggingService.info("Got " + index + " naptr records from db. Free memory: " + ((double) Runtime.getRuntime().freeMemory() / 1024));
            }
        }
    }

    @Override
    public long getParticipantCount() {
        jakarta.persistence.Query query = getEntityManager().createNamedQuery(PARTICIPANT_IDENTIFIER_ENTITY_GET_PARTICIPANT_COUNT);
        Object obj = query.getSingleResult();
        return castCountDatabaseObjectToLong(obj);
    }

    @Override
    public long getParticipantCountForSMP(String smpID) {
        jakarta.persistence.Query query = getEntityManager().createNamedQuery(PARTICIPANT_IDENTIFIER_ENTITY_GET_PARTICIPANT_COUNT_FOR_SMP);
        query.setParameter(SMP_ID, smpID);
        Object obj = query.getSingleResult();
        return castCountDatabaseObjectToLong(obj);
    }

    @Override
    public long getParticipantCountForSMPAndStatus(String smpID, boolean disabled) {
        jakarta.persistence.Query query = getEntityManager().createNamedQuery(PARTICIPANT_IDENTIFIER_ENTITY_GET_PARTICIPANT_COUNT_FOR_SMP_AND_STATUS);
        query.setParameter(SMP_ID, smpID);
        query.setParameter(DISABLED, disabled);
        Object obj = query.getSingleResult();
        return castCountDatabaseObjectToLong(obj);
    }

    @Override
    public long getParticipantCountForSubDomain(Long subdomain) {
        jakarta.persistence.Query query = getEntityManager().createNamedQuery(PARTICIPANT_IDENTIFIER_ENTITY_GET_PARTICIPANT_COUNT_FOR_SUB_DOMAIN);
        query.setParameter(SUBDOMAIN_ID, subdomain);
        Object obj = query.getSingleResult();
        return castCountDatabaseObjectToLong(obj);
    }


    public long getParticipantCountForParticipantIdentifierAndDomain(String participantId, String schema, long subdomainId) {
        String namedQuery = identifierFormatterBusiness.isSchemeNotCaseSensitive(schema)
                ? PARTICIPANT_IDENTIFIER_ENTITY_GET_PARTICIPANT_COUNT_DOMAIN_ID : PARTICIPANT_IDENTIFIER_ENTITY_GET_CASE_SENSITIVE_PARTICIPANT_COUNT_DOMAIN_ID;

        jakarta.persistence.Query query = getEntityManager().createNamedQuery(namedQuery);
        query.setParameter(SUBDOMAIN_ID, subdomainId);
        query.setParameter(PARTICIPANT_ID, participantId);
        query.setParameter(SCHEME, schema);

        Object obj = query.getSingleResult();
        return castCountDatabaseObjectToLong(obj);
    }

    public long castCountDatabaseObjectToLong(Object databaseNumber) {
        if (databaseNumber == null) {
            return 0;
        }

        if (databaseNumber instanceof Number) {
            return ((Number) databaseNumber).longValue();
        } else {
            throw new IllegalArgumentException("Invalid number type: [" + ClassUtils.getName(databaseNumber) + "]!");
        }
    }

    public void validateParticipantBeforePersist(ParticipantIdentifierEntity entity) throws BadRequestException {
        long cnt = getParticipantCountForParticipantIdentifierAndDomain(entity.getParticipantId(),
                entity.getScheme(),
                entity.getSmp().getSubdomain().getSubdomainId());

        if (cnt > 0) {
            throw new BadRequestException("The participant identifier: ["
                    + entity.getParticipantId() + "] and scheme [" + entity.getScheme() + "] is already registered on domain" +
                    "[" + entity.getSmp().getSubdomain().getSubdomainName() + "]");
        }
    }
}

