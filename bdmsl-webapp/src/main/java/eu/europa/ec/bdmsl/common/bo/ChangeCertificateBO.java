/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.bo;

import java.util.Arrays;

public class ChangeCertificateBO extends AbstractBusinessObject {


    private static final long serialVersionUID = 1L;

    private byte[] publicKey;
    private String serviceMetadataPublisherID;

    public byte[] getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(byte[] publicKey) {
        this.publicKey = publicKey;
    }

    public String getServiceMetadataPublisherID() {
        return serviceMetadataPublisherID;
    }

    public void setServiceMetadataPublisherID(String serviceMetadataPublisherID) {
        this.serviceMetadataPublisherID = serviceMetadataPublisherID;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof ChangeCertificateBO)) {
            return false;
        }

        if (this == o) {
            return true;
        }

        ChangeCertificateBO that = (ChangeCertificateBO) o;

        if (serviceMetadataPublisherID != null ? !serviceMetadataPublisherID.equals(that.serviceMetadataPublisherID) : that.serviceMetadataPublisherID != null)
            return false;

        return Arrays.equals(publicKey, that.publicKey);
    }

    @Override
    public int hashCode() {
        int result = publicKey != null ? Arrays.hashCode(publicKey) : 0;
        result = 31 * result + (serviceMetadataPublisherID != null ? serviceMetadataPublisherID.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ChangeCertificateBO{" +
                ", serviceMetadataPublisherID=" + serviceMetadataPublisherID +
                ", publicKey='" + publicKey + '\'' +
                '}';
    }
}
