/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.config;

import eu.europa.ec.bdmsl.common.enums.SMLPropertyEnum;
import eu.europa.ec.bdmsl.common.exception.ConfigurationException;
import eu.europa.ec.bdmsl.common.exception.KeyException;
import eu.europa.ec.bdmsl.common.util.KeyUtil;
import eu.europa.ec.bdmsl.dao.entity.ConfigurationEntity;
import eu.europa.ec.edelivery.security.utils.KeystoreUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.logging.log4j.core.config.Configurator;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.PersistenceException;
import javax.sql.DataSource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.util.Calendar;
import java.util.Optional;
import java.util.Properties;

import static eu.europa.ec.bdmsl.common.enums.SMLPropertyEnum.*;
import static eu.europa.ec.bdmsl.config.FileProperty.*;
import static java.lang.String.format;

public class PropertyUtils {

    public static final String PROPERTY_VALIDATE_XSD_SCHEME_SERVICEMETADATA = "sml.ws.servicemetadataservices.schema-validation-enabled";
    public static final String PROPERTY_VALIDATE_XSD_SCHEME_PARTICIPANTS = "sml.ws.participantservices.schema-validation-enabled";
    public static final String PROPERTY_VALIDATE_XSD_SCHEME_BDMSLSERVICE = "sml.ws.bdmslservices.schema-validation-enabled";
    public static final String PROPERTY_VALIDATE_XSD_SCHEME_BDMSLADMINSERVICE = "sml.ws.bdmsladminservices.schema-validation-enabled";
    public static final String PROPERTY_VALIDATE_XSD_SCHEME_BDMSLMONITORINGSERVICE = "sml.ws.bdmslmonitoringservices.schema-validation-enabled";

    private static final Log LOG = LogFactory.getLog(PropertyUtils.class);
    public static final String DAO_PACKAGE = "eu.europa.ec.bdmsl.dao.entity";
    private static final String VALID_PW_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()-_=+{}[]|:;<>?,./";
    private static final int DEFAULT_PASSWORD_LENGTH = 16;

    Environment environment;

    public PropertyUtils(Environment environment) {
        this.environment = environment;
    }

    public void updateLog4jConfiguration(String logFileFolder, String logPropertyFile) {
        String logPropertiesFile = "classpath:/sml-log4j2.xml";
        if (!StringUtils.isBlank(logPropertyFile)) {
            File f = new File(logPropertyFile);
            if (f.exists()) {
                logPropertiesFile = f.getAbsolutePath();
                LOG.info("Set log configuration file " + logPropertiesFile + "!");
            } else {
                LOG.warn("Log configuration file is given " + logPropertyFile + " but file not exist!");
            }
        }
        if (!StringUtils.isBlank(logFileFolder)) {
            LOG.info("Set log configuration folder " + logFileFolder + "! " +
                    "This property will not work for custom property file!");
            System.setProperty("sml.log.folder", StringUtils.isBlank(logFileFolder) ? "./logs" : StringUtils.trim(logFileFolder));
        }
        Configurator.initialize(null, logPropertiesFile);
    }

    public void validateMandatoryProperty(Properties prop, SMLPropertyEnum smlPropertyDef) {
        if (!prop.containsKey(smlPropertyDef.getProperty())) {
            LOG.error("Mandatory property: [" + smlPropertyDef.getProperty() + "] is not defined. Set default value: [" + smlPropertyDef.getDefValue() + "]!");
            prop.setProperty(smlPropertyDef.getProperty(), smlPropertyDef.getDefValue());
        }
    }

    public String getSystemProperty(String key, Properties defaultProperty) {
        return environment.getProperty(key, defaultProperty.getProperty(key));
    }

    public Optional<Properties> getDatabaseProperties(Properties fileProp) {

        String dialect = getSystemProperty(FileProperty.PROPERTY_DB_DIALECT, fileProp);
        if (org.apache.commons.lang3.StringUtils.isBlank(dialect)) {
            LOG.warn("The application property: [" + FileProperty.PROPERTY_DB_DIALECT + "] is not set!. Database might not initialize!");
        }
        // get datasource
        DataSource dataSource = getDatasource(fileProp);
        if (dataSource == null) {
            LOG.error("Could not initialize PropertySourcesPlaceholderConfigurer because datasource. Check if datasource is configured on server!");
            return Optional.empty();
        }
        // use entity manager to handle date properties
        EntityManager em = null;
        DatabaseProperties prop = null;
        try {
            em = createEntityManager(dataSource, dialect);
            prop = new DatabaseProperties(em);
            if (prop.size() == 0) {
                initializeProperties(em, fileProp, prop);
            } else {
                validateProperties(em, prop);
            }
        } finally {
            if (em != null && em.isOpen()) {
                em.close();
            }
        }
        return Optional.of(prop);
    }


    /**
     * Use EntityManager to persist date while inserting new properties
     *
     * @param dataSource
     * @return
     */
    private EntityManager createEntityManager(DataSource dataSource, String databaseDialect) {
        Properties prop = new Properties();
        prop.setProperty("hibernate.connection.autocommit", "true");
        if (StringUtils.isNotBlank(databaseDialect)) {
            prop.setProperty("hibernate.dialect", databaseDialect);
        }
        prop.setProperty("org.hibernate.envers.store_data_at_delete", "true");
        prop.setProperty("org.hibernate.envers.audit_table_suffix", "_aud");


        LocalContainerEntityManagerFactoryBean lef = new LocalContainerEntityManagerFactoryBean();
        lef.setEntityManagerFactoryInterface(jakarta.persistence.EntityManagerFactory.class);
        lef.setDataSource(dataSource);

        HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
        hibernateJpaVendorAdapter.setDatabasePlatform(databaseDialect);

        lef.setJpaVendorAdapter(hibernateJpaVendorAdapter);
        lef.setPackagesToScan(DAO_PACKAGE);
        lef.setJpaProperties(prop);
        lef.afterPropertiesSet();
        EntityManagerFactory enf = lef.getObject();
        return enf.createEntityManager();
    }

    /**
     * create datasource to read properties from database
     *
     * @return
     */
    private DataSource getDatasource(Properties defaultProperties) {
        LOG.info("Start database properties");
        DataSource datasource;
        String url = getSystemProperty(FileProperty.PROPERTY_DB_URL, defaultProperties);
        String jndiDatasourceName = getSystemProperty(FileProperty.PROPERTY_DB_JNDI, defaultProperties);
        jndiDatasourceName = org.apache.commons.lang3.StringUtils.isBlank(jndiDatasourceName) ? "java:comp/env/jdbc/edelivery" : jndiDatasourceName;

        if (!org.apache.commons.lang3.StringUtils.isBlank(url)) {
            LOG.info("Connect to [" + url + "].");
            DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
            driverManagerDataSource.setDriverClassName(getSystemProperty(PROPERTY_DB_DRIVER, defaultProperties));
            driverManagerDataSource.setUrl(url);
            driverManagerDataSource.setUsername(getSystemProperty(PROPERTY_DB_USER, defaultProperties));
            driverManagerDataSource.setPassword(getSystemProperty(PROPERTY_DB_TOKEN, defaultProperties));
            datasource = driverManagerDataSource;
        } else {
            LOG.info("Use JNDI [" + jndiDatasourceName + "] to connect to database.");
            JndiObjectFactoryBean dataSource = new JndiObjectFactoryBean();
            dataSource.setJndiName(jndiDatasourceName);
            try {
                dataSource.afterPropertiesSet();
            } catch (IllegalArgumentException | NamingException e) {
                LOG.error("Error occurred while retrieving datasource with JNDI [" + jndiDatasourceName + "]. Is datasource configured in server!");
                throw new ConfigurationException("Error occurred while retrieving datasource: " + jndiDatasourceName, e);
            }
            datasource = (DataSource) dataSource.getObject();
        }

        return datasource;
    }


    /**
     * Method initialize properties from database
     *
     * @param em
     * @param fileProperties
     */
    protected void initializeProperties(EntityManager em, Properties fileProperties, Properties initProperties) {

        String initFolder = fileProperties.getProperty(PROPERTY_INIT_FOLDER, "sml");
        File folder = new File(initFolder);
        if (folder.isFile()) {
            String message = "Can not create init folder [" + folder.getAbsolutePath() + "] because it already exists and is a file!";
            LOG.error(message);
            throw new SMLInitException(message);
        }
        if (!folder.exists()) {
            if (!folder.mkdirs()) {
                String message = "Can not create init folder [" + folder.getAbsolutePath() + "]! (Check filesystem permissions or update property sml.init.folder;)";
                LOG.error(message);
                throw new SMLInitException(message);
            }
        }

        em.getTransaction().begin();
        LOG.info("Database configuration table is empty! initialize new values from property file!");
        initNewValues(em, folder, initProperties);
        // init default property values
        for (SMLPropertyEnum val : SMLPropertyEnum.values()) {
            ConfigurationEntity dbConf = null;
            switch (val) {
                // this properties are already added to database in method initNewValues
                case CONFIGURATION_DIR:
                case KEYSTORE_FILENAME:
                case KEYSTORE_PASSWORD:
                case TRUSTSTORE_FILENAME:
                case TRUSTSTORE_PASSWORD:
                case SIGNATURE_ALIAS:
                case ENCRYPTION_FILENAME:
                case KEYSTORE_PASSWORD_DECRYPTED:
                case TRUSTSTORE_PASSWORD_DECRYPTED:
                    // skip value
                    break;
                default:
                    dbConf = createDBEntry(val.getProperty(), fileProperties.getProperty(val.getProperty(), val.getDefValue()),
                            val.getDesc());
            }
            if (dbConf != null) {
                initProperties.setProperty(dbConf.getProperty(), dbConf.getValue());
                em.persist(dbConf);
            }
        }
        em.getTransaction().commit();
    }

    protected void validateProperties(EntityManager em, Properties properties) {

        // check configuration folder
        em.getTransaction().begin();

        File confFolder = null;
        if (!properties.containsKey(CONFIGURATION_DIR.getProperty())) {
            String confFolderPath = CONFIGURATION_DIR.getDefValue();
            ConfigurationEntity dbConf = createDBEntry(CONFIGURATION_DIR.getProperty(), CONFIGURATION_DIR.getDefValue(),
                    CONFIGURATION_DIR.getDesc());
            em.persist(dbConf);
            confFolder = new File(confFolderPath);
        } else {
            confFolder = new File(properties.getProperty(CONFIGURATION_DIR.getProperty()));
        }
        if (!confFolder.exists() && !confFolder.mkdirs()) {
            LOG.error("Configuration folder [" + confFolder.getAbsolutePath() + "] does not exist" +
                    " and can not created the folder!");
        } else {
            validateConfigurationFiles(em, confFolder, properties);
        }


        // init default property values
        for (SMLPropertyEnum val : SMLPropertyEnum.values()) {
            switch (val) {
                // this properties are already added to database in method initNewValues
                case CONFIGURATION_DIR:
                case KEYSTORE_FILENAME:
                case KEYSTORE_PASSWORD:
                case TRUSTSTORE_FILENAME:
                case TRUSTSTORE_PASSWORD:
                case SIGNATURE_ALIAS:
                case ENCRYPTION_FILENAME:
                case KEYSTORE_PASSWORD_DECRYPTED:
                case TRUSTSTORE_PASSWORD_DECRYPTED:
                    // skip value
                    break;
                default:
                    if (!properties.containsKey(val.getProperty()) && val.isMandatory()) {
                        ConfigurationEntity dbConf = createDBEntry(val.getProperty(), val.getDefValue(),
                                val.getDesc());
                        LOG.info("Add default value [" + val.getDefValue() + "] for property [" + val.getProperty() + "]");
                        em.persist(dbConf);
                        properties.setProperty(dbConf.getProperty(), dbConf.getValue());
                    }
            }
        }
        try {
            em.getTransaction().commit();
        } catch (PersistenceException e) {
            LOG.error("Error occurred while committing transaction: " + e.getMessage() + "!, Skip database property update!", e);
        }
    }

    /**
     * Method initialize new values for configuration dir, encryption filename, keystore password, and keystore filename.
     *
     * @param em
     */
    protected void initNewValues(EntityManager em, File settingsFolder, Properties initProperties) {


        LOG.info("Generate new keystore to folder: " + settingsFolder.getAbsolutePath());

        // add configuration path
        storeDBEntry(em, CONFIGURATION_DIR, settingsFolder.getPath());
        initProperties.setProperty(CONFIGURATION_DIR.getProperty(), settingsFolder.getPath());

        // store encryption filename
        File fEncryption = new File(settingsFolder, SMLPropertyEnum.ENCRYPTION_FILENAME.getDefValue());

        backupFileIfExists(fEncryption);
        LOG.info("Generate new encryption key: " + fEncryption.getName());
        try {
            KeyUtil.generatePrivateSymmetricKey(fEncryption.getAbsolutePath());
        } catch (KeyException e) {
            String msg = "Error occurred while generation private symmetric key: " + e.getMessage();
            throw new SMLInitException(msg, e);
        }
        storeDBEntry(em, SMLPropertyEnum.ENCRYPTION_FILENAME, fEncryption.getName());
        initProperties.setProperty(SMLPropertyEnum.ENCRYPTION_FILENAME.getProperty(), fEncryption.getName());

        // init keystore
        initKeystore(em, settingsFolder, fEncryption, initProperties, KEYSTORE_PASSWORD, KEYSTORE_PASSWORD_DECRYPTED, KEYSTORE_FILENAME);
        // init truststore
        initKeystore(em, settingsFolder, fEncryption, initProperties, TRUSTSTORE_PASSWORD, TRUSTSTORE_PASSWORD_DECRYPTED, TRUSTSTORE_FILENAME);
    }

    protected void validateConfigurationFiles(EntityManager em, File settingsFolder, Properties initProperties) {
        LOG.info("Validate Configuration files: in folder" + settingsFolder.getAbsolutePath());

        String encKeyFile = initProperties.getProperty(SMLPropertyEnum.ENCRYPTION_FILENAME.getProperty(), SMLPropertyEnum.ENCRYPTION_FILENAME.getDefValue());
        // store encryption filename
        File fEncryption = new File(settingsFolder, encKeyFile);
        if (!fEncryption.exists()) {
            LOG.info("Encryption file does not exist generate new encryption key: " + fEncryption.getAbsolutePath());
            try {
                KeyUtil.generatePrivateSymmetricKey(fEncryption.getAbsolutePath());
            } catch (KeyException e) {
                String msg = "Error occurred while generation private symmetric key: " + e.getMessage();
                throw new SMLInitException(msg, e);
            }
        }
        if (!initProperties.containsKey(SMLPropertyEnum.ENCRYPTION_FILENAME.getProperty())) {
            storeDBEntry(em, SMLPropertyEnum.ENCRYPTION_FILENAME, fEncryption.getName());
            initProperties.setProperty(SMLPropertyEnum.ENCRYPTION_FILENAME.getProperty(), fEncryption.getName());
        }
        // init keystore
        validateKeystore(em, settingsFolder, fEncryption, initProperties, KEYSTORE_PASSWORD, KEYSTORE_PASSWORD_DECRYPTED, KEYSTORE_FILENAME);
        // init truststore
        validateKeystore(em, settingsFolder, fEncryption, initProperties, TRUSTSTORE_PASSWORD, TRUSTSTORE_PASSWORD_DECRYPTED, TRUSTSTORE_FILENAME);
    }


    /**
     * Init keystore
     *
     * @param em                  - entity manager for storing new values to databse configuration table
     * @param settingsFolder      - Configuration folder where keystore will be created
     * @param encKey              - encyrption key file for password encryption
     * @param initProperties      - initialized  properties
     * @param passwordProperty    - SMLProperty for password
     * @param passwordDecProperty - SMLProperty for decrypted password
     * @param filenameProperty    - keystore filename property
     */
    public void initKeystore(EntityManager em, File settingsFolder, File encKey, Properties initProperties, SMLPropertyEnum passwordProperty, SMLPropertyEnum passwordDecProperty, SMLPropertyEnum filenameProperty) {

        // init truststore
        String newPassword = generateStrongPassword();
        // store keystore password  filename
        String encPasswd = null;
        try {
            encPasswd = KeyUtil.encrypt(encKey.getAbsolutePath(), newPassword);
        } catch (KeyException e) {
            String msg = "Error occurred while encrypting private symmetric key";
            throw new SMLInitException(msg, e);
        }

        storeDBEntry(em, passwordDecProperty, newPassword);
        storeDBEntry(em, passwordProperty, encPasswd);
        initProperties.setProperty(passwordProperty.getProperty(), encPasswd);

        //store new keystore
        File keystore = new File(settingsFolder, filenameProperty.getDefValue());
        backupFileIfExists(keystore);
        storeDBEntry(em, filenameProperty, keystore.getName());
        initProperties.setProperty(filenameProperty.getProperty(), keystore.getName());

        createNewKeystore(keystore, newPassword);

    }

    /**
     * validate keystore and create it if not  exists
     *
     * @param em                  - entity manager for storing new values to databse configuration table
     * @param settingsFolder      - Configuration folder where keystore will be created
     * @param encKey              - encryption key file for password encryption
     * @param initProperties      - initialized  properties
     * @param passwordProperty    - SMLProperty for password
     * @param passwordDecProperty - SMLProperty for decrypted password
     * @param filenameProperty    - keystore filename property
     */
    public void validateKeystore(EntityManager em, File settingsFolder, File encKey, Properties initProperties, SMLPropertyEnum passwordProperty, SMLPropertyEnum passwordDecProperty, SMLPropertyEnum filenameProperty) {

        if (!initProperties.containsKey(filenameProperty.getProperty())) {
            initKeystore(em, settingsFolder, encKey, initProperties, passwordProperty, passwordDecProperty, filenameProperty);
        }
    }

    public void createNewKeystore(File keystore, String secToken) {

        try (FileOutputStream out = new FileOutputStream(keystore)) {
            KeystoreUtils.createNewKeystore(keystore, secToken);
        } catch (IOException e) {
            throw new SMLInitException("IOException occurred while creating keystore: " + e.getMessage(), e);
        } catch (CertificateException e) {
            throw new SMLInitException("CertificateException occurred while creating keystore: " + e.getMessage(), e);
        } catch (NoSuchAlgorithmException e) {
            throw new SMLInitException("NoSuchAlgorithmException occurred while creating keystore: " + e.getMessage(), e);
        } catch (KeyStoreException e) {
            throw new SMLInitException("KeyStoreException occurred while creating keystore: " + e.getMessage(), e);
        } catch (Exception e) {
            throw new SMLInitException("Exception occurred while creating keystore: " + e.getMessage(), e);
        }
    }


    public void backupFileIfExists(File newFile) {
        if (!newFile.exists()) {
            LOG.info("File: [" + newFile.getAbsolutePath() + "] does not exist!");
            return;
        }
        try {
            int i = 1;
            String fileFormat = newFile.getAbsolutePath() + ".%03d";
            File backupFileTarget = new File(format(fileFormat, i++));

            while (backupFileTarget.exists()) {
                backupFileTarget = new File(format(fileFormat, i++));
            }
            LOG.info("Backup file: [" + newFile.getAbsolutePath() + "] to [" + backupFileTarget.getAbsolutePath() + "] !");
            Files.move(newFile.toPath(), backupFileTarget.toPath(), StandardCopyOption.ATOMIC_MOVE);
        } catch (IOException ex) {
            String msg = "Error occurred while creating the backup of the file [" + newFile.getAbsolutePath() + "]: " + ex.getMessage();
            throw new SMLInitException(msg, ex);
        }
    }

    protected ConfigurationEntity createDBEntry(String key, String value, String desc) {
        ConfigurationEntity dcnew = new ConfigurationEntity();
        dcnew.setProperty(key);
        dcnew.setDescription(desc);
        dcnew.setValue(value);
        dcnew.setLastUpdateDate(Calendar.getInstance());
        dcnew.setCreationDate(Calendar.getInstance());
        return dcnew;
    }

    protected ConfigurationEntity createDBEntry(SMLPropertyEnum prop, String value) {
        return createDBEntry(prop.getProperty(), value, prop.getDesc());
    }

    protected void storeDBEntry(EntityManager em, SMLPropertyEnum prop, String value) {
        ConfigurationEntity cnt = createDBEntry(prop.getProperty(), value, prop.getDesc());
        em.persist(cnt);
    }

    public static String generateStrongPassword() {
        String newKeyPassword = null;

        try {
            newKeyPassword = RandomStringUtils.random(DEFAULT_PASSWORD_LENGTH, 0, VALID_PW_CHARS.length(),
                    false, false,
                    VALID_PW_CHARS.toCharArray(), SecureRandom.getInstanceStrong());

        } catch (NoSuchAlgorithmException e) {
            String msg = "Error occurred while generation test password: No strong random algorithm. Error:"
                    + ExceptionUtils.getRootCauseMessage(e);
            throw new SMLInitException(msg, e);
        }
        return newKeyPassword;
    }

}
