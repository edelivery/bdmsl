/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.impl;

import eu.europa.ec.bdmsl.business.IConfigurationBusiness;
import eu.europa.ec.bdmsl.business.IReportBusiness;
import eu.europa.ec.bdmsl.common.reports.DataInconsistencyReport;
import eu.europa.ec.bdmsl.common.reports.SMPExpiredCertificatesReport;
import eu.europa.ec.bdmsl.common.service.AbstractServiceImpl;
import eu.europa.ec.bdmsl.service.IMailSenderService;
import eu.europa.ec.bdmsl.service.IReportService;
import org.springframework.stereotype.Service;

import static org.apache.commons.lang3.StringUtils.*;

/**
 * Report service implementation.  Currently the class  triggers generation of the report
 * - SMP list With Expired Certificate Report.
 * Add new reports to the class if needed.
 *
 * @author Joze RIHTARSIC
 * @since 4.2
 */
@Service
public class ReportServiceImpl extends AbstractServiceImpl implements IReportService {

    private static final String EMAIL_SUBJECT = "SMP list With Expired Certificate Report";


    private final IConfigurationBusiness configurationBusiness;
    private final IReportBusiness reportBusiness;
    private final IMailSenderService mailSenderService;

    public ReportServiceImpl(IConfigurationBusiness configurationBusiness, IReportBusiness reportBusiness, IMailSenderService mailSenderService) {
        this.configurationBusiness = configurationBusiness;
        this.reportBusiness = reportBusiness;
        this.mailSenderService = mailSenderService;
    }

    @Override
    public void generateSMPsWithExpiredCertificatesReport() {

        String serverAddress = getServerAddress();
        String generateByInstance = configurationBusiness.getSMPExpiredCertReportGenerateByInstance();
        if (isBlank(generateByInstance) || equalsIgnoreCase(trim(generateByInstance), serverAddress)) {

            String senderEmail = configurationBusiness.getSMPExpiredCertReportMailFrom();
            String recipientEmail = configurationBusiness.getSMPExpiredCertReportMailTo();
            generateSMPsWithExpiredCertificatesReport(serverAddress, senderEmail, recipientEmail);

        } else {
            loggingService.info("Generation of SMPsWithExpiredCertificatesReport was blocked on this instance: '" + serverAddress + "'!. Expected instance is: '" + generateByInstance + "'.");
        }
    }

    @Override
    public String generateSMPsWithExpiredCertificatesReport(String recipientEmail) {
        String serverAddress = getServerAddress();
        String senderEmail = configurationBusiness.getSMPExpiredCertReportMailFrom();
        generateSMPsWithExpiredCertificatesReport(serverAddress, senderEmail, recipientEmail);
        return "Report sent to mail: [" + recipientEmail + "]";
    }

    public void generateSMPsWithExpiredCertificatesReport(String serverAddress, String senderEmail, String recipientEmail) {
        loggingService.info("Start generating SMP list With Expired Certificates on server instance: [" + serverAddress + "].");
        SMPExpiredCertificatesReport report = reportBusiness.getSMPsWithExpiredCertificateReport(getServerAddress(), getServerName());

        try {
            loggingService.info("Sending report by E-Mail for: [" + recipientEmail + "].");
            mailSenderService.sendMessage(EMAIL_SUBJECT + " for " + serverAddress,
                    report.getDetailedReport(),
                    senderEmail, recipientEmail);
            loggingService.info(DataInconsistencyReport.LOG_PREFIX + " Report sent by E-Mail for:" + recipientEmail);
        } catch (Exception smptException) {
            loggingService.error(smptException.getMessage(), smptException);
            loggingService.info(String.format("## SMP list With Expired Certificates - %s ##", EMAIL_SUBJECT));
            if (report != null) {
                loggingService.info(report.getDetailedReport());
            }
        }
    }
}
