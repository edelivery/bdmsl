/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.impl;

import eu.europa.ec.bdmsl.common.bo.CertificateBO;
import eu.europa.ec.bdmsl.common.exception.InvalidArgumentException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.dao.AbstractDAOImpl;
import eu.europa.ec.bdmsl.dao.ICertificateDAO;
import eu.europa.ec.bdmsl.dao.entity.CertificateEntity;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Repository;

import jakarta.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static eu.europa.ec.bdmsl.dao.QueryNames.*;

/**
 * @author Adrien FERIAL
 * @since 15/06/2015
 */
@Repository
public class CertificateDAOImpl extends AbstractDAOImpl implements ICertificateDAO {

    @Autowired
    private ConversionService conversionService;
    
    @Override
    public CertificateBO findCertificateByCertificateId(String certificateId) throws TechnicalException {
        Optional<CertificateEntity> resultEntity = findCertificateEntityByCertificateId(certificateId);
        return resultEntity.isPresent() ? conversionService.convert(resultEntity.get(), CertificateBO.class) : null;
    }

    @Override
    public Optional<CertificateEntity> findCertificateEntityByCertificateId(String certificateId) throws TechnicalException {
        // find the certificate
        if (StringUtils.isBlank(certificateId)) {
            throw new InvalidArgumentException("Invalid certificateId! Certificate Id must not be Null/Empty!");
        }

        TypedQuery<CertificateEntity> queryCertificate = getEntityManager().createNamedQuery(CERTIFICATE_ENTITY_GET_BY_CERTIFICATE_ID, CertificateEntity.class);
        queryCertificate.setParameter("certificateId", certificateId);

        List<CertificateEntity> certificateEntityList = queryCertificate.getResultList();
        if (certificateEntityList.isEmpty()) {
            return Optional.empty();
        }

        if (certificateEntityList.size() > 1) {
            loggingService.warn("Inconsistent data! There are multiple certificate entries with id [" + certificateId
                    + "] in database!");
        }
        return Optional.of(certificateEntityList.get(0));
    }

    @Override
    public Long createCertificate(CertificateBO certificateBO) throws TechnicalException {
        CertificateEntity certificateEntity = conversionService.convert(certificateBO, CertificateEntity.class);
        super.persist(certificateEntity);
        return certificateEntity.getId();
    }

    @Override
    public void updateCertificate(CertificateBO certificateBO) throws TechnicalException {
        CertificateEntity certificateEntity = getEntityManager().find(CertificateEntity.class, certificateBO.getId());
        certificateEntity.setCertificateId(certificateBO.getCertificateId());
        certificateEntity.setPemEncoding(certificateBO.getPemEncoding());
        if (certificateBO.getNewCertificateId() != null) {
            certificateEntity.setNewCertificate(getEntityManager().find(CertificateEntity.class, certificateBO.getNewCertificateId()));
        }
        certificateEntity.setNewCertificateChangeDate(certificateBO.getMigrationDate());
        certificateEntity.setValidFrom(certificateBO.getValidFrom());
        certificateEntity.setValidTo(certificateBO.getValidTo());
        super.merge(certificateEntity);
    }

    @Override
    public List<CertificateBO> findCertificatesToChange(Calendar currentDate) throws TechnicalException {
        // This method is used in the context of a job that can be run on a clustered environment. To avoid concurrency issues, we do here a SELECT FOR UPDATE
        TypedQuery<CertificateEntity> query = getEntityManager().createNamedQuery(CERTIFICATE_ENTITY_LIST_CERTIFICATES_CHANGE_TO, CertificateEntity.class);
        query.setParameter("currentDate", currentDate);

        List<CertificateEntity> results = query.getResultList();
        List<CertificateBO> resultList = new ArrayList<>();
        if (!results.isEmpty()) {
            resultList = results.stream()
                    .map(result -> conversionService.convert(result, CertificateBO.class))
                    .collect(Collectors.toList());
        }
        return resultList;
    }

    @Override
    public void delete(CertificateBO certificateBO) throws TechnicalException {
        CertificateEntity certificateEntity = getEntityManager().find(CertificateEntity.class, certificateBO.getId());
        getEntityManager().remove(certificateEntity);
    }

    @Override
    public CertificateBO findCertificateById(Long id) throws TechnicalException {

        TypedQuery<CertificateEntity> query = getEntityManager().createNamedQuery(CERTIFICATE_ENTITY_GET_BY_ID, CertificateEntity.class);
        query.setParameter("id", id);
        List<CertificateEntity> results = query.getResultList();
        if (results.isEmpty()) {
            loggingService.info("No entry found for certificate id" + id);
            return null;
        }
        return conversionService.convert(results.get(0), CertificateBO.class);
    }

}
