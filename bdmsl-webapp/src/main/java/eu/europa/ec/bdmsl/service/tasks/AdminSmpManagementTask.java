/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.tasks;

import eu.europa.ec.bdmsl.business.IConfigurationBusiness;
import eu.europa.ec.bdmsl.business.IManageParticipantIdentifierBusiness;
import eu.europa.ec.bdmsl.business.IManageServiceMetadataBusiness;
import eu.europa.ec.bdmsl.common.bo.ParticipantBO;
import eu.europa.ec.bdmsl.common.bo.ServiceMetadataPublisherBO;
import eu.europa.ec.bdmsl.common.enums.AdminSMPManageActionEnum;
import eu.europa.ec.bdmsl.common.exception.BadRequestException;
import eu.europa.ec.bdmsl.common.exception.SmpDeleteException;
import eu.europa.ec.bdmsl.common.exception.SmpManageException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.common.service.AbstractServiceImpl;
import eu.europa.ec.bdmsl.service.IMailSenderService;
import eu.europa.ec.bdmsl.service.dns.IDnsClientService;
import eu.europa.ec.bdmsl.service.dns.impl.SupportedDnsRecordType;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

import static eu.europa.ec.bdmsl.common.exception.ErrorMessagesType.*;

/**
 * This class is a Runnable task that is used to manage SMPs with a large count of participants.
 * It is a prototype bean, so it can be used in a thread.
 *
 * @author Joze RIHTARSIC
 * @since 4.3
 */
@Component
@Scope("prototype")
public class AdminSmpManagementTask extends AbstractServiceImpl implements Runnable {

    protected static final Logger LOG = LoggerFactory.getLogger(AdminSmpManagementTask.class);

    private final IManageServiceMetadataBusiness manageServiceMetadataBusiness;
    private final IManageParticipantIdentifierBusiness manageParticipantIdentifierBusiness;
    private final IMailSenderService mailSenderService;
    private final IConfigurationBusiness configurationBusiness;
    private final IDnsClientService dnsClientService;


    String recipientEmail;
    AdminSMPManageActionEnum action;
    ServiceMetadataPublisherBO metadataPublisherBO;
    String physicalAddress;
    String logicalAddress;

    public AdminSmpManagementTask(IManageServiceMetadataBusiness manageServiceMetadataBusiness,
                                  IManageParticipantIdentifierBusiness manageParticipantIdentifierBusiness,
                                  IMailSenderService mailSenderService,
                                  IConfigurationBusiness configurationBusiness,
                                  IDnsClientService dnsClientService) {
        this.manageServiceMetadataBusiness = manageServiceMetadataBusiness;
        this.manageParticipantIdentifierBusiness = manageParticipantIdentifierBusiness;
        this.mailSenderService = mailSenderService;
        this.configurationBusiness = configurationBusiness;
        this.dnsClientService = dnsClientService;
    }

    public AdminSMPManageActionEnum getAction() {
        return action;
    }

    public void setAction(AdminSMPManageActionEnum action) {
        this.action = action;
    }

    public ServiceMetadataPublisherBO getMetadataPublisherBO() {
        return metadataPublisherBO;
    }

    public void setMetadataPublisherBO(ServiceMetadataPublisherBO metadataPublisherBO) {
        this.metadataPublisherBO = metadataPublisherBO;
    }

    public String getPhysicalAddress() {
        return physicalAddress;
    }

    public String getLogicalAddress() {
        return logicalAddress;
    }

    public void setUpdateData(String logicalAddress, String physicalAddress) {
        this.logicalAddress = logicalAddress;
        this.physicalAddress = physicalAddress;
    }

    public String getReceiverAddress() {
        return recipientEmail;
    }

    public void setReceiverAddress(String receiverAddress) {
        this.recipientEmail = receiverAddress;
    }

    @Override
    public void run() {
        String result;
        try {
            LOG.info("Execute smp management action: [{}]", action);
            result = executeAction(action);
            LOG.info("Smp management action: [{}] executed.", action);
        } catch (TechnicalException e) {
            result = "Error occurred while executing smp management action [" + action + "] with message: " + e.getMessage();
            LOG.error(result, e);
        }
        // send mail with result
        submitMailWithContent(result);
    }

    public String executeAction(AdminSMPManageActionEnum action) throws TechnicalException {
        switch (action) {
            case ENABLE:
                return enableSMP(metadataPublisherBO);
            case DISABLE:
                return disableSMP(metadataPublisherBO);
            case DELETE:
                return deleteSMP(metadataPublisherBO);
            case UPDATE:
                return updateSMP(metadataPublisherBO, logicalAddress, physicalAddress);
            case SYNC_DNS:
                return synchronizeDNSRecordsForSMP(metadataPublisherBO);
            default:
                throw new BadRequestException("Action: [" + action + "] is not supported!");
        }
    }

    /**
     * Method deletes SMP instance. Only deactivated SMP can be deleted by the SMP admin. The method validates that
     * all  participants identifiers are disabled and then deletes them.
     *
     * @param metadataPublisherBO the SMP to be deleted by the SMP admin
     * @return status description
     * @throws TechnicalException if technical error occurs
     */
    public String deleteSMP(ServiceMetadataPublisherBO metadataPublisherBO) throws TechnicalException {
        if (!metadataPublisherBO.isDisabled()) {
            throw new BadRequestException(PUBLISHER_ID_NOT_DISABLED.getMessage(metadataPublisherBO.getSmpId()));
        }

        long cnt = manageParticipantIdentifierBusiness.getParticipantCountForSMPAndStatus(metadataPublisherBO, false);

        if (cnt > 0) {
            throw new BadRequestException(PUBLISHER_ID_HAS_ENABLED_PARTICIPANT.getMessage(metadataPublisherBO.getSmpId(), String.valueOf(cnt)));
        }

        int updatedParticipantCount = 0;
        try {
            List<ParticipantBO> participantBOList = manageParticipantIdentifierBusiness.deleteNextParticipantsBatchForSMP(metadataPublisherBO);
            while (!participantBOList.isEmpty()) {
                updatedParticipantCount += participantBOList.size();
                participantBOList = manageParticipantIdentifierBusiness.deleteNextParticipantsBatchForSMP(metadataPublisherBO);
            }

        } catch (TechnicalException exc) {
            throw exc;
        } catch (Exception exc) {
            throw new SmpDeleteException("The SMP couldn't be deleted but some of its participants may have been deleted. Please see the logs or contact the system administrator to identify which participants have been deleted", exc);
        }
        manageServiceMetadataBusiness.deleteSMP(metadataPublisherBO);
        return "Deleted " + updatedParticipantCount + " participants for SMP " + metadataPublisherBO.getSmpId() + "!";
    }

    /**
     * Method enables SMP instance. Only disabled SMP can be enabled by the SMP admin. The method also registers
     * all associated participants to the DNS.
     *
     * @param metadataPublisherBO the SMP to be activated by the SMP admin
     * @return status description
     * @throws TechnicalException if technical error occurs
     */
    public String enableSMP(ServiceMetadataPublisherBO metadataPublisherBO) throws TechnicalException {
        return setSMPDisabled(metadataPublisherBO, false);
    }

    public String disableSMP(ServiceMetadataPublisherBO metadataPublisherBO) throws TechnicalException {
        return setSMPDisabled(metadataPublisherBO, true);
    }

    /**
     * Method updates SMP instance. physicalAddress and logicalAddress
     *
     * @param metadataPublisherBO the SMP to be updated by the SMP admin
     * @param logicalAddress      the new logical address of the SMP instance (URL)
     * @param physicalAddress     the new physical address of the SMP instance (IP)
     * @return status description
     * @throws TechnicalException if technical error occurs
     */
    public String updateSMP(ServiceMetadataPublisherBO metadataPublisherBO, String logicalAddress, String physicalAddress) throws TechnicalException {
        if (StringUtils.isBlank(logicalAddress) && StringUtils.isBlank(physicalAddress)) {
            return "No update data provided for SMP: [" + metadataPublisherBO.getSmpId() + "]";
        }

        // check if logical address must be changed - no need to change SMP record in DNS if only physical address is changed
        boolean updateLogicalAddress = StringUtils.isNotBlank(logicalAddress) && !StringUtils.equals(logicalAddress, metadataPublisherBO.getLogicalAddress());
        ServiceMetadataPublisherBO smpToUpdate = createSMPData(metadataPublisherBO, logicalAddress, physicalAddress);
        String resultMessage = "Updated SMP: [" + metadataPublisherBO.getSmpId() + "] address. ";

        // If DNS update fails, then an exception is thrown and the database import is rolled back
        if (configurationBusiness.isDNSEnabled() && updateLogicalAddress && !metadataPublisherBO.isDisabled()){
            // if logical address is changed and domain has naptr records. then
            // all naptr participant records must be updated.
            if (hasToUpdateNaptrRecords(metadataPublisherBO, logicalAddress)) {
                // Prepare to migrate all participants to new DNS records
                // create duplicate smp with new logical address - and same SMP identifier (rename old identifier)
                ServiceMetadataPublisherBO newSMP = manageServiceMetadataBusiness.prepareToBatchUpdateSMP(smpToUpdate);
                dnsClientService.updateSMPDNSRecord(smpToUpdate);
                // get batch from  database and migrate it to new smp with new logical address.
                int updatedParticipantCount = 0;
                try {
                    // repeat until all participants are migrated
                    List<ParticipantBO> participantBOList = manageParticipantIdentifierBusiness.updateNaptrRecordsForNextParticipantsBatchSMP(smpToUpdate, newSMP);
                    while (!participantBOList.isEmpty()) {
                        updatedParticipantCount += participantBOList.size();
                        participantBOList = manageParticipantIdentifierBusiness.updateNaptrRecordsForNextParticipantsBatchSMP(smpToUpdate, newSMP);
                    }
                    // delete old empty smp and update new smp identifier
                    ServiceMetadataPublisherBO deletedSmp = manageServiceMetadataBusiness.deleteSMP(smpToUpdate);
                    manageServiceMetadataBusiness.updateSMPIdentifier(newSMP, deletedSmp.getSmpId());
                } catch (TechnicalException exc) {
                    throw exc;
                } catch (Exception exc) {
                    throw new SmpManageException("The SMP couldn't be updated but some of its participants may have been updated. Please see the logs or contact the system administrator to identify which participants have been updated", exc);
                }
                resultMessage += "Updated ["+updatedParticipantCount+"] NAPTR records for participants !";

            } else {
                // only update smp
                resultMessage += "Only DNS updated of the SMP record!";
                dnsClientService.updateSMPDNSRecord(smpToUpdate);
                manageServiceMetadataBusiness.updateSMP(smpToUpdate);
            }
        } else {
            // update only database
            resultMessage += "Only database updated due to disabled SMP or DNS disabled! or SMP does not have logical address!";
            manageServiceMetadataBusiness.updateSMP(smpToUpdate);
        }

        return resultMessage;
    }

    /**
     * Method disables SMP instance. Only enabled SMP can be disabled by the SMP admin. The method also removes
     * all the SMPs records and all associated participants from the DNS.
     *
     * @param metadataPublisherBO the SMP to be deactivated by the SMP admin.
     * @return status description
     * @throws TechnicalException if technical error occurs
     */
    public String setSMPDisabled(ServiceMetadataPublisherBO metadataPublisherBO, boolean disable) throws TechnicalException {
        String message = disable ? PUBLISHER_ID_ALREADY_DISABLED.getMessage(metadataPublisherBO.getSmpId()): PUBLISHER_ID_ALREADY_ENABLED.getMessage(metadataPublisherBO.getSmpId());
        if (metadataPublisherBO.isDisabled() == disable) {
            throw new BadRequestException(message);
        }

        long cnt = manageParticipantIdentifierBusiness.getParticipantCountForSMP(metadataPublisherBO);
        int updatedParticipantCount = 0;
        try {
            List<ParticipantBO> participantBOList = manageParticipantIdentifierBusiness.updateNextParticipantsBatchStatusForSMP(metadataPublisherBO, disable);
            while (!participantBOList.isEmpty()) {
                updatedParticipantCount += participantBOList.size();
                participantBOList = manageParticipantIdentifierBusiness.updateNextParticipantsBatchStatusForSMP(metadataPublisherBO, disable);
            }

        } catch (TechnicalException exc) {
            throw exc;
        } catch (Exception exc) {
            throw new SmpManageException("The SMP couldn't be ["+action+"] but some of its participants may have been updated. Please see the logs or contact the system administrator to identify which participants have been updated", exc);
        }
        if (disable) {
            dnsClientService.deleteDNSRecordsForSMP(metadataPublisherBO);
        } else {
            dnsClientService.createDNSRecordsForSMP(metadataPublisherBO);
        }
        manageServiceMetadataBusiness.disableSMP(metadataPublisherBO, disable);
        return "Action ["+action+"] executed on [" + updatedParticipantCount + "] participants for SMP " + metadataPublisherBO.getSmpId() + " (total " + cnt + " participants)";
    }

    /**
     * Method iterates over all participants and updates their DNS records if needed.
     */
    public String synchronizeDNSRecordsForSMP(ServiceMetadataPublisherBO metadataPublisherBO) throws SmpManageException {
        if (!configurationBusiness.isDNSEnabled()){
            return "DNS is not enabled! No DNS records are synchronized with the SMP: [" + metadataPublisherBO.getSmpId() + "]";
        }
        long participantCount = manageParticipantIdentifierBusiness.getParticipantCountForSMP(metadataPublisherBO);
        if (participantCount < 1){
            return "No participants for SMP: [" + metadataPublisherBO.getSmpId() + "]! No DNS records are synchronized!";
        }
        int pageSize = 10;
        int page = 0;
        int cnt = 0;
        try {
            while ((long) pageSize * page < participantCount){
                List<ParticipantBO> participantBOList = manageParticipantIdentifierBusiness.getParticipantsForSMP(metadataPublisherBO, page, pageSize);
                dnsClientService.synchronizeDNSRecordsForSMP(metadataPublisherBO, participantBOList);
                cnt += participantBOList.size();
                page++;
            }

        } catch (Exception exc) {
            throw new SmpManageException("The SMP couldn't be ["+action+"] but some of its participants may have been updated. Please see the logs or contact the system administrator to identify which participants have been updated", exc);
        }
        return "Action ["+action+"] executed on [" + participantCount + "] participants for SMP " + metadataPublisherBO.getSmpId() + " (total " + cnt + " participants)";

    }

    public void submitMailWithContent(String mailContent) {
        if (StringUtils.isBlank(recipientEmail)) {
            LOG.info("Receiver address is not set! No mail will be sent for action [{}] and result [{}]!", action, mailContent);
            return;
        }
        String serverAddress = getServerAddress();
        String senderEmail = configurationBusiness.getInconsistencyReportMailFrom();
        String mailSubject = "Admin SMP action [" + action + "] result for server [" + serverAddress + "]";
        try {
            LOG.info("Sending report by E-Mail for: [{}]", recipientEmail);
            mailSenderService.sendMessage(mailSubject,
                    mailContent,
                    senderEmail, recipientEmail);
            LOG.info(" Report sent by E-Mail for: [{}]", recipientEmail);
        } catch (Exception smptException) {
            LOG.error("Could not send mail to [{}], with content [{}] due to root cause [{}]", recipientEmail, mailContent, ExceptionUtils.getRootCauseMessage(smptException));
            LOG.debug(smptException.getMessage(), smptException);
        }
    }


    public ServiceMetadataPublisherBO createSMPData(ServiceMetadataPublisherBO metadataPublisherBO, String logicalAddress, String physicalAddress) {
        ServiceMetadataPublisherBO updatedSmp = new ServiceMetadataPublisherBO();
        updatedSmp.setSmpId(metadataPublisherBO.getSmpId());
        updatedSmp.setPhysicalAddress(StringUtils.defaultIfBlank(physicalAddress, metadataPublisherBO.getPhysicalAddress()));
        updatedSmp.setLogicalAddress(StringUtils.defaultIfBlank(logicalAddress,  metadataPublisherBO.getLogicalAddress()));
        updatedSmp.setSubdomain(metadataPublisherBO.getSubdomain());
        return updatedSmp;
    }

    public boolean hasToUpdateNaptrRecords(ServiceMetadataPublisherBO metadataPublisherBO, String logicalAddress) {
        boolean isLogicalAddressToBeUpdated = StringUtils.isNotBlank(logicalAddress) && !StringUtils.equals(logicalAddress, metadataPublisherBO.getLogicalAddress());
        //Checks if logical address is different to be updated on dns
        SupportedDnsRecordType recordType = SupportedDnsRecordType.valueOf(metadataPublisherBO.getSubdomain().getDnsRecordTypes().trim().toUpperCase());
        boolean hasSMPNaptrRecords = recordType == SupportedDnsRecordType.ALL || recordType == SupportedDnsRecordType.NAPTR;
        return isLogicalAddressToBeUpdated && hasSMPNaptrRecords;
    }
}
