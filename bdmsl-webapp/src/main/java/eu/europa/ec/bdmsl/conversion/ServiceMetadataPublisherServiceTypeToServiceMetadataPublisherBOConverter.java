/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2017 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.conversion;

import eu.europa.ec.bdmsl.common.bo.ServiceMetadataPublisherBO;
import org.apache.commons.lang3.StringUtils;
import org.busdox.servicemetadata.locator._1.PublisherEndpointType;
import org.busdox.servicemetadata.locator._1.ServiceMetadataPublisherServiceType;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * This converter class transforms ServiceMetadataPublisherServiceType objects into ServiceMetadataPublisherBO objects.
 * Note: WS Type to BO is converted manually and strings are trimmed!
 *
 * @author Thomas Dussart
 * @since 24/05/2024
 */
@Component
public class ServiceMetadataPublisherServiceTypeToServiceMetadataPublisherBOConverter extends AutoRegisteringConverter<ServiceMetadataPublisherServiceType, ServiceMetadataPublisherBO> {

    public ServiceMetadataPublisherServiceTypeToServiceMetadataPublisherBOConverter(ConversionService conversionService) {
        super(conversionService);
    }

    @Override
    public ServiceMetadataPublisherBO convert(ServiceMetadataPublisherServiceType source) {
        if (source == null) {
            return null;
        }

        ServiceMetadataPublisherBO target = new ServiceMetadataPublisherBO();

        // Custom mapping
        target.setSmpId(StringUtils.trim(source.getServiceMetadataPublisherID()));
        // Business-specific logic
        target.setCertificateId(SecurityContextHolder.getContext().getAuthentication().getName());
        PublisherEndpointType publisherEndpoint = source.getPublisherEndpoint();
        if(publisherEndpoint == null) {
            return target;
        }
        target.setPhysicalAddress(StringUtils.trim(publisherEndpoint.getPhysicalAddress()));
        target.setLogicalAddress(StringUtils.trim(publisherEndpoint.getLogicalAddress()));
        return target;
    }
}
