/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.business;

import eu.europa.ec.bdmsl.common.bo.ConfigurationBO;
import eu.europa.ec.bdmsl.common.enums.SMLPropertyEnum;
import eu.europa.ec.bdmsl.common.exception.KeyException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.edelivery.security.cert.impl.CertificateRVStrategyEnum;

import java.io.File;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author Adrien FERIAL
 * @since 18/06/2015
 */
public interface IConfigurationBusiness {

    /**
     * This method find a configuration property
     *
     * @param propertyName the property name to find
     * @return tge configuration bean
     * @throws TechnicalException  if an error occurs during the process of finding the property
     */
    ConfigurationBO findConfigurationProperty(String propertyName) throws TechnicalException;

    ConfigurationBO setPropertyToDatabase(SMLPropertyEnum key, String value, String description) throws TechnicalException;

    ConfigurationBO getPropertyFromDatabase(SMLPropertyEnum key) throws TechnicalException;

    ConfigurationBO deletePropertyFromDatabase(SMLPropertyEnum key) throws TechnicalException;

    String getMonitorToken();

    boolean isClientCertEnabled();

    boolean isSSLClientCertEnabled();

    boolean isDeployedOnCluster();

    boolean isLegacyDomainAuthorizationEnabled();

    String getSMPCertRegularExpression();

    String getPartyIdentifierSplitRegularExpression();

    List<String> getPartyIdentifierCaseSensitiveSchemes();

    Pattern getPartyIdentifierSplitRegularExpresionPattern();

    String getCertificateChangeCron();

    String getConfigurationFolder();

    String getInconsistencyReportCron();

    String getInconsistencyReportGenerateByInstance();

    String getInconsistencyReportMailTo();

    String getInconsistencyReportMailFrom();

    String getSMPExpiredCertReportCron();

    String getSMPExpiredCertReportGenerateByInstance();

    String getSMPExpiredCertReportMailTo();

    String getSMPExpiredCertReportMailFrom();

    String getMailSMPTHost();

    int getMailSMPTPort();

    boolean isDNSSig0Enabled();

    String getDNSSig0KeyFilename();

    String getDNSSig0KeyName();

    boolean isDNSTSigEnabled();

    String getDNSTSigName();

    String getDNSTSigAlgorithm();

    byte[] getDNSTSigKeyValue();

    boolean isShowDNSEntriesEnabled();

    boolean isDNSEnabled();

    String getDNSServer();

    int getDNSTCPTimeout();

    String getDNSPublisherPrefix();

    String getEncryptionFilename();

    boolean isProxyEnabled();

    String getHttpProxyHost();

    String getHttpNoProxyHosts();

    int getHttpProxyPort();

    String getHttpProxyUsername();

    String getHttpProxyPassword();

    boolean isSignResponseEnabled();

    String getSignResponseAlgorithm();

    String getSignResponseDigestAlgorithm();

    String getSignAlias();

    String getKeystoreFilename();

    File getKeystoreFile();

    String getKeystoreType();

    String getKeystorePassword();

    String getTruststoreFilename();

    File getTruststoreFile();

    String getTruststoreType();

    String getTruststorePassword();

    CertificateRVStrategyEnum getCertRevocationValidationStrategy();

    boolean isCertRevocationValidationGraceful();

    List<String> getCertRevocationValidationAllowedUrlProtocols();

    int getListPageSize();

    int getSMPUpdateMaxParticipantCount();

    boolean isUnsecureLoginEnabled();

    String encryptString(String value) throws KeyException;

    String decryptString(String value) throws KeyException;

    void forceRefreshProperties();

    boolean isHSTSEnabled();

    int getHSTSMaxAge();

    boolean isHSTSConfiguredWithIncludeSubDomain();

    boolean isHSTSConfiguredWithPreload();

}
