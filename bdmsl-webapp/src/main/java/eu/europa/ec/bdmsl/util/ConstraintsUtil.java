/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.util;

import eu.europa.ec.bdmsl.common.exception.GenericTechnicalException;
import org.apache.commons.lang3.StringUtils;

/**
 * @author Flavio SANTOS
 * @since 03/03/2017
 */
public class ConstraintsUtil {
    /**
     * The regular expression to be used for validating migration key.
     */
    public static final String LOGICAL_ADDRESS_PROTOCOL_REGEX = "^(http|https)://.*$";

    /**
     * The regular expression to be used for validating migration key.
     */
    public static final String MIGRATION_KEY_REGEX = "^(?=.{8,24}$)(?=(.*[@#$%()\\[\\]{}*^_\\-!~|+=]){2,})(?=(.*[A-Z]){2})(?=(.*[a-z]){2})(?=(.*[0-9]){2})(?=\\S+$).*$";

    /**
     * The regular expression to be used for validating participant values
     * which should match ASCII printable characters (32-127)
     */
    public static final String PARTICIPANT_IDENTIFIER_ASCII_REGEX = "[ -~]+";

    /**
     * The maximum number of participants when calling the CreateList or DeleteList operations
     */
    public static final int MAX_PARTICIPANT_LIST = 100;

    /**
     * The minimum length of a migration key.
     */
    public static final int MIN_MIGRATION_KEY_LENGTH = 8;

    /**
     * The maximum length of a migration key.
     */
    public static final int MAX_MIGRATION_KEY_LENGTH = 24;

    /**
     * The default number of participants per page when calling the 'list' service
     */
    public static final int DEFAULT_PARTICIPANT_PER_PAGE = 50;

    public static void emailDataValidator(String data, String complementaryMessage) throws GenericTechnicalException {
        if (StringUtils.isEmpty(data)) {
            throw new GenericTechnicalException(String.format("Email %s is null or empty.", complementaryMessage));
        }
    }
}
