/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao;

import eu.europa.ec.bdmsl.common.bo.CertificateDomainBO;
import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.security.CertificateDetails;

import java.util.List;

/**
 * @author Adrien FERIAL
 * @since 12/06/2015
 */
public interface ICertificateDomainDAO {

    List<CertificateDomainBO> findAll() throws TechnicalException;

    List<CertificateDomainBO> findBySubDomain(String domainName) throws TechnicalException;

    void createCertificateDomain(CertificateDomainBO certificateDomainBO) throws TechnicalException;

    CertificateDomainBO findCertificateDomainByCertificate(String certificate) throws TechnicalException;

    CertificateDomainBO deleteCertificateDomain(CertificateDomainBO certificateDomainBO) throws TechnicalException;

    /**
     * Method finds domain by the certificate truststore alias and domain certificate type:
     * issuer based/rootCA or certificate based authorization).
     *
     * @param alias             - certificate truststore alias
     * @param isRootCertificate check only root certificates or if false only leaf certificates
     * @return list of CertificateDomainBO objects
     */
    List<CertificateDomainBO> getDomainByCertificateAlias(String alias, boolean isRootCertificate);

    /**
     * Method finds domain by the certificate token.
     * Because of legacy support Certificate token can be: ordered/normalized values from
     * CN", "OU", "O", "L", "ST", "C"
     * If nothing matches it tries with minimal values
     * CN", "O",  "C"
     *
     * @param certificate       - certificate token
     * @param isRootCertificate check only root certificates or if false only leaf certificates
     * @return list of CertificateDomainBO objects which match the certificate token and certificate type
     * @throws TechnicalException - if error occurs
     */
    CertificateDomainBO findDomain(String certificate, boolean isRootCertificate) throws TechnicalException;

    CertificateDomainBO findIssuerBasedAuthorizationDomain(String subject, String issuer, String globalSubjectRexExp) throws TechnicalException;

    CertificateDomainBO findDomain(CertificateDetails certificateDetails) throws TechnicalException;

    SubdomainBO findSubDomainForCertificateId(String certificateId) throws TechnicalException;

    CertificateDomainBO updateCertificateDomain(CertificateDomainBO certificate, SubdomainBO newSubdomainBO, String newCrlUrl, Boolean isAdminCertificate) throws TechnicalException;

    CertificateDomainBO deleteCertificateDomain(CertificateDomainBO certificateDomainBO, SubdomainBO subdomainBO) throws TechnicalException;

    CertificateDomainBO updateX509CertificateForCertificateDomain(CertificateDomainBO certificateDomainBO) throws TechnicalException;

    List<CertificateDomainBO> listCertificateDomains(String certificate, SubdomainBO newSubdomainBO) throws TechnicalException;

}
