/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.entity;

import eu.europa.ec.bdmsl.dao.utils.ColumnDescription;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.envers.Audited;

import jakarta.persistence.*;
import java.util.Calendar;
import java.util.Objects;

import static eu.europa.ec.bdmsl.dao.QueryNames.*;

/**
 * @author Adrien FERIAL
 * @since 15/06/2015
 */
@Entity
@Audited
@Table(name = "bdmsl_certificate_domain")
@org.hibernate.annotations.Table(appliesTo = "bdmsl_certificate_domain", comment = "Table contains domain authorization " +
        "certificate data for Issuer based authorization and Leaf certificate base authorization.")
@NamedQueries({
        @NamedQuery(name = CERTIFICATE_DOMAIN_ENTITY_GET_ALL, query = "SELECT c FROM CertificateDomainEntity c"),
        @NamedQuery(name = CERTIFICATE_DOMAIN_ENTITY_GET_BY_CERTIFICATE_ID, query = "SELECT c FROM CertificateDomainEntity c WHERE c.certificate = :certificateId"),
        @NamedQuery(name = CERTIFICATE_DOMAIN_ENTITY_GET_BY_CERTIFICATE_ID_AND_SUB_DOMAIN_ID, query = "SELECT c FROM CertificateDomainEntity c WHERE c.certificate = :certificateId AND c.subdomain.id = :subdomainId"),
        @NamedQuery(name = CERTIFICATE_DOMAIN_ENTITY_GET_BY_CERTIFICATE_ALIAS_AND_IS_ROOT,
                query = "SELECT c FROM CertificateDomainEntity c WHERE c.truststoreAlias = :truststoreAlias and c.isRootCA =:isRoot"),
        @NamedQuery(name = CERTIFICATE_DOMAIN_ENTITY_LIST_BY_CERTIFICATE_ID,
                query = "SELECT c FROM CertificateDomainEntity c WHERE lower(c.certificate) like lower(:certificateId)"),
        @NamedQuery(name = CERTIFICATE_DOMAIN_ENTITY_LIST_BY_CERTIFICATE_ID_AND_SUB_DOMAIN,
                query = "SELECT c FROM CertificateDomainEntity c WHERE lower(c.certificate) like lower(:certificateId) and c.subdomain = :subdomain"),
        @NamedQuery(name = CERTIFICATE_DOMAIN_ENTITY_LIST_BY_SUB_DOMAIN,
                query = "SELECT c FROM CertificateDomainEntity c WHERE  c.subdomain = :subdomain"),
})

public class CertificateDomainEntity extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "bdmsl_certificate_domain_seq")
    @GenericGenerator(name = "bdmsl_certificate_domain_seq", strategy = "native")
    @Column(name = "id")
    Long id;

    // Legacy natural id
    @Column(name = "certificate", length = CommonColumnsLengths.MAX_CERTIFICATE_ID, nullable = false)
    @ColumnDescription(comment = "Legacy Certificate id.")
    private String certificate;


    @Column(name = "truststore_alias", length = CommonColumnsLengths.MAX_TEXT_LENGTH_512)
    @ColumnDescription(comment = "TrustStore alias")
    private String truststoreAlias;

    @ManyToOne
    @JoinColumn(name = "fk_subdomain_id", nullable = false)
    @ColumnDescription(comment = "DNS (sub)domain reference for the root certificate")
    private SubdomainEntity subdomain;

    @Column(name = "crl_url", length = CommonColumnsLengths.MAX_CRL_URL)
    @ColumnDescription(comment = "URL to the certificate revocation list (CRL)")
    private String crl;

    @Column(name = "is_root_ca", nullable = false)
    @ColumnDescription(comment = "Is certificate Root certificate.")
    private boolean isRootCA;

    @Column(name = "pem_encoding", columnDefinition = "CLOB")
    @ColumnDescription(comment = "PEM encoding for the certificate")
    @Lob
    private String pemEncoding;

    @Column(name = "valid_from")
    @ColumnDescription(comment = "Start validity date of the certificate")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar validFrom;

    @Column(name = "valid_until")
    @ColumnDescription(comment = "Expiry date of the certificate")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar validTo;

    @Column(name = "is_admin", nullable = false)
    @ColumnDescription(comment = "Can certificate(s) call also the admin services. True only for nonroot certificates")
    private boolean isAdmin;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCertificate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public String getTruststoreAlias() {
        return truststoreAlias;
    }

    public void setTruststoreAlias(String truststoreAlias) {
        this.truststoreAlias = truststoreAlias;
    }

    public SubdomainEntity getSubdomain() {
        return subdomain;
    }

    public void setSubdomain(SubdomainEntity subdomainEntity) {
        this.subdomain = subdomainEntity;
    }

    public String getCrl() {
        return crl;
    }

    public void setCrl(String crl) {
        this.crl = crl;
    }

    public boolean isRootCA() {
        return isRootCA;
    }

    public void setRootCA(boolean rootCA) {
        isRootCA = rootCA;
    }

    public String getPemEncoding() {
        return pemEncoding;
    }

    public void setPemEncoding(String pemEncoding) {
        this.pemEncoding = pemEncoding;
    }

    public Calendar getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Calendar validFrom) {
        this.validFrom = validFrom;
    }

    public Calendar getValidTo() {
        return validTo;
    }

    public void setValidTo(Calendar validTo) {
        this.validTo = validTo;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CertificateDomainEntity)) return false;
        CertificateDomainEntity that = (CertificateDomainEntity) o;
        return isRootCA == that.isRootCA &&
                Objects.equals(certificate, that.certificate) &&
                Objects.equals(subdomain, that.subdomain) &&
                Objects.equals(crl, that.crl);
    }

    @Override
    public int hashCode() {
        //hash code only property because we should find object in hash buckets even if we change
        // value
        return Objects.hash(certificate);
    }

    @Override
    public String toString() {
        return "CertificateDomainEntity{" +
                "certificate='" + certificate + '\'' +
                ", isRootCA=" + isRootCA +
                '}';
    }
}
