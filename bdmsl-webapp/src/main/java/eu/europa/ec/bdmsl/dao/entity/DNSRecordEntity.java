/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.entity;


import eu.europa.ec.bdmsl.dao.utils.ColumnDescription;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.envers.Audited;

import jakarta.persistence.*;
import java.util.Objects;

import static eu.europa.ec.bdmsl.dao.QueryNames.*;

@Entity
@Audited
@Table(name = "bdmsl_dns_record",
        indexes = {@Index(name = "sml_dns_record_idx", columnList = "type, name,value", unique = true),
        }
)
@NamedQueries({
        @NamedQuery(name = DNS_RECORD_ENTITY_GET_ALL, query = "SELECT dr FROM DNSRecordEntity dr"),
        @NamedQuery(name = DNS_RECORD_ENTITY_GET_BY_NAME_AND_ZONE, query = "SELECT dr FROM DNSRecordEntity dr where  upper(dr.name)= upper(:name) and upper(dr.dnsZone)= upper(:dnsZone)"),
        @NamedQuery(name = DNS_RECORD_ENTITY_GET_BY_NAME_AND_ZONE_AND_TYPE_AND_VALUE,
                query = "SELECT dr FROM DNSRecordEntity dr where upper(dr.name)=upper(:name) and upper(dr.dnsZone)= upper(:dnsZone) and upper(dr.type)=upper(:dnsType) and dr.value=:dnsValue"),
})
@org.hibernate.annotations.Table(appliesTo = "bdmsl_dns_record", comment = "Contains custom DNS records")
public class DNSRecordEntity extends AbstractEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "bdmsl_dns_rec_seq")
    @GenericGenerator(name = "bdmsl_dns_rec_seq", strategy = "native")
    @Column(name = "id")
    @ColumnDescription(comment = "Surrogate key for DNSRecord Entity")
    Long id;

    @Column(name = "type", nullable = false, length = CommonColumnsLengths.MAX_DNS_TYPE_LENGTH)
    @ColumnDescription(comment = "Record type: A, CNAME, NAPTR.")
    protected String type;

    @Column(name = "name", nullable = false, length = CommonColumnsLengths.MAX_SML_SUBDOMAIN_LENGTH)
    @ColumnDescription(comment = "Dns name.")
    protected String name;

    @Column(name = "dns_zone", nullable = false, length = CommonColumnsLengths.MAX_SML_SUBDOMAIN_LENGTH)
    @ColumnDescription(comment = "Domain (dns zone) of dns server.")
    private String dnsZone;


    @Column(name = "value", length = CommonColumnsLengths.MAX_TEXT_LENGTH_512)
    @ColumnDescription(comment = "Dns Value.  For A type it must be IP address, for CNAME it must valid Domain, for NAPTR it must be regular expresion.")
    protected String value;

    @Column(name = "service", length = CommonColumnsLengths.MAX_DNS_NAPTR_SERVICE)
    @ColumnDescription(comment = "Service - part of naptr record. If not given (for naptr) default value is: Meta:SMP.")
    protected String service;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getDNSZone() {
        return dnsZone;
    }

    public void setDNSZone(String value) {
        this.dnsZone = value;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DNSRecordEntity)) return false;
        DNSRecordEntity that = (DNSRecordEntity) o;
        return Objects.equals(type, that.type) &&
                Objects.equals(name, that.name) &&
                Objects.equals(dnsZone, that.dnsZone) &&
                Objects.equals(value, that.value) &&
                Objects.equals(service, that.service);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, name, value, dnsZone);
    }
}
