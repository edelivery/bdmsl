/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.dns.impl;

import eu.europa.ec.bdmsl.business.IIdentifierFormatterBusiness;
import eu.europa.ec.bdmsl.common.exception.BadRequestException;
import eu.europa.ec.bdmsl.common.exception.DnsRecordBuildException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.common.logging.ILoggingService;
import eu.europa.ec.dynamicdiscovery.enums.DNSLookupHashType;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.xbill.DNS.*;
import org.xbill.DNS.Record;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Arrays;

import static eu.europa.ec.bdmsl.common.util.Constant.FAILED_TO_BUILD_DNS_NAME_FROM;
import static eu.europa.ec.bdmsl.common.util.Constant.INVALID_IPV_4_ADDRESS;

/**
 * @author Flavio SANTOS
 */
@Service
public final class DnsRecordManager {

    private ILoggingService loggingService;

    private IIdentifierFormatterBusiness identifierFormatterBusiness;

    private final int DEFAULT_TTL_SECS = 60;

    private final String DNS_CNAME_PARTICIPANT_PREFIX = "B-";

    private final String DNS_U_NAPTR_REGEXP_RFC4848 = ".*";

    public DnsRecordManager(ILoggingService loggingService, IIdentifierFormatterBusiness identifierFormatterBusiness) {
        this.loggingService = loggingService;
        this.identifierFormatterBusiness = identifierFormatterBusiness;
    }

    public Record createSMP(String smpId, String subdomain, String logicalAddress, String dnsPublisherPrefix) throws TechnicalException {
        Name publisherHost = generateSmpCNAME(smpId, subdomain, dnsPublisherPrefix);
        return createSMP(publisherHost, logicalAddress);
    }

    private Record createSMP(Name publisherHost, String logicalAddress) throws TechnicalException {
        String endPoint;
        try {
            endPoint = new URL(logicalAddress).getHost();
        } catch (MalformedURLException e) {
            throw new BadRequestException("Logical address is malformed: " + e.getMessage(), e);
        }

        byte[] ipAddressBytes = Address.toByteArray(endPoint, Address.IPv4);
        if (ipAddressBytes != null) {
            final InetAddress inetAddress;
            try {
                inetAddress = InetAddress.getByAddress(ipAddressBytes);
            } catch (UnknownHostException exc) {
                throw new BadRequestException(INVALID_IPV_4_ADDRESS + endPoint + "'", exc);
            }
            return new ARecord(publisherHost, DClass.IN, DEFAULT_TTL_SECS, inetAddress);
        } else {
            try {
                return new CNAMERecord(publisherHost, DClass.IN, DEFAULT_TTL_SECS, new Name(endPoint + "."));
            } catch (RelativeNameException | TextParseException exc) {
                throw new DnsRecordBuildException(FAILED_TO_BUILD_DNS_NAME_FROM + endPoint + "'", exc);
            }
        }
    }

    public final Name generateSmpCNAME(final String smpId, String subDomain, String dnsPublisherPrefix) throws TechnicalException {
        String smpDnsName = null;

        if (StringUtils.isEmpty(smpId) || StringUtils.isEmpty(subDomain) || StringUtils.isEmpty(dnsPublisherPrefix)) {
            throw new DnsRecordBuildException("Failed to build DNS object, SMP data must be not null.");
        }

        try {
            smpDnsName = smpId + "." + dnsPublisherPrefix + "." + subDomain;
            return Name.fromString(smpDnsName);
        } catch (final RelativeNameException | TextParseException exc) {
            throw new DnsRecordBuildException(FAILED_TO_BUILD_DNS_NAME_FROM + smpDnsName + "'", exc);
        }
    }

    public Record createParticipantCNAME(String participantId, String scheme, String subDomain, String smpId, String dnsPublisherPrefix) throws TechnicalException {
        Name participantCnameHost = generateParticipantCNAME(participantId, scheme, subDomain);
        Name smpHost = generateSmpCNAME(smpId, subDomain, dnsPublisherPrefix);
        return new CNAMERecord(participantCnameHost, DClass.IN, DEFAULT_TTL_SECS, smpHost);
    }

    public Name generateDnsQuery(String participantId, String scheme, String subDomain, int participantType) throws TechnicalException {
        if (!Arrays.asList(Type.CNAME, Type.NAPTR).contains(participantType)) {
            loggingService.warn("Cannot generate participant DNS Name for participant '" +participantId + "' having scheme '" + scheme
                    + "' because of unsupported DNS type: '" + Type.string(participantType) + "'. Possible values: CNAME and NAPTR");
            throw new DnsRecordBuildException(FAILED_TO_BUILD_DNS_NAME_FROM + Type.string(participantType) + "' type");
        }

        return participantType == Type.CNAME
                ? generateParticipantCNAME(participantId, scheme, subDomain)
                : generateParticipantNAPTR(participantId, scheme, subDomain);
    }

    public Name generateParticipantCNAME(String participantId, String scheme, String subDomain) throws TechnicalException {
        String smpDnsName = null;

        if (StringUtils.isEmpty(participantId) || StringUtils.isEmpty(subDomain)) {
            throw new DnsRecordBuildException("Failed to build DNS object, Participant data must be not null.");
        }

        try {
            if ("*".equals(participantId)) {
                smpDnsName = "*" + getSchemeDNSPart(scheme) + "." + subDomain;

            } else {
                smpDnsName = identifierFormatterBusiness.dnsLookupFormat(scheme, participantId, DNSLookupHashType.MD5_HEX)
                        + "." + subDomain;
            }
            return Name.fromString(smpDnsName);
        } catch (final RelativeNameException | TextParseException exc) {
            throw new DnsRecordBuildException(FAILED_TO_BUILD_DNS_NAME_FROM + smpDnsName + "'", exc);
        }
    }

    /**
     * Method generated U-NAPTR DNS record from participant identifier .
     *
     * @param participantId     - participant id part of the participant identifier
     * @param scheme            - participant scheme part of the participant identifier
     * @param subDomain         - the DNS subdomain for the FQDN
     * @param type              - U-NAPTR service
     * @param endpointAddress   - Endpoint address
     * @return DNS record
     * @throws TechnicalException
     */
    public final Record createParticipantNAPTR(String participantId, String scheme, String subDomain, String type, String endpointAddress) throws TechnicalException {
        if (endpointAddress == null) {
            throw new DnsRecordBuildException("SMP Address was not found for creating NAPTR record.");
        }
        endpointAddress = removeDomainExtraCharacter(endpointAddress);
        String naptrValue = generateUNaptrValue(endpointAddress);
        Name participantNaptrHost = generateParticipantNAPTR(participantId, scheme, subDomain);
        try {
            return new NAPTRRecord(participantNaptrHost, DClass.IN, DEFAULT_TTL_SECS, 100, 10, "U", type, naptrValue, Name.fromString("."));
        } catch (RelativeNameException | TextParseException exc) {
            throw new DnsRecordBuildException(FAILED_TO_BUILD_DNS_NAME_FROM + endpointAddress + "'", exc);
        }
    }

    /**
     * Generate U-NAPTR value for the URL according to the RFC4848
     *
     * @param endpointAddress   - the endpoint URL
     * @return
     */
    public String generateUNaptrValue(String endpointAddress) {
        return "!" +  DNS_U_NAPTR_REGEXP_RFC4848 + "!" + endpointAddress + "!";
    }

    public final Name generateParticipantNAPTR(String participantId, String scheme, String subDomain) throws
            TechnicalException {

        if (StringUtils.isEmpty(participantId) || StringUtils.isEmpty(subDomain)) {
            throw new DnsRecordBuildException("Failed to build DNS object, Participant data must be not null.");
        }

        String smpDnsName;
        try {
            if ("*".equals(participantId)) {
                smpDnsName = "*" + getSchemeDNSPart(scheme) + "." + subDomain;
            } else {
                smpDnsName = identifierFormatterBusiness.dnsLookupFormat(scheme, participantId, DNSLookupHashType.SHA256_BASE32)
                        + "." + subDomain;
            }
            return Name.fromString(smpDnsName);
        } catch (final RelativeNameException | TextParseException exc) {
            throw new DnsRecordBuildException(String.format("Failed to build DNS Name for participant=%s, scheme=%s, subdomain=%s ", participantId, scheme, subDomain), exc);
        }
    }

    private String getSchemeDNSPart(String scheme) {
        return (StringUtils.isBlank(scheme) ? "" : "." + scheme);
    }

    private String removeDomainExtraCharacter(String domain) {
        if (!StringUtils.isEmpty(domain) && domain.substring(domain.length() - 1).equals(".")) {
            return domain.substring(0, domain.length() - 1);
        }
        return domain;
    }

    public Record createCustomARecord(Name dnsName, String address) throws TechnicalException {
        byte[] ipAddressBytes = Address.toByteArray(address, Address.IPv4);
        if (ipAddressBytes != null) {
            return createIP4ARecordFromByte(dnsName, ipAddressBytes);
        }
        throw new BadRequestException(INVALID_IPV_4_ADDRESS + address + "'");
    }

    protected Record createIP4ARecordFromByte(Name dnsName, byte[] ipAddressBytes) throws TechnicalException {
        final InetAddress inetAddress;
        try {
            inetAddress = InetAddress.getByAddress(ipAddressBytes);
        } catch (UnknownHostException exc) {
            throw new BadRequestException(INVALID_IPV_4_ADDRESS + Arrays.toString(ipAddressBytes) + "'", exc);
        }
        return new ARecord(dnsName, DClass.IN, DEFAULT_TTL_SECS, inetAddress);
    }

    public Record createCustomCNameRecord(Name dnsName, String aliasDomain) throws TechnicalException {
        try {
            return new CNAMERecord(dnsName, DClass.IN, DEFAULT_TTL_SECS, new Name(aliasDomain + "."));
        } catch (RelativeNameException | TextParseException exc) {
            throw new DnsRecordBuildException("Failed to build custom DNS CName record for " + dnsName.toString() + " for '" + aliasDomain + "'", exc);
        }
    }

    public Record createCustomNaptrRecord(Name dnsName, String regExpr, String service) throws TechnicalException {
        try {
            return new NAPTRRecord(dnsName, DClass.IN, DEFAULT_TTL_SECS, 100, 10, "U", service, "!^.*$!" + regExpr + "!", Name.fromString("."));
        } catch (RelativeNameException | TextParseException exc) {
            throw new DnsRecordBuildException("Failed to build custom DNS Naptr record for  '" + dnsName.toString() + ", service: " + service + "', regexpr: " + regExpr + ".", exc);
        }
    }
}
