/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.util;

import eu.europa.ec.bdmsl.common.exception.InvalidArgumentException;
import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.apache.commons.lang3.StringUtils.trim;


/**
 * This class is used to validate data.
 *
 * @author Joze RIHTARSIC
 * @since 4.2.1
 *
 */
public class DataValidator {

    private static final Pattern emailRegExpValidation = Pattern.compile("(?:[\\p{L}0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[\\p{L}0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-zA-Z0-9-]*[a-zA-Z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])");
    private static final Pattern domainRegExpValidation = Pattern.compile("^(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\\.)*[a-z0-9][a-z0-9-]{0,61}[a-z0-9]$");


    protected DataValidator() {
    }

    /**
     * Validate email address.
     *
     * @param emailAddress email address to validate
     * @throws InvalidArgumentException  if email address is not valid
     */
    public static void validateEmail(String emailAddress) throws InvalidArgumentException {
        if (StringUtils.isEmpty(emailAddress)) {
            throw new InvalidArgumentException("Empty email address is invalid!");
        }
        Matcher matcher = emailRegExpValidation.matcher(trim(emailAddress));
        if (!matcher.matches()) {
            throw new InvalidArgumentException(String.format("Email address [%s] is not valid.", emailAddress));
        }
    }

    /**
     * Validate hostname.
     *
     * @param hostname to validate
     * @throws InvalidArgumentException if hostname is not valid
     */
    public static void validateDnsDomainName(String hostname) throws InvalidArgumentException {
        if (StringUtils.isEmpty(hostname)) {
            throw new InvalidArgumentException("Empty Hostname is invalid!");
        }
        Matcher matcher = domainRegExpValidation.matcher(hostname);
        if (!matcher.matches()) {
            throw new InvalidArgumentException(String.format("Invalid hostname [%s].", hostname));
        }
    }
}
