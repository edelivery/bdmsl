/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.entity;

import eu.europa.ec.bdmsl.dao.utils.ColumnDescription;
import org.hibernate.envers.Audited;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import java.io.Serializable;
import java.util.Calendar;

/**
 * @author Adrien FERIAL
 * @since 15/06/2015
 */
@MappedSuperclass
abstract public class AbstractEntity implements Serializable {

    /**
     * Date of the creation.
     */
    @Audited
    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    @ColumnDescription(comment = "Date of creation")
    protected Calendar creationDate;

    /**
     * Date of the last update.
     */
    @Audited
    @Column(name = "last_updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    @ColumnDescription(comment = "Date of the last update")
    protected Calendar lastUpdateDate;

    abstract public Object getId();

    public Calendar getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Calendar creationDate) {
        this.creationDate = creationDate;
    }

    public Calendar getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Calendar lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }


}
