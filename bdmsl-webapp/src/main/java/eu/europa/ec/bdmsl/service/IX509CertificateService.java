/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service;

import eu.europa.ec.bdmsl.common.bo.CertificateDomainBO;

import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.edelivery.security.cert.IRevocationValidator;

import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;

/**
 * @author Adrien FERIAL
 * @since 18/06/2015
 */
public interface IX509CertificateService {

    /*
     * Checks if certificate is Non Root CA
     */
    CertificateDomainBO validateNonRootCA(final X509Certificate cert) throws TechnicalException,  CertificateNotYetValidException, CertificateExpiredException;

    CertificateDomainBO validateRootCA(final X509Certificate cert) throws TechnicalException,  CertificateNotYetValidException, CertificateExpiredException;

    void validateClientX509Certificate(final X509Certificate[] certificates, boolean isCertificateAlreadyTrusted) throws TechnicalException;

    void validateClientX509Certificate(final X509Certificate[] certificates) throws TechnicalException;

    X509Certificate getCertificate(final X509Certificate[] requestCerts) throws TechnicalException;

    String getTrustedRootCertificateDN(X509Certificate[] certificates) throws TechnicalException;

    IRevocationValidator getCrlVerifierService();
}
