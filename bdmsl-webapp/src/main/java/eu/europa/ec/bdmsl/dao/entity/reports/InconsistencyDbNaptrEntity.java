/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.entity.reports;

import java.util.Objects;

/**
 * This entity is just for serializing the naptr records which should be  registered in DNS
 */


public class InconsistencyDbNaptrEntity {


    String scheme;
    String participantId;
    String subdomainName;
    String logicalAddress;
    String dnsRecordName;
    boolean disabled;

    /**
     * Constructor for mysql database
     * @param scheme
     * @param participantId
     * @param dnsRecordName
     * @param subdomainName
     * @param logicalAddress
     * @param disabled
     */
    public InconsistencyDbNaptrEntity(String scheme, String participantId, String dnsRecordName, String subdomainName, String logicalAddress, boolean disabled) {
        this.scheme = scheme;
        this.participantId = participantId;
        this.subdomainName = subdomainName;
        this.logicalAddress = logicalAddress;
        this.dnsRecordName = dnsRecordName;
        this.disabled = disabled;
    }

    /**
     * constructor for oracle database
     * @param scheme
     * @param participantId
     * @param dnsRecordName
     * @param subdomainName
     * @param logicalAddress
     * @param disabled
     */
    public InconsistencyDbNaptrEntity(String scheme, String participantId, String dnsRecordName, String subdomainName, String logicalAddress, Number disabled) {
        this(scheme, participantId, dnsRecordName, subdomainName, logicalAddress, disabled!=null && disabled.longValue() >0);
    }

    public String getScheme() {
        return scheme;
    }

    public String getParticipantId() {
        return participantId;
    }

    public String getSubdomainName() {
        return subdomainName;
    }

    public String getLogicalAddress() {
        return logicalAddress;
    }

    public String getDnsRecordName() {
        return dnsRecordName;
    }

    public boolean isDisabled() {
        return disabled;
    }

    @Override
    public int hashCode() {
        // important to return hash dns record for searching in HasSet
        return this.dnsRecordName.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof InconsistencyDbNaptrEntity)) return false;
        InconsistencyDbNaptrEntity that = (InconsistencyDbNaptrEntity) o;
        return Objects.equals(dnsRecordName, that.dnsRecordName);
    }
}
