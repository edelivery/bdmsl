/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.business;

import eu.europa.ec.bdmsl.common.bo.CertificateBO;
import eu.europa.ec.bdmsl.common.bo.CertificateDomainBO;
import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.security.CertificateDetails;
import eu.europa.ec.edelivery.security.PreAuthenticatedCertificatePrincipal;

import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Optional;

/**
 * @author Adrien FERIAL
 * @since 18/06/2015
 */
public interface ICertificateDomainBusiness {

    /**
     * Method find domain for which the principal has grants to access. If no domain is found empty Optional is return.
     *
     * @param principal          - A principal is an entity that can be authenticated.
     * @param issuerBasedDomains if true, only issuer based domains are searched, if false only certificate based domains are searched/
     * @return - Return CertificateDomainBO wrapped in Optional if found, else  empty optional.
     * @throws TechnicalException  - If an error occurs.
     */
    Optional<CertificateDomainBO> findDomainForPrincipal(PreAuthenticatedCertificatePrincipal principal, boolean issuerBasedDomains) throws TechnicalException;

    /**
     * Method returns domain for the certificate alias. If no domain is found empty Optional is return.
     *
     * @param alias              - certificate alias for domain.
     * @param issuerBasedDomains if true, only issuer based domains are searched, if false only certificate based domains are searched/
     * @return - Return List of CertificateDomainBO
     */
    List<CertificateDomainBO> getDomainForCertificateAlias(String alias, boolean issuerBasedDomains);

    /**
     * This method find a certificate, whatever the order of the fields
     *
     * @param certificate - certificate token to find
     * @return - Return CertificateDomainBO else return null.
     * @throws TechnicalException - If an error occurs.
     */
    CertificateDomainBO findDomain(String certificate) throws TechnicalException;

    CertificateDomainBO findDomain(CertificateDetails certificateDetails) throws TechnicalException;

    void changeCertificateDomain(X509Certificate newCertificate, CertificateBO oldCertificateBO, CertificateBO newCertificateBO, boolean isToCreateNewCertificateDomain) throws TechnicalException;

    List<CertificateDomainBO> findAll() throws TechnicalException;

    void changeCertificateDomain(CertificateBO oldCertificateBO, CertificateBO newcertificateBO, X509Certificate newDomainCertificate, boolean isToCreateNewCertificateDomain) throws TechnicalException;

    CertificateDomainBO deleteCertificateDomain(CertificateBO oldCertificateBO) throws TechnicalException;

    CertificateDomainBO deleteCertificateDomain(CertificateDomainBO certificateDomainBO) throws TechnicalException;

    boolean isCertificateDomainNonRootCA(CertificateBO certificateBO) throws TechnicalException;

    CertificateDomainBO addCertificateDomain(X509Certificate newCertificate, SubdomainBO subdomainBO, boolean isRootCertificate, boolean isAdminCertificate, String truststoreAlias) throws TechnicalException;

    CertificateDomainBO updateCertificateDomain(CertificateDomainBO certificate, SubdomainBO newSubdomainBO, String newCrlUrl, Boolean isAdminCertificate) throws TechnicalException;

    CertificateDomainBO deleteCertificateDomain(CertificateDomainBO certificate, SubdomainBO newSubdomainBO) throws TechnicalException;

    List<CertificateDomainBO> listCertificateDomains(String certificate, SubdomainBO newSubdomainBO) throws TechnicalException;

}
