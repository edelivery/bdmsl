/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.config.properties;

import eu.europa.ec.bdmsl.common.enums.SMLPropertyEnum;
import eu.europa.ec.bdmsl.config.TaskSchedulerConfig;
import eu.europa.ec.bdmsl.cron.DynamicCronTrigger;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.scheduling.support.CronExpression;
import org.springframework.stereotype.Component;

import java.util.*;


/**
 * Property change listener for cron expression. Component updates crone version for the trigger with matching
 * Cron Expression Property
 *
 * @author Joze RIHTARSIC
 * @since 4.2
 */
@Component
public class CronExpressionPropertyUpdateListener implements PropertyUpdateListener {
    private static final Log LOG = LogFactory.getLog(CronExpressionPropertyUpdateListener.class);

    final List<DynamicCronTrigger> smpDynamicCronTriggerList;

    TaskSchedulerConfig taskSchedulerConfig;

    public CronExpressionPropertyUpdateListener(Optional<List<DynamicCronTrigger>> cronTriggerList,
                                                TaskSchedulerConfig taskSchedulerConfig
    ) {
        this.smpDynamicCronTriggerList = cronTriggerList.orElse(Collections.emptyList());
        this.taskSchedulerConfig = taskSchedulerConfig;
    }

    @Override
    public void updateProperties(Map<SMLPropertyEnum, Object> properties) {
        if (smpDynamicCronTriggerList.isEmpty()) {
            LOG.warn("No cron trigger bean is configured!");
            return;
        }
        // update cron expressions!
        boolean cronExpressionChanged = false;
        for (DynamicCronTrigger trigger : smpDynamicCronTriggerList) {
            // check if updated properties contains value for the cron trigger
            if (!properties.containsKey(trigger.getCronExpressionProperty())) {
                LOG.debug("Update cron properties does not contain change for cron [" + trigger.getCronExpressionProperty() + "]");
                continue;
            }
            // check if cron was changed
            CronExpression newCronExpression = (CronExpression) properties.get(trigger.getCronExpressionProperty());
            if (newCronExpression == null) {
                LOG.debug("New cron expression for property:  [" + trigger.getCronExpressionProperty() + "] is not set!, skip re-setting the cron!");
                continue;
            }

            if (StringUtils.equalsIgnoreCase(trigger.getExpression(), newCronExpression.toString())) {
                LOG.debug("Cron expression did not changed for cron: [" + trigger.getCronExpressionProperty() + "], skip re-setting the cron!");
                continue;
            }
            LOG.info("Change expression from [" + trigger.getExpression() + "] to ["
                    + newCronExpression.toString() + "] for property: [" + trigger.getCronExpressionProperty() + "]!");

            trigger.updateCronExpression((CronExpression)
                    properties.get(trigger.getCronExpressionProperty()));
            cronExpressionChanged = true;
        }

        if (cronExpressionChanged) {
            LOG.debug("One of monitored cron expression changed! Reset the cron task configuration!");
            taskSchedulerConfig.updateCronTasks();
        }
    }

    @Override
    public List<SMLPropertyEnum> handledProperties() {
        return Arrays.asList(SMLPropertyEnum.SML_PROPERTY_REFRESH_CRON,
                SMLPropertyEnum.CERT_CHANGE_CRON,
                SMLPropertyEnum.DIA_CRON,
                SMLPropertyEnum.REPORT_EXPIRED_CERT_CRON);
    }
}
