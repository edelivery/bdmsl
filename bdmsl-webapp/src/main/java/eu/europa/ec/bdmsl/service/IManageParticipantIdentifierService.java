/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service;

import eu.europa.ec.bdmsl.business.IManageParticipantIdentifierBusiness;
import eu.europa.ec.bdmsl.common.bo.MigrationRecordBO;
import eu.europa.ec.bdmsl.common.bo.PageRequestBO;
import eu.europa.ec.bdmsl.common.bo.ParticipantBO;
import eu.europa.ec.bdmsl.common.bo.ParticipantListBO;

import eu.europa.ec.bdmsl.common.exception.TechnicalException;

import java.util.List;

/**
 * @author Adrien FERIAL
 * @since 12/06/2015
 */
public interface IManageParticipantIdentifierService {
    ParticipantListBO list(PageRequestBO pageRequestBO) throws
             TechnicalException;

    void create(ParticipantBO participantBO) throws
             TechnicalException;

    void createList(ParticipantListBO participantListBO) throws
             TechnicalException;

    void delete(ParticipantBO participantBO) throws
             TechnicalException;

    ParticipantBO getParticipant(ParticipantBO participantBO) throws
             TechnicalException;

    void deleteList(ParticipantListBO participantListBO) throws
             TechnicalException;

    void prepareToMigrate(MigrationRecordBO prepareToMigrateBO) throws
             TechnicalException;

    void migrate(MigrationRecordBO migrateBO) throws
             TechnicalException;

    void delete(String smpId, List<ParticipantBO> participantBOList) throws  TechnicalException;

    void deleteDNSForMissingParticipants(ParticipantListBO participantListBO);

    IManageParticipantIdentifierBusiness getManageParticipantIdentifierBusiness();
}
