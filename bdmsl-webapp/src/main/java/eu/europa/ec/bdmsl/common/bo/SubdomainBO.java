/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.bo;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Tiago MIGUEL
 * @since 02/03/2017
 */
public class SubdomainBO extends AbstractBusinessObject {

    private Long subdomainId;
    private String subdomainName;
    private String description;
    private String dnsZone;

    private String participantIdRegexp;
    private String dnsRecordTypes;
    private String smpUrlSchemas;
    private String smpCertSubjectRegex;
    private String smpCertPolicyOIDs;
    private BigInteger maxParticipantCountForDomain;
    private BigInteger maxParticipantCountForSMP;

    public String getDnsZone() {
        return dnsZone;
    }

    public void setDnsZone(String dnsZone) {
        this.dnsZone = dnsZone;
    }

    public String getParticipantIdRegexp() {
        return participantIdRegexp;
    }

    public void setParticipantIdRegexp(String participantIdRegexp) {
        this.participantIdRegexp = participantIdRegexp;
    }

    public String getSmpCertPolicyOIDs() {
        return smpCertPolicyOIDs;
    }

    public void setSmpCertPolicyOIDs(String smpCertPolicyOIDs) {
        this.smpCertPolicyOIDs = smpCertPolicyOIDs;
    }

    public List<String> getSmpCertPolicyOIDsAsList() {
        return StringUtils.isBlank(smpCertPolicyOIDs) ? Collections.emptyList() :
                Arrays.stream(StringUtils.split(smpCertPolicyOIDs, ","))
                        .map(String::trim).collect(Collectors.toList());
    }

    public String getDnsRecordTypes() {
        return dnsRecordTypes;
    }

    public void setDnsRecordTypes(String dnsRecordTypes) {
        this.dnsRecordTypes = dnsRecordTypes;
    }

    public String getSmpUrlSchemas() {
        return smpUrlSchemas;
    }

    public void setSmpUrlSchemas(String smpUrlSchemas) {
        this.smpUrlSchemas = smpUrlSchemas;
    }

    public Long getSubdomainId() {
        return subdomainId;
    }

    public void setSubdomainId(Long subdomainId) {
        this.subdomainId = subdomainId;
    }

    public String getSubdomainName() {
        return subdomainName;
    }

    public void setSubdomainName(String subdomainName) {
        this.subdomainName = subdomainName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSmpCertSubjectRegex() {
        return smpCertSubjectRegex;
    }

    public void setSmpCertSubjectRegex(String smpCertSubjectRegex) {
        this.smpCertSubjectRegex = smpCertSubjectRegex;
    }

    public BigInteger getMaxParticipantCountForDomain() {
        return maxParticipantCountForDomain;
    }

    public void setMaxParticipantCountForDomain(BigInteger maxParticipantCountForDomain) {
        this.maxParticipantCountForDomain = maxParticipantCountForDomain;
    }

    public BigInteger getMaxParticipantCountForSMP() {
        return maxParticipantCountForSMP;
    }

    public void setMaxParticipantCountForSMP(BigInteger maxParticipantCountForSMP) {
        this.maxParticipantCountForSMP = maxParticipantCountForSMP;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SubdomainBO)) return false;

        SubdomainBO that = (SubdomainBO) o;

        return new EqualsBuilder()
                .append(subdomainId, that.getSubdomainId())
                .append(subdomainName, that.getSubdomainName())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(subdomainId)
                .append(subdomainName)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("subdomainId", subdomainId)
                .append("subdomainName", subdomainName)
                .toString();
    }
}
