/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.ws;

import eu.europa.ec.bdmsl.config.PropertiesConfig;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.WebApplicationInitializer;

import jakarta.servlet.ServletContext;
import java.util.Collections;
import java.util.Properties;

/**
 * Purpose of this class is to programmatically setup JSP servlet for redirecting JSP pages
 * in web.xml. JSP servlet is application server dependant.
 */
public class SMLWebApplicationInitializer implements WebApplicationInitializer {

    private static Logger LOG = LogManager.getLogger(SMLWebApplicationInitializer.class);


    @Override
    public void onStartup(ServletContext container) {

        String jspServletClass = getJSPServletName(container);
        if (!StringUtils.isBlank(jspServletClass)) {
            LOG.info("Register jsp servlet for SML: [" + jspServletClass + "]");
            container.addServlet("smlJSPServlet", jspServletClass);
        } else {
            LOG.error("Define JSP servlet in file properties: [" + PropertiesConfig.PROPERTY_JSP_SERVLET + "].");
        }

        LOG.info("Define session tracking modes: [NONE]");
        container.setSessionTrackingModes(Collections.emptySet());
    }

    public String getJSPServletName(ServletContext container) {
        String jspServletClass = null;
        Properties fileProperties = PropertiesConfig.getFileProperties(null);

        if (fileProperties.containsKey(PropertiesConfig.PROPERTY_JSP_SERVLET))
            jspServletClass = fileProperties.getProperty(PropertiesConfig.PROPERTY_JSP_SERVLET);
        LOG.debug("Register JSP servlet from property:" + jspServletClass);
        if (container.getServletRegistrations().containsKey("jsp")) {
            jspServletClass = container.getServletRegistrations().get("jsp").getClassName();
            LOG.debug("Register default 'jsp' servlet:" + jspServletClass);
        } else if (container.getServletRegistrations().containsKey("JSP")) {
            jspServletClass = container.getServletRegistrations().get("JSP").getClassName();
            LOG.debug("Register default 'JSP' servlet:" + jspServletClass);
        }
        return jspServletClass;

    }
}
