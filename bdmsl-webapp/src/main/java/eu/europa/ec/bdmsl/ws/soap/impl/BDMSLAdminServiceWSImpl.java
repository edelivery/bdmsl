/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.ws.soap.impl;

import ec.services.wsdl.bdmsl.admin.data._1.*;
import eu.europa.ec.bdmsl.common.bo.*;
import eu.europa.ec.bdmsl.common.enums.AdminParticipantIdentifierManageActionEnum;
import eu.europa.ec.bdmsl.common.exception.BadRequestException;
import eu.europa.ec.bdmsl.common.exception.Severity;
import eu.europa.ec.bdmsl.common.util.LogEvents;
import eu.europa.ec.bdmsl.service.IBDMSLAdminService;
import eu.europa.ec.bdmsl.service.IManageCertificateService;
import eu.europa.ec.bdmsl.util.JaxbUtils;
import eu.europa.ec.bdmsl.ws.soap.*;
import jakarta.jws.WebService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.EnumSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static eu.europa.ec.bdmsl.common.exception.Messages.*;
import static org.apache.commons.lang3.StringUtils.trim;


@Service
@WebService(endpointInterface = "eu.europa.ec.bdmsl.ws.soap.IBDMSLAdminServiceWS")
public class BDMSLAdminServiceWSImpl extends AbstractWSImpl implements IBDMSLAdminServiceWS {

    @Autowired
    private IManageCertificateService manageCertificateService;

    @Autowired
    private IBDMSLAdminService bdmslAdminService;

    @Autowired
    private ConversionService conversionService;

    @Override
    public String generateReport(GenerateReport messagePart) throws InternalErrorFault, UnauthorizedFault, BadRequestFault {
        loggingService.info("Calling BDMSLAdminServiceWSImpl.generateReport");
        String result = null;
        try {
            if (messagePart == null) {
                throw new BadRequestException(BAD_REQUEST_NULL);
            }
            result = bdmslAdminService.generateReport(messagePart.getReportCode(), messagePart.getReceiverEmailAddress());
        } catch (Exception e) {
            loggingService.error(e.getMessage(), e);
            handleExceptionExcludingNotFoundFault(e);
        }
        return result;
    }

    @Override
    public String generateInconsistencyReport(GenerateInconsistencyReport messagePart) throws InternalErrorFault, UnauthorizedFault, BadRequestFault {
        loggingService.info("Calling BDMSLAdminServiceWSImpl.generateInconsistencyReport");
        String result = null;
        try {
            if (messagePart == null) {
                throw new BadRequestException(BAD_REQUEST_NULL);
            }
            result = bdmslAdminService.generateInconsistencyReport(messagePart.getReceiverEmailAddress());
        } catch (Exception e) {
            loggingService.error(e.getMessage(), e);
            handleExceptionExcludingNotFoundFault(e);
        }
        return result;
    }

    @Override
    public SubDomainType getSubDomain(GetSubDomainRequest messagePart) throws InternalErrorFault, UnauthorizedFault, NotFoundFault, BadRequestFault {
        loggingService.info("Calling BDMSLAdminServiceWSImpl.getSubDomain");
        SubDomainType result = null;
        try {
            if (messagePart == null) {
                throw new BadRequestException(BAD_REQUEST_NULL);
            }

            SubdomainBO subdomainBO = bdmslAdminService.getSubDomain(
                    StringUtils.trimToNull(messagePart.getSubDomainName())
            );
            result = conversionService.convert(subdomainBO, SubDomainType.class);
        } catch (Exception e) {
            loggingService.error(e.getMessage(), e);
            handleException(e);
        }
        return result;
    }

    @Override
    public SubDomainType deleteSubDomain(DeleteSubDomainRequest deleteSubDomainType) throws InternalErrorFault, UnauthorizedFault, NotFoundFault, BadRequestFault {
        loggingService.info("Calling BDMSLAdminServiceWSImpl.deleteSubDomain");
        SubDomainType result = null;
        try {
            if (deleteSubDomainType == null) {
                throw new BadRequestException(BAD_REQUEST_NULL);
            }
            SubdomainBO subdomainBO = bdmslAdminService.deleteSubDomain(
                    StringUtils.trimToNull(deleteSubDomainType.getSubDomainName())
            );
            result = conversionService.convert(subdomainBO, SubDomainType.class);
        } catch (Exception e) {
            loggingService.error(e.getMessage(), e);
            handleException(e);
        }
        return result;
    }


    @Override
    public SubDomainType createSubDomain(SubDomainType subDomainType) throws InternalErrorFault, UnauthorizedFault, BadRequestFault {
        loggingService.info("Calling BDMSLAdminServiceWSImpl.createSubDomain");
        SubDomainType result = null;
        try {
            if (subDomainType == null) {
                throw new BadRequestException(BAD_REQUEST_NULL);
            }
            SubdomainBO subdomainBO = conversionService.convert(subDomainType, SubdomainBO.class);
            SubdomainBO subdomainBORes = bdmslAdminService.createSubDomain(subdomainBO);
            result = conversionService.convert(subdomainBORes, SubDomainType.class);
        } catch (Exception e) {
            loggingService.error(e.getMessage(), e);
            handleExceptionExcludingNotFoundFault(e);
        }
        return result;
    }

    @Override
    public SubDomainType updateSubDomain(UpdateSubDomainRequest updateSubDomain)
            throws InternalErrorFault, UnauthorizedFault, NotFoundFault, BadRequestFault {
        loggingService.info("Calling BDMSLAdminServiceWSImpl.updateSubDomain");
        SubDomainType result = null;
        try {
            if (updateSubDomain == null) {
                throw new BadRequestException(BAD_REQUEST_NULL);
            }
            SubdomainBO subdomainBORes = bdmslAdminService.updateSubDomain(
                    trim(updateSubDomain.getSubDomainName()),
                    trim(updateSubDomain.getParticipantRegularExpression()),
                    trim(updateSubDomain.getDNSRecordType()),
                    trim(updateSubDomain.getSmpUrlScheme()),
                    trim(updateSubDomain.getCertSubjectRegularExpression()),
                    trim(updateSubDomain.getCertPolicyOIDs()),
                    updateSubDomain.getMaxParticipantCountForDomain(),
                    updateSubDomain.getMaxParticipantCountForSMP()
            );
            result = conversionService.convert(subdomainBORes, SubDomainType.class);
        } catch (Exception e) {
            loggingService.error(e.getMessage(), e);
            handleException(e);
        }
        return result;
    }


    @Override
    public DomainCertificateType addSubDomainCertificate(AddSubDomainCertificateRequest messagePart) throws InternalErrorFault, UnauthorizedFault, BadRequestFault, NotFoundFault {
        loggingService.info("Calling BDMSLAdminServiceWSImpl.addSubDomainCertificate");
        DomainCertificateType result = null;
        try {
            if (messagePart == null) {
                throw new BadRequestException(BAD_REQUEST_NULL);
            }
            CertificateDomainBO certificateDomainBO = bdmslAdminService.addCertificateDomain(
                    messagePart.getCertificatePublicKey(),
                    trim(messagePart.getSubDomainName()),
                    messagePart.isIsRootCertificate(),
                    messagePart.isIsAdminCertificate(),
                    messagePart.getAlias()
            );
            result = conversionService.convert(certificateDomainBO, DomainCertificateType.class);
        } catch (Exception e) {
            loggingService.error(e.getMessage(), e);
            handleException(e);
        }
        return result;
    }

    @Override
    public DomainCertificateType updateSubDomainCertificate(UpdateSubDomainCertificateRequest messagePart) throws InternalErrorFault, UnauthorizedFault, NotFoundFault, BadRequestFault {
        loggingService.info("Calling BDMSLAdminServiceWSImpl.updateSubDomainCertificate");
        DomainCertificateType result = null;
        try {
            if (messagePart == null) {
                throw new BadRequestException(BAD_REQUEST_NULL);
            }
            CertificateDomainBO certificateDomainBO = bdmslAdminService.updateCertificateDomain(
                    messagePart.getCertificateDomainId(),
                    trim(messagePart.getSubDomainName()),
                    trim(messagePart.getCrlDistributionPoint()),
                    messagePart.isIsAdminCertificate()
            );
            loggingService.info("Calling BDMSLAdminServiceWSImpl.updateSubDomainCertificate after" + certificateDomainBO.isAdmin());
            DomainCertificateType dct = conversionService.convert(certificateDomainBO, DomainCertificateType.class);
            loggingService.info("Calling BDMSLAdminServiceWSImpl.updateSubDomainCertificate after msp" + dct.isIsAdminCertificate());
            result = conversionService.convert(certificateDomainBO, DomainCertificateType.class);
        } catch (Exception e) {
            loggingService.error(e.getMessage(), e);
            handleException(e);
        }
        return result;
    }

    @Override
    public DomainCertificateType deleteSubDomainCertificate(DeleteSubDomainCertificateRequest messagePart) throws InternalErrorFault, UnauthorizedFault, BadRequestFault, NotFoundFault {
        loggingService.info("Calling BDMSLAdminServiceWSImpl.deleteSubDomainCertificate");
        DomainCertificateType result = null;
        try {
            if (messagePart == null) {
                throw new BadRequestException(BAD_REQUEST_NULL);
            }

            CertificateDomainBO certificateDomainBO = bdmslAdminService.deleteCertificateDomain(
                    messagePart.getCertificateDomainId(),
                    trim(messagePart.getSubDomainName())
            );
            result = conversionService.convert(certificateDomainBO, DomainCertificateType.class);
        } catch (Exception e) {
            loggingService.error(e.getMessage(), e);
            handleException(e);
        }
        return result;
    }

    @Override
    public ListSubDomainCertificateResponse listSubDomainCertificate(
            ListSubDomainCertificateRequest messagePart)
            throws InternalErrorFault, UnauthorizedFault, NotFoundFault, BadRequestFault {
        ListSubDomainCertificateResponse result = null;
        try {
            if (messagePart == null) {
                throw new BadRequestException(BAD_REQUEST_NULL);
            }

            List<CertificateDomainBO> certificateDomainBO = bdmslAdminService.listCertificateDomains(
                    messagePart.getCertificateDomainId(),
                    StringUtils.trimToNull(messagePart.getSubDomainName())
            );

            List<DomainCertificateType> dom = certificateDomainBO.stream().map(migrateEntity ->
                    conversionService.convert(migrateEntity, DomainCertificateType.class)).collect(Collectors.toList());

            ListSubDomainCertificateResponse listDomainCertificateResponseType = new ListSubDomainCertificateResponse();
            listDomainCertificateResponseType.getDomainCertificateTypes().addAll(dom);
            result = listDomainCertificateResponseType;
        } catch (Exception e) {
            loggingService.error(e.getMessage(), e);
            handleException(e);
        }
        return result;
    }

    @Override
    public TruststoreCertificateType addTruststoreCertificate(TruststoreCertificateType messagePart) throws InternalErrorFault, UnauthorizedFault, BadRequestFault {
        TruststoreCertificateType result = null;
        try {
            if (messagePart == null) {
                throw new BadRequestException(BAD_REQUEST_NULL);
            }
            if (messagePart.getCertificatePublicKey() == null) {
                throw new BadRequestException(BAD_REQUEST_NULL);
            }

            TruststoreCertificateBO truststoreCertificateBO = bdmslAdminService.addCertificateToTruststore(
                    messagePart.getCertificatePublicKey(), messagePart.getAlias());
            result = conversionService.convert(truststoreCertificateBO, TruststoreCertificateType.class);
        } catch (Exception e) {
            loggingService.error(e.getMessage(), e);
            handleExceptionExcludingNotFoundFault(e);
        }
        return result;
    }

    @Override
    public TruststoreCertificateType deleteTruststoreCertificate(DeleteTruststoreCertificateRequest messagePart) throws InternalErrorFault, UnauthorizedFault, NotFoundFault, BadRequestFault {
        TruststoreCertificateType result = null;
        try {
            if (messagePart == null) {
                throw new BadRequestException(BAD_REQUEST_NULL);
            }
            if (StringUtils.isBlank(messagePart.getAlias())) {
                throw new BadRequestException(BAD_REQUEST_NULL);
            }

            TruststoreCertificateBO truststoreCertificateBO = bdmslAdminService.deleteCertificateFromTruststore(
                    trim(messagePart.getAlias()));
            result = conversionService.convert(truststoreCertificateBO, TruststoreCertificateType.class);
        } catch (Exception e) {
            loggingService.error(e.getMessage(), e);
            handleException(e);
        }
        return result;
    }

    @Override
    public TruststoreCertificateType getTruststoreCertificate(GetTruststoreCertificateRequest messagePart) throws InternalErrorFault, UnauthorizedFault, NotFoundFault, BadRequestFault {
        TruststoreCertificateType result = null;
        try {
            if (messagePart == null) {
                throw new BadRequestException(BAD_REQUEST_NULL);
            }
            if (StringUtils.isBlank(messagePart.getAlias())) {
                throw new BadRequestException(BAD_REQUEST_NULL);
            }

            TruststoreCertificateBO truststoreCertificateBO = bdmslAdminService.getCertificateFromTruststore(
                    trim(messagePart.getAlias()));
            result = conversionService.convert(truststoreCertificateBO, TruststoreCertificateType.class);
        } catch (Exception e) {
            loggingService.error(e.getMessage(), e);
            handleException(e);
        }
        return result;
    }

    @Override
    public ListTruststoreCertificateAliasesResponse listTruststoreCertificateAliases(ListTruststoreCertificateAliasesRequest messagePart) throws InternalErrorFault, UnauthorizedFault, BadRequestFault {
        ListTruststoreCertificateAliasesResponse responseType = null;
        try {
            if (messagePart == null) {
                throw new BadRequestException(BAD_REQUEST_NULL);
            }

            List<String> lstAliases = bdmslAdminService.listTruststoreCertificateAliases(messagePart.getContainsStringInAlias());
            responseType = new ListTruststoreCertificateAliasesResponse();
            responseType.getAlias().addAll(lstAliases);
        } catch (Exception e) {
            loggingService.error(e.getMessage(), e);
            handleExceptionExcludingNotFoundFault(e);
        }
        return responseType;
    }

    @Override
    public DeleteDNSRecordResponse deleteDNSRecord(DeleteDNSRecordRequest messagePart) throws InternalErrorFault, UnauthorizedFault, NotFoundFault, BadRequestFault {

        DeleteDNSRecordResponse result = new DeleteDNSRecordResponse();
        loggingService.info("Calling BDMSLAdminServiceWSImpl.deleteDNSRecord: " + (messagePart != null ? messagePart.getName() : "null") + " dnsZone: " + (messagePart != null ? messagePart.getDNSZone() : "null"));
        try {
            if (messagePart == null) {
                throw new BadRequestException(BAD_REQUEST_NULL);
            }
            List<DNSRecordBO> dnsRecordBOList = bdmslAdminService.deleteDNSRecord(messagePart.getName(), messagePart.getDNSZone());

            List<DNSRecordType> dnsRecordTypeList = dnsRecordBOList.stream().map(dnsRecordBO ->
                    conversionService.convert(dnsRecordBO, DNSRecordType.class)).collect(Collectors.toList());
            result.getDNSRecords().addAll(dnsRecordTypeList);
        } catch (Exception e) {
            loggingService.error(e.getMessage(), e);
            handleException(e);
        }
        return result;
    }

    @Override
    public DNSRecordType addDNSRecord(DNSRecordType dnsRecordType) throws InternalErrorFault, UnauthorizedFault, NotFoundFault, BadRequestFault {
        loggingService.info("Calling BDMSLAdminServiceWSImpl.addDNSRecord: " + (dnsRecordType != null ? dnsRecordType.getName() : "null") + " dnsZone: " + (dnsRecordType != null ? dnsRecordType.getDNSZone() : "null"));
        DNSRecordType result = null;
        try {
            if (dnsRecordType == null) {
                throw new BadRequestException(BAD_REQUEST_NULL);
            }
            DNSRecordBO recordBO = conversionService.convert(dnsRecordType, DNSRecordBO.class);
            loggingService.info("BDMSLAdminServiceWSImpl.addDNSRecord:  Record: " + recordBO);
            DNSRecordBO dnsRecordBO = bdmslAdminService.addDNSRecord(recordBO);
            result = conversionService.convert(dnsRecordBO, DNSRecordType.class);
        } catch (Exception e) {
            loggingService.error(e.getMessage(), e);
            handleException(e);
        }
        return result;
    }

    @Override
    public PropertyType setProperty(PropertyType messagePart) throws InternalErrorFault, UnauthorizedFault, NotFoundFault, BadRequestFault {
        // do not print value - it could be password!);
        loggingService.info("Calling BDMSLAdminServiceWSImpl.setProperty: " + (messagePart != null ? messagePart.getKey() : "null"));
        PropertyType result = null;
        try {
            if (messagePart == null) {
                throw new BadRequestException(BAD_REQUEST_NULL);
            }
            ConfigurationBO configurationBo = bdmslAdminService.setPropertyToDatabase(StringUtils.trimToNull(messagePart.getKey()),
                    trim(messagePart.getValue()),
                    StringUtils.trimToNull(messagePart.getDescription()));

            result = conversionService.convert(configurationBo, PropertyType.class);
        } catch (Exception e) {
            loggingService.error(e.getMessage(), e);
            handleException(e);
        }
        return result;
    }

    @Override
    public PropertyType getProperty(PropertyKeyType messagePart) throws InternalErrorFault, UnauthorizedFault, NotFoundFault, BadRequestFault {

        loggingService.info("Calling BDMSLAdminServiceWSImpl.getProperty: " + (messagePart != null ? messagePart.getKey() : "null"));
        PropertyType result = null;
        try {
            if (messagePart == null) {
                throw new BadRequestException(BAD_REQUEST_NULL);
            }
            ConfigurationBO configurationBo = bdmslAdminService.getPropertyFromDatabase(StringUtils.trimToNull(messagePart.getKey()));

            result = conversionService.convert(configurationBo, PropertyType.class);
        } catch (Exception e) {
            loggingService.error(e.getMessage(), e);
            handleException(e);
        }
        return result;
    }

    @Override
    public PropertyType deleteProperty(PropertyKeyType messagePart) throws InternalErrorFault, UnauthorizedFault, NotFoundFault, BadRequestFault {

        loggingService.info("Calling BDMSLAdminServiceWSImpl.deleteProperty: " + (messagePart != null ? messagePart.getKey() : "null"));
        PropertyType result = null;
        try {
            if (messagePart == null) {
                throw new BadRequestException(BAD_REQUEST_NULL);
            }
            ConfigurationBO configurationBo = bdmslAdminService.deletePropertyFromDatabase(StringUtils.trimToNull(messagePart.getKey()));
            result = conversionService.convert(configurationBo, PropertyType.class);
        } catch (Exception e) {
            loggingService.error(e.getMessage(), e);
            handleException(e);
        }
        return result;
    }

    @Override
    public void changeCertificate(
            ChangeCertificate ChangeCertificate
    ) throws InternalErrorFault, UnauthorizedFault, BadRequestFault {
        loggingService.info("Calling BDMSLAdminServiceWSImpl.changeCertificate with ChangeCertificate=" + (ChangeCertificate != null ? JaxbUtils.serializeJaxbEntity(ChangeCertificate) : "null"));
        try {
            if (ChangeCertificate == null) {
                throw new BadRequestException(BAD_REQUEST_NULL);
            }
            ChangeCertificateBO changeCertificateBO = conversionService.convert(ChangeCertificate, ChangeCertificateBO.class);
            manageCertificateService.changeCertificate(changeCertificateBO);
            loggingService.businessLog(LogEvents.BUS_CERTIFICATE_CHANGE_SERVICE_SUCCESS, changeCertificateBO.getServiceMetadataPublisherID());
        } catch (Exception exc) {
            // convert the exception to the associated SOAP fault
            loggingService.businessLog(Severity.ERROR, LogEvents.BUS_CERTIFICATE_CHANGE_SERVICE_FAILED, SecurityContextHolder.getContext().getAuthentication().getName());
            loggingService.error(exc.getMessage(), exc);
            handleExceptionExcludingNotFoundFault(exc);
        }
    }

    @Override
    public void clearCache(
            ClearCache messagePart
    ) throws InternalErrorFault, UnauthorizedFault, BadRequestFault {
        loggingService.info("Calling BDMSLAdminServiceWSImpl.clearCache");
        try {
            if (messagePart == null) {
                throw new BadRequestException(BAD_REQUEST_NULL);
            }
            bdmslAdminService.clearCache();
            loggingService.info("Call to BDMSLAdminServiceWSImpl.clearCache ended successfully");
        } catch (Exception e) {
            loggingService.error(e.getMessage(), e);
            handleExceptionExcludingNotFoundFault(e);
        }
    }

    @Override
    public ManageServiceMetadataPublisherResponse manageServiceMetadataPublisher(ManageServiceMetadataPublisherType messagePart) throws InternalErrorFault, UnauthorizedFault, NotFoundFault, BadRequestFault {
        loggingService.info("Calling BDMSLAdminServiceWSImpl.manageServiceMetadataPublisher");
        ManageServiceMetadataPublisherResponse result = null;
        try {
            if (messagePart == null) {
                throw new BadRequestException(BAD_REQUEST_NULL);
            }

            String resultMsg = bdmslAdminService.manageServiceMetadataPublisher(messagePart.getAction(),
                    messagePart.getReceiverEmailAddress(),
                    messagePart.getServiceMetadataPublisherID(), messagePart.getDNSZone(), messagePart.getSmpOwnerCertificateId(),
                    messagePart.getLogicalAddress(), messagePart.getPhysicalAddress());

            result = new ManageServiceMetadataPublisherResponse();
            result.setAction(messagePart.getAction());
            result.setReceiverEmailAddress(messagePart.getReceiverEmailAddress());
            result.setServiceMetadataPublisherID(messagePart.getServiceMetadataPublisherID());
            result.setDNSZone(messagePart.getDNSZone());
            result.setSmpOwnerCertificateId(messagePart.getSmpOwnerCertificateId());
            result.setStatus(resultMsg);
        } catch (Exception e) {
            loggingService.error(e.getMessage(), e);
            handleExceptionExcludingNotFoundFault(e);
        }
        return result;
    }

    @Override
    public ManageParticipantIdentifierResponse manageParticipantIdentifier(ManageParticipantIdentifierType messagePart) throws InternalErrorFault, UnauthorizedFault, NotFoundFault, BadRequestFault {
        loggingService.info("Calling BDMSLAdminServiceWSImpl.manageServiceMetadataPublisher");
        ManageParticipantIdentifierResponse result = null;
        try {
            AdminParticipantIdentifierManageActionEnum action = validateManageParticipantIdentifierType(messagePart);

            // convert the ParticipantIdentifierType to ParticipantBO
            List<ParticipantBO> participantBOList = messagePart.getParticipantIdentifiers().stream()
                    .map(participantIdentifierType ->
                            conversionService.convert(participantIdentifierType, ParticipantBO.class))
                    .filter(Objects::nonNull)
                    .peek(participantBO -> participantBO.setSmpId(messagePart.getServiceMetadataPublisherID()))
                    .collect(Collectors.toList());


            String resultMsg = bdmslAdminService.manageParticipantIdentifiers(action,
                    messagePart.getServiceMetadataPublisherID(),
                    participantBOList);

            result = new ManageParticipantIdentifierResponse();
            result.setAction(messagePart.getAction());
            result.setServiceMetadataPublisherID(messagePart.getServiceMetadataPublisherID());
            result.getParticipantIdentifiers().addAll(messagePart.getParticipantIdentifiers());
            result.setStatus(resultMsg);

        } catch (Exception e) {
            loggingService.error(e.getMessage(), e);
            handleException(e);
        }
        return result;
    }

    private AdminParticipantIdentifierManageActionEnum validateManageParticipantIdentifierType(ManageParticipantIdentifierType request) throws BadRequestException {
        if (request == null) {
            throw new BadRequestException(BAD_REQUEST_NULL);
        }

        AdminParticipantIdentifierManageActionEnum action = AdminParticipantIdentifierManageActionEnum.getByName(request.getAction())
                .orElseThrow(() ->
                        new BadRequestException(BAD_REQUEST_PARAMETER_INVALID_ENUM,
                                "Action",
                                request.getAction(),
                                EnumSet.allOf(AdminParticipantIdentifierManageActionEnum.class).stream().map(Enum::name).collect(Collectors.joining(", "))));
        if (StringUtils.isBlank(request.getServiceMetadataPublisherID())) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_EMPTY, "ServiceMetadataPublisherID");
        }
        if (request.getParticipantIdentifiers() == null || request.getParticipantIdentifiers().isEmpty()) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_EMPTY, "ParticipantIdentifiers");
        }
        return action;
    }

}
