/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.business.impl;

import eu.europa.ec.bdmsl.business.IConfigurationBusiness;
import eu.europa.ec.bdmsl.common.bo.ConfigurationBO;
import eu.europa.ec.bdmsl.common.business.AbstractBusinessImpl;
import eu.europa.ec.bdmsl.common.enums.SMLPropertyEnum;
import eu.europa.ec.bdmsl.common.enums.SMLPropertyTypeEnum;
import eu.europa.ec.bdmsl.common.exception.BadRequestException;
import eu.europa.ec.bdmsl.common.exception.KeyException;
import eu.europa.ec.bdmsl.common.exception.BadConfigurationException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.common.util.KeyUtil;
import eu.europa.ec.bdmsl.dao.IConfigurationDAO;
import eu.europa.ec.bdmsl.service.validation.SMLPropertyValidator;
import eu.europa.ec.edelivery.security.cert.impl.CertificateRVStrategyEnum;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.*;
import java.util.regex.Pattern;

import static eu.europa.ec.bdmsl.common.enums.SMLPropertyEnum.*;

/**
 * @author Flavio SANTOS
 * @since 08/05/2017
 */
@Component
public class ConfigurationBusinessImpl extends AbstractBusinessImpl implements IConfigurationBusiness {
    private static final Logger LOG = LoggerFactory.getLogger(ConfigurationBusinessImpl.class);

    private final IConfigurationDAO configurationDAO;

    private final List<SMLPropertyValidator> propertyValidators;

    public ConfigurationBusinessImpl(IConfigurationDAO configurationDAO, List<SMLPropertyValidator> propertyValidators) {
        this.configurationDAO = configurationDAO;
        this.propertyValidators = propertyValidators;
    }

    @Override
    public ConfigurationBO findConfigurationProperty(String propertyName) throws TechnicalException {
        return configurationDAO.findConfigurationProperty(propertyName);
    }

    @Override
    public ConfigurationBO setPropertyToDatabase(SMLPropertyEnum key, String value, String description) throws TechnicalException {
        validateProperty(key, value);

        String finalValue = StringUtils.trim(value);
        if (Objects.equals(key.getPropertyType(), SMLPropertyTypeEnum.BOOLEAN)) {
            finalValue = finalValue.toLowerCase();
        }

        // encrypt file
        if (key.isEncrypted()) {
            if (key == DNS_TSIG_KEY_VALUE) {
                finalValue = encryptBase64Value(value);
            } else {
                finalValue = encryptString(value);
            }

        }
        ConfigurationBO res = configurationDAO.setPropertyToDatabase(key, finalValue, description);
        if (key.isEncrypted()) {
            res.setValue("*******");
        }
        // is is not cluster deployment update properties right-away.
        // else cron updates at the same time all nodes
        if (!isDeployedOnCluster()) {
            configurationDAO.forceRefreshProperties();
        }
        return res;
    }

    private void validateProperty(SMLPropertyEnum key, String value) throws BadRequestException {
        if (propertyValidators == null ) {
            LOG.debug("No SML property validators registered!");
            return;
        }

        String rootFolder = getConfigurationFolder();
        rootFolder = StringUtils.defaultString(rootFolder, "./");
        File configFolder = new File(rootFolder);

        try {
            for (SMLPropertyValidator validator : propertyValidators) {
                if (validator.supports(key)) {
                    validator.validate(key, value, configFolder);
                }
            }
        } catch (BadConfigurationException e) {
            throw new BadRequestException(String.format("The property [%s] of type [%s] has an invalid value [%s]! Error: %s", key.getProperty(), key.getPropertyType(), value, e.getMessageWithoutErrorCode()), e);
        }
    }

    @Override
    public ConfigurationBO getPropertyFromDatabase(SMLPropertyEnum key) throws TechnicalException {
        Optional<ConfigurationBO> property = configurationDAO.getPropertyFromDatabase(key);
        return handlePropertyResult(key, property);
    }

    @Override
    public ConfigurationBO deletePropertyFromDatabase(SMLPropertyEnum key) throws TechnicalException {
        Optional<ConfigurationBO> property = configurationDAO.deletePropertyFromDatabase(key);
        return handlePropertyResult(key, property);
    }

    protected ConfigurationBO handlePropertyResult(SMLPropertyEnum key, Optional<ConfigurationBO> property) throws TechnicalException {
        if (property.isEmpty()) {
            throw new BadRequestException("Property: " + key.getProperty() + " does not exist in database!");
        }
        ConfigurationBO result = property.get();
        if (key.isEncrypted()) {
            result.setValue("*******");
        }
        return result;
    }


    @Override
    public String getMonitorToken() {
        return configurationDAO.getProperty(MONITOR_TOKEN);
    }

    @Override
    public boolean isClientCertEnabled() {
        return Boolean.parseBoolean(configurationDAO.getProperty(AUTH_BLUE_COAT_ENABLED));
    }

    @Override
    public boolean isSSLClientCertEnabled() {
        return Boolean.parseBoolean(configurationDAO.getProperty(AUTH_SSLCLIENTCERT_ENABLED));
    }

    @Override
    public String getSMPCertRegularExpression() {
        return configurationDAO.getProperty(AUTH_SMP_CERT_REGEXP);
    }

    @Override
    public String getPartyIdentifierSplitRegularExpression() {
        return configurationDAO.getProperty(PARTY_IDENTIFIER_URN_PATTERN);
    }

    @Override
    public List<String> getPartyIdentifierCaseSensitiveSchemes() {
        return configurationDAO.getPropertyValue(PARTY_IDENTIFIER_CASESENSITIVE_SCHEMES);
    }

    @Override
    public Pattern getPartyIdentifierSplitRegularExpresionPattern() {
        return configurationDAO.getPropertyValue(PARTY_IDENTIFIER_URN_PATTERN);
    }

    @Override
    public String getCertificateChangeCron() {
        return configurationDAO.getProperty(CERT_CHANGE_CRON);
    }

    @Override
    public String getConfigurationFolder() {
        return configurationDAO.getProperty(CONFIGURATION_DIR);
    }

    @Override
    public String getInconsistencyReportCron() {
        return configurationDAO.getProperty(DIA_CRON);
    }

    @Override
    public String getInconsistencyReportMailTo() {
        return configurationDAO.getProperty(DIA_MAIL_TO);
    }

    @Override
    public String getInconsistencyReportMailFrom() {
        return configurationDAO.getProperty(DIA_MAIL_FROM);
    }

    @Override
    public String getInconsistencyReportGenerateByInstance() {
        return configurationDAO.getProperty(DIA_GENERATE_SERVER);
    }

    @Override
    public String getSMPExpiredCertReportCron() {
        return configurationDAO.getProperty(REPORT_EXPIRED_CERT_CRON);
    }

    @Override
    public String getSMPExpiredCertReportMailTo() {
        return configurationDAO.getProperty(REPORT_EXPIRED_CERT_MAIL_TO);
    }

    @Override
    public String getSMPExpiredCertReportMailFrom() {
        return configurationDAO.getProperty(REPORT_EXPIRED_CERT_MAIL_FROM);
    }

    @Override
    public String getSMPExpiredCertReportGenerateByInstance() {
        return configurationDAO.getProperty(REPORT_EXPIRED_CERT_GENERATE_SERVER);
    }

    @Override
    public String getMailSMPTHost() {
        return configurationDAO.getProperty(MAIL_SERVER_HOST);
    }

    @Override
    public int getMailSMPTPort() {
        return Integer.parseInt(configurationDAO.getProperty(MAIL_SERVER_PORT));
    }

    @Override
    public boolean isDNSSig0Enabled() {
        return Boolean.parseBoolean(configurationDAO.getProperty(DNS_SIG0_ENABLED));
    }

    @Override
    public boolean isDNSTSigEnabled() {
        return Boolean.parseBoolean(configurationDAO.getProperty(DNS_TSIG_ENABLED));
    }

    @Override
    public String getDNSTSigName() {
        return configurationDAO.getProperty(DNS_TSIG_KEY_NAME);
    }

    @Override
    public String getDNSTSigAlgorithm() {
        return configurationDAO.getProperty(DNS_TSIG_HASH_ALGORITHM);
    }

    @Override
    public byte[] getDNSTSigKeyValue() {
        String value = configurationDAO.getProperty(DNS_TSIG_KEY_VALUE);
        if (StringUtils.isBlank(value)) {
            return null;
        }
        try {
            return decrypt(value);
        } catch (KeyException e) {
            loggingService.error("Can not decrypt proxy password.", e);
        }
        return null;
    }

    @Override
    public boolean isLegacyDomainAuthorizationEnabled() {
        return Boolean.parseBoolean(configurationDAO.getProperty(AUTH_LEGACY_ENABLED));
    }

    @Override
    public boolean isCertRevocationValidationGraceful() {
        return Boolean.parseBoolean(configurationDAO.getProperty(CERT_VERIFICATION_GRACEFUL));
    }


    @Override
    public boolean isDeployedOnCluster() {
        return Boolean.parseBoolean(configurationDAO.getProperty(BDMSL_CLUSTER_ENABLED));
    }

    @Override
    public List<String> getCertRevocationValidationAllowedUrlProtocols() {
        String val = configurationDAO.getProperty(CRL_ALLOWED_PROTOCOLS);
        return StringUtils.isBlank(val) ? Collections.emptyList() : Arrays.asList(val.split(","));
    }

    @Override
    public CertificateRVStrategyEnum getCertRevocationValidationStrategy() {
        String val = configurationDAO.getProperty(CERT_VERIFICATION_STRATEGY);
        return CertificateRVStrategyEnum.fromCode(val);
    }

    @Override
    public String getDNSSig0KeyFilename() {
        return configurationDAO.getProperty(DNS_SIG0_KEY_FILENAME);
    }

    @Override
    public String getDNSSig0KeyName() {
        return configurationDAO.getProperty(DNS_SIG0_PKEY_NAME);
    }

    @Override
    public boolean isDNSEnabled() {
        return Boolean.parseBoolean(configurationDAO.getProperty(DNS_ENABLED));
    }

    @Override
    public boolean isShowDNSEntriesEnabled() {
        return Boolean.parseBoolean(configurationDAO.getProperty(DNS_SHOW_ENTRIES));
    }

    @Override
    public String getDNSServer() {
        return configurationDAO.getProperty(DNS_SERVER);
    }

    @Override
    public int getDNSTCPTimeout() {
        return Integer.parseInt(configurationDAO.getProperty(DNS_TCP_TIMEOUT));
    }

    @Override
    public String getDNSPublisherPrefix() {
        return configurationDAO.getProperty(DNS_PUBLISHER_PREFIX);
    }

    @Override
    public String getEncryptionFilename() {
        return configurationDAO.getProperty(ENCRYPTION_FILENAME);
    }

    @Override
    public boolean isProxyEnabled() {
        return Boolean.parseBoolean(configurationDAO.getProperty(PROXY_ENABLED));
    }

    @Override
    public String getHttpProxyHost() {
        return configurationDAO.getProperty(HTTP_PROXY_HOST);
    }

    @Override
    public String getHttpNoProxyHosts() {
        return configurationDAO.getProperty(HTTP_NO_PROXY_HOSTS);
    }

    @Override
    public int getHttpProxyPort() {
        return Integer.parseInt(configurationDAO.getProperty(HTTP_PROXY_PORT));
    }

    @Override
    public String getHttpProxyUsername() {
        return configurationDAO.getProperty(HTTP_PROXY_USER);
    }

    @Override
    public String getHttpProxyPassword() {
        String value = configurationDAO.getProperty(HTTP_PROXY_PASSWORD);
        if (StringUtils.isBlank(value)) {
            return null;
        }
        try {
            return decryptString(value);
        } catch (KeyException e) {
            loggingService.error("Can not decrypt proxy password.", e);
        }
        return null;
    }

    @Override
    public boolean isSignResponseEnabled() {
        return Boolean.parseBoolean(configurationDAO.getProperty(SIGN_RESPONSE));
    }

    @Override
    public String getSignResponseAlgorithm() {
        return configurationDAO.getProperty(SIGN_RESPONSE_ALGORITHM);
    }

    @Override
    public String getSignResponseDigestAlgorithm() {
        return configurationDAO.getProperty(SIGN_RESPONSE_DIGEST_ALGORITHM);
    }

    @Override
    public String getSignAlias() {
        return configurationDAO.getProperty(SIGNATURE_ALIAS);
    }

    @Override
    public String getKeystoreFilename() {
        return configurationDAO.getProperty(KEYSTORE_FILENAME);
    }

    @Override
    public File getKeystoreFile() {
        return getConfigurationFile(getKeystoreFilename());
    }

    @Override
    public String getKeystoreType() {
        return configurationDAO.getProperty(KEYSTORE_TYPE);
    }

    @Override
    public String getKeystorePassword() {
        return configurationDAO.getProperty(KEYSTORE_PASSWORD);
    }

    @Override
    public String getTruststoreFilename() {
        return configurationDAO.getProperty(TRUSTSTORE_FILENAME);
    }

    @Override
    public File getTruststoreFile() {
        return getConfigurationFile(getTruststoreFilename());
    }

    @Override
    public String getTruststoreType() {
        return configurationDAO.getProperty(TRUSTSTORE_TYPE);
    }

    @Override
    public String getTruststorePassword() {
        return configurationDAO.getProperty(TRUSTSTORE_PASSWORD);
    }

    @Override
    public int getListPageSize() {
        return Integer.parseInt(configurationDAO.getProperty(PAGE_SIZE));
    }

    @Override
    public int getSMPUpdateMaxParticipantCount() {
        return Integer.parseInt(configurationDAO.getProperty(SMP_CHANGE_MAX_PARTC_SIZE));
    }

    @Override
    public boolean isUnsecureLoginEnabled() {
        return Boolean.parseBoolean(configurationDAO.getProperty(AUTH_UNSEC_LOGIN));
    }

    public String encryptBase64Value(String value) throws KeyException {
        if (StringUtils.isBlank(value)) {
            return null;
        }
        byte[] decoded = Base64.getDecoder().decode(value);
        return encrypt(decoded);
    }

    @Override
    public String encryptString(String value) throws KeyException {
        if (StringUtils.isBlank(value)) {
            return null;
        }
        return encrypt(value.getBytes());
    }
    public String encrypt(byte[] value) throws KeyException {
        try {
            File location = getConfigurationFile(getEncryptionFilename());
            if (location == null) {
                throw new KeyException("Bad configuration. Encryption key does not exist!");
            }
            return KeyUtil.encrypt(location.getAbsolutePath(), value);
        } catch (KeyException kexc) {
            throw kexc;
        } catch (Exception exc) {
            throw new KeyException(exc.getMessage(), exc);
        }
    }

    @Override
    public String decryptString(String value) throws KeyException {
        byte[] decrypted = decrypt(value);
        return new String(decrypted);
    }

    public byte[] decrypt(String value) throws KeyException {
        try {
            File location = getConfigurationFile(getEncryptionFilename());
            if (location == null) {
                throw new KeyException("Bad configuration. Encryption key does not exist!");
            }
            return KeyUtil.decrypt(location.getAbsolutePath(), value);
        } catch (KeyException kexc) {
            throw kexc;
        } catch (Exception exc) {
            throw new KeyException(exc.getMessage(), exc);
        }
    }


    protected File getConfigurationFile(String filename) {
        return StringUtils.isBlank(filename) ? null : new File(getConfigurationFolder(), filename);
    }

    @Override
    public void forceRefreshProperties() {
        configurationDAO.forceRefreshProperties();
    }

    @Override
    public boolean isHSTSEnabled() {
        return Boolean.parseBoolean(configurationDAO.getProperty(HSTS_ENABLED));
    }

    @Override
    public int getHSTSMaxAge() {
        return Integer.parseInt(configurationDAO.getProperty(HSTS_MAX_AGE));
    }

    @Override
    public boolean isHSTSConfiguredWithPreload() {
        return Boolean.parseBoolean(configurationDAO.getProperty(HSTS_PRELOAD));
    }

    @Override
    public boolean isHSTSConfiguredWithIncludeSubDomain() {
        return Boolean.parseBoolean(configurationDAO.getProperty(HSTS_INCLUDE_SUBDOMAIN));
    }
}
