/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.ws;

import com.ctc.wstx.exc.WstxParsingException;
import eu.europa.ec.bdmsl.common.logging.ILoggingService;
import eu.europa.ec.bdmsl.util.ExceptionUtils;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.wsdl.interceptors.DocLiteralInInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static eu.europa.ec.bdmsl.common.exception.ErrorCode.BAD_REQUEST_ERROR;
import static java.lang.String.format;
import static org.apache.commons.lang3.exception.ExceptionUtils.getRootCauseMessage;

/**
 * This interceptor handle invalid characters from the request
 * <p>
 *
 * @author Flavio SANTOS
 * @since 25/10/2016
 */
@Component(value = "characterValidatorInInterceptor")
public class CharacterValidatorInInterceptor extends DocLiteralInInterceptor {

    @Autowired
    protected ILoggingService loggingService;

    public CharacterValidatorInInterceptor() {
        getBefore().add(DocLiteralInInterceptor.class.getName());
    }

    @Override
    public void handleMessage(Message message) throws Fault {
        try {
            super.handleMessage(message);
        } catch (Exception uexc) {
            loggingService.error(uexc.getMessage(), uexc);
            if (uexc.getCause() instanceof jakarta.xml.bind.UnmarshalException ||
                    uexc.getCause() instanceof WstxParsingException) {
                uexc = ExceptionUtils.buildBadRequestFault(
                        format("[%s] Request values should only contain ASCII characters and must be well-formed. [%s] ",
                                BAD_REQUEST_ERROR.getErrorCode(), getRootCauseMessage(uexc)));
            }
            throw new Fault(uexc);
        }
    }
}
