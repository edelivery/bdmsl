/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2017 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.conversion;

import ec.services.wsdl.bdmsl.admin.data._1.SubDomainType;
import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

/**
 * This converter class transforms SubdomainBO objects into SubDomainType objects.
 * <p/>
 *
 * @author Thomas Dussart
 * @since 24/05/2024
 */
@Component
public class SubdomainBOToSubDomainTypeConverter extends AutoRegisteringConverter<SubdomainBO, SubDomainType> {

    public SubdomainBOToSubDomainTypeConverter(ConversionService conversionService) {
        super(conversionService);
    }

    @Override
    public SubDomainType convert(SubdomainBO source) {
        if (source == null) {
            return null;
        }

        SubDomainType target = new SubDomainType();
        BeanUtils.copyProperties(source, target);
        target.setSubDomainName(source.getSubdomainName());
        target.setSubDomainDescription(source.getDescription());
        target.setDNSZone(source.getDnsZone());
        target.setParticipantRegularExpression(source.getParticipantIdRegexp());
        target.setDNSRecordType(source.getDnsRecordTypes());
        target.setSmpUrlScheme(source.getSmpUrlSchemas());
        target.setCertSubjectRegularExpression(source.getSmpCertSubjectRegex());
        target.setCertPolicyOIDs(source.getSmpCertPolicyOIDs());
        target.setMaxParticipantCountForDomain(source.getMaxParticipantCountForDomain());
        target.setMaxParticipantCountForSMP(source.getMaxParticipantCountForSMP());

        return target;
    }
}
