/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.exception;


/**
 * Subdomain Out Of Participant Quota Exception is thrown at participant creation time
 * when domain reached the max number of allowed participants for the network domain / DNS subdomain.
 *
 * @author Joze RIHTARSIC
 * @since 4.2
 */
public class SMPOutOfParticipantQuotaException extends TechnicalException {

    public SMPOutOfParticipantQuotaException(String message) {
        super(ErrorCode.SUBDOMAIN_OUT_OF_PARTICIPANT_QUOTA_EXCEPTION, message);
    }


    public SMPOutOfParticipantQuotaException(String message, Throwable t) {
        super(ErrorCode.SUBDOMAIN_OUT_OF_PARTICIPANT_QUOTA_EXCEPTION, message, t);
    }
}
