/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.entity;

import eu.europa.ec.bdmsl.dao.utils.ColumnDescription;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NaturalId;
import org.hibernate.envers.Audited;

import jakarta.persistence.*;
import java.util.Calendar;
import java.util.Objects;

import static eu.europa.ec.bdmsl.dao.QueryNames.*;

/**
 * @author Adrien FERIAL
 * @since 15/06/2015
 */
@Entity
@Audited
@Table(name = "bdmsl_certificate")
@NamedQueries({
        @NamedQuery(name = CERTIFICATE_ENTITY_GET_BY_ID, query = "SELECT cert from CertificateEntity cert where cert.id = :id"),
        @NamedQuery(name = CERTIFICATE_ENTITY_GET_BY_CERTIFICATE_ID, query = "SELECT cert from CertificateEntity cert where cert.certificateId = :certificateId"),
        @NamedQuery(name = CERTIFICATE_ENTITY_LIST_CERTIFICATES_CHANGE_TO, query = "SELECT cert from CertificateEntity cert where cert.newCertificateChangeDate <= :currentDate"),
})
public class CertificateEntity extends AbstractEntity {

    // strategy native uses sequence in oracle and auto_increment on mysql
    // from hibernate 5.4 name is also the name of sequence, before it was hibernate
    //@SequenceGenerator(name = "bdmsl_certificate_seq", sequenceName = "bdmsl_certificate_seq")
    //@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bdmsl_certificate_seq")
    @Id
    @GenericGenerator(name = "bdmsl_certificate_seq", strategy = "native")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "bdmsl_certificate_seq")
    @Column(name = "id")
    private Long id;

    @Column(name = "certificate_id", length = CommonColumnsLengths.MAX_CERTIFICATE_ID, unique = true, nullable = false)
    @ColumnDescription(comment = "The certificate_id is a key composed of the subject and the serial number of the certificate.")
    @NaturalId
    private String certificateId;

    @Column(name = "valid_from", nullable = false)
    @ColumnDescription(comment = "Start validity date of the certificate")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar validFrom;

    @Column(name = "valid_until", nullable = false)
    @ColumnDescription(comment = "Expiry date of the certificate")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar validTo;

    @Column(name = "pem_encoding", columnDefinition = "CLOB")
    @ColumnDescription(comment = "PEM encoding for the certificate")
    @Lob
    private String pemEncoding;

    @Column(name = "new_cert_change_date")
    @ColumnDescription(comment = "The date of the change for the new certificate. Hour of change is defined in Configuration!")
    private Calendar newCertificateChangeDate;

    @ManyToOne
    @JoinColumn(name = "new_cert_id")
    @ColumnDescription(comment = "The new certificate id. Links to the certificate that will be valid after the current one is expired. At the migration date, it aims to replace the existing certificate!")
    private CertificateEntity newCertificate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCertificateId() {
        return certificateId;
    }

    public void setCertificateId(String certificateId) {
        this.certificateId = certificateId;
    }

    public Calendar getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Calendar validFrom) {
        this.validFrom = validFrom;
    }

    public Calendar getValidTo() {
        return validTo;
    }

    public void setValidTo(Calendar validTo) {
        this.validTo = validTo;
    }

    public String getPemEncoding() {
        return pemEncoding;
    }

    public void setPemEncoding(String pemEncoding) {
        this.pemEncoding = pemEncoding;
    }

    public Calendar getNewCertificateChangeDate() {
        return newCertificateChangeDate;
    }

    public void setNewCertificateChangeDate(Calendar newCertificateChangeDate) {
        this.newCertificateChangeDate = newCertificateChangeDate;
    }

    public CertificateEntity getNewCertificate() {
        return newCertificate;
    }

    public void setNewCertificate(CertificateEntity newCertificate) {
        this.newCertificate = newCertificate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CertificateEntity)) return false;
        CertificateEntity that = (CertificateEntity) o;
        return Objects.equals(certificateId, that.certificateId) &&
                Objects.equals(validFrom, that.validFrom) &&
                Objects.equals(validTo, that.validTo) &&
                Objects.equals(pemEncoding, that.pemEncoding) &&
                Objects.equals(newCertificateChangeDate, that.newCertificateChangeDate) &&
                Objects.equals(newCertificate, that.newCertificate);
    }

    @Override
    public int hashCode() {
        //hash code only property because we should find object in hash buckets even if we change
        // value
        return Objects.hash(certificateId);
    }
}
