/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2017 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.conversion;

import eu.europa.ec.bdmsl.common.bo.CertificateDomainBO;
import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import eu.europa.ec.bdmsl.dao.entity.CertificateDomainEntity;
import eu.europa.ec.bdmsl.util.CertificateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

/**
 * This converter class transforms CertificateDomainEntity objects into CertificateDomainBO objects.
 * <p/>
 *
 * @author Thomas Dussart
 * @since 24/05/2024
 */
@Component
public class CertificateDomainEntityToCertificateDomainBOConverter extends AutoRegisteringConverter<CertificateDomainEntity, CertificateDomainBO> {

    public CertificateDomainEntityToCertificateDomainBOConverter(ConversionService conversionService) {
        super(conversionService);
    }

    @Override
    public CertificateDomainBO convert(CertificateDomainEntity source) {
        if (source == null) {
            return null;
        }

        CertificateDomainBO target = new CertificateDomainBO();
        BeanUtils.copyProperties(source, target);

        // Custom mappings
        target.setCertificate(CertificateUtils.normalizeForX509(source.getCertificate()));
        target.setSubdomain(getConversionService().convert(source.getSubdomain(), SubdomainBO.class));

        return target;
    }
}
