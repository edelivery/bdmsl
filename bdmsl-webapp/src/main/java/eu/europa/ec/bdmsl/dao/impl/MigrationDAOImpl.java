/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.impl;

import com.google.common.base.Strings;
import eu.europa.ec.bdmsl.common.bo.MigrationRecordBO;
import eu.europa.ec.bdmsl.common.bo.ParticipantBO;
import eu.europa.ec.bdmsl.common.exception.DuplicateParticipantException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.dao.AbstractDAOImpl;
import eu.europa.ec.bdmsl.dao.IMigrationDAO;
import eu.europa.ec.bdmsl.dao.entity.MigrateEntity;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

import static eu.europa.ec.bdmsl.dao.QueryNames.*;
import static eu.europa.ec.bdmsl.dao.entity.MigrateEntity.NULL_SCHEME;

/**
 * @author Adrien FERIAL
 * @since 15/06/2015
 */
@Repository
public class MigrationDAOImpl extends AbstractDAOImpl implements IMigrationDAO {
    @Autowired
    private ConversionService conversionService;

    @Override
    public MigrationRecordBO findNonMigratedRecord(MigrationRecordBO migrationRecordBO) throws TechnicalException {
        List<MigrateEntity> resultEntityList = getEntityManager()
                .createNamedQuery(MIGRATE_ENTITY_GET_BY_PARTICIPANT_AND_MIGRATION_STATUS, MigrateEntity.class)
                .setParameter(SCHEME, normalizeScheme(migrationRecordBO.getScheme()))
                .setParameter(PARTICIPANT_ID, migrationRecordBO.getParticipantId())
                .setParameter(MIGRATED, false)
                .getResultList();
        return resultHandler(resultEntityList, migrationRecordBO);
    }

    @Override
    public MigrationRecordBO findMigratedRecord(MigrationRecordBO migrationRecordBO) throws TechnicalException {
        List<MigrateEntity> resultEntityList = getEntityManager().createNamedQuery(MIGRATE_ENTITY_GET_RECORD, MigrateEntity.class)
                .setParameter(SCHEME, normalizeScheme(migrationRecordBO.getScheme()))
                .setParameter(PARTICIPANT_ID, migrationRecordBO.getParticipantId())
                .setParameter(MIGRATION_KEY, migrationRecordBO.getMigrationCode())
                .setParameter(MIGRATED, true)
                .setParameter(OLD_SMP_ID, migrationRecordBO.getOldSmpId())
                .setParameter(NEW_SMP_ID, migrationRecordBO.getNewSmpId())
                .getResultList();

        return resultHandler(resultEntityList, migrationRecordBO);
    }

    @Override
    public MigrationRecordBO findMigrationRecordByCompositePrimaryKey(MigrationRecordBO migrationRecordBO) throws TechnicalException {
        List<MigrateEntity> resultEntityList = getEntityManager().createNamedQuery(MIGRATE_ENTITY_GET_BY_PARTICIPANT_AND_MIGRATION_KEY, MigrateEntity.class)
                .setParameter(SCHEME, normalizeScheme(migrationRecordBO.getScheme()))
                .setParameter(PARTICIPANT_ID, migrationRecordBO.getParticipantId())
                .setParameter(MIGRATION_KEY, migrationRecordBO.getMigrationCode())
                .getResultList();
        return resultHandler(resultEntityList, migrationRecordBO);
    }

    private MigrationRecordBO resultHandler(List<MigrateEntity> resultEntityList, MigrationRecordBO migrationRecordBO) throws DuplicateParticipantException {
        if (resultEntityList != null && resultEntityList.size() > 1) {
            throw new DuplicateParticipantException("There are more than one migration record for the given participant : " + migrationRecordBO.getScheme() + "/" + migrationRecordBO.getParticipantId());
        } else if (resultEntityList != null && resultEntityList.size() == 1) {
            return conversionService.convert(resultEntityList.get(0), MigrationRecordBO.class);
        } else {
            return null;
        }
    }

    @Override
    public void updateMigrationRecord(MigrationRecordBO migrationRecordBO) throws TechnicalException {
        MigrateEntity resultEntity = getEntityManager().createNamedQuery(MIGRATE_ENTITY_GET_BY_PARTICIPANT_AND_MIGRATION_STATUS, MigrateEntity.class)
                .setParameter(SCHEME, normalizeScheme(migrationRecordBO.getScheme()))
                .setParameter(PARTICIPANT_ID, migrationRecordBO.getParticipantId())
                .setParameter(MIGRATED, migrationRecordBO.isMigrated())
                .getSingleResult();

        if (resultEntity != null) {
            getEntityManager().remove(resultEntity);
        }
        createMigrationRecord(migrationRecordBO);
    }

    @Override
    public void performMigration(MigrationRecordBO migrationRecordBO) throws TechnicalException {
        MigrateEntity resultEntity = getEntityManager().createNamedQuery(MIGRATE_ENTITY_GET_BY_OLD_SMP_IDENTIFIER_AND_MIGRATION_KEY, MigrateEntity.class)
                .setParameter(SCHEME, normalizeScheme(migrationRecordBO.getScheme()))
                .setParameter(PARTICIPANT_ID, migrationRecordBO.getParticipantId())
                .setParameter(MIGRATION_KEY, migrationRecordBO.getMigrationCode())
                .setParameter(OLD_SMP_ID, migrationRecordBO.getOldSmpId())
                .getSingleResult();
        resultEntity.setMigrationKey(migrationRecordBO.getMigrationCode());
        resultEntity.setOldSmpId(migrationRecordBO.getOldSmpId());
        if (!Strings.isNullOrEmpty(migrationRecordBO.getNewSmpId())) {
            resultEntity.setNewSmpId(migrationRecordBO.getNewSmpId());
        }
        resultEntity.setMigrated(migrationRecordBO.isMigrated());
        super.merge(resultEntity);
    }


    @Override
    public void createMigrationRecord(MigrationRecordBO migrationRecordBO) throws TechnicalException {
        MigrateEntity migrateEntity = conversionService.convert(migrationRecordBO, MigrateEntity.class);
        super.persist(migrateEntity);
    }

    @Override
    public List<MigrationRecordBO> findMigrationsRecordsForParticipants(String smpId, List<ParticipantBO> participantBOList) throws TechnicalException {

        List<String> participantIds = participantBOList.stream().map(partcBO -> StringUtils.upperCase(partcBO.getParticipantId())).collect(Collectors.toList());
        List<MigrateEntity> resultEntityList = getEntityManager().createNamedQuery(MIGRATE_ENTITY_GET_BY_OLD_SMP_AND_PARTICIPANT_AND_MIGRATION_STATUS, MigrateEntity.class)
                .setParameter(SMP_ID, smpId)
                .setParameter(PARTICIPANT_IDS, participantIds)
                .setParameter(MIGRATED, Boolean.FALSE)
                .getResultList();
        return resultEntityList.stream().map(migrateEntity ->
                conversionService.convert(migrateEntity, MigrationRecordBO.class)).collect(Collectors.toList());
    }

    @Override
    public List<MigrationRecordBO> findMigrationsRecordsForSMP(String smpId) throws TechnicalException {
        List<MigrateEntity> resultEntityList = getEntityManager().createNamedQuery(MIGRATE_ENTITY_GET_BY_OLD_SMP_IDENTIFIER_AND_MIGRATION_STATUS, MigrateEntity.class)
                .setParameter(SMP_ID, smpId)
                .setParameter(MIGRATED, Boolean.FALSE)
                .getResultList();
        return resultEntityList.stream().map(migrateEntity ->
                conversionService.convert(migrateEntity, MigrationRecordBO.class)).collect(Collectors.toList());
    }

    public String normalizeScheme(String scheme) {
        return StringUtils.isBlank(scheme) ? NULL_SCHEME : scheme;
    }
}
