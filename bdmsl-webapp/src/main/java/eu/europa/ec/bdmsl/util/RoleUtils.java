/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.util;

import eu.europa.ec.bdmsl.common.enums.SMLRoleEnum;
import eu.europa.ec.bdmsl.common.exception.CertificateAuthenticationException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;

import javax.naming.InvalidNameException;
import javax.naming.ldap.LdapName;
import javax.naming.ldap.Rdn;
import java.util.ArrayList;
import java.util.List;


public class RoleUtils {

    private static RoleUtils sINSTANCE = null;

    private String smpCertSubjectRegex;

    private RoleUtils() {

    }

    public String getSmpCertSubjectRegex() {
        return smpCertSubjectRegex;
    }

    public void setSmpCertSubjectRegex(String smpCertSubjectRegex) {
        this.smpCertSubjectRegex = smpCertSubjectRegex;
    }

    public static RoleUtils getsInstance() {
        if (sINSTANCE == null) {
            sINSTANCE = new RoleUtils();
        }
        return sINSTANCE;
    }


    public synchronized List<GrantedAuthority> getRolesForCertSubject(String subject, Boolean isNonRootCACertificate)
            throws CertificateAuthenticationException {

        List<GrantedAuthority> roles = new ArrayList<>();
        if (StringUtils.isEmpty(subject)) {
            return roles;
        }

        LdapName ldapName;
        try {
            ldapName = new LdapName(subject);
        } catch (InvalidNameException exc) {
            throw new CertificateAuthenticationException("Impossible to identify authorities for certificate " + subject, exc);
        }

        String commonName = null;
        for (final Rdn rdn : ldapName.getRdns()) {
            if ("CN".equalsIgnoreCase(rdn.getType())) {
                commonName = (String) rdn.getValue();
            }
        }
        if (commonName == null) {
            throw new CertificateAuthenticationException("Invalid certificate subject: Missing CN. Subject value:" + subject);
        }

        boolean hasSMPRegExp = !StringUtils.isEmpty(smpCertSubjectRegex);


        // check new Regular expresion value
        if ((hasSMPRegExp && subject.matches(smpCertSubjectRegex))) {
            roles.add(SMLRoleEnum.ROLE_SMP.getAuthority());
            // if starts with SMP_ or is non root certificate
        } else if (commonName.startsWith("SMP_")
                || (isNonRootCACertificate != null && isNonRootCACertificate)) {
            // SMP certificate --> SMP_ROLE
            roles.add(SMLRoleEnum.ROLE_SMP.getAuthority());
        } else {
            roles.clear();
        }
        return roles;

    }
}
