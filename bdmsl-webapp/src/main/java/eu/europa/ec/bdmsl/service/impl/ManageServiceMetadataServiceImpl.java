/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.impl;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import eu.europa.ec.bdmsl.business.IConfigurationBusiness;
import eu.europa.ec.bdmsl.business.IManageParticipantIdentifierBusiness;
import eu.europa.ec.bdmsl.business.IManageServiceMetadataBusiness;
import eu.europa.ec.bdmsl.common.bo.CertificateBO;
import eu.europa.ec.bdmsl.common.bo.ParticipantBO;
import eu.europa.ec.bdmsl.common.bo.ServiceMetadataPublisherBO;
import eu.europa.ec.bdmsl.common.exception.*;
import eu.europa.ec.bdmsl.common.service.AbstractServiceImpl;
import eu.europa.ec.bdmsl.common.util.LogEvents;
import eu.europa.ec.bdmsl.service.IManageParticipantIdentifierService;
import eu.europa.ec.bdmsl.service.IManageServiceMetadataService;
import eu.europa.ec.bdmsl.service.dns.IDnsClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Adrien FERIAL
 * @since 12/06/2015
 */
@Service
@Transactional(readOnly = true, propagation = Propagation.REQUIRES_NEW)
public class ManageServiceMetadataServiceImpl extends AbstractServiceImpl implements IManageServiceMetadataService {

    @Autowired
    private IManageServiceMetadataBusiness manageServiceMetadataBusiness;

    @Autowired
    @Qualifier(value = "manageParticipantIdentifierServiceImpl")
    private IManageParticipantIdentifierService manageParticipantIdentifierService;

    @Autowired
    private IManageParticipantIdentifierBusiness manageParticipantIdentifierBusiness;

    @Autowired
    private IDnsClientService dnsClientService;

    @Autowired
    private IConfigurationBusiness configurationBusiness;

    /**
     * Number of participants to delete at once
     */
    public final static int NUMBER_PARTICIPANTS_DELETE = 300;

    @Override
    @PreAuthorize("hasAnyRole('ROLE_SMP')")
    @Transactional(readOnly = false, rollbackFor = Throwable.class)
    public void create(final ServiceMetadataPublisherBO smpBO) throws
             TechnicalException {
        // Validate input data
        manageServiceMetadataBusiness.validateSMPData(smpBO);

        // Then make sure that the smp does not exist.
        manageServiceMetadataBusiness.verifySMPNotExist(smpBO.getSmpId());

        // Save the service metadata
        CertificateBO certificateBO = manageServiceMetadataBusiness.findCertificate(smpBO.getCertificateId());
        if (certificateBO == null) {
            manageServiceMetadataBusiness.createCurrentCertificate();
        }

        ServiceMetadataPublisherBO persistedSMP =  manageServiceMetadataBusiness.createSMP(smpBO);

        if (configurationBusiness.isDNSEnabled()) {
            // If DNS create fails, then an exception is thrown and the database import is rolled back
            dnsClientService.createDNSRecordsForSMP(persistedSMP);
        }
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_SMP')")
    @Transactional(readOnly = false, rollbackFor = Throwable.class)
    public void delete(String smpId) throws  TechnicalException {
        // Validate input data
        manageServiceMetadataBusiness.validateSMPId(smpId);

        // Then make sure that the smp exists.
        ServiceMetadataPublisherBO smpBO = manageServiceMetadataBusiness.verifySMPExist(smpId);

        // validate authorization
        manageServiceMetadataBusiness.validateAuthorization(smpBO);

        // validate smp subdomain and authenticated cert subdomain
        manageServiceMetadataBusiness.validateSMPSubdomain(smpBO);

        long cnt = manageParticipantIdentifierBusiness.getParticipantCountForSMP(smpBO);
        long maxAllow = configurationBusiness.getSMPUpdateMaxParticipantCount();
        if (cnt > maxAllow) {
            throw new SmpDeleteException("The SMP couldn't be deleted because it has too many participants: '" + cnt + "' (Max allowed:" + maxAllow + ")! Delete participants first!");
        }

        List<ParticipantBO> participants = manageParticipantIdentifierBusiness.getAllParticipantsForSMP(smpBO);

        // Check that no migration is planned for this SMP
        manageServiceMetadataBusiness.checkNoMigrationPlanned(smpId);

        // find all participants for this SMP and delete them
        try {
            if (participants != null) {
                /* we delete by batch of 300 participants to avoid an exception in the DNS because the maximum size of a message is 65k.
                If the number of participants is big, then the size of 65k is exceeded and the update is refused */
                List<List<ParticipantBO>> subParticipantsList = Lists.partition(participants, NUMBER_PARTICIPANTS_DELETE);
                // This method requires a new transaction to reduce the risk of de-synchronization between the DNS and the database
                for (List<ParticipantBO> participantBOList : subParticipantsList) {
                    manageParticipantIdentifierService.delete(smpBO.getSmpId(), participantBOList);
                    loggingService.businessLog(LogEvents.BUS_PARTICIPANT_LIST_DELETED, participantBOList.toString());
                }
            }
        } catch (TechnicalException exc) {
            throw exc;
        } catch (Exception exc) {
            throw new SmpDeleteException("The SMP couldn't be deleted but some of its participants may have been deleted. Please see the logs or contact the system administrator to identify which participants have been deleted", exc);
        }

        // all the participants have been deleted
        manageServiceMetadataBusiness.deleteSMP(smpBO);

        if (configurationBusiness.isDNSEnabled()) {
            // If DNS deletion fails, then an exception is thrown and the database import is rolled back
            dnsClientService.deleteDNSRecordsForSMP(smpBO);
        }

    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_SMP')")
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void update(ServiceMetadataPublisherBO smpBO) throws  TechnicalException {
        // Validate input data
        manageServiceMetadataBusiness.validateSMPData(smpBO);

        // Then make sure that the smp exists.
        ServiceMetadataPublisherBO existingSmpBO = manageServiceMetadataBusiness.verifySMPExist(smpBO.getSmpId());

        // validate authorization
        manageServiceMetadataBusiness.validateAuthorization(existingSmpBO);

        // validate smp subdomain and authenticated cert subdomain
        manageServiceMetadataBusiness.validateSMPSubdomain(existingSmpBO);

        //Checks if logical address is different to be updated on dns
        boolean isLogicalAddressToBeUpdated = manageServiceMetadataBusiness.isLogicalAddressToBeUpdated(smpBO);
        boolean hasSMPNaptrRecords = manageServiceMetadataBusiness.hasNaptrRecords(smpBO);

        // if dns is enabled add if naptr record should be updated also the verify participant count
        if (configurationBusiness.isDNSEnabled() && isLogicalAddressToBeUpdated && hasSMPNaptrRecords) {

            long cnt = manageParticipantIdentifierBusiness.getParticipantCountForSMP(smpBO);
            long maxAllow = configurationBusiness.getSMPUpdateMaxParticipantCount();
            if (cnt > maxAllow) {
                throw new SmpDeleteException("Logical address of SMP can not be updated because SMP has too many participants: '" + cnt + "' (Max allowed:" + maxAllow + ")! Create new SMP and transfer participants!");
            }
        }
        // update smp
        ServiceMetadataPublisherBO updatedSMPBO = manageServiceMetadataBusiness.updateSMP(smpBO);

        // If DNS update fails, then an exception is thrown and the database import is rolled back
        if (configurationBusiness.isDNSEnabled()) {
            // if logical address is changed and domain has naptr records.. then
            // all naptr participant records must be updated.
            if (isLogicalAddressToBeUpdated && hasSMPNaptrRecords) {
                List<ParticipantBO> participants = manageParticipantIdentifierBusiness.getAllParticipantsForSMP(smpBO);
                dnsClientService.updateDNSRecordsForSMP(updatedSMPBO, participants, isLogicalAddressToBeUpdated);
            } else {
                // only update smp
                dnsClientService.updateSMPDNSRecord(updatedSMPBO);
            }
        }
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_SMP')")
    @Transactional
    public ServiceMetadataPublisherBO read(String id) throws
             TechnicalException {

        if (Strings.isNullOrEmpty(id)) {
            throw new BadRequestException("The SMP ID must not be null");
        }

        ServiceMetadataPublisherBO resultBO = manageServiceMetadataBusiness.read(id);
        if (resultBO == null) {
            throw new SmpNotFoundException("The SMP with id " + id + " couldn't be found");
        }

        // validate authorization
        manageServiceMetadataBusiness.validateAuthorization(resultBO);

        return resultBO;
    }

}

