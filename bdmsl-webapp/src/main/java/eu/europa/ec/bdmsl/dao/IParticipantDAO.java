/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao;

import eu.europa.ec.bdmsl.common.bo.PageRequestBO;
import eu.europa.ec.bdmsl.common.bo.ParticipantBO;
import eu.europa.ec.bdmsl.common.bo.ServiceMetadataPublisherBO;
import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import org.xbill.DNS.IRDnsRangeData;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Interface for the DAO class with common functions!
 *
 * @author Adrien FERIAL
 * @since 3.0.0
 */
public interface IParticipantDAO {
    void createParticipant(ParticipantBO participantBO) throws TechnicalException;

    ParticipantBO findParticipant(ParticipantBO participantBO) throws TechnicalException;

    Optional<ParticipantBO> getParticipantForDomain(ParticipantBO participantBO, SubdomainBO subdomainBO) throws TechnicalException;

    Optional<ParticipantBO> getParticipantForDifferentSMPOnDomain(ParticipantBO participantBO, ServiceMetadataPublisherBO smpBO) throws TechnicalException;

    void deleteParticipant(ParticipantBO participantBO) throws TechnicalException;

    List<ParticipantBO> listParticipant(PageRequestBO pageRequestBO, int participantPerPageCount) throws TechnicalException;

    void updateParticipant(ParticipantBO participantBO, final String oldSmpId) throws TechnicalException;

    List<ParticipantBO> getAllParticipants();
    List<ParticipantBO> getAllParticipantsForSMP(ServiceMetadataPublisherBO smpBO);
    List<ParticipantBO> getParticipantsForSMP(ServiceMetadataPublisherBO smpBO, int page, int pageSize);

    List<ParticipantBO> findParticipants(String smpId, Map<String, List<String>> mapParticipants) throws TechnicalException;

    void deleteParticipants(String smpId, Map<String, List<String>> mapParticipants) throws TechnicalException;

    int updateNextBatchHashValuesForParticipants() throws TechnicalException;

    List<ParticipantBO> setDisabledNextSMPParticipantsBatch(ServiceMetadataPublisherBO smpBO, int batchSize, boolean disabled) throws TechnicalException;

    List<ParticipantBO> deleteNextSMPParticipantsBatch(ServiceMetadataPublisherBO smpBO, int batchSize) throws TechnicalException;
    List<ParticipantBO>  updateSMPForNextParticipantsBatchSMP(ServiceMetadataPublisherBO smpNewBO, ServiceMetadataPublisherBO smpOldBO, int batchSize) throws TechnicalException;

    void retrieveCNameDBDataForInconsistencyReport(String publisherName, IRDnsRangeData irCNameData, Date createDateTo);

    void retrieveNaptrDBDataForInconsistencyReport(IRDnsRangeData irNaptrData, Date createDateTo);

    long getParticipantCount();

    long getParticipantCountForSMP(String smpId);

    long getParticipantCountForSMPAndStatus(String smpId, boolean disabled);

    long getParticipantCountForSubDomain(Long subdomainID);
}
