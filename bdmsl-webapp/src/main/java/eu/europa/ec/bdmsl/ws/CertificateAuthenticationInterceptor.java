/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.ws;

import eu.europa.ec.bdmsl.business.IConfigurationBusiness;

import eu.europa.ec.bdmsl.common.exception.CertificateAuthenticationException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.common.logging.ILoggingService;
import eu.europa.ec.bdmsl.common.util.LogEvents;
import eu.europa.ec.bdmsl.security.CertificateAuthentication;
import eu.europa.ec.bdmsl.security.SMLAuthenticationProvider;
import eu.europa.ec.bdmsl.security.SecurityTokenAuthentication;
import eu.europa.ec.bdmsl.security.UnsecureAuthentication;
import eu.europa.ec.bdmsl.util.ExceptionUtils;
import eu.europa.ec.bdmsl.util.RoleUtils;
import eu.europa.ec.bdmsl.ws.soap.UnauthorizedFault;
import eu.europa.ec.edelivery.exception.ClientCertParseException;
import eu.europa.ec.edelivery.security.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;

import jakarta.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Collections;

/**
 * @author Adrien FERIAL
 * @since 25/06/2015
 */
@Component(value = "certificateAuthenticationInterceptor")
public class CertificateAuthenticationInterceptor extends AbstractPhaseInterceptor<Message> {

    public static final String NOT_AUTHENTICATED = "Not authenticated";

    @Autowired
    protected ILoggingService loggingService;

    @Autowired
    protected IConfigurationBusiness configurationBusiness;


    private static final String CLIENT_CERT_HEADER_KEY = "Client-Cert";
    private static final String ADMIN_HEADER_KEY = "Admin-Pwd";

    private static final String CLIENT_CERT_HEADER_SSL_KEY = "SSLClientCert";
    private static final String CLIENT_CERT_ATTRIBUTE_KEY = "jakarta.servlet.request.X509Certificate";


    @Autowired
    private SMLAuthenticationProvider smlAuthenticationProvider;


    public CertificateAuthenticationInterceptor() {
        super(Phase.PRE_PROTOCOL);
    }

    ClientCertAuthenticationFilter blueCoatAuthenticationFilter = new ClientCertAuthenticationFilter();
    EDeliveryX509AuthenticationFilter eDeliveryX509AuthenticationFilter = new EDeliveryX509AuthenticationFilter();
    EDeliveryTokenAuthenticationFilter eDeliveryTokenAuthenticationFilter = new EDeliveryTokenAuthenticationFilter();

    @Override
    public void handleMessage(Message message) throws Fault {
        HttpServletRequest httpRequest = (HttpServletRequest) message.get("HTTP.REQUEST");
        try {
            loggingService.putMDC("requestId", RequestContextHolder.currentRequestAttributes().getSessionId());
            loggingService.putMDC("user", "no-user-yet-logged");

            final Object certificateAttribute = httpRequest.getAttribute(CLIENT_CERT_ATTRIBUTE_KEY);
            final String certificateAttributeSSL = httpRequest.getHeader(CLIENT_CERT_HEADER_SSL_KEY);
            final String certHeaderValue = httpRequest.getHeader(CLIENT_CERT_HEADER_KEY);
            final String adminHeaderValue = httpRequest.getHeader(ADMIN_HEADER_KEY);


            RoleUtils.getsInstance().setSmpCertSubjectRegex(configurationBusiness.getSMPCertRegularExpression());

            loggingService.securityLog("CP: " + httpRequest.getContextPath() + ", Cert: " + certificateAttribute + ", " + CLIENT_CERT_HEADER_KEY + ": " + certHeaderValue +
                    ", " + ADMIN_HEADER_KEY + ": " + (StringUtils.isBlank(adminHeaderValue) ? "null/empty/blank" : "*******") +
                    ", SSLCert: " + certificateAttributeSSL

            );


            blueCoatAuthenticationFilter.setClientCertAuthenticationEnabled(configurationBusiness.isClientCertEnabled());
            eDeliveryX509AuthenticationFilter.setHttpHeaderAuthenticationEnabled(configurationBusiness.isSSLClientCertEnabled());

            Authentication authentication = getAuthenticationFromHttpRequest(httpRequest);

            authenticate(authentication, httpRequest);

        } catch (final Exception exc) {
            loggingService.businessLog(LogEvents.BUS_AUTHENTICATION_ERROR, exc);
            if (exc instanceof UnauthorizedFault) {
                loggingService.error("Unauthorized access", exc);
                throw new Fault(exc);
            } else if (exc instanceof ClientCertParseException) {
                loggingService.error("Client-Cert parse error:" + org.apache.commons.lang3.exception.ExceptionUtils.getRootCauseMessage(exc), exc);
                throw new Fault(ExceptionUtils.buildUnauthorizedFault(exc.getMessage()));
            } else if (exc instanceof TechnicalException) {
                loggingService.error("Technical error:" + org.apache.commons.lang3.exception.ExceptionUtils.getRootCauseMessage(exc), exc);
                throw new Fault(ExceptionUtils.buildUnauthorizedFault(exc.getMessage()));
            } else if (exc instanceof java.lang.ClassCastException) {
                loggingService.error("Application error:" + org.apache.commons.lang3.exception.ExceptionUtils.getRootCauseMessage(exc), exc);
                throw new Fault(ExceptionUtils.buildUnauthorizedFault(exc.getMessage()));
            } else {
                String messageFault = "Internal error during authentication process";
                loggingService.error(messageFault + ":" + org.apache.commons.lang3.exception.ExceptionUtils.getRootCauseMessage(exc), exc);
                throw new Fault(ExceptionUtils.buildUnauthorizedFault(messageFault));
            }
        }
    }

    private Authentication getAuthenticationFromHttpRequest(HttpServletRequest httpRequest) {
        // test with bluecoat
        Principal principal = blueCoatAuthenticationFilter.buildDetails(httpRequest);
        if (principal != null) {
            return new CertificateAuthentication((PreAuthenticatedCertificatePrincipal) principal, Collections.emptyList());
        }
        // client certificate
        principal = eDeliveryX509AuthenticationFilter.buildDetails(httpRequest);
        if (principal != null) {
            return new CertificateAuthentication((PreAuthenticatedCertificatePrincipal) principal, Collections.emptyList());
        }
        // security token
        principal = eDeliveryTokenAuthenticationFilter.buildDetails(httpRequest);
        if (principal != null) {
            return new SecurityTokenAuthentication((PreAuthenticatedTokenPrincipal) principal, Collections.emptyList());
        }

        return new UnsecureAuthentication();
    }

    private void authenticate(Authentication authentication, HttpServletRequest httpRequest) throws TechnicalException {
        loggingService.securityLog(LogEvents.SEC_CONNECTION_ATTEMPT, httpRequest.getRemoteHost(), httpRequest.getRequestURL().toString(),
                authentication != null && authentication.getPrincipal() != null ? authentication.getPrincipal().toString() : NOT_AUTHENTICATED);
        Authentication authenticationResult;
        try {
            authenticationResult = smlAuthenticationProvider.authenticate(authentication);
        } catch (AuthenticationException exc) {
            loggingService.securityLog(LogEvents.SEC_UNAUTHORIZED_ACCESS, httpRequest.getRemoteHost(), httpRequest.getRequestURL().toString(), (authentication != null ? authentication.getName() : NOT_AUTHENTICATED));
            loggingService.error(exc.getMessage(), exc);
            throw new CertificateAuthenticationException(exc.getMessage(), exc);
        }

        loggingService.putMDC("user", authenticationResult.getName());

        if (authenticationResult.isAuthenticated()) {
            loggingService.securityLog(LogEvents.SEC_AUTHORIZED_ACCESS, httpRequest.getRemoteHost(), httpRequest.getRequestURL().toString(), authenticationResult.getAuthorities().toString());
            loggingService.debug("Request authenticated. Storing the authentication result in the security context");
            loggingService.debug("Authentication result: " + authenticationResult);
            SecurityContextHolder.getContext().setAuthentication(authenticationResult);
        } else {
            loggingService.securityLog(LogEvents.SEC_UNAUTHORIZED_ACCESS, httpRequest.getRemoteHost(), httpRequest.getRequestURL().toString(), (authentication != null ? authentication.getName() : NOT_AUTHENTICATED));
            throw new CertificateAuthenticationException("The certificate might not be valid or present, the Admin credentials are invalid for " + (authentication != null ? authentication.getName() : NOT_AUTHENTICATED) + " or Unsecured authentication is not allowed.");
        }
    }
}
