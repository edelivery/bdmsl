/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.business;

import eu.europa.ec.bdmsl.common.bo.CertificateBO;
import eu.europa.ec.bdmsl.common.bo.ChangeCertificateBO;
import eu.europa.ec.bdmsl.common.bo.PrepareChangeCertificateBO;
import eu.europa.ec.bdmsl.common.bo.WildcardBO;

import eu.europa.ec.bdmsl.common.exception.TechnicalException;

import java.security.cert.X509Certificate;

/**
 * @author Adrien FERIAL
 * @since 12/06/2015
 */
public interface IManageCertificateBusiness {

    void validateChangeCertificate(PrepareChangeCertificateBO prepareChangeCertificateBO) throws
             TechnicalException;

    void validateChangeCertificate(ChangeCertificateBO changeCertificateBO) throws
             TechnicalException;

    CertificateBO extractCertificate(byte[] publicKey) throws  TechnicalException;

    CertificateBO extractCertificate(String publicKey) throws  TechnicalException;

    Long createNewCertificate(CertificateBO certificateBO) throws  TechnicalException;

    void updateCertificate(CertificateBO certificateBO) throws  TechnicalException;

    X509Certificate getX509Certificate(String publicKey) throws  TechnicalException;

    X509Certificate getX509Certificate(byte[] publicKey) throws  TechnicalException;

    void checkMigrationDate(X509Certificate certificate, PrepareChangeCertificateBO prepareChangeCertificateBO) throws  TechnicalException;

    CertificateBO findCertificate(String certificateId) throws  TechnicalException;

    X509Certificate getX509CertificateForCertificateId(String certificateId) throws TechnicalException;

    CertificateBO findCertificateById(Long id) throws  TechnicalException;

    int changeCertificates() throws  TechnicalException;

    WildcardBO findWildCard(String scheme, String currentCertificate) throws  TechnicalException;

    boolean equalsCertificate(String certificateId1, String certificateId2) throws TechnicalException;

    void changeCertificate(CertificateBO newCertificateBO, CertificateBO oldCertificateBO) throws  TechnicalException;

    void validateCurrentCertificate(CertificateBO currentCertificate) throws TechnicalException;

    void domainValidationForNewCertificate(X509Certificate newCertificate) throws  TechnicalException;
}
