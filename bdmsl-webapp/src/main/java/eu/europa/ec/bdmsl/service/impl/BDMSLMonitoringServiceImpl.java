/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.impl;

import eu.europa.ec.bdmsl.business.ISubdomainBusiness;
import eu.europa.ec.bdmsl.common.exception.GenericTechnicalException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.common.service.AbstractServiceImpl;
import eu.europa.ec.bdmsl.service.IBDMSLMonitoringService;
import eu.europa.ec.bdmsl.service.dns.IDnsClientService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Sebastian-Ion TINCU
 * @since 5.0
 */
@Service
@Transactional(readOnly = true)
public class BDMSLMonitoringServiceImpl extends AbstractServiceImpl implements IBDMSLMonitoringService {

    private final IDnsClientService dnsClientService;

    private final ISubdomainBusiness subdomainBusiness;

    public BDMSLMonitoringServiceImpl(IDnsClientService dnsClientService, ISubdomainBusiness subdomainBusiness) {
        this.dnsClientService = dnsClientService;
        this.subdomainBusiness = subdomainBusiness;
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_MONITOR')")
    @CacheEvict(value = {"allDomains"}, allEntries = true)
    public void isAlive() throws TechnicalException {
        synchronized (BDMSLServiceImpl.class) {
            try {
                // try to access the database
                loggingService.debug("VERIFYING DATABASE");
                findAllSubdomains();

                // try to write and lookup a record on the DNS
                dnsClientService.verifyDNSAccess();

            } catch (TechnicalException exc) {
                loggingService.error(exc.getMessage(), exc);
                throw exc;
            } catch (Exception exc) {
                loggingService.error(exc.getMessage(), exc);
                throw new GenericTechnicalException(exc.getMessage(), exc);
            }
        }
    }

    private void findAllSubdomains() throws TechnicalException {
        subdomainBusiness.findAll();
    }
}
