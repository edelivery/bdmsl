/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.bo;

import eu.europa.ec.edelivery.text.DistinguishedNamesCodingUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Calendar;

/**
 * @author Adrien FERIAL
 * @since 12/06/2015
 */
public class CertificateDomainBO extends AbstractBusinessObject {

    Long id;
    private String certificate;
    private SubdomainBO subdomain;
    private String crl;
    private boolean isRootCA;
    private String pemEncoding;
    private Calendar validFrom;
    private Calendar validTo;
    private boolean isAdmin;
    private String truststoreAlias;

    String normalizedFullPath;
    String normalizedShortPath;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCertificate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public String getTruststoreAlias() {
        return truststoreAlias;
    }

    public void setTruststoreAlias(String truststoreAlias) {
        this.truststoreAlias = truststoreAlias;
    }

    public SubdomainBO getSubdomain() {
        return subdomain;
    }

    public void setSubdomain(SubdomainBO subdomain) {
        this.subdomain = subdomain;
    }

    public String getCrl() {
        return crl;
    }

    public void setCrl(String crl) {
        this.crl = crl;
    }

    public boolean isRootCA() {
        return isRootCA;
    }

    public void setRootCA(boolean rootCA) {
        isRootCA = rootCA;
    }

    public String getPemEncoding() {
        return pemEncoding;
    }

    public void setPemEncoding(String pemEncoding) {
        this.pemEncoding = pemEncoding;
    }

    public Calendar getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Calendar validFrom) {
        this.validFrom = validFrom;
    }

    public Calendar getValidTo() {
        return validTo;
    }

    public void setValidTo(Calendar validTo) {
        this.validTo = validTo;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public String normalizedFullDN() {
        if (StringUtils.isBlank(normalizedFullPath) && !StringUtils.isBlank(certificate)) {
            normalizedFullPath = DistinguishedNamesCodingUtil.normalizeOrTrim(certificate, DistinguishedNamesCodingUtil.getCommonAttributesDN());
        }

        return normalizedFullPath;
    }

    public String normalizedShortDN() {
        if (StringUtils.isBlank(normalizedShortPath) && !StringUtils.isBlank(certificate)) {
            normalizedShortPath = DistinguishedNamesCodingUtil.normalizeOrTrim(certificate, DistinguishedNamesCodingUtil.getMinimalAttributesDN());
        }

        return normalizedShortPath;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CertificateDomainBO that = (CertificateDomainBO) o;

        return new EqualsBuilder()
                .append(certificate, that.getCertificate())
                .append(subdomain, that.getSubdomain())
                .append(crl, that.getCrl())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(certificate)
                .append(subdomain)
                .append(crl)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("certificate", certificate)
                .append("subdomain", subdomain)
                .append("crl", crl)
                .append("isRootCA", isRootCA)
                .toString();
    }
}
