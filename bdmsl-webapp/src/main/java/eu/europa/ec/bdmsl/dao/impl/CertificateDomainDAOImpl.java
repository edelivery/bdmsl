/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.impl;

import eu.europa.ec.bdmsl.common.bo.CertificateDomainBO;
import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import eu.europa.ec.bdmsl.common.exception.*;
import eu.europa.ec.bdmsl.dao.AbstractDAOImpl;
import eu.europa.ec.bdmsl.dao.ICertificateDomainDAO;
import eu.europa.ec.bdmsl.dao.entity.CertificateDomainEntity;
import eu.europa.ec.bdmsl.dao.entity.SubdomainEntity;
import eu.europa.ec.bdmsl.security.CertificateDetails;
import eu.europa.ec.edelivery.text.DistinguishedNamesCodingUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Repository;

import jakarta.persistence.Query;
import jakarta.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static eu.europa.ec.bdmsl.dao.QueryNames.*;

/**
 * @author Adrien FERIAL
 * @since 15/06/2015
 */
@Repository
public class CertificateDomainDAOImpl extends AbstractDAOImpl implements ICertificateDomainDAO {

    @Autowired
    private ConversionService conversionService;

    @Override
    public CertificateDomainBO findIssuerBasedAuthorizationDomain(String subject, String issuer, String globalSubjectRexExp) throws TechnicalException {
        List<CertificateDomainBO> certificateDomainBOs = findAll();
        String orderedFullName = DistinguishedNamesCodingUtil.normalizeDN(issuer, DistinguishedNamesCodingUtil.getCommonAttributesDN());
        String orderedShortName = DistinguishedNamesCodingUtil.normalizeDN(issuer, DistinguishedNamesCodingUtil.getMinimalAttributesDN());

        certificateDomainBOs = certificateDomainBOs.stream().filter(certificateDomainBO -> matchIssuerBasedAuthorizationDomain(orderedFullName, orderedShortName, certificateDomainBO, subject, globalSubjectRexExp)).collect(Collectors.toList());


        if (certificateDomainBOs.size() == 0) {
            return null;
        }

        if (certificateDomainBOs.size() == 1) {
            return certificateDomainBOs.get(0);
        }
        throw new CertificateNotFoundException("Certificate match multiple domains!");
    }

    protected boolean matchIssuerBasedAuthorizationDomain(String orderedFullSubject, String orderedShortSubject, CertificateDomainBO certificateDomainBO, String subject, String globalRegExp) {
        String orderedDomainSubject = certificateDomainBO.normalizedFullDN();
        String orderedDomainShortSubject = certificateDomainBO.normalizedShortDN();
        if (!certificateDomainBO.isRootCA()) {
            return false;
        }
        if (!orderedDomainSubject.equalsIgnoreCase(orderedFullSubject) && !orderedDomainShortSubject.equalsIgnoreCase(orderedShortSubject)) {
            return false;
        }
        SubdomainBO subdomainBO = certificateDomainBO.getSubdomain();

        String regExp = StringUtils.isBlank(subdomainBO.getSmpCertSubjectRegex()) ?
                globalRegExp : subdomainBO.getSmpCertSubjectRegex();

        Pattern regExpCollection = StringUtils.isNotBlank(regExp) ? Pattern.compile(regExp) : null;
        if (regExpCollection == null) {
            return true;
        }
        Matcher matcher = regExpCollection.matcher(subject);
        return matcher.find();
    }

    @Override
    public CertificateDomainBO findDomain(String certificate, boolean isRootCertificate) throws TechnicalException {
        // get all domains from cache
        List<CertificateDomainBO> certificateDomainBOs = findAll();
        String orderedFullSubject = DistinguishedNamesCodingUtil.normalizeDN(certificate, DistinguishedNamesCodingUtil.getCommonAttributesDN());
        String orderedShortSubject = DistinguishedNamesCodingUtil.normalizeDN(certificate, DistinguishedNamesCodingUtil.getMinimalAttributesDN());


        for (CertificateDomainBO certificateDomainBO : certificateDomainBOs) {
            // test to check expected certificate type
            if (isRootCertificate != certificateDomainBO.isRootCA()) {
                continue;
            }

            String orderedDomainSubject = certificateDomainBO.normalizedFullDN();
            String orderedDomainShortSubject = certificateDomainBO.normalizedShortDN();

            if (orderedDomainSubject.equalsIgnoreCase(orderedFullSubject)) {
                return certificateDomainBO;
            }

            if (orderedDomainShortSubject.equalsIgnoreCase(orderedShortSubject)) {
                return certificateDomainBO;
            }
        }
        return null;
    }

    @Override
    public List<CertificateDomainBO> getDomainByCertificateAlias(String alias, boolean isRootCertificate) {
        List<CertificateDomainEntity> certificateDomainEntities
                = getEntityManager().createNamedQuery(CERTIFICATE_DOMAIN_ENTITY_GET_BY_CERTIFICATE_ALIAS_AND_IS_ROOT, CertificateDomainEntity.class)
                .setParameter("truststoreAlias", alias)
                .setParameter("isRoot", isRootCertificate)
                .getResultList();

        if (!isRootCertificate && certificateDomainEntities.size() > 1) {
            loggingService.warn("Non root certificate [" + alias + "] is registered to more than one domain!");
        }
        // map to CertificateDomainBO
        return certificateDomainEntities.stream().map(certificateDomainEntity ->
                conversionService.convert(certificateDomainEntity, CertificateDomainBO.class)
        ).collect(Collectors.toList());
    }

    @Override
    public CertificateDomainBO findDomain(CertificateDetails certificateDetails) throws TechnicalException {
        if (certificateDetails != null) {

            CertificateDomainBO nonRootCA = findDomain(certificateDetails.getSubject(), false);
            if (nonRootCA != null) {
                return nonRootCA;
            }

            CertificateDomainBO rootCA = findDomain(certificateDetails.getRootCertificateDN(), true);
            if (rootCA != null) {
                return rootCA;
            }
            throw new CertificateNotFoundException("Impossible to find out the certificate domain. Subject: [" + certificateDetails.getSubject() + "] Issuer: [" + certificateDetails.getIssuer() + "]");
        }
        throw new CertificateNotFoundException("Impossible to find out the certificate domain for null certificateDetails!");

    }

    /**
     * Not many certificates are expected to be registered in bdmsl_certificate_domain
     * because certificate issuer based domain authorization is expected for large networks.
     *
     * @return cached domain certificates
     * @throws TechnicalException if any error occurs
     */
    @Override
    @Cacheable("allDomains")
    public List<CertificateDomainBO> findAll() throws TechnicalException {
        List<CertificateDomainEntity> certificateDomainEntities
                = getEntityManager().createNamedQuery(CERTIFICATE_DOMAIN_ENTITY_GET_ALL, CertificateDomainEntity.class).getResultList();
        List<CertificateDomainBO> resultBOList = new ArrayList<CertificateDomainBO>();
        if (certificateDomainEntities != null && !certificateDomainEntities.isEmpty()) {
            for (CertificateDomainEntity certificateDomainEntity : certificateDomainEntities) {
                resultBOList.add(conversionService.convert(certificateDomainEntity, CertificateDomainBO.class));
            }
        }
        return resultBOList;
    }

    @Override
    public SubdomainBO findSubDomainForCertificateId(String certificateId) throws TechnicalException {
        TypedQuery<SubdomainEntity> query = getEntityManager().createNamedQuery(SUBDOMAIN_ENTITY_GET_BY_CERTIFICATE_ID, SubdomainEntity.class);
        query.setParameter(CERTIFICATE_ID, certificateId);
        List<SubdomainEntity> resultEntities = query.getResultList();
        if (resultEntities.isEmpty()) {
            loggingService.debug("CertificateId [" + certificateId + "] is not registered on any domain!");
            return null;
        }
        if (resultEntities.size() > 1) {
            throw new BadConfigurationException("CertificateId [" + certificateId + "] is registered on more that one domain!");
        }
        return conversionService.convert(resultEntities.get(0), SubdomainBO.class);
    }


    @Override
    public List<CertificateDomainBO> findBySubDomain(String domainName) throws TechnicalException {
        List<CertificateDomainBO> allList = findAll();
        if (StringUtils.isBlank(domainName)) {
            throw new BadRequestException("Subdomain name must not be null!");
        }

        return allList.stream().filter(certificateDomainBO
                        -> certificateDomainBO.getSubdomain().getSubdomainName().equalsIgnoreCase(domainName))
                .collect(Collectors.toList());

    }

    @Override
    public void createCertificateDomain(CertificateDomainBO certificateDomainBO) throws TechnicalException {

        CertificateDomainEntity certificateDomainEntity = conversionService.convert(certificateDomainBO, CertificateDomainEntity.class);
        super.persist(certificateDomainEntity);
    }


    @Override
    public CertificateDomainBO findCertificateDomainByCertificate(String certificate) throws TechnicalException {
        CertificateDomainBO certificateDomainBO = null;

        Optional<CertificateDomainEntity> optCertificateDomainEntity = getCertificateDomainEntityByCertificateId(certificate);
        if (optCertificateDomainEntity.isPresent()) {
            certificateDomainBO = conversionService.convert(optCertificateDomainEntity.get(), CertificateDomainBO.class);
        }

        return certificateDomainBO;
    }

    @Override
    public CertificateDomainBO deleteCertificateDomain(CertificateDomainBO certificateDomainBO) throws TechnicalException {
        // in order to catch deletion with Envers do no use Delete statement
        Optional<CertificateDomainEntity> optCertificateDomainEntity = getCertificateDomainEntityByCertificateId(certificateDomainBO.getCertificate());
        if (optCertificateDomainEntity.isPresent()) {
            CertificateDomainEntity certificateDomainEntity = optCertificateDomainEntity.get();
            getEntityManager().remove(certificateDomainEntity);
            return conversionService.convert(optCertificateDomainEntity.get(), CertificateDomainBO.class);
        }
        return null;
    }

    private Optional<CertificateDomainEntity> getCertificateDomainEntityByCertificateId(String certificateId) throws GenericTechnicalException {
        List<CertificateDomainEntity> lstVal;
        try {
            Query query = getEntityManager().createNamedQuery(CERTIFICATE_DOMAIN_ENTITY_GET_BY_CERTIFICATE_ID, CertificateDomainEntity.class);
            query.setParameter(CERTIFICATE_ID, certificateId);
            lstVal = query.getResultList();
        } catch (Exception ex) {
            throw new GenericTechnicalException(String.format("Impossible to find out the current certificate domain by the certificate id [%s] ", certificateId), ex);
        }

        if (lstVal.isEmpty()) {
            return Optional.empty();
        } else if (lstVal.size() == 1) {
            return Optional.of(lstVal.get(0));
        } else {
            throw new GenericTechnicalException(String.format("More than one certificate domain for the certificate id %s ", certificateId));
        }
    }

    private Optional<CertificateDomainEntity> getCertificateDomainEntityByCertificateIdAndSubdomain(String certificateId, Long subdomainId) throws GenericTechnicalException {
        List<CertificateDomainEntity> lstVal;
        try {
            TypedQuery<CertificateDomainEntity> query = getEntityManager().createNamedQuery(CERTIFICATE_DOMAIN_ENTITY_GET_BY_CERTIFICATE_ID_AND_SUB_DOMAIN_ID, CertificateDomainEntity.class);
            query.setParameter(CERTIFICATE_ID, certificateId);
            query.setParameter(SUBDOMAIN_ID, subdomainId);
            lstVal = query.getResultList();
        } catch (Exception ex) {
            throw new GenericTechnicalException(String.format("Impossible to find out the current certificate domain by the certificate id [%s] and subdomainId [%s] ", certificateId, subdomainId), ex);
        }

        if (lstVal.isEmpty()) {
            return Optional.empty();
        } else if (lstVal.size() == 1) {
            return Optional.of(lstVal.get(0));
        } else {
            throw new GenericTechnicalException(String.format("More than one certificate domain for the certificate id [%s] and subdomain id [%s]", certificateId, subdomainId));
        }
    }

    public List<CertificateDomainBO> listCertificateDomains(String certificate, SubdomainBO subdomainBO) throws TechnicalException {

        Optional<SubdomainEntity> subdomainEntity = subdomainBO == null ? Optional.empty() : getSubdomainEntity(subdomainBO);
        try {
            TypedQuery<CertificateDomainEntity> query;
            if (!StringUtils.isBlank(certificate) && !subdomainEntity.isPresent()) {
                query = getEntityManager().createNamedQuery(CERTIFICATE_DOMAIN_ENTITY_LIST_BY_CERTIFICATE_ID, CertificateDomainEntity.class);
                query.setParameter(CERTIFICATE_ID, "%" + certificate + "%");
            } else if (StringUtils.isBlank(certificate) && subdomainEntity.isPresent()) {
                query = getEntityManager().createNamedQuery(CERTIFICATE_DOMAIN_ENTITY_LIST_BY_SUB_DOMAIN, CertificateDomainEntity.class);
                query.setParameter("subdomain", subdomainEntity.get());
            } else if (!StringUtils.isBlank(certificate) && subdomainEntity.isPresent()) {
                query = getEntityManager().createNamedQuery(CERTIFICATE_DOMAIN_ENTITY_LIST_BY_CERTIFICATE_ID_AND_SUB_DOMAIN, CertificateDomainEntity.class);
                query.setParameter("subdomain", subdomainEntity.get());
                query.setParameter(CERTIFICATE_ID, "%" + certificate + "%");
            } else {
                query = getEntityManager().createNamedQuery(CERTIFICATE_DOMAIN_ENTITY_GET_ALL, CertificateDomainEntity.class);
            }
            return query.getResultList().stream()
                    .map(result -> conversionService.convert(result, CertificateDomainBO.class))
                    .collect(Collectors.toList());
        } catch (Exception ex) {
            throw new GenericTechnicalException(String.format("Error occured while searching domain certificates %s", certificate), ex);
        }
    }

    @Override
    public CertificateDomainBO updateX509CertificateForCertificateDomain(CertificateDomainBO certificateDomainBO) throws TechnicalException {

        Optional<CertificateDomainEntity> optCertEntity = getCertificateDomainEntityByCertificateId(certificateDomainBO.getCertificate());
        if (!optCertEntity.isPresent()) {
            throw new CertificateNotFoundException("Certificate domain not found! CertificateDomain: " + certificateDomainBO.getCertificate());
        }
        CertificateDomainEntity certificateDomainEntity = optCertEntity.get();
        certificateDomainEntity.setPemEncoding(certificateDomainBO.getPemEncoding());
        certificateDomainEntity.setValidFrom(certificateDomainBO.getValidFrom());
        certificateDomainEntity.setValidTo(certificateDomainBO.getValidTo());
        certificateDomainEntity.setCrl(certificateDomainBO.getCrl());
        certificateDomainEntity.setTruststoreAlias(certificateDomainBO.getTruststoreAlias());

        CertificateDomainEntity result = getEntityManager().merge(certificateDomainEntity);
        return conversionService.convert(result, CertificateDomainBO.class);
    }

    @Override
    public CertificateDomainBO updateCertificateDomain(CertificateDomainBO certificate, SubdomainBO newSubdomainBO, String newCrlUrl, Boolean isAdminCertificate) throws TechnicalException {

        Optional<CertificateDomainEntity> optCertEntity = getCertificateDomainEntityByCertificateId(certificate.getCertificate());
        if (!optCertEntity.isPresent()) {
            throw new CertificateNotFoundException("Certificate domain not found! CertificateDomain: " + certificate.getCertificate());
        }
        CertificateDomainEntity certificateDomainEntity = optCertEntity.get();
        // update subdomain
        if (newSubdomainBO != null &&
                certificateDomainEntity.getSubdomain().getSubdomainId() != newSubdomainBO.getSubdomainId().longValue()) {

            Optional<SubdomainEntity> subdomainEntity = getSubdomainEntity(newSubdomainBO);
            if (!subdomainEntity.isPresent()) {
                throw new NotFoundException("No subdomain for domain in database:" + newSubdomainBO);
            } else {
                certificateDomainEntity.setSubdomain(subdomainEntity.get());
            }

        }

        if (isAdminCertificate != null) {
            certificateDomainEntity.setAdmin(isAdminCertificate);
        }
        // update crl
        if (newCrlUrl != null) {
            certificateDomainEntity.setCrl(StringUtils.isBlank(newCrlUrl) ? null : newCrlUrl);
        }

        CertificateDomainEntity result = getEntityManager().merge(certificateDomainEntity);
        return conversionService.convert(result, CertificateDomainBO.class);
    }

    public CertificateDomainBO deleteCertificateDomain(CertificateDomainBO certificateDomainBO, SubdomainBO subdomainBO) throws TechnicalException {
        // in order to catch deletion with Envers do no use Delete statement
        Optional<CertificateDomainEntity> optCertificateDomainEntity = getCertificateDomainEntityByCertificateIdAndSubdomain(certificateDomainBO.getCertificate(), subdomainBO.getSubdomainId());
        if (optCertificateDomainEntity.isPresent()) {
            CertificateDomainEntity certificateDomainEntity = optCertificateDomainEntity.get();
            getEntityManager().remove(certificateDomainEntity);
            return conversionService.convert(optCertificateDomainEntity.get(), CertificateDomainBO.class);
        }
        return null;
    }

    private Optional<SubdomainEntity> getSubdomainEntity(SubdomainBO subdomainBO) {

        TypedQuery<SubdomainEntity> query = getEntityManager().createNamedQuery(SUBDOMAIN_ENTITY_GET_BY_ID, SubdomainEntity.class);
        query.setParameter("id", subdomainBO.getSubdomainId());
        List<SubdomainEntity> resultEntityList = query.getResultList();
        if (resultEntityList.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(resultEntityList.get(0));
    }
}
