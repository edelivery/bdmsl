/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.dns.impl;

import eu.europa.ec.bdmsl.common.exception.SIG0Exception;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xbill.DNS.DNSSEC;

import java.io.*;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.spec.*;
import java.util.Base64;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.apache.commons.lang3.StringUtils.split;
import static org.apache.commons.lang3.StringUtils.trim;

/**
 * Util Class is implementation of Bind9 private key parser for sig0 signatures
 * Supported key algorithms: DSA, RSA, EC, ED25519, ED448
 *
 * @author Joze RIHTARSIC
 * @since 4.3
 * <p>
 */
public class Bind9PrivateKeyParser {

    private static final Logger LOG = LoggerFactory.getLogger(Bind9PrivateKeyParser.class);
    // generic parameters
    public static final String KEY_PROPERTY_ALGORITHM = "Algorithm";
    public static final String KEY_PROPERTY_CREATED = "Created";
    public static final String KEY_PROPERTY_PUBLISH = "Publish";
    public static final String KEY_PROPERTY_ACTIVATE = "Activate";
    // DSA parameters
    public static final String KEY_PROPERTY_PRIME = "Prime(p)";
    public static final String KEY_PROPERTY_SUBPRIME = "Subprime(q)";
    public static final String KEY_PROPERTY_BASE = "Base(g)";
    public static final String KEY_PROPERTY_PRIVATE_VALUE = "Private_value(x)";
    public static final String KEY_PROPERTY_PUBLIC_VALUE = "Public_value(y)";
    // RSA parameters
    public static final String KEY_PROPERTY_MODULUS = "Modulus";
    public static final String KEY_PROPERTY_PUBLIC_EXPONENT = "PublicExponent";
    public static final String KEY_PROPERTY_PRIVATE_EXPONENT = "PrivateExponent";
    public static final String KEY_PROPERTY_PRIME1 = "Prime1";
    public static final String KEY_PROPERTY_PRIME2 = "Prime2";
    public static final String KEY_PROPERTY_EXPONENT1 = "Exponent1";
    public static final String KEY_PROPERTY_EXPONENT2 = "Exponent2";
    public static final String KEY_PROPERTY_COEFFICIENT = "Coefficient";
    // EC parameters
    public static final String KEY_PROPERTY_PRIVATE_KEY = "PrivateKey";


    // RFC 5114 Section 2.6
    private static final ECParameterSpec ECDSA_P256 = toECParameterSpec(
            parseHexEncodedBigInteger("FFFFFFFF00000001000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF"),
            parseHexEncodedBigInteger("FFFFFFFF00000001000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFC"),
            parseHexEncodedBigInteger("5AC635D8AA3A93E7B3EBBD55769886BC651D06B0CC53B0F63BCE3C3E27D2604B"),
            parseHexEncodedBigInteger("6B17D1F2E12C4247F8BCE6E563A440F277037D812DEB33A0F4A13945D898C296"),
            parseHexEncodedBigInteger("4FE342E2FE1A7F9B8EE7EB4A7C0F9E162BCE33576B315ECECBB6406837BF51F5"),
            parseHexEncodedBigInteger("FFFFFFFF00000000FFFFFFFFFFFFFFFFBCE6FAADA7179E84F3B9CAC2FC632551"));


    // RFC 5114 Section 2.7
    private static final ECParameterSpec ECDSA_P384 = toECParameterSpec(
            parseHexEncodedBigInteger("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFF0000000000000000FFFFFFFF"),
            parseHexEncodedBigInteger("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFF0000000000000000FFFFFFFC"),
            parseHexEncodedBigInteger("B3312FA7E23EE7E4988E056BE3F82D19181D9C6EFE8141120314088F5013875AC656398D8A2ED19D2A85C8EDD3EC2AEF"),
            parseHexEncodedBigInteger("AA87CA22BE8B05378EB1C71EF320AD746E1D3B628BA79B9859F741E082542A385502F25DBF55296C3A545E3872760AB7"),
            parseHexEncodedBigInteger("3617DE4A96262C6F5D9E98BF9292DC29F8F41DBD289A147CE9DA3113B5F0B8C00A60B1CE1D7E819D7A431D7C90EA0E5F"),
            parseHexEncodedBigInteger("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC7634D81F4372DDF581A0DB248B0A77AECEC196ACCC52973"));

    protected Bind9PrivateKeyParser() {
    }

    /**
     * Converts input stream to a PrivateKey, using the algorithm specified in the key  header line (e.g. "Algorith: 15 (ED25519)")
     *
     * @param inputStream - input stream of the key
     * @return PrivateKey - java.security.PrivateKey
     * @throws SIG0Exception - if key can not be parsed
     */
    public static PrivateKey toPrivateKey(InputStream inputStream) throws SIG0Exception {
        LOG.debug("Parsing private key from input stream");
        Map<String, String> values = parsePrivateKeyValues(inputStream);
        String algorithm = values.get(KEY_PROPERTY_ALGORITHM);
        if (StringUtils.isBlank(algorithm)) {
            throw new SIG0Exception("Algorithm is missing!");
        }
        String[] algDesc = split(algorithm, " ", 2);
        int algValue = Integer.parseInt(algDesc[0]);
        return toPrivateKey(algValue, values);
    }

    /**
     * Converts key values to a PrivateKey, using the algorithm specified in the key  header line (e.g. "Algorith: 15 (ED25519)")
     *
     * @param values - key values
     * @return PrivateKey - java.security.PrivateKey
     * @throws SIG0Exception - if invalid key values
     */
    public static PrivateKey toPrivateKey(int alg, Map<String, String> values) throws SIG0Exception {

        switch (alg) {
            case DNSSEC.Algorithm.RSAMD5:
            case DNSSEC.Algorithm.RSASHA1:
            case DNSSEC.Algorithm.RSA_NSEC3_SHA1:
            case DNSSEC.Algorithm.RSASHA256:
            case DNSSEC.Algorithm.RSASHA512:
                return toRSAPrivateKey(values);
            case DNSSEC.Algorithm.DSA:
            case DNSSEC.Algorithm.DSA_NSEC3_SHA1:
                return toDSAPrivateKey(values);
            case DNSSEC.Algorithm.ECDSAP256SHA256:
                return toECDSAPrivateKey(values, ECDSA_P256);
            case DNSSEC.Algorithm.ECDSAP384SHA384:
                return toECDSAPrivateKey(values, ECDSA_P384);
            case DNSSEC.Algorithm.ED25519:
                return toEdDSAPrivateKey(values, "1.3.101.112");
            case DNSSEC.Algorithm.ED448:
                return toEdDSAPrivateKey(values, "1.3.101.113");
            default:
                throw new SIG0Exception("Unsupported private key  algorithm [" + alg + "]");
        }
    }

    /**
     * Creates a new DSAPrivateKeySpec with the specified parameter values.
     * Params:
     * x – the private key. p – the prime. q – the sub-prime. g – the base.
     *
     * @param values - key values
     * @return PrivateKey - java.security.PrivateKey
     * @throws TechnicalException      - if invalid key values
     * @throws InvalidKeySpecException - if invalid key values
     */
    protected static PrivateKey toDSAPrivateKey(Map<String, String> values) throws SIG0Exception {
        LOG.info("Parsing DSA private key");
        DSAPrivateKeySpec privateKeySpec = new DSAPrivateKeySpec(
                parseBase64EncodedBigInteger(values.get(KEY_PROPERTY_PRIVATE_VALUE)),
                parseBase64EncodedBigInteger(values.get(KEY_PROPERTY_PRIME)),
                parseBase64EncodedBigInteger(values.get(KEY_PROPERTY_SUBPRIME)),
                parseBase64EncodedBigInteger(values.get(KEY_PROPERTY_BASE)));

        return generatePrivateKey("DSA", null, privateKeySpec);
    }

    /**
     * Creates a new RSAPrivateCrtKeySpec with the specified parameter values.
     * Params:
     * <ul><li>modulus - the modulus n</li>
     * <li>publicExponent  the public exponent e</li>
     * <li>privateExponent the private exponent d</li>
     * <li>primeP          the prime factor p of n</li>
     * <li>primeQ          the prime factor q of n</li>
     * <li>primeExponentP  this is d mod (p-1)</li>
     * <li>primeExponentQ  this is d mod (q-1)</li>
     * <li>crtCoefficient  the Chinese Remainder Theorem coefficient q-1 mod p</li>
     * </
     *
     * @param values - key values from the key
     * @return PrivateKey - java.security.PrivateKey
     * @throws SIG0Exception - if invalid key values
     */
    protected static PrivateKey toRSAPrivateKey(Map<String, String> values) throws SIG0Exception {
        LOG.info("Parsing RSA private key");
        BigInteger modulus = parseBase64EncodedBigInteger(values.get(KEY_PROPERTY_MODULUS));
        BigInteger publicExp = parseBase64EncodedBigInteger(values.get(KEY_PROPERTY_PUBLIC_EXPONENT));
        BigInteger privateExp = parseBase64EncodedBigInteger(values.get(KEY_PROPERTY_PRIVATE_EXPONENT));
        BigInteger prime1 = parseBase64EncodedBigInteger(values.get(KEY_PROPERTY_PRIME1));
        BigInteger prime2 = parseBase64EncodedBigInteger(values.get(KEY_PROPERTY_PRIME2));
        BigInteger exp1 = parseBase64EncodedBigInteger(values.get(KEY_PROPERTY_EXPONENT1));
        BigInteger exp2 = parseBase64EncodedBigInteger(values.get(KEY_PROPERTY_EXPONENT2));
        BigInteger crtCoef = parseBase64EncodedBigInteger(values.get(KEY_PROPERTY_COEFFICIENT));

        RSAPrivateCrtKeySpec privateKeySpec = new RSAPrivateCrtKeySpec(modulus, publicExp, privateExp, prime1, prime2, exp1, exp2, crtCoef);
        return generatePrivateKey("RSA", null, privateKeySpec);
    }

    /**
     * Creates a new EdDSAPrivateKeySpec with the specified parameter values.
     * Params:
     * <ul><li>seed - the private key seed</li>
     * <li>params - the key parameters</li>
     * </ul
     *
     * @param values - key values from the key
     * @return PrivateKey - java.security.PrivateKey
     * @throws SIG0Exception - if invalid key values
     */
    protected static PrivateKey toECDSAPrivateKey(Map<String, String> values, ECParameterSpec ecKeySpec) throws SIG0Exception {
        LOG.info("Parsing ECDSA private key");
        ECPrivateKeySpec privateKeySpec = new ECPrivateKeySpec(parseBase64EncodedBigInteger(values.get(KEY_PROPERTY_PRIVATE_KEY)), ecKeySpec);

        return generatePrivateKey("EC", null, privateKeySpec);
    }

    /**
     * Use bounce castle provider to parse ED private keys. After JDK 15 this should be supported by the JDK and method can be rewrited
     * to use pure JCA API.
     *
     * @param values
     * @param keyAlgId
     * @return
     * @throws SIG0Exception
     */
    protected static PrivateKey toEdDSAPrivateKey(Map<String, String> values, String keyAlgId)
            throws SIG0Exception {
        LOG.info("Parsing EdDSA private key [{}]", keyAlgId);
        ASN1ObjectIdentifier id_edwards_curve_algs = new ASN1ObjectIdentifier(keyAlgId);

        String keyString = values.get(KEY_PROPERTY_PRIVATE_KEY);
        byte[] key = Base64.getDecoder().decode(keyString);

        PKCS8EncodedKeySpec pkcs8KeySpec;
        try {
            PrivateKeyInfo privKeyInfo = new PrivateKeyInfo(new AlgorithmIdentifier(id_edwards_curve_algs), new DEROctetString(key));
            pkcs8KeySpec = new PKCS8EncodedKeySpec(privKeyInfo.getEncoded());
        } catch (IOException e) {

            throw new SIG0Exception("Can not initiate instantiate EdDSA PrivateKeyInfo", e);
        }
        // EdDSA is not supported by the JDK  up tp JDK15, so we need to use Bouncy Castle
        return generatePrivateKey("EdDSA", BouncyCastleProvider.PROVIDER_NAME, pkcs8KeySpec);
    }

    /**
     * Creates a new DSAPrivateKeySpec with the specified parameter values.
     *
     * @param algorithm      - the private algorithm name
     * @param provider       - the JCA provider name for the algorithm, if null the default provider will be used.
     * @param privateKeySpec - the key spec for the private key to be generated
     * @return PrivateKey - java.security.PrivateKey
     * @throws SIG0Exception - if key algorighm is not supported or invalid KeySpec values
     */
    private static PrivateKey generatePrivateKey(String algorithm, String provider, KeySpec privateKeySpec) throws SIG0Exception {
        KeyFactory keyFactory;
        try {
            keyFactory = StringUtils.isBlank(provider) ? KeyFactory.getInstance(algorithm) : KeyFactory.getInstance(algorithm, provider);
            return keyFactory.generatePrivate(privateKeySpec);
        } catch (NoSuchAlgorithmException e) {
            throw new SIG0Exception("Can not initiate the key factory with the [" + algorithm + "] algorithm", e);
        } catch (InvalidKeySpecException e) {
            throw new SIG0Exception("Can not generate the private key", e);
        } catch (NoSuchProviderException e) {
            throw new SIG0Exception("Can not generate the private key. Provider [" + provider + "] does not exist!", e);
        }
    }

    private static ECParameterSpec toECParameterSpec(BigInteger p, BigInteger a, BigInteger b, BigInteger
            gx, BigInteger gy, BigInteger n) {
        return new ECParameterSpec(new EllipticCurve(new ECFieldFp(p), a, b), new ECPoint(gx, gy), n, 1);
    }


    /**
     * Parse private key from input stream and return file lines as stream of strings
     *
     * @param inputs - input key input stream
     * @return Stream<String> - stream of key lines
     * @throws IOException - if stream can not be read
     */
    private static Stream<String> lines(InputStream inputs) throws IOException {

        final BufferedReader br = new BufferedReader(new InputStreamReader(inputs));
        try {
            return br.lines().onClose(asUncheckedRunnable(br));
        } catch (Error | RuntimeException e) {
            try {
                br.close();
            } catch (IOException ex) {
                try {
                    e.addSuppressed(ex);
                } catch (Throwable ignore) {
                }
            }
            throw e;
        }
    }

    /**
     * Convert a Closeable to a Runnable by converting checked IOException
     * to UncheckedIOException
     */
    private static Runnable asUncheckedRunnable(Closeable c) {
        return () -> {
            try {
                c.close();
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        };
    }

    /**
     * Parse the Base64 encoded value to BigInteger.
     *
     * @param value
     * @return
     * @throws SIG0Exception
     */
    private static BigInteger parseBase64EncodedBigInteger(String value) throws SIG0Exception {
        try {
            final byte[] data = Base64.getDecoder().decode(StringUtils.replace(value, "\t", ""));
            return new BigInteger(1, data);
        } catch (NumberFormatException ex) {
            throw new SIG0Exception("Error occurred while parsing the private key value!", ex);
        }
    }

    /**
     * Parse string to BigInteger
     *
     * @param s - string to parse
     * @return BigInteger - parsed value
     */
    private static BigInteger parseHexEncodedBigInteger(String s) {
        return new BigInteger(s, 16);
    }


    /**
     * Parse private key from input stream and return key values as map
     *
     * @param inputs - input key input stream
     * @return Map<String, String> - map of key values
     * @throws SIG0Exception - if key can not be parsed
     */
    private static Map<String, String> parsePrivateKeyValues(final InputStream inputs) throws SIG0Exception {
        Map<String, String> values;
        try (Stream<String> keyLineStream = lines(inputs)) {
            values = keyLineStream.filter(StringUtils::isNotBlank)
                    .map(s -> split(s, ":", 2))
                    .collect(Collectors.toMap(s -> trim(s[0]), s -> trim(s[1])));
        } catch (IOException e) {
            throw new SIG0Exception("Error occurred while parsing the private key!", e);
        }
        return values;
    }
}
