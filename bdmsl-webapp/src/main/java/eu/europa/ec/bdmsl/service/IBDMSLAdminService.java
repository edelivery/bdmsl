/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service;

import eu.europa.ec.bdmsl.common.bo.*;
import eu.europa.ec.bdmsl.common.enums.AdminParticipantIdentifierManageActionEnum;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;

import java.math.BigInteger;
import java.util.List;

/**
 * @author Joze RIHTARSIC
 * @since 15/03/2019
 */
public interface IBDMSLAdminService {

    /**
     * Trigger generation of inconsistency report. Because method
     * heavily use CPU and memory it checks if generation of report is already in progress.
     * If yes than method must not trigger new generation.
     *
     * @throws TechnicalException a technical exception
     */
    String generateInconsistencyReport(String recipientEmailAddress) throws TechnicalException;

    /**
     * Generic function for the report generation.
     *
     * @throws TechnicalException a technical exception
     */
    String generateReport(String reportCode, String recipientEmailAddress) throws TechnicalException;

    SubdomainBO createSubDomain(SubdomainBO subdomainBO) throws TechnicalException;

    SubdomainBO updateSubDomain(String domainName, String regExp, String dnsRecordType, String urlSchemas,
                                String certSubRegExp,
                                String certPolicyOIDs,
                                BigInteger maxParticipantCountForDomain,
                                BigInteger maxParticipantCountForSMP)
            throws TechnicalException;

    SubdomainBO deleteSubDomain(String domainName) throws TechnicalException;

    SubdomainBO getSubDomain(String domainName) throws TechnicalException;

    CertificateDomainBO addCertificateDomain(byte[] x509CertBytes, String domainName, boolean isRootCertificate, boolean isAdminCertificate, String truststoreAlias) throws TechnicalException;

    CertificateDomainBO updateCertificateDomain(String certificateId, String domainName, String crlUrl, Boolean isAdminCertificate) throws TechnicalException;

    CertificateDomainBO deleteCertificateDomain(String certificateId, String domainName) throws TechnicalException;

    List<CertificateDomainBO> listCertificateDomains(String certificateId, String domainName) throws TechnicalException;

    List<DNSRecordBO> deleteDNSRecord(String dnsRecordName, String dnsZone) throws TechnicalException;

    DNSRecordBO addDNSRecord(DNSRecordBO messagePart) throws TechnicalException;

    ConfigurationBO setPropertyToDatabase(String key, String value, String description) throws TechnicalException;

    ConfigurationBO getPropertyFromDatabase(String key) throws TechnicalException;

    ConfigurationBO deletePropertyFromDatabase(String key) throws TechnicalException;

    TruststoreCertificateBO addCertificateToTruststore(byte[] x509CertBytes, String alias) throws TechnicalException;

    TruststoreCertificateBO deleteCertificateFromTruststore(String alias) throws TechnicalException;

    TruststoreCertificateBO getCertificateFromTruststore(String alias) throws TechnicalException;

    List<String> listTruststoreCertificateAliases(String containsAlias) throws TechnicalException;

    /**
     * Clears all the caches
     *
     */
    void clearCache();

    /**
     * Method allows DomiSML administrators to activate, deactivate or delete serviceMetadataPublisher.
     * Only deactivated serviceMetadataPublisher can be deleted.
     * Before executing the action method validates that serviceMetadataPublisherId belongs
     * to the domainZone and certificateOwnerId. If not it throws exception. The service
     * is invoked asynchronously and the result is sent to the recipientEmailAddress.
     *
     * @param action                     name defined in enum {@link eu.europa.ec.bdmsl.common.enums.AdminSMPManageActionEnum}
     * @param recipientEmailAddress      email address of the recipient of the report
     * @param serviceMetadataPublisherId SMP identifier
     * @param domainZone                 domain DNS zone for safety validation
     * @param certificateOwnerId         certificate owner identifier for safety validation
     * @param logicalAddress             logical address of the SMP (SMP URL address)
     * @param physicalAddress            physical address of the SMP (IP address)
     * @return description of the successfully executed action.
     * @throws TechnicalException in case action is not allowed or other technical errors
     */
    String manageServiceMetadataPublisher(String action, String recipientEmailAddress, String serviceMetadataPublisherId,
                                          String domainZone, String certificateOwnerId,
                                          String logicalAddress, String physicalAddress) throws TechnicalException;



    /**
     * Method allows DomiSML administrators to create/delete publishers resource/participant
     * entries.
     *
     * @param action                     name defined in enum {@link eu.europa.ec.bdmsl.common.enums.AdminParticipantIdentifierManageActionEnum}
     * @param publisherId                publisher identifier who owns the participant
     * @param participantBOList          list of participants to managed
     * @return description of the successfully executed action.
     * @throws TechnicalException in case action is not allowed or other technical errors
     */
    String manageParticipantIdentifiers(AdminParticipantIdentifierManageActionEnum action,
                                        String publisherId, List<ParticipantBO>  participantBOList ) throws TechnicalException;
}
