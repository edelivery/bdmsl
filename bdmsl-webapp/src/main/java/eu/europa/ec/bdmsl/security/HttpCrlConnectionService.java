/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.security;

import eu.europa.ec.edelivery.security.cert.ProxyDataCallback;
import eu.europa.ec.edelivery.security.cert.URLFetcher;
import eu.europa.ec.edelivery.security.cert.impl.HttpURLFetcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;


/**
 * Purpose of the class is to fetch CRL data from given  url. Class uses {@link ProxyDataCallback}
 * for http proxy configuration.
 *
 * @author Joze RIHTARSIC
 * @since 4.1
 */
@Component
public class HttpCrlConnectionService implements URLFetcher {

    HttpURLFetcher httpConnection;

    @Autowired
    public HttpCrlConnectionService(ProxyDataCallback proxyDataCallback) {
        httpConnection = new HttpURLFetcher(proxyDataCallback);
    }

    @Override
    public InputStream fetchURL(String url) throws IOException {
        return fetchURL(url, null, null);
    }

    @Override
    public InputStream fetchURL(String url, Map<String, String> map, InputStream inputStream) throws IOException {
        return httpConnection.fetchURL(url, map, inputStream);
    }
}
