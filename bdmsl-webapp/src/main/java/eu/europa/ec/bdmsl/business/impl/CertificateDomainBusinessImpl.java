/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.business.impl;

import eu.europa.ec.bdmsl.business.ICertificateDomainBusiness;
import eu.europa.ec.bdmsl.business.IConfigurationBusiness;
import eu.europa.ec.bdmsl.common.bo.CertificateBO;
import eu.europa.ec.bdmsl.common.bo.CertificateDomainBO;
import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import eu.europa.ec.bdmsl.common.business.AbstractBusinessImpl;
import eu.europa.ec.bdmsl.common.exception.BadRequestException;
import eu.europa.ec.bdmsl.common.exception.GenericTechnicalException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.dao.ICertificateDomainDAO;
import eu.europa.ec.bdmsl.security.CertificateDetails;
import eu.europa.ec.bdmsl.security.UnsecureAuthentication;
import eu.europa.ec.bdmsl.service.ITruststoreService;
import eu.europa.ec.bdmsl.util.CertificateUtils;
import eu.europa.ec.edelivery.security.PreAuthenticatedCertificatePrincipal;
import eu.europa.ec.edelivery.security.utils.X509CertificateUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @author Adrien FERIAL
 * @since 15/06/2015
 */
@Component
public class CertificateDomainBusinessImpl extends AbstractBusinessImpl implements ICertificateDomainBusiness {

    @Autowired
    private ICertificateDomainDAO certificateDomainDAO;

    @Autowired
    IConfigurationBusiness configurationBusiness;

    @Autowired
    private ITruststoreService truststoreService;

    @Override
    public Optional<CertificateDomainBO> findDomainForPrincipal(PreAuthenticatedCertificatePrincipal principal, boolean issuerBasedDomains) throws TechnicalException {

        CertificateDomainBO certificateDomainBO;
        if (issuerBasedDomains) {
            certificateDomainBO = certificateDomainDAO.findIssuerBasedAuthorizationDomain(principal.getSubjectCommonDN(), principal.getIssuerDN(), configurationBusiness.getSMPCertRegularExpression());
        } else {
            certificateDomainBO = certificateDomainDAO.findDomain(principal.getSubjectCommonDN(), false);
        }
        return Optional.ofNullable(certificateDomainBO);
    }

    public List<CertificateDomainBO> getDomainForCertificateAlias(String alias, boolean issuerBasedDomains) {
        return StringUtils.isBlank(alias) ? Collections.emptyList() : certificateDomainDAO.getDomainByCertificateAlias(alias, issuerBasedDomains);
    }

    @Override
    public CertificateDomainBO findDomain(String certificate) throws TechnicalException {
        CertificateDomainBO domainBO = certificateDomainDAO.findDomain(certificate, false);
        if (domainBO == null) {
            domainBO = certificateDomainDAO.findDomain(certificate, true);
        }
        return domainBO;
    }

    @Override
    public CertificateDomainBO findDomain(CertificateDetails certificateDetails) throws TechnicalException {
        return certificateDomainDAO.findDomain(certificateDetails);
    }

    public SubdomainBO findSubDomainForCertificateId(String certificateId) throws TechnicalException {
        return certificateDomainDAO.findSubDomainForCertificateId(certificateId);
    }

    @Override
    public List<CertificateDomainBO> findAll() throws TechnicalException {
        return certificateDomainDAO.findAll();
    }

    @Override
    public CertificateDomainBO deleteCertificateDomain(CertificateBO oldCertificateBO) throws TechnicalException {
        try {
            CertificateDomainBO currentCertificateDomainBO = certificateDomainDAO.findCertificateDomainByCertificate(CertificateUtils.removeSerialFromSubject(oldCertificateBO.getCertificateId()));
            if (currentCertificateDomainBO != null) {
                return deleteCertificateDomain(currentCertificateDomainBO);
            }
            return null;
        } catch (TechnicalException exc) {
            loggingService.error(exc.getMessage(), exc);
            throw new GenericTechnicalException(exc.getMessage(), exc);
        }
    }

    @Override
    public CertificateDomainBO deleteCertificateDomain(CertificateDomainBO certificateDomainBO) throws TechnicalException {
        try {
            loggingService.info(String.format("Removing old certificate domain %s. ", certificateDomainBO));
            return certificateDomainDAO.deleteCertificateDomain(certificateDomainBO);
        } catch (TechnicalException exc) {
            loggingService.error(exc.getMessage(), exc);
            throw new GenericTechnicalException(exc.getMessage(), exc);
        }
    }

    public CertificateDomainBO findRootCaCertificateDomainForCertificate(X509Certificate certificate) throws TechnicalException {
        String issuerDN = CertificateUtils.normalizeForX509(certificate.getIssuerDN().toString());
        return certificateDomainDAO.findDomain(issuerDN, true);
    }

    @Override
    public boolean isCertificateDomainNonRootCA(CertificateBO certificateBO) throws TechnicalException {
        // unsecured example is root ca
        if (StringUtils.equals(UnsecureAuthentication.UNSECURE_HTTP_CLIENT, certificateBO.getCertificateId())) {
            loggingService.debug("Unsecure certificate is a root Certificate.");
            return false;
        }
        String certificateDomain = CertificateUtils.removeSerialFromSubject(certificateBO.getCertificateId());
        CertificateDomainBO certificateDomainBO = certificateDomainDAO.findDomain(certificateDomain, false);
        return certificateDomainBO != null;
    }

    @Override
    public CertificateDomainBO addCertificateDomain(X509Certificate newCertificate, SubdomainBO subdomainBO, boolean isRootCertificate, boolean isAdminCertificate, String truststoreAlias) throws TechnicalException {

        PreAuthenticatedCertificatePrincipal principal = X509CertificateUtils.extractPrincipalFromCertificate(newCertificate);
        String certificateDomain = principal.getSubjectShortDN();

        String httpCrlDistributionPoint;
        try {
            List<String> crlList = X509CertificateUtils.getCrlDistributionPoints(newCertificate);
            httpCrlDistributionPoint = X509CertificateUtils.extractHttpCrlDistributionPoint(crlList);
        } catch (CertificateException e) {
            throw new GenericTechnicalException("Error while extracting CRL distribution point URLs", e);
        }

        // check if certificate already exist  database
        CertificateDomainBO certificateDomainBO = certificateDomainDAO.findDomain(certificateDomain, isRootCertificate);
        if (certificateDomainBO != null && !certificateDomainBO.isRootCA()) {
            loggingService.error("Error occurred while adding certificate [" + certificateDomain
                    + "]. It is already registered for domain with certificate id:[" + certificateDomainBO.getCertificate() + "]!", null);
            throw new BadRequestException("Certificate already registered for domain: [" + certificateDomainBO.getSubdomain().getSubdomainName() + "]!");
        }

        // test if certificate subject has minimal required values
        CertificateUtils.validateSubjectDNRequiredValues(newCertificate.getSubjectX500Principal().getName());

        // add certificate to truststore.
        CertificateDomainBO newCertificateDomainBO = new CertificateDomainBO();
        newCertificateDomainBO.setCertificate(certificateDomain);
        newCertificateDomainBO.setRootCA(isRootCertificate);
        newCertificateDomainBO.setAdmin(isAdminCertificate);
        newCertificateDomainBO.setCrl(httpCrlDistributionPoint);
        newCertificateDomainBO.setSubdomain(subdomainBO);
        newCertificateDomainBO.setValidFrom(DateUtils.toCalendar(principal.getNotBefore()));
        newCertificateDomainBO.setValidTo(DateUtils.toCalendar(principal.getNotAfter()));
        newCertificateDomainBO.setPemEncoding(getPemValue(newCertificate));

        // add certificate to truststore
        try {
            String alias = truststoreService.getAliasForCertificate(newCertificate);
            if (StringUtils.isEmpty(alias)) {
                // certificate does not exist add certificate to truststore
                alias = truststoreService.addCertificate(newCertificate, truststoreAlias);
            }
            newCertificateDomainBO.setTruststoreAlias(alias);
        } catch (TechnicalException e) {
            if (configurationBusiness.isLegacyDomainAuthorizationEnabled()) {
                loggingService.warn("Can not add certificate to truststore! Error: " + ExceptionUtils.getRootCauseMessage(e), e);
            } else {
                throw e;
            }
        }

        certificateDomainDAO.createCertificateDomain(newCertificateDomainBO);
        return newCertificateDomainBO;
    }

    /**
     * Update of certificate for nonRoot certificate!
     *
     * @param existingCertificateDomainBO existing certificate domain
     * @param newCertificate             new certificate
     * @return  updated certificate domain
     * @throws TechnicalException if error occurs
     */
    public CertificateDomainBO updateX509CertificateForCertificateDomain(CertificateDomainBO existingCertificateDomainBO, X509Certificate newCertificate) throws TechnicalException {

        if (existingCertificateDomainBO.isRootCA()) {
            throw new BadRequestException("Domain Certificate is rootCA certificate and can be only changed manually: [" + existingCertificateDomainBO.getCertificate() + "]!");
        }

        PreAuthenticatedCertificatePrincipal principal = X509CertificateUtils.extractPrincipalFromCertificate(newCertificate);
        String certificateDomain = principal.getSubjectShortDN();

        String httpCrlDistributionPoint;
        try {
            List<String> crlList = X509CertificateUtils.getCrlDistributionPoints(newCertificate);
            httpCrlDistributionPoint = X509CertificateUtils.extractHttpCrlDistributionPoint(crlList);
        } catch (CertificateException e) {
            throw new GenericTechnicalException("Error while extracting CRL distribution point URLs", e);
        }

        // test if certificate subject has minimal required values
        CertificateUtils.validateSubjectDNRequiredValues(newCertificate.getSubjectX500Principal().getName());

        // add certificate to truststore.
        existingCertificateDomainBO.setCertificate(certificateDomain);
        existingCertificateDomainBO.setCrl(httpCrlDistributionPoint);
        existingCertificateDomainBO.setValidFrom(DateUtils.toCalendar(principal.getNotBefore()));
        existingCertificateDomainBO.setValidTo(DateUtils.toCalendar(principal.getNotAfter()));
        existingCertificateDomainBO.setPemEncoding(getPemValue(newCertificate));

        // add certificate to truststore
        try {
            String alias = truststoreService.getAliasForCertificate(newCertificate);
            if (StringUtils.isEmpty(alias)) {
                // certificate does not exist add certificate to truststore
                alias = truststoreService.addCertificate(newCertificate, null);
            }
            existingCertificateDomainBO.setTruststoreAlias(alias);
        } catch (TechnicalException e) {
            if (configurationBusiness.isLegacyDomainAuthorizationEnabled()) {
                loggingService.warn("Can not add certificate to truststore! Error: " + ExceptionUtils.getRootCauseMessage(e), e);
            } else {
                throw e;
            }
        }

        return certificateDomainDAO.updateX509CertificateForCertificateDomain(existingCertificateDomainBO);
    }

    @Override
    public void changeCertificateDomain(CertificateBO oldCertificateBO, CertificateBO newCertificateBO, X509Certificate newCertificate, boolean createNewCertificateDomain) throws
            TechnicalException {
        try {
            CertificateDomainBO currentCertificateDomainBO = certificateDomainDAO.findCertificateDomainByCertificate(CertificateUtils.removeSerialFromSubject(oldCertificateBO.getCertificateId()));
            if (currentCertificateDomainBO == null) {
                throw new GenericTechnicalException(String.format("Impossible to change Non Root certificate.The current certificate domain %s does not exist or it might be a Root CA certificate.", oldCertificateBO));
            }

            if(currentCertificateDomainBO.isRootCA()) {
                loggingService.info("Changing or creating a non-root certificate is not allowed");
                return;
            }

            if(!createNewCertificateDomain) {
                loggingService.info("Creating a new certificate domain is not allowed");
                return;
            }

            String newCertificateDomain = CertificateUtils.removeSerialFromSubject(newCertificateBO.getCertificateId());
            String oldCertificateDomain = CertificateUtils.removeSerialFromSubject(oldCertificateBO.getCertificateId());

            Optional<CertificateDomainBO> existsNewCertificateDomainBO = getExistingCertificate(currentCertificateDomainBO, newCertificateDomain, oldCertificateDomain);
            if (!existsNewCertificateDomainBO.isPresent()) {
                createCertificate(newCertificate, newCertificateBO, currentCertificateDomainBO, newCertificateDomain, oldCertificateDomain);
            }
        } catch (TechnicalException exc1) {
            loggingService.error(exc1.getMessage(), exc1);
            throw exc1;
        } catch (Exception exc2) {
            loggingService.error(exc2.getMessage(), exc2);
            throw new BadRequestException(exc2.getMessage(), exc2);
        }
    }

    private Optional<CertificateDomainBO> getExistingCertificate(CertificateDomainBO currentCertificateDomainBO, String newCertificateDomain, String oldCertificateDomain) throws TechnicalException {
        // check if new certificate is already registered in domain and is not the "same domain" by subject
        Optional<CertificateDomainBO> existsNewCertificateDomainBO = Optional.empty();
        if (!StringUtils.equals(oldCertificateDomain, newCertificateDomain)) {
            existsNewCertificateDomainBO = Optional.ofNullable(certificateDomainDAO.findCertificateDomainByCertificate(newCertificateDomain));
            if (existsNewCertificateDomainBO.isPresent()) {
                loggingService.warn(String.format("The new certificate domain %s already exists in the database.", newCertificateDomain));
                if (!Objects.equals(existsNewCertificateDomainBO.get().getSubdomain().getSubdomainId(),
                        currentCertificateDomainBO.getSubdomain().getSubdomainId())) {
                    throw new GenericTechnicalException(String.format("The new certificate domain %s already exists in the database on different domain.", newCertificateDomain));
                }
            }
        }
        return existsNewCertificateDomainBO;
    }

    private void createCertificate(X509Certificate newCertificate, CertificateBO newCertificateBO, CertificateDomainBO currentCertificateDomainBO, String newCertificateDomain, String oldCertificateDomain) throws TechnicalException {
        if (!StringUtils.equals(oldCertificateDomain, newCertificateDomain)) {
            loggingService.info(String.format("Creating certificate domain %s for the new certificate %s.", newCertificateDomain, newCertificateBO));

            addCertificateDomain(newCertificate, currentCertificateDomainBO.getSubdomain(),
                    currentCertificateDomainBO.isRootCA(), currentCertificateDomainBO.isAdmin(), null);
            loggingService.info(String.format("Removing old certificate domain %s.", currentCertificateDomainBO));
            deleteCertificateDomain(currentCertificateDomainBO);
        } else {
            loggingService.info(String.format("Update certificate domain %s with the new certificate %s.", newCertificateDomain, newCertificateBO));
            updateX509CertificateForCertificateDomain(currentCertificateDomainBO, newCertificate);
        }
    }

    public void validateCRLDistributionUrl(String url) throws BadRequestException {

        URL urltest;
        try {
            URI test = URI.create(url);
            urltest = test.toURL();
        } catch (MalformedURLException | IllegalArgumentException e) {
            loggingService.error(e.getMessage(), e);
            throw new BadRequestException("Malformed Certificate revocation list URL: [" + url + "]!. Error: " + e.getMessage());
        }
        String scheme = urltest.getProtocol().toLowerCase();
        if (!scheme.equals("https") && !scheme.equals("http")) {
            throw new BadRequestException("Not supported url scheme for CRL distribution url. Supported values: http, https. Found: [" + scheme + "]");
        }
    }

    @Override
    public void changeCertificateDomain(X509Certificate newCertificate, CertificateBO oldCertificateBO, CertificateBO newCertificateBO, boolean isToCreateNewCertificateDomain) throws TechnicalException {
        try {
            // check if old certificate is nonRootCA then replace it with  the new certificate
            if (isCertificateDomainNonRootCA(oldCertificateBO)) {
                changeCertificateDomain(oldCertificateBO, newCertificateBO, newCertificate, isToCreateNewCertificateDomain);
            } else {
                // check if new  certificate is registered as RootCA if yes throw exception because it is not allowed
                // to replace a Root CA certificate by a Non Root CA certificate.
                CertificateDomainBO newCertDomain = findRootCaCertificateDomainForCertificate(newCertificate);
                if (newCertDomain == null) {
                    throw new BadRequestException("It is not allowed to replace a Root CA certificate by a Non Root CA certificate.");
                }
                SubdomainBO subdomainBO = findSubDomainForCertificateId(oldCertificateBO.getCertificateId());
                if (subdomainBO == null) {
                    loggingService.warn("The old certificate [" + oldCertificateBO.getCertificateId() + "] is not associated to any domain.");
                    return;
                }
                if (!subdomainBO.equals(newCertDomain.getSubdomain())) {
                    throw new BadRequestException("The new IssuerBasedAuthorized certificate [" + newCertificateBO.getCertificateId() + "] belongs to domain: ["
                            + newCertDomain.getSubdomain().getSubdomainName() + "] but old belongs to domain: [" + subdomainBO.getSubdomainName() + "] .");
                }


                loggingService.info("Nothing to do because SML does not add certificate domain for ROOT CA certificates on change certificate.");
            }
        } catch (TechnicalException exc1) {
            loggingService.error(exc1.getMessage(), exc1);
            throw exc1;
        } catch (Exception exc2) {
            loggingService.error(exc2.getMessage(), exc2);
            throw new BadRequestException(exc2.getMessage(), exc2);
        }
    }

    @Override
    public CertificateDomainBO updateCertificateDomain(CertificateDomainBO certificate, SubdomainBO newSubdomainBO, String newCrlUrl, Boolean isAdminCertificate) throws TechnicalException {
        String crlUr = StringUtils.trim(newCrlUrl);
        if (!StringUtils.isBlank(crlUr)) {
            validateCRLDistributionUrl(crlUr);
        }

        if (isAdminCertificate != null && isAdminCertificate && certificate.isRootCA()) {
            throw new BadRequestException("Only non root domain certificate can be flagged as admin!");
        }
        return certificateDomainDAO.updateCertificateDomain(certificate, newSubdomainBO, crlUr, isAdminCertificate);
    }

    @Override
    public CertificateDomainBO deleteCertificateDomain(CertificateDomainBO certificate, SubdomainBO subdomain) throws TechnicalException {
        loggingService.info(String.format("Removing certificate domain %s from subdomain.", certificate, subdomain));
        return certificateDomainDAO.deleteCertificateDomain(certificate, subdomain);
    }

    @Override
    public List<CertificateDomainBO> listCertificateDomains(String certificate, SubdomainBO subdomainBO) throws TechnicalException {
        return certificateDomainDAO.listCertificateDomains(certificate, subdomainBO);
    }

    private String getPemValue(X509Certificate x509Cert) {
        StringWriter sw = new StringWriter();
        try {
            try (JcaPEMWriter pw = new JcaPEMWriter(sw)) {
                pw.writeObject(x509Cert);
            }
        } catch (IOException e) {
            loggingService.error("Error occurred while creating pem file for certificate:" + x509Cert.getSubjectX500Principal().getName(), e);
        }
        return sw.toString();
    }
}
