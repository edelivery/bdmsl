/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.impl;

import eu.europa.ec.bdmsl.common.bo.CertificateDomainBO;
import eu.europa.ec.bdmsl.common.bo.SubdomainBO;
import eu.europa.ec.bdmsl.common.exception.BadConfigurationException;
import eu.europa.ec.bdmsl.common.exception.BadRequestException;
import eu.europa.ec.bdmsl.common.exception.NotFoundException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.dao.AbstractDAOImpl;
import eu.europa.ec.bdmsl.dao.ICertificateDomainDAO;
import eu.europa.ec.bdmsl.dao.ISubdomainDAO;
import eu.europa.ec.bdmsl.dao.entity.SubdomainEntity;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Repository;

import jakarta.persistence.Query;
import jakarta.persistence.TypedQuery;
import jakarta.transaction.Transactional;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static eu.europa.ec.bdmsl.common.exception.Messages.BAD_REQUEST_PARAMETER_NOT_EXISTS_SUBDOMAIN;
import static eu.europa.ec.bdmsl.dao.QueryNames.*;

/**
 * @author Tiago MIGUEL
 * @since 02/03/2017
 */
@Repository
public class SubdomainDAOImpl extends AbstractDAOImpl implements ISubdomainDAO {

    @Autowired
    private ICertificateDomainDAO certificateDomainDAO;

    @Autowired
    private ConversionService conversionService;


    @Override
    public List<SubdomainBO> findAll() throws TechnicalException {
        List<SubdomainEntity> resultEntityList = getEntityManager().createNamedQuery(SUBDOMAIN_ENTITY_GET_ALL, SubdomainEntity.class).getResultList();
        List<SubdomainBO> resultBOList = new ArrayList<>();
        if (resultEntityList != null && !resultEntityList.isEmpty()) {
            for (SubdomainEntity subdomainEntity : resultEntityList) {
                resultBOList.add(conversionService.convert(subdomainEntity, SubdomainBO.class));
            }
        }
        return resultBOList;
    }

    public Optional<SubdomainBO> getSubDomainByName(String domainName) throws TechnicalException {

        Optional<SubdomainEntity> optionalSubdomainEntity = getSubDomainEntityByName(domainName);
        if (optionalSubdomainEntity.isPresent()) {
            return Optional.of(conversionService.convert(optionalSubdomainEntity.get(),
                    SubdomainBO.class));
        } else {
            return Optional.empty();
        }
    }

    private Optional<SubdomainEntity> getSubDomainEntityByName(String domainName) throws TechnicalException {
        // get by list to avoid unchecked exception use list instead getSingleResult
        TypedQuery<SubdomainEntity> query = getEntityManager().createNamedQuery(SUBDOMAIN_ENTITY_GET_BY_NAME, SubdomainEntity.class);
        query.setParameter("name", domainName);
        List<SubdomainEntity> resultEntityList = query.getResultList();
        if (resultEntityList.isEmpty()) {
            return Optional.empty();
        } else if (resultEntityList.size() == 1) {
            return Optional.of(resultEntityList.get(0));
        } else {
            throw new BadConfigurationException("More than one subdomain for domain name" + domainName);
        }
    }

    @Override
    public List<String> findAllSchemesPerDomain() throws TechnicalException {
        Query query = getEntityManager().createNamedQuery(SUBDOMAIN_ENTITY_GET_ALL_SCHEMES_PER_DOMAIN);
        List<Object> resultList = query.getResultList();
        return resultList.stream().map(val -> val == null ? "" : val.toString()).collect(Collectors.toList());
    }

    @Override
    public void createSubDomain(SubdomainBO subdomainBO) throws TechnicalException {

        SubdomainEntity subdomainEntity = conversionService.convert(subdomainBO, SubdomainEntity.class);
        super.persist(subdomainEntity);
    }

    @Override
    @Transactional
    public void updateSubDomainValues(String domainName,
                                      String regExp,
                                      String dnsRecordType,
                                      String urlSchemas,
                                      String certSubjectRegEx,
                                      String certPolicyOids,
                                      BigInteger maxParticipantCountForDomain,
                                      BigInteger maxParticipantCountForSMP) throws TechnicalException {
        Optional<SubdomainEntity> optSubDomain = getSubDomainEntityByName(domainName);
        if (optSubDomain.isPresent()) {
            SubdomainEntity subdomainEntity = optSubDomain.get();
            boolean hasChanged = false;
            // update only no null/empty
            if (!StringUtils.isEmpty(regExp)) {
                subdomainEntity.setParticipantIdRegexp(regExp.trim());
                hasChanged = true;
            }
            // update only no null/empty / blank values
            if (!StringUtils.isEmpty(dnsRecordType)) {
                subdomainEntity.setDnsRecordTypes(dnsRecordType.trim());
                hasChanged = true;
            }
            // update only no null/empty / blank values
            if (!StringUtils.isEmpty(urlSchemas)) {
                subdomainEntity.setSmpUrlSchemas(urlSchemas.trim());
                hasChanged = true;
            }
            // update only no null values
            if (certSubjectRegEx != null) {
                subdomainEntity.setSmpCertSubjectRegex(certSubjectRegEx.trim());
                hasChanged = true;
            }
            // update only no null values
            if (!StringUtils.isBlank(certPolicyOids)) {
                subdomainEntity.setSmpCertPolicyOIDs(certPolicyOids);
                hasChanged = true;
            }
            // update only no null values
            if (maxParticipantCountForDomain != null) {
                subdomainEntity.setMaxParticipantCountForDomain(maxParticipantCountForDomain.longValue());
                hasChanged = true;
            }
            // update only no null values
            if (maxParticipantCountForSMP != null) {
                subdomainEntity.setMaxParticipantCountForSMP(maxParticipantCountForSMP.longValue());
                hasChanged = true;
            }
            if (hasChanged) {
                super.merge(subdomainEntity);
            }
        } else {
            throw new NotFoundException(BAD_REQUEST_PARAMETER_NOT_EXISTS_SUBDOMAIN + domainName);
        }

    }

    @Override
    public SubdomainBO deleteSubDomain(String domainName) throws TechnicalException {
        Optional<SubdomainEntity> optSubDomain = getSubDomainEntityByName(domainName);
        if (!optSubDomain.isPresent()) {
            throw new NotFoundException(BAD_REQUEST_PARAMETER_NOT_EXISTS_SUBDOMAIN + domainName);
        }
        List<CertificateDomainBO> crtLst = certificateDomainDAO.findBySubDomain(domainName);
        if (!crtLst.isEmpty()) {
            throw new BadRequestException("SubDomain has registered certificates count: " + crtLst.size());
        }
        SubdomainEntity subdomainEntity = optSubDomain.get();

        super.getEntityManager().remove(subdomainEntity);

        return conversionService.convert(subdomainEntity, SubdomainBO.class);
    }

    @Override
    public SubdomainBO getSubDomain(String domainName) throws TechnicalException {
        Optional<SubdomainEntity> optSubDomain = getSubDomainEntityByName(domainName);
        if (!optSubDomain.isPresent()) {
            throw new NotFoundException(BAD_REQUEST_PARAMETER_NOT_EXISTS_SUBDOMAIN + domainName);
        }

        SubdomainEntity subdomainEntity = optSubDomain.get();

        return conversionService.convert(subdomainEntity, SubdomainBO.class);
    }
}
