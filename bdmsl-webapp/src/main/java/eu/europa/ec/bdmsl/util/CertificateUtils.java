/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.util;

import eu.europa.ec.bdmsl.common.bo.CertificateBO;
import eu.europa.ec.bdmsl.common.exception.BadConfigurationException;
import eu.europa.ec.bdmsl.common.exception.CertificateAuthenticationException;
import eu.europa.ec.bdmsl.common.exception.GenericTechnicalException;
import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.common.util.Constant;
import eu.europa.ec.bdmsl.security.UnsecureAuthentication;
import eu.europa.ec.edelivery.security.PreAuthenticatedCertificatePrincipal;
import eu.europa.ec.edelivery.security.utils.X509CertificateUtils;
import eu.europa.ec.edelivery.text.DistinguishedNamesCodingUtil;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.authentication.BadCredentialsException;

import javax.naming.InvalidNameException;
import javax.naming.ldap.LdapName;
import javax.naming.ldap.Rdn;
import javax.security.auth.x500.X500Principal;
import jakarta.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.Normalizer;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class with some utils used in Certificates
 *
 * @author Tiago MIGUEL
 * @author Flavio SANTOS
 * @since 06/12/2016
 */
public class CertificateUtils {

    private static final Pattern HEX_LITERALS_PATTERN = Pattern.compile("(\\\\x[0-9a-fA-F]{2})+");

    private static final Log logger = LogFactory.getLog(CertificateUtils.class);

    /**
     * The regular expression to be used for finding serial number with decimal and hexadecimal representation
     */
    public static final String SHORT_SERIAL_NUMBER_DECIMAL_HEX_REPRESENTANTION_REGEX = "\\d+\\s*\\((.*?)\\)$";

    private static final Pattern DIACRITICS = Pattern.compile("[\\p{InCombiningDiacriticalMarks}\\p{IsLm}\\p{IsSk}]+");


    /**
     * Issuer normalization contains only most commons attributes:
     * CN: CommonName
     * OU: OrganizationalUnit
     * O: Organization
     * L: Locality
     * ST: StateOrProvinceName
     * C: CountryName
     * Ignore trouble making emailAddress, SeriaNumber becase different ReverseProxy add this attributes in various way.
     * (As part of CN separated by / or as seperate RDN...)
     */
    protected static final List<String> DOMAIN_ATTRIBUTES = Arrays.asList("CN", "OU", "O", "L", "ST", "C");

    /**
     * Subject of the certificate must have values
     * CN: CommonName
     * O: Organization
     * C: CountryName
     */
    private static final String[] SUBJECT_REQUIRED_DN_VALUES = {"CN", "O", "C"};

    public static List<String> getDomainCertAttributes() {
        return DOMAIN_ATTRIBUTES;
    }

    public static String[] getSubjectRequiredDnValues() {
        return SUBJECT_REQUIRED_DN_VALUES;
    }

    /**
     * Left pads a String with 0's
     *
     * @param str String to be padded
     * @return String padded
     */
    private static String convertStringToPaddedString(String str) {
        return StringUtils.leftPad(str, 32, "0");
    }

    /**
     * Converts number into an Hexadecimal String
     *
     * @param number BigInteger number
     * @return Hexadecimal string value of number
     */
    public static String convertBigIntToHexString(BigInteger number) {
        return convertStringToPaddedString(number.toString(16));
    }

    public static void validateSubjectDNRequiredValues(String subjectDn) throws CertificateAuthenticationException {

        if (!X509CertificateUtils.hasDistinguishedNameAllRequiredValues(subjectDn, CertificateUtils.getSubjectRequiredDnValues())) {
            throw new CertificateAuthenticationException("Certificate Subject DN [" + subjectDn + "] for required values: [" + String.join(",", CertificateUtils.getSubjectRequiredDnValues()) + "].");
        }
    }

    public static CertificateBO extractCertificate(X509Certificate newCert) throws TechnicalException {
        PreAuthenticatedCertificatePrincipal principal = extractPrincipalFromCertificate(newCert);
        CertificateBO certificateBO = new CertificateBO();

        certificateBO.setValidFrom(DateUtils.toCalendar(principal.getNotBefore()));
        certificateBO.setValidTo(DateUtils.toCalendar(principal.getNotAfter()));
        certificateBO.setCertificateId(principal.getName(32));

        try {
            certificateBO.setPemEncoding(Base64.getEncoder().encodeToString(newCert.getEncoded()));
        } catch (CertificateEncodingException cee) {
            throw new GenericTechnicalException("Error occurred while encoding the certificate: " + newCert.getSubjectX500Principal().getName(), cee);
        }

        return certificateBO;
    }

    public static PreAuthenticatedCertificatePrincipal extractPrincipalFromCertificate(X509Certificate cert) {

        String subject = cert.getSubjectX500Principal().getName(X500Principal.RFC2253);
        String issuer = cert.getIssuerX500Principal().getName(X500Principal.RFC2253);
        BigInteger serial = cert.getSerialNumber();

        return new PreAuthenticatedCertificatePrincipal(cert, subject, issuer, serial, cert.getNotBefore(), cert.getNotAfter());
    }

    public static String calculateCertificateId(final X509Certificate cert) {
        return extractPrincipalFromCertificate(cert).getName(32);
    }

    /**
     * Removes hexadecimal representation between brackets from decimal serial number string
     *
     * @param serial number to be normalized
     * @return serial number normalized
     */
    public static String removeShorterDecimalFromHexSerialNumberString(String serial) {
        Matcher m = Pattern.compile(SHORT_SERIAL_NUMBER_DECIMAL_HEX_REPRESENTANTION_REGEX).matcher(serial);
        if (m.matches()) {
            serial = m.group(1);
        }

        return serial;
    }

    /**
     * Returns Certificate Id
     *
     * @param subjectName  Subject Name
     * @param serialNumber Serial Number
     * @return CertificateId composed by SubjectName + ":" + SerialNumber HexString
     */
    private static String prepareCertificateId(String subjectName, String serialNumber) {
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append(subjectName);
        strBuilder.append(":");
        serialNumber = removeHexHeader(serialNumber);
        strBuilder.append(convertStringToPaddedString(serialNumber));
        return String.valueOf(strBuilder);
    }

    /**
     * Returns Certificate Id
     *
     * @param subjectName  Subject Name
     * @param serialNumber Serial Number
     * @return CertificateId composed by SubjectName + ":" + SerialNumber HexString
     */
    public static String returnCertificateId(String subjectName, BigInteger serialNumber) {
        return prepareCertificateId(subjectName, convertBigIntToHexString(serialNumber));
    }

    /**
     * Returns Certificate Id
     *
     * @param subjectName  Subject Name
     * @param serialNumber Serial Number
     * @return CertificateId composed by SubjectName + ":" + SerialNumber HexString
     */
    public static String returnCertificateId(String subjectName, String serialNumber) {
        serialNumber = removeShorterDecimalFromHexSerialNumberString(serialNumber);
        return prepareCertificateId(subjectName, serialNumber);
    }

    /**
     * Removes hexa header (0x) from string, if it contains or itself if not
     *
     * @param hexString Hexadecimal String
     * @return String without 0x
     */
    public static String removeHexHeader(String hexString) {
        return hexString.replaceFirst("0x", "");
    }


    private static boolean isUnsecuredUser(String certificate) {
        //this info is malformed so we cannot order it by CN,C,O, it is ignored by this method
        return certificate.equals(UnsecureAuthentication.UNSECURE_HTTP_CLIENT);
    }

    public static String order(String certificate) throws TechnicalException {
        synchronized (CertificateUtils.class) {

            if (isUnsecuredUser((certificate))) {
                return certificate;
            }

            StringBuilder sb = new StringBuilder();
            final LdapName ldapName;
            try {
                ldapName = new LdapName(certificate);
                // Make a map from type to name
                Map<String, Rdn> parts = new HashMap<>();
                for (final Rdn rdn : ldapName.getRdns()) {
                    parts.put(rdn.getType(), rdn);
                }
                // The treemap orders the keys according to the natural ordering. The keys are strings so the order is alphabetical.
                Map<String, Rdn> treeMap = new TreeMap<>(parts);
                int i = 0;

                for (String key : treeMap.keySet()) {
                    sb.append(treeMap.get(key));
                    if (i != treeMap.keySet().size() - 1) {
                        sb.append(",");
                    }
                    i++;
                }

            } catch (InvalidNameException exc) {
                throw new CertificateAuthenticationException("Impossible to re-order the root certificate of the domain " + certificate, exc);
            }

            return sb.toString();
        }
    }

    public static String orderSubjectByDefaultMetadata(String subject) throws CertificateAuthenticationException {
        synchronized (CertificateUtils.class) {
            try {

                if (isUnsecuredUser((subject))) {
                    return subject;
                }
                LdapName ldapName = new LdapName(subject);

                // Make a map from type to name
                final Map<String, Rdn> parts = new HashMap<>();
                for (final Rdn rdn : ldapName.getRdns()) {
                    parts.put(rdn.getType(), rdn);
                }

                return parts.get("CN").toString() + "," + parts.get("O").toString() + "," + parts.get("C").toString();
            } catch (Exception exc) {
                throw new CertificateAuthenticationException("Impossible to identify authorities for certificate " + subject, exc);
            }
        }
    }

    public static String normalizeForBlueCoat(String value) throws UnsupportedEncodingException {
        String normalizedValue = normalizeUnescape(value);
        normalizedValue = normalizeUnicode(normalizedValue);

        return normalizedValue;
    }

    public static String normalizeForX509(String value) {
        try {
            return DistinguishedNamesCodingUtil.normalizeDN(value, DistinguishedNamesCodingUtil.getCommonAttributesDN());
        } catch (BadCredentialsException ex) {
            logger.error("Error occurred while normalizing value: [" + value + "]! Is value LDAP value path!" + ex);
            return value;
        }
    }

    public static String normalizeUnescape(String value) throws UnsupportedEncodingException {
        String unescapedString = URLDecoder.decode(value, Constant.DEFAULT_CHARSET.name());
        unescapedString = StringEscapeUtils.unescapeHtml4(unescapedString);
        unescapedString = decodeUtfHexLiterals(unescapedString);

        return unescapedString;
    }

    public static String decodeUtfHexLiterals(String mixedStringAndHexInput) {
        String unescapedResult = mixedStringAndHexInput;

        for (Matcher m = HEX_LITERALS_PATTERN.matcher(mixedStringAndHexInput); m.find(); m = HEX_LITERALS_PATTERN.matcher(unescapedResult)) {
            String hexWithPrefixes = m.group();
            String hexBinarySequence = hexWithPrefixes.replace("\\x", "");
            byte[] bytes = DatatypeConverter.parseHexBinary(hexBinarySequence);
            String utfDecoded = new String(bytes, StandardCharsets.UTF_8);
            unescapedResult = m.replaceFirst(utfDecoded);
        }

        return unescapedResult;
    }

    /**
     * Fixes not escaped characters within Distinguished Name.
     * I.e.: Special characters defined in RFC2253 (like comma and some others) break DN parsing if they are not escaped from RDN value.
     *
     * @param notEscapedDN Distinguished Name that needs escaping, i.e.: CN=comma,inside,C=PL
     * @return escaped DN, i.e.: CN=comma\,inside,C=PL
     */
    public static String normalizeDistinguishedName(String notEscapedDN) throws IllegalArgumentException {
        return DistinguishedNamesCodingUtil.smartEscapeDistinguishedName(notEscapedDN);
    }

    /**
     * It replaces special charactes by it representant in the alphabet.
     * <p>
     * Ex = "äåãaaáeéiïíoóöőuúüűńñÿ AÁEËÉIÍOÓÖŐUŪÚÜŰŘ" will be replaced byaaaaaaeeiiioooouuuunny AAEEEIIOOOOUUUUUR
     */
    public static String normalizeUnicode(String value) {
        String normalizedValue = Normalizer.normalize(value, Normalizer.Form.NFD);
        return DIACRITICS.matcher(normalizedValue).replaceAll("");
    }

    public static String removeSerialFromSubject(String certificateId) throws TechnicalException {

        int idx = certificateId.lastIndexOf(":");
        if (idx <= 0) {
            throw new GenericTechnicalException("Impossible to remove serial number from the certificate id " + certificateId);
        }
        return certificateId.substring(0, idx);
    }

    public static X509Certificate getNotCACertificate(final X509Certificate[] requestCerts) throws TechnicalException {
        if (requestCerts == null || requestCerts.length == 0) {
            // Empty array
            return null;
        }

        // must be  nonCA certificate (most likely seflsigned)
        if (requestCerts.length == 1) {
            return requestCerts[0];
        }

        // Find all certificates that are not issuer to another certificate
        final List<X509Certificate> nonIssuerCertList = new ArrayList<>();
        for (final X509Certificate requestCert : requestCerts) {
            final X500Principal subject = requestCert.getSubjectX500Principal();

            // Search for the issuer of the current certificate
            boolean found = false;
            for (final X509Certificate issuerCert : requestCerts)
                if (subject.equals(issuerCert.getIssuerX500Principal())) {
                    found = true;
                    break;
                }
            if (!found)
                nonIssuerCertList.add(requestCert);
        }

        // Do we have exactly 1 certificate to verify?
        if (nonIssuerCertList.size() != 1)
            throw new CertificateAuthenticationException("Found " +
                    nonIssuerCertList.size() +
                    " certificates that are not issuer certificates!");

        return nonIssuerCertList.get(0);
    }

    public static X509Certificate getIssuerCertificate(final X509Certificate cert, final X509Certificate[] requestCerts) {
        if (cert == null || requestCerts == null || requestCerts.length == 0) {
            // Empty array
            return null;
        }

        for (final X509Certificate issuer : requestCerts) {
            if (issuer.getSubjectX500Principal().equals(cert.getIssuerX500Principal())) {
                return issuer;
            }
        }

        return null;
    }

    public static KeyStore getKeystore(String keystoreType, File keystorePath, String keystorePassword) throws BadConfigurationException {
        KeyStore ks;
        try {
            ks = KeyStore.getInstance(keystoreType);
            ks.load(new FileInputStream(keystorePath), keystorePassword.toCharArray());
        } catch (KeyStoreException | CertificateException |
                 NoSuchAlgorithmException e) {
            throw new BadConfigurationException("Can not open keystore [" + keystorePath.getAbsolutePath() + "]!", e);
        } catch (IOException e) {
            throw new BadConfigurationException("Error occurred while reading file: [" + keystorePath.getAbsolutePath() + "]!", e);
        }
        return ks;
    }

    public static Key getSigningKey(String keystoreType, File keystorePath, String keystorePassword, String alias) throws BadConfigurationException {
        KeyStore ks = getKeystore(keystoreType, keystorePath, keystorePassword);
        return getSigningKey(ks, keystorePassword, alias);
    }

    public static Key getSigningKey( KeyStore ks, String keystorePassword, String alias) throws BadConfigurationException {

        try {
            if (!ks.containsAlias(alias)) {
                throw new BadConfigurationException("Alias  [" + alias + "] does not exist in keystore!");
            }
            return ks.getKey(alias, keystorePassword.toCharArray());
        } catch (NoSuchAlgorithmException | KeyStoreException e) {
            throw new BadConfigurationException("Error occurred while reading keystore!", e);
        } catch (UnrecoverableKeyException e) {
            throw new BadConfigurationException("Error reading key with alias  [" + alias + "] in keystore!");
        }
    }

    public static X509Certificate getCertificate(KeyStore ks, String alias) throws BadConfigurationException {

        try {
            if (!ks.containsAlias(alias)) {
                throw new BadConfigurationException("Alias  [" + alias + "] does not exist in keystore!");
            }
            return (X509Certificate)ks.getCertificate(alias);
        } catch (KeyStoreException e) {
            throw new BadConfigurationException("Error occurred while reading keystore!", e);
        }
    }


}
