/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.ws;

import eu.europa.ec.bdmsl.common.logging.ILoggingService;
import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.interceptor.AttachmentInInterceptor;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * This interceptor redirects urls without parameter "wsdl" to the correct service call
 *
 * @author Flavio SANTOS
 * @since 13/09/2016
 */
@Component(value = "redirectInterceptor")
public class RedirectInterceptor extends AbstractPhaseInterceptor<Message> {

    @Autowired
    protected ILoggingService loggingService;

    public RedirectInterceptor() {
        super(Phase.RECEIVE);
        getBefore().add(AttachmentInInterceptor.class.getName());
    }

    @Override
    public void handleMessage(Message message) {
        try {
            if (isGET(message)) {
                String query = (String) message.get(Message.QUERY_STRING);
                if (StringUtils.isEmpty(query) ||
                        !(query.toLowerCase().startsWith("xsd") || query.toLowerCase().startsWith("wsdl"))) {
                    query = "wsdl";
                }
                message.put(Message.QUERY_STRING, query);
            }
        } catch (Exception ex) {
            loggingService.error("RedirectInterceptor", ex);
        }
    }
}
