/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.impl;

import eu.europa.ec.bdmsl.business.*;
import eu.europa.ec.bdmsl.common.bo.*;
import eu.europa.ec.bdmsl.common.enums.AdminParticipantIdentifierManageActionEnum;
import eu.europa.ec.bdmsl.common.enums.AdminSMPManageActionEnum;
import eu.europa.ec.bdmsl.common.enums.SMLPropertyEnum;
import eu.europa.ec.bdmsl.common.enums.SMLReportEnum;
import eu.europa.ec.bdmsl.common.exception.*;
import eu.europa.ec.bdmsl.common.service.AbstractServiceImpl;
import eu.europa.ec.bdmsl.security.CertificateAuthentication;
import eu.europa.ec.bdmsl.service.IBDMSLAdminService;
import eu.europa.ec.bdmsl.service.IReportService;
import eu.europa.ec.bdmsl.service.ITruststoreService;
import eu.europa.ec.bdmsl.service.tasks.AdminSmpManagementTask;
import eu.europa.ec.bdmsl.service.tasks.DataInconsistencyReportTask;
import eu.europa.ec.bdmsl.util.DataValidator;
import eu.europa.ec.edelivery.security.utils.X509CertificateUtils;
import eu.europa.ec.edelivery.text.DistinguishedNamesCodingUtil;
import jakarta.transaction.Transactional;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static eu.europa.ec.bdmsl.common.exception.Messages.*;

/**
 * @author Joze RIHTARSIC
 * @since 15/03/2019
 */
@Service
@Transactional(value = Transactional.TxType.REQUIRED, rollbackOn = {RuntimeException.class, TechnicalException.class, DNSClientException.class})
public class BDMSLAdminServiceImpl extends AbstractServiceImpl implements IBDMSLAdminService {

    public static final String LOG_PREFIX = "BDMSLAdminServiceImpl >> ";

    @Autowired
    private IConfigurationBusiness configurationBusiness;

    @Autowired
    private ISubdomainBusiness subdomainBusiness;

    @Autowired
    private ITruststoreService truststoreService;

    @Autowired
    private ICertificateDomainBusiness certificateDomainBusiness;

    @Autowired
    private IManageDNSRecordBusiness manageDNSRecordBusiness;

    @Autowired
    private ThreadPoolTaskExecutor smlReportTaskExecutor;

    @Autowired
    private IReportService reportService;

    @Autowired
    private ApplicationContext appContext;

    @Autowired
    private IAdminManageServiceMetadataBusiness adminManageServiceMetadataBusiness;

    @Autowired
    private IManageParticipantIdentifierBusiness manageParticipantIdentifierBusiness;

    @Override
    @PreAuthorize("hasAnyRole( 'ROLE_ADMIN')")
    public String generateInconsistencyReport(String recipientEmailAddress) throws TechnicalException {
        String rspMsg;
        String emlAddr = validateEmailAddress(recipientEmailAddress, true);

        if (!configurationBusiness.isDNSEnabled()) {
            rspMsg = "DNS server is not enabled. Request for Inconsistency report is ignored!";
            loggingService.info(LOG_PREFIX + rspMsg);
        } else if (smlReportTaskExecutor.getActiveCount() == 0) {

            DataInconsistencyReportTask reportTask = (DataInconsistencyReportTask) appContext.getBean("dataInconsistencyReportTask");

            reportTask.setReceiverAddress(emlAddr);
            smlReportTaskExecutor.execute(reportTask);
            rspMsg = "Start task for generating inconsistency report!";
        } else {
            rspMsg = "Inconsistency report already in progress. Request is ignored!";
            loggingService.info(LOG_PREFIX + rspMsg);
        }

        return rspMsg;
    }

    /**
     * Method parse the report code to enumeration and triggers the report.
     *
     * @param reportCode            - the report code
     * @param recipientEmailAddress - the receiver email for the report
     * @return short description of success
     * @throws TechnicalException - thrown if the report code or email are invalid
     */
    @Override
    @PreAuthorize("hasAnyRole( 'ROLE_ADMIN')")
    public String generateReport(String reportCode, String recipientEmailAddress) throws TechnicalException {

        if (StringUtils.isBlank(reportCode)) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_EMPTY_REPORT);
        }
        Optional<SMLReportEnum> reportEnumOptional = SMLReportEnum.getByName(reportCode);

        SMLReportEnum reportType = reportEnumOptional.orElseThrow(() -> new BadRequestException(BAD_REQUEST_PARAMETER_INVALID_REPORT));

        String emlAddr = validateEmailAddress(recipientEmailAddress, true);

        if (Objects.requireNonNull(reportType) == SMLReportEnum.DATABASE_DNS_INCONSISTENCY) {
            return generateInconsistencyReport(emlAddr);
        } else if (reportType == SMLReportEnum.SMP_WITH_EXPIRED_CERTIFICATE) {
            return reportService.generateSMPsWithExpiredCertificatesReport(emlAddr);
        }
        return "Report type  [" + reportType + "] is not supported!";
    }

    @Override
    @PreAuthorize("hasAnyRole( 'ROLE_ADMIN')")
    public SubdomainBO createSubDomain(SubdomainBO subdomainBO) throws TechnicalException {
        return subdomainBusiness.createSubDomain(subdomainBO);
    }

    @Override
    @PreAuthorize("hasAnyRole( 'ROLE_ADMIN')")
    public SubdomainBO updateSubDomain(String domainName, String regExp, String dnsRecordType, String urlSchemas,
                                       String certSubRegExp,
                                       String certPolicyOIDs,
                                       BigInteger maxParticipantCountForDomain,
                                       BigInteger maxParticipantCountForSMP) throws TechnicalException {
        return subdomainBusiness.updateSubDomain(domainName,
                regExp,
                dnsRecordType,
                urlSchemas,
                certSubRegExp,
                certPolicyOIDs,
                maxParticipantCountForDomain,
                maxParticipantCountForSMP);
    }

    @Override
    @PreAuthorize("hasAnyRole( 'ROLE_ADMIN')")
    public SubdomainBO deleteSubDomain(String domainName) throws TechnicalException {
        return subdomainBusiness.deleteSubDomain(domainName);

    }

    @Override
    @PreAuthorize("hasAnyRole( 'ROLE_ADMIN')")
    public SubdomainBO getSubDomain(String domainName) throws TechnicalException {
        return subdomainBusiness.getSubDomain(domainName);
    }

    @Override
    @PreAuthorize("hasAnyRole( 'ROLE_ADMIN')")
    public CertificateDomainBO addCertificateDomain(byte[] x509CertBytes, String domainName, boolean isRootCertificate
            , boolean isAdminCertificate, String truststoreAlias) throws TechnicalException {

        if (x509CertBytes == null || x509CertBytes.length == 0) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_EMPTY_PUBLIC_KEY);
        }

        if (isRootCertificate && isAdminCertificate) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_INCONSISTEN_DOMAIN_CERT);
        }

        SubdomainBO subdomainBO = subdomainBusiness.getSubDomain(domainName);
        X509Certificate certificate;
        try {
            certificate = X509CertificateUtils.getX509Certificate(x509CertBytes);
        } catch (CertificateException exc) {
            throw new BadRequestException(String.format("The certificate is not valid - [%s]", exc.getMessage()), exc);
        }

        return certificateDomainBusiness.addCertificateDomain(certificate, subdomainBO, isRootCertificate, isAdminCertificate, truststoreAlias);
    }

    @Override
    @PreAuthorize("hasAnyRole( 'ROLE_ADMIN')")
    public CertificateDomainBO updateCertificateDomain(String certificateId,
                                                       String domainName,
                                                       String urlDistributionPoint,
                                                       Boolean isAdminCertificate
    ) throws TechnicalException {
        CertificateDomainBO cd = validateCertificateId(certificateId);
        SubdomainBO subdomainBO = domainName == null ? null : subdomainBusiness.getSubDomain(domainName);
        return certificateDomainBusiness.updateCertificateDomain(cd, subdomainBO, urlDistributionPoint, isAdminCertificate);
    }

    @Override
    @PreAuthorize("hasAnyRole( 'ROLE_ADMIN')")
    public CertificateDomainBO deleteCertificateDomain(String certificateId,
                                                       String domainName
    ) throws TechnicalException {
        CertificateDomainBO cd = validateCertificateId(certificateId);

        CertificateDomainBO currentCertificate = ((CertificateAuthentication) SecurityContextHolder.getContext().getAuthentication()).getGrantedSubDomainAnchor();
        if (cd.equals(currentCertificate)) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_INVALID_CERT_ID_CURRENTLY_IN_USE);
        }

        SubdomainBO subdomainBO = domainName == null ? null : subdomainBusiness.getSubDomain(domainName);
        return certificateDomainBusiness.deleteCertificateDomain(cd, subdomainBO);
    }

    private CertificateDomainBO validateCertificateId(String certificateId) throws TechnicalException {
        String normalizedCertificateId = StringUtils.trimToNull(certificateId);

        if (normalizedCertificateId == null) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_EMPTY_CERTIFICATE);
        }
        try {
            normalizedCertificateId = DistinguishedNamesCodingUtil.normalizeDN(normalizedCertificateId, DistinguishedNamesCodingUtil.getCommonAttributesDN());
        } catch (BadCredentialsException ex) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_INVALID_CERT_ID);
        }

        CertificateDomainBO cd = certificateDomainBusiness.findDomain(normalizedCertificateId);

        if (cd == null) {
            throw new BadRequestException("Domain certificate [" + normalizedCertificateId + "] does not exist!");
        }
        return cd;

    }

    @Override
    @PreAuthorize("hasAnyRole( 'ROLE_ADMIN')")
    public List<CertificateDomainBO> listCertificateDomains(String certificateId,
                                                            String domainName
    ) throws TechnicalException {
        SubdomainBO subdomainBO = StringUtils.isBlank(domainName) ? null : subdomainBusiness.getSubDomain(domainName);
        return certificateDomainBusiness.listCertificateDomains(certificateId, subdomainBO);
    }

    @Override
    @PreAuthorize("hasAnyRole( 'ROLE_ADMIN')")
    public TruststoreCertificateBO addCertificateToTruststore(byte[] x509CertBytes, String alias) throws TechnicalException {
        String aliasPrivate = normalizeAlias(alias);
        X509Certificate certificate;
        // check if certificate alias already exists
        if (!StringUtils.isBlank(aliasPrivate)) {
            aliasPrivate = aliasPrivate.trim().toLowerCase();
            certificate = truststoreService.getCertificate(aliasPrivate);
            if (certificate != null) {
                throw new BadRequestException(String.format("Certificate with alias [%s] already exists!", aliasPrivate));
            }
        }
        try {
            certificate = X509CertificateUtils.getX509Certificate(x509CertBytes);
        } catch (CertificateException exc) {
            throw new BadRequestException(String.format("The certificate is not valid - [%s]", exc.getMessage()), exc);
        }
        // test if certificate already exists
        String entryAlias = truststoreService.getAliasForCertificate(certificate);
        if (!StringUtils.isEmpty(entryAlias)) {
            throw new BadRequestException(String.format("The certificate already exists with alias [%s].", entryAlias));
        }

        aliasPrivate = truststoreService.addCertificate(certificate, aliasPrivate);
        return getCertificateFromTruststore(aliasPrivate);
    }

    @Override
    @PreAuthorize("hasAnyRole( 'ROLE_ADMIN')")
    public TruststoreCertificateBO getCertificateFromTruststore(String alias) throws TechnicalException {
        if (StringUtils.isEmpty(alias)) {
            throw new BadRequestException(String.format("Invalid alias [%s]!", alias));
        }

        X509Certificate certificate = truststoreService.getCertificate(alias);
        if (certificate == null) {
            throw new NotFoundException(String.format("The certificate with alias [%s] does not exist!", alias));
        }
        TruststoreCertificateBO truststoreCertificateBO = new TruststoreCertificateBO();
        truststoreCertificateBO.setAlias(alias);
        try {
            truststoreCertificateBO.setCertificatePublicKey(certificate.getEncoded());
        } catch (CertificateEncodingException exc) {
            throw new BadRequestException(String.format("Can not encode certificate with alias[%s]!", alias), exc);
        }
        return truststoreCertificateBO;
    }

    @Override
    @PreAuthorize("hasAnyRole( 'ROLE_ADMIN')")
    public TruststoreCertificateBO deleteCertificateFromTruststore(String alias) throws TechnicalException {
        if (StringUtils.isEmpty(alias)) {
            throw new BadRequestException(String.format("Invalid alias [%s]!", alias));
        }

        X509Certificate certificate = truststoreService.deleteCertificate(alias);
        if (certificate == null) {
            throw new NotFoundException(String.format("The certificate with alias [%s] does not exist!", alias));
        }

        TruststoreCertificateBO truststoreCertificateBO = new TruststoreCertificateBO();
        truststoreCertificateBO.setAlias(alias);
        try {
            truststoreCertificateBO.setCertificatePublicKey(certificate.getEncoded());
        } catch (CertificateEncodingException exc) {
            throw new BadRequestException(String.format("Can not encode certificate with alias[%s]!", alias), exc);
        }
        return truststoreCertificateBO;
    }

    @Override
    @PreAuthorize("hasAnyRole( 'ROLE_ADMIN')")
    public List<String> listTruststoreCertificateAliases(String containsAlias) throws TechnicalException {
        return truststoreService.getAllAliases(containsAlias);
    }


    @Override
    @PreAuthorize("hasAnyRole( 'ROLE_ADMIN')")
    public List<DNSRecordBO> deleteDNSRecord(String dnsRecordName, String dnsZone) throws TechnicalException {
        if (!configurationBusiness.isDNSEnabled()) {
            throw new BadConfigurationException(BAD_REQUEST_DNS_SERVER_NOT_ENABLED);
        }
        return manageDNSRecordBusiness.deleteDNSRecords(dnsRecordName, dnsZone);
    }

    @Override
    @PreAuthorize("hasAnyRole( 'ROLE_ADMIN')")
    public DNSRecordBO addDNSRecord(DNSRecordBO messagePart) throws TechnicalException {
        if (!configurationBusiness.isDNSEnabled()) {
            throw new BadConfigurationException(BAD_REQUEST_DNS_SERVER_NOT_ENABLED);
        }
        return manageDNSRecordBusiness.addDNSRecord(messagePart);
    }

    @Override
    @PreAuthorize("hasAnyRole( 'ROLE_ADMIN')")
    public ConfigurationBO setPropertyToDatabase(String key, String value, String description) throws TechnicalException {
        SMLPropertyEnum property = getPropertyDefinition(key);
        return configurationBusiness.setPropertyToDatabase(property, value, description);
    }

    @Override
    @PreAuthorize("hasAnyRole( 'ROLE_ADMIN')")
    public ConfigurationBO getPropertyFromDatabase(String key) throws TechnicalException {
        SMLPropertyEnum property = getPropertyDefinition(key);
        return configurationBusiness.getPropertyFromDatabase(property);
    }

    @Override
    @PreAuthorize("hasAnyRole( 'ROLE_ADMIN')")
    public ConfigurationBO deletePropertyFromDatabase(String key) throws TechnicalException {
        SMLPropertyEnum property = getPropertyDefinition(key);
        return configurationBusiness.deletePropertyFromDatabase(property);
    }

    private SMLPropertyEnum getPropertyDefinition(String key) throws TechnicalException {
        if (StringUtils.isBlank(key)) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_EMPTY_PROPERTY);
        }


        Optional<SMLPropertyEnum> propertyEnum = SMLPropertyEnum.getByProperty(key);
        if (propertyEnum.isEmpty()) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_INVALID_PROPERTY);
        }
        return propertyEnum.get();
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @CacheEvict(value = {"crlByUrl", "crlByCert", "allDomains"}, allEntries = true)
    public void clearCache() {
        //do nothing, the cleaning is done by annotations
    }

    protected String normalizeAlias(String alias) {
        return StringUtils.trimToNull(StringUtils.lowerCase(alias));
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public String manageServiceMetadataPublisher(String actionName, String recipientEmailAddress,
                                                 String serviceMetadataPublisherId,
                                                 String domainZone, String certificateOwnerId,
                                                 String logicalAddress, String physicalAddress) throws TechnicalException {

        AdminSMPManageActionEnum action = AdminSMPManageActionEnum.getByName(actionName)
                .orElseThrow(() ->
                        new BadRequestException("Unknown action: [" + actionName + "]. Action names are: [" +
                                EnumSet.allOf(AdminSMPManageActionEnum.class).stream().map(Enum::name).collect(Collectors.joining(", "))
                                + "]!"));
        String emlAddr = validateEmailAddress(recipientEmailAddress, false);
        int iCntTasks = smlReportTaskExecutor.getActiveCount();
        String rspMsg;
        if (smlReportTaskExecutor.getActiveCount() == 0) {
            // validate the input data
            ServiceMetadataPublisherBO metadataPublisherBO = adminManageServiceMetadataBusiness.validateSMPIdentifier(serviceMetadataPublisherId, certificateOwnerId, domainZone);
            adminManageServiceMetadataBusiness.validateTaskData(action, metadataPublisherBO, logicalAddress, physicalAddress);
            // create the task
            AdminSmpManagementTask task = (AdminSmpManagementTask) appContext.getBean("adminSmpManagementTask");
            task.setAction(action);
            task.setMetadataPublisherBO(metadataPublisherBO);
            task.setReceiverAddress(emlAddr);
            task.setUpdateData(logicalAddress, physicalAddress);
            // execute the task
            smlReportTaskExecutor.execute(task);

            rspMsg = "Start executing the smp management task [" + action + "]!";
        } else {
            rspMsg = "Only one batch task is allowed. Currently executing [" + iCntTasks + "]. Request is ignored!";
            loggingService.info(LOG_PREFIX + rspMsg);
        }
        loggingService.info(LOG_PREFIX + rspMsg);
        return rspMsg;
    }


    @Override
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public String manageParticipantIdentifiers(AdminParticipantIdentifierManageActionEnum action,
                                               String publisherId,
                                               List<ParticipantBO> participantBOs)
            throws TechnicalException {

        switch (action) {
            case CREATE:
                manageParticipantIdentifierBusiness.createParticipantList(publisherId, participantBOs);
                return "Participant identifier count: [" + participantBOs.size() + "] created!";
            case DELETE:
                manageParticipantIdentifierBusiness.deleteParticipantList(publisherId, participantBOs);
                return "Participant identifier count: [" + participantBOs.size() + "] deleted!";
            default:
                throw new BadRequestException(BAD_REQUEST_PARAMETER_INVALID_ENUM,
                        "action",
                        action.name(),
                        EnumSet.allOf(AdminSMPManageActionEnum.class).stream().map(Enum::name).collect(Collectors.joining(", "))
                );
        }
    }

    public String validateEmailAddress(String emailAddress, boolean mandatory) throws BadRequestException {

        String emlAddr = StringUtils.trim(emailAddress);
        if (StringUtils.isEmpty(emlAddr) && mandatory) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_EMPTY_EMAIL);
        }

        try {
            DataValidator.validateEmail(emlAddr);
        } catch (InvalidArgumentException ex) {
            throw new BadRequestException(BAD_REQUEST_PARAMETER_INVALID_EMAIL, emlAddr);
        }
        return emlAddr;
    }
}

