/*
 * (C) Copyright 2024 - European Commission | CEF eDelivery
 *
 * Licensed under the EUPL, Version 1.2 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * EUPL.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12, https://joinup.ec.europa.eu/sites/default/files/custom-page/attachment/eupl_v1.2_en.pdf
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.europa.ec.bdmsl.security;

import eu.europa.ec.bdmsl.config.FileProperty;
import eu.europa.ec.bdmsl.config.PropertiesConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletContext;
import java.util.Enumeration;
import java.util.Properties;

/**
 * A delegate class for customizing the initialization parameters of a servlet.
 * This class wraps an existing {@link ServletConfig} and overrides specific parameters
 * to ensure the service list page is hidden based on external configuration.
 *
 * @author Thomas Dussart
 * @since 2024
 */
public class ServletConfigDelegate implements ServletConfig {

    private static final Logger LOG = LoggerFactory.getLogger(ServletConfigDelegate.class);

    public static final String HIDE_SERVICE_LIST_PAGE = "hide-service-list-page";

    private final ServletConfig servletConfig;

    /**
     * Constructs a new {@code ServletConfigDelegate} with the specified {@link ServletConfig}.
     *
     * @param servletConfig the original servlet configuration
     */
    public ServletConfigDelegate(ServletConfig servletConfig) {
        this.servletConfig = servletConfig;
    }

    /**
     * Gets the name of the servlet.
     *
     * @return the servlet name
     */
    @Override
    public String getServletName() {
        return servletConfig.getServletName();
    }

    /**
     * Gets the {@link ServletContext} to which the servlet belongs.
     *
     * @return the servlet context
     */
    @Override
    public ServletContext getServletContext() {
        return servletConfig.getServletContext();
    }

    /**
     * Gets the value of the initialization parameter with the given name.
     * If the parameter name is {@code hide-service-list-page}, it determines the value based on external configuration.
     *
     * @param name the name of the initialization parameter
     * @return the value of the initialization parameter, or {@code null} if the parameter does not exist
     */
    @Override
    public String getInitParameter(String name) {
        if (HIDE_SERVICE_LIST_PAGE.equals(name)) {
            Properties fileProperties = PropertiesConfig.getFileProperties(null);
            if (!fileProperties.containsKey(FileProperty.DISPLAY_SERVICE_PAGE)) {
                return Boolean.TRUE.toString();
            } else {
                String displayServicePageProperty = fileProperties.getProperty(FileProperty.DISPLAY_SERVICE_PAGE);
                String inverseDisplayService = Boolean.toString(!Boolean.parseBoolean(displayServicePageProperty));
                LOG.info("Hide service list page: {}", inverseDisplayService);
                // Convenient as Boolean.parseBoolean returns false in case of a wrong value.
                // Then if the property is wrong, the service list page will be hidden.
                return inverseDisplayService;
            }
        }
        return servletConfig.getInitParameter(name);
    }

    /**
     * Gets the names of the servlet's initialization parameters as an {@link Enumeration}.
     *
     * @return an enumeration of the initialization parameter names
     */
    @Override
    public Enumeration<String> getInitParameterNames() {
        return servletConfig.getInitParameterNames();
    }
}
