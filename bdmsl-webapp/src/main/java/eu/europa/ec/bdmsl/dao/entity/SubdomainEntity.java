/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.dao.entity;

import eu.europa.ec.bdmsl.dao.QueryNames;
import eu.europa.ec.bdmsl.dao.utils.ColumnDescription;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.envers.Audited;

import jakarta.persistence.*;
import java.util.Objects;

/**
 * @author Tiago MIGUEL
 * @since 02/03/2017
 */
@Entity
@Audited
@Table(name = "bdmsl_subdomain", indexes = {
        @Index(name = "bdmsl_subdomain_name_idxe", columnList = "subdomain_name", unique = true)
})
@org.hibernate.annotations.Table(appliesTo = "bdmsl_subdomain", comment = "SML handle multiple business domains. This table contains domain specific data")
@NamedQueries({
        @NamedQuery(name = QueryNames.SUBDOMAIN_ENTITY_GET_ALL, query = "SELECT d FROM SubdomainEntity d"),
        @NamedQuery(name = QueryNames.SUBDOMAIN_ENTITY_GET_BY_ID, query = "SELECT d FROM SubdomainEntity d where d.subdomainId=:id"),
        @NamedQuery(name = QueryNames.SUBDOMAIN_ENTITY_GET_BY_NAME, query = "SELECT d FROM SubdomainEntity d where lower(d.subdomainName)=lower(:name)"),
        @NamedQuery(name = QueryNames.SUBDOMAIN_ENTITY_GET_ALL_SCHEMES_PER_DOMAIN, query = "SELECT distinct concat(part.scheme,'.',part.smp.subdomain.subdomainName) FROM ParticipantIdentifierEntity part"),
        @NamedQuery(name = QueryNames.SUBDOMAIN_ENTITY_GET_BY_CERTIFICATE_ID, query = "SELECT distinct smp.subdomain FROM SmpEntity smp where smp.certificate.certificateId=:certificateId"),

})
public class SubdomainEntity extends AbstractEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "bdmsl_subdomain_seq")
    // strategy native uses sequence in oracle and auto_increment on mysql
    // from hibernate 5.4 name is also the name of sequence, before it was hibernate
    @GenericGenerator(name = "bdmsl_subdomain_seq", strategy = "native")
    @Column(name = "subdomain_id")
    private long subdomainId;

    @Column(name = "subdomain_name", nullable = false, unique = true, length = CommonColumnsLengths.MAX_SML_SUBDOMAIN_LENGTH)
    @ColumnDescription(comment = "subdomain name - part of domain")
    private String subdomainName;


    @Column(name = "description", length = CommonColumnsLengths.MAX_MEDIUM_TEXT_LENGTH)
    @ColumnDescription(comment = "Domain (short) description.")
    private String description;

    @Column(name = "dns_zone", nullable = false, length = CommonColumnsLengths.MAX_TEXT_LENGTH_512)
    @ColumnDescription(comment = "Domain (dns zone) for subdomain.")
    private String dnsZone;

    @Column(name = "participant_id_regexp", nullable = false, length = CommonColumnsLengths.MAX_MEDIUM_TEXT_LENGTH)
    @ColumnDescription(comment = "Regular expression for the participant identifier validation.")
    private String participantIdRegexp;

    @Column(name = "dns_record_types", nullable = false, length = CommonColumnsLengths.MAX_TEXT_LENGTH_128)
    @ColumnDescription(comment = "Type of DNS Record when registering/updating participant, all means that both DNS record types are accepted as possible values: [cname, naptr, all].")
    private String dnsRecordTypes;

    @Column(name = "smp_url_schemas", nullable = false)
    @ColumnDescription(comment = "Protocol that MUST be used for LogicalAddress when registering new SMP, all means both protocols are accepted possible values: [ http, https, all].")
    private String smpUrlSchemas;

    @Column(name = "smp_ia_cert_regexp")
    @ColumnDescription(comment = "User with issuer-authorized SMP certificate is granted SMP_ROLE only if its certificate Subject DN matches configured regexp.")
    private String smpCertSubjectRegex;

    @Column(name = "domain_max_participant_count")
    @ColumnDescription(comment = "Maximum number of participant allowed to be registered on the domain")
    private Long maxParticipantCountForDomain;

    @Column(name = "smp_max_participant_count")
    @ColumnDescription(comment = "Maximum number of participant allowed to be registered on the SMP")
    private Long maxParticipantCountForSMP;

    @Column(name = "smp_ia_cert_policy_oids", length = CommonColumnsLengths.MAX_MEDIUM_TEXT_LENGTH)
    @ColumnDescription(comment = "User with issuer-authorized SMP certificate is granted SMP_ROLE only if one of the certificate policy extension matches the list. Value is a list of certificate policy OIDs separated by ','.")
    private String smpCertPolicyOIDs;

    @Override
    public Object getId() {
        return subdomainId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getSubdomainId() {
        return subdomainId;
    }

    public void setSubdomainId(long subdomainId) {
        this.subdomainId = subdomainId;
    }

    public String getSubdomainName() {
        return subdomainName;
    }

    public void setSubdomainName(String subdomainName) {
        this.subdomainName = subdomainName;
    }

    public String getParticipantIdRegexp() {
        return participantIdRegexp;
    }

    public void setParticipantIdRegexp(String participantIdRegexp) {
        this.participantIdRegexp = participantIdRegexp;
    }

    public String getDnsZone() {
        return dnsZone;
    }

    public void setDnsZone(String dnsZone) {
        this.dnsZone = dnsZone;
    }

    public String getDnsRecordTypes() {
        return dnsRecordTypes;
    }

    public void setDnsRecordTypes(String dnsRecordTypes) {
        this.dnsRecordTypes = dnsRecordTypes;
    }

    public String getSmpUrlSchemas() {
        return smpUrlSchemas;
    }

    public void setSmpUrlSchemas(String smpUrlSchemas) {
        this.smpUrlSchemas = smpUrlSchemas;
    }

    public String getSmpCertSubjectRegex() {
        return smpCertSubjectRegex;
    }

    public void setSmpCertSubjectRegex(String smpCertSubjectRegex) {
        this.smpCertSubjectRegex = smpCertSubjectRegex;
    }

    public Long getMaxParticipantCountForDomain() {
        return maxParticipantCountForDomain;
    }

    public void setMaxParticipantCountForDomain(Long maxParticipantCountForDomain) {
        this.maxParticipantCountForDomain = maxParticipantCountForDomain;
    }

    public String getSmpCertPolicyOIDs() {
        return smpCertPolicyOIDs;
    }

    public void setSmpCertPolicyOIDs(String smpCertPolicyOids) {
        this.smpCertPolicyOIDs = smpCertPolicyOids;
    }

    public Long getMaxParticipantCountForSMP() {
        return maxParticipantCountForSMP;
    }

    public void setMaxParticipantCountForSMP(Long maxParticipantCountForSMP) {
        this.maxParticipantCountForSMP = maxParticipantCountForSMP;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SubdomainEntity)) return false;
        SubdomainEntity that = (SubdomainEntity) o;
        return subdomainId == that.subdomainId &&
                Objects.equals(subdomainName, that.subdomainName) &&
                Objects.equals(description, that.description) &&
                Objects.equals(dnsZone, that.dnsZone) &&
                Objects.equals(participantIdRegexp, that.participantIdRegexp) &&
                Objects.equals(dnsRecordTypes, that.dnsRecordTypes) &&
                Objects.equals(smpUrlSchemas, that.smpUrlSchemas);
    }

    @Override
    public int hashCode() {
        return Objects.hash(subdomainName);
    }
}
