/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.impl;

import eu.europa.ec.bdmsl.business.ICertificateDomainBusiness;
import eu.europa.ec.bdmsl.business.IManageCertificateBusiness;
import eu.europa.ec.bdmsl.business.IManageServiceMetadataBusiness;
import eu.europa.ec.bdmsl.common.bo.CertificateBO;
import eu.europa.ec.bdmsl.common.bo.ChangeCertificateBO;
import eu.europa.ec.bdmsl.common.bo.PrepareChangeCertificateBO;
import eu.europa.ec.bdmsl.common.bo.ServiceMetadataPublisherBO;
import eu.europa.ec.bdmsl.common.exception.BadRequestException;

import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.common.service.AbstractServiceImpl;
import eu.europa.ec.bdmsl.common.util.LogEvents;
import eu.europa.ec.bdmsl.service.IManageCertificateService;
import eu.europa.ec.bdmsl.service.IX509CertificateService;
import eu.europa.ec.bdmsl.util.CertificateUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.security.cert.X509Certificate;


/**
 * @author Adrien FERIAL
 * @since 12/06/2015
 */
@Service(value = "manageCertificateService")
@Transactional(readOnly = true, propagation = Propagation.REQUIRES_NEW)
public class ManageCertificateServiceImpl extends AbstractServiceImpl implements IManageCertificateService {

    @Autowired
    private IManageCertificateBusiness manageCertificateBusiness;

    @Autowired
    private ICertificateDomainBusiness certificateDomainBusiness;

    @Autowired
    private IX509CertificateService x509CertificateService;

    @Autowired
    private IManageServiceMetadataBusiness manageServiceMetadataBusiness;

    @Override
    @PreAuthorize("hasAnyRole('ROLE_SMP')")
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void prepareChangeCertificate(PrepareChangeCertificateBO prepareChangeCertificateBO) throws  TechnicalException {
        // validate the data contained in the request
        manageCertificateBusiness.validateChangeCertificate(prepareChangeCertificateBO);

        // validate currentCertificate dates in database
        manageCertificateBusiness.validateCurrentCertificate(prepareChangeCertificateBO.getCurrentCertificate());

        // validate the certificate
        X509Certificate newCert = manageCertificateBusiness.getX509Certificate(prepareChangeCertificateBO.getPublicKey());

        if (newCert == null) {
            throw new BadRequestException("The certificate is not valid!");
        }
        // certificate is trusted because is added by trusted user
        x509CertificateService.validateClientX509Certificate(new X509Certificate[]{newCert}, true);

        // validate if certificate contains minimal RNs in Subject (at the time is CN, O and C)
        CertificateUtils.validateSubjectDNRequiredValues(newCert.getSubjectX500Principal().getName());

        // If the migrationDate element is empty, then the "Valid From" date is extracted from the certificate and is used as the migrationDate.
        if (prepareChangeCertificateBO.getMigrationDate() == null) {
            prepareChangeCertificateBO.setMigrationDate(DateUtils.toCalendar(newCert.getNotBefore()));
        }

        // Check if the migration date is valid for the given certificate
        manageCertificateBusiness.checkMigrationDate(newCert, prepareChangeCertificateBO);

        //Check if new ROOT certificate belongs to the same subdomain
        if (!certificateDomainBusiness.isCertificateDomainNonRootCA(prepareChangeCertificateBO.getCurrentCertificate())) {
            manageCertificateBusiness.domainValidationForNewCertificate(newCert);
        }

        // extract information from the certificate
        CertificateBO newCertificateBO = manageCertificateBusiness.extractCertificate(prepareChangeCertificateBO.getPublicKey());

        // create or update the new certificate in the DB
        Long newCertId;
        CertificateBO existingNewCertificate = manageCertificateBusiness.findCertificate(newCertificateBO.getCertificateId());
        if (existingNewCertificate == null) {
            newCertId = manageCertificateBusiness.createNewCertificate(newCertificateBO);
        } else {
            newCertId = existingNewCertificate.getId();
            newCertificateBO.setId(existingNewCertificate.getId());
            manageCertificateBusiness.updateCertificate(newCertificateBO);
        }
        if (StringUtils.equals(newCertificateBO.getCertificateId(), prepareChangeCertificateBO.getCurrentCertificate().getCertificateId())) {
            loggingService.error("Can not update certificate with itself: User certificate: ["
                    + prepareChangeCertificateBO.getCurrentCertificate().getCertificateId() +
                    "], X509 certificate: [" + newCertificateBO.getCertificateId() + "]", null);

            throw new BadRequestException("Can not update certificate with itself: [" + prepareChangeCertificateBO.getCurrentCertificate().getCertificateId() + "]!");
        }

        // update the current certificate
        prepareChangeCertificateBO.getCurrentCertificate().setNewCertificateId(newCertId);
        prepareChangeCertificateBO.getCurrentCertificate().setMigrationDate(prepareChangeCertificateBO.getMigrationDate());

        manageCertificateBusiness.updateCertificate(prepareChangeCertificateBO.getCurrentCertificate());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void changeCertificates() throws  TechnicalException {
        try {
            int count = manageCertificateBusiness.changeCertificates();
            loggingService.businessLog(LogEvents.BUS_CERTIFICATE_CHANGE_JOB_SUCCESS, Integer.toString(count));
        } catch (Exception exc) {
            loggingService.businessLog(LogEvents.BUS_CERTIFICATE_CHANGE_JOB_FAILED, exc);
            throw exc;
        }
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void changeCertificate(ChangeCertificateBO changeCertificateBO) throws  TechnicalException {
        loggingService.info("Changing certificate for SMP:" + changeCertificateBO);

        //Checks if the SMP is valid
        manageServiceMetadataBusiness.validateSMPId(changeCertificateBO.getServiceMetadataPublisherID());

        //Checks if SMP exist
        ServiceMetadataPublisherBO existingSmpBO = manageServiceMetadataBusiness.verifySMPExist(changeCertificateBO.getServiceMetadataPublisherID());

        //Validate the certificate
        manageCertificateBusiness.validateChangeCertificate(changeCertificateBO);

        //Get the Certificate
        X509Certificate newCertificate = manageCertificateBusiness.getX509Certificate(changeCertificateBO.getPublicKey());

        try {
            // certificate is trusted because it is changed by administrator ROLE_ADMIN who we trust
            x509CertificateService.validateClientX509Certificate(new X509Certificate[]{newCertificate}, true);
        } catch (Exception exc) {
            loggingService.error(exc.getMessage(), exc);
            String errorMessage = exc.getMessage() + " %s .";
            throw new BadRequestException(String.format(errorMessage, newCertificate.getSubjectX500Principal().toString()), exc);
        }

        //Get Certificate Id
        CertificateBO newCertificateBO = manageCertificateBusiness.extractCertificate(changeCertificateBO.getPublicKey());


        CertificateBO certChecked = manageCertificateBusiness.findCertificate(newCertificateBO.getCertificateId());
        if (certChecked != null) {
            loggingService.info("Changing certificate for SMP:" + changeCertificateBO.getServiceMetadataPublisherID() + ": Certificate '"
                    + newCertificateBO.getCertificateId() + "' already exists in DB");
            newCertificateBO.setId(certChecked.getId());
        } else {
            Long certId = manageCertificateBusiness.createNewCertificate(newCertificateBO);
            newCertificateBO.setId(certId);
        }

        CertificateBO oldCertificateBO = manageCertificateBusiness.findCertificate(existingSmpBO.getCertificateId());
        if (StringUtils.equals(newCertificateBO.getCertificateId(), oldCertificateBO.getCertificateId())) {
            loggingService.error("Can not update certificate with itself: Old certificate: ["
                    + oldCertificateBO.getCertificateId() +
                    "], X509 certificate: [" + newCertificateBO.getCertificateId() + "]", null);

            throw new BadRequestException("Can not update certificate with itself: [" + oldCertificateBO.getCertificateId() + "]!");
        }
        // method updates SMP entries wildcard authorization f
        manageCertificateBusiness.changeCertificate(newCertificateBO, oldCertificateBO);
        // method updates domain certificate
        certificateDomainBusiness.changeCertificateDomain(newCertificate, oldCertificateBO, newCertificateBO, true);
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_SMP','ROLE_ADMIN')")
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public CertificateBO findCertificate(String certificateId) throws  TechnicalException {
        return manageCertificateBusiness.findCertificate(certificateId);
    }
}

