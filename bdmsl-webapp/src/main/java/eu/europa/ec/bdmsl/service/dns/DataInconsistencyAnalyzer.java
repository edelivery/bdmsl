/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.dns;

import eu.europa.ec.bdmsl.common.exception.InconsistencyReportException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xbill.DNS.IRDnsRangeData;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Stream;

/**
 * @author Flavio SANTOS
 * @since 14/06/2017
 */
public class DataInconsistencyAnalyzer {

    private static final Logger LOG = LogManager.getLogger(DataInconsistencyAnalyzer.class);


    private int dbTotalOfSMPs;
    private int dbTotalInactiveSMPs;
    private int dbTotalOfParticipants;
    private int dbTotalOfParticipantsCNAME;
    private int dbTotalOfParticipantsNAPTR;

    private int dbTotalOfDisabledParticipantsCNAME;
    private int dbTotalOfDisabledParticipantsNAPTR;

    private int dnsTotalOfSMPs;
    private int dnsTotalOfParticipantsCNAME;
    private int dnsTotalOfParticipantsNAPTR;
    private List<DataInconsistencyEntry> dataInconsistencyEntries;

    public DataInconsistencyAnalyzer() {
        dataInconsistencyEntries = new ArrayList<>();
    }

    public List<DataInconsistencyEntry> getSmpsDataInconsistencies() {
        return getParticipantsDataInconsistencies(DataInconsistencyEntry.DataType.SMP, DataInconsistencyEntry.IssueType.CNAME);
    }

    public List<DataInconsistencyEntry> getParticipantCNAMEDataInconsistencies() {
        return getParticipantsDataInconsistencies(DataInconsistencyEntry.DataType.PARTICIPANT, DataInconsistencyEntry.IssueType.CNAME);
    }

    public List<DataInconsistencyEntry> geDataInconsistencyErrors() {
        List<DataInconsistencyEntry> filteredEntries = new ArrayList<>();

        for (DataInconsistencyEntry entry : dataInconsistencyEntries) {
            if (entry.getMissingType() == DataInconsistencyEntry.IssueType.ERROR) {
                filteredEntries.add(entry);
            }
        }
        return filteredEntries;
    }

    public List<DataInconsistencyEntry> getParticipantNAPTRDataInconsistencies() {
        return getParticipantsDataInconsistencies(DataInconsistencyEntry.DataType.PARTICIPANT, DataInconsistencyEntry.IssueType.NAPTR);
    }

    private List<DataInconsistencyEntry> getParticipantsDataInconsistencies(DataInconsistencyEntry.DataType dataType, DataInconsistencyEntry.IssueType missingType) {
        List<DataInconsistencyEntry> filteredEntries = new ArrayList<>();

        for (DataInconsistencyEntry entry : dataInconsistencyEntries) {
            if (entry.getDataType() == dataType) {
                if (entry.getMissingType() == missingType) {
                    filteredEntries.add(entry);
                }
            }
        }
        return filteredEntries;
    }


    public void compareCNameRecords(IRDnsRangeData data) throws InconsistencyReportException {
        compare(DataInconsistencyEntry.IssueType.CNAME, DataInconsistencyEntry.DataType.PARTICIPANT, data);
    }

    public void compareNaptrRecords(IRDnsRangeData data) throws InconsistencyReportException {
        compare(DataInconsistencyEntry.IssueType.NAPTR, DataInconsistencyEntry.DataType.PARTICIPANT, data);
    }

    public void compareSMPRecords(IRDnsRangeData data) throws InconsistencyReportException {
        // cname and A are equally represented in compare files...
        compare(DataInconsistencyEntry.IssueType.CNAME, DataInconsistencyEntry.DataType.SMP, data);
    }

    public void compare(DataInconsistencyEntry.IssueType searchIssueType, DataInconsistencyEntry.DataType dataType, IRDnsRangeData data) throws InconsistencyReportException {
        for (int i = 0; i < data.getNumberOfRanges(); i++) {
            compareFiles(data.getDNSFile(i), data.getDBFile(i), dataType, searchIssueType);
        }
    }

    private void compareFiles(File fileDNS, File fileDB, DataInconsistencyEntry.DataType dataType, DataInconsistencyEntry.IssueType searchIssueType) throws InconsistencyReportException {
        Set<String> dnsEntries = new TreeSet<>();

        // first read to memory
        try (Stream<String> lines = Files.lines(fileDNS.toPath())) {
            lines.forEach(dnsEntries::add);
        } catch (IOException e) {
            DataInconsistencyEntry dataInconsistencyData = new DataInconsistencyEntry();
            dataInconsistencyData.setMissingType(DataInconsistencyEntry.IssueType.ERROR);
            dataInconsistencyData.setErrorMessage("Error occurred while analysing data for inconsistencies: " + e.getMessage());
            addInconsistency(dataInconsistencyData);
            LOG.error("Error occurred while reading file: " + fileDNS, e);
            return;
        }

        try (Stream<String> lines = Files.lines(fileDB.toPath())) {
            lines.forEach(dbLine -> {
                // for database participant  - first value when delimited by | is
                // participant identifier which is not in fileDNS
                // but for SMP is only smp domain...
                int iVal = dbLine.indexOf('#');

                String dnsValue = iVal > 0 ? dbLine.substring(0, iVal) : dbLine;
                String dbIdentifier = iVal > 0 ? dbLine.substring(iVal + 1) : "";

                if (!dnsEntries.remove(dnsValue)) {
                    addInconsistency(dnsValue, dbIdentifier, DataInconsistencyEntry.MissingSourceType.DNS, dataType, searchIssueType);
                }
            });
        } catch (IOException e) {
            DataInconsistencyEntry dataInconsistencyData = new DataInconsistencyEntry();
            dataInconsistencyData.setMissingType(DataInconsistencyEntry.IssueType.ERROR);
            dataInconsistencyData.setErrorMessage("Error occurred while analysing data for inconsistencies: " + e.getMessage());
            addInconsistency(dataInconsistencyData);
            LOG.error("Error occurred while reading file: " + fileDB, e);
            return;
        }
        // add all missing database values
        for (String dnsValue : dnsEntries) {
            addInconsistency(dnsValue, null, DataInconsistencyEntry.MissingSourceType.DATABASE, dataType, searchIssueType);
        }
    }


    private void addInconsistency(String dnsValue, String identifier, DataInconsistencyEntry.MissingSourceType source,
                                  DataInconsistencyEntry.DataType dataType, DataInconsistencyEntry.IssueType searchIssueType) throws InconsistencyReportException {
        int iSpl = dnsValue.indexOf('|');
        String domainName = iSpl > 0 ? dnsValue.substring(0, iSpl) : dnsValue;
        String target = iSpl > 0 ? dnsValue.substring(iSpl + 1) : "";

        DataInconsistencyEntry dataInconsistencyData = new DataInconsistencyEntry();
        dataInconsistencyData.setId(identifier);
        dataInconsistencyData.setMissingSourceType(source);
        dataInconsistencyData.setDataType(dataType);
        dataInconsistencyData.setDomainName(domainName);
        dataInconsistencyData.setTarget(target);

        dataInconsistencyData.setMissingType(searchIssueType);
        addInconsistency(dataInconsistencyData);
    }

    public void addInconsistency(DataInconsistencyEntry dataInconsistencyData) throws InconsistencyReportException {
        dataInconsistencyEntries.add(dataInconsistencyData);
        if (dataInconsistencyEntries.size() > 50000) {
            throw new InconsistencyReportException("More than 50 000 inconsistencies. Stop validation!");
        }
    }

    public void addInconsistencyNoLimitation(DataInconsistencyEntry dataInconsistencyData) throws InconsistencyReportException {
        dataInconsistencyEntries.add(dataInconsistencyData);
    }

    public void clear() {
        dataInconsistencyEntries.clear();
        dbTotalOfSMPs = 0;
        dbTotalInactiveSMPs = 0;
        dbTotalOfParticipants = 0;
        dbTotalOfParticipantsCNAME = 0;
        dbTotalOfParticipantsNAPTR = 0;
        dbTotalOfDisabledParticipantsCNAME = 0;
        dbTotalOfDisabledParticipantsNAPTR = 0;

        dnsTotalOfSMPs = 0;
        dnsTotalOfParticipantsCNAME = 0;
        dnsTotalOfParticipantsNAPTR = 0;
    }

    public int getDnsTotalOfSMPs() {
        return dnsTotalOfSMPs;
    }

    public void setDnsTotalOfSMPs(int dnsTotalOfSMPs) {
        this.dnsTotalOfSMPs = dnsTotalOfSMPs;
    }

    public int getDbTotalOfSMPs() {
        return dbTotalOfSMPs;
    }

    public void setDbTotalOfSMPs(int dbTotalOfSMPs) {
        this.dbTotalOfSMPs = dbTotalOfSMPs;
    }

    public int getDbTotalInactiveSMPs() {
        return dbTotalInactiveSMPs;
    }

    public void setDbTotalInactiveSMPs(int dbTotalInactiveSMPs) {
        this.dbTotalInactiveSMPs = dbTotalInactiveSMPs;
    }

    public int getDbTotalOfParticipants() {
        return dbTotalOfParticipants;
    }

    public void setDbTotalOfParticipants(int dbTotalOfParticipants) {
        this.dbTotalOfParticipants = dbTotalOfParticipants;
    }

    public int getDnsTotalOfParticipantsCNAME() {
        return dnsTotalOfParticipantsCNAME;
    }

    public void setDnsTotalOfParticipantsCNAME(int dnsTotalOfParticipantsCNAME) {
        this.dnsTotalOfParticipantsCNAME = dnsTotalOfParticipantsCNAME;
    }

    public int getDbTotalOfParticipantsCNAME() {
        return dbTotalOfParticipantsCNAME;
    }

    public void setDbTotalOfParticipantsCNAME(int dbTotalOfParticipantsCNAME) {
        this.dbTotalOfParticipantsCNAME = dbTotalOfParticipantsCNAME;
    }

    public int getDbTotalOfParticipantsNAPTR() {
        return dbTotalOfParticipantsNAPTR;
    }

    public void setDbTotalOfParticipantsNAPTR(int dbTotalOfParticipantsNAPTR) {
        this.dbTotalOfParticipantsNAPTR = dbTotalOfParticipantsNAPTR;
    }

    public int getDnsTotalOfParticipantsNAPTR() {
        return dnsTotalOfParticipantsNAPTR;
    }

    public void setDnsTotalOfParticipantsNAPTR(int dnsTotalOfParticipantsNAPTR) {
        this.dnsTotalOfParticipantsNAPTR = dnsTotalOfParticipantsNAPTR;
    }

    public int getDbTotalOfDisabledParticipantsCNAME() {
        return dbTotalOfDisabledParticipantsCNAME;
    }

    public void setDbTotalOfDisabledParticipantsCNAME(int dbTotalOfDisabledParticipantsCNAME) {
        this.dbTotalOfDisabledParticipantsCNAME = dbTotalOfDisabledParticipantsCNAME;
    }

    public int getDbTotalOfDisabledParticipantsNAPTR() {
        return dbTotalOfDisabledParticipantsNAPTR;
    }

    public void setDbTotalOfDisabledParticipantsNAPTR(int dbTotalOfDisabledParticipantsNAPTR) {
        this.dbTotalOfDisabledParticipantsNAPTR = dbTotalOfDisabledParticipantsNAPTR;
    }

    public List<DataInconsistencyEntry> getDataInconsistencyEntries() {
        return dataInconsistencyEntries;
    }

    public static class DataInconsistencyEntry {
        private String id;
        private String domainName;
        private String target;
        private IssueType missingType;
        private MissingSourceType missingSourceType;
        private DataType dataType;
        private String errorMessage;

        public DataInconsistencyEntry() {
            this(null, null, null, null, null);
        }

        public DataInconsistencyEntry(String id, String domainName, IssueType missingType, MissingSourceType missingSourceType, DataType dataType) {
            this.id = id;
            this.domainName = domainName;
            this.missingType = missingType;
            this.missingSourceType = missingSourceType;
            this.dataType = dataType;
        }

        public String getTarget() {
            return target;
        }

        public void setTarget(String target) {
            this.target = target;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDomainName() {
            return domainName;
        }

        public void setDomainName(String domainName) {
            this.domainName = domainName;
        }

        public IssueType getMissingType() {
            return missingType;
        }

        public void setMissingType(IssueType missingType) {
            this.missingType = missingType;
        }

        public DataType getDataType() {
            return dataType;
        }

        public void setDataType(DataType dataType) {
            this.dataType = dataType;
        }

        public MissingSourceType getMissingSourceType() {
            return missingSourceType;
        }

        public void setMissingSourceType(MissingSourceType missingSourceType) {
            this.missingSourceType = missingSourceType;
        }

        public String getErrorMessage() {
            return errorMessage;
        }

        public void setErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
        }

        public enum IssueType {
            NAPTR, CNAME, ERROR
        }

        public enum MissingSourceType {
            DNS, DATABASE
        }

        public enum DataType {
            SMP, PARTICIPANT
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            DataInconsistencyEntry that = (DataInconsistencyEntry) o;
            return Objects.equals(id, that.id) &&
                    Objects.equals(domainName, that.domainName) &&
                    missingType == that.missingType &&
                    missingSourceType == that.missingSourceType &&
                    dataType == that.dataType &&
                    Objects.equals(errorMessage, that.errorMessage);
        }

        @Override
        public int hashCode() {
            return Objects.hash(id, domainName, target, missingType, missingSourceType, dataType, errorMessage);
        }
    }
}
