/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.service.impl;

import eu.europa.ec.bdmsl.business.IX509CertificateBusiness;
import eu.europa.ec.bdmsl.common.bo.CertificateDomainBO;

import eu.europa.ec.bdmsl.common.exception.TechnicalException;
import eu.europa.ec.bdmsl.common.service.AbstractServiceImpl;
import eu.europa.ec.bdmsl.service.IX509CertificateService;
import eu.europa.ec.edelivery.security.cert.IRevocationValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;

/**
 * @author Adrien FERIAL
 * @since 18/06/2015
 */
@Service
public class X509CertificateServiceImpl extends AbstractServiceImpl implements IX509CertificateService {

    @Autowired
    private IRevocationValidator crlVerifierService;

    @Autowired
    private IX509CertificateBusiness x509CertificateBusiness;

    @Override
    public void validateClientX509Certificate(final X509Certificate[] certificates) throws TechnicalException {
        x509CertificateBusiness.validateClientX509Certificate(certificates, false);
    }

    @Override
    public void validateClientX509Certificate(final X509Certificate[] certificates, boolean isCertificateAlreadyTrusted) throws TechnicalException {
        x509CertificateBusiness.validateClientX509Certificate(certificates, isCertificateAlreadyTrusted);
    }

    @Override
    public String getTrustedRootCertificateDN(X509Certificate[] certificates) throws TechnicalException {
        return x509CertificateBusiness.getTrustedRootCertificateDN(certificates);
    }

    @Override
    public X509Certificate getCertificate(final X509Certificate[] requestCerts) throws TechnicalException {
        return x509CertificateBusiness.getCertificate(requestCerts);
    }

    @Override
    public IRevocationValidator getCrlVerifierService() {
        return crlVerifierService;
    }

    @Override
    public CertificateDomainBO validateRootCA(final X509Certificate cert) throws TechnicalException,  CertificateNotYetValidException, CertificateExpiredException {
        return x509CertificateBusiness.validateRootCA(cert);
    }

    @Override
    public CertificateDomainBO validateNonRootCA(final X509Certificate cert) throws TechnicalException,  CertificateNotYetValidException, CertificateExpiredException {
        return x509CertificateBusiness.validateNonRootCA(cert);
    }
}
