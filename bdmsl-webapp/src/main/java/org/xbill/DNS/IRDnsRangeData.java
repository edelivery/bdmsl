/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package org.xbill.DNS;


import eu.europa.ec.bdmsl.common.exception.InconsistencyReportException;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Stream;

/**
 * @author Joze Rihtasic
 * @since 4.0.2
 */

public class IRDnsRangeData {

    private static final Logger LOG = LogManager.getLogger(IRDnsRangeData.class);

    long iMinVal;
    long iMaxVal;
    int numberOfRanges;
    long iIntervalSize;
    String filePref;

    int addedRowsDNS = 0;
    int addedRowsDB = 0;
    int disabledDBEntity = 0;

    FileRangeData[] listRangesFilesDNS;
    FileRangeData[] listRangesFilesDB;

    public IRDnsRangeData(int numberOfRanges) {
        this(Integer.MIN_VALUE, Integer.MAX_VALUE, numberOfRanges, "inc-rep");
    }

    public IRDnsRangeData(long iMinVal, long iMaxVal, int numberOfRanges, String filePref) {
        this.iMinVal = iMinVal;
        this.iMaxVal = iMaxVal;
        this.numberOfRanges = numberOfRanges;
        this.filePref = filePref;


        if ((iMaxVal - iMinVal) < (long) numberOfRanges) {
            throw new IllegalArgumentException("More ranges than is numbers between min and max value. (Interval: '" + (iMaxVal - iMinVal) + "', number of ranges:" + numberOfRanges + ")");
        }

        listRangesFilesDNS = new FileRangeData[numberOfRanges];
        listRangesFilesDB = new FileRangeData[numberOfRanges];
        iIntervalSize = (iMaxVal - iMinVal) / (numberOfRanges);

        for (int idx = 0; idx < numberOfRanges; idx++) {
            long iStart = iIntervalSize * (idx - 1) + iMinVal;
            listRangesFilesDNS[idx] = new FileRangeData(iStart, iStart + iIntervalSize - 1, filePref + String.format("_%03d_DNS_", idx));
            listRangesFilesDB[idx] = new FileRangeData(iStart, iStart + iIntervalSize - 1, filePref + String.format("_%03d_DB_", idx));
        }
    }

    public void incrementDisabledDBEntityCount() {
        disabledDBEntity++;
    }
    public int getDisabledDBEntityCount() {
        return disabledDBEntity;
    }

    public int getNumberOfRanges() {
        return numberOfRanges;
    }

    public File getDNSFile(int i) {
        return listRangesFilesDNS[i].fileRecords;
    }

    public File getDBFile(int i) {
        return listRangesFilesDB[i].fileRecords;
    }

    public long getMinValue() {
        return iMinVal;
    }

    public long getMaxValue() {
        return iMaxVal;
    }

    public long getIntervalSize() {
        return iIntervalSize;
    }

    public void flush() {
        try {
            for (int idx = 0; idx < numberOfRanges; idx++) {
                listRangesFilesDNS[idx].buffWriter.flush();
                listRangesFilesDB[idx].buffWriter.flush();
            }
        } catch (IOException ex) {
            throw new InconsistencyReportException("Error occurred while flushing file buffer", ex);
        }
    }

    public void close() throws IOException {
        if (listRangesFilesDNS != null) {
            for (int idx = 0; idx < numberOfRanges; idx++) {
                listRangesFilesDNS[idx].buffWriter.flush();
                listRangesFilesDNS[idx].fileWriter.close();
            }
        }
        if (listRangesFilesDB != null) {
            for (int idx = 0; idx < numberOfRanges; idx++) {
                listRangesFilesDB[idx].buffWriter.flush();
                listRangesFilesDB[idx].fileWriter.close();
            }
        }
    }

    public void clear() {
        deleteFiles();
        addedRowsDNS = 0;
        addedRowsDB = 0;
        iIntervalSize = 0;
        listRangesFilesDNS = null;
        listRangesFilesDB = null;
    }

    public void deleteFiles() {
        // make sure files are closed
        try {
            close();
        } catch (IOException e) {
            LOG.error("Error occurred whole closing files: " + ExceptionUtils.getRootCauseMessage(e));
        }
        for (int idx = 0; idx < numberOfRanges; idx++) {
            File dnsFile = listRangesFilesDNS[idx].fileRecords;
            File dbFile = listRangesFilesDB[idx].fileRecords;
            if (dnsFile.exists() && dnsFile.delete()) {
                LOG.debug("File " + dnsFile.getAbsolutePath() + " was deleted!");
            }
            if (dbFile.exists() && dbFile.delete()) {
                LOG.debug("File " + dbFile.getAbsolutePath() + " was deleted!");
            }
        }
    }

    public void addDNSEntry(String name) {
        addString(name, listRangesFilesDNS);
        addedRowsDNS++;
    }

    public void addDBEntry(String name, String identifier) {
        addString(name, identifier, listRangesFilesDB);
        addedRowsDB++;
    }

    public void addParticipantDBEntry(String name, String participantId) {
        addString(name, participantId, listRangesFilesDB);
        addedRowsDB++;
    }

    protected void addString(String name, FileRangeData[] data) {
        addString(name, null, data);
    }

    protected void addString(String name, String identifier, FileRangeData[] data) {
        int hash = name.hashCode();
        int idx = getRangeForInt(hash);
        try {
            data[idx].write(name, identifier);
        } catch (IOException e) {
            LOG.error("Error occurred when adding to indexed range:" + idx + " max size:" + data.length, e);
        }
    }

    public int getRangeForInt(int ival) {
        int idx = (int) ((ival - iMinVal) / iIntervalSize);
        return idx < numberOfRanges ? idx : numberOfRanges - 1;
    }

    public int getAddedRowsDNS() {
        return addedRowsDNS;
    }

    public int getAddedRowsDB() {
        return addedRowsDB;
    }

    public void compare(Set<String> missingInDNS, Set<String> missingInDB) {
        for (int i = 0; i < numberOfRanges; i++) {
            compareFiles(listRangesFilesDNS[i].fileRecords, listRangesFilesDB[i].fileRecords, missingInDNS, missingInDB);
        }
    }

    protected void compareFiles(File fileDNS, File fileDB, Set<String> missingInDNS, Set<String> missingInDB) {
        Set<String> dnsEntries = new TreeSet<>();

        // first read to memory
        try (Stream<String> lines = Files.lines(fileDNS.toPath())) {
            lines.forEach(dnsEntries::add);
        } catch (IOException e) {
            LOG.error("Error occurred while reading files:  " + fileDNS.getAbsolutePath()
                    + " and: " + fileDB.getAbsolutePath(), e);
        }

        try (Stream<String> lines = Files.lines(fileDB.toPath())) {
            lines.forEach(dbLine -> {
                // for database participant  - first value when delimited by | is
                // participant identifier which is not in fileDNS
                // but for SMP is only smp domain...
                int iVal = dbLine.lastIndexOf('#');
                String dnsVal = iVal > 0 ? dbLine.substring(0, iVal) : dbLine;
                if (!dnsEntries.remove(dnsVal)) {
                    missingInDNS.add(dbLine);
                }
            });
        } catch (IOException e) {
            LOG.error("Error occurred while reading files:  " + fileDB.getAbsolutePath()
                    + " and: " + fileDB.getAbsolutePath(), e);
        }
        // add all missing database values
        missingInDB.addAll(dnsEntries);
    }


    public static class FileRangeData {
        long iMin;
        long iMax;
        File fileRecords;
        FileWriter fileWriter;
        BufferedWriter buffWriter;


        public FileRangeData(long iMin, long iMax, String filePrefix) {
            this.iMin = iMin;
            this.iMax = iMax;

            try {
                fileRecords = File.createTempFile(filePrefix, ".dat");
                fileWriter = new FileWriter(fileRecords);
                buffWriter = new BufferedWriter(fileWriter);

            } catch (IOException e) {
                LOG.error("Error occurred while creating file:  " + fileRecords.getAbsolutePath(), e);
            }
        }

        public void write(String value, String value1) throws IOException {
            fileWriter.write(value);
            if (value1 != null) {
                fileWriter.write('#');
                fileWriter.write(value1);
            }
            fileWriter.write(System.lineSeparator());
        }
    }
}
