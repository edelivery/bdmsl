/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package org.xbill.DNS;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.security.PrivateKey;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Adrien FERIAL
 * @since 05/06/2015
 */
public class DomiSmlSIG0Service {

    static final Logger s_aLogger = LogManager.getLogger(DomiSmlSIG0Service.class);

    private static final int MAX_RETRY = 5;

    public static void signMessage(Message message, KEYRecord key, PrivateKey privkey,
                                   SIGRecord previous, int validityMinutesBack) throws Exception {
        signMessage(message, key, privkey, previous, validityMinutesBack, 0, null);
    }

    /**
     * Sign a message with SIG0. This method is a copy of the method in the DNSSEC class, but with the addition of the validityMinutesBack parameter.
     *
     * @param message             The message to sign
     * @param key                 The key to use for signing
     * @param privkey             The private key to use for signing
     * @param previous            The SIG record returned by the DNS server
     * @param validityMinutesBack The number of minutes back in time that the signature is valid. the parameters enable to set the validity of the signature to a time in the past.
     *                            This is needed because the DNS server may have a clock that is not in  exact sync with the clock of the client.
     * @param retry               The number of times this method has been called recursively
     * @param lastException       The last exception that was thrown
     * @throws Exception
     */
    private static void signMessage(Message message, KEYRecord key, PrivateKey privkey,
                                    SIGRecord previous, int validityMinutesBack, int retry, Exception lastException) throws Exception {
        if (retry < MAX_RETRY) {
            try {
                int validity = Options.intValue("sig0validity");

                if (validity < 0)
                    validity = 300; // 5 minutes by default

                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.MINUTE, -validityMinutesBack);
                Date minutesBack = cal.getTime();

                long minutesBackBackLong = minutesBack.getTime();

                Date timeSigned = new Date(minutesBackBackLong);
                Date timeExpires = new Date(minutesBackBackLong + validity * 1000);

                SIGRecord sigRecord = DNSSEC.signMessage(message, previous, key, privkey,
                        timeSigned.toInstant(), timeExpires.toInstant());
                message.addRecord(sigRecord, Section.ADDITIONAL);

            } catch (final Exception exc) {
                s_aLogger.warn("There was an error when trying to sign the message for record ["+key+"], trying again for the " + (retry + 1) + " times. Exception was: " + exc.getMessage());

                signMessage(message, key, privkey, previous, validityMinutesBack, retry + 1, exc);
            }
        } else {
            throw lastException;
        }
    }
}
