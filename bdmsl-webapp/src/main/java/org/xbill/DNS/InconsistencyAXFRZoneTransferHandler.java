/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package org.xbill.DNS;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

/**
 * @author Joze Rihtasic
 * @since 4.0.2
 * <p>
 * Class retrieve all dns records and sorts them to participant NAPTR and CNAME records and SMP records.
 * Records are stored in temp files where records are split to sub-files according to hash value of the string.
 * This is done for large zones with million of records...
 */
public class InconsistencyAXFRZoneTransferHandler implements ZoneTransferIn.ZoneTransferHandler {

    public static final String INCONSISTENCY_AXFRZONE_TRANSFER_HANDLER_MUST_BE_USED_ONLY_FOR_AXFR = "InconsistencyAXFRZoneTransferHandler must be used only for AXFR";

    private static Logger LOG = LogManager.getLogger(InconsistencyAXFRZoneTransferHandler.class);
    String zoneName;
    String smpSubDomain;
    int readIndex = 0;
    int allRecordCount = 0;
    IRDnsRangeData smpData;
    IRDnsRangeData cnameData;
    IRDnsRangeData naptrData;


    public InconsistencyAXFRZoneTransferHandler(String zoneName, String smpSubDomain, IRDnsRangeData smpData, IRDnsRangeData cnameData, IRDnsRangeData naptrData) throws IOException {
        this.smpData = smpData;
        this.cnameData = cnameData;
        this.naptrData = naptrData;
        this.zoneName = zoneName;
        this.smpSubDomain = smpSubDomain;
    }

    public void startAXFR() {
        LOG.info("Start transfer DNS zone:" + zoneName);
    }

    public void startIXFR() {
        throw new IllegalStateException(INCONSISTENCY_AXFRZONE_TRANSFER_HANDLER_MUST_BE_USED_ONLY_FOR_AXFR);
    }

    public void startIXFRDeletes(Record soa) {
        throw new IllegalStateException(INCONSISTENCY_AXFRZONE_TRANSFER_HANDLER_MUST_BE_USED_ONLY_FOR_AXFR);

    }

    public void startIXFRAdds(Record soa) {
        throw new IllegalStateException(INCONSISTENCY_AXFRZONE_TRANSFER_HANDLER_MUST_BE_USED_ONLY_FOR_AXFR);
    }

    public void handleRecord(Record record) {
        allRecordCount++;
        if (record instanceof CNAMERecord) {
            // compare toLower case because domain should be case insensitive
            String name = String.valueOf(record.getName());
            String nameLower = name.toLowerCase();
            if (nameLower.startsWith("b-")) {
                cnameData.addDNSEntry(nameLower + "|" +
                        ((CNAMERecord) record).getTarget().
                                toString(true).toLowerCase());

            } else if (nameLower.contains(smpSubDomain)) {
                smpData.addDNSEntry(nameLower + "|" +
                        ((CNAMERecord) record).getTarget().
                                toString(true).toLowerCase());
            } else {
                LOG.info("A/CName record without prefix B- or is not SMP publisher record: " + record);
            }
        } else if (record instanceof ARecord) {
            // compare toLower case because domain should be case insensitive
            String name = String.valueOf(record.getName());
            String nameLower = name.toLowerCase();

            if (nameLower.contains(smpSubDomain)) {
                smpData.addDNSEntry(nameLower + "|" + ((ARecord) record).getAddress().getHostAddress());
            } else {
                LOG.info("A/CName record without prefix B- or is not SMP publisher record: " + record);
            }
        } else if (record instanceof NAPTRRecord) {
            String nameLower = String.valueOf(record.getName()).toLowerCase();
            String[] regexUrl = ((NAPTRRecord) record).getRegexp().split("!");
            naptrData.addDNSEntry(nameLower + "|" + regexUrl[2]);
        }

        if (readIndex % 100000 == 0) {
            LOG.debug("Processed records: " + readIndex + " (CName: " + cnameData.getAddedRowsDNS() + ", NAPTR:"
                    + naptrData.getAddedRowsDNS() + ", SMP: "
                    + smpData.getAddedRowsDNS() + ")");

            readIndex++;
        }

    }

    public int getAllRecordCount() {
        return allRecordCount;
    }
}
