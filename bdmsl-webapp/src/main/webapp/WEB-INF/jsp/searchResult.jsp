<%@ page import="eu.europa.ec.bdmsl.common.util.StringUtil" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="org.xbill.DNS.Record" %>
<%@ page import="java.util.Collection" %>
<%@ page import="java.util.Map" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <% response.addHeader("X-Frame-Options", "SAMEORIGIN"); %>
    <title>BDMSL search result</title>
</head>
<style>
    pre {
        margin-top: 0px;
        margin-bottom: 0px;
    }
</style>
<body>
<table>
    <tr>
        <td style="padding: 0 10px"><a href="${pageContext.servletContext.contextPath}/index.html">Home</a></td>
        <td style="padding: 0 10px"><a href="${pageContext.servletContext.contextPath}/listDNS">List DNS</a></td>
        <td style="padding: 0 10px"><a href="${pageContext.servletContext.contextPath}/search">Search participant</a></td>
    </tr>
</table>
<h1>Search participant result</h1>

<%
    boolean dnsEnabled = (boolean) request.getAttribute("dnsEnabled");
    String searchType = (String) request.getAttribute("searchType");


    if (!dnsEnabled) {
        out.println("<ul><li>The DNS client is disabled.</li></ul>");
    } else {
        if (StringUtils.equalsIgnoreCase(searchType, "domain")) {
            out.println("<ul><li><b>Domain:</b> "+StringUtil.escapeHtml4((String)request.getAttribute("domainName"))+"</li></ul>");
            Collection<Record> records = (Collection<Record>) request.getAttribute("recordList");
            if (records == null || records.isEmpty()) {
                out.println("<ul><li>Not results!</li></ul>");
            } else {
                for (Record record : records) {
                    out.println("<pre>");
                    out.println(record.toString());
                    out.println("</pre>")                                       ;
                }
            }
        } else {
            Map<String, Collection<Record>> recordMap = (Map<String, Collection<Record>>) request.getAttribute("recordMap");
            out.println("<ul><li><b>Identifier:</b> "+StringUtil.escapeHtml4((String)request.getAttribute("identifier"))+"</li>");
            out.println("<li><b>Scheme:</b> "+StringUtil.escapeHtml4((String)request.getAttribute("scheme"))+"</li></ul>");
            if (recordMap == null) {
                out.println("<ul><li>Did nof found any SML sub-domains! Validate BDMSL configuration for SML sub-domains!</li></ul>");
            } else {

                for (String domain : recordMap.keySet()) {
                    out.println("<h3>SML sub-domain: ");
                    out.println(domain);
                    out.println("</h3>");
                    Collection<Record> records = recordMap.get(domain);
                    if (records.isEmpty()) {
                        out.println("No records for sml domain!");
                    } else {
                        for (Record record : records) {
                            out.println("<pre>");
                            out.println(record.toString());
                            out.println("</pre>");
                        }
                    }
                }
            }
        }
    }
%>
</body>

</html>
