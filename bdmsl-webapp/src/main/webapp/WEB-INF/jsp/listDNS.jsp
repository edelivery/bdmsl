<%@ page import="org.xbill.DNS.Record" %>
<%@ page import="java.util.Collection" %>
<%@ page import="java.util.Map" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title>BDMSL Service</title>
        <% response.addHeader("X-Frame-Options", "SAMEORIGIN"); %>
    </head>
    <style>
        pre {
            margin-top: 0px;
            margin-bottom: 0px;
        }
    </style>
    <body>
    <table ><tr>
        <td style="padding: 0 10px"><a href="${pageContext.servletContext.contextPath}/index.html">Home</a></td>
        <td style="padding: 0 10px"><a href="${pageContext.servletContext.contextPath}/listDNS">List DNS</a></td>
        <td style="padding: 0 10px"><a href="${pageContext.servletContext.contextPath}/search">Search participant</a></td>
    </table>
        <h1>ListDNS</h1>
        <table ><tr>
        <%
            boolean dnsEnabled = (boolean)request.getAttribute("dnsEnabled");
            boolean isShowDNSEntriesEnabled = (boolean)request.getAttribute("isShowDNSEntriesEnabled");

            if (!dnsEnabled) {
                out.println("<ul><li>The DNS client is disabled.</li></ul>");
            }else if (!isShowDNSEntriesEnabled) {
                out.println("<ul><li>The DNS list entries is disabled.</li></ul>");
            } else {
                Map<String, Collection<Record>> recordMap =(Map<String, Collection<Record>>)request.getAttribute("recordMap");
                String dnsServer = (String)request.getAttribute("dnsServer");
                int numberOfRecords = (int)request.getAttribute("numberOfRecords");
                out.println("<ul><li>DNS server: ");
                out.println(dnsServer);
                out.println("</li></ul>");
                out.println("<ul><li>Number of records : ");
                out.println(numberOfRecords+"");
                out.println("</li></ul>");

                for (String domain: recordMap.keySet() ) {
                    out.println("<h2>");
                    out.println(domain);
                    out.println("</h2>");
                    Collection<Record> records = recordMap.get(domain);

                    for (Record record: records){
                        out.println("<pre>");
                        out.println(record.toString());
                        out.println("</pre>");
                    }
                }
            }
        %>
    </body>
</html>
