<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <% response.addHeader("X-Frame-Options", "SAMEORIGIN"); %>
    <title>BDMSL search Service</title>
    <style>


        /* Style the tab */
        .tab {
            overflow: hidden;
            border: 1px solid #ccc;
            background-color: #f1f1f1;
        }

        /* Style the buttons inside the tab */
        .tab button {
            background-color: inherit;
            float: left;
            border: none;
            outline: none;
            cursor: pointer;
            padding: 14px 16px;
            transition: 0.3s;
            font-size: 17px;
        }

        /* Change background color of buttons on hover */
        .tab button:hover {
            background-color: #ddd;
        }

        /* Create an active/current tablink class */
        .tab button.active {
            background-color: #ccc;
        }

        /* Style the tab content */
        .tabcontent {
            display: none;
            padding: 6px 12px;
            border: 1px solid #ccc;
            border-top: none;
        }

    </style>

    <script>
        function openCity(evt, cityName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }
    </script>
</head>
<body>
<table>
    <tr>
        <td style="padding: 0 10px"><a href="${pageContext.servletContext.contextPath}/index.html">Home</a></td>
        <td style="padding: 0 10px"><a href="${pageContext.servletContext.contextPath}/listDNS">List DNS</a></td>
        <td style="padding: 0 10px"><a href="${pageContext.servletContext.contextPath}/search">Search participant</a></td>
    </tr>
</table>
<h1>Search</h1>

<div class="tab">
    <button class="tablinks active" onclick="openCity(event, 'participant')">Participant</button>
    <button class="tablinks" onclick="openCity(event, 'domain')">DNS Domain</button>
</div>


<div id="participant" class="tabcontent" style="display: block;">
    <h2>Participant</h2>
    <table width="100%">
        <tr>

            <td width="60%" style="vertical-align:top">
                <h3>Search parameters:</h3>
                <form method="POST" action="searchResult">
                    <input type="hidden" name="searchType" value="participant"/>
                    <table width="60%">
                        <tr>
                            <td><b>Participant identifier:</b></td>
                            <td><input style="width:100%; padding-left:0px; padding-right:0px;" name="identifier"
                                       type="text" value=""/></td>
                        </tr>
                        <tr>
                            <td><b>Participant scheme:</b></td>
                            <td><input style="width:100%; padding-left:0px; padding-right:0px;" name="scheme"
                                       type="text" value=""/></td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right">
                                <input type="submit" value="Search"/>
                            </td>
                        </tr>
                    </table>
                </form>

            </td>
        <tr>
    </tr>
        <td width="40%">
            <div STYLE="color:#0c54ad;background-color:#bee5eb;border-color:#bee5eb; padding: 20px;margin: 30px">
                Search
                all domains
                on sml for participant.<br/>
                <b>PEPPOL identifier example:</b>
                <ul>
                    <li>Identifier: <b><i>0088:123456</i></b></li>
                    <li>Scheme: <b><i>iso6523-actorid-upis</i></b></li>
                </ul>
                <b>OASIS identifier example:</b>
                <ul>
                    <li>Identifier: <b><i>123456</i></b></li>
                    <li>Scheme: <b><i>urn:oasis:names:tc:ebcore:partyid-type:iso6523:0088</i></b></li>
                </ul>
            </div>
        </td>
        </tr>
    </table>
</div>

<div id="domain" class="tabcontent">
    <h2>DNS domain name</h2>
    <table width="100%">
        <tr>
            <td style="vertical-align:top">
                <h3>Search parameters:</h3>
                <form method="POST" action="searchResult">
                    <input type="hidden" name="searchType" value="domain"/>
                    <table width="60%">
                        <tr>
                            <td><b>Domain name</b></td>
                        </tr>
                        <tr>
                            <td><input style="width:100%; padding-left:0px; padding-right:0px;" name="domainName"
                                       type="text" value=""/></td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right">
                                <input type="submit" value="Search domain"/>
                            </td>
                        </tr>
                    </table>
                </form>
            </td>
        </tr>
        <td>
            <div STYLE="color:#0c54ad;background-color:#bee5eb;border-color:#bee5eb; padding: 20px;margin: 30px">
                Search by DNS domain<br/>
                <b>SMP domain</b> is generated as:
                <pre>&lt;SMP identifier>.publisher.&lt;sml domain>.&lt;dns zone></pre>

                <b>CNAME domain</b> for participant is generated as:
                <pre>B-md5(&lt;identifier>).&lt;participant-scheme>.&lt;sml domain>.&lt;dns zone> </pre>

                <b>NAPTR domain</b> for participant is generated as:
                <pre>sha256(&lt;identifier>).&lt;participant-scheme>.&lt;sml domain>.&lt;dns zone> </pre>

            </div>
        </td>
        </tr>
    </table>
</div>


</body>
</html>
