<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <% response.addHeader("X-Frame-Options", "SAMEORIGIN"); %>
    <title>BDMSL search Service</title>
</head>
<style>
    pre {
        margin-top: 0px;
        margin-bottom: 0px;
    }
</style>
<body>
<table>
    <tr>
        <td style="padding: 0 10px"><a href="${pageContext.servletContext.contextPath}/index.html">Home</a></td>
        <td style="padding: 0 10px"><a href="${pageContext.servletContext.contextPath}/listDNS">List DNS</a></td>
        <td style="padding: 0 10px"><a href="${pageContext.servletContext.contextPath}/search">Search participant</a></td>
    </tr>
</table>
<h1>DNS Search error!</h1>
<td>${error}</td>
</body>
</html>
