Keys are generated with Bind 9.11.3 and Bind 9.18.12 for ED2259 and ED448 using the following commands:
To use this keys make sure also DNS is configured properly! 
NOTE on newer Bind9 the DSA algorithm is disabled by default.
https://github.com/isc-projects/bind9/blob/bind-9.18/CHANGES
5045.    [func]        Remove support for DNSSEC algorithms 3 (DSA) and 6 (DSA-NSEC3-SHA1). GL #22


ED2259 and ED448 are supported from Bind 9.11.3 

```
dnssec-keygen -a DSA -b 1024 -T KEY -n HOST domisml-dsa.test.edelivery.local
dnssec-keygen -a RSASHA256 -b 4096 -T KEY -n HOST domisml-rsasha256.test.edelivery.local
dnssec-keygen -a RSASHA512 -b 4096 -T KEY -n HOST domisml-rsasha512.test.edelivery.local
dnssec-keygen -a ECDSAP256SHA256 -T KEY -n HOST domisml-ecdsap256sha256.test.edelivery.local
dnssec-keygen -a ECDSAP384SHA384 -T KEY -n HOST domisml-ecdsap384sha384.test.edelivery.local
dnssec-keygen -a ED25519 -T KEY -n HOST domisml-ed25519.test.edelivery.local
dnssec-keygen -a ED448 -T KEY -n HOST domisml-ed448.test.edelivery.local
```
