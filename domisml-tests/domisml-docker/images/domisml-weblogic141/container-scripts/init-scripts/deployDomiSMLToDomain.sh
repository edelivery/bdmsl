#!/usr/bin/env bash

#Define WL_DOMAIN_HOME
INIT_SCRIPTS=$1
echo "Domain Home is: $WL_DOMAIN_HOME"
echo "Scripts folder is: $INIT_SCRIPTS"

# set datasource property
DATA_SOURCE_PROPERTY_FILE="${INIT_SCRIPTS}/../properties/datasource.properties"
if [  -f "${WL_INIT_PROPERTIES}/datasource.properties" ]; then
  DATA_SOURCE_PROPERTY_FILE="${WL_INIT_PROPERTIES}/datasource.properties"
fi

#deploy smp datasource
wlst.sh -loadProperties "${DATA_SOURCE_PROPERTY_FILE}" "${INIT_SCRIPTS}/ds-deploy.py"

# copy smp startup configuration  - check first init folder else use default
if [  -f "${WL_INIT_PROPERTIES}/sml.config.properties" ]; then
  cat "${WL_INIT_PROPERTIES}/sml.config.properties" > "${CLASSPATH}/sml.config.properties"
elif [ -f "${INIT_SCRIPTS}/../properties/sml.config.properties" ]; then
  cat "${INIT_SCRIPTS}/../properties/sml.config.properties" > "${CLASSPATH}/sml.config.properties"
else
  cat <<EOT >"${CLASSPATH}/sml.config.properties"
# create sml property file
sml.hibernate.dialect=org.hibernate.dialect.Oracle10gDialect
sml.datasource.jndi=jdbc/eDeliverySmlDs
sml.jsp.servlet.class=weblogic.servlet.JSPServlet
sml.log.folder=./logs/
EOT
fi

# copy test keystore and encryption keys from default from the settings as the base artefacts
jar -xf "${ORACLE_HOME}"/init/bdmsl-webapp-setup.zip
cp bdmsl-webapp-*/*.*  "$SML_SECURITY_DIR"
cp "${ORACLE_HOME}"/init/edelivery-sml.war $WL_DOMAIN_HOME
cp "${ORACLE_HOME}"/init/sig0-keys/*.*  "$SML_SECURITY_DIR"

# override init artefacts as keystore, truststore, keys, from init compose folder  ...
if [  -d /opt/smlconf/init-configuration ]; then
  cp -r /opt/smlconf/init-configuration/*.*  ${SML_SECURITY_DIR}
fi

# Deploy Application
wlst.sh -skipWLSModuleScanning "${ORACLE_HOME}"/sml-app-deploy.py
