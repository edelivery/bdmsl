#!/usr/bin/env sh

CA_HOME=/opt/ocsp-ca
CA_LOG_FOLDER=${CA_HOME}/logs
CA_LOG_FILENAME=openssl.log
CA_PORT=8080

mkdir -p ${CA_LOG_FOLDER}

startOcspServer() {
    echo "Start OCSP on port ${CA_PORT}"
    openssl ocsp -index testCA/index.txt -port ${CA_PORT} -rsigner ocspSigning.crt -rkey ocspSigning.key -CA rootCA.crt -text -out "${CA_LOG_FOLDER}/${CA_LOG_FILENAME}"
}

# Start OCSP responder
cd "${CA_HOME}"
until false; do
    startOcspServer
    echo "Server 'openssl ocsp' crashed with exit code $?.  Respawning.." >&2
    mv "${CA_LOG_FOLDER}/${CA_LOG_FILENAME}" "${CA_LOG_FOLDER}/${CA_LOG_FILENAME}.$(date '+%Y-%m-%dT%H%M%S')"
    sleep 1
done
