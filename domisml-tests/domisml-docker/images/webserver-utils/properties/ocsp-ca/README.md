The content of the file  describes how examples were generated. 
(Based on post: https://bhashineen.medium.com/create-your-own-ocsp-server-ffb212df8e63)

1.  Create a folder to store the certificates and switch to that folder.

    cd /opt
    mkdir -p testCA/newcerts
    touch testCA/index.txt
    echo 000001 > testCA/serial


3. Copy the content of the openssl.cnf into a separate file. We will be using this new file as the configuration file to create certificates, certificate signing requests and etc.

       cp  /etc/ssl/openssl.cnf  ocsp-server.cnf

fix demoCA to testCA in ocsp-server.cnf
4. Add the following line under the section [ usr_cert ]. (this is for generating the end user certificate ocs extensions)

       [ usr_cert ]
       authorityInfoAccess = OCSP;URI:http://sml-crl-hostname:8080/

4. Create a new section in ocsp-server.cnf as follows,

       [ v3_OCSP ]
       basicConstraints = CA:FALSE
       keyUsage = nonRepudiation, digitalSignature, keyEncipherment
       extendedKeyUsage = OCSPSigning

5. Create a private key for root CA.

       openssl genrsa -out rootCA.key 2048

6. Based on this key, generate a CA certificate which is valid for 10 years based on the root CA’ s private key.

       openssl req -new -x509 -days 3650 -key rootCA.key -out rootCA.crt -config ocsp-server.cnf

7. Create another private key to be used as the end user private key.

       openssl genrsa -out certKeyValid.key 2048
       openssl genrsa -out certKeyRevoked.key 2048

8. Create an end user certificate based on the generated private key.

       openssl req -new -x509 -days 3650 -key certKeyValid.key -out certificateValid.crt -config ocsp-server.cnf
       openssl req -new -x509 -days 3650 -key certKeyRevoked.key -out certificateRevoked.crt -config ocsp-server.cnf

9. Generate the certificate signing requests(CSR) for the generated end-user certificate.

        openssl x509 -x509toreq -in certificateValid.crt -out CSR-CertValid.csr -signkey certKeyValid.key
        openssl x509 -x509toreq -in certificateRevoked.crt -out CSR-CertRevoked.csr -signkey certKeyRevoked.key

10. Sign the client certificates, using above created CA and include CRL URLs and OCSP URLs in the certificate

        openssl ca -batch -startdate 230831080000Z -enddate 330831090000Z -keyfile rootCA.key -cert rootCA.crt -policy policy_anything -config ocsp-server.cnf -notext -out certificateValidSigned.crt -infiles CSR-CertValid.csr
        openssl ca -batch -startdate 230831080000Z -enddate 330831090000Z -keyfile rootCA.key -cert rootCA.crt -policy policy_anything -config ocsp-server.cnf -notext -out certificateRevokedSigned.crt -infiles CSR-CertRevoked.csr

11. Creating the OCSP server  In order to host an OCSP server, an OCSP signing certificate has to be generated. Run following 2 commands.

        openssl req -new -nodes -out ocspSigning.csr -keyout ocspSigning.key
        openssl ca  -startdate 230831080000Z -enddate 330831090000Z -keyfile rootCA.key -cert rootCA.crt -in ocspSigning.csr -out ocspSigning.crt -config ocsp-server.cnf


**EXAMPLES:**

1. Example how to start OCSP Server.
   
        openssl ocsp -index testCA/index.txt -port 8080 -rsigner ocspSigning.crt -rkey ocspSigning.key -CA rootCA.crt -text -out openssl-ocsp.txt 

2. Example how to Verify Certificate Revocation. Switch to a new terminal and run

        openssl ocsp -CAfile rootCA.crt -issuer rootCA.crt -cert certificate.crt -url http://127.0.0.1:8080 -resp_text -noverify


3. Example how to Revoke a certificate

    openssl ca -keyfile rootCA.key -cert rootCA.crt -revoke certificateRevokedSigned.crt -config ocsp-server.cnf

NOTE: Restart the OCSP server after certificate is revoked!.


