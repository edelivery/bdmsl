DomiSML HTTP server for testing CRL retrieving
================================

Purpose of the image is to provide HTTP server for testing CRL/OCSP retrieving. It is based on httpd image and it is listening on port 80 and 443.
For HTTPS It is using self-signed certificate with CN=sml_crl_hostname. When using docker image in compose environment make sure
the container is accessible via hostname: sml_crl_hostname and generated certificate must have CRL set to

    http://sml_crl_hostname/[crl-filename].

as example: 

    http://sml-crl-hostname/sml-test-soapui-not-exists-01.crl

Crl files must be mapped to folder!

/usr/local/apache2/htdocs


DomiSML HTTP server for testing OCSP stapling 
================================
DomiSML 4.3+ provide support for newer SSL/TLS features like OCSP and OCSP stapling. To test this feature it is necessary to have certificates issued by our own OCSP servers to provide the status (revoked or good) of a certificate.
Image uses OpenSSL OCSP server functionalities setup "CA", issues example certificates and mimic OCSP stapling.

The "OpenSSL OCSP CA" server has two certificates  
 - ocspSigning.crt - OCSP signing certificate used for singing the responses
 - rootCA.crt - Root CA certificate used for issuing the certificates 
 - certificate/certificateValid.crt (the key: certKeyValid.key ) - example certificate issued by the CA and is not revoked
 - certificate/certificateRevoked.crt (the key: certKeyRevoked.key ) - example certificate issued by the CA and is revoked

