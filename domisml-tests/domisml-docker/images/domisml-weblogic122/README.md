BDMSL Image with WLS Domain
================================
This Dockerfile extends the Oracle WebLogic image built under 12213-domain-home-in-image and deploy the BDMSL application to admin server.
For initial configuration files bdmsl-webapp-${BDMSL_VERSION}-setup.zip are copied to weblogic domain. 

Deployment is using external datasource which configured in file:  weblogic-12.2.1.3-sml/container-scripts/datasource.properties.oracle

    dsname=cipaeDeliveryDs
    dsdbname=default;create=true
    dsjndiname=jdbc/cipaeDeliveryDs
    dsdriver=oracle.jdbc.OracleDriver
    dsurl=jdbc:oracle:thin:@//database:1521/xe
    dsusername=sml
    dspassword=test
    dstestquery=SQL ISVALID
    dsmaxcapacity=1

If needed change configuration before building an image.

# How to build and run

To build an image with BDMSL application first copy and rename arterfacts into folder:

    $ cp "${SML_ARTEFACTS}/bdmsl-webapp-${BDMSL_VERSION}.war" ./weblogic-12.2.1.3-sml/edelivery-sml.war 
    $ cp "${SML_ARTEFACTS}/bdmsl-webapp-${BDMSL_VERSION}-setup.zip" ./weblogic-12.2.1.3-sml/bdmsl-webapp-setup.zip

Then build image with command:

        $ docker build -t weblogic_sml ./weblogic-12.2.1.3-sml/ --build-arg BDMSL_VERSION="$BDMSL_VERSION" .

# How to run the domain

Tu run image execute command: 
  $ docker run weblogic_sml -p 7001:7001 . 

In your browser, enter `https://localhost:7001/edelivery_SMP`.
