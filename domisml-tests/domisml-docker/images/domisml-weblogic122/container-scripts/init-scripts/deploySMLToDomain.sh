#!/bin/bash

#Define DOMAIN_HOME
echo "Domain Home is: " $WL_DOMAIN_HOME
echo "Classpath / configuration folder: " $CLASSPATH
echo "SML security folderDomain Home is: " $SML_SECURITY_DIR

#deploy smp datasource
wlst.sh -loadProperties "/u01/init/datasource.properties" "/u01/oracle/init/scripts/ds-deploy.py"

if [ ! -d "$CLASSPATH" ]; then
  mkdir -p "$CLASSPATH";
fi

if [ ! -d "$SML_SECURITY_DIR" ]; then
  mkdir -p "$SML_SECURITY_DIR";
fi


# create sml property file
echo "sml.hibernate.dialect=org.hibernate.dialect.Oracle10gDialect" > "$CLASSPATH/sml.config.properties"
echo "sml.datasource.jndi=jdbc/eDeliverySmlDs" >> "$CLASSPATH/sml.config.properties"
echo "sml.jsp.servlet.class=weblogic.servlet.JSPServlet" >> "$CLASSPATH/sml.config.properties"
echo "sml.log.folder=./logs/" >> "$CLASSPATH/sml.config.properties"


# copy test keystore and encryption from the settings as the base artefacts
jar -xf "${ORACLE_HOME}"/init/bdmsl-webapp-setup.zip
cp "${ORACLE_HOME}"/init/edelivery-sml.war $WL_DOMAIN_HOME
cp bdmsl-webapp-*/*.*  "$SML_SECURITY_DIR"
cp "${ORACLE_HOME}"/init/sig0-keys/*.*  "$SML_SECURITY_DIR"


# override init artefacts as keystore, truststore, keys, ...
if [  -d /opt/smlconf/init-configuration ]; then
  cp -r /opt/smlconf/init-configuration/*.*  ${SML_SECURITY_DIR}
fi

# Deploy Application
wlst.sh -skipWLSModuleScanning /u01/oracle/sml-app-deploy.py
