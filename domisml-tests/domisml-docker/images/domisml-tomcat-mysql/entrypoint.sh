#!/usr/bin/env bash

set -e

ROOT_PASSWORD=${ROOT_PASSWORD:-password}
BIND_DATA_DIR=${DATA_DIR}/bind
MYSQL_DATA_DIR=${DATA_DIR}/mysql
TOMCAT_DIR=${DATA_DIR}/tomcat

if [ ! -d ${DATA_DIR} ]; then
  mkdir -p ${DATA_DIR}
fi

init_bind() {

  # move configuration if it does not exist
  if [ ! -d ${BIND_DATA_DIR} ]; then
    mv /etc/bind ${BIND_DATA_DIR}
    cp /opt/smlconf/bind/*.* ${BIND_DATA_DIR}/
  fi
  if [ ! -d /var/log/named/ ]; then
    mkdir /var/log/named/
    chown -R ${BIND_USER}:${BIND_USER} /var/log/named/
  fi

  if [ ! -d /run/named ]; then
    mkdir /run/named
    chown -R ${BIND_USER}:${BIND_USER} /run/named/
  fi

  rm -rf /etc/bind
  ln -sf ${BIND_DATA_DIR} /etc/bind
  mkdir -p ${BIND_DATA_DIR}/logs

  chmod -R 0775 ${BIND_DATA_DIR}
  chown -R ${BIND_USER}:${BIND_USER} ${BIND_DATA_DIR}

}

init_mysql() {
  # start MYSQL
  echo "[INFO]  Initialize mysql service: $(service mysql status)."
  #service mysql start
  # reinitialize mysql to start it with enabled lowercase tables, 'root' password and change the data folder
  service mysql stop
  rm -rf /var/lib/mysql
  if [ ! -d ${MYSQL_DATA_DIR} ]; then
    mkdir -p ${MYSQL_DATA_DIR}
  fi
  ln -sf ${MYSQL_DATA_DIR} /var/lib/mysql

  chmod -R 0775 ${MYSQL_DATA_DIR}
  usermod -d ${MYSQL_DATA_DIR} mysql

  chown mysql:mysql ${MYSQL_DATA_DIR}
  chmod 0775 ${MYSQL_DATA_DIR}
  echo "ALTER USER 'root'@'localhost' IDENTIFIED BY '${MYSQL_ROOT_PASSWORD}';" >/tmp/mysql-init

  mysqld --defaults-file=/etc/mysql/my.cnf --initialize --lower_case_table_names=1 --init-file=/tmp/mysql-init --user=mysql --console
  service mysql start

  PID_MYSQL=$(cat /var/run/mysqld/mysqld.sock.lock)
  if [ ! -d ${MYSQL_DATA_DIR}/${DB_SCHEMA} ]; then
    # create database
    echo "[INFO] recreate database schema: $DB_SCHEMA."
    mysql -h localhost -u root --password=${MYSQL_ROOT_PASSWORD} -e "drop schema if exists $DB_SCHEMA;DROP USER IF EXISTS $DB_USER; create schema $DB_SCHEMA;alter database $DB_SCHEMA charset=utf8; create user $DB_USER identified by '$DB_USER_PASSWORD';grant all on $DB_SCHEMA.* to $DB_USER;"
    # initialize database
    echo "[INFO] Init  scripts in folder /opt/smlconf/database-scripts/"
    ls -ltr /opt/smlconf/database-scripts/
    echo "[INFO] create database objects from mysql5innodb.ddl"
    mysql -h localhost -u root --password=${MYSQL_ROOT_PASSWORD} $DB_SCHEMA < /opt/smlconf/database-scripts/mysql5innodb.ddl

    if [ -f "/opt/smlconf/database-scripts/mysql5innodb-migrate.sql" ]; then
      echo "[INFO] Execute the migration script mysql5innodb-migrate.sql"
      mysql -h localhost -u root --password=${MYSQL_ROOT_PASSWORD} $DB_SCHEMA < /opt/smlconf/database-scripts/mysql5innodb-migrate.sql
    else
      echo "Migration script /opt/smlconf/database-scripts/mysql5innodb-migrate.sql does not exist"
    fi
    # init data
    echo "[INFO] Execute the init data script mysql5innodb-data.sql"
    mysql -h localhost -u root --password=${MYSQL_ROOT_PASSWORD} $DB_SCHEMA < /opt/smlconf/database-scripts/mysql5innodb-data.sql
  fi
}

init_tomcat() {

  # add java code coverage agent to image
  if [ -e /opt/jacoco/jacoco-agent.jar ]; then
    JAVA_OPTS="-javaagent:/opt/jacoco/jacoco-agent.jar=output=tcpserver,address=*,port=6901,includes=eu.europa.ec.bdmsl.* $JAVA_OPTS"
  fi

  # for debugging
  JAVA_OPTS="$JAVA_OPTS -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=9999 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Djava.rmi.server.hostname=localhost"

  echo "[INFO] init tomcat folders: $TOMCAT_DIR"
  if [ ! -d ${TOMCAT_DIR} ]; then
    mkdir -p ${TOMCAT_DIR}
  fi

  # move tomcat log folder to data folder
  if [ ! -d ${TOMCAT_DIR}/logs ]; then
    if [ ! -d ${TOMCAT_HOME}/logs ]; then
      mkdir -p ${TOMCAT_DIR}/logs
    else
      mv ${TOMCAT_HOME}/logs ${TOMCAT_DIR}/
      rm -rf ${TOMCAT_HOME}/logs
    fi
  fi
  rm -rf ${TOMCAT_HOME}/logs
  ln -sf ${TOMCAT_DIR}/logs ${TOMCAT_HOME}/logs
  echo "Test image: everyone can access the tomcat server log folder/files"
  chmod a+rwx -R  ${TOMCAT_DIR}/logs

  # move  conf folder to data folder
  if [ ! -d ${TOMCAT_DIR}/conf ]; then
    mv ${TOMCAT_HOME}/conf ${TOMCAT_DIR}/
  fi
  rm -rf ${TOMCAT_HOME}/conf
  ln -sf ${TOMCAT_DIR}/conf ${TOMCAT_HOME}/conf
  chown -R tomcat:tomcat ${TOMCAT_DIR}
  chmod u+x $TOMCAT_HOME/bin/*.sh

  init_sml_properties

  # override init artefacts as keystore, truststore, keys, ...
  if [ -d /opt/smlconf/init-configuration ]; then
    cp -r /opt/smlconf/init-configuration/*.* /opt/smlconf/
  fi

  # add trusted hostname certificate for CRL download over HTTPS test
  if [ -f /opt/smlconf/init-configuration/sml_crl_hostname.cer ]; then
    echo "Add CRL server certificate sml_crl_hostname.cer to truststore (for java 8 is jre/lib/security/cacerts)"
    "${JAVA_HOME}"/bin/keytool -importcert -alias test-host -keystore "${JAVA_HOME}/lib/security/cacerts" -storepass changeit -file /opt/smlconf/init-configuration/sml_crl_hostname.cer -noprompt
  fi

  configureServerHttps
}

function configureServerHttps() {
  echo "Create Tomcat HTTPS..."

  sed -i.bak -e "s#</Service>#<Connector port=\"8443\" protocol=\"org.apache.coyote.http11.Http11NioProtocol\" \
		maxThreads=\"150\" SSLEnabled=\"true\" scheme=\"https\" secure=\"true\" \
		clientAuth=\"true\" sslProtocol=\"TLS\" \
		truststoreFile=\"/opt/smlconf/truststore.p12\" \
  	truststorePass=\"test123\" \
  	truststoreType=\"PKCS12\" \
  	keystoreType=\"JKS\" \
		keystoreFile=\"/opt/smlconf/server-keystore.jks\" \
  	keystorePass=\"test123\" /> \
	</Service>#g" "${TOMCAT_HOME}/conf/server.xml"
}

init_sml_properties() {
  echo "[INFO] init sml properties: $SML_INIT_PROPERTIES"
  # set SML configuration
  if [ ! -d ${TOMCAT_DIR}/conf/sml ]; then
    mkdir ${TOMCAT_DIR}/conf/sml
    cp /tmp/sml.config.properties ${TOMCAT_DIR}/conf/sml/sml.config.properties
  fi
  addOrReplaceProperties "${TOMCAT_DIR}/conf/sml/sml.config.properties" "$SML_INIT_PROPERTIES" "$SML_INIT_PROPERTY_DELIMITER"
}

addOrReplaceProperties() {

  PROP_FILE=$1
  INIT_PROPERTIES=$2
  INIT_PROPERTY_DELIMITER=$3

  # replace domibus properties
  if [ -n "$INIT_PROPERTIES" ]; then
    echo "Parse init properties: $INIT_PROPERTIES"
    # add delimiter also to end :)
    s="$INIT_PROPERTIES$INIT_PROPERTY_DELIMITER"

    array=()
    while [[ $s ]]; do
      array+=("${s%%"$INIT_PROPERTY_DELIMITER"*}")
      s=${s#*"$INIT_PROPERTY_DELIMITER"}
    done

    echo "" >>"$PROP_FILE"
    # replace parameters
    IFS='='
    for property in "${array[@]}"; do
      read -r key value <<<"$property"

      property="$key=$value"

      keyRE="$(printf '%s' "${key// /}" | sed 's/[[\*^$()+?{|]/\\&/g')"
      propertyRE="$(printf '%s' "${property// /}" | sed 's/[[\*^$()+?{|/]/\\&/g')"

      echo "replace or add property: [$keyRE] with value [$propertyRE]"
      # replace key line and commented #key line with new property
      sed "s|^$keyRE=.*|$propertyRE|;s|^#$keyRE=.*|$propertyRE|" $PROP_FILE
      #      # test if replaced if the line not exists add in on the end
      grep -qF -- "$property" "$PROP_FILE" || echo "$property" >>"$PROP_FILE"
    done

  fi
}

#
# Function initialize  and star squid proxy. Prepositions for function are
# installed packages squid and apache2-utils!
#
function init_squid() {

  PROXY_FOLDER=${PROXY_FOLDER:-/data/proxy}
  PROXY_CONFIG_LOGS="${PROXY_FOLDER}/logs"
  PROXY_CONFIG_FILE="${PROXY_FOLDER}/squid.conf"
  PROXY_USERS_FILE=/etc/squid/passwd
  PROXY_AUTHENTICATION=${PROXY_AUTHENTICATION:-true}
  PROXY_USERS=${PROXY_USERS:-proxyuser1:test123,proxyuser2:test123}
  PROXY_PORT=${PROXY_PORT:-3127}
  # system dependant
  PROXY_LIBS=${PROXY_LIBS:-/usr/lib/squid}

  echo "[INFO] starting squid configuration"
  echo "---------------------------< suquid conf >---------------------------"
  echo "PROXY_CONFIG_FILE=${PROXY_CONFIG_FILE}"
  echo "PROXY_FOLDER=${PROXY_FOLDER}"
  echo "PROXY_USERS_FILE=${PROXY_USERS_FILE}"
  echo "PROXY_AUTHENTICATION=${PROXY_AUTHENTICATION}"
  echo "PROXY_USERS=${PROXY_USERS}"
  echo "------------------------------[ suquid conf ]-------------------------------"
  echo

  # set configuration
  [[ ! -d "${PROXY_FOLDER}" ]] && mkdir -p "${PROXY_FOLDER}"
  [[ ! -d "${PROXY_CONFIG_LOGS}" ]] && mkdir -p "${PROXY_CONFIG_LOGS}"
  [[ ! -d "/var/run/squid/" ]] && mkdir -p "/var/run/squid/"

  echo "# BDMSL squid configuration" >"${PROXY_CONFIG_FILE}"
  {
    echo "cache_access_log ${PROXY_CONFIG_LOGS}/access.log"
    echo "cache_log ${PROXY_CONFIG_LOGS}/cache.log"
    echo "cache_store_log ${PROXY_CONFIG_LOGS}/store.log"
    echo ""
    echo "pid_filename /var/run/squid/squidm.pid"
    echo "cache_effective_user tomcat"
    echo ""
    echo "http_port ${PROXY_PORT}"
    echo ""
  } >>"${PROXY_CONFIG_FILE}"

  if [ "${PROXY_AUTHENTICATION}" == "true" ]; then
    local users=(${PROXY_USERS//,/ })
    local userNames=()

    # clear file
    echo "# BDMSL proxy users" >"${PROXY_USERS_FILE}"
    for user in "${users[@]}"; do
      local userCredentials=(${user//:/ })
      userNames+=(${userCredentials[0]})
      # create a user
      htpasswd -b ${PROXY_USERS_FILE} ${userCredentials[0]} ${userCredentials[1]} || exit $?
    done
    echo "Created proxy users: ${userNames[*]}"

    # create squid property file
    {
      echo "auth_param basic program ${PROXY_LIBS}/basic_ncsa_auth  ${PROXY_USERS_FILE}"
      echo "auth_param basic children 5"
      echo "auth_param basic realm Squid proxy-caching web server"
      echo "auth_param basic credentialsttl 1 minute"
      echo "auth_param basic casesensitive off"
      echo ""
      echo "acl ncsa_users proxy_auth REQUIRED"
      echo "http_access allow ncsa_users"
      echo ""
    } >>${PROXY_CONFIG_FILE}
    # example to test
    # wget -e use_proxy=yes --proxy-user=proxyuser2 --proxy-password=test123 -e http_proxy=http://127.0.0.1:3127 -e https_proxy=http://127.0.0.1:3127 https://www.google.com/ --no-check-certificate
  else
    {
      echo "http_access allow all"
      echo ""
    } >>${PROXY_CONFIG_FILE}
  fi
  chown -R tomcat:tomcat ${PROXY_FOLDER}
  echo "Start squid proxy server"
  $(nohup $(which squid) -f ${PROXY_CONFIG_FILE} -NYCd 1 &>${PROXY_CONFIG_LOGS}/squid.out &)

  echo "Test image: everyone can access the proxy server log folder/files"
  chmod a+rwx -R ${PROXY_CONFIG_LOGS}
}

init_squid
init_bind
init_mysql
init_tomcat

#----------------------------------------------------
# stard bind 9
# allow arguments to be passed to named
echo "[INFO]  Start bind"
if [[ ${1:0:1} == '-' ]]; then
  EXTRA_ARGS="$@"
  set --
elif [[ ${1} == named || ${1} == $(which named) ]]; then
  EXTRA_ARGS="${@:2}"
  set --
fi

echo "Starting named..."
service named stop
$(which named) -4 -u ${BIND_USER} ${EXTRA_ARGS} -d 0 -L ${BIND_DATA_DIR}/logs/stdout.txt &

#----------------------------------------------------
# start tomcat
echo "[INFO] init tomcat JAVA_OPTS: $JAVA_OPTS"
cd "${TOMCAT_HOME}"
echo "export JAVA_OPTS=\"$JAVA_OPTS \$JAVA_OPTS\"" >>"${TOMCAT_HOME}"/bin/setenv.sh
su -s /bin/sh tomcat -c "${TOMCAT_HOME}/bin/catalina.sh jpda run "
