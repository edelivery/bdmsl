BDMSL on tomcat/mysql database.
================================

The Images are intended for internal testing of the DomiSML nightly snapshots builds. The images should not
be used in production environment.  

Image contains DomiSML deployed on the Tomcat 9 server with the MYSQL.

# How to build

To build an image with BDMSL application first copy and rename arterfacts into folder:

    cp "${SML_ARTEFACTS}/bdmsl-webapp-${BDMSL_VERSION}.war" ./tomcat-mysql/artefacts/edelivery-sml.war
    cp "${SML_ARTEFACTS}/bdmsl-webapp-${BDMSL_VERSION}-setup.zip" ./tomcat-mysql/artefacts/bdmsl-webapp-setup.zip

Then build image with command:

    docker build -t tomcat_mysql_sml ./tomcat-mysql/

# How to run

Tu run image execute command:

    docker run --name bdmsl -p 8084:8080 -p 3304:3306 bdmsl_tomcat_mysql:5.0-SNAPSHOT

In your browser, enter `https://localhost:8080/edelivery-sml` .

# how to run image from edelivery nexus.

The edelivery nexus contains prebuild images for the testing. To start the Tomcat Mysql image
login to docker registry 'edelivery-docker.devops.tech.ec.europa.eu' and execute the following command. 

    docker run --name bdmsl-tomcat edelivery-docker.devops.tech.ec.europa.eu/bdmsl_tomcat_mysql:5.0-SNAPSHOT  -p 3306:3306 -p 8080:8080


# Init database

By default the database is initialized using the scripts provided in bdmsl-webapp-${BDMSL_VERSION}-setup.zip. To initialize the database with custom script mount the volume with the script to internal folder  of the container "/opt/smlconf/database-scripts/" 

    docker exec -it bdmsl-tomcat /opt/init-db.sh -v "$(pwd)"/properties/db-scripts/:/opt/smlconf/database-scripts/

When the docker starts up for the first time it will execute the scripts with the following names in the order: (If scripts are not it the folder will be skipped) 
 - mysql5innodb.ddl: the database initialization script. If script is not present in the folder, then the script from setup bundle will be used.
 - mysql5innodb-migrate.sql: the database migration script. If not given script will be ignored.
 - mysql5innodb-data.sql: the data initialization script. If script is not present in the folder, then the script from setup bundle will be used.
