#!/usr/bin/env bash

# This is build script for building image.
# first it copies external resources to resources folder
# then it builds the image using docker-compose.build.yml
# and finally it cleans the external resources
WORKING_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
cd "${WORKING_DIR}"

source "${WORKING_DIR}/../../functions/common.functions"
initializeCommonVariables

: "${SML_PROJECT_FOLDER:?Need to set SML project folder non-empty!}"
: "${BDMSL_VERSION:?Need to set SML version non-empty!}"
: "${SML_ARTEFACTS:?Need to set SML_ARTEFACTS non-empty!}"


copyExternalImageResources() {
		echo "Copy test project resources ..."
    # copy artefact to docker build folder
    cleanExternalImageResources
    mkdir -p ./artefacts
    # copy artefact to docker build folder
    cp -r ../shared-artefacts/* ./artefacts/
    buildPrepareSMLArtefacts ./artefacts
}

cleanExternalImageResources() {
  echo "Clean external resources ..."
  [[ -d  ./artefacts ]] && rm -rf ./artefacts/
}

composeBuildImage() {
	echo "Build ${IMAGE_NAME_DOMIBUS_SOAPUI} image..."
	docker compose -f docker-compose.build.yml build
}


# clean external resources before copy
copyExternalImageResources
composeBuildImage
cleanExternalImageResources
