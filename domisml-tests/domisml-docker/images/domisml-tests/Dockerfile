# create builder to install maven which is needed
FROM maven:3.9.6-eclipse-temurin-11

ARG TEST_USER=edeltest
ARG TEST_GROUP=${TEST_USER}
ARG UID=1700
ARG GID=1701
# Build artefacts

#===================
# Timezone settings set to UTC by default!
#===================
ENV TZ "UTC"


#======================================
# Configure environement
#======================================
ENV TEST_USER=${TEST_USER} \
    DATA=/data \
    TEST_URL=http://localhost:8080/smp/

ENV TEST_UI=${DATA}/test-ui \
    TEST_API=${DATA}/test-api \
    RESULT_FOLDER=${DATA}/results \
    TEST_PLAN=ui

USER root
# create test user, folders and configure timezone
RUN groupadd ${TEST_GROUP} \
         --gid ${GID} \
  && useradd ${TEST_USER} \
         --create-home \
         --gid ${GID} \
         --shell /bin/bash \
         --uid ${UID} \
  && mkdir -p "${DATA}" \
  && ln -fs /usr/share/zoneinfo/${TZ} /etc/localtime \
  && dpkg-reconfigure -f noninteractive tzdata \
  && cat /etc/timezone


# install firefox dependencies libgtk-3-0 libasound2 x11-common libx11-xcb1
# install oher usefull tools bzip2 ca-certificates tzdata unzip wget
RUN apt-get update -qqy \
  && apt-get -qqy --no-install-recommends install  \
    libgtk-3-0 \
    libasound2 \
    x11-common \
    libx11-xcb1 \
    bzip2 \
    ca-certificates \
    tzdata \
    unzip \
    wget \
  && rm -rf /var/lib/apt/lists/* /var/cache/apt/*


COPY ./artefacts/test-api ${TEST_API}
COPY ./entrypoint.sh /sbin/entrypoint.sh

RUN chmod 755 /sbin/entrypoint.sh \
    && mkdir -p /${DATA}/results \
    && chown -R ${TEST_USER}:${TEST_USER} /data

USER ${TEST_USER}
# dry run mvn and download all dependencies
RUN cd ${TEST_API}  \
    && mvn dependency:resolve -P run-soapui -DskipITs -DskipTests \
    && mvn clean install -P run-soapui -DskipITs -DskipTests

ENTRYPOINT ["/sbin/entrypoint.sh"]
