#!/usr/bin/env bash

# This is build script for building image.
# first it copies external resources to resources folder
# then it builds the image using docker-compose.build.yml
# and finally it cleans the external resources

WORKDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
cd "${WORKDIR}"
source "${WORKDIR}/../../functions/common.functions"
initializeCommonVariables

: "${SML_PROJECT_FOLDER:?Need to set SML project folder non-empty!}"
: "${BDMSL_VERSION:?Need to set SMP version non-empty!}"
: "${SML_ARTEFACTS:?Need to set SMP_ARTEFACTS non-empty!}"


copyExternalImageResources() {
     # first clean the artefacts
     cleanExternalImageResources
     # copy artefact to docker build folder
     echo "Copy test project resources [${SML_PROJECT_FOLDER}/domisml-tests/domisml-tests-api"
     mkdir -p ./artefacts/
     # copy artefact to docker build folder
     cp -r "${SML_PROJECT_FOLDER}/domisml-tests/domisml-tests-api" ./artefacts/test-api
}

cleanExternalImageResources() {
    echo "Clean external resources ..."
    [[ -d  ./artefacts/test-api ]] && rm -rf ./artefacts/test-api
}

composeBuildImage() {
	echo "Build ${IMAGE_NAME_DOMIBUS_SOAPUI} image..."
	docker compose -f docker-compose.build.yml build
}


# clean external resources before copy
copyExternalImageResources
composeBuildImage
cleanExternalImageResources
