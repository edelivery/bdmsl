#!/bin/bash
set -e

BIND_DATA_DIR=${DATA_DIR}/bind

if [ ! -d ${DATA_DIR} ]; then
   mkdir -p ${DATA_DIR}
fi

init_bind() {

  # move configuration if it does not exist
  if [ ! -d ${BIND_DATA_DIR} ]; then
    mv /etc/bind ${BIND_DATA_DIR}
    cp /opt/smlconf/bind/*.* ${BIND_DATA_DIR}/
  fi
  if [ ! -d /var/log/named/ ]; then
    mkdir /var/log/named/
    chown -R ${BIND_USER}:${BIND_USER} /var/log/named/
  fi

  if [ ! -d /run/named ]; then
    mkdir /run/named
    chown -R ${BIND_USER}:${BIND_USER} /run/named/
  fi

  rm -rf /etc/bind
  ln -sf ${BIND_DATA_DIR} /etc/bind
  mkdir -p ${BIND_DATA_DIR}/logs

  chmod -R 0775 ${BIND_DATA_DIR}
  chown -R ${BIND_USER}:${BIND_USER} ${BIND_DATA_DIR}

}

#
# Function initialize  and star squid proxy. Prepositions for function are
# installed packages squid and apache2-utils!
#
function init_squid() {

    PROXY_FOLDER=${PROXY_FOLDER:-/data/proxy}
    PROXY_CONFIG_LOGS="${PROXY_FOLDER}/logs"
    PROXY_CONFIG_FILE="${PROXY_FOLDER}/squid.conf"
    PROXY_USERS_FILE=/etc/squid/passwd
    PROXY_AUTHENTICATION=${PROXY_AUTHENTICATION:-true}
    PROXY_USERS=${PROXY_USERS:-proxyuser1:test123,proxyuser2:test123}
    PROXY_PORT=${PROXY_PORT:-3127}
    # system dependant
    PROXY_LIBS=${PROXY_LIBS:-/usr/lib/squid}

    echo "[INFO] starting squid configuration"
    echo "---------------------------< suquid conf >---------------------------"
    echo "PROXY_CONFIG_FILE=${PROXY_CONFIG_FILE}"
    echo "PROXY_FOLDER=${PROXY_FOLDER}"
    echo "PROXY_USERS_FILE=${PROXY_USERS_FILE}"
    echo "PROXY_AUTHENTICATION=${PROXY_AUTHENTICATION}"
    echo "PROXY_USERS=${PROXY_USERS}"
    echo "------------------------------[ suquid conf ]-------------------------------"; echo

    # set configuration
    [[ ! -d "${PROXY_FOLDER}" ]] &&  mkdir -p "${PROXY_FOLDER}"
    [[ ! -d "${PROXY_CONFIG_LOGS}" ]] &&  mkdir -p "${PROXY_CONFIG_LOGS}"
    [[ ! -d "/var/run/squid/" ]] &&  mkdir -p "/var/run/squid/"

    echo "# Squid configuration" > "${PROXY_CONFIG_FILE}"

    {
        echo "cache_access_log ${PROXY_CONFIG_LOGS}/access.log"
        echo "cache_log ${PROXY_CONFIG_LOGS}/cache.log"
        echo "cache_store_log ${PROXY_CONFIG_LOGS}/store.log"
        echo ""
        echo "pid_filename /var/run/squid/squidm.pid"
        echo "cache_effective_user edelivery"
        echo ""
        echo "http_port ${PROXY_PORT}"
        echo ""
    } >> "${PROXY_CONFIG_FILE}"

    if [ "${PROXY_AUTHENTICATION}" == "true" ]; then
        local users=(${PROXY_USERS//,/ })
        local userNames=()

        # clear file
        echo "# Proxy users" > "${PROXY_USERS_FILE}"
        for user in "${users[@]}"; do
            local userCredentials=(${user//:/ })
            userNames+=(${userCredentials[0]})
            # create a user
            htpasswd -b  ${PROXY_USERS_FILE} ${userCredentials[0]} ${userCredentials[1]} || exit $?
        done
        echo "Created proxy users: ${userNames[*]}"

        # create squid property file
        {
            echo "auth_param basic program ${PROXY_LIBS}/basic_ncsa_auth  ${PROXY_USERS_FILE}"
            echo "auth_param basic children 5"
            echo "auth_param basic realm Squid proxy-caching web server"
            echo "auth_param basic credentialsttl 1 minute"
            echo "auth_param basic casesensitive off"
            echo ""
            echo "acl ncsa_users proxy_auth REQUIRED"
            echo "http_access allow ncsa_users"
            echo ""
        } >> ${PROXY_CONFIG_FILE}
        # example to test
        # wget -e use_proxy=yes --proxy-user=proxyuser2 --proxy-password=test123 -e http_proxy=http://127.0.0.1:3127 -e https_proxy=http://127.0.0.1:3127 https://www.google.com/ --no-check-certificate
    else
      {
         echo "http_access allow all"
         echo ""
      } >> ${PROXY_CONFIG_FILE}
    fi
    chown -R edelivery:edelivery ${PROXY_FOLDER}

    echo "Create squid log files in : ${PROXY_CONFIG_FILE}"
    touch ${PROXY_CONFIG_LOGS}/access.log
    touch ${PROXY_CONFIG_LOGS}/cache.log
    touch ${PROXY_CONFIG_LOGS}/store.log

   echo "Start squid proxy server"
   $(nohup $(which squid) -f ${PROXY_CONFIG_FILE} -NYCd 1 &> ${PROXY_CONFIG_LOGS}/squid.out &)
   chmod a+rwx -R "${PROXY_CONFIG_LOGS}"
}

init_squid
init_bind

#----------------------------------------------------
# start bind 9
# allow arguments to be passed to named
echo  "[INFO]  Start bind"
if [[ ${1:0:1} = '-' ]]; then
  EXTRA_ARGS="$@"
  set --
elif [[ ${1} == named || ${1} == $(which named) ]]; then
  EXTRA_ARGS="${@:2}"
  set --
fi

echo "Starting named..."
/etc/init.d/bind9 start
# this log is set in named.conf.local
sleep 2
tail -f "/var/log/named/general.log" &
childPID=$!
wait $childPID
