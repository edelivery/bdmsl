BDMSL on tomcat/mysql database and bind9 DNS server.
================================

Image contains simple bind9 DNS server.

# How to build and run

To build an image with BDMSL application first copy and rename arterfacts into folder:

    $ cp "${SML_ARTEFACTS}/bdmsl-webapp-${BDMSL_VERSION}.war" ./tomcat-mysql/artefacts/edelivery-sml.war
    $ cp "${SML_ARTEFACTS}/bdmsl-webapp-${BDMSL_VERSION}-setup.zip" ./tomcat-mysql/artefacts/bdmsl-webapp-setup.zip

Then build image with command:

        $  docker build -t sml_dns_sample .

# How to run the domain

Tu run image execute command:
  $ docker run tomcat_mysql_sml -p 3306:3306 -p 8080:8080 .

In your browser, enter `https://localhost:8080/edelivery-sml`.
