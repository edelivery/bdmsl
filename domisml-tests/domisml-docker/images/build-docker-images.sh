#!/usr/bin/env bash

WORKDIR="$(cd -P $( dirname "${BASH_SOURCE[0]}" ) && pwd)"
cd ${WORKDIR} || exit 100
echo "Working Directory: ${WORKDIR}"
#load common functions
source "${WORKDIR}/../functions/common.functions"
initializeCommonVariables
exportBuildArtefactNames

# Script builds docker images for SML oracle/weblogic environment. Docker images for database and weblogic are from
# https://github.com/oracle/docker-images

# Prerequisites:
# 1. From oracle download:
#  - OracleDB: oracle-xe-11.2.0-1.0.x86_64.rpm.zip
#  - Server JDK 1.8:  server-jre-8u211-linux-x64.tar.gz  (https://github.com/oracle/docker-images/tree/master/OracleJava)
#  - weblogic 12.2.1.3: fmw_12.2.1.3.0_wls_quick_Disk1_1of1.zip
#  - weblogic 14.1.1.0: fmw_14.1.1.0.0_wls_lite_Disk1_1of1.zip
# and put them to folder ${ORACLE_ARTEFACTS}
#
# 2. build SML mvn clean install
# 3. run the scripts with arguments
# build-docker-images.sh  -f build-docker-images.sh  -f ${oracle_artefact_folder}

#ORA_VERSION="19.3.0"
#ORA_EDITION="se2"
#ORA_SERVICE="ORCLPDB1"

ORA_VERSION="11.2.0.2"
ORA_EDITION="xe"
ORA_SERVICE="xe"

ORACLE_ARTEFACTS="/CEF/repo"

SML_PROJECT_FOLDER=$(readlink -e "${WORKDIR}/../../..")
SML_ARTEFACTS="${SML_PROJECT_FOLDER}/bdmsl-webapp/target"
SML_SPRINGBOOT_ARTEFACTS="${SML_PROJECT_FOLDER}/bdmsl-springboot/target"
SML_ARTEFACTS_CLEAR="false"

SML_IMAGE_PUBLISH="false"
DOCKER_USER=${bamboo_DOCKER_USER:-edeliverytest}
DOCKER_PASSWORD=$bamboo_DOCKER_PASSWORD
DOCKER_REGISTRY_HOST=${bamboo_DOCKER_REGISTRY_HOST}
DOCKER_FOLDER=${bamboo_DOCKER_FOLDER:-${bamboo_DOCKER_USER}}

# READ arguments
while getopts v:o:a:s:c:p: option; do
  case "${option}" in

  v) BDMSL_VERSION=${OPTARG} ;;
  o) ORACLE_ARTEFACTS=${OPTARG} ;;
  a) SML_ARTEFACTS=${OPTARG} ;;
  s) SML_SPRINGBOOT_ARTEFACTS=${OPTARG} ;;
  c) SML_ARTEFACTS_CLEAR=${OPTARG} ;;
  p) SML_IMAGE_PUBLISH=${OPTARG} ;;
  *) echo "Unknown option: $option. Use [-v version] [-o oracle_artefact_folder] [-a sml_artefact_folder] [-s sml_springboot_artefact_folder] [-c clear_sml_artefact_folder] [-p publish_image]"
    exit 1 ;;
  esac
done


echo "*****************************************************************"
echo "* SML artefact folders: [$SML_ARTEFACTS], (Clear folder after build: [$SML_ARTEFACTS_CLEAR] )"
echo "* SML artefact springboot folders: [$SML_SPRINGBOOT_ARTEFACTS]"
echo "* Build SML image for version [$BDMSL_VERSION]"
echo "* Oracle artefact folders: [$ORACLE_ARTEFACTS]"
echo "*****************************************************************"
echo ""

export BDMSL_VERSION
export SML_PROJECT_FOLDER
export SML_ARTEFACTS
export SML_PLUGIN_EXAMPLE
export SML_SPRINGBOOT_ARTEFACTS
export ORACLE_ARTEFACTS

# -----------------------------------------------------------------------------
# validate all necessary artefacts and prepare files to build images
# -----------------------------------------------------------------------------
validateAndPrepareArtefacts() {
  case "${ORA_VERSION}" in
  "19.3.0")
    ORACLE_DB_FILE="${ORACLE_DB19_FILE}"
    ORACLE_DOCKERFILE="Dockerfile"
    ;;
  "11.2.0.2")
    ORACLE_DB_FILE="${ORACLE_DB11_FILE}"
    ORACLE_DOCKERFILE="Dockerfile.xe"
    ;;
  esac

  export ORA_VERSION
  export ORA_EDITION
  export ORA_SERVICE

  # check oracle database
  if [[ ! -f "${ORACLE_ARTEFACTS}/Oracle/OracleDatabase/${ORA_VERSION}/${ORACLE_DB_FILE}" ]]; then
    echo "Oracle database artefacts '${ORACLE_ARTEFACTS}/Oracle/OracleDatabase/${ORA_VERSION}/${ORACLE_DB_FILE}' not found."
    exit 1
  else
    # copy artefact to docker build folder
    cp "${ORACLE_ARTEFACTS}/Oracle/OracleDatabase/${ORA_VERSION}/${ORACLE_DB_FILE}" ./oracle/oracle-db-${ORA_VERSION}/
  fi

}


# -----------------------------------------------------------------------------
# build docker images
# -----------------------------------------------------------------------------
buildImages() {
  buildOracleDatabaseImage
  buildUtils
# The DomiSML 5.0 does not support weblogic 12/14 since they not support
# jakarta EE 10 and java jdk 21
#  buildImage "${IMAGE_DOMISML_WEBLOGIC122}"
#  buildImage "${IMAGE_DOMISML_WEBLOGIC141}"
  buildImage "${IMAGE_DOMISML_TOMCAT_MYSQL}"
  buildImage "${IMAGE_DOMISML_SPRINGBOOT_MYSQL}"
  buildImage "${IMAGE_DOMISML_TESTS}"
}

buildImage(){
  echo "Build image [${IMAGE_TAG:-edeliverytest}/$1:${BDMSL_VERSION}]."
  ./"$1"/build.sh
  if [ $? -ne 0 ]; then
    echo "Error occurred while building image [${IMAGE_TAG:-edeliverytest}/$1:${BDMSL_VERSION}]!"
    exit 10
  fi
}

buildOracleDatabaseImage(){
  # -----------------------------------------------------------------------------
  # build docker image for oracle database
  # -----------------------------------------------------------------------------
  # oracle 1.2.0.2-xe (https://github.com/oracle/docker-images/tree/master/OracleDatabase/SingleInstance/dockerfiles/11.2.0.2)
  docker build -f ./oracle/oracle-db-${ORA_VERSION}/${ORACLE_DOCKERFILE} -t "${IMAGE_TAG:-edeliverytest}/${IMAGE_DOMISML_DB_ORACLE}-${ORA_VERSION}-${ORA_EDITION}:${BDMSL_VERSION}" --build-arg DB_EDITION=${ORA_EDITION} ./oracle/oracle-db-${ORA_VERSION}/
  if [ $? -ne 0 ]; then
    echo "Error occurred while building image [${IMAGE_TAG:-edeliverytest}/${IMAGE_DOMISML_DB_ORACLE}-${ORA_VERSION}-${ORA_EDITION}:${BDMSL_VERSION}]!"
    exit 10
  fi
}


buildUtils(){

# build the httpd image for LB. The Http is configured to allow encoded characters which
  # are not decoded!
  docker build -t "${IMAGE_TAG:-edeliverytest}/sml-httpd:${BDMSL_VERSION}" ./sml-httpd/
   if [ $? -ne 0 ]; then
     echo "Error occurred while building image [sml-httpd:${BDMSL_VERSION}]!"
     exit 10
   fi

  # build standard dns and proxy utils.
  [[ -d  ./test-utils-dns-proxy/artefacts ]] && rm -rf ./test-utils-dns-proxy/artefacts
  mkdir -p ./test-utils-dns-proxy/artefacts
  # copy artefact to docker build folder
  cp -r ./shared-artefacts/* ./test-utils-dns-proxy/artefacts/

  docker build -t ${IMAGE_TAG:-edeliverytest}/${IMAGE_DOMISML_DNS_PROXY_UTILS}:${BDMSL_VERSION} ./test-utils-dns-proxy/
  if [ $? -ne 0 ]; then
      echo "Error occurred while building image: [${IMAGE_DOMISML_DNS_PROXY_UTILS}:${BDMSL_VERSION}]!"
      exit 107
  fi

  # build httpd for sharing crl  files to test revocation list retrieving
  docker build -t ${IMAGE_TAG:-edeliverytest}/${IMAGE_DOMISML_WEBSERVER_UTILS}:${BDMSL_VERSION} ./webserver-utils/
  if [ $? -ne 0 ]; then
      echo "Error occurred while building image: [${IMAGE_DOMISML_WEBSERVER_UTILS}:${BDMSL_VERSION}]!"
      exit 107
  fi
}

function pushImageToDockerhub() {

  if [[ "V$SML_IMAGE_PUBLISH" == "Vtrue" ]]; then
    # login to docker
    echo "--username=${DOCKER_USER} --password=${DOCKER_PASSWORD} ${DOCKER_REGISTRY_HOST}"
    #docker login --username="${DOCKER_USER}" --password="${DOCKER_PASSWORD}" "${DOCKER_REGISTRY_HOST}"
    # push images
    pushImageIfExisting "${IMAGE_DOMISML_TOMCAT_MYSQL}:${BDMSL_VERSION}"
    pushImageIfExisting "${IMAGE_DOMISML_SPRINGBOOT_MYSQL}:${BDMSL_VERSION}"
    pushImageIfExisting "${IMAGE_DOMISML_WEBLOGIC122}:${BDMSL_VERSION}"
    pushImageIfExisting "${IMAGE_DOMISML_WEBLOGIC141}:${BDMSL_VERSION}"
    pushImageIfExisting "${IMAGE_DOMISML_DB_ORACLE}-${ORA_VERSION}-${ORA_EDITION}:${BDMSL_VERSION}"
    pushImageIfExisting "${IMAGE_DOMISML_TESTS}:${BDMSL_VERSION}"
  fi
}

function pushImageIfExisting() {
  if [[ "x$(docker images -q "${IMAGE_TAG:-edeliverytest}/${1}")" != "x" ]]; then
    local TAGGED_IMAGE="${DOCKER_REGISTRY_HOST:+$DOCKER_REGISTRY_HOST/}${DOCKER_FOLDER:+$DOCKER_FOLDER/}${1}"
    docker tag "${IMAGE_TAG:-edeliverytest}/${1}" "${TAGGED_IMAGE}"
    echo "Pushing image ${1} as ${TAGGED_IMAGE}"
    docker push "${TAGGED_IMAGE}"
  else
    echo "Could not find image ${1} to push!"
  fi
  return 0
}

# -----------------------------------------------------------------------------
# clean
# -----------------------------------------------------------------------------
cleanArtefacts() {
  rm "./oracle/oracle-db-${ORA_VERSION}/${ORACLE_DB_FILE}"   # clean
  rm "./oracle/OracleJava/java-8/${SERVER_JDK_FILE}"         # clean
  rm "./oracle/weblogic-12.2.1.4/${WEBLOGIC_122_QUICK_FILE}" # clean
  rm "./oracle/weblogic-14.1.1.0/${WEBLOGIC_14_FILE}" # clean

  rm -rf "./${IMAGE_DOMISML_WEBLOGIC122}/artefacts/*.*"

  if [[ "V$SML_ARTEFACTS_CLEAR" == "Vtrue" ]]; then
    rm -rf "${SML_ARTEFACTS}/sml-setup.zip"
    rm -rf "${SML_ARTEFACTS}/sml.war"
  fi
}

validateAndPrepareArtefacts
buildImages
pushImageToDockerhub
cleanArtefacts
