#!/bin/bash

# This is build script clean starting the docker compose containers for weblogic and oracle db integration tests.
# The script is used for local development and CI integration testing only.
#
# IMPORTANT NOTE: The script clears all old containers, volumes and bind volumes and then starts the docker compose containers.


# init plan variables
WORKDIR="$(cd -P $(dirname "${BASH_SOURCE[0]}" ) && pwd)"
source "${WORKDIR}/../../functions/run-test.functions"
initializeVariables
START_LOCAL="false"

SML_INIT_DATABASE="${SML_PROJECT_FOLDER}/bdmsl-webapp/src/main/sml-setup/database-scripts/oracle10g.ddl"
#SML_INIT_DATABASE_DATA="${SML_PROJECT_FOLDER}/sml-webapp/src/main/sml-setup/database-scripts/oracle10g-data.sql"
SML_INIT_DATABASE_DATA="${SML_PROJECT_FOLDER}/domisml-tests/domisml-tests-api/src/test/resources/init-data/init-test-oracle-soapui.sql"
SML_INIT_CONFIGURATION="${SML_PROJECT_FOLDER}/domisml-tests/domisml-tests-api/src/test/resources/init-data/configuration"

#ORA_VERSION="19.3.0"
#ORA_EDITION="se2"
#ORA_SERVICE="ORCLPDB1"
#ORACLE_PDB="ORCLPDB1"
ORA_VERSION="11.2.0.2"
ORA_EDITION="xe"
ORA_SERVICE="xe"

SML_DB_USERNAME="sml"
SML_DB_PASSWORD="test"
# this is JDBC URL for SML application, the hostname must match the one from docker-compose.yml for database service
SML_JDBC_URL="jdbc:oracle:thin:@//sml-oracle-db:1521/${ORA_SERVICE}"
SML_DB_SCRIPTS=./properties/db-scripts
SML_WLS_INIT_SCRIPTS=./properties/weblogic-init

# READ arguments
while getopts i:v:l: option
do
  case "${option}"
  in
    i) SML_INIT_DATABASE_DATA=${OPTARG};;
    v) BDMSL_VERSION=${OPTARG};;
    l) START_LOCAL=${OPTARG};;
    *) echo "Unknown option [${option}]. Usage: $0 [-i] [-v] [-l]"; exit 1;;
  esac
done

echo "*************************************************************************"
echo "SML version: $BDMSL_VERSION"
echo "Init sql data: ${SML_INIT_DATABASE_DATA}"
echo "Working Directory: ${WORKDIR}"
echo "*************************************************************************"
cd "$WORKDIR" || exit 1

# clear old containers mounted volume ./data
function clearMoundDataVolume() {
  : "${WORKDIR?"Need to set $WORKDIR non-empty!"}"
  : "${SML_DB_SCRIPTS?"Need to set SML_DB_SCRIPTS non-empty!"}"
  echo "Clear container data ${WORKDIR}/data/"
  rm -rf "${WORKDIR}/data"
  rm -rf "${SML_DB_SCRIPTS}"
  mkdir -p ${WORKDIR}/data/upload
  mkdir -p ${WORKDIR}/data/sml/config
  mkdir -p ${WORKDIR}/data/sml/security
  mkdir -p ${WORKDIR}/data/weblogic/keystores
  # create database init scripts
  mkdir -p "${SML_DB_SCRIPTS}"
}

# start
export BDMSL_VERSION
export ORA_VERSION
export ORA_EDITION
export BDMSL_VERSION

echo "Clear old containers"
stopAndClearTestContainers
clearMoundDataVolume
initOracleDatabaseConfiguration $SML_DB_USERNAME $SML_DB_PASSWORD "${SML_DB_SCRIPTS}" "${SML_WLS_INIT_SCRIPTS}"

echo "Copy configuration files from ${SML_INIT_CONFIGURATION}"
[[ ! -d "${WORKDIR}/properties/init-configuration" ]] && mkdir -p "${WORKDIR}/properties/init-configuration"
cp -r  "${SML_INIT_CONFIGURATION}"/*.*  ${WORKDIR}/properties/init-configuration/

# start "
echo "Start containers"
startTestContainers
