#!/bin/bash

# Variables
# -i: path to the database data initialization script, default: SML_PROJECT_FOLDER/smp-webapp/src/main/smp-setup/database-scripts/mysql5innodb-data.sql
# -v: version of the SMP to start. If not provided, the version will defined by maven project version
# -l: start with local compose file docker-compose.localhost.yml, default: false. The compose file is used to start
#      the SMP with local configuration (e.g. exporting ports, etc.)

# init plan variables
WORKDIR="$(cd -P $(dirname ${BASH_SOURCE[0]} ) && pwd)"
source "${WORKDIR}/../../functions/run-test.functions"
initializeVariables
START_LOCAL="false"

SML_INIT_DATABASE="${SML_PROJECT_FOLDER}/bdmsl-webapp/src/main/sml-setup/database-scripts/mysql5innodb.ddl"
SML_MIGRATE_DATABASE=
#SML_INIT_DATABASE_DATA="${SML_PROJECT_FOLDER}/bdmsl-webapp/src/main/sml-setup/database-scripts/mysql5innodb-data.sql"
# soap ui data
SML_INIT_DATABASE_DATA="${SML_PROJECT_FOLDER}/domisml-tests/domisml-tests-api/src/test/resources/init-data/init-test-mysql-soapui.sql"
SML_INIT_CONFIGURATION="${SML_PROJECT_FOLDER}/domisml-tests/domisml-tests-api/src/test/resources/init-data/configuration"
# example to test migration from 4.2.1 to 4.3
# SML_INIT_DATABASE="/test/sml/migration/mysql5innodb-4.2.1.ddl"
# SML_MIGRATE_DATABASE="${SML_PROJECT_FOLDER}/bdmsl-webapp/src/main/sml-setup/database-scripts/migration/mysql-4.2.1-to-4.3-migration.ddl"
# SML_INIT_DATABASE_DATA="${SML_PROJECT_FOLDER}/domisml-tests/domisml-tests-api/src/test/resources/init-data/init-test-mysql-soapui.sql"




# READ arguments
while getopts i:v:l: option
do
  case "${option}"
  in
    i) SML_INIT_DATABASE_DATA=${OPTARG};;
    v) BDMSL_VERSION=${OPTARG};;
    l) START_LOCAL=${OPTARG};;
    *) echo "Unknown option [${option}]. Usage: $0 [-i] [-v] [-l]"; exit 1;;
  esac
done

echo "*****************************************************************"
echo "* Start SMP image for version: [$BDMSL_VERSION]"
echo "* Plan prefix: [${PLAN_PREFIX}]"
echo "* WORKDIR: [${WORKDIR}]"
echo "*****************************************************************"
echo ""
# export plan variables
export BDMSL_VERSION
# init database scripts
DB_SCRIPT_FOLDER="./properties/db-scripts"
[[ ! -d "${DB_SCRIPT_FOLDER}" ]] &&  mkdir -p "${DB_SCRIPT_FOLDER}"
# create  database init script from l
echo "Copy database scripts from ${SML_INIT_DATABASE} and ${SML_INIT_DATABASE_DATA}"
cp  "${SML_INIT_DATABASE}" ./properties/db-scripts/mysql5innodb.ddl
cp  "${SML_INIT_DATABASE_DATA}" ./properties/db-scripts/mysql5innodb-data.sql

echo "Copy configuration files from ${SML_INIT_CONFIGURATION}"
[[ ! -d "${WORKDIR}/properties/init-configuration" ]] && mkdir -p "${WORKDIR}/properties/init-configuration"
cp -r  "${SML_INIT_CONFIGURATION}"/*.*  ${WORKDIR}/properties/init-configuration/

echo "Clear old containers"
stopAndClearTestContainers
# start " 
echo "Start containers"
startTestContainers
