#!/bin/bash

# Variables
# -i: path to the database data initialization script, default: SML_PROJECT_FOLDER/smp-webapp/src/main/smp-setup/database-scripts/mysql5innodb-data.sql
# -v: version of the SMP to start. If not provided, the version will defined by maven project version
# -l: start with local compose file docker-compose.localhost.yml, default: false. The compose file is used to start
#      the SMP with local configuration (e.g. exporting ports, etc.)

# init plan variables
WORKDIR="$(cd -P $(dirname ${BASH_SOURCE[0]} ) && pwd)"
source "${WORKDIR}/../../functions/run-test.functions"
initializeVariables
START_LOCAL="false"

PREFIX="${BUILD_KEY}-sml-springboot-mysql"

SML_INIT_DATABASE="${SML_PROJECT_FOLDER}/bdmsl-webapp/src/main/sml-setup/database-scripts/mysql5innodb.ddl"
SML_MIGRATE_DATABASE=
#SML_INIT_DATABASE_DATA="${SML_PROJECT_FOLDER}/bdmsl-webapp/src/main/sml-setup/database-scripts/mysql5innodb-data.sql"
# soap ui data
SML_INIT_DATABASE_DATA="${SML_PROJECT_FOLDER}/domisml-tests/domisml-tests-api/src/test/resources/init-data/init-test-mysql-soapui.sql"
SML_INIT_CONFIGURATION="${SML_PROJECT_FOLDER}/domisml-tests/domisml-tests-api/src/test/resources/init-data/configuration"

# READ arguments
while getopts i:v:l: option
do
  case "${option}"
  in
    i) SML_INIT_DATABASE_DATA=${OPTARG};;
    v) BDMSL_VERSION=${OPTARG};;
    l) START_LOCAL=${OPTARG};;
    *) echo "Unknown option [${option}]. Usage: $0 [-i] [-v]"; exit 1;;
  esac
done


discoverApplicationVersion

echo "*************************************************************************"
echo "SML version: [${BDMSL_VERSION}]"
echo "Init sql data: [${SML_INIT_DATABASE_DATA}]"
echo "Working Directory: [${WORKDIR}]"
echo "*************************************************************************"

export BDMSL_VERSION


# check if property folder exists if not create it
if  [ ! -d "./properties/db-scripts/" ]
then
    mkdir -p "./properties/db-scripts/"
fi
if [ -f "${SML_MIGRATE_DATABASE}" ]; then
  echo "Copy migrate file ${SML_MIGRATE_DATABASE}"
  cp  "${SML_MIGRATE_DATABASE}" ./properties/db-scripts/mysql5innodb-migrate.sql
fi
# create  database init script from l
cp   "${SML_INIT_DATABASE}" ./properties/db-scripts/mysql5innodb.ddl
cp   "${SML_INIT_DATABASE_DATA}" ./properties/db-scripts/mysql5innodb-data.sql

[[ ! -d "./properties/init-configuration" ]] && mkdir -p "./properties/init-configuration"
cp -r  "${SML_INIT_CONFIGURATION}"/*.* ./properties/init-configuration/


echo "Clear old containers"
stopAndClearTestContainers
# start "
echo "Start containers"
startTestContainers


