/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;

/*!50503 SET character_set_client = utf8mb4 */;

insert into bdmsl_configuration(property, value, description, created_on, last_updated_on) values
('useProxy','true','true if a proxy is required to connect to the internet. Possible values: true/false', NOW(), NOW()),
('unsecureLoginAllowed','false','true if the use of HTTPS is not required. If the value is set to true, then the user unsecure-http-client is automatically created. Possible values: true/false', NOW(), NOW()),
('signResponse','false','true if the responses must be signed. Possible values: true/false', NOW(), NOW()),
('signResponseAlgorithm','','The signature algorithm to use when signing responses. Examples: ''http://www.w3.org/2001/04/xmldsig-more#rsa-sha256'', ''http://www.w3.org/2021/04/xmldsig-more#eddsa-ed25519'', ...', NOW(), NOW()),
('signResponseDigestAlgorithm','http://www.w3.org/2001/04/xmlenc#sha256','The signature digest algorithm to use when signing responses. Examples: ''http://www.w3.org/2001/04/xmlenc#sha256'', ''http://www.w3.org/2001/04/xmlenc#sha512''', NOW(), NOW()),
('paginationListRequest','100','Number of participants per page for the list operation of ManageParticipantIdentifier service. This property is used for pagination purposes.', NOW(), NOW()),
('keystorePassword','vXA7JjCy0iDQmX1UEN1Qwg==','Base64 encrypted password for Keystore.', NOW(), NOW()),
('keystoreFileName','keystore.jks','The keystore file. Should be just the filename if the file is in the classpath or in the configurationDir', NOW(), NOW()),
('keystoreType','JKS','The keystore type. Possible values: JKS/PKCS12.', NOW(), NOW()),
('truststorePassword','CmBwV155qIcuOR4QPh+a2w==','Base64 encrypted password for Keystore.', NOW(), NOW()),
('truststoreFileName','truststore.p12','The truststore file. Should be just the filename if the file is in the classpath or in the configurationDir', NOW(), NOW()),
('truststoreType','PKCS12','The truststore type. Possible values: JKS/PKCS12.', NOW(), NOW()),
('keystoreAlias','sendercn','The alias in the keystore.', NOW(), NOW()),
('httpProxyUser','proxyuser1','The proxy user', NOW(), NOW()),
('httpProxyPort','3127','The http proxy port', NOW(), NOW()),
('httpProxyPassword','CmBwV155qIcuOR4QPh+a2w==','Base64 encrypted password for Proxy.', NOW(), NOW()),
('httpProxyHost','127.0.0.1','The http proxy host', NOW(), NOW()),
('encriptionPrivateKey','encriptionPrivateKey.private','Name of the 256 bit AES secret key to encrypt or decrypt passwords.', NOW(), NOW()),
('dnsClient.server','sml-dns-service','The DNS server', NOW(), NOW()),
('dnsClient.use.legacy.regexp','true','Legacy regexp', NOW(), NOW()),
('dnsClient.publisherPrefix','publisher','This is the prefix for the publisher(SMP). This is to be concatenated with the associated DNS domain in the table bdmsl_certificate_domain', NOW(), NOW()),
('dnsClient.enabled','true','true if registration of DNS records is required. Must be true in production. Possible values: true/false', NOW(), NOW()),
('dnsClient.SIG0PublicKeyName','sig0.test.edelivery.local.','The public key name of the SIG0 key', NOW(), NOW()),
('dnsClient.SIG0KeyFileName','Ksig0.test.edelivery.local.+003+48216.private','The actual SIG0 key file. Should be just the filename if the file is in the classpath or in the configurationDir', NOW(), NOW()),
('dnsClient.SIG0Enabled','true','true if the SIG0 signing is enabled. Required fr DNSSEC. Possible values: true/false', NOW(), NOW()),
('dataInconsistencyAnalyzer.senderEmail','automated-notifications@nomail.ec.europa.eu','Sender email address for reporting Data Inconsistency Analyzer.', NOW(), NOW()),
('dataInconsistencyAnalyzer.recipientEmail','email@domain.com','Email address to receive Data Inconsistency Checker results', NOW(), NOW()),
('dataInconsistencyAnalyzer.cronJobExpression','0 0 3 ? * *','Cron expression for dataInconsistencyChecker job. Example: 0 0 3 ? * * (everyday at 3:00 am)', NOW(), NOW()),
('configurationDir','/opt/smlconf/','The absolute path to the folder containing all the configuration file(keystore and sig0 key)', NOW(), NOW()),
('certificateChangeCronExpression','0 0 2 ? * *','Cron expression for the changeCertificate job. Example: 0 0 2 ? * * (everyday at 2:00 am)', NOW(), NOW()),
('authorization.smp.certSubjectRegex','^.*(CN=SMP_|OU=PEPPOL TEST SMP|CN=senderCN).*$','User with ROOT-CA is granted SMP_ROLE only if its certificates Subject matches configured regexp', NOW(), NOW()),
('authentication.bluecoat.enabled','true','Enables reverse proxy authentication.', NOW(), NOW()),
('authentication.sslclientcert.enabled','true','Enables reverse proxy authentication with x509Certificate', NOW(), NOW()),
('adminPassword','$2a$10$9RzbkquhBYRkHUoKMTNZhOPJmevTbUKWf549MEiCWUd.1LdblMhBi','BCrypt Hashed password to access admin services', NOW(), NOW()),
('mail.smtp.host','mail-service','SMTP host name ', NOW(), NOW()),
('mail.smtp.port','2500','SMTP server port', NOW(), NOW()),
('sml.property.refresh.cronJobExpression','5 */1 * * * *','Properties update', NOW(), NOW()),
('sml.cluster.enabled','false','Define if application is set in cluster. In not cluster environment, properties are updated on setProperty.', NOW(), NOW()),
('dnsClient.show.entries','true','Enable tool for showing dns entries.', NOW(), NOW());



insert into bdmsl_subdomain(subdomain_id, subdomain_name,dns_zone, description, participant_id_regexp, dns_record_types, smp_url_schemas, created_on, last_updated_on) values
(1, 'test.edelivery.local', 'test.edelivery.local','Domain for OpenPeppol ', '^((((0002|0007|0009|0037|0060|0088|0096|0097|0106|0135|0142|9901|9902|9904|9905|9906|9907|9908|9909|9910|9912|9913|9914|9915|9916|9917|9918|9919|9920|9921|9922|9923|9924|9925|9926|9927|9928|9929|9930|9931|9932|9933|9934|9935|9936|9937|9938|9939|9940|9941|9942|9943|9944|9945|9946|9947|9948|9949|9950|9951|9952|9953|9954|9955|9956|9957|0184|0191|0192):).*)|(\\*))$','all','all', NOW(), NOW()),
(2, 'ehealth.test.edelivery.local','test.edelivery.local','Domain for eHealth ','^.*$','all','all',NOW(), NOW()),
(3, 'generalerds.test.edelivery.local','test.edelivery.local','Domain for isaitb ','^.*$','all','all',NOW(), NOW()),
(4, 'isaitb.test.edelivery.local','test.edelivery.local','Domain for isaitb ','^.*$','all','all',NOW(), NOW());

LOCK TABLES `bdmsl_certificate_domain` WRITE;
/*!40000 ALTER TABLE `bdmsl_certificate_domain` DISABLE KEYS */;
INSERT INTO bdmsl_certificate_domain(certificate, crl_url,  is_root_ca, fk_subdomain_id, created_on, last_updated_on, is_admin, truststore_alias) VALUES
('CN=rootCNTest,OU=B4,O=DIGIT,L=Brussels,ST=BE,C=BE','',1, 1, NOW(), NOW(),0, NULL),
('CN=rootCNIsa,OU=B4,O=DIGIT,L=Brussels,ST=BE,C=BE','',1, 3, NOW(), NOW(),1,NULL),
('CN=AdministratorSML,OU=B4,O=DIGIT,C=BE','',0, 2, NOW(), NOW(),1,NULL),
('CN=unsecure_root,O=delete_in_production,C=only_for_testing','',1,1,NOW(),NOW(),0,NULL),
('CN=SMLAdmin,O=DIGIT,C=BE','',0,2,NOW(),NOW(),1,NULL),
('CN=unsecure_root_testTeam,O=delete_in_production,C=only_for_testing','',1,1,NOW(),NOW(),0,NULL),
('CN=EHEALTH_SMP_z_001,O=DI_z_GIT,C=B_z_E','',1,2,NOW(),NOW(),0,NULL),
('CN=rootCN,OU=B4,O=DIGIT,L=Brussels,ST=BE,C=BE','http://crl.globalsign.net/root.crl',1,1,NOW(),NOW(),0,NULL),
('CN=EHEALTH_SMP_OU_777000,O=DIGIT,C=BE','',0,2,NOW(),NOW(),0,NULL),
('CN=EHEALTH_SMP_\\ ,C=B_\ ,O=DI_\\ ','',1,2,NOW(),NOW(),0,NULL),
('CN=EHEALTH_SMP_\\+_001,C=B_\\+_E,O=DI_\\+_GIT','',1,2,NOW(),NOW(),0,NULL),
('CN=GENERALERDStest_SMP_TEST_setcce,O=European Commission,C=SI','http://crl.globalsign.net/root.crl',0,1,NOW(),NOW(),0,'smlintermsubdomain1rootca1'),
('CN=EHEALTH_SMP_\\,_001,C=B_\\,_E,O=DI_\\,_GIT','',1,2,NOW(),NOW(),0,NULL),
('CN=EHEALTH_SMP_\\;_001,C=B_\\;_E,O=DI_\\;_GIT','',1,2,NOW(),NOW(),0,NULL),
('CN=EHEALTH_SMP_\\<_001,C=B_\\<_E,O=DI_\\<_GIT','',1,2,NOW(),NOW(),0,NULL),
('CN=Connectivity Test Team CA,OU=Connecting Europe Facility,O=Connectivity Test,ST=Belgium,C=BE','http://crl.globalsign.net/root.crl',1,2,NOW(),NOW(),0,NULL),
('CN=EHEALTH_SMP_\\=_001,C=B_\\=_E,O=DI_\\=_GIT','',1,2,NOW(),NOW(),0,NULL),
('CN=EHEALTH_SMP_\\>_001,C=B_\\>_E,O=DI_\\>_GIT','',1,2,NOW(),NOW(),0,NULL),
('CN=EHEALTH_SMP_\\:_001,C=B_\\:_E,O=DI_\\:_GIT','',1,2,NOW(),NOW(),0,NULL),
('CN=EHEALTH_SMP_\\&_001,C=B_\\&_E,O=DI_\\&_GIT','',1,2,NOW(),NOW(),0,NULL),
('CN=SMP_700082,O=DIGIT,C=BE','',1,2,NOW(),NOW(),0,NULL),
('CN=SML_admin_subdomain1,O=DIGIT,C=BE','',0,1,NOW(),NOW(),1,'smladminsubdomain1'),
('CN=SML_admin_subdomain2,O=DIGIT,C=BE','',0,2,NOW(),NOW(),1,'smladminsubdomain2'),
('CN=SML_admin_subdomain3,O=DIGIT,C=BE','',0,3,NOW(),NOW(),1,'smladminsubdomain3'),
('CN=SML_admin_subdomain4,O=DIGIT,C=BE','',0,4,NOW(),NOW(),1,'smladminsubdomain4'),
('CN=SML_interm_subdomain1_rootca,O=DIGIT,C=BE','',1,1,NOW(),NOW(),0,'smlintermsubdomain1rootca'),
('CN=SML_interm_subdomain1_rootca_outdated,O=DIGIT,C=BE','',1,1,NOW(),NOW(),0,'smlintermsubdomain1rootcaout'),
('CN=SML_interm_subd1_ca_random_order,O=DIGIT,C=BE','',1,1,NOW(),NOW(),0,'smlintermsubd1carandomorder'),
('CN=root_noCrlUrl,O=DIGIT,C=BE','',1,1,NOW(),NOW(),0,NULL),
('CN=SML_interm_subd1_ca_not_root,O=DIGIT,C=BE','',0,1,NOW(),NOW(),0,'smlintermsubdomain1notroot'),
('CN=SML_interm_subd1_sp_char_&\\+-\\=neeeecøðaæœecu,O=DI\\,G\\;IT,C=BE','',1,1,NOW(),NOW(),0,'smlintermsubd1specialchars'),
('CN=SMP_subd2_subject_is_root,O=DIGIT,C=BE','',1,2,NOW(),NOW(),0,'certsubd2subjectisroot'),
('CN=SMP_subd2_trust_by_subject,O=DIGIT,C=BE','',0,2,NOW(),NOW(),0,'certsubd2trustbysubject'),
('CN=ehealth_subd2_trust_by_subject,O=DIGIT,C=BE','',0,2,NOW(),NOW(),0,'certsubd2trustbysubjectnorole'),
('CN=peppol_trust_priority,O=DIGIT,C=BE','',0,2,NOW(),NOW(),0,'peppoltrustpriority'),
('CN=SML_interm_subdomain3_rootca,O=DIGIT,C=BE','',1,3,NOW(),NOW(),0,'smlintermsubdomain3rootca'),
('CN=cert_subdomain_3,O=DIGIT,C=BE','',1,3,NOW(),NOW(),0,'interm_cert_subdomain_3'),
('CN=SMP_https_sml_4_testTeam,O=DIGIT,C=BE','',1,1,NOW(),NOW(),0,'smp_https_sml_4_testteam'),
('CN=SMP_700084,O=DIGIT,C=BE','',0,2,NOW(),NOW(),0,'smp_700084'),
('CN=EHEALTH_SMP_700084,O=DIGIT,C=BE','',0,2,NOW(),NOW(),0,'ehealth_smp_700084');
/*!40000 ALTER TABLE `bdmsl_certificate_domain` ENABLE KEYS */;
UNLOCK TABLES;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;


