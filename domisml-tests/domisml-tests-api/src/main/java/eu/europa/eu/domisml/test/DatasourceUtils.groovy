package eu.europa.eu.domisml.test

import org.apache.commons.dbcp2.BasicDataSource

import javax.sql.DataSource

class DatasourceUtils {

    DataSource dataSource;

    private static DatasourceUtils datasourceUtils;

    private DatasourceUtils(def context) {
        // initialize datasource
        dataSource = new BasicDataSource(driverClassName: context.expand( '${#Project#jdbc.driver}' ),
                url: context.expand( '${#Project#jdbc.url}' ), username: context.expand( '${#Project#dbUser}' ),
                password: context.expand( '${#Project#dbPassword}' ));
    }


    static DatasourceUtils getInstance(def context) {
        if (datasourceUtils == null) {
            datasourceUtils = new DatasourceUtils(context);
        }
        return datasourceUtils;
    }
}
