/*
 * (C) Copyright 2023 - European Commission | CEF eDelivery
 *
 * Licensed under the EUPL, Version 1.1 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * \BDMSL\bdmsl-parent-pom\License EUPL-1.1.pdf or http://ec.europa.eu/idabc/servlets/Docbb6d.pdf?id=31979
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.eviware.soapui.support.GroovyUtils;
import com.google.common.io.BaseEncoding
import eu.europa.eu.domisml.test.DatasourceUtils;
import groovy.sql.Sql;
import groovyx.net.http.HTTPBuilder;

import java.security.MessageDigest;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import static groovyx.net.http.ContentType.*;
import static groovyx.net.http.Method.*;

/**
 * @author GHOUILI Ahmed
 * @since 20/10/2017
 */
class BDMSL
{
    // Database parameters
    def sql;
    def messageExchange;
    def context;
    def log;
    def url;
    def driver;
	def dnsSubDomain=null;
    def requestParamTable = [];
    def responseParamTable = [];
    def checker=0;
    def String testDatabase="false";
	def String testDns="false";
	def urlDNS=null;
	def dbUser=null;
	def dbPassword=null
	def dns_client_record_types="dnsClient.dnsRecordTypes.";
	def dns_client_domain="dnsClient.domain.";
	def participant_id_regex="subdomain.validation.participantIdRegex.";
	def smp_logical_address_protocol="subdomain.validation.smpLogicalAddressProtocolRestriction.";
	def smp_update_max_part_size="smp.update.max.part.size";
	def dnsClient_show_entries="dnsClient.show.entries";
	def dnsClient_use_legacy_regexp="dnsClient.use.legacy.regexp"
	def maxSMPparticipants=0;
	def listDNSenabled=false;
	def NAPTRregularExp="!^.*\$!";
	
	static def DEFAULT_LOG_LEVEL = 0;

    // Constructor of the SMP Class
    BDMSL(log,messageExchange,context) {
        this.log = log;
        this.messageExchange = messageExchange;
        this.context=context;
        this.url=context.expand( '${#Project#jdbc.url}' );
        driver=context.expand( '${#Project#jdbc.driver}' );
        testDatabase=context.expand( '${#Project#testDB}' );
		testDns=context.expand( '${#Project#testDNS}' );
		urlDNS=context.expand( '${#Project#url}' );
		dbUser=context.expand( '${#Project#dbUser}' );
        dbPassword=context.expand( '${#Project#dbPassword}' );
		sql = null;
		maxSMPparticipants=getIntegerFromString(getSMLConfigProperty(smp_update_max_part_size));
		listDNSenabled=getbooleanFromString(getSMLConfigProperty(dnsClient_show_entries));
		NAPTRregularExp=getNAPTRregularExpressionValue(dnsClient_use_legacy_regexp)
		//displayConstrParams(); 
    }

    // Class destructor
    void finalize() {
        log.info "Test finished."
		closeConnection();
    }
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
	// Log information wrapper 
	static def void debugLog(logMsg, log,  logLevel = DEFAULT_LOG_LEVEL) {
		if (logLevel.toString()=="1" || logLevel.toString() == "true") 
			log.info (logMsg)
	}
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
    // Simply open DB connection (dev or test depending on testEnvironment variable)	
	def openConnection(){
       
		// do not open connection if is already  open
        if(testDatabase.toLowerCase()=="true" && sql==null){
            try{
				if(driver.contains("oracle")){
					// Oracle DB
					GroovyUtils.registerJdbcDriver( "oracle.jdbc.driver.OracleDriver" )
				}else{
					// Mysql DB (assuming fallback: currently, we use only those 2 DBs ...)
					GroovyUtils.registerJdbcDriver( "com.mysql.jdbc.Driver" )
				}
				sql = Sql.newInstance(DatasourceUtils.getInstance(this.context).getDataSource());
            }
            catch (SQLException ex)
            {
                assert 0,"SQLException occurred: " + ex;
            }
		}
    }
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
    // Close the DB connection opened previously
    def closeConnection(){
        if(testDatabase.toLowerCase()=="true"){
            if(sql){
				try{
					sql.close();
				}
				catch (SQLException ex){
					assert 0,"SQLException occurred: " + ex;
				}
                sql = null;
            }
        }
    }
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
    // Extract sudomains configuration values 	
	def getSubdomainsConfiguration(testSuite){
       
	    def usedefault=true;
		def i=0;
		def connOpenHere=false;
		
	    def subdomainsProperties=[
								  "subdomain1":["id":"1","name":"test.edelivery.local","zone":"test.edelivery.local","idregexp":"^((((0002|0007|0009|0037|0060|0088|0096|0097|0106|0135|0142|9901|9902|9904|9905|9906|9907|9908|9909|9910|9912|9913|9914|9915|9916|9917|9918|9919|9920|9921|9922|9923|9924|9925|9926|9927|9928|9929|9930|9931|9932|9933|9934|9935|9936|9937|9938|9939|9940|9941|9942|9943|9944|9945|9946|9947|9948|9949|9950|9951|9952|9953|9954|9955|9956|9957|0184):).*)|(\\*))\$","dnsrecordtypes":"all","smpurlschemas":"all"], 
								  "subdomain2":["id":"2","name":"ehealth.test.edelivery.local","zone":"test.edelivery.local","idregexp":"^.*\$","dnsrecordtypes":"all","smpurlschemas":"all"],
								  "subdomain3":["id":"3","name":"generalerds.test.edelivery.local","zone":"test.edelivery.local","idregexp":"^.*\$","dnsrecordtypes":"all","smpurlschemas":"all"],
								  "subdomain4":["id":"4","name":"isaitb.test.edelivery.local","zone":"test.edelivery.local","idregexp":"^.*\$","dnsrecordtypes":"all","smpurlschemas":"all"]
								]
		
		// do not open connection if is already open
        if(testDatabase.toLowerCase()=="true"){
			if(sql==null){
				openConnection();
				connOpenHere=true;
			}			
			for(i=1;i<=subdomainsProperties.size();i++){
				log.info("==== Checking  subdomain "+i);
				try{
					sql.eachRow("SELECT * FROM bdmsl_subdomain WHERE subdomain_id = ?",[i]){
						testSuite.setPropertyValue("subd"+i+"Name",it.subdomain_name);
						testSuite.setPropertyValue("subd"+i+"Zone",it.dns_zone);
						testSuite.setPropertyValue("subd"+i+"IdRegExp",it.participant_id_regexp);
						testSuite.setPropertyValue("subd"+i+"DNSRecTypes",it.dns_record_types);
						testSuite.setPropertyValue("subd"+i+"SMPUrlSchema",it.smp_url_schemas);
					}
				}
				catch (Exception ex){
					if(connOpenHere){
						closeConnection();
					}
					log.error "getSubdomainsConfiguration [][] Exception occured: " + ex;
					log.info "Assume subdomains default configuration ..."	
				}
			}
			if(connOpenHere){
				closeConnection();
			}
			usedefault=false
		}
		if(usedefault){
			for(i=1;i<=subdomainsProperties.size();i++){
				testSuite.setPropertyValue("subd"+i+"Name",subdomainsProperties["subdomain"+i].name);
				testSuite.setPropertyValue("subd"+i+"Zone",subdomainsProperties["subdomain"+i].zone);
				testSuite.setPropertyValue("subd"+i+"IdRegExp",subdomainsProperties["subdomain"+i].idregexp);
				testSuite.setPropertyValue("subd"+i+"DNSRecTypes",subdomainsProperties["subdomain"+i].dnsrecordtypes);
				testSuite.setPropertyValue("subd"+i+"SMPUrlSchema",subdomainsProperties["subdomain"+i].smpurlschemas);
			}
		}
		
		for(i=1;i<=subdomainsProperties.size();i++){
			log.info "Configuration for sudomain"+i
			log.info "name="+testSuite.getPropertyValue("subd"+i+"Name");
			log.info "dns_zone="+testSuite.getPropertyValue("subd"+i+"Zone");
			log.info "participant_id_regexp="+testSuite.getPropertyValue("subd"+i+"IdRegExp");
			log.info "dns_record_types="+testSuite.getPropertyValue("subd"+i+"DNSRecTypes");
			log.info "smp_url_schemas="+testSuite.getPropertyValue("subd"+i+"SMPUrlSchema");
			log.info "===================================================================="
		}
    }
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
	// Get property value from Bdmsl configuration without DB access
	def static getBdmslPropertyValue(testRunner,context,log, String propertyName){
	
		def testStepRef=null;
		def testStepResponse=null;
		def respContent=null;
		def allNodes=null;
		def respPropName=null;
		def respPropvalue=null;
		
		debugLog("getBdmslPropertyValue [][]	Trying to retrieve the property value of property \"$propertyName\".",log);
		
		// Retrieve testStep reference
		testStepRef=testRunner.testCase.testSuite.project.testSuites['Administration'].testCases['manageProperties'];
		if(testStepRef==null){
			log.warn "getBdmslPropertyValue [][]	Failed to find the testStep to retrieve properties: will return null.";
			return(null);
		}
		
		// Set property name in project properties
		testRunner.testCase.testSuite.project.setPropertyValue("propertyKey",propertyName);
		
		// Run testStep to retrieve the property value
		try{
			testStepRef.testSteps['getProperty'].run(testRunner, context);
		}
		catch (Exception ex){
            log.warn "Exception occured while trying to run the \"GetPropertyRequest\" request: " + ex;
			log.warn "getBdmslPropertyValue [][]	Failed to run the \"GetPropertyRequest\" request: will return null.";
			return(null);
        }
		
		// Retrieve the response
		try{
			testStepResponse=testStepRef.testSteps['getProperty'].testRequest.response.responseContent;
		}
		catch (Exception ex){
            log.warn "Exception occured while trying to retrieve the \"GetPropertyRequest\" response: " + ex;
			log.warn "getBdmslPropertyValue [][]	Failed to retrieve the \"GetPropertyRequest\" response: will return null.";
			return(null);
        }
		
		if(testStepResponse==null){
			log.warn "getBdmslPropertyValue [][]	Failed to retrieve the testStep response: will return null.";
			return(null);
		}
		
		// Extract the property value from the response
		try{
			respContent=new XmlSlurper().parseText(testStepResponse);
			allNodes=respContent.depthFirst().each{
				if(it.name()== "Key"){
					respPropName=it.text();
				}
				if(it.name()== "Value"){
					respPropvalue=it.text();
				}
			}
		}            
		catch (Exception ex){
            log.warn "Exception occured while parsing the getProperty response: " + ex;
			log.warn "getBdmslPropertyValue [][]	Failed to retrieve the \"GetPropertyRequest\" response: will return null.";
			return(null);
        }
		debugLog("getBdmslPropertyValue [][]	Rterievd data: respPropName=\"$respPropName\" == respPropvalue=\"$respPropvalue\".",log);
		if((respPropName==null)||(respPropvalue==null)){
			log.warn "getBdmslPropertyValue [][]	Failed to retrieve the \"GetPropertyRequest\" response parameters: will return null.";
			return(null);
		}
		if(respPropName.trim().toLowerCase().equals(propertyName.trim().toLowerCase())){
			log.info "getBdmslPropertyValue [][]	Property \"$respPropName\" has value \"$respPropvalue\".";
			return(respPropvalue);
		}
		log.warn "getBdmslPropertyValue [][]	Failed to retrieve the value of the property \"$propertyName\": will return null.";
		return(null);
	}
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
	// Set the certificates headers and values
	def static setCertificatesHeaders(testRunner,context,log,tstoreCertMap,clientCertMap){
		def String certHeaderName="SSLClientCert";
		def selectedMap=tstoreCertMap;
		def useAuthLegacy=false;
		def authLegacyValue=null;
		def forceAuthLegacy=null;
		
		authLegacyValue=getBdmslPropertyValue(testRunner,context,log,"authorization.domain.legacy.enabled").toLowerCase();
		forceAuthLegacy=testRunner.testCase.testSuite.project.getPropertyValue("forceAuthorizationLegacy").toLowerCase();
		
		if( (authLegacyValue.equals("true")) && (forceAuthLegacy.equals("true")) ){
			certHeaderName="Client-Cert";
			selectedMap=clientCertMap;
		}
		
		// Set the authorization header name as custom properties (testcase level)
		testRunner.testCase.setPropertyValue("headerName",certHeaderName);
		
		// Set the authorization certificate values as custom properties (testcase level)
		selectedMap.each { entry ->
			testRunner.testCase.setPropertyValue(entry.key,entry.value);
		}
		log.info "setCertificatesHeaders [][]	Certificate authorization headers set as custom properties.";	
	}
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
	def checkSubdomainConfigStatus(subdomainMax){
		debugLog("======== Call checkSubdomainConfigStatus",log);
		log.info "******** Full configuration should be done for subdomains with ids from 1 to "+(subdomainMax-1);
		if(testDatabase.toLowerCase()=="true"){
			def i=1; def propertyName=null; def found=0; def connOpenHere=false;
			if(sql==null){
				openConnection();
				connOpenHere=true;
			}
			for(i=1;i<subdomainMax;i++){
				log.info("==== Checking existence of subdomain "+i);
				try{
					sql.eachRow("SELECT COUNT(*) lignes FROM bdmsl_subdomain WHERE subdomain_id = ${i}"){
						found=it.lignes;
					}
				}
				catch (SQLException ex){
					if(connOpenHere){
						closeConnection();
					}
					assert 0,"SQLException occured: " + ex;
				}
				assert(found==1),"Error: subdomain with id "+i+" was not found. Please fully configure subdomains with ids from 1 to "+(subdomainMax-1);
				found=0;

				/* from 4.0 domain properties are in subdomain table!
				log.info("---- Checking property dns_client_record_types for subdomain "+i);
				getConfigProperty(i,"record_type");
				log.info("---- Checking property dns_client_domain for subdomain "+i);
				getConfigProperty(i,"client_domain");
				log.info("---- Checking property participant_id_regex for subdomain "+i);
				getConfigProperty(i,"participant_id_regex");
				log.info("---- Checking property smp_logical_address_protocol for subdomain "+i);
				getConfigProperty(i,"smp_address_protocol");
				*/
				sql.eachRow("SELECT * FROM bdmsl_subdomain WHERE subdomain_id = ${i}"){
					assert(it.subdomain_name!=null),  "Error: property [subdomain_name] not found for subdomain "+${i};
					assert(it.dns_record_types!=null),  "Error: property [dns_record_types] not found for subdomain "+${i};
					assert(it.dns_zone!=null),  "Error: property [dns_zone] not found for subdomain "+${i};
					assert(it.participant_id_regexp!=null),  "Error: property [participant_id_regexp] not found for subdomain "+${i};
					assert(it.smp_url_schemas!=null),  "Error: property [smp_url_schemas] not found for subdomain "+${i};
				}
			}
			if(connOpenHere){
				closeConnection();
			}
		}
		debugLog("= End checkSubdomainConfigStatus",log);
	}
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
	def returnSubdomainColumnName(subdomainId,propertyType){
		debugLog("======== Call returnSubdomainColumnName",log);
		def propertyName=null;
		switch(propertyType.toLowerCase()){
			case "record_type":
				propertyName="dns_record_types";
				break;
			case "client_domain":
				propertyName="subdomain_name";
				break;
			case "participant_id_regex":
				propertyName="participant_id_regexp";
				break;
			case "smp_address_protocol":
				propertyName="smp_url_schemas";
				break;
			default:
				assert(0),"Error in returnSubdomainColumnName: Can't find the property related to "+propertyType;
				break;
		}
		assert(propertyName!=null),"Error in returnSubdomainColumnName: Property related to "+propertyType+" was not set";
		debugLog("= End returnSubdomainColumnName",log);
		return(propertyName);
	}
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII	
	// better name would be setSubdomainValue - but for back-compatibility it stays as it is
	def setConfigProperty(subdomainId,propertyType,propertyValue){
		debugLog("======== Call setConfigProperty",log);
		if(testDatabase.toLowerCase()=="true"){
			def propertyName=null;
			def propertyValueFormatted=null;
			propertyName=returnSubdomainColumnName(subdomainId,propertyType);

			log.info "UPDATE bdmsl_subdomain set ${propertyName}='${propertyValue}' WHERE subdomain_id = '${subdomainId}'";
			if(driver.contains("oracle")){
				propertyValueFormatted=propertyValue;
			}
			else{
				propertyValueFormatted=propertyValue.replaceAll("\\\\","\\\\\\\\");
			}
			runSql("UPDATE bdmsl_subdomain set ${propertyName}='${propertyValueFormatted}' WHERE subdomain_id = '${subdomainId}'");
			//insertConfigProperty(propertyName,propertyValue);
		}
		debugLog("= End setConfigProperty",log);
	}
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII	
	// better name would be getSubdomainValue - but for back-compatibility it stays as it is
	def getConfigProperty(subdomainId,propertyType){
		debugLog("======== Call getConfigProperty",log);
		def propertyName=null; def connOpenHere=false; def propertyValue=null;
		if(testDatabase.toLowerCase()=="true"){
			propertyName=returnSubdomainColumnName(subdomainId,propertyType);
			if(sql==null){
				openConnection();
				connOpenHere=true;
			}
			try{
				sql.eachRow("SELECT "+ propertyName+" as value FROM bdmsl_subdomain WHERE subdomain_id = ${subdomainId}"){
					propertyValue=it.value.toString();
				}
			}
			catch (SQLException ex){
				if(connOpenHere){
					closeConnection();
				}
				assert 0,"SQLException occured: " + ex;
			}
			if(connOpenHere){
				closeConnection();
			}
			assert(propertyValue!=null),"Error: property "+propertyName+" not found for subdomain "+subdomainId;
		}
		debugLog("= End getConfigProperty",log);
		log.info "return propert $propertyName with walue $propertyValue for subdomain $subdomainId";
		return(propertyValue);
	}
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII	
	def returnSubdomainName(subdomainId){
		debugLog("======== Call returnSubdomainName",log);
		def connOpenHere=false; def subdomainName=null;
		if(testDatabase.toLowerCase()=="true"){
			if(sql==null){
				openConnection();
				connOpenHere=true;
			}
			try{
				sql.eachRow("SELECT subdomain_name FROM bdmsl_subdomain WHERE subdomain_id = ${subdomainId}"){
					subdomainName=it.subdomain_name;
				}
			}
			catch (SQLException ex){
				if(connOpenHere){
					closeConnection();
				}
				assert 0,"SQLException occured: " + ex;
			}				
			if(connOpenHere){
				closeConnection();
			}
			assert(subdomainName!=null),"Error: no subdomain found for subdomainId: "+subdomainId;
		}
		debugLog("= End returnSubdomainName",log);
		return(subdomainName);
	}
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII	
	static def loadSubDomainsProp(testRunner, log){
		log.info("======== Call loadSubDomainsProp");
		def i=1; def propTable=[]; def propRow = null;
		while(i!=0){
			try{
				propRow = testRunner.testCase.testSuite.project.testSuites['Administration'].testCases['subdomainsConfiguration'].testSteps['subDomainProperties'].getPropertyValue("subDomain"+i);
			}
			catch (Exception ex)
            {
                assert 0,"Property not found: " + ex;
            }
			if((propRow!=null)&&(propRow!="")&&(propRow.length()>3)){
				propTable[i-1]=propRow.substring(1,propRow.length()-1).split(",");
				i++;
			}else{
				i = 0;
			}
			// To be safe
			if (i>=100){
				i=0;
			}
		}
		assert (propTable!=null),"Error occured when trying to load the subdomains configuration.";
		assert (propTable.size()>=4),"Error occured when trying to load the subdomains configuration.";
		return(propTable);
		log.info("= End loadSubDomainsProp.");
	}
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII	
	def insertConfigProperty(propertyName,propertyValue,description=null){
		debugLog("======== Call insertConfigProperty "+propertyName,log);
		if(testDatabase.toLowerCase()=="true"){
			def connOpenHere = false; def found = 0;
			if(sql==null){
				openConnection();
				connOpenHere=true;
			}
			sql.eachRow("Select count(*) lignes from bdmsl_configuration where property = ${propertyName}"){
				found=it.lignes;
			}
			if(found==0){
				runSql("INSERT INTO bdmsl_configuration(property,value) VALUES('${propertyName}','${propertyValue}')");
			}
			else{
				runSql("UPDATE bdmsl_configuration set value='${propertyValue}' WHERE property = '${propertyName}'");
			}
			if(connOpenHere){
				closeConnection();
			}
		}
		debugLog("= End insertConfigProperty.",log);
	}
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII	
	def applySingleSubdomainConfig(subdomainTableProp){
		debugLog("======== Call applySingleSubdomainConfig for "+subdomainTableProp[1],log);
		if(testDatabase.toLowerCase()=="true"){
			def connOpenHere = false; def propName = null; def oldSubdomainId=null;
			if(sql==null){
				openConnection();
				connOpenHere=true;
			}
		
			// Insert subdomain: overwrite in case other configuration already set
			sql.eachRow("Select subdomain_id from bdmsl_subdomain WHERE subdomain_name = ${subdomainTableProp[1]}"){
				oldSubdomainId=it.subdomain_id;
			}
			if(driver.contains("mysql")){
				runSql("SET referential_integrity false;")
			}
			runSql("DELETE FROM bdmsl_certificate_domain WHERE (fk_subdomain_id='${oldSubdomainId}')");
			runSql("DELETE FROM bdmsl_participant_identifier WHERE (fk_smp_id IN (SELECT id FROM bdmsl_smp WHERE fk_subdomain_id='${oldSubdomainId}'))");
			runSql("DELETE FROM bdmsl_smp WHERE fk_subdomain_id='${oldSubdomainId}'");
			runSql("DELETE FROM bdmsl_subdomain WHERE subdomain_name='${subdomainTableProp[1]}'");
			runSql("INSERT INTO bdmsl_subdomain(subdomain_id,subdomain_name) VALUES ('${subdomainTableProp[0]}','${subdomainTableProp[1]}')");

			if(driver.contains("mysql")){
				runSql("SET referential_integrity TRUE;")
			}

			// Insert record type configuration: overwrite in case other configuration already set
			propName = dns_client_record_types+subdomainTableProp[1];
			insertConfigProperty(propName,subdomainTableProp[2]);
		
			// Insert dns client domain: overwrite in case other configuration already set
			propName = dns_client_domain+subdomainTableProp[1];
			insertConfigProperty(propName,subdomainTableProp[3]);
		
			// Insert participant id regular expression: overwrite in case other configuration already set
			propName = participant_id_regex+subdomainTableProp[1];
			insertConfigProperty(propName,subdomainTableProp[4]);

			// Insert smp logical address protocol: overwrite in case other configuration already set
			propName = smp_logical_address_protocol+subdomainTableProp[1];
			insertConfigProperty(propName,subdomainTableProp[5]);
		
			if(connOpenHere){
				closeConnection();
			}
		}
		debugLog("== End applySingleSubdomainConfig.",log);
	}
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII	
	def applySubdomainConfiguration(subdomainsTable){
		if(testDatabase.toLowerCase()=="true"){
			debugLog("==== Call applySubdomainConfiguration.",log);
			def i = 0;
			for(i=0;i<subdomainsTable.size();i++){
				applySingleSubdomainConfig(subdomainsTable[i]);
			}
			debugLog("==== End applySubdomainConfiguration.",log);
		}
	}
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
	// Add certificate to Certificate Domain table.
	def insertDomainCert(String cert, String crlURL, Integer isRoot, Integer subDomain, Integer isAdmin=0){
		debugLog("======== Call insertDomainCert: certificate="+cert+"|crlURL="+crlURL+"|isRoot="+isRoot+"|subDomain="+subDomain,log);
		if(testDatabase.toLowerCase()=="true"){
			def connOpenHere = false;
			def foundLines=0;
			def plainFormatter = new SimpleDateFormat("yyyy-MM-dd");
			def functionDate = "STR_TO_DATE";
			def pattern = "%Y-%m-%d";
			def date = functionDate + "('" + plainFormatter.format(new Date()) + "', '" + pattern + "')";
			if (context.expand( driver ) == "oracle.jdbc.OracleDriver") {	
				functionDate = "TO_DATE";
				pattern = "YYYY-MM-DD";
				date = functionDate + "('" + plainFormatter.format(new Date()) + "', '" + pattern + "')"
			}

			if(sql==null){
				openConnection();
				connOpenHere=true;
			}
			sql.eachRow("Select count(*) lignes from bdmsl_certificate_domain where certificate = ${cert}"){
				foundLines=it.lignes;
			}
			if(foundLines==0){
				try{
					if(crlURL){
						sql.execute "insert into bdmsl_certificate_domain (certificate,crl_url,created_on, last_updated_on,is_root_ca,fk_subdomain_id,is_admin) values (${cert},${crlURL}, "+ date +", "+ date +",${isRoot},${subDomain},${isAdmin})";
					}else{
						sql.execute "insert into bdmsl_certificate_domain (certificate,created_on, last_updated_on,is_root_ca,fk_subdomain_id,is_admin) values (${cert}, "+ date +", "+ date +",${isRoot},${subDomain},${isAdmin})";
					}
				}
				catch (SQLException ex){
					closeConnection();
					assert 0,"SQLException occured: " + ex;
				}
			}
            if(connOpenHere){
				closeConnection();
			}
		}
		debugLog("==== End insertDomainCert.",log);
	}
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII	
	// Remove certificate from Certificate Domain table.
	def deleteDomainCert(String cert){
		debugLog("======== Call deleteDomainCert: certificate="+cert,log);
		if(testDatabase.toLowerCase()=="true"){
			def connOpenHere = false;
			if(sql==null){
				openConnection();
				connOpenHere=true;
			}
			try{
				sql.execute "DELETE from bdmsl_certificate_domain where certificate = ${cert}";
			}
			catch (SQLException ex){
				closeConnection();
				assert 0,"SQLException occured: " + ex;
			}
            if(connOpenHere){
				closeConnection();
			}
        }
		debugLog("==== End deleteDomainCert.",log);
	}

//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
	// Add certificate to the DB
	def addCertificateDB(Integer certID,String cert, String certDomain=null,String crlURL=null, Integer isSelfSigned){
		debugLog("======== Call addCertificateDB: certificate="+cert+"|certDomain="+certDomain+"|crlURL="+crlURL+"|isSelfSigned="+isSelfSigned,log);
		if(testDatabase.toLowerCase()=="true"){
			def connOpenHere = false;
			def String swap = null;
			def plainFormatter = new SimpleDateFormat("yyyy-MM-dd");
			def functionDate = "STR_TO_DATE";
			def pattern = "%Y-%m-%d";
			def date = functionDate + "('" + plainFormatter.format(new Date()) + "', '" + pattern + "')";
			if (context.expand( driver ) == "oracle.jdbc.OracleDriver") {	
				functionDate = "TO_DATE";
				pattern = "YYYY-MM-DD";
				date = functionDate + "('" + plainFormatter.format(new Date()) + "', '" + pattern + "')"
			}
			
			if(sql==null){
				openConnection();
				connOpenHere=true;
			}
			sql.execute "DELETE from bdmsl_certificate where certificate_id = ${cert}";
			try{
                sql.execute "insert into bdmsl_certificate(id,certificate_id, valid_from, valid_until, created_on, last_updated_on) values (${certID},${cert}, " + functionDate + "('2015-01-01', '"+ pattern +"'), " + functionDate + "('2045-01-01', '"+ pattern +"'), "+ date +", "+ date +")";
            }
            catch (SQLException ex){	
				closeConnection();
                assert 0,"SQLException occured: " + ex;
            }
			if(isSelfSigned==1){
				swap = cert.split(":")[0];
				cert=null; cert=swap;
				sql.execute "DELETE from bdmsl_certificate_domain where certificate = ${cert}";
				sql.execute "insert into bdmsl_certificate_domain (certificate,domain,crl_url,created_on, last_updated_on,is_root_ca) values (${cert},${certDomain},${crlURL}, "+ date +", "+ date +",0)";
			}
            if(connOpenHere){
				closeConnection();
			}
		}
		debugLog("==== End addCertificateDB.",log);
	}
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
	// Add out dated certificate to the DB
	def addOutDatedCertificateDB(Integer certID,String cert, String certDomain=null,String crlURL=null, Integer isSelfSigned){
		debugLog("======== Call addOutDatedCertificateDB: certificate="+cert+"|certDomain="+certDomain+"|crlURL="+crlURL+"|isSelfSigned="+isSelfSigned,log);
		if(testDatabase.toLowerCase()=="true"){
			def connOpenHere = false;
			def String swap = null;
			def plainFormatter = new SimpleDateFormat("yyyy-MM-dd");
			def functionDate = "STR_TO_DATE";
			def pattern = "%Y-%m-%d";
			def date = functionDate + "('" + plainFormatter.format(new Date()) + "', '" + pattern + "')";
			if (context.expand( driver ) == "oracle.jdbc.OracleDriver") {	
				functionDate = "TO_DATE";
				pattern = "YYYY-MM-DD";
				date = functionDate + "('" + plainFormatter.format(new Date()) + "', '" + pattern + "')"
			}

			if(sql==null){
				openConnection();
				connOpenHere=true;
			}
			sql.execute "DELETE from bdmsl_certificate where certificate_id = ${cert}";
			try{
                sql.execute "insert into bdmsl_certificate(id,certificate_id, valid_from, valid_until, created_on, last_updated_on) values (${certID},${cert}, " + functionDate + "('2014-01-01', '"+ pattern +"'), " + functionDate + "('2015-01-01', '"+ pattern +"'), "+ date +", "+ date +")";
            }
            catch (SQLException ex)
            {
                assert 0,"SQLException occured: " + ex;
            }
			if(isSelfSigned==1){
				swap = cert.split(":")[0];
				cert=null; cert=swap;
				sql.execute "DELETE from bdmsl_certificate_domain where certificate = ${cert}";
				sql.execute "insert into bdmsl_certificate_domain (certificate,domain,crl_url,created_on, last_updated_on,is_root_ca) values (${cert},${certDomain},${crlURL}, "+ date +", "+ date +",0)";
			}
            if(connOpenHere){
				closeConnection();
			}
		}
		debugLog("==== End addOutDatedCertificateDB.",log);
	}
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII	
	// Delete certificate from the database
	def deleteCertificateDB(String cert,Integer isSelfSigned){
		debugLog("======== Call deleteCertificateDB: certificate="+cert+"|isSelfSigned="+isSelfSigned,log);
		if(testDatabase.toLowerCase()=="true"){
			def connOpenHere = false;
			def Integer linked =0;
			def Integer idCertif =0;
			def String swap = null;
			if(sql==null){
				openConnection();
				connOpenHere=true;
			}
			sql.eachRow("Select id from bdmsl_certificate where certificate_id = ?",[cert]){
				idCertif=it.id;
			}

			sql.execute "DELETE from bdmsl_certificate where new_cert_id = ?",[idCertif];
		
			sql.eachRow("Select count(*) lignes from bdmsl_smp where fk_certificate_id = ?",[idCertif]){
				linked=it.lignes;
			}
			assert (linked==0),"Certificate "+cert+" is linked to an smp."
       
			sql.execute "DELETE from bdmsl_certificate where certificate_id = ?",[cert];
			if(isSelfSigned==1){
				swap = cert.split(":")[0];
				cert=null; cert=swap;
				sql.execute "DELETE from bdmsl_certificate_domain where certificate = ?",[cert];
			}
            if(connOpenHere){
				closeConnection();
			}
		}
		debugLog("==== End deleteCertificateDB.",log);
	}
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
	def checkCertificateLinked(String smpID, String cert){
		debugLog("======== Call checkCertificateLinked: certificate="+cert+"|smpID="+smpID,log);
		if(testDatabase.toLowerCase()=="true"){
			def connOpenHere = false;
			def Integer indexCert=0; def Integer matchFound=0;
            if(sql==null){
				openConnection();
				connOpenHere=true;
			}
			sql.eachRow("Select id from bdmsl_certificate where certificate_id = ?",[cert]){
                indexCert=it.id;
            }
			sql.eachRow("Select count(*) lignes from bdmsl_smp where fk_certificate_id = ? AND smp_id = ?",[indexCert,smpID]){
                matchFound=it.lignes;
            }			
			assert (matchFound == 1),locateTest()+"Error:Exec: The smp ${smpID} is not linked to certificate "+cert+".";
            if(connOpenHere){
				closeConnection();
			}
        }
		debugLog("==== End checkCertificateLinked.",log);
	}
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII	
	def checkChangeCertificateScheduled(String currentCert, String newCert,String state){
		debugLog("======== Call checkChangeCertificateScheduled: currentCert="+currentCert+"|newCert="+newCert+"|state="+state,log);
		def connOpenHere = false;
		if(testDatabase.toLowerCase()=="true"){
			def Integer indexCert=0; def Integer matchFound=0;
			if(sql==null){
				openConnection();
				connOpenHere=true;
			}
			if(state.toLowerCase()=="ok"){
				sql.eachRow("Select id from bdmsl_certificate where certificate_id = ${newCert}"){
					indexCert=it.id;
				}
				assert (indexCert != 0),locateTest()+"Error:Exec: New certificate is not present in the database.";
				sql.eachRow("Select count(*) lignes from bdmsl_certificate where (certificate_id = ${currentCert}) AND (new_cert_id = ${indexCert})"){
					matchFound=it.lignes;
				}			
				assert (matchFound == 1),locateTest()+"Error:Exec: Certificate change is not scheduled";
			}
			if(state.toLowerCase()=="ko"){
				sql.eachRow("Select id from bdmsl_certificate where certificate_id = ${newCert}"){
					indexCert=it.id;
				}
				assert (indexCert == 0),locateTest()+"Error:Exec: New certificate is present in the database.";
				sql.eachRow("Select new_cert_id from bdmsl_certificate where (certificate_id = ${currentCert})"){
					matchFound=it.new_cert_id;
				}			
				assert (!matchFound),locateTest()+"Error:Exec: Certificate change is scheduled";
			}
			if(connOpenHere){
				closeConnection();
			}
        }
		debugLog("==== End checkChangeCertificateScheduled.",log);
	}
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
    // Prepare Database
    def prepareDatabase(String smpidd1, String smpidd2, String smpidd3,String smpidd4,String partidd){
        if(testDatabase.toLowerCase()=="true"){
			def connOpenHere = false;
            def smpid1 = smpidd1.toLowerCase();
            def smpid2 = smpidd2.toLowerCase();
            def smpid3 = smpidd3.toLowerCase();
            def smpid4 = smpidd4.toLowerCase();
            def partid = partidd.toLowerCase();
			if(sql==null){
				openConnection();
				connOpenHere=true;
			}
            if(partid != ""){
                sql.execute "DELETE from bdmsl_migrate where LOWER(participant_id) = LOWER(${partid})";
            }
            if((smpid1 != "") || (smpid2 != "") || (smpid3 != "") || (smpid4 != "")){
                sql.execute "DELETE FROM bdmsl_participant_identifier WHERE  (LOWER(fk_smp_id) = ${smpid1}) or (LOWER(fk_smp_id) = ${smpid2}) or (LOWER(fk_smp_id) = ${smpid3}) or (LOWER(fk_smp_id) = ${smpid4})";
                sql.execute "DELETE FROM bdmsl_smp WHERE (LOWER(smp_id) = ${smpid1}) or (LOWER(smp_id) = ${smpid2}) or (LOWER(smp_id) = ${smpid3}) or (LOWER(smp_id) = ${smpid4})";
            }
			if(connOpenHere){
				closeConnection();
			}
        }
    }
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
    // Clean all the BDMSLconcerned tables.
    def cleanDBAll(){
        if(testDatabase.toLowerCase()=="true"){
            openConnection();

            // BDMSL_MIGRATE
            sql.execute "DELETE from bdmsl_migrate where (LOWER(new_smp_id) LIKE LOWER('testsmpCreate%')) or (LOWER(old_smp_id) LIKE LOWER('testsmpCreate%'))";
            sql.execute "DELETE from bdmsl_migrate where (LOWER(new_smp_id) LIKE LOWER('testsmpDelete%')) or (LOWER(old_smp_id) LIKE LOWER('testsmpDelete%'))";
            sql.execute "DELETE from bdmsl_migrate where (LOWER(new_smp_id) LIKE LOWER('testsmpPrepareToMigrate%')) or (LOWER(old_smp_id) LIKE LOWER('testsmpPrepareToMigrate%'))";
            sql.execute "DELETE from bdmsl_migrate where (LOWER(new_smp_id) LIKE LOWER('testsmpMigrate%')) or (LOWER(old_smp_id) LIKE LOWER('testsmpMigrate%'))";
            sql.execute "DELETE from bdmsl_migrate where (LOWER(new_smp_id) LIKE LOWER('testsmpList%')) or (LOWER(old_smp_id) LIKE LOWER('testsmpList%'))";
            sql.execute "DELETE from bdmsl_migrate where (LOWER(new_smp_id) LIKE LOWER('testsmpGhost%')) or (LOWER(old_smp_id) LIKE LOWER('testsmpGhost%'))";
            sql.execute "DELETE from bdmsl_migrate where (LOWER(new_smp_id) LIKE LOWER('testsmpRead%')) or (LOWER(old_smp_id) LIKE LOWER('testsmpRead%'))";
            sql.execute "DELETE from bdmsl_migrate where (LOWER(new_smp_id) LIKE LOWER('testsmpUpdate%')) or (LOWER(old_smp_id) LIKE LOWER('testsmpUpdate%'))";
            sql.execute "DELETE from bdmsl_migrate where (LOWER(new_smp_id) LIKE LOWER('testsmpLoad%')) or (LOWER(old_smp_id) LIKE LOWER('testsmpLoad%'))";

            // BDMSL_PARTICIPANT_IDENTIFIER
            sql.execute "DELETE from bdmsl_participant_identifier where fk_smp_id in (SELECT id from bdmsl_smp WHERE LOWER(smp_id) LIKE LOWER('testsmpCreate%') )";
			sql.execute "DELETE from bdmsl_participant_identifier where fk_smp_id in (SELECT id from bdmsl_smp WHERE LOWER(smp_id) LIKE LOWER('testsmpDelete%') )";
			sql.execute "DELETE from bdmsl_participant_identifier where fk_smp_id in (SELECT id from bdmsl_smp WHERE LOWER(smp_id) LIKE LOWER('testsmpPrepareToMigrate%') )";
			sql.execute "DELETE from bdmsl_participant_identifier where fk_smp_id in (SELECT id from bdmsl_smp WHERE LOWER(smp_id) LIKE LOWER('testsmpMigrate%') )";
			sql.execute "DELETE from bdmsl_participant_identifier where fk_smp_id in (SELECT id from bdmsl_smp WHERE LOWER(smp_id) LIKE LOWER('testsmpGhost%') )";
			sql.execute "DELETE from bdmsl_participant_identifier where fk_smp_id in (SELECT id from bdmsl_smp WHERE LOWER(smp_id) LIKE LOWER('testsmpRead%') )";
			sql.execute "DELETE from bdmsl_participant_identifier where fk_smp_id in (SELECT id from bdmsl_smp WHERE LOWER(smp_id) LIKE LOWER('testsmpUpdate%') )";
			sql.execute "DELETE from bdmsl_participant_identifier where fk_smp_id in (SELECT id from bdmsl_smp WHERE LOWER(smp_id) LIKE LOWER('testsmpLoad%') )";


            // BDMSL_SMP
            sql.execute "DELETE from bdmsl_smp where LOWER(smp_id) LIKE LOWER('testsmpCreate%')";
            sql.execute "DELETE from bdmsl_smp where LOWER(smp_id) LIKE LOWER('testsmpDelete%')";
            sql.execute "DELETE from bdmsl_smp where LOWER(smp_id) LIKE LOWER('testsmpPrepareToMigrate%')";
            sql.execute "DELETE from bdmsl_smp where LOWER(smp_id) LIKE LOWER('testsmpMigrate%')";
            sql.execute "DELETE from bdmsl_smp where LOWER(smp_id) LIKE LOWER('testsmpList%')";
            sql.execute "DELETE from bdmsl_smp where LOWER(smp_id) LIKE LOWER('testsmpGhost%')";
            sql.execute "DELETE from bdmsl_smp where LOWER(smp_id) LIKE LOWER('testsmpRead%')";
            sql.execute "DELETE from bdmsl_smp where LOWER(smp_id) LIKE LOWER('testsmpUpdate%')";
            sql.execute "DELETE from bdmsl_smp where LOWER(smp_id) LIKE LOWER('testsmpLoad%')";
            sql.execute "DELETE from bdmsl_smp where LOWER(smp_id) LIKE LOWER('smpChangeCertSML%')";


            closeConnection();
            log.info "DONE.";
        }
    }
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
    // Extract request parameters
    def extractRequestParameters(String testType){
		debugLog("================ Call extractRequestParameters: testType="+testType,log);
		def allNodes = null;
        def listSize=0;
        def cursor = 1;
        def requestContent = messageExchange.getRequestContentAsXml() ;
        def requestFile = new XmlSlurper().parseText(requestContent);
        switch(testType.toUpperCase()){
            case "PARTICIPANTIDENTIFIER":
            // Initialize parameters
                allNodes = requestFile.depthFirst().each{
					if(it.name()== "ParticipantIdentifier"){
						listSize++;
					}
				}
                InitializeParameters("PARTICIPANTIDENTIFIER","Request",listSize);
                allNodes = requestFile.depthFirst().each{
                    if(it.name()== "ServiceMetadataPublisherID"){
                        requestParamTable[0][0]=it.text().trim().toLowerCase();
                    }
                    if(it.name()== "ParticipantIdentifier"){
                        requestParamTable[cursor][0]=it.@scheme.text().trim();
                        requestParamTable[cursor+1][0]=it.text().trim().toLowerCase();
                        cursor=cursor+2;
                    }
                }
                assertOperationsParameters("Request");
                break;
            case "PARTICIPANTIDENTIFIERSP":
            // Initialize parameters
                allNodes = requestFile.depthFirst().each{
                    if(it.name()== "ParticipantIdentifier"){
                        listSize++;
                    }
                }
                InitializeParameters("PARTICIPANTIDENTIFIER","Request",listSize);
                allNodes = requestFile.depthFirst().each{
					if(it.name()== "ServiceMetadataPublisherID"){
                        requestParamTable[0][0]=it.text().trim().toLowerCase();
                    }
                    if(it.name()== "ParticipantIdentifier"){
                        requestParamTable[cursor][0]=it.@schem.text().trim();
                        requestParamTable[cursor+1][0]=it.text().trim().toLowerCase();
                        cursor=cursor+2;
                    }
                }
                assertOperationsParameters("Request");
                break;
            case "PREPAREMIGRATIONRECORD":
            // Initialize parameters
                InitializeParameters("PREPAREMIGRATIONRECORD","Request",0);
                allNodes = requestFile.depthFirst().each{
                    if(it.name()== "ServiceMetadataPublisherID"){
                        requestParamTable[0][0]=it.text().trim().toLowerCase();
                    }
                    if(it.name()== "ParticipantIdentifier"){
                        requestParamTable[1][0]=it.@scheme.text().trim();
                        requestParamTable[2][0]=it.text().trim().toLowerCase();
                    }
                    if(it.name()== "MigrationKey"){
                        requestParamTable[3][0]=it.text().trim();
                    }
                }
                assertOperationsParameters("Request");
                break;
            case "METADATAUPDATE":
            case "METADATACREATE":
            // Initialize parameters
                InitializeParameters("METADATACREATE","Request",0);
                allNodes = requestFile.depthFirst().each{
                    if(it.name()== "ServiceMetadataPublisherID"){
                        requestParamTable[0][0]=it.text().trim().toLowerCase();
                    }
                    if(it.name()== "LogicalAddress"){
                        requestParamTable[1][0]=it.text().trim();
                    }
                    if(it.name()== "PhysicalAddress"){
                        requestParamTable[2][0]=it.text().trim();
                    }
                }
                assertOperationsParameters("Request");
                break;
            case "METADATADELETE":
            case "METADATAREAD":
                // Initialize parameters
                InitializeParameters("METADATAREAD","Request",0);
                allNodes = requestFile.depthFirst().each{
                    if(it.name()== "ServiceMetadataPublisherID"){
                        requestParamTable[0][0]=it.text().trim().toLowerCase();
                    }
                }
                assertOperationsParameters("Request");
                break;
            case "LIST":
            // Initialize parameters
                InitializeParameters("LIST","Request",0);
                allNodes = requestFile.depthFirst().each{
                    if(it.name()== "ServiceMetadataPublisherID"){
						requestParamTable[0][0]=it.text().trim().toLowerCase();
                    }
                    if(it.name()== "NextPageIdentifier"){
                        requestParamTable[1][0]=it.text().trim().toInteger();
                    }
                }
                //assertOperationsParameters("Request");
                break;
            default:
                log.info "Unknown operation";
        }
		debugLog("================ End extractRequestParameters.",log);
    }
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
    // Extract response parameters
    def extractResponseParameters(String testType){
		debugLog("================ Call extractResponseParameters: testType="+testType,log);
        def responseContent = messageExchange.getResponseContentAsXml() ;
        def requestFile = new XmlSlurper().parseText(responseContent);
        def listSize = 0;
        def counter = 0;
        switch(testType.toUpperCase()){
            case "METADATAREAD":
                InitializeParameters("METADATAREAD","Response",0);
                def allNodes = requestFile.depthFirst().each{
                    if(it.name()== "ServiceMetadataPublisherID"){
                        responseParamTable[0][0]=it.text().toLowerCase();
                    }
                    if(it.name()== "LogicalAddress"){
                        responseParamTable[1][0]=it.text();
                    }
                    if(it.name()== "PhysicalAddress"){
                        responseParamTable[2][0]=it.text();
                    }
                }
                assertOperationsParameters("Response");
                break;
            case "PARTICIPANTSLIST":
                InitializeParameters("PARTICIPANTSLIST","Response",0);
                def allNodes = requestFile.depthFirst().each{
                    if(it.name()== "ParticipantIdentifier"){
                        responseParamTable[counter]=["0","ParticipantIdentifierSchemeResponse"];
                        responseParamTable[counter+1]=["0","ParticipantIdentifierResponse"];
                        responseParamTable[counter+2]=["0","ServiceMetadataPublisherID"];
                        responseParamTable[counter][0]=it.@scheme.text();
                        responseParamTable[counter+1][0]=it.text();
                    }
                    if(it.name()== "ServiceMetadataPublisherID"){
                        responseParamTable[counter+2][0]=it.text().toLowerCase();
                        counter = counter + 3;
                    }
                }
                //assertOperationsParameters("Response");
                break;
            case "LIST":
                def allNodes = requestFile.depthFirst().each{
                    if(it.name()== "ParticipantIdentifier"){
                        listSize++;
                    }
                }
                InitializeParameters("LIST","Response",listSize);
                counter = 2;
                allNodes = requestFile.depthFirst().each{
                    if(it.name()== "ParticipantIdentifier"){
                        responseParamTable[counter][0]=it.@scheme.text();
                        responseParamTable[counter+1][0]=it.text().toLowerCase();
                        counter = counter + 2;
                    }
                    if(it.name()== "ServiceMetadataPublisherID"){
                        responseParamTable[0][0]=it.text().toLowerCase();
                    }
                    if(it.name()== "NextPageIdentifier"){
                        responseParamTable[1][0]=it.text().toInteger();
                    }
                }
                //assertOperationsParameters("Response");
                break;
            case "SERVICEMETADATA":
                break;
            default:
                log.info "Unknown operation";
        }
		debugLog("================ End extractResponseParameters.",log);
    }
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
    // Verification
    def verification(String testType, String logicalAddressRaw=null){
		debugLog("================ Call verification: testType="+testType+"|logicalAddress="+logicalAddressRaw,log);
		def migrationKeyDump=null;
        def pageSize = 0;
        def startPage = 1;
        def endPage = 1;
        def nbre = 1;
        def recordNumber = 0;
        def now = new Date();
        def toDay = null;
        def dateMigration = null;
        def TimeStampMigration = null;
		def logicalAddress=null;
		if(logicalAddressRaw != null){
			logicalAddress=logicalAddressRaw.trim();
		}
        openConnection();
        switch(testType.toUpperCase()){
            case "PARTICIPANTIDENTIFIER":
				if(testDatabase.toLowerCase()=="true"){
					sql.eachRow("Select count(*) as lignes from bdmsl_smp where LOWER(smp_id) = ${requestParamTable[0][0]}"){
						checker=it.lignes;
					}
					assert (checker == 1),locateTest()+"Error:Exec: The smp ${requestParamTable[0][0]} mentioned in the request does not exist";
				}
                checker = 0;
                while(nbre<requestParamTable.size()){
					debugLog("testType = PARTICIPANTIDENTIFIER. checking participant identifier: "+requestParamTable[nbre+1][0],log);
					if(testDatabase.toLowerCase()=="true"){
						sql.eachRow("Select count(*) as lignes from bdmsl_participant_identifier pi INNER JOIN bdmsl_smp smp on pi.fk_smp_id = smp.id  where LOWER(smp.smp_id)=${requestParamTable[0][0]} and pi.scheme=${requestParamTable[nbre][0]} and LOWER(pi.participant_id)=${requestParamTable[nbre+1][0]}"){
							checker=it.lignes;
						}
						assert (checker == 1),locateTest()+"Error:Exec: The participant identifier ${requestParamTable[nbre+1][0]} is not present";
					}
					dnsSubDomain=getSubDomain(requestParamTable[nbre+1][0]);
					debugLog("METADATACREATE::requestParamTable[nbre+1][0]: "+requestParamTable[nbre+1][0],log);
					debugLog("PARTICIPANTIDENTIFIER::dnsSubDomain: "+dnsSubDomain,log);
                    checkDNSList("PARTICIPANTIDENTIFIER",requestParamTable[0][0],requestParamTable[nbre][0],requestParamTable[nbre+1][0],logicalAddress);
                    checker = 0;
                    nbre=nbre+2;
                }
                break;
            case "PARTICIPANTIDENTIFIERONLY":
                checker = 0;
				if(testDatabase.toLowerCase()=="true"){
					while(nbre<requestParamTable.size()){
						sql.eachRow("Select count(*) as lignes from bdmsl_participant_identifier where scheme=${requestParamTable[nbre][0]} and LOWER(participant_id)=${requestParamTable[nbre+1][0]}"){
							checker=it.lignes;
						}
						assert (checker == 1),locateTest()+"Error:Exec: The participant identifier ${requestParamTable[nbre+1][0]} is not present";
						checker = 0;
						nbre=nbre+2;
					}
				}
                break;
            case "PARTICIPANTIDENTIFIERMULTI":
                checker = 0;
				if(testDatabase.toLowerCase()=="true"){
					//log.info "Select count(*) as lignes from bdmsl_participant_identifier where scheme=${requestParamTable[1][0]} and LOWER(participant_id)=${requestParamTable[2][0]}";
					sql.eachRow("Select count(*) as lignes from bdmsl_participant_identifier where scheme=${requestParamTable[1][0]} and LOWER(participant_id)=${requestParamTable[2][0]}"){
						checker=it.lignes;
					}
					assert (checker == 1),locateTest()+"Error:Exec: The participant identifier ${requestParamTable[2][0]} must be present only once (only associated to one smp).";
					sql.eachRow("SELECT * FROM bdmsl_smp WHERE id IN (Select fk_smp_id from bdmsl_participant_identifier where scheme=${requestParamTable[1][0]} and LOWER(participant_id)=${requestParamTable[2][0]})"){
						assert (requestParamTable[0][0] != it.smp_id.toLowerCase()),locateTest()+"Error:Exec: The previous participant identifier entry is overruled).";
					}
				}
				dnsSubDomain=getSubDomain(requestParamTable[2][0]);
                checkDNSList("PARTICIPANTIDENTIFIERMULTI",requestParamTable[0][0],requestParamTable[1][0],requestParamTable[2][0],logicalAddress);
                break;
            case "PARTICIPANTIDENTIFIERNOTFOUND":
                checker = 0;
                while(nbre<requestParamTable.size()){
					if(testDatabase.toLowerCase()=="true"){
						sql.eachRow("Select count(*) lignes from bdmsl_participant_identifier where scheme=${requestParamTable[nbre][0]} and LOWER(participant_id)=${requestParamTable[nbre+1][0]}"){
							checker=it.lignes;
						}
						assert (checker == 0),locateTest()+"Error:Exec: The participant identifier ${requestParamTable[nbre+1][0]} is present.";
					}
					dnsSubDomain=getSubDomain(requestParamTable[nbre+1][0]);
                    checkDNSList("PARTICIPANTIDENTIFIERNOTFOUND",requestParamTable[0][0],requestParamTable[nbre][0],requestParamTable[nbre+1][0],logicalAddress);
                    nbre=nbre+2;
                }
                break;
            case "PREPAREMIGRATIONRECORD":
				if(testDatabase.toLowerCase()=="true"){
					checker = 0;
					sql.eachRow("select count(*) lignes from bdmsl_migrate where migrated = 0 and LOWER(old_smp_id) = ${requestParamTable[0][0]} and LOWER(participant_id) = ${requestParamTable[2][0]} and MIGRATION_KEY = ${requestParamTable[3][0]}"){
						checker=it.lignes;
					}
					assert (checker == 1),locateTest()+"Error:Exec: The request from the smp ${requestParamTable[0][0]} to Migrate the participant identifier ${requestParamTable[2][0]} is not present in the MIGRATE table.";
					if (DEFAULT_LOG_LEVEL.toString()=="1" || DEFAULT_LOG_LEVEL.toString() == "true"){
						sql.eachRow("select * from bdmsl_migrate where migrated = 0 and LOWER(old_smp_id) = ${requestParamTable[0][0]} and LOWER(participant_id) = ${requestParamTable[2][0]}"){
							migrationKeyDump=it.MIGRATION_KEY;
						}
						log.info "Migration key stored for participant "+requestParamTable[1][0]+"::"+requestParamTable[2][0]+" is --"+migrationKeyDump+"--";
					}
				}
                break;
            case "PREPAREMIGRATIONRECORDNOTFOUND":
				if(testDatabase.toLowerCase()=="true"){
					checker = 0;
					sql.eachRow("select count(*) lignes from bdmsl_migrate where LOWER(old_smp_id) = ${requestParamTable[0][0]} and LOWER(participant_id) = ${requestParamTable[2][0]} and MIGRATION_KEY = ${requestParamTable[3][0]}"){
						checker=it.lignes;
					}
					assert (checker == 0),locateTest()+"Error:Exec: The request from the smp ${requestParamTable[0][0]} to Migrate the participant identifier ${requestParamTable[2][0]} is is present in the MIGRATE table.";
				}
                break;
            case "MIGRATION":
                checker = 0;
				if(testDatabase.toLowerCase()=="true"){
					sql.eachRow("select count(*) lignes from bdmsl_migrate where migrated = 1 and LOWER(participant_id) = ${requestParamTable[2][0]} and scheme = ${requestParamTable[1][0]} and LOWER(new_smp_id) = ${requestParamTable[0][0]} and MIGRATION_KEY = ${requestParamTable[3][0]}"){
						checker=it.lignes;
					}
					assert (checker == 1),locateTest()+"Error:Exec: The participant identifier ${requestParamTable[2][0]} Migration failed.";
					sql.eachRow("select * from bdmsl_migrate where migrated = 1 and LOWER(participant_id) = ${requestParamTable[2][0]} and scheme = ${requestParamTable[1][0]} and LOWER(new_smp_id) = ${requestParamTable[0][0]} "){
						TimeStampMigration=it.last_updated_on;
					}
					if(driver.contains("oracle")){
						dateMigration = TimeStampMigration.dateValue().clearTime();
					}else{
						dateMigration = new java.sql.Date(TimeStampMigration.getTime()).clearTime();
					}
					toDay = new java.sql.Date(now.getTime()).clearTime();
					assert (toDay == dateMigration),locateTest()+"Error:Exec: The participant identifier ${requestParamTable[2][0]} Migration failed (an older one is present).";
					checker = 0;
					sql.eachRow("select count(*) as lignes from bdmsl_participant_identifier pi INNER JOIN bdmsl_smp smp on pi.fk_smp_id = smp.id  where LOWER(smp.smp_id) = ${requestParamTable[0][0]} and LOWER(pi.participant_id) = ${requestParamTable[2][0]} and pi.scheme = ${requestParamTable[1][0]}"){
						checker=it.lignes;
					}
					assert (checker == 1),locateTest()+"Error:Exec: The participant identifier ${requestParamTable[2][0]} info is not linked to the new smp.";
				}
				dnsSubDomain=getSubDomain(requestParamTable[2][0]);
                checkDNSList("MIGRATION",requestParamTable[0][0],requestParamTable[1][0],requestParamTable[2][0],logicalAddress);
                break;
            case "MIGRATIONFAIL":
				if(testDatabase.toLowerCase()=="true"){
					checker = 0;
					sql.eachRow("select count(*) lignes from bdmsl_participant_identifier where LOWER(participant_id) = ${requestParamTable[2][0]} and scheme = ${requestParamTable[1][0]} and LOWER(fk_smp_id) = ${requestParamTable[0][0]} "){
						checker=it.lignes;
					}
					assert (checker == 0),locateTest()+"Error:Exec: The participant identifier ${requestParamTable[2][0]} is linked to the new smp.";
					checker = 0;
					sql.eachRow("select count(*) lignes from bdmsl_migrate where migrated = 0 and LOWER(participant_id) = ${requestParamTable[2][0]} and scheme = ${requestParamTable[1][0]} and MIGRATION_KEY = ${requestParamTable[3][0]}"){
						checker=it.lignes;
					}
					assert (checker == 1),locateTest()+"Error:Exec: The participant identifier ${requestParamTable[2][0]} is not present in the MIGRATE table.";
				}
                break;
            case "MIGRATIONFAILSP":
				if(testDatabase.toLowerCase()=="true"){
					checker = 0;
					sql.eachRow("select count(*) lignes from bdmsl_participant_identifier where LOWER(participant_id) = ${requestParamTable[2][0]} and scheme = ${requestParamTable[1][0]} and LOWER(fk_smp_id) = ${requestParamTable[0][0]} "){
						checker=it.lignes;
					}
					assert (checker == 0),locateTest()+"Error:Exec: The participant identifier ${requestParamTable[2][0]} is linked to the new smp.";
				}
                break;
            case "METADATACREATE":
                checker = 0;
				if(testDatabase.toLowerCase()=="true"){
					sql.eachRow("select count(*) as lignes from bdmsl_smp where LOWER(smp_id) = ${requestParamTable[0][0]} "){
						checker=it.lignes;
					}
					assert (checker == 1),locateTest()+"Error:Exec: Metadata Publisher ${requestParamTable[0][0]} is not created.";
					sql.eachRow("select * from bdmsl_smp where LOWER(smp_id) = ${requestParamTable[0][0]} "){
						assert (requestParamTable[1][0] == it.endpoint_logical_address),locateTest()+"Error:Exec: Wrong Logical Address for Metadata Publisher ${requestParamTable[0][0]}.";
						assert (requestParamTable[2][0] == it.endpoint_physical_address),locateTest()+"Error:Exec: Wrong Physical Address for Metadata Publisher ${requestParamTable[0][0]}.";
					}
				}
				dnsSubDomain=getSubDomain("dummy",requestParamTable[0][0]);
				debugLog("METADATACREATE::requestParamTable[0][0]: "+requestParamTable[0][0],log);
				debugLog("METADATACREATE::dnsSubDomain: "+dnsSubDomain,log);
                checkDNSList("METADATACREATE",requestParamTable[0][0],requestParamTable[1][0], "");
                break;
            case "METADATADELETE":
                checker = 0;
				if(testDatabase.toLowerCase()=="true"){
					if(countParticipantsLinked(requestParamTable[0][0]) <= maxSMPparticipants){
						sql.eachRow("select count(*) lignes from bdmsl_smp where LOWER(smp_id) = ${requestParamTable[0][0]} "){
							checker=it.lignes;
						}
						assert (checker == 0),locateTest()+"Error:Exec: Metadata Publisher ${requestParamTable[0][0]} is present in the Database.";
						dnsSubDomain=getSubDomain("dummy",requestParamTable[0][0]);
						checkDNSList("METADATADELETE",requestParamTable[0][0],"", "",logicalAddress);
					}else{
						sql.eachRow("select count(*) as lignes from bdmsl_smp where LOWER(smp_id) = ${requestParamTable[0][0]} "){
							checker=it.lignes;
						}
						assert (checker == 1),locateTest()+"Error:Exec: Metadata Publisher ${requestParamTable[0][0]} was deleted from the database.";
						dnsSubDomain=getSubDomain("dummy",requestParamTable[0][0]);
						checkDNSList("METADATADELETEFAIL",requestParamTable[0][0],"", "",logicalAddress);
					}					
				}
                break;
            case "METADATACREATEFAIL":
                checker = 0;
				if(testDatabase.toLowerCase()=="true"){
					sql.eachRow("select count(*) lignes from bdmsl_smp where LOWER(smp_id) = ${requestParamTable[0][0]} "){
						checker=it.lignes;
					}
					assert (checker == 0),locateTest()+"Error:Exec: Metadata Publisher ${requestParamTable[0][0]} is present in the Database.";
				}
				dnsSubDomain=getSubDomain("dummy",requestParamTable[0][0]);
                checkDNSList("METADATADELETE",requestParamTable[0][0],"", "",logicalAddress);
                break;
            case "METADATADELETEFAIL":
                checker = 0;
				if(testDatabase.toLowerCase()=="true"){
					sql.eachRow("select count(*) lignes from bdmsl_smp where LOWER(smp_id) = ${requestParamTable[0][0]} "){
						checker=it.lignes;
					}
					assert (checker == 1),locateTest()+"Error:Exec: Metadata Publisher ${requestParamTable[0][0]} is not present in the Database.";
				}
				dnsSubDomain=getSubDomain("dummy",requestParamTable[0][0]);
                checkDNSList("METADATADELETEFAIL",requestParamTable[0][0],"", "",logicalAddress);
                break;
            case "METADATAREAD":
				if(testDatabase.toLowerCase()=="true"){
					checker = 0;
					sql.eachRow("select count(*) lignes from bdmsl_smp where LOWER(smp_id) = ${requestParamTable[0][0]} "){
						checker=it.lignes;
					}
					assert (checker == 1),locateTest()+"Error:Exec: Metadata Publisher ${requestParamTable[0][0]} does not exist.";
					assert (responseParamTable[0][0] == requestParamTable[0][0]),locateTest()+"Error:Exec: Metadata Publisher ${requestParamTable[0][0]} does not match with response value responseParamTable[0][0].";
					sql.eachRow("select * from bdmsl_smp where LOWER(smp_id) = ${requestParamTable[0][0]} "){
						assert (responseParamTable[1][0] == it.endpoint_logical_address),locateTest()+"Error:Exec: Wrong Logical Address for Metadata Publisher ${requestParamTable[0][0]}.";
						assert (responseParamTable[2][0] == it.endpoint_physical_address),locateTest()+"Error:Exec: Wrong Physical Address for Metadata Publisher ${requestParamTable[0][0]}.";
					}
				}
                break;
            case "METADATAUPDATE":
                checker = 0;
				if(testDatabase.toLowerCase()=="true"){					
					if(countParticipantsLinked(requestParamTable[0][0]) <= maxSMPparticipants){
						sql.eachRow("select count(*) as lignes from bdmsl_smp where LOWER(smp_id) = ${requestParamTable[0][0]} "){
							checker=it.lignes;
						}
						assert (checker == 1),locateTest()+"Error:Exec: Metadata Publisher ${requestParamTable[0][0]} is not created.";
						sql.eachRow("select * from bdmsl_smp where LOWER(smp_id) = ${requestParamTable[0][0]} "){
							assert (requestParamTable[1][0] == it.endpoint_logical_address),locateTest()+"Error:Exec: Wrong Logical Address for Metadata Publisher ${requestParamTable[0][0]}.";
							assert (requestParamTable[2][0] == it.endpoint_physical_address),locateTest()+"Error:Exec: Wrong Physical Address for Metadata Publisher ${requestParamTable[0][0]}.";
						}
						dnsSubDomain=getSubDomain("dummy",requestParamTable[0][0]);
						debugLog("METADATAUPDATE::requestParamTable[0][0]: "+requestParamTable[0][0],log);
						debugLog("METADATAUPDATE::dnsSubDomain: "+dnsSubDomain,log);
						checkDNSList("METADATACREATE",requestParamTable[0][0],requestParamTable[1][0], "");
					}else{
						sql.eachRow("select count(*) lignes from bdmsl_smp where LOWER(smp_id) = ${requestParamTable[0][0]} "){
							checker=it.lignes;
						}
						assert (checker == 1),locateTest()+"Error:Exec: Metadata Publisher ${requestParamTable[0][0]} does not exist.";
						def tempo = "";
						//assert (responseParamTable[0][0] == requestParamTable[0][0]),locateTest()+"Error:Exec: Metadata Publisher ${requestParamTable[0][0]} does not match with response value responseParamTable[0][0].";
						sql.eachRow("select * from bdmsl_smp where LOWER(smp_id) = ${requestParamTable[0][0]} "){
							assert ((requestParamTable[1][0] != it.endpoint_logical_address)||(requestParamTable[2][0] != it.endpoint_physical_address)),locateTest()+"Error:Exec: Update done (or update values are the same as the old ones).";
						}
						dnsSubDomain=getSubDomain("dummy",requestParamTable[0][0]);
						checkDNSList("METADATAUPDATEFAIL",requestParamTable[0][0],"","",logicalAddress);						
					}
				}
                break;
            case "METADATAUPDATEFAIL":
				if(testDatabase.toLowerCase()=="true"){
					checker = 0;
					sql.eachRow("select count(*) lignes from bdmsl_smp where LOWER(smp_id) = ${requestParamTable[0][0]} "){
						checker=it.lignes;
					}
					assert (checker == 1),locateTest()+"Error:Exec: Metadata Publisher ${requestParamTable[0][0]} does not exist.";
					def tempo = "";
					//assert (responseParamTable[0][0] == requestParamTable[0][0]),locateTest()+"Error:Exec: Metadata Publisher ${requestParamTable[0][0]} does not match with response value responseParamTable[0][0].";
					sql.eachRow("select * from bdmsl_smp where LOWER(smp_id) = ${requestParamTable[0][0]} "){
						assert ((requestParamTable[1][0] != it.endpoint_logical_address)||(requestParamTable[2][0] != it.endpoint_physical_address)),locateTest()+"Error:Exec: Update done (or update values are the same as the old ones).";
					}
				}
				dnsSubDomain=getSubDomain("dummy",requestParamTable[0][0]);
                checkDNSList("METADATAUPDATEFAIL",requestParamTable[0][0],"","",logicalAddress);
                break;
            case "PARTICIPANTSLIST":
				if(testDatabase.toLowerCase()=="true"){
					sql.eachRow("select count(*) as lignes from bdmsl_participant_identifier"){
						recordNumber=it.lignes;
					}
					nbre = responseParamTable.size()/3;
					assert(nbre== recordNumber),locateTest()+"Error:Exec: Number of participant IDs in DB ( $recordNumber ) is not equal to the one in the response ( $nbre ).";
					sql.eachRow("select pi.participant_id as participant_id, pi.scheme as scheme, smp.smp_id as smp_id  from bdmsl_participant_identifier pi INNER JOIN bdmsl_smp smp on pi.fk_smp_id = smp.id"){
						checker = 0;
						nbre = 0;
						while (nbre<responseParamTable.size()){
							if((it.scheme.trim().toLowerCase() == responseParamTable[nbre][0].trim().toLowerCase())
									&& (it.participant_id.trim().toLowerCase() == responseParamTable[nbre+1][0].trim().toLowerCase())
									&& (it.smp_id.trim().toLowerCase() == responseParamTable[nbre+2][0].trim().toLowerCase())){
								checker=1;
								nbre=responseParamTable.size();
							}
							else{
								nbre = nbre + 3;
							}
						}
						assert (checker == 1),locateTest()+"Error:Exec: participant ID "+it.scheme+"	"+it.participant_id+" is not returned in the response";
					}
				}
                break;
            case "LIST":
				if(testDatabase.toLowerCase()=="true"){
                // Verify that the number of participant IDs displayed is not greater than a page size.
					sql.eachRow("select VALUE from bdmsl_configuration where property = 'paginationListRequest'"){
						pageSize=it.VALUE.toInteger();
					}
					assert(pageSize >= (responseParamTable.size()-2)/2),locateTest()+"Error:Exec: Number of participant IDs displayed is greater than a page size.";
					if(pageSize >= (responseParamTable.size()-2)/2){
						startPage = pageSize*(requestParamTable[1][0] - 1) +1;
						endPage = startPage + pageSize - 1;
						recordNumber = 2;
						nbre=1;
						sql.eachRow("select * from bdmsl_participant_identifier where LOWER(fk_smp_id) = ${requestParamTable[0][0]} "){
							if((nbre>=startPage)&&(nbre<=endPage)){
								assert((responseParamTable[recordNumber][0]== it.scheme) && (responseParamTable[recordNumber+1][0]== it.participant_id.toLowerCase())),locateTest()+"Error:Exec: participant ID shouldn't be in this page.";
								recordNumber = recordNumber + 2;
							}
							nbre++;
						}
					}
				}
                break;
            default:
                log.info "Unknown operation";
        }
        closeConnection();
		debugLog("================ End verification.",log);
    }
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
    // Initialize operation parameters
    def InitializeParameters(String testType, String indicator, int taille){
        def curseur=1;
        if(indicator=="Request"){
            switch(testType.toUpperCase()){
                case "PARTICIPANTIDENTIFIER":
                    requestParamTable[0]=["0","ServiceMetadataPublisherID"]
                    while(curseur< (taille*2)){
                        requestParamTable[curseur]=["0","ParticipantIdentifierSchemeRequest"]
                        requestParamTable[curseur+1]=["0","ParticipantIdentifierRequest"]
                        curseur=curseur+2
                    }
                    break;
                case "PREPAREMIGRATIONRECORD":
					requestParamTable[0]=["0","ServiceMetadataPublisherID"]
                    requestParamTable[1]=["0","ParticipantIdentifierSchemeRequest"]
                    requestParamTable[2]=["0","ParticipantIdentifierRequest"]
                    requestParamTable[3]=["0","MigrationKey"]
                    break;
                case "METADATACREATE":
                    requestParamTable[0]=["0","ServiceMetadataPublisherID"]
                    requestParamTable[1]=["0","LogicalAddress"]
                    requestParamTable[2]=["0","PhysicalAddress"]
                    break;
                case "METADATAREAD":
                    requestParamTable[0]=["0","ServiceMetadataPublisherID"]
                    break;
                case "LIST":
                    requestParamTable = [];
                    requestParamTable[0]=["0","ServiceMetadataPublisherID"];
                    requestParamTable[1]=[1,"NextPageIdentifier"];
                    break;
                default:
                    log.info "Unknown operation";
            }
        }
		
        if(indicator=="Response"){
            switch(testType.toUpperCase()){
                case "METADATAREAD":
                    responseParamTable = [];
                    responseParamTable[0]=["0","ServiceMetadataPublisherID"];
                    responseParamTable[1]=["0","LogicalAddress"];
                    responseParamTable[2]=["0","PhysicalAddress"];
                    break;
                case "PARTICIPANTSLIST":
                    responseParamTable = [];
                    break;
                case "LIST":
                    responseParamTable[0]=["0","ServiceMetadataPublisherID"];
                    responseParamTable[1]=[1,"NextPageIdentifier"];
                    while(curseur< (taille*2)){
                        responseParamTable[curseur+1]=["0","ParticipantIdentifierSchemeRequest"];
                        responseParamTable[curseur+2]=["0","ParticipantIdentifierRequest"];
                        curseur=curseur+2;
                    }
                    break;
                default:
                    log.info "Unknown operation";
            }
        }
    }
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
    // Check DNS server
    def checkDNSList(String testType, String chain1, String chain2, String chain3, String infoLogAddr=null){
		debugLog("======== Call checkDNSList: testType="+testType+" | chain1="+chain1+" | chain2="+chain2+" | chain3="+chain3+" | infoLogAddr="+infoLogAddr,log);
        if(testDns.toLowerCase()=="true"){
			def boolean CNAMEpresent = true;	def boolean NAPTRpresent = true
            def String CNAMEhash = null;	def String NAPTRhash = null;
            def String CNAMEHashAlgo = "MD5";	def String NAPTRHashAlgo = "SHA256";
            def oldSMP = null;
            def logAddress;
            def allContent=null;
			def dnsRecordfetched=null;
			
			(CNAMEpresent,NAPTRpresent) = getDNSrecordTypeNeeded(dnsSubDomain);
			if((chain3!=null) && (chain3!="")){
			    CNAMEhash=encodeHash(chain3,CNAMEHashAlgo,false);
                NAPTRhash=encodeHash(chain3,NAPTRHashAlgo,true);
				dnsRecordfetched=fetchRecordsFromDNS("participant",listDNSenabled,chain3.trim().toLowerCase(),chain2.trim().toLowerCase());
			}
			allContent=fetchRecordsFromDNS("sml",listDNSenabled);
            switch(testType.toUpperCase()){
                case "METADATACREATE":
					// -- SMP --
					debugLog("checkDNSList:METADATACREATE: Checking SMP record in DNS.",log);
					logAddress = chain2.split("://");
					assert(checkTarget(allContent,chain1+".publisher."+dnsSubDomain,logAddress[1])== true),locateTest()+"Error:Exec: SMP \"$chain1\" is not found in the DNS.";
					debugLog("checkDNSList:METADATACREATE: SMP record check successfully done.",log);
                    break;
                case "PARTICIPANTIDENTIFIER":
					assert(infoLogAddr != null),locateTest()+"Error:Exec: SMP logical adress was not provided.";
					// -- SMP --
					debugLog("checkDNSList:PARTICIPANTIDENTIFIER: Checking SMP record in DNS.",log);
					assert(checkTarget(allContent,chain1+".publisher."+dnsSubDomain,infoLogAddr.split("://")[1])== true),locateTest()+"Error:Exec: SMP \"$chain1\" not found in the DNS.";
					debugLog("checkDNSList:PARTICIPANTIDENTIFIER: SMP record check successfully done.",log);
					// -- NAPTR --
					debugLog("Checking NAPTR record in DNS."+" NAPTR record type presence should be "+NAPTRpresent+" for this subdomain.",log);
					assert(checkTarget(dnsRecordfetched,NAPTRregularExp+infoLogAddr+"!",NAPTRhash+"."+chain2+"."+dnsSubDomain)== NAPTRpresent),locateTest()+"Error:Exec: NAPTR record for Participant ID \"$chain3\" ( $NAPTRhash ) presence must be $NAPTRpresent";
					assert(checkTarget(dnsRecordfetched,NAPTRregularExp+infoLogAddr+"!","B-"+NAPTRhash+"."+chain2+"."+dnsSubDomain)== false),locateTest()+"Error:Exec: NAPTR record is present with B- prefix for Participant ID \"$chain3\" ( $NAPTRhash )";
					// -- CNAME --
					debugLog("Checking CNAME record in DNS."+" CNAME record type presence should be "+CNAMEpresent+" for this subdomain.",log);
					assert(checkTarget(dnsRecordfetched,chain1+".publisher."+dnsSubDomain,"B-"+CNAMEhash+"."+chain2+"."+dnsSubDomain)== CNAMEpresent),locateTest()+"Error:Exec: CNAME record for Participant ID \"$chain3\" ( $CNAMEhash ) presence must be $CNAMEpresent";					
                    break;
                case "MIGRATION":
					assert(infoLogAddr != null),locateTest()+"Error:Exec: SMP logical adress was not provided.";
					// -- SMP --
					debugLog("checkDNSList:MIGRATION: Checking SMP record in DNS.",log);
					assert(checkTarget(allContent,chain1+".publisher."+dnsSubDomain,infoLogAddr.split("://")[1])== true),locateTest()+"Error:Exec: SMP \"$chain1\" not found in the DNS.";
					debugLog("checkDNSList:MIGRATION: SMP record check successfully done.",log);
					// -- NAPTR --
					debugLog("Checking NAPTR record in DNS."+" NAPTR record type presence should be "+NAPTRpresent+" for this subdomain.",log);
					if(NAPTRpresent){
						assert(checkTarget(dnsRecordfetched,NAPTRregularExp+infoLogAddr+"!",NAPTRhash+"."+chain2+"."+dnsSubDomain)== true),locateTest()+"Error:Exec: Migration of Participant ID \"$chain3\" ( $NAPTRhash ) to SMP \"$chain1\" failed for NAPTR record.";
					}
					else{
						assert(checkTarget(dnsRecordfetched,NAPTRregularExp+infoLogAddr+"!",NAPTRhash+"."+chain2+"."+dnsSubDomain)== false),locateTest()+"Error:Exec: NAPTR record for Participant ID \"$chain3\" ( $NAPTRhash ) presence must be $NAPTRpresent";
					}
					debugLog("Checking CNAME record in DNS."+" CNAME record type presence should be "+CNAMEpresent+" for this subdomain.",log);
					// -- CNAME --
					if(CNAMEpresent){
						assert(checkTarget(dnsRecordfetched,chain1+".publisher."+dnsSubDomain,"B-"+CNAMEhash+"."+chain2+"."+dnsSubDomain)== true),locateTest()+"Error:Exec: Migration of Participant ID \"$chain3\" ( $NAPTRhash ) to SMP \"$chain1\" failed for CNAME record.";
					}
					else{
						assert(checkTarget(dnsRecordfetched,chain1+".publisher."+dnsSubDomain,"B-"+CNAMEhash+"."+chain2+"."+dnsSubDomain)== false),locateTest()+"Error:Exec: CNAME record for Participant ID \"$chain3\" ( $CNAMEhash ) presence must be $CNAMEpresent";
					}
                    break;
                case "METADATADELETE":
					// -- SMP --
					debugLog("checkDNSList:METADATADELETE: Checking SMP record in DNS.",log);
					assert(checkTarget(allContent,chain1+".publisher."+dnsSubDomain,chain1+".publisher."+dnsSubDomain)== false),locateTest()+"Error:Exec: SMP \"$chain1\" is found in the DNS.";
					debugLog("checkDNSList:METADATADELETE: SMP record check successfully done.",log);
					break;
                case "METADATAUPDATEFAIL":
                case "METADATADELETEFAIL":
					assert(infoLogAddr != null),locateTest()+"Error:Exec: SMP logical adress was not provided.";
					logAddress = infoLogAddr.split("://");
					// -- SMP --
					debugLog("checkDNSList:METADATAUPDATEFAIL/METADATADELETEFAIL: Checking SMP record in DNS.",log);
					assert(checkTarget(allContent,chain1+".publisher."+dnsSubDomain,logAddress[1])== true),locateTest()+"Error:Exec: SMP \"$chain1\" with logical address \"$infoLogAddr\"+ not found in the DNS.";
					debugLog("checkDNSList:METADATAUPDATEFAIL/METADATADELETEFAIL: SMP record check successfully done.",log);
                    break;
                case "PARTICIPANTIDENTIFIERMULTI":
                case "PARTICIPANTIDENTIFIERNOTFOUND":
					assert(infoLogAddr != null),locateTest()+"Error:Exec: SMP logical adress was not provided.";
					// -- NAPTR --
					debugLog("Checking NAPTR record in DNS."+" NAPTR record type presence should be "+NAPTRpresent+" for this subdomain.",log);
					assert(checkTarget(dnsRecordfetched,NAPTRregularExp+infoLogAddr+"!",NAPTRhash+"."+chain2+"."+dnsSubDomain)== false),locateTest()+"Error:Exec: NAPTR record for Participant ID \"$chain3\" is found in the DNS";
					assert(checkTarget(dnsRecordfetched,NAPTRregularExp+infoLogAddr+"!","B-"+NAPTRhash+"."+chain2+"."+dnsSubDomain)== false),locateTest()+"Error:Exec: NAPTR record with \"B-\" prefix for Participant ID \"$chain3\" is found in the DNS";
					// -- CNAME --
					debugLog("Checking CNAME record in DNS."+" CNAME record type presence should be "+CNAMEpresent+" for this subdomain.",log);
					assert(checkTarget(dnsRecordfetched,chain1+".publisher."+dnsSubDomain,"B-"+CNAMEhash+"."+chain2+"."+dnsSubDomain)== false),locateTest()+"Error:Exec: Participant ID CNAME entry for $chain3 is found in the DNS.";
					assert(checkTarget(dnsRecordfetched,chain1+".publisher."+dnsSubDomain,CNAMEhash+"."+chain2+"."+dnsSubDomain)== false),locateTest()+"Error:Exec: Participant ID CNAME entry for $chain3 is found in the DNS.";
                    break;
                default:
                    log.info "Nothing found."
            }
        }
		debugLog("== End checkDNSList.",log);
    }
	
	
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
    def String getNAPTRregularExpressionValue(String naptrRegExpPropName){
	
		def legacyOn=getSMLConfigProperty(naptrRegExpPropName).toLowerCase();
		def returnedValue="!^.*\$!";
		if(legacyOn.equals("false")){
			returnedValue="!.*!"
		}
        return returnedValue;
		
    }
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
    def String encodeHash(String stringToHash, String algoName, boolean toBase32){
        def String result = null;
        if(toBase32){
            BaseEncoding base32 = BaseEncoding.base32().omitPadding();
            result=	base32.encode(MessageDigest.getInstance(algoName).digest(stringToHash.toLowerCase(Locale.US).bytes))
        }
        else{
            result = MessageDigest.getInstance(algoName).digest(stringToHash.toLowerCase(Locale.US).bytes).encodeHex().toString()
        }
        return result;
    }
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
    def String removeDot(String input){
        def String result = null;
        if((input!=null)&&(input.length()>0)){
            if((input.length()-1)==(input.lastIndexOf("."))){
                result=input.substring(0,input.lastIndexOf("."));
            }
            else{
                result=input;
            }
        }
        else{
            result=input;
        }
        return result;
    }
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
    def checkTarget(String input, String pattern1, String pattern2){
		debugLog("======== Call checkTarget: pattern1="+pattern1+"|pattern2="+pattern2,log);
        def result = 0;
		//log.info "checking:"+pattern1;
		//log.info "checking:"+pattern2;
		if(input==null){
			if(!listDNSenabled){
				log.warn "checkTarget:Warning: Input to check is null but we will not trigger an exception.";
				log.warn "checkTarget:Warning: Following patterns will not be checked in DNS:";
				log.warn "checkTarget:Warning: ||pattern1:$pattern1||";
				log.warn "checkTarget:Warning: ||pattern2:$pattern2||";
				return true;
			}else{
				assert(false),"checkTarget:Error: Null input to check for patterns ||pattern1:$pattern1|| and ||pattern2:$pattern2||";
			}
		}
        input.eachLine { line ->
            if((line.toLowerCase().contains(pattern1.toLowerCase())) && (line.toLowerCase().contains(pattern2.toLowerCase()))){
                result++;
            }
        }
		debugLog("= End checkTarget.",log);
        return ((result==1)?true:false);
    }
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
    // Check if parameters are missing
    def assertOperationsParameters(String indicator){
        def counter = 0
        if(indicator == "Request"){
            while(counter<requestParamTable.size()){
                assert ((requestParamTable[counter][0] != "0") && requestParamTable[counter][0]), "Error can't extract ${requestParamTable[counter][1]} from the request"
                counter++
            }
        }
        if(indicator == "Response"){
            while(counter<responseParamTable.size()){
                assert ((responseParamTable[counter][0] != "0") && responseParamTable[counter][0]), "Error can't extract ${responseParamTable[counter][1]} from the response"
				counter++
            }
        }
    }
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
    def runSql(String request){
		debugLog("======== Call runSql: request="+request,log);
		def connOpenHere = false;
		def String reqType = null;
        if(testDatabase.toLowerCase()=="true"){
			reqType = request.split(" ")[0];
			if(reqType=="ALTER"){
				if(sql==null){
					openConnection();
					connOpenHere=true;
				}
				try{
					sql.executeUpdate(request);
				}
				catch (SQLException ex){
					if(ex.toString().contains("ORA-00957")){
						log.debug "duplicate column name";
					}else{
						assert 0,"SQLException occured: " + ex;
					}
				}
				if(connOpenHere){
					closeConnection();
				}
			}
			else{
				if(sql==null){
					openConnection();
					connOpenHere=true;
				}
				try{
					sql.execute(request);
				}
				catch (SQLException ex){
					assert 0,"SQLException occured: " + ex;
				}
				if(connOpenHere){
					closeConnection();
				}
			}

        }
		debugLog("= End runSql.",log);
    }
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
    // Returns: "--TestCase--testStep--"  
    def String locateTest(){
        return("--"+context.testCase.name+"--"+context.testCase.getTestStepAt(context.getCurrentStepIndex()).getLabel()+"--  ");
    }
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
    // Returns the subdomain related to the SMP  
    def String getSubDomain(String partIdent, String smpIdent=null){
		debugLog("======== Call getSubDomain: partIdent="+partIdent+"	|	smpIdent="+smpIdent,log);
		def connOpenHere = false;
		def smpIdLinked = null;
		def smpNameLinked = null;
		def subDomainLinked = null;
		if(testDatabase.toLowerCase()=="true"){
			if(sql==null){
				openConnection();
				connOpenHere=true;
			}
			if(smpIdent != null){
				smpNameLinked=smpIdent;
				sql.eachRow("Select id from bdmsl_smp where LOWER(smp_id) = LOWER(${smpNameLinked})"){
					smpIdLinked=it.id;
				}
			}
			else{
				sql.eachRow("Select fk_smp_id from bdmsl_participant_identifier where LOWER(participant_id) = LOWER(${partIdent})"){
					smpIdLinked=it.fk_smp_id;
				}				
			}
			debugLog("::getSubDomain:: Fetched smp id: "+smpIdLinked,log);
			if(smpIdLinked==null){
				// Fall on the default one acc.edelivery.tech.ec.europa.eu
				debugLog("::getSubDomain:: (smpIdLinked==null). Fall on the default domain: acc.edelivery.tech.ec.europa.eu.",log);
				return "acc.edelivery.tech.ec.europa.eu";
			}else{
				sql.eachRow("Select	subdomain_name from bdmsl_subdomain where subdomain_id IN (Select fk_subdomain_id from bdmsl_smp where LOWER(id) = LOWER(${smpIdLinked}))"){
					subDomainLinked=it.subdomain_name;
				}
				debugLog("::getSubDomain:: Fetched subDomain name: "+subDomainLinked,log);
				if(subDomainLinked==null){
					debugLog("::getSubDomain:: (subDomainLinked==null). Fall on the default domain: acc.edelivery.tech.ec.europa.eu.",log);
					return "acc.edelivery.tech.ec.europa.eu";
				}
				return subDomainLinked;
			}
			if(connOpenHere){
				closeConnection();
			}
		}
		else{
			// Fall on the default one acc.edelivery.tech.ec.europa.eu
			debugLog("::getSubDomain:: Not possible to access the Database. Fall on the default domain: acc.edelivery.tech.ec.europa.eu.",log);
			return "acc.edelivery.tech.ec.europa.eu";
		}
    }
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII	
	def getDNSrecordTypeNeeded(String dnsSubDomainNeeded){
		debugLog("======== Call getDNSrecordTypeNeeded: dnsSubDomainNeeded="+dnsSubDomainNeeded,log);
		if(testDatabase.toLowerCase()=="true"){
			def target = "all";
			def connOpenHere = false;
			if(sql==null){
				openConnection();
				connOpenHere=true;
			}
			sql.eachRow("Select dns_record_types from bdmsl_subdomain where LOWER(subdomain_name) = LOWER(${dnsSubDomainNeeded})"){
				target=it.dns_record_types.toString();
			}
			if(connOpenHere){
				closeConnection();
			}
			debugLog("DNS record types = "+target,log);
			switch(target.toLowerCase()){
				case "all":
					debugLog("= End getDNSrecordTypeNeeded.",log);
					return [true,true]; // [CNAME,NAPTR]
					break;
				case "cname":
					debugLog("= End getDNSrecordTypeNeeded.",log);
					return [true,false]; // [CNAME,NAPTR]
					break;
				case "naptr":
					debugLog("= End getDNSrecordTypeNeeded.",log);
					return [false,true]; // [CNAME,NAPTR]
					break;
				default:
					debugLog("= End getDNSrecordTypeNeeded.",log);
					return [true,true]; // [CNAME,NAPTR]
			}
		}
		else{
			debugLog("= End getDNSrecordTypeNeeded.",log);
			return [true,true]; // [CNAME,NAPTR]
		}
	}
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII	
	def getSubdomainNameFromID(String subDomainID){
		debugLog("======== Call getSubdomainNameFromID: subDomainID="+subDomainID,log);
		def subdomainName="dummy";
		if(testDatabase.toLowerCase()=="true"){
			def connOpenHere = false;
			if(sql==null){
				openConnection();
				connOpenHere=true;
			}
			sql.eachRow("Select subdomain_name from bdmsl_subdomain where LOWER(subdomain_id) = LOWER(${subDomainID})"){
				subdomainName=it.subdomain_name.toString();
			}
			if(connOpenHere){
				closeConnection();
			}
			return(subdomainName);
		}
	}
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII	
	// Still needed: investigate removing it
	def getSMLConfigProperty(String propertyName){
		debugLog("======== Call getSMLConfigProperty",log);
		debugLog("getSMLConfigProperty: Looking for property \"$propertyName\" ...",log);
		def connOpenHere=false; def propertyValue=null;
		if(testDatabase.toLowerCase()=="true"){
			if(sql==null){
				openConnection();
				connOpenHere=true;
			}
			try{
				sql.eachRow("SELECT value as value FROM bdmsl_configuration WHERE property = ${propertyName}"){
					propertyValue=it.value.toString();
				}
			}
			catch (SQLException ex){
				if(connOpenHere){
					closeConnection();
				}
				assert 0,"SQLException occured: " + ex;
			}
			if(connOpenHere){
				closeConnection();
			}
			assert(propertyValue!=null),"Error:getSMLConfigProperty: property "+propertyName+" not found.";
		}
		else{
			// Needed for initialization
			if(propertyName==smp_update_max_part_size){
				log.info "getSMLConfigProperty: DB access not available. To progress, assume \"smp_update_max_part_size\" = 0."
				return "0"
			}
			if(propertyName==dnsClient_show_entries){
				log.info "getSMLConfigProperty: DB access not available. To progress, assume \"dnsClient_show_entries\" = false."
				return "false"
			}		
			if(propertyName==dnsClient_use_legacy_regexp){
				log.info "getSMLConfigProperty: DB access not available. To progress, assume \"dnsClient_use_legacy_regexp\" = false."
				return "false"
			}			
		}
		log.info "getSMLConfigProperty: Property \"$propertyName\" has value \"$propertyValue\".";
		debugLog("= End getSMLConfigProperty",log);
		return(propertyValue);
	}
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII	
	// Not needed anymore: a service was created for this purpose
	def setSMLConfigProperty(String propertyName, String propertyValue, Integer delayTime=360000){
		debugLog("======== Call setSMLConfigProperty",log);
		if(testDatabase.toLowerCase()=="true"){
			log.info "setSMLConfigProperty: Start changing property \"$propertyName\" value to \"$propertyValue\" ...";
			runSql("update BDMSL_CONFIGURATION set VALUE='${propertyValue}', LAST_UPDATED_ON=CURRENT_TIMESTAMP where property='${propertyName}'");
			log.info "Sleeping for $delayTime ms ..."
			sleep(delayTime);
			log.info "setSMLConfigProperty: Property \"$propertyName\" has new value \"$propertyValue\".";
		}	
		debugLog("= End setSMLConfigProperty",log);
	}
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII	
	def getIntegerFromString(String stringValue){
		debugLog("======== Call getIntegerFromString",log);
		assert(stringValue!=null),"Error:getIntegerFromString: Input value to be converted is null.";
		assert(stringValue.trim().isInteger()),"Error:getIntegerFromString: value \"$stringValue\" can't be converted to an integer.";
		return(stringValue.trim().toInteger());
	}
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII	
	def getbooleanFromString(String stringValue){
		debugLog("======== Call getbooleanFromString",log);
		assert(stringValue!=null),"Error:getbooleanFromString: Input value to be converted is null.";
		if(stringValue.trim().toLowerCase().equals("true")){
			return true;
		}
		if(stringValue.trim().toLowerCase().equals("false")){
			return false;
		}
		assert(false),"Error:getbooleanFromString: value \"$stringValue\" can't be converted to a boolean.";
	}
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
	def countParticipantsLinked(String SMP_ID){
		debugLog("======== Call countParticipantsLinked",log);
		def connOpenHere=false;
		def countValue=0
		if(testDatabase.toLowerCase()=="true"){
			if(sql==null){
				openConnection();
				connOpenHere=true;
			}
			try{
				sql.eachRow("SELECT count(*) as LineCount FROM bdmsl_participant_identifier where fk_smp_id IN (select id from bdmsl_smp WHERE LOWER(smp_id) = LOWER(${SMP_ID}))"){
					countValue=it.LineCount;
				}
			}
			catch (SQLException ex){
				if(connOpenHere){
					closeConnection();
				}
				assert 0,"SQLException occured: " + ex;
			}
			if(connOpenHere){
				closeConnection();
			}
		}
		log.info "countParticipantsLinked: Found $countValue participants linked to SMP \"$SMP_ID\".";
		return(countValue);
	}
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
	def fetchRecordsFromDNS(String searchType="sml",listDNSon="true",String participant_id=null, String participant_id_scheme=null){
		debugLog("======== Call fetchRecordsFromDNS",log);
		debugLog("fetchRecordsFromDNS: ||searchType=$searchType||listDNSon=$listDNSon||participant_id=$participant_id||participant_id_scheme=$participant_id_scheme|| ",log);
		def returnedContent=null;
		def addressDNSList1=urlDNS+"/listDNS";
		def addressDNSList2=urlDNS+"/searchResult";
		def http=null;
		assert ((searchType.trim().toLowerCase().equals("sml"))||(searchType.trim().toLowerCase().equals("participant"))),"Unknown search type: $searchType."
		if(searchType.trim().toLowerCase().equals("sml")){
			debugLog("fetchRecordsFromDNS: Looking for SMP record.",log);
			if(listDNSon){
				http = new HTTPBuilder(addressDNSList1);
				http.request( GET, TEXT ) { req ->
					// executed for all successful responses:
					response.success = { resp, reader ->
						returnedContent = reader.getText();
						assert ((resp.statusLine.statusCode == 200)||(resp.statusLine.statusCode == 201)),"DNS Connection problem."
					}
					response.'404' = { resp ->
						log.error "DNS Connection problem."
					}
				}
			}
		}
		if(searchType.trim().toLowerCase().equals("participant")){
			debugLog("fetchRecordsFromDNS: Looking for participant identifiers records.",log);
			assert((participant_id!=null)&&(participant_id_scheme!=null)),"fetchRecordsFromDNS:Error: participantID or participantID scheme is null.";
			http = new HTTPBuilder(addressDNSList2);
			http.request(POST, TEXT) {
					requestContentType = URLENC
					body=[searchType: 'participant', identifier: participant_id, scheme: participant_id_scheme]
					response.success = { resp, reader ->
						returnedContent=reader.getText();
						debugLog("POST response status: $returnedContent",log);
						assert ((resp.statusLine.statusCode == 200)||(resp.statusLine.statusCode == 201)),"DNS Connection problem."
					}
					response.'404' = { resp ->
					log.error "DNS Connection problem."
				}
			}
		}
		debugLog("======== fetchRecordsFromDNS ended.",log);
		return returnedContent;
	}
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
    static void  setPositionInLogFile(logFileName, log, context, testRunner){
        //debugLog("  ====  Calling \"setPositionInLogFile\".", log)
		log.info "  ====  Calling \"setPositionInLogFile\"."
		def testFile = null
		def projectPath = context.expand('${projectDir}')
        def pathToLogFile = context.expand("\${#Project#pathProxyLogs}")+logFileName
		log.info "  ====  setPositionInLogFile from project path:" + projectPath;
        debugLog("  setPositionInLogFile  [][]  Checking log file [$pathToLogFile]",log)

        // Check file exists
		try{
			testFile = new File(projectPath,pathToLogFile)
		} catch (Exception ex) {
            log.error "  setPositionInLogFile  [][]  Error while trying to create file, exception: " + ex
            assert 0
        }

		assert(testFile!=null),"Error:setPositionInLogFile: testFile is null.";
		
        if (!testFile.exists()) {
            testRunner.fail("File [${pathToLogFile}] does not exist. Can't check logs.")
            return
        } else debugLog("  setPositionInLogFile  [][]  File [${pathToLogFile}] exists.", log)

		
		
        def lineCount = 0
        testFile.eachLine { lineCount++}
        debugLog("  setPositionInLogFile  [][]  Line count = " + lineCount, log)

        testRunner.testCase.setPropertyValue( "skipNumberOfLines", lineCount.toString() )
        log.info "  setPositionInLogFile  [][]  Testcase level property skipNumberOfLine set to = " + lineCount
    }
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
    static void  checkLogFile(logFileName, logValueList, log, context, testRunner,checkPresent = true){
        debugLog("  ====  Calling \"checkLogFile\".", log)

		def projectPath = context.expand('${projectDir}')
        def pathToLogFile = context.expand("\${#Project#pathProxyLogs}")+logFileName

        def skipNumberOfLines = context.expand('${#TestCase#skipNumberOfLines}')
        if (skipNumberOfLines == "") {
            log.info "  checkLogFile  [][]  skipNumberOfLines property not defined on the test case level would start to search on first line"
            skipNumberOfLines = 0
        } else
            skipNumberOfLines = skipNumberOfLines.toInteger()

        // Check file exists
        def testFile = new File(projectPath,pathToLogFile)
        if (!testFile.exists()) {
            testRunner.fail("File [${pathToLogFile}] does not exist. Can't check logs.")
            return
        } else log.debug "  checkLogFile  [][]  File [${pathToLogFile}] exists."

        //def skipNumberOfLines = 0
        def foundTotalNumber = 0
        def fileContent = testFile.text

        def logSizeInLines = fileContent.readLines().size()
		log.info " checkLogFile  [][]  would skip ${skipNumberOfLines} lines out of ${logSizeInLines}"
        if (logSizeInLines < skipNumberOfLines) {
            log.info "Incorrect number of line to skip - it is higher than number of lines in log file (" + logSizeInLines + "). Maybe it is new log file would reset skipNumberOfLines value."
            skipNumberOfLines = 0
        }

        for(logEntryToFind  in logValueList){
            def found = false
            testFile.eachLine{
                line, lineNumber ->
                    if (lineNumber > skipNumberOfLines) {
						log.info "  checkLogFileEntry  [][]  In log line $line searched for: $logEntryToFind"
                        if(line =~ logEntryToFind) {
                            log.info "  checkLogFile  [][]  In log line $lineNumber searched entry was found. Line value is: $line"
                            found = true
                        }
                    }
            }
            if (! found ){
                if(checkPresent){
                    log.warn " checkLogFile  [][]  The search string [$logEntryToFind] was NOT found in the file [${pathToLogFile}]"
                }
            }
            else{
                foundTotalNumber++
            }
        } //loop end
        if(checkPresent){
            if (foundTotalNumber != logValueList.size())
                testRunner.fail(" checkLogFile  [][]  Searching log file failed: Only ${foundTotalNumber} from ${logValueList.size()} entries found.")
            else
                log.info " checkLogFile  [][]  All ${logValueList.size()} entries were found in log file."
        }
        else{
            if (foundTotalNumber != 0)
                testRunner.fail(" checkLogFile  [][]  Searching log file failed: ${foundTotalNumber} from ${logValueList.size()} entries were found.")
            else
                log.info " checkLogFile  [][]  All ${logValueList.size()} entries were not found in log file."
        }
    }
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
	def uiSearchParticipant(String searchType="participant",String participant_id=null, String participant_id_scheme=null,String domain_name=null,expectedPresent=[],expectedNotPresent=[]){
		debugLog("======== Call uiSearchParticipant",log);
		log.info("uiSearchParticipant: ||searchType=$searchType||participant_id=$participant_id||participant_id_scheme=$participant_id_scheme||domain_name=$domain_name||");
		def returnedContent=null;
		def addressDNSList=urlDNS+"/searchResult";		
		def http=null;
		def iCount=0;
		
		assert ((searchType.trim().toLowerCase().equals("participant"))||(searchType.trim().toLowerCase().equals("domain"))),"Unknown search type: $searchType."
		
		if(searchType.trim().toLowerCase().equals("participant")){
			debugLog("uiSearchParticipant: Looking for participant identifiers records.",log);
			// HTTP request
			http = new HTTPBuilder(addressDNSList);
			http.request(POST, TEXT) {
					requestContentType = URLENC
					body=[searchType: 'participant', identifier: participant_id, scheme: participant_id_scheme]
					response.success = { resp, reader ->
						returnedContent=reader.getText();
						debugLog("POST response status: $returnedContent",log);
						assert ((resp.statusLine.statusCode == 200)||(resp.statusLine.statusCode == 201)),"DNS Connection problem."
					}
					response.'404' = { resp ->
					log.error "DNS Connection problem."
				}
			}
		}
		
		if(searchType.trim().toLowerCase().equals("domain")){
			debugLog("uiSearchParticipant: Looking for participant identifiers records.",log);
			// HTTP request
			http = new HTTPBuilder(addressDNSList);
			http.request(POST, TEXT) {
					requestContentType = URLENC
					body=[searchType: 'domain', domainName: domain_name]
					response.success = { resp, reader ->
						returnedContent=reader.getText();
						debugLog("POST response status: $returnedContent",log);
						assert ((resp.statusLine.statusCode == 200)||(resp.statusLine.statusCode == 201)),"DNS Connection problem."
					}
					response.'404' = { resp ->
					log.error "DNS Connection problem."
				}
			}
		}

		
		// Verify results
		while(iCount<expectedPresent.size()){
			assert (returnedContent.contains(expectedPresent[iCount])),"uiSearchParticipant: Error: String \""+expectedPresent[iCount]+"\" not found in search results:\n $returnedContent"
			iCount++;
		}
		iCount=0;
		while(iCount<expectedNotPresent.size()){
			assert (!returnedContent.contains(expectedNotPresent[iCount])),"uiSearchParticipant: Error: String \""+expectedNotPresent[iCount]+"\" found in search results !"
			iCount++;
		}			
		
		debugLog("======== uiSearchParticipant ended.",log);
	}
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
	void displayConstrParams(){
		debugLog("==url :"+url+"==",log);
		debugLog("==driver :"+driver+"==",log);
		debugLog("==sql :"+sql+"==",log);
		debugLog("==testDatabase :"+testDatabase+"==",log);
		debugLog("==testDns :"+testDns+"==",log);
		debugLog("==urlDNS :"+urlDNS+"==",log);
		debugLog("==dbUser :"+dbUser+"==",log);
		debugLog("==dbPassword :"+dbPassword+"==",log);	
		debugLog("==maxSMPparticipants :"+maxSMPparticipants+"==",log);
		debugLog("==listDNSenabled :"+listDNSenabled+"==",log);
		debugLog("==NAPTRregularExp :"+NAPTRregularExp+"==",log);
	}
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
	void DumpDBTables(){
		
		def colTable=[];def ii=0;
		def connOpenHere = false;
		if(sql==null){
			openConnection();
			connOpenHere=true;
		}
		try{
			log.info "--------------------------------------------------------------------------------------------";
			log.info "subdomain_id--dns_record_types--dns_zone--subdomain_name--participant_id_regexp";
			log.info "--------------------------------------------------------------------------------------------";
			sql.eachRow("SELECT * FROM bdmsl_subdomain"){
				colTable[ii]=["","","","",""];
				colTable[ii][0]=it.subdomain_id;colTable[ii][1]=it.dns_record_types;colTable[ii][2]=it.dns_zone;colTable[ii][3]=it.subdomain_name;colTable[ii][4]=it.participant_id_regexp;
				ii=ii+1;
			}
			for(int jj=0;jj<colTable.size;jj++){
				log.info (colTable[jj].join('--'));
			}
			ii=0;
			colTable=[];
			log.info "\n\n\n";
			log.info "------------------------------------------------";
			log.info "certificate--is_root_ca--fk_subdomain_id";
			log.info "------------------------------------------------";
			sql.eachRow("SELECT * FROM bdmsl_certificate_domain"){
				colTable[ii]=["","",""];
				colTable[ii][0]=it.certificate;colTable[ii][1]=it.is_root_ca;colTable[ii][2]=it.fk_subdomain_id;
				ii=ii+1;
			}
			for(int jj=0;jj<colTable.size;jj++){
				log.info (colTable[jj].join('--'));
			}
			
			
			ii=0;
			colTable=[];
			log.info "\n\n\n";
			log.info "------------------------------------";
			log.info "property--value";
			log.info "------------------------------------";
			sql.eachRow("SELECT * FROM bdmsl_configuration"){
				colTable[ii]=["",""];
				colTable[ii][0]=it.property;colTable[ii][1]=it.value;
				ii=ii+1;
			}
			for(int jj=0;jj<colTable.size;jj++){
				log.info (colTable[jj].join('--'));
			}
		}
		catch (SQLException ex){
			if(connOpenHere){
				closeConnection();
			}
			assert 0,"SQLException occured: " + ex;
		}
		closeConnection();
	}
}

