# DomiSML
### Sample implementation, open source project of the eDelivery service metadata publisher.

[![License badge](https://img.shields.io/badge/license-EUPL-blue.svg)](https://ec.europa.eu/digital-building-blocks/wikis/download/attachments/52601883/eupl_v1.2_en%20.pdf?version=1&modificationDate=1507206778126&api=v2)
[![Documentation badge](https://img.shields.io/badge/docs-latest-brightgreen.svg)](https://ec.europa.eu/digital-building-blocks/wikis/display/DIGITAL/SML)
[![Support badge]( https://img.shields.io/badge/support-sof-yellowgreen.svg)](https://ec.europa.eu/digital-building-blocks/wikis/display/DIGITAL/Support+eDelivery)

## Introduction

This is the code repository for eDelivery DomiSML (previously known as BDMSL), the sample implementation of the SML.
The DomiSML supports the [eDelivery BDXL 1.6](https://ec.europa.eu/digital-building-blocks/sites/display/DIGITAL/eDelivery+BDXL) profile, as well as the [PEPPOL SML 1.01](https://docs.peppol.eu/edelivery/sml/ICT-Transport-SML_Service_Specification-101.pdf) specification.


Any feedback on the application or the following documentation is highly welcome, including bugs, typos
or things you think should be included but aren't. You can use [JIRA](https://ec.europa.eu/digital-building-blocks/tracker/projects/EDELIVERY/issues) to provide feedback.

Following documents are available on the [DomiSML release page](https://ec.europa.eu/digital-building-blocks/wikis/display/DIGITAL/SML)

[Top](#DomiSML)

## Build

In order to build eDelivery SMP, you need to have the following installed: 
*  Java JDK 21 
*  Maven 3.9+   

To build eDelivery SMP, run the following command in the root directory of the project:

    mvn clean install 


[Top](#DomiSML)

## Install and run

How to install and run eDelivery SMP can be read in the Admin Guide available on the [eDelivery SML Release Page](https://ec.europa.eu/digital-building-blocks/wikis/display/DIGITAL/SML+software).

[Top](#DomiSML)

## License

eDelivery SMP is licensed under European Union Public Licence (EUPL) version 1.2.

[Top](#DomiSML)

## Support

Have questions? Consult our [Q&A section](https://ec.europa.eu/digital-building-blocks/wikis/display/DIGITAL/SMP+FAQs). 
Still have questions? Contact [eDelivery support](https://ec.europa.eu/digital-building-blocks/tracker/plugins/servlet/desk/portal/6).


[Top](#DomiSML)
