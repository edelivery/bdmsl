/*-
 * #START_LICENSE#
 * bdmsl-common
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.util;

import eu.europa.ec.bdmsl.AbstractTest;
import eu.europa.ec.bdmsl.common.exception.KeyException;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author Flavio SANTOS
 * @since 3.1.1
 */
public class EncryptPasswordTest extends AbstractTest {

    @Test
    public void testMain() {
        Path resourceDirectory = Paths.get("src", "test", "resources", "masterKeyWithIV.key");
        String[] args = new String[]{resourceDirectory.toAbsolutePath().toString(), "123456789"};
        assertDoesNotThrow(() -> EncryptPassword.main(args));
    }

    @Test
    public void testMainPrivateKeyNotFound() {
        Path resourceDirectory = Paths.get("src", "test", "resources", "masterKeyNotFound.key");
        String[] args = new String[]{resourceDirectory.toAbsolutePath().toString(), "123456789"};
        KeyException result = assertThrows(KeyException.class,
                () -> EncryptPassword.main(args));

        MatcherAssert.assertThat(result.getMessage(),
                CoreMatchers.containsString("[ERR-105]"));
    }

    @Test
    public void testMainPrivateMissingParams() {
        Path resourceDirectory = Paths.get("src", "test", "resources", "masterKeyNotFound.key");
        String[] args = new String[]{resourceDirectory.toAbsolutePath().toString()};

        KeyException result = assertThrows(KeyException.class,
                () -> EncryptPassword.main(args));

        MatcherAssert.assertThat(result.getMessage(),
                CoreMatchers.containsString("[ERR-105] Check required parameters, null/empty value is not allowed."));

    }
}
