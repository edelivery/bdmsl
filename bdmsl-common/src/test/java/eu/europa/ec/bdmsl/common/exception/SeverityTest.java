/*-
 * #START_LICENSE#
 * bdmsl-common
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.exception;

import eu.europa.ec.bdmsl.AbstractTest;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Flavio SANTOS
 * @since 3.1.1
 */
public class SeverityTest extends AbstractTest {

    @Test
    public void checkValues1() {
        assertEquals(6, Severity.values().length);
    }

    @Test
    public void checkValues2() {
        //GIVEN
        List<String> severities1 = getAvailableValues();
        int severities1Size = severities1.size();
        List<Severity> severities2 = Arrays.asList(Severity.values());

        //WHEN
        for (Severity severity : severities2) {
            severities1.remove(severity.toString());
        }

        //THEN
        assertEquals(0, severities1.size());
        assertEquals(severities1Size, severities2.size());
    }

    private List<String> getAvailableValues() {
        return new ArrayList<String>() {
            {
                add("TRACE");
                add("DEBUG");
                add("INFO");
                add("WARN");
                add("ERROR");
                add("FATAL");
            }
        };
    }
}
