/*-
 * #START_LICENSE#
 * bdmsl-common
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.util;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Flavio SANTOS
 * @since 3.1.1
 */
public class LogEventsTest {

    @Test
    public void testLogEvents() throws Exception {
        //GIVEN THEN WHEN
        for (Field field : LogEvents.class.getFields()) {
            assertTrue(isCodeValid(field.getName(), (String) field.get(LogEvents.class)));
        }
    }

    @Test
    public void testLogEventMessages() {
        //GIVEN
        LogEvents logEvents = new LogEvents();
        //THEN WHEN
        assertEquals(logEvents.getMessage("SEC-001"), "The host %s attempted to access %s with principal %s");
        assertEquals(logEvents.getMessage("BUS-013"), "The list of participants couldn't be created: %s.");
        assertEquals(logEvents.getMessage("BUS-028"), "The following CNAME record has been added to the DNS for the participant %s : %s");
        assertNull(logEvents.getMessage("BUS-02845656"));
    }

    private boolean isCodeValid(String codeName, String value) {
        switch (codeName) {
            case "SEC_CONNECTION_ATTEMPT":
                return value.equals("SEC-001");
            case "SEC_AUTHORIZED_ACCESS":
                return value.equals("SEC-002");
            case "SEC_UNAUTHORIZED_ACCESS":
                return value.equals("SEC-003");
            case "SEC_REVOKED_CERTIFICATE":
                return value.equals("SEC-004");
            case "SEC_UNKNOWN_CERTIFICATE":
                return value.equals("SEC-005");
            case "SEC_CERTIFICATE_EXPIRED":
                return value.equals("SEC-006");
            case "SEC_CERTIFICATE_NOT_YET_VALID":
                return value.equals("SEC-007");
            case "BUS_AUTHENTICATION_ERROR":
                return value.equals("BUS-001");
            case "BUS_SMP_CREATED":
                return value.equals("BUS-002");
            case "BUS_SMP_CREATION_FAILED":
                return value.equals("BUS-003");
            case "BUS_SMP_READ":
                return value.equals("BUS-004");
            case "BUS_SMP_READ_FAILED":
                return value.equals("BUS-005");
            case "BUS_SMP_DELETED":
                return value.equals("BUS-006");
            case "BUS_SMP_DELETION_FAILED":
                return value.equals("BUS-007");
            case "BUS_SMP_UPDATED":
                return value.equals("BUS-008");
            case "BUS_SMP_UPDATE_FAILED":
                return value.equals("BUS-009");
            case "BUS_PARTICIPANT_CREATED":
                return value.equals("BUS-010");
            case "BUS_PARTICIPANT_CREATION_FAILED":
                return value.equals("BUS-011");
            case "BUS_PARTICIPANT_LIST_CREATED":
                return value.equals("BUS-012");
            case "BUS_PARTICIPANT_LIST_CREATION_FAILED":
                return value.equals("BUS-013");
            case "BUS_PARTICIPANT_DELETED":
                return value.equals("BUS-014");
            case "BUS_PARTICIPANT_DELETION_FAILED":
                return value.equals("BUS-015");
            case "BUS_PARTICIPANT_LIST_DELETED":
                return value.equals("BUS-016");
            case "BUS_PARTICIPANT_LIST_DELETION_FAILED":
                return value.equals("BUS-017");
            case "BUS_PARTICIPANT_LIST":
                return value.equals("BUS-018");
            case "BUS_PARTICIPANT_LIST_FAILED":
                return value.equals("BUS-019");
            case "BUS_PREPARE_TO_MIGRATE_SUCCESS":
                return value.equals("BUS-020");
            case "BUS_PREPARE_TO_MIGRATE_FAILED":
                return value.equals("BUS-021");
            case "BUS_MIGRATE_SUCCESS":
                return value.equals("BUS-022");
            case "BUS_MIGRATE_FAILED":
                return value.equals("BUS-023");
            case "BUS_LIST_ALL_PARTICIPANT_SUCCESS":
                return value.equals("BUS-024");
            case "BUS_LIST_ALL_PARTICIPANT_FAILED":
                return value.equals("BUS-025");
            case "BUS_CERTIFICATE_CHANGED":
                return value.equals("BUS-026");
            case "BUS_CERTIFICATE_CHANGE_FAILED":
                return value.equals("BUS-027");
            case "BUS_CNAME_RECORD_FOR_PARTICIPANT_CREATED":
                return value.equals("BUS-028");
            case "BUS_NAPTR_RECORD_FOR_PARTICIPANT_CREATED":
                return value.equals("BUS-029");
            case "BUS_CNAME_RECORD_FOR_SMP_CREATED":
                return value.equals("BUS-030");
            case "BUS_A_RECORD_FOR_SMP_CREATED":
                return value.equals("BUS-031");
            case "BUS_CERTIFICATE_CHANGE_JOB_SUCCESS":
                return value.equals("BUS-032");
            case "BUS_CERTIFICATE_CHANGE_JOB_FAILED":
                return value.equals("BUS-033");
            case "BUS_CONFIGURATION_ERROR":
                return value.equals("BUS-034");
            case "BUS_CERTIFICATE_CHANGE_SERVICE_SUCCESS":
                return value.equals("BUS-035");
            case "BUS_CERTIFICATE_CHANGE_SERVICE_FAILED":
                return value.equals("BUS-036");
            case "CATEGORY_SECURITY":
                return value.equals("SECURITY");
            case "CATEGORY_BUSINESS":
                return value.equals("BUSINESS");
            case "BUS_CUSTOM_DNS_CREATED":
                return value.equals("BUS-037");
            case "BUS_CUSTOM_DNS_DELETED":
                return value.equals("BUS-038");
            case "BUS_PARTICIPANT_FOUND":
                return value.equals("BUS-039");
            case "BUS_PARTICIPANT_FOUND_FAILED":
                return value.equals("BUS-040");
            default:
                return false;
        }
    }
}
