/*-
 * #START_LICENSE#
 * bdmsl-common
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.util;

import eu.europa.ec.bdmsl.common.exception.KeyException;
import org.apache.commons.lang3.StringUtils;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Flavio SANTOS
 * @since 3.1.1
 */
public class KeyUtilTest {

    @Test
    public void testEncryptDecryptPasswordWithoutIV() throws Exception {
        Path resourceDirectory = Paths.get("src", "test", "resources", "masterKeyWithoutIV.key");
        String password = UUID.randomUUID() + "@!&#";

        String encodedPassword = KeyUtil.encrypt(resourceDirectory.toAbsolutePath().toString(), password);
        byte[] decrypted = KeyUtil.decrypt(resourceDirectory.toAbsolutePath().toString(), encodedPassword);
        String decryptedPassword = new String(decrypted);
        assertFalse(StringUtils.isEmpty(encodedPassword));
        assertNotEquals(password, encodedPassword);
        assertEquals(password, decryptedPassword);
    }

    @Test
    public void testEncryptDecryptPasswordWithIV() throws Exception {
        Path resourceDirectory = Paths.get("src", "test", "resources", "masterKeyWithIV.key");

        String password = UUID.randomUUID() + "@!&#";

        String encodedPassword = KeyUtil.encrypt(resourceDirectory.toAbsolutePath().toString(), password);
        byte[] decrypted = KeyUtil.decrypt(resourceDirectory.toAbsolutePath().toString(), encodedPassword);
        String decryptedPassword = new String(decrypted);
        assertFalse(StringUtils.isEmpty(encodedPassword));
        assertNotEquals(password, encodedPassword);
        assertEquals(password, decryptedPassword);
    }


    @Test
    public void testEncryptDecryptPasswordWithIVFailScenario() throws KeyException {
        Path encKey = Paths.get("src", "test", "resources", "masterKeyWithIV.key");
        Path decKey = Paths.get("src", "test", "resources", "masterKeyWithoutIV.key");
        String password = UUID.randomUUID() + "@!&#";

        String encodedPassword = KeyUtil.encrypt(encKey.toAbsolutePath().toString(), password);
        KeyException result = assertThrows(KeyException.class,
                () -> KeyUtil.decrypt(decKey.toAbsolutePath().toString(), encodedPassword));

        MatcherAssert.assertThat(result.getMessage(), Matchers.containsString("[ERR-105] Either private key or encrypted password might not be correct. Please check both."));
    }

    @Test
    public void testDecryptWrongData() throws KeyException {
        Path resourceDirectory = Paths.get("src", "test", "resources", "masterKeyWithoutIV.key");

        String encodedPassword = KeyUtil.encrypt(resourceDirectory.toAbsolutePath().toString(), "Password");
        assertNotNull(encodedPassword);
        KeyException result = assertThrows(KeyException.class,
                () -> KeyUtil.decrypt(resourceDirectory.toAbsolutePath().toString(), "vXA7JjCy0iDQmX1UEN1Qwg=="));

        MatcherAssert.assertThat(result.getMessage(), Matchers.containsString("[ERR-105] Either private key or encrypted password might not be correct. Please check both."));
    }

    @Test
    public void testGenerateMasterKeyWitIV() throws Exception {
        String tempPrivateKey = System.currentTimeMillis() + ".private";
        Path resourcePath = Paths.get("target", tempPrivateKey);

        KeyUtil.generatePrivateSymmetricKey(resourcePath.toAbsolutePath().toString());

        byte[] buff = Files.readAllBytes(resourcePath);
        assertTrue(buff.length > KeyUtil.IV_GCM_SIZE);
        // start tag
        assertEquals('#', buff[0]);
        // end IV tag
        assertEquals('#', buff[KeyUtil.IV_GCM_SIZE + 1]);
        byte[] keyBytes = Arrays.copyOfRange(buff, KeyUtil.IV_GCM_SIZE + 2, buff.length);


        SecretKey privateKey = new SecretKeySpec(keyBytes, "AES");
        assertNotNull(privateKey);

    }

}
