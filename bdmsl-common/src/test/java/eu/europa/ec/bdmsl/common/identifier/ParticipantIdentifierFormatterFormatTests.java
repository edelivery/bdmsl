/*-
 * #START_LICENSE#
 * bdmsl-common
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.identifier;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.regex.Pattern;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Joze Rihtarsic
 * @since 4.3
 */
class ParticipantIdentifierFormatterFormatTests {


    private static Stream<Arguments> partyIdentifierTestArguments() {
        return Stream.of(
                Arguments.of("urn example with split regular expression",
                        new ParticipantIdentifier("0088:195491", "urn:ehealth:partyid-type"),
                        "urn:ehealth:partyid-type:0088:195491",
                        "urn%3Aehealth%3Apartyid-type%3A0088%3A195491"),

                Arguments.of("ebCore unregistered",
                        new ParticipantIdentifier("ec.europa.eu", "urn:oasis:names:tc:ebcore:partyid-type:unregistered:domain"),
                        "urn:oasis:names:tc:ebcore:partyid-type:unregistered:domain:ec.europa.eu",
                        "urn%3Aoasis%3Anames%3Atc%3Aebcore%3Apartyid-type%3Aunregistered%3Adomain%3Aec.europa.eu"),
                Arguments.of("ebCore iso6523",
                        new ParticipantIdentifier("123456789", "urn:oasis:names:tc:ebcore:partyid-type:iso6523:0088"),
                        "urn:oasis:names:tc:ebcore:partyid-type:iso6523:0088:123456789",
                        "urn%3Aoasis%3Anames%3Atc%3Aebcore%3Apartyid-type%3Aiso6523%3A0088%3A123456789"),
                Arguments.of("Double colon basic",
                        new ParticipantIdentifier("b", "a-a-a"),
                        "a-a-a::b",
                        "a-a-a%3A%3Ab"),
                Arguments.of("Double colon twice", new ParticipantIdentifier("b::c", "a-a-a"),
                        "a-a-a::b::c",
                        "a-a-a%3A%3Ab%3A%3Ac"),
                Arguments.of("Double colon iso6523",
                        new ParticipantIdentifier("0002:12345", "iso6523-actorid-upis"),
                        "iso6523-actorid-upis::0002:12345",
                        "iso6523-actorid-upis%3A%3A0002%3A12345"),
                Arguments.of("Double colon eHealth",
                        new ParticipantIdentifier("urn:poland:ncpb", "ehealth-actorid-qns"),
                        "ehealth-actorid-qns::urn:poland:ncpb",
                        "ehealth-actorid-qns%3A%3Aurn%3Apoland%3Ancpb"),
                Arguments.of("Identifier with spaces -  formatted to uri with '%20",
                        new ParticipantIdentifier("urn ncpb test", "ehealth-actorid-qns"),
                        "ehealth-actorid-qns::urn ncpb test",
                        "ehealth-actorid-qns%3A%3Aurn%20ncpb%20test")
        );
    }

    ParticipantIdentifierFormatter testInstance = new ParticipantIdentifierFormatter();
    Pattern customSplitPattern = Pattern.compile("^(?i)\\s*?(?<scheme>urn:ehealth:partyid-type)::?(?<identifier>.+)?\\s*$");

    @BeforeEach
    void setUp() {
        // the regular expression is important for the firs test 'urn example with split regular expression'
        testInstance.setCustomURNSplitRegularExpression(customSplitPattern);
    }

    @ParameterizedTest(name = "{index}: {0}")
    @MethodSource("partyIdentifierTestArguments")
    void testFormat(String name, ParticipantIdentifier participantIdentifierType, String formattedIdentifier, String uriFormattedIdentifier) {

        String result = testInstance.format(participantIdentifierType);
        String uriResult = testInstance.urlEncodedFormat(participantIdentifierType);

        assertEquals(formattedIdentifier, result);
        assertEquals(uriFormattedIdentifier, uriResult);
    }

    @ParameterizedTest(name = "{index}: {0}")
    @MethodSource("partyIdentifierTestArguments")
    void testNormalizeIdentifier(String name, ParticipantIdentifier participantIdentifierType, String formattedIdentifier, String uriFormattedIdentifier) {

        ParticipantIdentifier result = testInstance.normalizeIdentifier(formattedIdentifier);
        assertEquals(participantIdentifierType, result);
    }
}
