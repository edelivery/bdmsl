/*-
 * #START_LICENSE#
 * bdmsl-common
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.logging.impl;

import eu.europa.ec.bdmsl.AbstractTest;
import eu.europa.ec.bdmsl.common.exception.Severity;
import eu.europa.ec.bdmsl.common.logging.ILogger;
import eu.europa.ec.bdmsl.common.util.LogEvents;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.*;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.Property;
import org.apache.logging.log4j.core.filter.ThresholdFilter;
import org.apache.logging.log4j.core.layout.PatternLayout;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Flavio SANTOS
 * @since 3.1.1
 */
public class LoggingServiceImplTest extends AbstractTest {
    @Autowired
    private ILogger loggerBusiness;


    private static TestAppender appender;

    @BeforeAll
    public static void setupClass() {

        Logger businessLogger = (Logger) LogManager.getLogger(LoggerBusinessImpl.class);
        Logger securityLogger = (Logger) LogManager.getLogger(LoggerSecurityImpl.class);

        removeAllAppenders(businessLogger);
        removeAllAppenders(securityLogger);

        businessLogger.setLevel(Level.INFO);
        securityLogger.setLevel(Level.INFO);

        ThresholdFilter thresholdFilter = ThresholdFilter.createFilter(Level.INFO, Filter.Result.ACCEPT, Filter.Result.ACCEPT);

        appender = new TestAppender("TestAppenderForUnitTests", thresholdFilter,
                PatternLayout.createDefaultLayout(),
                true,
                Property.EMPTY_ARRAY);

        businessLogger.addAppender(appender);
        securityLogger.addAppender(appender);
    }

    @BeforeEach
    public void beforeTest() {
        appender.log.clear();
    }

    public static void removeAllAppenders(Logger logger) {
        Map<String, Appender> mp = logger.getAppenders();

        mp.forEach((name, appd) -> {
            logger.removeAppender(appd);
        });
    }


    public static void printAllAppenders(Logger logger) {
        Map<String, Appender> mp = logger.getAppenders();

        mp.forEach((name, appd) -> {
            System.out.println("have appender: " + name);
        });
    }

    @Test
    @Disabled("TODO: Error since JAKARTA EE migration")
    public void securityLog1() {
        loggingService.securityLog(LogEvents.SEC_CERTIFICATE_EXPIRED, "1", "2", "3");

        assertEquals(1, appender.getLog().size());
        assertEquals("[SECURITY - SEC-006] Certificate is not valid at the current date 1. Certificate valid from 2 to 3", appender.getLog().get(0).getMessage());
        assertEquals("INFO", appender.getLog().get(0).getLevel());
    }

    @Test
    @Disabled("TODO: Error since JAKARTA EE migration")
    public void businessLog1() {
        loggingService.businessLog(LogEvents.BUS_SMP_READ, "SMP-TEST");

        assertEquals(1, appender.getLog().size());
        assertEquals("[BUSINESS - BUS-004] The following SMP was read: SMP-TEST.", appender.getLog().get(0).getMessage());
        assertEquals("INFO", appender.getLog().get(0).getLevel());
    }

    @Test
    public void businessLog2() {
        loggingService.businessLog(Severity.ERROR, LogEvents.BUS_MIGRATE_FAILED, "brazil-qns-id");

        assertEquals(1, appender.getLog().size());
        assertEquals("[BUSINESS - BUS-023] The call to migrate service failed for participant: brazil-qns-id.", appender.getLog().get(0).getMessage());
        assertEquals("ERROR", appender.getLog().get(0).getLevel());
    }

    @Test
    public void businessLog3() {
        loggingService.businessLog(LogEvents.BUS_CONFIGURATION_ERROR, new Exception("Exception Message"));

        assertEquals(1, appender.getLog().size());
        assertEquals("[BUSINESS - BUS-034] Error while configuring the application.", appender.getLog().get(0).getMessage());
        assertEquals("ERROR", appender.getLog().get(0).getLevel().toString());
    }

    @Test
    public void businessLogForNotHandledSeverities() {
        Severity[] severities = new Severity[]{Severity.FATAL, Severity.DEBUG, Severity.TRACE};
        for (Severity severity : severities) {
            loggingService.businessLog(severity, LogEvents.BUS_MIGRATE_FAILED, "123-qns-id");
            assertEquals(0, appender.getLog().size());
        }
    }

    @Test
    public void businessLogForHandledSeverities() {
        Severity[] severities = new Severity[]{Severity.ERROR, Severity.INFO, Severity.WARN};
        for (Severity severity : severities) {
            loggingService.businessLog(severity, LogEvents.BUS_MIGRATE_FAILED, "123-qns-id");
            assertEquals(1, appender.getLog().size());
            removeAllAppenders((Logger) ((LoggerBusinessImpl) loggerBusiness).getLogger());
        }
    }

    private static class TestAppender extends AbstractAppender {

        private final List<PrivateLogEvent> log = new ArrayList<>();

        protected TestAppender(String name, Filter filter, Layout layout, boolean ignoreExceptions, Property[] properties) {
            super(name, filter, layout, ignoreExceptions, properties);
        }

        @Override
        public void append(final LogEvent loggingEvent) {
            log.add(new PrivateLogEvent(loggingEvent.getMessage().getFormattedMessage(), loggingEvent.getLevel().name()));
        }

        public List<PrivateLogEvent> getLog() {
            return new ArrayList<>(log);
        }
    }

    private static class PrivateLogEvent {
        String message;
        String level;

        public PrivateLogEvent(String message, String level) {
            this.message = message;
            this.level = level;
        }

        public String getMessage() {
            return message;
        }

        public String getLevel() {
            return level;
        }
    }
}
