/*-
 * #START_LICENSE#
 * bdmsl-common
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.identifier;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Joze Rihtarsic
 * @since 4.3
 */
class ParticipantIdentifierTest {

    private static Stream<Arguments> participantIdentifierCases() {
        return Stream.of(
                Arguments.of(
                        "Equal party identifier",
                        true,
                        "iso6523-actorid-upis", "0002:12345",
                        "iso6523-actorid-upis", "0002:12345"
                ),
                Arguments.of(
                        "Identifier Schema mismatch ",
                        false,
                        "iso6523-not-equal", "0002:12345",
                        "iso6523-actorid-upis", "0002:12345"
                ),
                Arguments.of(
                        "Identifier value mismatch ",
                        false,
                        "iso6523-actorid-upis", "0002:12345",
                        "iso6523-actorid-upis", "0002:NotEqual"
                ),
                Arguments.of(
                        "Case party identifier",
                        false,
                        "iso6523-actorid-UPIS", "0002:12345",
                        "iso6523-actorid-upis", "0002:12345"
                ),
                Arguments.of(
                        "Case party identifier2",
                        false,
                        "iso6523-actorid-upis", "0002:12345a",
                        "iso6523-actorid-upis", "0002:12345A"
                )
                );
    }

    @ParameterizedTest(name = "{index}: {0}")
    @MethodSource("participantIdentifierCases")
    void testEqual(String name, boolean result, String scheme1, String value1, String scheme2, String value2) {
        ParticipantIdentifier participantIdentifier1 = new ParticipantIdentifier(value1, scheme1);
        ParticipantIdentifier participantIdentifier2 = new ParticipantIdentifier(value2, scheme2);

        assertEquals(result, participantIdentifier1.equals(participantIdentifier2));
    }

    @ParameterizedTest(name = "{index}: {0}")
    @MethodSource("participantIdentifierCases")
    void testHashValue(String name, boolean result, String scheme1, String value1, String scheme2, String value2) {
        ParticipantIdentifier participantIdentifier1 = new ParticipantIdentifier(value1, scheme1);
        ParticipantIdentifier participantIdentifier2 = new ParticipantIdentifier(value2, scheme2);

        assertEquals(result, participantIdentifier1.hashCode() == participantIdentifier2.hashCode());
    }

    @Test
    void testToString() {
        ParticipantIdentifier participantIdentifier1 = new ParticipantIdentifier("0002:12345", "iso6523-actorid-upis");
        String result = participantIdentifier1.toString();

        MatcherAssert.assertThat(result, CoreMatchers.containsString(participantIdentifier1.getIdentifier()));
        MatcherAssert.assertThat(result, CoreMatchers.containsString(participantIdentifier1.getScheme()));
    }

}
