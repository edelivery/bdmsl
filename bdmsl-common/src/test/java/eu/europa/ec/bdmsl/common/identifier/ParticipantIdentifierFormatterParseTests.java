/*-
 * #START_LICENSE#
 * bdmsl-common
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.identifier;


import eu.europa.ec.dynamicdiscovery.exception.MalformedIdentifierException;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Joze Rihtarsic
 * @since 4.3
 */
class ParticipantIdentifierFormatterParseTests {
    private static Stream<Arguments> partyIdentifierTestArguments() {
        return Stream.of(
                Arguments.of("ebCore unregistered", false, "urn:oasis:names:tc:ebcore:partyid-type:unregistered:domain:ec.europa.eu", "urn:oasis:names:tc:ebcore:partyid-type:unregistered:domain", "ec.europa.eu"),
                Arguments.of("ebCore iso6523", false, "urn:oasis:names:tc:ebcore:partyid-type:iso6523:0088:123456789", "urn:oasis:names:tc:ebcore:partyid-type:iso6523:0088", "123456789"),
                Arguments.of("ebCore with space 1", false, " urn:oasis:names:tc:ebcore:partyid-type:iso6523:0088:123456789", "urn:oasis:names:tc:ebcore:partyid-type:iso6523:0088", "123456789"),
                Arguments.of("ebCore with space 2", false, "urn:oasis:names:tc:ebcore:partyid-type:iso6523:0088:123456789 ", "urn:oasis:names:tc:ebcore:partyid-type:iso6523:0088", "123456789"),
                Arguments.of("ebCore with space 3", false, "urn:oasis:names:tc:ebcore:partyid-type:iso6523:0088::123456789 ", "urn:oasis:names:tc:ebcore:partyid-type:iso6523:0088", "123456789"),
                Arguments.of("ebCore unregistered with urn and colons", false, "urn:oasis:names:tc:ebcore:partyid-type:unregistered:ehealth:urn:ehealth:pl:ncp-idp", "urn:oasis:names:tc:ebcore:partyid-type:unregistered:ehealth", "urn:ehealth:pl:ncp-idp"),
                Arguments.of("ebCore unregistered with dash", false, "urn:oasis:names:tc:ebcore:partyid-type:unregistered:ehealth:pl:ncp-idp", "urn:oasis:names:tc:ebcore:partyid-type:unregistered:ehealth", "pl:ncp-idp"),
                Arguments.of("ebCore unregistered example double colon", false, "urn:oasis:names:tc:ebcore:partyid-type:unregistered::blue-gw", "urn:oasis:names:tc:ebcore:partyid-type:unregistered", "blue-gw"),
                Arguments.of("ebCore unregistered example", false, "urn:oasis:names:tc:ebcore:partyid-type:unregistered:blue-gw", "urn:oasis:names:tc:ebcore:partyid-type:unregistered", "blue-gw"),
                Arguments.of("ebCore unregistered domain example", false, "urn:oasis:names:tc:ebcore:partyid-type:unregistered:ec.europa.eu", "urn:oasis:names:tc:ebcore:partyid-type:unregistered", "ec.europa.eu"),
                Arguments.of("ebCore unregistered email scheme example", false, "urn:oasis:names:tc:ebcore:partyid-type:unregistered:email:test@my.mail.com", "urn:oasis:names:tc:ebcore:partyid-type:unregistered:email", "test@my.mail.com"),
                Arguments.of("ebCore unregistered email example", false, "urn:oasis:names:tc:ebcore:partyid-type:unregistered:test@my.mail.com", "urn:oasis:names:tc:ebcore:partyid-type:unregistered", "test@my.mail.com"),
                Arguments.of("ebCore with double colon", false, " urn:oasis:names:tc:ebcore:partyid-type:iso6523:0088::123456789", "urn:oasis:names:tc:ebcore:partyid-type:iso6523:0088", "123456789"),
                Arguments.of("ebCore with double colon start", false, " ::urn:oasis:names:tc:ebcore:partyid-type:iso6523:0088:123456789", "urn:oasis:names:tc:ebcore:partyid-type:iso6523:0088", "123456789"),
                Arguments.of("Double colon basic", false, "a-a-a::b", "a-a-a", "b"),
                Arguments.of("Double colon twice", false, "a-a-a::b::c", "a-a-a", "b::c"),
                Arguments.of("Double colon iso6523", false, "iso6523-actorid-upis::0002:gutek", "iso6523-actorid-upis", "0002:gutek"),
                Arguments.of("Double colon ehealth", false, "ehealth-actorid-qns::urn:poland:ncpb", "ehealth-actorid-qns", "urn:poland:ncpb"),
                Arguments.of("Double colon ehealth 2", false, "ehealth-actorid-qns::urn:ehealth:hr:ncpb-idp", "ehealth-actorid-qns", "urn:ehealth:hr:ncpb-idp"),
                Arguments.of("Double colon ehealth 3", false, "ehealth-actorid-qns::urn:ehealth:pt:ncpb-idp", "ehealth-actorid-qns", "urn:ehealth:pt:ncpb-idp"),
                Arguments.of("Double colon custom scheme", false, "test-test-test::urn:ehealth:be:ncpb-idp", "test-test-test", "urn:ehealth:be:ncpb-idp"),
                Arguments.of("ebCore iso6523", true, "urn:oasis:names:tc:ebcore:partyid-type:iso6523:Illegal-value-without-scheme", null, null),
                Arguments.of("ebCore with no catalog", true, " urn:oasis:names:tc:ebcore:partyid-type:0088123456789", null, null)
        );
    }

    ParticipantIdentifierFormatter testInstance = new ParticipantIdentifierFormatter();

    @ParameterizedTest(name = "{index}: {0}")
    @MethodSource("partyIdentifierTestArguments")
    void testPartyIdentifierParse(String name, boolean throwError, String identifier, String schemaPart, String idPart) {
        MalformedIdentifierException exception = null;
        ParticipantIdentifier result = null;
        if (throwError) {
            exception = assertThrows(MalformedIdentifierException.class, () -> testInstance.parse(identifier));
        } else {
            result = testInstance.parse(identifier);
        }

        assertNotNull(throwError ? exception : result);
        if (!throwError) {
            assertEquals(schemaPart, result.getScheme());
            assertEquals(idPart, result.getIdentifier());
        }
    }
}
