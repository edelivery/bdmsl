/*-
 * #START_LICENSE#
 * bdmsl-common
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.exception;

/**
 * @author Adrien FERIAL
 * @since 12/06/2015
 */
public abstract class TechnicalException extends Exception implements IBDMSLException {

    /**
     * UUID for the serialization.
     */
    private static final long serialVersionUID = -2888388417094351293L;

    /**
     * Exception code
     */
    private int code;


    public TechnicalException(ErrorCode code, String message) {
        super(message);
        this.code = code.getErrorCode();
    }

    public TechnicalException(ErrorCode code, String message, Throwable t) {
        super(message, t);
        this.code = code.getErrorCode();
    }

    public TechnicalException(Throwable cause) {
        super(cause);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCode() {
        return code;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return "[ERR-" + code + "] " + getMessageWithoutErrorCode();
    }

    @Override
    public String getMessageWithoutErrorCode() {
        return super.getMessage();
    }
}
