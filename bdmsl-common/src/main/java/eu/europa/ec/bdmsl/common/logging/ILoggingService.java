/*-
 * #START_LICENSE#
 * bdmsl-common
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.logging;

import eu.europa.ec.bdmsl.common.exception.Severity;

/**
 * @author Adrien FERIAL
 * @since 15/06/2015
 */
public interface ILoggingService {

    void securityLog(String code, String... params);

    void businessLog(String code, String... params);

    void businessLog(Severity severity, String code, String... params);

    void businessLog(String code, Exception t, String... params);

    void info(String message);

    void debug(String message);

    void warn(String message);

    void warn(String message, Exception throwable);

    void error(String message, Exception throwable);

    /**
     * Note that the underlying MDC is managed on a per thread basis.
     * If the current thread does not have a context map it is
     * created as a side effect.
     *
     * @param key   the key identifier
     * @param value context value as identified with the <code>key</code>
     */
    void putMDC(String key, String value);
}
