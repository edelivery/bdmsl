/*-
 * #START_LICENSE#
 * bdmsl-common
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.util;

import eu.europa.ec.bdmsl.common.exception.KeyException;

/**
 * @author Flavio SANTOS
 */
public class PrivateKeyGenerator {

    public static void main(String[] args) throws Exception {
        System.out.println("\n\n## PRIVATE KEY GENERATOR ##");
        System.out.println("\nRequired parameter:");
        System.out.println("\n[1]-Full directory path with privateKey file name (eg: /home/user/private.key)");

        if (args.length == 0) {
            throw new KeyException("Check required parameter, null/empty value is not allowed.");
        }

        KeyUtil.generatePrivateSymmetricKey(args[0]);
        System.out.println(String.format("Private key file created at %s", args[0]));
    }
}
