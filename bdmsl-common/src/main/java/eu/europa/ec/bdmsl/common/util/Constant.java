/*-
 * #START_LICENSE#
 * bdmsl-common
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.util;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Locale;

/**
 * @author Adrien FERIAL
 * @since 03/11/2015
 */
public class Constant {

    public static final Locale LOCALE = Locale.US;

    public static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;

    public static final String EXCEPTION_HANDLER_ERROR_CODE_STR = "EXCEPTION_HANDLER_ERROR_CODE";

    public static final String LOCAL_HOST= "localhost";

    public static final String DEF_PROPERTY_VALUE_VALIDATION_REGEXP = ".{0,2000}";

    public static final String INVALID_IPV_4_ADDRESS = "Invalid IPv4 address : ";

    public static final String FAILED_TO_BUILD_DNS_NAME_FROM = "Failed to build DNS Name from '";
}
