/*-
 * #START_LICENSE#
 * bdmsl-common
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.util;

import eu.europa.ec.bdmsl.common.exception.KeyException;
import org.springframework.util.StringUtils;

import javax.crypto.*;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Arrays;
import java.util.Base64;


/**
 * @author Flavio SANTOS
 */
public class KeyUtil {

    public static final String ALGORITHM_KEY = "AES";

    public static final String ALGORITHM_ENCRYPTION = "AES/GCM/NoPadding";

    public static final String ALGORITHM_ENCRYPTION_OBSOLETE = "AES/CBC/PKCS5Padding";

    public static final int KEY_SIZE = 256;

    public static final int GCM_TAG_LENGTH_BIT = 128;

    // for te gcm iv size is 12
    public static final int IV_GCM_SIZE = 12;

    // NULL IV is for CBC which  has IV size 16!
    private static final IvParameterSpec NULL_IV = new IvParameterSpec(new byte[16]);

    public static void generatePrivateSymmetricKey(String path) throws KeyException {
        if (StringUtils.isEmpty(path)) {
            throw new KeyException("Full directory path must be not null.");
        }

        try {
            // Generates a random key
            KeyGenerator keyGenerator = KeyGenerator.getInstance(ALGORITHM_KEY);
            keyGenerator.init(KEY_SIZE);
            SecretKey privateKey = keyGenerator.generateKey();
            SecureRandom rnd = SecureRandom.getInstanceStrong();
            // Using setSeed(byte[]) to reseed a Random object
            byte[] seed = rnd.generateSeed(IV_GCM_SIZE);
            rnd.setSeed(seed);

            byte[] buffIV = new byte[IV_GCM_SIZE];
            rnd.nextBytes(buffIV);

            try (FileOutputStream out = new FileOutputStream(path)) {
                // first write IV
                out.write('#');
                out.write(buffIV);
                out.write('#');
                out.write(privateKey.getEncoded());
                out.flush();
            }
        } catch (Exception exc) {
            throw new KeyException(exc.getMessage(), exc);
        }
    }

    public static String encrypt(String path, String plainTextPassword) throws KeyException {
        return encrypt(path, plainTextPassword.getBytes());
    }

    public static String encrypt(String path, byte[] plainBytes) throws KeyException {
        try {
            byte[] buff = Files.readAllBytes(Paths.get(path));
            AlgorithmParameterSpec iv = getSaltParameter(buff);
            SecretKey privateKey = getSecretKey(buff);

            Cipher cipher = Cipher.getInstance(iv == NULL_IV ? ALGORITHM_ENCRYPTION_OBSOLETE : ALGORITHM_ENCRYPTION);
            cipher.init(Cipher.ENCRYPT_MODE, privateKey, iv);
            byte[] encryptedData = cipher.doFinal(plainBytes);
            return new String(Base64.getEncoder().encode(encryptedData));
        } catch (Exception exc) {
            throw new KeyException(exc.getMessage(), exc);
        }
    }

    public static String decryptToString(String path, String encryptedPassword) throws KeyException {
        byte[] decrypted = decrypt(path, encryptedPassword);
        return new String(decrypted);
    }

    public static byte[] decrypt(String path, String encryptedPassword) throws KeyException {
        try {
            byte[] buff = Files.readAllBytes(Paths.get(path));

            AlgorithmParameterSpec iv = getSaltParameter(buff);
            SecretKey privateKey = getSecretKey(buff);

            byte[] decodedEncryptedPassword = Base64.getDecoder().decode(encryptedPassword.getBytes());
            // this is for back-compatibility - if key parameter is IV than is CBC else ie GCM
            Cipher cipher = Cipher.getInstance(iv instanceof IvParameterSpec ? ALGORITHM_ENCRYPTION_OBSOLETE : ALGORITHM_ENCRYPTION);
            cipher.init(Cipher.DECRYPT_MODE, privateKey, iv);
            return cipher.doFinal(decodedEncryptedPassword);
        } catch (BadPaddingException | IllegalBlockSizeException ibse) {
            throw new KeyException("Either private key or encrypted password might not be correct. Please check both.", ibse);
        } catch (Exception exc) {
            throw new KeyException(exc.getMessage(), exc);
        }
    }

    public static AlgorithmParameterSpec getSaltParameter(byte[] buff) {
        AlgorithmParameterSpec iv;
        // this is for back compatibility  - older versions were using "CBC" with IV to null
        // the GCM  is a new enforced algorithm  where GCM salt parameter
        if (buff[0] == '#' && buff[IV_GCM_SIZE + 1] == '#') {
            iv = new GCMParameterSpec(GCM_TAG_LENGTH_BIT, Arrays.copyOfRange(buff, 1, IV_GCM_SIZE + 1));
        } else {
            iv = NULL_IV;
        }
        return iv;
    }

    public static SecretKey getSecretKey(byte[] buff) {
        byte[] skey;
        if (buff[0] == '#') {
            // EAS Key value is after salt value following the patter: // #salt#key
            skey = Arrays.copyOfRange(buff, IV_GCM_SIZE + 2, buff.length);
        } else {
            skey = buff;
        }

        return new SecretKeySpec(skey, ALGORITHM_KEY);
    }
}
