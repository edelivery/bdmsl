/*-
 * #START_LICENSE#
 * bdmsl-common
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.logging.impl;

import eu.europa.ec.bdmsl.common.exception.Severity;
import eu.europa.ec.bdmsl.common.logging.ILogEvent;
import eu.europa.ec.bdmsl.common.logging.ILogger;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Adrien FERIAL
 * @since 16/07/2015
 */
public abstract class AbstractLoggerImpl implements ILogger {

    @Autowired
    private ILogEvent logEvent;

    protected abstract Logger getLogger();

    @Override
    public void categoryLog(Severity severity, String category, String code, Exception t, String... params) {
        String message = logEvent.getMessage(code);
        if (params != null) {
            try {
                message = String.format(message, (Object[]) params);
            } catch (final Exception exception) {
                getLogger().debug("Wrong use of the logging message of code " + code);
                message = logEvent.getMessage(code);
            }
        }

        if (Severity.WARN.equals(severity)) {
            getLogger().warn("[" + category + " - " + code + "] " + message);
        } else if (Severity.INFO.equals(severity)) {
            getLogger().info("[" + category + " - " + code + "] " + message);
        } else if (Severity.ERROR.equals(severity)) {
            getLogger().error("[" + category + " - " + code + "] " + message);
        }
    }
}
