/*-
 * #START_LICENSE#
 * bdmsl-common
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.service;

import eu.europa.ec.bdmsl.common.logging.ILoggingService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.InetAddress;

/**
 * Abstract class with common functions for the service beans!
 *
 * @author Adrien FERIAL
 * @since 3.0
 */

public abstract class AbstractServiceImpl {
    // environment variable for the server name
    public static final String ENV_SERVER_NAME = "SERVER_NAME";

    @Autowired
    protected ILoggingService loggingService;

    /**
     * Method returns the current server hostname
     *
     * @return server hostname
     */
    public String getServerAddress() {
        String serverAddress = StringUtils.EMPTY;
        try {
            serverAddress = InetAddress.getLocalHost().getHostName();
        } catch (Exception e) {
            loggingService.error(e.getMessage(), e);
        }
        return serverAddress;
    }

    public String getServerName() {
        return System.getenv(ENV_SERVER_NAME);
    }
}
