/*-
 * #START_LICENSE#
 * bdmsl-webapp
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.util;

import eu.europa.ec.bdmsl.common.exception.SMLRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.PublicKey;

/**
 * Key utils class provides utility methods for parsing and extracting information from keys.
 * The similar code was provided in the Apache Santuario library by the author of this class.
 * The class can be removed when XMLSEC 3.+ is used. and replaced by the
 * DERDecoderUtils.
 *
 * @author Joze RIHTARSIC
 * @since 5.0
 */
public class KeyUtils {
    private static final Logger LOG = LoggerFactory.getLogger(KeyUtils.class);

    /**
     * DER type identifier for a sequence value
     */
    public static final byte TYPE_SEQUENCE = 0x30;
    /**
     * DER type identifier for ASN.1 "OBJECT IDENTIFIER" value.
     */
    public static final byte TYPE_OBJECT_IDENTIFIER = 0x06;

    /**
     * Simple method parses an ASN.1 encoded byte array. The encoding uses "DER", a BER/1 subset, that means a triple { typeId, length, data }.
     * with the following structure:
     * <p>
     * <pre>
     *  PublicKeyInfo ::= SEQUENCE {
     *      algorithm   AlgorithmIdentifier,
     *      PublicKey   BIT STRING
     *  }
     * </pre>
     * <p>
     * Where AlgorithmIdentifier is formatted as:
     * <pre>
     *  AlgorithmIdentifier ::= SEQUENCE {
     *      algorithm   OBJECT IDENTIFIER,
     *      parameters  ANY DEFINED BY algorithm OPTIONAL
     *  }
     * </pre>
     *
     * @param derEncodedIS the DER-encoded input stream to decode.
     * @throws SMLRuntimeException in case of decoding error or if given InputStream is null or empty.
     * @throws IOException         if an I/O error occurs.
     */
    public static byte[] getAlgorithmIdBytes(InputStream derEncodedIS) throws SMLRuntimeException, IOException {
        if (derEncodedIS == null || derEncodedIS.available() <= 0) {
            throw new SMLRuntimeException("DER decoding error: Null data");
        }

        validateType(derEncodedIS.read(), TYPE_SEQUENCE);
        readLength(derEncodedIS);
        validateType(derEncodedIS.read(), TYPE_SEQUENCE);
        readLength(derEncodedIS);

        return readObjectIdentifier(derEncodedIS);
    }

    /**
     * Read the next object identifier from the given DER-encoded input stream.
     * <p>
     *
     * @param derEncodedIS the DER-encoded input stream to decode.
     * @return the object identifier as a byte array.
     * @throws SMLRuntimeException if parse error occurs.
     */
    public static byte[] readObjectIdentifier(InputStream derEncodedIS) throws SMLRuntimeException {
        try {
            validateType(derEncodedIS.read(), TYPE_OBJECT_IDENTIFIER);
            int length = readLength(derEncodedIS);
            LOG.debug("DER decoding algorithm id bytes");
            return readFromStream(derEncodedIS, length);
        } catch (IOException ex) {
            throw new SMLRuntimeException("Error occurred while reading the input stream.", ex);
        }
    }

    /**
     * The method extracts the algorithm OID from the public key and returns it as "dot encoded" OID string.
     * if the key is encapsulated in '1.2.840.10045.2.1' it check if eliptic curve is EDDSA type and returns the eddsa algorithm.
     *
     * @param publicKey the public key for which method returns algorithm ID.
     * @return String representing the algorithm ID.
     * @throws SMLRuntimeException if the algorithm ID cannot be determined.
     */
    public static String getAlgorithmIdFromPublicKey(PublicKey publicKey) throws SMLRuntimeException {
        String keyFormat = publicKey.getFormat();
        if (!("X.509".equalsIgnoreCase(keyFormat)
                || "X509".equalsIgnoreCase(keyFormat))) {
            throw new SMLRuntimeException("Unknown key format [" + keyFormat
                    + "]! Support for X.509-encoded public keys only!");
        }
        try (InputStream inputStream = new ByteArrayInputStream(publicKey.getEncoded())) {
            byte[] keyAlgOidBytes = getAlgorithmIdBytes(inputStream);
            String alg = decodeOID(keyAlgOidBytes);
            if (alg.equals("1.2.840.10045.2.1")) {
                keyAlgOidBytes = readObjectIdentifier(inputStream);
                String newAlg = decodeOID(keyAlgOidBytes);
                if (newAlg.startsWith("1.3.101.11")) {
                    // return the new algorithm only for EDDSA keys
                    // else return generic ECDSA algorithm
                    alg = newAlg;

                }
            }
            return alg;
        } catch (IOException ex) {
            throw new SMLRuntimeException("Error reading public key", ex);
        }
    }

    private static void validateType(int iType, byte expectedType) throws SMLRuntimeException {
        validateType((byte) (iType & 0xFF), expectedType);
    }

    private static void validateType(byte type, byte expectedType) throws SMLRuntimeException {
        if (type != expectedType) {
            throw new SMLRuntimeException("DER decoding error: Expected type [" + expectedType + "] but got [" + type + "]");
        }
    }

    /**
     * Get the DER length at the current position.
     * <p>
     * DER length is encoded as
     * <ul>
     * <li>If the first byte is 0x00 to 0x7F, it describes the actual length.
     * <li>If the first byte is 0x80 + n with 0<n<0x7F, the actual length is
     * described in the following 'n' bytes.
     * <li>The length value 0x80, used only in constructed types, is
     * defined as "indefinite length".
     * </ul>
     *
     * @return the length, -1 for indefinite length.
     * @throws SMLRuntimeException if the current position is at the end of the array or there is
     *                             an incomplete length specification.
     * @throws IOException         if an I/O error occurs.
     */
    public static int readLength(InputStream derEncodedIs) throws SMLRuntimeException, IOException {
        if (derEncodedIs.available() <= 0) {
            throw new SMLRuntimeException("Invalid DER format");
        }

        int value = derEncodedIs.read();

        if ((value & 0x080) == 0x00) { // short form, 1 byte size
            return value;
        }
        // number of bytes used to encode length
        int byteCount = value & 0x07f;
        //byteCount == 0 indicates indefinite length encoded data.
        if (byteCount == 0) {
            return -1;
        }

        // byteCount > 4 not able to handle more than 4Gb of data (max int size) tmp > 4 indicates.
        if (byteCount > 4) {
            throw new SMLRuntimeException("Data length byte size: [" + byteCount + "] is incorrect/too big");
        }
        byte[] intSizeBytes = readFromStream(derEncodedIs, byteCount);
        return new BigInteger(1, intSizeBytes).intValue();
    }

    /**
     * The first two nodes of the OID are encoded onto a single byte.
     * The first node is multiplied by the decimal 40 and the result is added to the value of the second node.
     * Node values less than or equal to 127 are encoded in one byte.
     * Node values greater than or equal to 128 are encoded on multiple bytes.
     * Bit 7 of the leftmost byte is set to one. Bits 0 through 6 of each byte contains the encoded value.
     *
     * @param oidBytes the byte array containing the OID
     * @return the decoded OID as a string
     */
    public static String decodeOID(byte[] oidBytes) {

        int length = oidBytes.length;
        StringBuilder sb = new StringBuilder(length * 4);

        int fromPos = 0;
        for (int i = 0; i < length; i++) {
            // if the 8th bit is set, it means the next byte is part of the current segment
            if ((oidBytes[i] & 0x80) != 0) {
                continue;
            }

            // decode the OID segment
            long decodedValue = decodeBytes(oidBytes, fromPos, i - fromPos + 1);
            if (fromPos == 0) {
                // first OID segment consists of two numbers
                if (decodedValue < 80) {
                    sb.append(decodedValue / 40);
                    decodedValue = decodedValue % 40;
                } else {
                    sb.append('2');
                    decodedValue = decodedValue - 80;
                }
            }

            //add next OID segment
            sb.append('.');
            sb.append(decodedValue);
            fromPos = i + 1;
        }
        return sb.toString();
    }

    /**
     * Decode a byte array into a long value. The most significant bit of each  byte is ignored because it
     * is used as a continuation flag. Bits are shifted to the left so that the most significant 7 bits are in first byte
     * next 7 bits in second byte and so on.
     *
     * @param inBytes the input byte array
     * @param iOffset start point inside <code>inBytes</code>
     * @param iLength number of bytes to decode
     * @return long value decoded from the byte array.
     */
    private static long decodeBytes(byte[] inBytes, int iOffset, int iLength) {
        // check if the OID segment is too big to decode with long value!
        if (iLength > 8) {
            throw new IllegalArgumentException("OID segment too long to parse: [" + iLength + "]");
        }
        if (iLength > 1) {
            int iSteps = iLength - 1;
            return ((long) (inBytes[iOffset] & 0x07f) << 7 * iSteps)
                    + decodeBytes(inBytes, iOffset + 1, iSteps);
        } else {
            return inBytes[iOffset] & 0x07f;
        }
    }

    private static byte[] readFromStream(InputStream is, int length) throws IOException {
        byte[] buff = new byte[length];
        is.read(buff);
        return buff;
    }
}
