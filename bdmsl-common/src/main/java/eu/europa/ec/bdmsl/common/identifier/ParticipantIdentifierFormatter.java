/*-
 * #START_LICENSE#
 * bdmsl-common
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.identifier;

import eu.europa.ec.dynamicdiscovery.model.identifiers.AbstractIdentifierFormatter;
import eu.europa.ec.dynamicdiscovery.model.identifiers.types.EBCorePartyIdFormatterType;
import eu.europa.ec.dynamicdiscovery.model.identifiers.types.PeppolPartyIdFormatterType;
import eu.europa.ec.dynamicdiscovery.model.identifiers.types.URNFormatterType;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Formatter for the ParticipantIdentifier with default "ebCoreParty" split regular expression and
 * '::' as split separator. For details see the {@link AbstractIdentifierFormatter}
 *
 * @author Joze Rihtarsic
 * @since @since 4.3
 */
public class ParticipantIdentifierFormatter extends AbstractIdentifierFormatter<ParticipantIdentifier> {
    private static final Logger LOG = LoggerFactory.getLogger(ParticipantIdentifierFormatter.class);

    public ParticipantIdentifierFormatter() {
        this.addFormatterTypes(new EBCorePartyIdFormatterType());
        setDefaultFormatter(new PeppolPartyIdFormatterType());
        setSchemeMandatory(true);
    }

    public void setWildcardEnabled(boolean enable) {
        this.formatterTypes.forEach(formatterType -> formatterType.setWildcardEnabled(enable));
        getDefaultFormatter().setWildcardEnabled(enable);
    }

    public void setValueValidationPattern(String participantIdRegex) {
        getDefaultFormatter().setValueValidationPattern(StringUtils.isBlank(participantIdRegex) ? null : Pattern.compile(participantIdRegex));
    }

    @Override
    protected String getSchemeFromObject(ParticipantIdentifier object) {
        return object != null ? object.getScheme() : null;
    }

    @Override
    protected String getIdentifierFromObject(ParticipantIdentifier object) {
        return object != null ? object.getIdentifier() : null;
    }

    @Override
    protected ParticipantIdentifier createObject(String scheme, String identifier) {
        return new ParticipantIdentifier(identifier, scheme);
    }

    @Override
    protected void updateObject(ParticipantIdentifier identifierObject, String scheme, String identifier) {
        identifierObject.setScheme(scheme);
        identifierObject.setIdentifier(identifier);
    }

    /**
     * Set custom split regular expression for the URN formatter type. Method first removes all other URNFormatterTypes and then sets the new
     * URNFormatterType with the provided splitRegularExpression as the first formatter type.
     * If null is provided, then all URNFormatterType are removed.
     *
     * @param splitRegularExpression
     */
    public void setCustomURNSplitRegularExpression(Pattern splitRegularExpression) {
        LOG.info("Set split splitRegularExpression: [{}]", splitRegularExpression);
        List<URNFormatterType> list = formatterTypes.stream()
                .filter(formatterType -> formatterType instanceof URNFormatterType)
                .map(formatterType -> (URNFormatterType) formatterType).collect(Collectors.toList());

        formatterTypes.removeAll(list);

        if (splitRegularExpression != null) {
            LOG.info("add URNFormatterType with RegularExpression: [{}]", splitRegularExpression);
            formatterTypes.add(0, new URNFormatterType(splitRegularExpression));
        }
    }
}
