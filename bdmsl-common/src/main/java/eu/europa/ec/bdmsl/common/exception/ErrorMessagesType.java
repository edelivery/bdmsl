/*-
 * #START_LICENSE#
 * bdmsl-common
 * %%
 * Copyright (C) 2017 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.exception;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * In this Enumeration we define the error messages that can be thrown by the application.
 * We also define and check the number of arguments that should be passed to the message.
 * <p/>
 *
 * @author Thomas Dussart
 * @since 13/05/2024
 */

public enum ErrorMessagesType {
    PUBLISHER_ID_NOT_EXISTING("Publisher ID: [%s] does not exist!", 1),
    PUBLISHER_ID_NOT_BOUND("Publisher ID: [%s] is not bound to certificateId: [%s]!", 2),
    PUBLISHER_ID_NOT_REGISTERED("Publisher ID: [%s] is not registered to domain zone: [%s]!", 2),
    PUBLISHER_ID_NOT_DISABLED("Publisher ID: [%s] is not disabled and can not be deleted!", 1),
    PUBLISHER_ID_HAS_ENABLED_PARTICIPANT("Publisher ID: [%s] has [%s] enabled participant! Please disable them first!", 2),
    PUBLISHER_ID_ALREADY_ENABLED("Publisher ID: [%s] is already enabled!", 1),
    PUBLISHER_ID_ALREADY_DISABLED("Publisher ID: [%s] is already disabled!", 1),
    NOT_VALID_IP_ADDRESS("Value: [%s] is not valid IP address!", 1),
    NOT_VALID_DOMAIN_ADDRESS("Value: [%s] is not valid domain address!", 1),
    CANNOT_UPDATE_SMP_BECAUSE_NOT_EXISTING("Cannot update SMP with smp id [%s] because it does not exist!", 1),
    SMP_WAS_NOT_CREATED_WITH_THE_CERTIFICATE("The SMP [%s] was not created with the certificate [%s]", 2),
    NOT_AUTHORIZED_FOR_WILDCARD_SCHEME(" The certificate [%s] is not allowed to create wild card record for the scheme [%s]", 2),
    INVALID_DNS_NAME("Invalid DNS Name: [%s]!",1),
    INVALID_DNS_ZONE_NAME("Invalid DNS Zone Name: [%s]!",1),
    INVALID_DOMAIN_DNS_RECORD_TYPES("Invalid DNS recordTypes [%s] for the domain [%s]!",2);


    private static final Logger LOG = LoggerFactory.getLogger(ErrorMessagesType.class);
    private final String template;
    private final int argumentNumber;

    ErrorMessagesType(String message, int code) {
        this.template = message;
        this.argumentNumber = code;
    }

    public String getTemplate() {
        return template;
    }

    public String getMessage(String... args) {
        if (getArgumentNumber() != args.length) {
            String concatenatedArgs = String.join(" ", args);
            String errorMessage = String.format("The number of arguments does not match the number of placeholders in the template:[%s] -> concatenated arguments:[%s]", getTemplate(), concatenatedArgs);
            LOG.error(errorMessage);
            return errorMessage;
        }
        return String.format(getTemplate(), args);
    }

    public int getArgumentNumber() {
        return argumentNumber;
    }
}
