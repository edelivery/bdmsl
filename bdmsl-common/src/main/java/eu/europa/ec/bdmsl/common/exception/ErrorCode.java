/*-
 * #START_LICENSE#
 * bdmsl-common
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.exception;

/**
 * @author Adrien FERIAL
 * @since 16/06/2015
 */
public enum ErrorCode {
    SMP_NOT_FOUND_ERROR(100),
    UNAUTHORIZED_ERROR(101),
    CERTIFICATE_AUTHENTICATION_ERROR(102),
    ROOT_CERTIFICATE_ALIAS_NOT_FOUND_ERROR(103),
    CERTIFICATE_REVOKED_ERROR(104),
    GENERIC_TECHNICAL_ERROR(105),
    BAD_REQUEST_ERROR(106),
    DNS_CLIENT_ERROR(107),
    SIG0_ERROR(108),
    BAD_CONFIGURATION_ERROR(109),
    PARTICIPANT_NOT_FOUND_ERROR(110),
    MIGRATION_NOT_FOUND_ERROR(111),
    DUPLICATE_PARTICIPANT_ERROR(112),
    SMP_DELETE_ERROR(113),
    MIGRATION_PLANNED_ERROR(114),
    CERTIFICATE_NOT_FOUND_ERROR(115),
    CERTIFICATE_NOT_YET_VALID(116),
    CERTIFICATE_EXPIRED(117),
    NOT_FOUND_ERROR(118),
    INVALID_ARGUMENT_ERROR(119),
    SMP_OUT_OF_PARTICIPANT_QUOTA_EXCEPTION(120),
    SUBDOMAIN_OUT_OF_PARTICIPANT_QUOTA_EXCEPTION(121),
    SMP_MANAGE_ERROR(122);

    private final int errorCode;

    ErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }

}
