/*-
 * #START_LICENSE#
 * bdmsl-common
 * %%
 * Copyright (C) 2016 - 2024 European Commission | eDelivery | DomiSML
 * %%
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 * [PROJECT_HOME]\license\eupl-1.2\license.txt or https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * #END_LICENSE#
 */
package eu.europa.ec.bdmsl.common.logging.impl;

import eu.europa.ec.bdmsl.common.exception.Severity;
import eu.europa.ec.bdmsl.common.logging.ILogEvent;
import eu.europa.ec.bdmsl.common.logging.ILogger;
import eu.europa.ec.bdmsl.common.logging.ILoggingService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Adrien FERIAL
 * @since 15/06/2015
 */
@Service
public class LoggingServiceImpl implements ILoggingService {

    private Logger logger = LogManager.getLogger(LoggingServiceImpl.class);

    @Autowired
    private ILogger loggerBusiness;

    @Autowired
    private ILogger loggerSecurity;

    private void categoryLog(Severity severity, String category, String code, Exception t, String... params) {
        // Also log into the specific file
        if (ILogEvent.CATEGORY_BUSINESS.equals(category)) {
            loggerBusiness.categoryLog(severity, category, code, t, params);
        } else if (ILogEvent.CATEGORY_SECURITY.equals(category)) {
            loggerSecurity.categoryLog(severity, category, code, t, params);
        }
        if (t != null) {
            logger.error(t.getMessage(), t);
        }
    }

    @Override
    public void securityLog(String code, String... params) {
        categoryLog(Severity.INFO, ILogEvent.CATEGORY_SECURITY, code, null, params);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void businessLog(String code, String... params) {
        categoryLog(Severity.INFO, ILogEvent.CATEGORY_BUSINESS, code, null, params);
    }

    @Override
    public void businessLog(Severity severity, String code, String... params) {
        categoryLog(severity, ILogEvent.CATEGORY_BUSINESS, code, null, params);
    }

    @Override
    public void businessLog(String code, Exception t, String... params) {
        categoryLog(Severity.ERROR, ILogEvent.CATEGORY_BUSINESS, code, t, params);
    }

    @Override
    public void info(String message) {
        logger.info(message);
    }

    @Override
    public void debug(String message) {
        logger.debug(message);
    }

    @Override
    public void warn(String message) {
        logger.warn(message);
    }

    @Override
    public void warn(String message, Exception t) {
        logger.warn(message, t);
    }

    @Override
    public void error(String message, Exception t) {
        logger.error(message, t);
    }

    @Override
    public void putMDC(String key, String value) {
        ThreadContext.put(key, value);
    }

}
