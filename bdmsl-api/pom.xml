<?xml version="1.0" encoding="UTF-8"?>
<!--
(C) Copyright 2023 - European Commission | CEF eDelivery

Licensed under the EUPL, Version 1.2 (the "License");
You may not use this file except in compliance with the License.
You may obtain a copy of the License at

\BDMSL\bdmsl-parent-pom\LICENSE-EUPL-v1.2.pdf or https://joinup.ec.europa.eu/sites/default/files/custom-page/attachment/eupl_v1.2_en.pdf

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>eu.europa.ec.bdmsl</groupId>
        <artifactId>bdmsl-builder</artifactId>
        <version>5.0-SNAPSHOT</version>
        <relativePath>../pom.xml</relativePath>
    </parent>
    <artifactId>bdmsl-api</artifactId>
    <packaging>jar</packaging>
    <name>bdmsl-api</name>
    <licenses>
        <license>
            <name>EUPL-1.2</name>
            <url>https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
            </url>
        </license>
    </licenses>
    <properties>
        <wsdlLocationDir>${basedir}/src/main/resources</wsdlLocationDir>
    </properties>

    <dependencies>
        <dependency>
            <groupId>jakarta.jws</groupId>
            <artifactId>jakarta.jws-api</artifactId>
        </dependency>
        <dependency>
            <groupId>jakarta.xml.ws</groupId>
            <artifactId>jakarta.xml.ws-api</artifactId>
        </dependency>
        <dependency>
            <groupId>jakarta.xml.bind</groupId>
            <artifactId>jakarta.xml.bind-api</artifactId>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-engine</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-params</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.cxf</groupId>
                <artifactId>cxf-codegen-plugin</artifactId>
                <executions>
                    <execution>
                        <id>generate-sources</id>
                        <phase>generate-sources</phase>
                        <configuration>
                            <sourceRoot>${basedir}/target/generated/src/main/java</sourceRoot>
                            <wsdlOptions>
                                <wsdlOption>
                                    <wsdl>${wsdlLocationDir}/BDMSLAdminService-1.0.wsdl
                                    </wsdl>
                                    <bindingFiles>
                                        <bindingFile>
                                            ${wsdlLocationDir}/BDMSLAdminService-1.0-jaxws.xml
                                        </bindingFile>
                                        <bindingFile>
                                            ${wsdlLocationDir}/commonServiceBinding.xjb
                                        </bindingFile>
                                    </bindingFiles>
                                    <extraargs>
                                        <extraarg>-xjc-Xannotate</extraarg>
                                    </extraargs>
                                </wsdlOption>

                                <wsdlOption>
                                    <wsdl>${wsdlLocationDir}/BDMSLMonitoringService-1.0.wsdl
                                    </wsdl>
                                    <bindingFiles>
                                        <bindingFile>
                                            ${wsdlLocationDir}/BDMSLMonitoringService-1.0-jaxws.xml
                                        </bindingFile>
                                        <bindingFile>
                                            ${wsdlLocationDir}/commonServiceBinding.xjb
                                        </bindingFile>
                                    </bindingFiles>
                                    <extraargs>
                                        <extraarg>-xjc-Xannotate</extraarg>
                                    </extraargs>
                                </wsdlOption>

                                <wsdlOption>
                                    <wsdl>${wsdlLocationDir}/BDMSLService-1.0.wsdl
                                    </wsdl>
                                    <bindingFiles>
                                        <bindingFile>
                                            ${wsdlLocationDir}/BDMSLService-1.0-jaxws.xml
                                        </bindingFile>
                                        <bindingFile>
                                            ${wsdlLocationDir}/commonServiceBinding.xjb
                                        </bindingFile>
                                    </bindingFiles>
                                    <extraargs>
                                        <extraarg>-xjc-Xannotate</extraarg>
                                    </extraargs>
                                </wsdlOption>
                                <wsdlOption>
                                    <wsdl>
                                        ${wsdlLocationDir}/ManageBusinessIdentifierService-1.0.wsdl
                                    </wsdl>
                                    <bindingFiles>
                                        <bindingFile>
                                            ${wsdlLocationDir}/ManageBusinessIdentifierService-1.0.jaxws.xml
                                        </bindingFile>
                                        <bindingFile>
                                            ${wsdlLocationDir}/commonServiceBinding.xjb
                                        </bindingFile>
                                    </bindingFiles>
                                    <extraargs>
                                        <extraarg>-xjc-Xannotate</extraarg>
                                    </extraargs>
                                </wsdlOption>
                                <wsdlOption>
                                    <wsdl>
                                        ${wsdlLocationDir}/ManageServiceMetadataService-1.0.wsdl
                                    </wsdl>
                                    <bindingFiles>
                                        <bindingFile>
                                            ${wsdlLocationDir}/ManageServiceMetadataService-1.0.jaxws.xml
                                        </bindingFile>
                                        <bindingFile>
                                            ${wsdlLocationDir}/commonServiceBinding.xjb
                                        </bindingFile>
                                    </bindingFiles>
                                    <extraargs>
                                        <extraarg>-xjc-Xannotate</extraarg>
                                    </extraargs>
                                </wsdlOption>
                            </wsdlOptions>
                        </configuration>
                        <goals>
                            <goal>wsdl2java</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>build-helper-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <id>add-source</id>
                        <phase>generate-sources</phase>
                        <goals>
                            <goal>add-source</goal>
                        </goals>
                        <configuration>
                            <sources>
                                <source>${basedir}/target/generated/src/main/java</source>
                            </sources>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
</project>
